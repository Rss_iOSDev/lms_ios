
import UIKit
import Reachability


let reach = try? Reachability()
var userDef = UserDefaults.standard
var user : UserModal?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
        let decoder = JSONDecoder()
            if let userM = try? decoder.decode(UserModal.self, from: userInfo) {
                user = userM
                let _window = UIWindow(frame: UIScreen.main.bounds)
                window = _window
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
                let rootViewController = initialViewController
                let navigationController = UINavigationController()
                navigationController.navigationBar.isHidden = true
                navigationController.viewControllers = [rootViewController]
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()
            }
        }
        else{
            let _window = UIWindow(frame: UIScreen.main.bounds)
            window = _window
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            let rootViewController = initialViewController
            let navigationController = UINavigationController()
            navigationController.navigationBar.isHidden = true
            navigationController.viewControllers = [rootViewController]
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
        }
        
        return true
    }

}


class Switcher {
    
    static func updateRootVC(){
        var rootVC : UIViewController?
        if let userInfo = userDef.object(forKey: "userInfo") as? Data {
            let decoder = JSONDecoder()
            if let user = try? decoder.decode(UserModal.self, from: userInfo) {
                Globalfunc.print(object:user.fname)
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! homeViewController
                appDelegate.window?.rootViewController = rootVC
            }
        }
        else{
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
            rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewController") as! loginViewController
            appDelegate.window?.rootViewController = rootVC
        }
    }
}
