//
//  BaseApi.swift
//  LMS
//
//  Created by Reinforce on 09/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import Foundation
import UIKit

class BaseApi {
    
    static var VC = UIViewController()
    
    
    
    static func onResponsePost(url: String ,parms: NSDictionary, completion: @escaping (_ res:Any) -> Void) {
        
       // Globalfunc.showLoaderView(view: controller.view)
        
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration)
            let url = NSURL(string:"\(url)")
            let request = NSMutableURLRequest(url: url! as URL)
            request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"

            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
                let task = session.dataTask(with: request as URLRequest) {
                    data, response, error in
                    guard let data = data, error == nil else
                    {
                          //  ApiResponse.alert(title: ApiResponse.getLanguageFromUserDefaults(inputString: "time_out"), message: ApiResponse.getLanguageFromUserDefaults(inputString: "try_again"), controller: controller)
                        return
                    }
                    if let httpStatus = response as? HTTPURLResponse,
                        
                        httpStatus.statusCode != 200 { // check for httperrors
                        if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
                        {
                            let _ = parsedData["msg"] as! String
                            OperationQueue.main.addOperation {
                               // Globalfunc.hideLoaderView(view: controller.view)
                               // Globalfunc.showAlertMessage(vc: controller, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                    if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
                    {
                        let success = parsedData["success"] as! Bool
                        if (success == true){
                            
                            completion(parsedData as NSDictionary)
                        }
                        else{
                            completion(parsedData as NSDictionary)
                        }
                    }
                    }
                    task.resume()
                }
                catch
                {

                }
  //          }

//            else {
//
//                let showMsg = ApiResponse.getLanguageFromUserDefaults(inputString: "oops_no_active_internet")
//
//                ApiResponse.alert(title: "", message: showMsg, controller: controller)
//
//            }


        }
    
    
    static func callApiRequestForGet(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
          
          var searchURL = NSURL()
          if let url = NSURL(string: "\(url)")
          {
              searchURL = url
          }
          else {
              let Nurl : NSString = url as NSString
              Globalfunc.print(object: Nurl)
              let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
              searchURL = NSURL(string: urlStr as String)!
              Globalfunc.print(object: searchURL)
          }
          
            var request = URLRequest(url: searchURL as URL)
            if let authToken = userDef.value(forKey: "authToken") {
            request.httpMethod = "GET"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("\(authToken)", forHTTPHeaderField: "Authorization")

        }
          
          
          URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            
             if let httpStatus = response as? HTTPURLResponse,
                 httpStatus.statusCode == 401 { // check for httperrors
                 if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                 {
                     Globalfunc.print(object:parsedData)
                     let dd = parsedData as! NSDictionary
                     let msg = dd["msg"] as! String
                     if(msg == "Your token has expired."){
                        OperationQueue.main.addOperation {
                            VC.callRefreshTokenFunc()
                            return
                        }
                     }
                 }
             }
            
              // Check if data was received successfully
              if error == nil && data != nil {
                  do {
                      // Convert NSData to Dictionary where keys are of type String, and values are of any type
                     let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        completionHandler(json, "")
                        
                      //do your stuff
                      
                  } catch {
                    let json : Any = (Any).self
                    completionHandler(json, "")
                  }
              }
              else if error != nil
              {
                let json : Any = (Any).self
                 completionHandler(json, "error")
              }
          }).resume()
          
          
          
      }
    
      static func callApiRequestForGetWithoutToken(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
            
            var searchURL = NSURL()
            if let url = NSURL(string: "\(url)")
            {
                searchURL = url
            }
            else {
                let Nurl : NSString = url as NSString
                Globalfunc.print(object: Nurl)
                let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
                searchURL = NSURL(string: urlStr as String)!
                Globalfunc.print(object: searchURL)
            }
            
              var request = URLRequest(url: searchURL as URL)
              request.httpMethod = "GET"
              request.setValue("application/json", forHTTPHeaderField: "Content-Type")

            
            URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
              
               if let httpStatus = response as? HTTPURLResponse,
                   
                   httpStatus.statusCode != 200 { // check for httperrors
                   if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                   {
                       Globalfunc.print(object:parsedData)
                       let dd = parsedData as! NSDictionary
                       let msg = dd["msg"] as! String
                       Globalfunc.print(object:msg)
                      
                   }
               }
                // Check if data was received successfully
                if error == nil && data != nil {
                    do {
                        // Convert NSData to Dictionary where keys are of type String, and values are of any type
                       let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                          completionHandler(json, "")
                          
                      
                        //do your stuff
                        
                    } catch {
                      let json : Any = (Any).self
                      completionHandler(json, "Status_Not_200")
                    }
                }
                else if error != nil
                {
                  let json : Any = (Any).self
                   completionHandler(json, "error")
                }
            }).resume()
            
            
            
        }
      
        static func onResponsePostWithToken(url: String, controller: UIViewController ,parms: Any, completion: @escaping (_ res: Any, _ error: String) -> Void) {
            
            
                let configuration = URLSessionConfiguration.default
                let session = URLSession(configuration: configuration)
                let url = NSURL(string:"\(url)")
                let request = NSMutableURLRequest(url: url! as URL)
                request.httpMethod = "POST"
            
                if let authToken = userDef.value(forKey: "authToken") {
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                    request.setValue("\(authToken)", forHTTPHeaderField: "Authorization")
                }

                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
                    let task = session.dataTask(with: request as URLRequest) {
                        data, response, error in
                        
                        if let httpStatus = response as? HTTPURLResponse,
                            httpStatus.statusCode == 401 { // check for httperrors
                            if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                                {
                                    Globalfunc.print(object:parsedData)
                                    let dd = parsedData as! NSDictionary
                                    let msg = dd["msg"] as! String
                               
                                    if(msg == "Your token has expired."){
                                        OperationQueue.main.addOperation {
                                            VC.callRefreshTokenFunc()
                                            return
                                        }
                                    }
                                }
                        }
                        if error == nil && data != nil{
                            do {
                               let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                  completion(json, "")
                            }
                            catch {
                              let json : Any = (Any).self
                              completion(json, "Status_Not_200")
                            }
                            }
                        else{
                            let json : Any = (Any).self
                            completion(json, error.debugDescription)
                        }
                        }
                        task.resume()
                    }
                    catch
                    {

                    }
        }
    
    
    
        static func onResponsePutWithToken(url: String, controller: UIViewController ,parms: NSDictionary, completion: @escaping (_ res: Any, _ error: String) -> Void)
        {
                    let configuration = URLSessionConfiguration.default
                    let session = URLSession(configuration: configuration)
                    let url = NSURL(string:"\(url)")
                    let request = NSMutableURLRequest(url: url! as URL)
                    request.httpMethod = "PUT"
                
                    if let authToken = userDef.value(forKey: "authToken") {
                        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                        request.setValue("\(authToken)", forHTTPHeaderField: "Authorization")
                    }


                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
                        let task = session.dataTask(with: request as URLRequest) {
                            data, response, error in
                           
                            if let httpStatus = response as? HTTPURLResponse,
                                httpStatus.statusCode == 401 { // check for httperrors
                                if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                                    {
                                        Globalfunc.print(object:parsedData)
                                        let dd = parsedData as! NSDictionary
                                        let msg = dd["msg"] as! String
                                   
                                        if(msg == "Your token has expired."){
                                            OperationQueue.main.addOperation {
                                                VC.callRefreshTokenFunc()
                                                return
                                            }
                                        }
                                    }
                            }
                            
                            if error == nil && data != nil{
                                do {
                                   let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                      completion(json, "")
                                }
                                catch {
                                  let json : Any = (Any).self
                                  completion(json, "Status_Not_200")
                                }
                                }
                            }
                            task.resume()
                    }
                    catch
                    {

                    }
            }
    
    
    
    static func onResponseDeleteWithToken(url : String, completionHandler: @escaping (_ result: Any, _ error: String) -> Void){
        
        var searchURL = NSURL()
        if let url = NSURL(string: "\(url)")
        {
            searchURL = url
        }
        else {
            let Nurl : NSString = url as NSString
            Globalfunc.print(object: Nurl)
            let urlStr : NSString = Nurl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            searchURL = NSURL(string: urlStr as String)!
            Globalfunc.print(object: searchURL)
        }
        
          var request = URLRequest(url: searchURL as URL)
      
          if let authToken = userDef.value(forKey: "authToken") {
          
          request.httpMethod = "DELETE"
          request.setValue("application/json", forHTTPHeaderField: "Content-Type")
          request.setValue("\(authToken)", forHTTPHeaderField: "Authorization")

      }
        
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
          
           if let httpStatus = response as? HTTPURLResponse,
               
               httpStatus.statusCode == 401 { // check for httperrors
               if let parsedData = try? JSONSerialization.jsonObject(with: data!)
               {
                   Globalfunc.print(object:parsedData)
                   let dd = parsedData as! NSDictionary
                   let msg = dd["msg"] as! String
                  
                   if(msg == "Your token has expired."){
                      OperationQueue.main.addOperation {
                          VC.callRefreshTokenFunc()
                          return
                      }
                   }
               }
           }
          
            // Check if data was received successfully
            if error == nil && data != nil {
                do {
                    // Convert NSData to Dictionary where keys are of type String, and values are of any type
                   let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                      completionHandler(json, "")
                      
                  
                    //do your stuff
                    
                } catch {
                  let json : Any = (Any).self
                  completionHandler(json, "")
                }
            }
            else if error != nil
            {
              let json : Any = (Any).self
               completionHandler(json, "error")
            }
        }).resume()
        
        
        
    }
    
    
    static func onResponseDeleteWithTP(url: String, controller: UIViewController ,parms: NSDictionary, completion: @escaping (_ res: Any, _ error: String) -> Void)
    {
                let configuration = URLSessionConfiguration.default
                let session = URLSession(configuration: configuration)
                let url = NSURL(string:"\(url)")
                let request = NSMutableURLRequest(url: url! as URL)
                request.httpMethod = "DELETE"
            
                if let authToken = userDef.value(forKey: "authToken") {
                    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
                    request.setValue("\(authToken)", forHTTPHeaderField: "Authorization")
                }


                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
                    let task = session.dataTask(with: request as URLRequest) {
                        data, response, error in
                       
                        if let httpStatus = response as? HTTPURLResponse,
                            httpStatus.statusCode == 401 { // check for httperrors
                            if let parsedData = try? JSONSerialization.jsonObject(with: data!)
                                {
                                    Globalfunc.print(object:parsedData)
                                    let dd = parsedData as! NSDictionary
                                    let msg = dd["message"] as! String
                               
                                    if(msg == "Your token has expired."){
                                        OperationQueue.main.addOperation {
                                            return
                                        }
                                    }
                                }
                        }
                        
                        if error == nil && data != nil{
                            do {
                               let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                  completion(json, "")
                            }
                            catch {
                              let json : Any = (Any).self
                              completion(json, "Status_Not_200")
                            }
                            }
                        }
                        task.resume()
                }
                catch
                {

                }
        }
}
