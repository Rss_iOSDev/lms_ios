import Foundation

class Constant{
    
  //  http://3.129.150.191/lms

    
    static let base_Url :String = "http://3.129.150.191:3000/api/" //server
    
    static let docBaseUrl :String = "http://3.129.150.191:3000/"
    
    //static let base_Url :String = "http://c19cb629f18d.ngrok.io/api/"  //local
    
    static let file_baseUrl = "http://3.129.150.191:3000/uploads/files/"
    
    static let login_url :String = "\(Constant.base_Url)auth/signin"
    
    static let forgetPass_url :String = "\(Constant.base_Url)forgetpassword"
    
    static let add_user_url :String = "\(Constant.base_Url)auth/signup"
    
    static let borrower_url :String = "\(Constant.base_Url)account/borrower"
    
    static let search_borrower_url :String = "\(Constant.base_Url)account/borrower/bookAccountSearch"
    
    static let user_list_url :String = "\(Constant.base_Url)user/status"
    
    static let institution_url: String = "\(Constant.base_Url)institute"
    
    static let timezone_url: String = "\(Constant.base_Url)timezone"
    
    static let default_branch_url: String = "\(Constant.base_Url)channel/ctypeid?ct_id=1"
    
    static let buyer_url: String = "\(Constant.base_Url)channel/ctypeid?ct_id=8"
    
    static let user_detail_url: String = "\(Constant.base_Url)user/id"
    
    static let update_user_pref: String = "\(Constant.base_Url)user"
    
    static let security_rights: String = "\(Constant.base_Url)security-rights"
    
    static let get_layoutId_url: String = "\(Constant.base_Url)layout-setting/layoutid"
    
    static let layout_setting_url: String = "\(Constant.base_Url)layout-setting"
    
    static let queue_layouts_url: String = "\(Constant.base_Url)queue-layouts/layoutname?dataSourceId=1"
    
    static let layoutcolumn_url: String = "\(Constant.base_Url)queue-layouts/layoutcolumn"
    
    static let queue_group: String = "\(Constant.base_Url)queue-group/datasource"
    

    static let get_layout_url: String = "\(Constant.base_Url)loan-account/loan-accounts"
    
    static let master_url: String = "\(Constant.base_Url)master"
    
    static let account_type_url: String = "\(Constant.base_Url)account-type"
    
    static let account_status_url: String = "\(Constant.base_Url)account-status"
    
    static let portfolio_url: String = "\(Constant.base_Url)portfolio"
    
    static let flag_url: String = "\(Constant.base_Url)flag"
    
    static let account_flag: String = "\(Constant.base_Url)loan-account/account-flag"
    
    static let relationship: String = "\(Constant.base_Url)relationship"
    
    /*------Call Option-----*/
    
    static let getCall_url: String = "\(Constant.base_Url)loan-account/action"
    
    static let Savecontact_url: String = "\(Constant.base_Url)contact"
    
    /*------Notes*/
    
    static let getNotes_url: String = "\(Constant.base_Url)notes/search-notes"
    
    /*------Send----*/
    static let getTemplate_url: String = "\(Constant.base_Url)comm-template?status=false&dataSource=all"
    
    static let getTemplate_layout_url: String = "\(Constant.base_Url)comm-template/layout"
    
    static let send_url: String = "\(Constant.base_Url)send"
    
    /*------Promise to Pay----*/
    static let promise_url: String = "\(Constant.base_Url)promise-to-pay/accid"
    
    static let delivery_method_url: String = "\(Constant.base_Url)delivery-method"
    
    static let pay_form_url: String = "\(Constant.base_Url)delivery-method/id"
    
    static let promise_pay_url: String = "\(Constant.base_Url)promise-to-pay"
    
    static let eps_account: String = "\(Constant.base_Url)eps-account"
    
    
    
    
    
    
    /*-Payment Post----*/
    
    static let payment_type_url: String = "\(Constant.base_Url)payment-type"
    
    static let payment_url: String = "\(Constant.base_Url)loan-account/payment"
    
    static let id_acctDetails: String = "\(Constant.base_Url)loan-account/id"
    
    static let repo_url: String = "\(Constant.base_Url)repo/accid"
    
    static let insurance_url: String = "\(Constant.base_Url)insurance/accid"
    
    static let gap_insurance_url: String = "\(Constant.base_Url)gap-insurance"
    
     static let payment_schedule_url: String = "\(Constant.base_Url)loan-account/payment-schedule"
    
    static let transac_url: String = "\(Constant.base_Url)transaction"
    
    static let receipt: String = "\(Constant.base_Url)transaction/receipt"
    
    
    static let file_url: String = "\(Constant.base_Url)loan-account/add-file"
    
   
    
    static let borrower_refrence: String = "\(Constant.base_Url)add-reference/accid"
    
    static let add_reference: String = "\(Constant.base_Url)add-reference"
    
    static let ecabinet_doc_url: String = "\(Constant.base_Url)document/accid"
    
    

    //Snapshot Api
    
    static let remaingPay: String = "\(Constant.base_Url)loan-account/snapshot"
    
    static let snap_notes: String = "\(Constant.base_Url)notes/account_id"
    
    
    static let collateral_vehicle : String = "\(Constant.base_Url)collateral/collateral-vehicle"
    
    static let Coll_img_url : String = "\(Constant.base_Url)loan-account/add-image"
    
    static let Coll_purchase : String = "\(Constant.base_Url)collateral/collateral-purchase"
    
    static let Coll_paymnt : String = "\(Constant.base_Url)collateral/collateral-payment-gps-device"
    
    static let Coll_options : String = "\(Constant.base_Url)collateral/collateral-options"
    
    static let Coll_pricing : String = "\(Constant.base_Url)collateral/collateral-pricing"
    
    static let balances : String = "\(Constant.base_Url)loan-account/balances"

    static let post_payment : String = "\(Constant.base_Url)loan-account/payment"
    
    static let pay_perf_color : String = "\(Constant.base_Url)payment-performance"
    
    static let pay_perf_data : String = "\(Constant.base_Url)loan-account/payment-performance"
    
    static let chargeOff : String = "\(Constant.base_Url)loan-account/chargeOff"
    
    static let AppName = "LMS"
    
    static var arrBookingstatus = ["working","on hold","booked"]
    
    static var arrSalutation = ["Mr." , "Mrs." , "Miss.", "Ms."];
    
    static var arrSuffix = ["Jr." , "Sr." , "I", "II", "III"];
    
    static var arrTimeZoneData : NSMutableArray = []
    
    static var arrInstitutionData : NSMutableArray = []
    
    static var arrUrl_forCommon = ["\(Constant.timezone_url)","\(Constant.institution_url)","\(Constant.default_branch_url)"]
    
    static var arrDElivryMethod : NSMutableArray = []
    
    
    
    //Action Tabs Api
    
    
    static let get_agentList: String = "\(Constant.base_Url)channel/ctypeid?ct_id=9"
    
    static let post_OutforRepo: String = "\(Constant.base_Url)repo"
    
    static let action_redeemRepo : String = "\(Constant.base_Url)repo/redeemrepo"
    
    static let action_calcelRepo : String = "\(Constant.base_Url)repo/cancel"
    
    static let action_notes : String = "\(Constant.base_Url)notes"
    
    static let action_update_alert : String = "\(Constant.base_Url)loan-account/update-alert"
    
    static let assign_getUSer : String = "\(Constant.base_Url)user"
    
    static let action_assgn_task : String = "\(Constant.base_Url)loan-account/add-assign-task"
    
    static let action_insurance : String = "\(Constant.base_Url)insurance"
    
    static let action_update_rtc : String = "\(Constant.base_Url)loan-account/update-rtc"
    
    static let action_update_cr : String = "\(Constant.base_Url)loan-account/update-credit-bureau"
    
    static let action_acctLock : String = "\(Constant.base_Url)loan-account/account-lock"
    
    static let action_cons_ind_info : String = "\(Constant.base_Url)cons-ind-info"
    
    static let action_bankRuptcy : String = "\(Constant.base_Url)loan-account/bank-ruptcy"
    
    //get api for update insurance Action
    
    static let ins_Agt : String = "\(Constant.base_Url)channel/ctypeid?ct_id=6"
    
    static let ins_test : String = "\(Constant.base_Url)channel/ctypeid?ct_id=7"

    //get api for update or record repo
    
    static let recover_rec : String = "\(Constant.base_Url)channel/ctypeid?ct_id=9"
    
    static let state : String = "\(Constant.base_Url)state"
    
    static let country : String = "\(Constant.base_Url)country"
    
    static let vehicle_loc : String = "\(Constant.base_Url)channel/ctype"
    
    static let get_repo_record : String = "\(Constant.base_Url)repo/repossession"
    
    //borrower
    static let borro_detail: String = "\(Constant.base_Url)borrower/detail"
    
    static let detail_identity_info: String = "\(Constant.base_Url)borrower/detail-identity-info"
    
    static let detail_drivers_license: String = "\(Constant.base_Url)borrower/detail-drivers-license"
    
    static let detail_doing_business: String = "\(Constant.base_Url)borrower/detail-doing-business-as"
    
    static let detail_alternate_id_info: String = "\(Constant.base_Url)borrower/detail-alternate-id-info"
    
    static let address_history: String = "\(Constant.base_Url)borrower/address-history"
    
    static let employment_history: String = "\(Constant.base_Url)borrower/employment-history"
    
    static let ref_identity_info: String = "\(Constant.base_Url)add-reference/detail-identity-info"
    
    static let detail_contact_info: String = "\(Constant.base_Url)borrower/detail-contact-info"
    
    static let detail_address: String = "\(Constant.base_Url)borrower/detail-address"
    
     static let detail_employment: String = "\(Constant.base_Url)borrower/detail-employment"
    
    static let get_reference: String = "\(Constant.base_Url)borrower/get-reference"

    static let borrower_accounts: String = "\(Constant.base_Url)borrower/borrower-accounts"

    //Dollar section Api
    
    static let post_extension: String = "\(Constant.base_Url)loan-account/post-extension"
    
    static let skip_auto_pay: String = "\(Constant.base_Url)loan-account/skip-auto-pay"
    
    static let extention_reverse: String = "\(Constant.base_Url)loan-account/extention-reverse"
    
    static let rev_skip_auto_pay: String = "\(Constant.base_Url)loan-account/rev-skip-auto-pay"
    
    
    
    
    
    
    static let post_reverse: String = "\(Constant.base_Url)transaction/post-reverse"
    
    static let post_adjustment: String = "\(Constant.base_Url)loan-account/post-adjustment"
    
    static let post_nsf: String = "\(Constant.base_Url)transaction/post-nsf"
    
    static let reverse_chargeOff: String = "\(Constant.base_Url)loan-account/reverse-chargeOff"
    
    static let account_details: String = "\(Constant.base_Url)loan-account/account-details"
    
    static let post_payoff: String = "\(Constant.base_Url)loan-account/post-payoff"
    
    static let post_chargeOff: String = "\(Constant.base_Url)loan-account/chargeOff"
    
    static let post_writedown: String = "\(Constant.base_Url)loan-account/postWriteDown"
    
    static let side_note: String = "\(Constant.base_Url)loan-account/side-note"
    
    static let side_note_calc: String = "http://b054d716428f.ngrok.io/api/loan-account/ajax_calculator"
    
    //Setting
    //http://3.129.150.191:3000/api/channel/id?id=1
    static let channels: String = "\(Constant.base_Url)channel/channels"
    
    static let channeltype: String = "\(Constant.base_Url)channeltype"
    
    static let addChannel: String = "\(Constant.base_Url)channel"
    static let buyerGuide : String = "\(Constant.base_Url)channel/buyersGuideDefaults"
    static let accounting: String = "\(Constant.base_Url)accounting"
    
    static let channlId: String = "\(Constant.base_Url)channel/ctypeid?ct_id=2"
    static let channelSellerFees : String = "\(Constant.base_Url)channel/sellerFees"
   
    static let channelSellerTaxes : String = "\( Constant.base_Url)channel/sellerTaxes"
    static let channelAccessory : String = "\( Constant.base_Url)channel/accessories"
    
    //Tasking
    static let bin: String = "\(Constant.base_Url)bin"
    
    static let remove_record: String = "\(Constant.base_Url)bin/remove-record"
    
    static let record_type: String = "\(Constant.base_Url)record-type"
    
    static let queue: String = "\(Constant.base_Url)queue"
    
    static let add_queue: String = "\(Constant.base_Url)bin/add-queue"
    
    static let task: String = "\(Constant.base_Url)task"
    
    static let step: String = "\(Constant.base_Url)step"
    
    static let task_step: String = "\(Constant.base_Url)task-step"
    
    static let runtask: String = "\(Constant.base_Url)task-step/runtask"
    
    static let task_notes: String = "\(Constant.base_Url)task-notes"
    
    static let docTemplate: String = "\(Constant.base_Url)doc-template?status=false&dataSource=all"
    
    static let inventory_docTemplate: String = "\(Constant.base_Url)doc-template?status=false&dataSource=11"
    
    static let generate_pdf: String = "\(Constant.base_Url)doc-template/generate-pdf"
    
    static let up_document: String = "\(Constant.base_Url)document"
    
    static let favorite: String = "\(Constant.base_Url)favorite"
    
    static let reports: String = "\(Constant.base_Url)reports"
    


    ///Inventory---------------
    //http://3.129.150.191:3000/api/inventory/titling?id=17
    
    static let inventory_queue_layout: String = "\(Constant.base_Url)inventory/inventory"
    static let inventory: String = "\(Constant.base_Url)inventory"
    static let inventory_titling: String = "\(Constant.base_Url)titling"
    static let inventory_notes: String = "\(Constant.base_Url)inventory-notes"
    static let inventory_vehicle: String = "\(Constant.base_Url)inventory-vehicle/"
    static let decode_vin: String = "\(Constant.base_Url)zip/vin-decode"
    static let colorApi: String = "\(Constant.base_Url)color/type"
    
    static let inevtry_layoutS: String = "\(Constant.base_Url)queue-layouts/layoutname?dataSourceId=11"
    static let rem_invntry: String = "\(Constant.base_Url)inventory/remove-inventory"
    
}



