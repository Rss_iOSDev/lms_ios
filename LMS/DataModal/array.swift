//
//  File.swift
//  LMS
//
//  Created by Apple on 25/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import Foundation

class consArrays{
    static var arrDefaultBranch : NSMutableArray = []
    
    static var arrHour = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14",
                          "15","16","17","18","19","20","21","22","23"]
    
    static var arrMin = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14",
                          "15","16","17","18","19","20","21","22","23","24","25","26","27",
                          "28","29","30","31","32","33","34","35","36","37","38","39","40",
                          "41","42","43","44","45","46","47","48","49","50","51","52","53",
                          "54","55","56","57","58","59"]
    
    
    
    static var arrRecoveryTyp = [["title":"Bloodhound","id":"1"],
                                 ["title":"CalAmp (Aercept) GPS (non-integrated)","id":"2"],
                                 ["title":"Callpass","id":"3"],
                                 ["title":"Enfotrace","id":"4"],
                                 ["title":"Gold Star GPS (non-integrated)","id":"5"],
                                 ["title":"Guidepoint Systems","id":"6"],
                                 ["title":"iGotcha","id":"7"],
                                 ["title":"Imetrik (non-integrated)","id":"8"],
                                 ["title":"Inilex GPS","id":"9"],
                                 ["title":"InterTrak GPS","id":"10"],
                                 ["title":"ITURAN (non-integrated)","id":"11"],
                                 ["title":"One Track","id":"12"],
                                 ["title":"OnTime PPS","id":"13"],
                                 ["title":"PassTime (non-integrated)","id":"14"],
                                 ["title":"Pay Teck","id":"15"],
                                 ["title":"Position Plus GPS","id":"16"],
                                 ["title":"RCI","id":"17"],
                                 ["title":"SareKon","id":"18"],
                                 ["title":"Sky Patrol (non-integrated)","id":"19"],
                                 ["title":"Sky Watch","id":"20"],
                                 ["title":"SkyHawk","id":"21"],
                                 ["title":"Star Sensor","id":"22"],
                                 ["title":"SVR Tracking","id":"23"],
                                 ["title":"SysLocate","id":"24"],
                                 ["title":"Ufollowit","id":"25"],
                                 ["title":"UTS","id":"26"],
                                 ["title":"Webtek","id":"27"]]
    
    
    
    static var arrRecoveryFunc = [["title":"GPS and Starter Interrupt","id":"1"],
                                  ["title":"GPS Only","id":"2"],
                                  ["title":"Starter Interrupt Only","id":"3"]]
    
    
    
    static var arrMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    
    static var arrMonthDates = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14",
    "15","16","17","18","19","20","21","22","23","24","25","26","27",
    "28","29","30","31"]
    
    static var arrYr = ["2016","2017","2018","2019","2020","2021","2022","2023","2024","2025","2026","2027","2028","2029","2030","2031"]

    
    

}

class consText{
    
    static let additionalText = "Anti-Brake System    4-Wheel ABS \n Basic-distance    36,000 mile \n Basic-duration    36 month \n Cargo Length    No data in. \n Cargo Volume    11.80 cu.ft. \n Curb Weight-automatic    2928 lbs \n Curb Weight-manual    2868 lbs \n Dealer Invoice    No data USD \n Depth    No data in. \n Destination Charge    $670 USD \n Front Brake Type    Disc \n Front Headroom    38.90 in. \n Front Hip Room    53.70 in. \n Front Legroom    42.00 in. \n Front Shoulder Room    54.90 in. \n Front Spring Type    Coil \n Front Suspension    Ind \n Fuel Economy-city    22 - 24 miles/gallon \n Fuel Economy-highway    30 - 32 miles/gallon \n Ground Clearance    4.70 in. \n Manufactured in    JAPAN \n Maximum GVWR    No data lbs \n Maximum Payload    No data lbs \n Maximum Towing    No data lbs \n MSRP    $15,975 USD \n Optional Seating    No data \n Overall Height    57.90 in. \n Overall Length    180.70 in. \n Overall Width    69.10 in. \n Passenger Volume    94.10 cu.ft. \n Powertrain-distance    60,000 mile \n Powertrain-duration    60 month \n Production Seq. Number    217683 \n Rear Brake Type    Disc \n Rear Headroom    37.50 in. \n Rear Hip Room    52.20 in. \n Rear Legroom    36.20 in. \n Rear Shoulder Room    54.00 in. \n Rear Spring Type    Coil \n Rear Suspension    Ind \n Rust-distance    Unlimited mile \n Rust-duration    60 month \n Standard GVWR    No data lbs \n Standard Payload    No data lbs \n Standard Seating    5 \n Standard Towing    No data lbs \n Steering Type    R&P \n Tank    14.50 gallon \n Tires    205/55R16 \n Track Front    60.40 in. \n Track Rear    59.80 in. \n Turning Diameter    34.20 in. \n Wheelbase    103.90 in. \n Width at Wall    No data in. \n Width at Wheelwell    No data in."
}
