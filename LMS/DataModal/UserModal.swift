struct ApiResponse: Codable{
    var msg: String
    var success: Bool
}

class UserModal: NSObject , Codable
{
   // var status: Int = 0
    var _id: Int = 0
   // var __v: Int = 0
  //  var alt_email: String = ""
  //  var cell_phone: String = ""
  //  var created_at: String = ""
    var email: String = ""
  //  var fax: String = ""
    var fname: String = ""
  //  var home_phone: String = ""
    var institute_id: Int?
    var institute_code: String = ""
    var institute_title: String = ""
    var lname: String = ""
    var is_deleted: Bool?
   // var mname: String = ""
  //  var other: String = ""
  //  var password: String = ""
    var role_id = [Role_id]()
  //  var salutation: String = ""
  //  var suffix: String = ""
  //  var timezone: String = ""
 //   var title: String = ""
  //  var updated_at: String = ""
  //  var user_code: String = ""
  //  var work_phone: String = ""
    
    enum CodingKeys:String,CodingKey
    {
        case email
        case fname
        case lname
        case institute_id
        case _id
        case institute_code
        case institute_title
        case is_deleted
        case role_id
       // case alt_email
    }
    
}



class Role_id : NSObject , Codable
{
    var id: Int = 0
    var title: String = ""
    enum CodingKeys:String,CodingKey
    {
        case id
        case title
    }
}
