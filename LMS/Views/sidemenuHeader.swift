//
//  sidemenuHeader.swift
//  LMS
//
//  Created by Reinforce on 19/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import Foundation
import UIKit

final class sidemenuHeader: UITableViewHeaderFooterView {
    static let reuseIdentifier: String = String(describing: self)
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgDropdown: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: nil)
    }

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        
        

//        imageView = UIImageView()
//        contentView.addSubview(imageView)
//
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//
//        imageView.widthAnchor.constraint(equalToConstant: 24.0).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: 24.0).isActive = true
//        imageView.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor).isActive = true
//        imageView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor).isActive = true
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
