//
//  AccordionHeaderView.swift
//  FZAccordionTableViewExample
//
//  Created by Krisjanis Gaidis on 10/5/15.
//  Copyright © 2015 Fuzz. All rights reserved.
//

import UIKit

class AccordionHeaderView: FZAccordionTableViewHeaderView {
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 305.0;
    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifier";
    
    @IBOutlet weak var view_bg : UIView!
    @IBOutlet weak var lblType : UILabel!
    @IBOutlet weak var lblAmt : UILabel!
    @IBOutlet weak var lblDueOn : UILabel!
    @IBOutlet weak var lblPstedOn : UILabel!
    @IBOutlet weak var lblDaysLate : UILabel!
    @IBOutlet weak var lblHistryTier : UILabel!
}
