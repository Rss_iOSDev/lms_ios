//
//  queueGridViewController.swift
//  LMS
//
//  Created by Apple on 23/02/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class queueGridViewController: UIViewController {
    
    var passId = ""
    
    var isTyp = ""
    
    @IBOutlet weak var lblTitle : UILabel!
    
    @IBOutlet weak var gridCollectionView: UICollectionView! {
        didSet {
            gridCollectionView.bounces = false
        }
    }
    
    @IBOutlet weak var gridLayout: StickyGridCollectionViewLayout! {
        didSet {
            gridLayout.stickyColumnsCount = 1
        }
    }
    var mainArrListData : [NSMutableArray] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(isTyp == "inventory"){
            self.lblTitle.text = "Inventory Queues"
        }else{
            self.lblTitle.text = "Account Queues"
        }

        self.callQueueGridListApi()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}

extension queueGridViewController {
    
    func callQueueGridListApi(){
        
            let url = "\(Constant.queue)/preview-queue?id=\(self.passId)"
            BaseApi.callApiRequestForGet(url: url) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let response = dict as? NSDictionary {
                            if let rr = response["data"] as? NSDictionary{
                                self.parseResponseAndSetData(dict: rr, strCheck: "get")
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }

    }
    
}




// MARK: - Collection view data source and delegate methods

extension queueGridViewController: UICollectionViewDataSource , UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.mainArrListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let arr = self.mainArrListData[section]
        return arr.count //column
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collAccountCell", for: indexPath) as? collAccountCell else {
            return UICollectionViewCell()
        }
        
        
        if(self.mainArrListData.count > 0){
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary
            {
                print(arrInside)
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    cell.lblLine.isHidden = true
                    cell.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                    cell.titleLabel.text = "\(arrInside["column"] as! String)"
                    cell.titleLabel.textColor = .white
                }
                else{
                    cell.lblLine.isHidden = false
                    cell.titleLabel.textColor = .black
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        cell.backgroundColor = .systemGroupedBackground
                        cell.titleLabel.text = "rrrr"
                    }
                    else{
                        Globalfunc.print(object:arrInside)
                        
                        if let val = arrInside["value"] as? String{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                        if let val = arrInside["value"] as? Int{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(self.mainArrListData.count > 0){
            
            let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as! NSDictionary
            let columnNam = arrInside["column"] != nil
            if(columnNam){
                //sorting
            }
            else{
                
                if(isTyp == "inventory"){
                    
                    let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as! NSDictionary
                    let id_dict = arrInside["id_data"] as! NSDictionary
                    let addId = id_dict["id"] as! Int
                    
                    let story = UIStoryboard.init(name: "Inventory", bundle: nil)
                    let detail = story.instantiateViewController(identifier: "inventoryDetailViewController") as! inventoryDetailViewController
                    //detail.isDetailOpenfrom = "invenrory"
                    detail.inventoryId = addId
                    //detail.passDict_LayoutId = self.dictLayout
                    detail.modalPresentationStyle = .fullScreen
                    self.present(detail, animated: true, completion: nil)

                }else{
                    
                    let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as! NSDictionary
                    let id_dict = arrInside["id_data"] as! NSDictionary
                    let addId = id_dict["id"] as! Int
                    let detail = self.storyboard?.instantiateViewController(identifier: "accountDetailPageController") as! accountDetailPageController
                    detail.isDetailOpenfrom = "accounts"
                    detail.accId = addId
                    detail.modalPresentationStyle = .fullScreen
                    self.present(detail, animated: true, completion: nil)

                }
                
            }
        }
    }
}

extension queueGridViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height = 0.0
        let width = (collectionView.frame.width) / 2
        
        if(self.mainArrListData.count > 0){
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary{
                
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    height = 30.0
                }
                else{
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        height = 10.0
                    }
                    else{
                        height = 50
                    }
                }
            }
        }
        return CGSize(width: Double(width), height: height)
    }
}




//MARK:
extension queueGridViewController{
    
    func parseResponseAndSetData(dict : Any, strCheck : String){
        
        if let dictResponse = dict as? NSDictionary {
            
            Globalfunc.print(object:dict)
            
//            if(strCheck == "get"){
                
                
                
//                let cpNo = CGFloat(currentPageNumber)
//                let pagSize = CGFloat(defaultRecordsPage)
                
//                let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
//                let pagecount = Int(pageNo)
//
//                let buttonPadding:CGFloat = 10
//                var xOffset:CGFloat = 10
//
//                for i in 0..<pagecount {
//                    if (i == 0){
//                        self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
//                    }
//                    let button = UIButton()
//                    button.tag = i+1
//                    button.setTitle("\(i+1)", for: .normal)
//                    button.layer.borderWidth = 1
//                    button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
//                    button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
//                    button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
//                    button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
//                    button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
//                    xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
//                    self.scrollViewPages.addSubview(button)
//                }
                
                
///            }
            
            let arrHeading = dictResponse["heading"] as! NSMutableArray
            print(arrHeading)
            
            for i in 0..<arrHeading.count{
                let new_arr : NSMutableArray = []
                let arr = arrHeading.object(at: i) as! NSMutableArray
                Globalfunc.print(object:arr)
                for i in 0..<arr.count{
                    let dict = arr[i] as! NSDictionary
                    let arr = dict["attribute"] as! NSDictionary
                    print(arr)
                    let name = dict["name"] as? String
                    let columnname = arr["colname"] as? String
                    let dictt = ["column":columnname,"name":name]
                    new_arr.add(dictt)
                }
                self.mainArrListData.append(new_arr)
            }
            
            let arrRowCount = dictResponse["loan"] as! NSMutableArray
            for i in 0..<arrRowCount.count
            {
                let arr0 = arrRowCount.object(at: i) as! NSMutableArray
                for j in 0..<arr0.count{
                    let arr1 = arr0[j] as! NSMutableArray
                    Globalfunc.print(object:arr1)
                    if(j == arr0.count-1){
                        let arrId = arr0[j] as! NSMutableArray
                        let dictId : NSMutableDictionary = [:]
                        let dd = arrId[0] as! NSDictionary
                        let val = dd["id"] as! Int
                        dictId.setValue(val, forKey: "id")
                        for _ in 0..<j-1{
                            let dict = ["id":""]
                            arr1.add(dict)
                        }
                        arr0.replaceObject(at: j, with: arr1)
                        arrRowCount.replaceObject(at: i, with: arr0)
                    }
                }
            }
            for x in 0..<arrRowCount.count{
                let arr0 = arrRowCount.object(at: x) as! NSMutableArray
                Globalfunc.print(object:arr0)
                let arr1 = arr0[arr0.count - 1] as! NSMutableArray
                let dictId = arr1[0] as! NSDictionary
                Globalfunc.print(object:dictId)
                for y in 0..<arr0.count{
                    let newArr = arr0[y] as! NSMutableArray
                    for z in 0..<newArr.count{
                        let newDict = newArr[z] as! NSDictionary
                        let tempDic: NSMutableDictionary = newDict.mutableCopy() as! NSMutableDictionary
                        tempDic.setValue(dictId, forKey: "id_data")
                        Globalfunc.print(object:tempDic)
                        newArr.replaceObject(at: z, with: tempDic)
                    }
                    self.mainArrListData.append(newArr)
                }
            }
            Globalfunc.print(object:self.mainArrListData)
            self.gridLayout.stickyRowsCount = arrHeading.count
            self.gridCollectionView.reloadData()
        }
    }
}
