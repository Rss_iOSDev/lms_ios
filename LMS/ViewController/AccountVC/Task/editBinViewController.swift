//
//  editBinViewController.swift
//  LMS
//
//  Created by Apple on 10/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class editBinViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtBtn: UITextField!
    @IBOutlet weak var txtRecordTyp: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPrivate: UITextField!
    @IBOutlet weak var txtqueue: UITextField!
    
    @IBOutlet weak var tblAccount: UITableView!
    
    @IBOutlet weak var viewTbl: UIView!
    @IBOutlet weak var hgtView: NSLayoutConstraint!
    @IBOutlet weak var lblRecodStatus: UILabel!
    
    @IBOutlet weak var lblbinRecord: UILabel!
    @IBOutlet weak var lblbinRecordCount: UILabel!
    
    var pickerStatus: UIPickerView!
    var arrStatus : [[String : Any]] = []
    
    var pickerPrivate: UIPickerView!
    var arrPrivate : [[String : Any]] = []
    
    var recTyp = 0
    var status = false
    var strPrivate = true
    
    var passBinId : Int?
    var detailDict : NSDictionary = [:]
    
    var arrUrl = [Constant.queue,Constant.record_type]
    
    var arrQueue = NSMutableArray()
    var pickerQueue: UIPickerView!
    var queueId = 0
    
    var arrRecordTyp = NSMutableArray()
    
    var acctList = NSMutableArray()
    
    var arrSelectId = NSMutableArray()
    var arrSelectIndex = NSMutableArray()
    var arrSelectAccId = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dict_1 = ["title":"Active","value":false] as [String : Any]
        let dict_2 = ["title":"Delete","value":true] as [String : Any]
        arrStatus.append(dict_1)
        arrStatus.append(dict_2)
        
        let dict_3 = ["title":"Yes","value":true] as [String : Any]
        let dict_4 = ["title":"No","value":false] as [String : Any]
        arrPrivate.append(dict_3)
        arrPrivate.append(dict_4)
        
        self.viewTbl.isHidden = true
        self.lblRecodStatus.isHidden = true
        self.hgtView.constant = 0
        self.lblbinRecord.isHidden = true
        self.lblbinRecordCount.isHidden = true
        
        self.tblAccount.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerData()
        
    }
}

extension editBinViewController {
    
    @IBAction func clickONCheckBoxes(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if sender.isSelected == true {
                sender.isSelected = false
                
                for i in 0..<self.acctList.count{
                    let dd = self.acctList[i] as! NSMutableDictionary
                    dd.setValue(false, forKey: "is_deleted")
                    self.acctList.replaceObject(at: i, with: dd)
                }
                self.arrSelectId.removeAllObjects()
                self.tblAccount.reloadData()
            }
            else {
                sender.isSelected = true
                self.arrSelectId = []
                self.arrSelectAccId = []
                self.arrSelectIndex = []
                
                if(self.acctList.count > 0){
                    for i in 0..<self.acctList.count{
                        let dd = self.acctList[i] as! NSMutableDictionary
                        let _id = dd["id"] as! Int
                        let acc_id = dd["acc_id"] as! Int
                        let index = dd["index"] as! Int
                        self.arrSelectId.add(_id)
                        self.arrSelectAccId.add(acc_id)
                        self.arrSelectIndex.add(index)
                        dd.setValue(true, forKey: "is_deleted")
                        self.acctList.replaceObject(at: i, with: dd)
                    }
                }
                self.tblAccount.reloadData()
            }
        }
        else if(sender.tag == 20){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
        else if(sender.tag == 30){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
    }
}

extension editBinViewController {
    
    func callAllPickerData()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.queue){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrQueue.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        if(obj == Constant.record_type){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrRecordTyp.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            let url = "\(Constant.bin)/id?id=\(self.passBinId!)"
                            self.call_getApiwithParam(strUrl: url, strTyp: "id")
                        })
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
}


extension editBinViewController {
    func call_getApiwithParam(strUrl: String, strTyp: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(strTyp == "id"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let data = dict as? NSDictionary {
                            
                            print(data)
                            
                            if let response = data["data"] as? NSDictionary
                            {
                                self.detailDict = response
                                if let _id = response["_id"] as? Int
                                {
                                    self.passBinId = _id
                                    self.lblTitle.text = "Edit Bin \(_id)"
                                }
                                if let binName = response["binName"] as? String
                                {
                                    self.txtBtn.text = binName
                                }
                                if let recordType = response["recordType"] as? NSDictionary {
                                    for i in 0..<self.arrRecordTyp.count{
                                        let dict = self.arrRecordTyp[i] as! NSDictionary
                                        if(dict == recordType){
                                            let strTitle = dict["title"] as! String
                                            self.txtRecordTyp.text = strTitle
                                            let _id = dict["_id"] as! Int
                                            self.recTyp = _id
                                            break
                                        }
                                    }
                                }
                                if let isPrivate = response["isPrivate"] as? Bool {
                                    for i in 0..<self.arrPrivate.count{
                                        let dict = self.arrPrivate[i]
                                        let value = dict["value"] as! Bool
                                        if(value == isPrivate){
                                            let strTitle = dict["title"] as! String
                                            self.txtPrivate.text = strTitle
                                            self.strPrivate = value
                                            break
                                        }
                                    }
                                }
                                
                                if let accounts = response["accounts"] as? NSMutableArray{
                                    if(accounts.count > 0){
                                        self.viewTbl.isHidden = false
                                        self.lblRecodStatus.isHidden = true
                                        self.hgtView.constant = 600
                                        for i in 0..<accounts.count{
                                            let dictA = accounts[i] as! NSMutableDictionary
                                            dictA.setValue(false, forKey: "is_deleted")
                                            self.acctList.add(dictA)
                                        }
                                        self.tblAccount.reloadData()
                                        self.lblbinRecord.isHidden = false
                                        self.lblbinRecordCount.isHidden = false
                                        self.lblbinRecordCount.text = "\(accounts.count)"
                                        
                                        
                                    }
                                    else{
                                        self.acctList = []
                                        self.hgtView.constant = 0
                                        self.viewTbl.isHidden = true
                                        self.lblRecodStatus.isHidden = false
                                        self.lblbinRecord.isHidden = true
                                        self.lblbinRecordCount.isHidden = true
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.bin, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "post"){
            
            BaseApi.onResponsePostWithToken(url: Constant.add_queue, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                    let url = "\(Constant.bin)/id?id=\(self.passBinId!)"
                                    self.call_getApiwithParam(strUrl: url, strTyp: "id")
                                })
                                
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
    }
}

extension editBinViewController {
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        if(self.arrSelectId.count == 0){
            if(txtBtn.text == "" || txtBtn.text?.count == 0 || txtBtn.text == nil){
                txtBtn.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Bin should not left blank.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                Globalfunc.showLoaderView(view: self.view)
                let params = [ "binName": self.txtBtn.text!,
                               "insid": (user?.institute_id!)!,
                               "id": self.passBinId!,
                               "isPrivate":self.strPrivate,
                               "recordType": self.recTyp,
                               "status": self.status,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "put")
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else{
            
            Globalfunc.showLoaderView(view: self.view)
            
            let param = ["id":self.arrSelectId,
                         "index": self.arrSelectIndex,
                         "acc_id": self.arrSelectAccId,
                         "bin_id": self.passBinId!] as [String: Any]
            
            BaseApi.onResponseDeleteWithTP(url: Constant.remove_record, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        
        
    }
    
    @IBAction func clickOnAddQueueBtn(_ sender : UIButton)
    {
        if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            let params = ["id": self.passBinId!,
                          "queue_id": self.queueId] as [String : Any]
            Globalfunc.print(object:params)
            self.callUpdateApi(param: params, strTyp: "post")
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
}

extension editBinViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerStatus{
            return self.arrStatus.count
        }
        else if pickerView == pickerPrivate{
            return self.arrPrivate.count
        }
        else if pickerView == pickerQueue{
            return self.arrQueue.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerPrivate{
            let dict = arrPrivate[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerQueue{
            let dict = arrQueue[row] as! NSDictionary
            let queue_name = dict["queue_name"] as! String
            var title = ""
            if let queue_group = dict["queue_group"] as? NSDictionary{
                title = queue_group["title"] as! String
            }
            let strTitle = "[\(title)] \(queue_name)"
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            self.txtStatus.text = strTitle
            let value = dict["value"] as! Bool
            self.status = value
        }
        else if pickerView == pickerPrivate{
            let dict = arrPrivate[row]
            let strTitle = dict["title"] as! String
            self.txtPrivate.text = strTitle
            let value = dict["value"] as! Bool
            self.strPrivate = value
        }
        else if pickerView == pickerQueue{
            let dict = arrQueue[row] as! NSDictionary
            let queue_name = dict["queue_name"] as! String
            var title = ""
            if let queue_group = dict["queue_group"] as? NSDictionary{
                title = queue_group["title"] as! String
            }
            let strTitle = "[\(title)] \(queue_name)"
            self.txtqueue.text = strTitle
            let _id = dict["_id"] as! Int
            self.queueId = _id
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtStatus){
            self.pickUp(txtStatus)
        }
        else if(textField == self.txtPrivate){
            self.pickUp(txtPrivate)
        }
        else if(textField == self.txtqueue){
            self.pickUp(self.txtqueue)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtStatus){
            self.pickerStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerStatus.delegate = self
            self.pickerStatus.dataSource = self
            self.pickerStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerStatus
        }
            
        else if(textField == self.txtPrivate){
            self.pickerPrivate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPrivate.delegate = self
            self.pickerPrivate.dataSource = self
            self.pickerPrivate.backgroundColor = UIColor.white
            textField.inputView = self.pickerPrivate
        }
        else if(textField == self.txtqueue){
            self.pickerQueue = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerQueue.delegate = self
            self.pickerQueue.dataSource = self
            self.pickerQueue.backgroundColor = UIColor.white
            textField.inputView = self.pickerQueue
        }
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtRecordTyp.resignFirstResponder()
        txtStatus.resignFirstResponder()
        txtPrivate.resignFirstResponder()
        self.txtqueue.resignFirstResponder()
    }
}


extension editBinViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return acctList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAccount.dequeueReusableCell(withIdentifier: "tblEditAcctCell") as! tblEditAcctCell
        let dict = acctList[indexPath.row] as! NSDictionary
        cell.btnDelSelect.isSelected = false
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.btnDelSelect.isSelected = false
            }
            else{
                cell.btnDelSelect.isSelected = true
            }
        }
        
        if let account_no = dict["account_no"] as? String {
            cell.lblTyp.text = "Account(\(account_no))"
        }
        
        let first_name = dict["first_name"] as! String
        let last_name = dict["last_name"] as! String
        cell.lblDetail.text = "\(first_name) \(last_name)"
        
        cell.btnViewRecord.tag = indexPath.row
        cell.btnViewRecord.addTarget(self, action: #selector(clickONViewRecord(_:)), for: .touchUpInside)
        
        cell.btnDelSelect.tag = indexPath.row
        cell.btnDelSelect.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnDel(_ sender: UIButton){
        
        let dict = acctList[sender.tag] as! NSMutableDictionary
        let _id = dict["id"] as! Int
        let acc_id = dict["acc_id"] as! Int
        
        let index = dict["index"] as! Int
        print(dict)
        
        if(self.arrSelectId.contains(_id))
        {
            sender.isSelected = false
            self.arrSelectId.remove(_id)
            self.arrSelectAccId.remove(acc_id)
            self.arrSelectIndex.remove(index)
            dict.setValue(false, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
        else{
            sender.isSelected = true
            self.arrSelectId.add(_id)
            self.arrSelectAccId.add(acc_id)
            self.arrSelectIndex.add(index)
            dict.setValue(true, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
    }
    
    @objc func clickONViewRecord(_ sender: UIButton)
    {
        let dict = acctList[sender.tag] as! NSDictionary
        let _id = dict["id"] as! Int
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let detail = story.instantiateViewController(identifier: "accountDetailPageController") as! accountDetailPageController
        detail.accId = _id
        detail.isDetailOpenfrom = "bin"
        detail.modalPresentationStyle = .fullScreen
        self.present(detail, animated: true, completion: nil)
    }
}

class tblEditAcctCell : UITableViewCell
{
    @IBOutlet weak var lblTyp : UILabel!
    @IBOutlet weak var lblDetail : UILabel!
    @IBOutlet weak var btnDelSelect : UIButton!
    @IBOutlet weak var btnViewRecord : UIButton!
}
