//
//  detailTaskVC.swift
//  LMS
//
//  Created by Apple on 17/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class detailTaskVC: UIViewController {
    
    var interval = ""
    var recTyp = 0
    var timeZone = 0
    var status = false
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    
    @IBOutlet weak var txtDescription : UITextField!
    @IBOutlet weak var txtRecordTyp : UITextField!
    @IBOutlet weak var txtStatus : UITextField!
    
    @IBOutlet weak var txtEveryMin : UITextField!
    @IBOutlet weak var txtEveryHr : UITextField!
    
    @IBOutlet weak var txtTimeZone : UITextField!
    @IBOutlet weak var txtExecutionDate : UITextField!
    @IBOutlet weak var txtStartTime : UITextField!
    @IBOutlet weak var txtMonthDate : UITextField!
    
    @IBOutlet weak var btn1 : UIButton!
    @IBOutlet weak var btn2 : UIButton!
    @IBOutlet weak var btn3 : UIButton!
    @IBOutlet weak var btn4 : UIButton!
    @IBOutlet weak var btn5 : UIButton!
    @IBOutlet weak var btn6 : UIButton!
    @IBOutlet weak var btn7 : UIButton!
    @IBOutlet weak var btn8 : UIButton!
    @IBOutlet weak var btn9 : UIButton!
    
    @IBOutlet weak var btnRecordDisable: UIButton!
    @IBOutlet weak var btnStatusDisable: UIButton!
    
    @IBOutlet weak var viewDate : UIView!
    @IBOutlet weak var viewTime : UIView!
    
    @IBOutlet weak var viewTrash : UIView!
    
    @IBOutlet weak var hgtView : NSLayoutConstraint!
    
    var arrRecirdTyp : [NSDictionary] = []
    var pickerRecordTyp: UIPickerView!
    
    var arrTimeZone : [NSDictionary] = []
    var pickerTimeZone: UIPickerView!
    
    var pickerNextDate : UIDatePicker!
    var pickerstartTime : UIDatePicker!
    
    var pickerStatus: UIPickerView!
    var arrStatus : [[String : Any]] = []

    
    var arrUrl = [Constant.record_type,Constant.timezone_url]
    var isSelectDateTime = ""
    
    var pickerMonth: UIPickerView!
    
    var isdetailFrom = ""
    var passDict : NSDictionary = [:]
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
       self.viewTrash.isHidden = true
        
        Globalfunc.showLoaderView(view: self.view)
        self.callDataApi()

        
    }
    
    func setUpUI(){
        
        if(isdetailFrom == "edit"){
            
            self.viewTrash.isHidden = false
            
            print(self.passDict)
            self.btnRecordDisable.isHidden = false
            self.btnStatusDisable.isHidden = true
            
            
            let _id = self.passDict["_id"] as! Int
            self.lblHeadertitle.text = "Edit Task \(_id)"
            
            if let status = self.passDict["status"] as? Bool {
                for i in 0..<self.arrStatus.count{
                    let dict = self.arrStatus[i]
                    let value = dict["value"] as! Bool
                    if(value == status){
                        let strTitle = dict["title"] as! String
                        self.txtStatus.text = strTitle
                        let value = dict["value"] as! Bool
                        self.status = value
                        break
                    }
                }
            }

            let ddlAMPM = self.passDict["ddlAMPM"] as! String
            
            var ddHr = ""
            if let ddlHour = self.passDict["ddlHour"] as? Int{
                ddHr = "\(ddlHour)"
            }
            
            if let ddlHour = self.passDict["ddlHour"] as? String{
                ddHr = ddlHour
            }

            var ddMin = ""
            if let ddlMinute = self.passDict["ddlMinute"] as? Int{
                ddMin = "\(ddlMinute)"
            }
            
            if let ddlMinute = self.passDict["ddlMinute"] as? String{
                ddMin = ddlMinute
            }

            self.txtStartTime.text = "\(ddHr):\(ddMin) \(ddlAMPM)"

            let binName = self.passDict["taskName"] as! String
            self.txtDescription.text = "\(binName)"
            
            if let ddlProcessDay = self.passDict["ddlProcessDay"] as? Int{
                self.txtMonthDate.text = "\(ddlProcessDay)"
            }

            if let executionDate = self.passDict["executionDate"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: executionDate)
                self.txtExecutionDate.text = formatter.string(from: date!)
            }

            
            if let ddlTimeZone = self.passDict["ddlTimeZone"] as? Int {
                for i in 0..<self.arrTimeZone.count{
                    let dict = self.arrTimeZone[i]
                    let _id = dict["_id"] as! Int
                    if(_id == ddlTimeZone){
                        let strTitle = dict["title"] as! String
                        self.txtTimeZone.text = strTitle
                        let _id = dict["_id"] as! Int
                        self.timeZone = _id
                        break
                    }
                }
            }
            
            if let recordType = self.passDict["recordType"] as? Int {
                for i in 0..<self.arrRecirdTyp.count{
                    let dict = self.arrRecirdTyp[i]
                    let _id = dict["_id"] as! Int
                    if(_id == recordType){
                        let strTitle = dict["title"] as! String
                        self.txtRecordTyp.text = strTitle
                        let _id = dict["_id"] as! Int
                        self.recTyp = _id
                        break
                    }
                }
            }
             
            let intervl = self.passDict["interval"] as! String
            self.interval = intervl
            if(self.interval == "One Time Run") {
                self.btn1.isSelected = true
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = false
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 62

            }
            
            else if(self.interval == "Every \(txtEveryMin.text!) Minutes"){
                
                self.btn1.isSelected = false
                self.btn2.isSelected = true
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = false
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 62
                
                
            }
            else if(self.interval == "Every \(txtEveryHr.text!) Hours"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = true
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = false
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 62
                
                
            }
            else if(self.interval == "Daily"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = true
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = false
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 62
                
                
            }
            else if(self.interval == "Weekly"){
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = true
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = false
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 62
                
                
            }
            else if(self.interval == "Monthly"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = true
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = true
                self.viewTime.isHidden = false
                
                self.hgtView.constant = 62
                
            }
            else if(self.interval == "End Of Month"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = true
                self.btn8.isSelected = false
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = true
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 0
                
                
            }
            else if(self.interval == "1st Day Of Month"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = true
                self.btn9.isSelected = false
                
                self.viewDate.isHidden = true
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 0
                
                
            }
            else if(self.interval == "Last Day Of Month"){
                
                
                self.btn1.isSelected = false
                self.btn2.isSelected = false
                self.btn3.isSelected = false
                self.btn4.isSelected = false
                self.btn5.isSelected = false
                self.btn6.isSelected = false
                self.btn7.isSelected = false
                self.btn8.isSelected = false
                self.btn9.isSelected = true
                
                self.viewDate.isHidden = true
                self.viewTime.isHidden = true
                
                self.hgtView.constant = 0
                
            }

        }
        else{
            
            self.viewTrash.isHidden = true
            self.lblHeadertitle.text = "New Automated Task"
            
            self.btnRecordDisable.isHidden = true
            self.btnStatusDisable.isHidden = false
            
            self.txtStatus.text = "Active"
            self.btn1.isSelected = true
            self.interval = "One Time Run"
        }
        
        self.viewDate.isHidden = false
        self.viewTime.isHidden = true
        self.hgtView.constant = 62
        

    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        //self.dismiss(animated: true, completion: nil)
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        if(txtDescription.text == "" || txtDescription.text?.count == 0 || txtDescription.text == nil){
            txtDescription.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Description should not left blank.")
        }
        else if(txtRecordTyp.text == "" || txtRecordTyp.text?.count == 0 || txtRecordTyp.text == nil){
            txtRecordTyp.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Record Type.")
        }
        else if(txtStartTime.text == "" || txtStartTime.text?.count == 0 || txtStartTime.text == nil){
            txtStartTime.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Start Time.")
        }

        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            if(isdetailFrom == "edit"){
                
                var followHr = ""
                var followMin = ""
                var followFormat = ""
                
                let result = self.txtStartTime.text!.split(separator: ":") as NSArray
                
                followHr = result.object(at: 0) as! String
                let follow = result.object(at: 1) as! String

                let newReslt = follow.split(separator: " ") as NSArray
                followMin = newReslt.object(at: 0) as! String
                followFormat = newReslt.object(at: 1) as! String
                
                let _id = self.passDict["_id"] as! Int
                
                let params = [ "taskName": self.txtDescription.text!,
                               "insid": (user?.institute_id!)!,
                               "ddlAMPM": followFormat,
                               "ddlHour": followHr,
                               "ddlMinute": followMin,
                               "ddlProcessDay": self.txtMonthDate.text!,
                               "ddlTimeZone": self.timeZone,
                               "executionDate": self.txtExecutionDate.text!,
                               "id": _id,
                               "interval":self.interval,
                               "recordType": self.recTyp,
                               "inactive": false,
                               "status": self.status,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "put")


            }
            else{
                var followHr = ""
                var followMin = ""
                var followFormat = ""
                
                let result = self.txtStartTime.text!.split(separator: ":") as NSArray
                
                followHr = result.object(at: 0) as! String
                let follow = result.object(at: 1) as! String

                let newReslt = follow.split(separator: " ") as NSArray
                followMin = newReslt.object(at: 0) as! String
                followFormat = newReslt.object(at: 1) as! String
                
                let params = [ "taskName": self.txtDescription.text!,
                               "insid": (user?.institute_id!)!,
                               "ddlAMPM": followFormat,
                               "ddlHour": followHr,
                               "ddlMinute": followMin,
                               "ddlProcessDay": self.txtMonthDate.text!,
                               "ddlTimeZone": self.timeZone,
                               "executionDate": self.txtExecutionDate.text!,
                               "interval":self.interval,
                               "recordType": self.recTyp,
                               "inactive": false,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "post")

            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
}



extension detailTaskVC {
    
    func callDataApi()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if(obj == Constant.timezone_url){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrTimeZone.append(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        if(obj == Constant.record_type){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrRecirdTyp.append(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            
                            let dict_1 = ["title":"Active","value":true] as [String : Any]
                            let dict_2 = ["title":"Delete","value":false] as [String : Any]
                            self.arrStatus.append(dict_1)
                            self.arrStatus.append(dict_2)

                            
                            self.setUpUI()
                        })
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
    
    
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.task, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.task, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }

    }
}

extension detailTaskVC {
    
    @IBAction func clickOnRadioBtn(_ sender: UIButton)
    {
        if(sender.tag == 1){
            self.interval = "One Time Run"
            self.btn1.isSelected = true
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = false
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 62
            
        }
        else if(sender.tag == 2){
            self.interval = "Every \(txtEveryMin.text!) Minutes"
            self.btn1.isSelected = false
            self.btn2.isSelected = true
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = false
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 62
            
            
        }
        else if(sender.tag == 3){
            self.interval = "Every \(txtEveryHr.text!) Hours"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = true
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = false
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 62
            
            
        }
        else if(sender.tag == 4){
            self.interval = "Daily"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = true
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = false
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 62
            
            
        }
        else if(sender.tag == 5){
            self.interval = "Weekly"
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = true
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = false
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 62
            
            
        }
        else if(sender.tag == 6){
            self.interval = "Monthly"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = true
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = true
            self.viewTime.isHidden = false
            
            self.hgtView.constant = 62
            
        }
        else if(sender.tag == 7){
            self.interval = "End Of Month"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = true
            self.btn8.isSelected = false
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = true
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 0
            
            
        }
        else if(sender.tag == 8){
            self.interval = "1st Day Of Month"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = true
            self.btn9.isSelected = false
            
            self.viewDate.isHidden = true
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 0
            
            
        }
        else if(sender.tag == 9){
            self.interval = "Last Day Of Month"
            
            self.btn1.isSelected = false
            self.btn2.isSelected = false
            self.btn3.isSelected = false
            self.btn4.isSelected = false
            self.btn5.isSelected = false
            self.btn6.isSelected = false
            self.btn7.isSelected = false
            self.btn8.isSelected = false
            self.btn9.isSelected = true
            
            self.viewDate.isHidden = true
            self.viewTime.isHidden = true
            
            self.hgtView.constant = 0
            
            
        }
        
        
        
        
    }
    
}

extension detailTaskVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerRecordTyp{
            return self.arrRecirdTyp.count
        }
        else if pickerView == pickerTimeZone{
            return self.arrTimeZone.count
        }
        else if(pickerView == pickerMonth){
            return consArrays.arrMonthDates.count
        }
        else if pickerView == pickerStatus{
            return self.arrStatus.count
        }

        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView == pickerRecordTyp{
            let dict = arrRecirdTyp[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerTimeZone{
            let dict = arrTimeZone[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerMonth){
            let strTitle = consArrays.arrMonthDates[row]
            return strTitle
        }
        else if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }

        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerRecordTyp{
            let dict = arrRecirdTyp[row]
            let strTitle = dict["title"] as! String
            self.txtRecordTyp.text = strTitle
            let _id = dict["_id"] as! Int
            self.recTyp = _id
        }
        else if pickerView == pickerTimeZone{
            let dict = arrTimeZone[row]
            let strTitle = dict["title"] as! String
            self.txtTimeZone.text = strTitle
            let _id = dict["_id"] as! Int
            self.timeZone = _id
        }
        else if(pickerView == pickerMonth){
            let strTitle = consArrays.arrMonthDates[row]
            self.txtMonthDate.text = strTitle
        }
        else if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            self.txtStatus.text = strTitle
            let value = dict["value"] as! Bool
            self.status = value
        }


        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtRecordTyp){
            self.pickUp(txtRecordTyp)
        }
        else if(textField == self.txtTimeZone){
            self.pickUp(txtTimeZone)
        }
        else if(textField == self.txtExecutionDate){
            self.pickUpDateTime(txtExecutionDate)
        }
        else if(textField == self.txtStartTime){
            self.pickUpDateTime(txtStartTime)
        }
        else if(textField == self.txtMonthDate){
            self.pickUp(txtMonthDate)
        }
        else if(textField == self.txtStatus){
            self.pickUp(txtStatus)
        }

    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtExecutionDate){
            isSelectDateTime = "date"
            self.pickerNextDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerNextDate.datePickerMode = .date
            self.pickerNextDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerNextDate
        }
        
        else if(textField == self.txtStartTime){
            isSelectDateTime = "time"
            self.pickerstartTime = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerstartTime.datePickerMode = .time
            self.pickerstartTime.backgroundColor = UIColor.white
            textField.inputView = self.pickerstartTime
        }

        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        if(isSelectDateTime == "date"){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtExecutionDate.text = formatter.string(from: pickerNextDate.date)
            self.txtExecutionDate.resignFirstResponder()
        }
        else{
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            self.txtStartTime.text = formatter.string(from: pickerstartTime.date)
            self.txtStartTime.resignFirstResponder()
        }
        
    }
    
    @objc func cancelClickDate() {
        txtExecutionDate.resignFirstResponder()
        txtStartTime.resignFirstResponder()
    }

    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtRecordTyp){
            self.pickerRecordTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRecordTyp.delegate = self
            self.pickerRecordTyp.dataSource = self
            self.pickerRecordTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerRecordTyp
        }
        else if(textField == self.txtTimeZone){
            self.pickerTimeZone = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTimeZone.delegate = self
            self.pickerTimeZone.dataSource = self
            self.pickerTimeZone.backgroundColor = UIColor.white
            textField.inputView = self.pickerTimeZone
        }
        
        else if(textField == self.txtMonthDate){
            self.pickerMonth = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerMonth.delegate = self
            self.pickerMonth.dataSource = self
            self.pickerMonth.backgroundColor = UIColor.white
            textField.inputView = self.pickerMonth
        }
        else if(textField == self.txtStatus){
            self.pickerStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerStatus.delegate = self
            self.pickerStatus.dataSource = self
            self.pickerStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerStatus
        }


        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtRecordTyp.resignFirstResponder()
        txtTimeZone.resignFirstResponder()
        txtMonthDate.resignFirstResponder()
        txtStatus.resignFirstResponder()
    }
    
}
