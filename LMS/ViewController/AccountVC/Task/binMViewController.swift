//
//  binMViewController.swift
//  LMS
//
//  Created by Apple on 08/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class binMViewController: baseViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var tblBin : UITableView!
    
    @IBOutlet weak var txtFilter : UITextField!
    var pickerviewFilter : UIPickerView!
    
    var pickerFilterData : NSMutableArray = []
    
    var arrSelectId = NSMutableArray()
    var is_status_filter = ""
    var isSelectedPicker = ""
    
    var arrBinList : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        self.tblBin.tableFooterView = UIView()
        
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        
        let dict_1 = ["name":"Active","value":"false"]
        let dict_2 = ["name":"Deleted","value":"true"]
        let dict_3 = ["name":"All","value":"all"]
        
        
        pickerFilterData.add(dict_1)
        pickerFilterData.add(dict_2)
        pickerFilterData.add(dict_3)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrBinList = []
        self.tblBin.isHidden = true
        self.lblStatus.isHidden = true
        
        is_status_filter = "false"
        
        self.txtFilter.text = "Active"
        
        
        
        
        Globalfunc.showLoaderView(view: self.view)
        let url = "\(Constant.bin)/status?status=\(is_status_filter)"
        self.callUserListApiwithstatus(strUrl: url)
        
    }
    
    
    
    func callUserListApiwithstatus(strUrl: String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary]
                        {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrBinList.add(dictA)
                                }
                                self.tblBin.isHidden = false
                                self.lblStatus.isHidden = true
                                self.tblBin.reloadData()
                            }
                            else{
                                self.lblStatus.isHidden = false
                                self.tblBin.isHidden = true
                                self.arrBinList = []
                            }
                        }
                        
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension binMViewController {
    
    @IBAction func clikOnPlusBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("addBinViewController", strStoryName: "taskingStory")
    }
    
    @IBAction func clickONDelIdBtn(_ sender: UIButton)
    {
        if(self.arrSelectId.count > 0){
            Globalfunc.showLoaderView(view: self.view)
            let param = ["id":self.arrSelectId] as [String: Any]
            
            BaseApi.onResponseDeleteWithTP(url: Constant.bin, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        else{
            Globalfunc.showToastWithMsg(view: self.view, str: "Select item to delete.")
        }
    }
    
}

extension binMViewController :  UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewFilter){
            return pickerFilterData.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            self.txtFilter.text = (dict["name"] as! String)
            is_status_filter = dict["value"] as! String
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtFilter){
            self.pickUp(self.txtFilter)
        }
        
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtFilter){
            self.pickerviewFilter = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewFilter.delegate = self
            self.pickerviewFilter.dataSource = self
            self.pickerviewFilter.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewFilter
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        self.arrBinList = []
        Globalfunc.showLoaderView(view: self.view)
        txtFilter.resignFirstResponder()
        let url = "\(Constant.bin)/status?status=\(is_status_filter)"
        self.callUserListApiwithstatus(strUrl: url)
    }
    
    @objc func cancelClick(){
        txtFilter.resignFirstResponder()
    }
}

extension binMViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBinList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblBin.dequeueReusableCell(withIdentifier: "tblBinCell") as! tblBinCell
        let dict = arrBinList[indexPath.row] as! NSDictionary
        
        print(dict)
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.btnDelSelect.isSelected = false
            }
            else{
                cell.btnDelSelect.isSelected = true
            }
        }
        
        if let status = dict["status"] as? Bool{
            if(status == false){
                cell.lblstatus.text = "Delete"
                cell.btnDelSelect.isUserInteractionEnabled = false
            }
            else{
                cell.lblstatus.text = "Active"
                cell.btnDelSelect.isUserInteractionEnabled = true
            }
        }
        
        if let binName = dict["binName"] as? String{
            cell.lblBin.text = binName
        }
        
        
        if let recordType = dict["recordType"] as? NSDictionary{
            if let title = recordType["title"] as? String{
                cell.lblRecordTyp.text = title
            }
        }
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clickONBtnEdit(_:)), for: .touchUpInside)
        
        cell.btnDelSelect.tag = indexPath.row
        cell.btnDelSelect.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnDel(_ sender: UIButton){
        
        let dict = arrBinList[sender.tag] as! NSMutableDictionary
        let _id = dict["_id"] as! Int
        
        if(self.arrSelectId.contains(_id))
        {
            sender.isSelected = false
            self.arrSelectId.remove(_id)
            dict.setValue(false, forKey: "is_deleted")
            self.arrBinList.replaceObject(at: sender.tag, with: dict)
            self.tblBin.reloadData()
        }
        else{
            sender.isSelected = true
            self.arrSelectId.add(_id)
            dict.setValue(true, forKey: "is_deleted")
            self.arrBinList.replaceObject(at: sender.tag, with: dict)
            self.tblBin.reloadData()
        }
    }
    
    @objc func clickONBtnEdit(_ sender: UIButton)
    {
        let dict = arrBinList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! Int
        let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
        let edit = story.instantiateViewController(withIdentifier: "editBinViewController") as! editBinViewController
        edit.passBinId = _id
        edit.modalPresentationStyle = .fullScreen
        self.present(edit, animated: true, completion: nil)
        
    }
}

class tblBinCell : UITableViewCell
{
    @IBOutlet weak var lblBin : UILabel!
    @IBOutlet weak var lblRecordTyp : UILabel!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var btnDelSelect : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
}




