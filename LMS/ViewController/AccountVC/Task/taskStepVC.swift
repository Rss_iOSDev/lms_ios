//
//  taskStepVC.swift
//  LMS
//
//  Created by Apple on 30/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

var priority = 0

class taskStepVC: UIViewController {
    
    @IBOutlet weak var tblAccount: UITableView!
    @IBOutlet weak var txtSelectStep : UITextField!
    
    @IBOutlet weak var btnAllCehckBox : UIButton!
    
    var acctList = NSMutableArray()
    var passId : Int?
    var arrSelectId = NSMutableArray()
    
    var pickerSteps: UIPickerView!
    var step_id = ""
    var arrSteps = NSMutableArray()
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblAccount.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        acctList = []
        self.arrSteps = []
        self.btnAllCehckBox.isSelected = false
        self.txtSelectStep.text = ""
        if let id = userDef.value(forKey: "task_id") as? Int{
            self.lblHeadertitle.text = "Edit Task \(id)"
            let url_step = "\(Constant.task_step)/task?id=\(id)"
            self.call_getApiwithParam(strUrl: url_step, strTyp: "step_list")
        }
        let url = "\(Constant.step)/record?id=1"
        self.call_getApiwithParam(strUrl: url, strTyp: "steps")
    }
}

extension taskStepVC {
    
    @IBAction func clickONCheckBoxes(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if sender.isSelected == true {
                sender.isSelected = false
                
                for i in 0..<self.acctList.count{
                    let dd = self.acctList[i] as! NSMutableDictionary
                    dd.setValue(false, forKey: "is_deleted")
                    self.acctList.replaceObject(at: i, with: dd)
                }
                self.arrSelectId.removeAllObjects()
                self.tblAccount.reloadData()
                
            }else {
                sender.isSelected = true
                self.arrSelectId = []
                
                if(self.acctList.count > 0){
                    for i in 0..<self.acctList.count{
                        let dd = self.acctList[i] as! NSMutableDictionary
                        let _id = dd["_id"] as! Int
                        self.arrSelectId.add(_id)
                        dd.setValue(true, forKey: "is_deleted")
                        self.acctList.replaceObject(at: i, with: dd)
                    }
                }
                self.tblAccount.reloadData()
            }
        }
    }
}

extension taskStepVC {
    func call_getApiwithParam(strUrl: String, strTyp: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                if(strTyp == "steps"){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let data = dict as? NSDictionary {
                            
                            if let arrRes = data["data"] as? [NSDictionary]
                            {
                                if(arrRes.count > 0){
                                    for i in 0..<arrRes.count{
                                        let dd = arrRes[i]
                                        let id = dd["_id"] as! Int
                                        if(id == 1 || id == 8 || id == 9){
                                            //skip step
                                        }
                                        else{
                                            self.arrSteps.add(dd)
                                        }
                                    }
                                    
                                }
                                else{
                                    
                                }
                            }
                        }
                        
                    }
                }
                else if(strTyp == "step_list"){
                    
                    OperationQueue.main.addOperation {
                        if let data = dict as? NSDictionary {
                            if let arrRes = data["data"] as? [NSDictionary]
                            {
                                if(arrRes.count > 0){
                                    for i in 0..<arrRes.count{
                                        let dd = arrRes[i]
                                        self.acctList.add(dd)
                                    }
                                    
                                    let arrCount = self.acctList.count
                                    priority = arrCount + 1
                                    
                                    self.tblAccount.isHidden = false
                                    self.tblAccount.reloadData()
                                }
                                else{
                                    priority = 1
                                    self.tblAccount.isHidden = true
                                    self.acctList = []
                                }
                            }
                        }
                    }
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.bin, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        
    }
}

extension taskStepVC {
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        self.delSteps()
    }
    
    func delSteps(){
        
        if(self.arrSelectId.count > 0){
            let param = ["id":self.arrSelectId] as [String: Any]
            BaseApi.onResponseDeleteWithTP(url: Constant.task_step, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        else{
            Globalfunc.showToastWithMsg(view: self.view, str: "Select item to delete.")
        }
        
    }
    
}



extension taskStepVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return acctList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAccount.dequeueReusableCell(withIdentifier: "tblStepLitsCell") as! tblStepLitsCell
        let dict = acctList[indexPath.row] as! NSDictionary
        cell.btnDelSelect.isSelected = false
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.btnDelSelect.isSelected = false
                cell.lblStatus.text = "Active"
            }
            else{
                cell.btnDelSelect.isSelected = true
                cell.lblStatus.text = "Deleted"
            }
        }
        
        if let description = dict["description"] as? String {
            cell.lblDesription.text = "\(description)"
        }
        
        if let step = dict["step"] as? NSDictionary {
            
            let title = step["title"] as! String
            cell.lblStepTyp.text = "\(title)"
        }
        
        cell.btnViewRecord.tag = indexPath.row
        cell.btnViewRecord.addTarget(self, action: #selector(clickONViewRecord(_:)), for: .touchUpInside)
        
        cell.btnDelSelect.tag = indexPath.row
        cell.btnDelSelect.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnDel(_ sender: UIButton){
        
        let dict = acctList[sender.tag] as! NSMutableDictionary
        print(dict)
        let _id = dict["_id"] as! Int
        
        if(self.arrSelectId.contains(_id))
        {
            sender.isSelected = false
            self.arrSelectId.remove(_id)
            dict.setValue(false, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
        else{
            sender.isSelected = true
            self.arrSelectId.add(_id)
            dict.setValue(true, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
    }
    
    @objc func clickONViewRecord(_ sender: UIButton)
    {
        let dict = acctList[sender.tag] as! NSDictionary
        
        let step = dict["step"] as! NSDictionary
        let s_id = step["_id"] as! Int
        
        if(s_id == 2){
            let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
            let edit = story.instantiateViewController(withIdentifier: "stepClearTaskVC") as! stepClearTaskVC
            edit.pssBinId = self.passId!
            edit.step_id = s_id
            edit.pasDictstep = dict
            edit.isEditForm = "true"
            edit.modalPresentationStyle = .fullScreen
            self.present(edit, animated: true, completion: nil)
        }
        else if(s_id == 3){
            let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
            let edit = story.instantiateViewController(withIdentifier: "stepsLoadTaskVC") as! stepsLoadTaskVC
            edit.pssBinId = self.passId!
            edit.step_id = s_id
            edit.pasDictstep = dict
            edit.isEditForm = "true"
            edit.modalPresentationStyle = .fullScreen
            self.present(edit, animated: true, completion: nil)
        }
        else if(s_id == 4){
            let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
            let edit = story.instantiateViewController(withIdentifier: "stepsSendEmailVC") as! stepsSendEmailVC
            edit.pssBinId = self.passId!
            edit.step_id = s_id
            edit.pasDictstep = dict
            edit.isEditForm = "true"
            edit.modalPresentationStyle = .fullScreen
            self.present(edit, animated: true, completion: nil)
        }
        else if(s_id == 5){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepsSendMsgVC") as! stepsSendMsgVC
                edit.pssBinId = self.passId!
                edit.pasDictstep = dict
                edit.step_id = s_id
                edit.isEditForm = "true"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
        }
        else if(s_id == 6){
            let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
            let edit = story.instantiateViewController(withIdentifier: "stepsFlagsVC") as! stepsFlagsVC
            edit.pssBinId = self.passId!
            edit.step_id = s_id
            edit.pasDictstep = dict
            edit.isEditForm = "true"
            edit.isFlagAssgn = "true"
            edit.modalPresentationStyle = .fullScreen
            self.present(edit, animated: true, completion: nil)
        }
        else if(s_id == 7){
            let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
            let edit = story.instantiateViewController(withIdentifier: "stepsFlagsVC") as! stepsFlagsVC
            edit.pssBinId = self.passId!
            edit.pasDictstep = dict
            edit.step_id = s_id
            edit.isEditForm = "true"
            edit.isFlagAssgn = "false"
            edit.modalPresentationStyle = .fullScreen
            self.present(edit, animated: true, completion: nil)
        }
    }
}

extension taskStepVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerSteps){
            return arrSteps.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerSteps){
            let dict = arrSteps[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerSteps){
            let dict = arrSteps[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let id = dict["_id"] as! Int
            self.step_id = "\(id)"
            self.txtSelectStep.text = strTitle
            
            if(self.step_id == "2"){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepClearTaskVC") as! stepClearTaskVC
                edit.pssBinId = self.passId!
                edit.step_id = id
                edit.isEditForm = "false"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
            }
            else if(self.step_id == "3"){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepsLoadTaskVC") as! stepsLoadTaskVC
                edit.pssBinId = self.passId!
                edit.step_id = id
                edit.isEditForm = "false"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
            }
            else if(self.step_id == "4"){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepsSendEmailVC") as! stepsSendEmailVC
                edit.pssBinId = self.passId!
                edit.step_id = id
                edit.isEditForm = "false"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
            }
            else if(self.step_id == "5"){
                    let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                    let edit = story.instantiateViewController(withIdentifier: "stepsSendMsgVC") as! stepsSendMsgVC
                    edit.pssBinId = self.passId!
                    edit.step_id = id
                    edit.isEditForm = "false"
                    edit.modalPresentationStyle = .fullScreen
                    self.present(edit, animated: true, completion: nil)
            }
            else if(self.step_id == "6"){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepsFlagsVC") as! stepsFlagsVC
                edit.pssBinId = self.passId!
                edit.step_id = id
                edit.isEditForm = "false"
                edit.isFlagAssgn = "true"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
            }
            else if(self.step_id == "7"){
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "stepsFlagsVC") as! stepsFlagsVC
                edit.pssBinId = self.passId!
                edit.step_id = id
                edit.isEditForm = "false"
                edit.isFlagAssgn = "false"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtSelectStep){
            self.pickUp(txtSelectStep)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSelectStep){
            
            self.pickerSteps = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSteps.delegate = self
            self.pickerSteps.dataSource = self
            self.pickerSteps.backgroundColor = UIColor.white
            textField.inputView = self.pickerSteps
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtSelectStep.resignFirstResponder()
    }
}


class tblStepLitsCell : UITableViewCell
{
    @IBOutlet weak var lblDesription : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblStepTyp : UILabel!
    @IBOutlet weak var btnDelSelect : UIButton!
    @IBOutlet weak var btnViewRecord : UIButton!
}



