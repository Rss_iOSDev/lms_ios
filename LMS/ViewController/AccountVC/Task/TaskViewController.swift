//
//  TaskViewController.swift
//  LMS
//
//  Created by Apple on 14/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController {
    
    
    @IBOutlet weak var tblManage : UITableView!
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    @IBOutlet weak var lblAssociatedBin: UILabel!
    
    var arrList = [String]()
    
    var passDictDetail : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblManage.tableFooterView = UIView()
        if(isOpenpageFrom == "new"){
            arrList = ["Detail"]
            self.lblTitle.text = "NEW JOB"
            self.lblHeadertitle.text = "New Automated Task"
        }
        else if(isOpenpageFrom == "edit"){
            
            arrList = ["Detail","Steps","Item","Notes"]
            
            Globalfunc.showLoaderView(view: self.view)
            
            if let id = userDef.value(forKey: "task_id") as? Int{
                let url = "\(Constant.task)/id?id=\(id)"
                self.call_getApiwithParam(strUrl: url)
            }
            
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension TaskViewController {
    
    func call_getApiwithParam(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let data = dict as? NSDictionary {
                        
                        print(data)
                        
                        if let response = data["data"] as? NSDictionary
                        {
                            let _id = response["_id"] as! Int
                            self.lblHeadertitle.text = "Edit Task \(_id)"
                            
                            
                            self.passDictDetail = response
                            if(isOpenpageFrom == "new"){
                                self.lblStatus.text = "Status: New"
                                self.lblOwner.text = "Owner:"
                                self.lblAssociatedBin.text = "Associated bin:"
                            }
                            else if(isOpenpageFrom == "edit"){
                                
                                if let taskBinName = response["taskBinName"] as? NSDictionary{
                                    let binName = taskBinName["binName"] as! String
                                    self.lblAssociatedBin.text = "Associated bin: \(binName)"
                                    self.lblTitle.text = "\(binName)"
                                }
                                
                                
                                if let userid = response["userid"] as? NSDictionary{
                                    let fname = userid["fname"] as! String
                                    let lname = userid["lname"] as! String
                                    self.lblOwner.text = "Owner: \(fname) \(lname)"
                                }
                                
                                let status = response["status"] as! Bool
                                if(status == true){
                                    self.lblStatus.text = "Status: Active"
                                }
                                else{
                                    self.lblStatus.text = "Status: Delete"
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension TaskViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblManage.dequeueReusableCell(withIdentifier: "tblManage") as! tblManage
        cell.lblTitle.text = self.arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isOpenpageFrom == "new"){
            switch indexPath.row {
            case 0:
                
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "detailTaskVC") as! detailTaskVC
                edit.isdetailFrom = "new"
                edit.modalPresentationStyle = .fullScreen
                self.present(edit, animated: true, completion: nil)
                
                
            default:
                Globalfunc.print(object:"default")
            }
        }
        else if(isOpenpageFrom == "edit"){
            
            switch indexPath.row {
            case 0:
                
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "detailTaskVC") as! detailTaskVC
                edit.modalPresentationStyle = .fullScreen
                edit.isdetailFrom = "edit"
                edit.passDict = self.passDictDetail
                self.present(edit, animated: true, completion: nil)
                
            case 1:
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "taskStepVC") as! taskStepVC
                edit.modalPresentationStyle = .fullScreen
                let taskBinName = self.passDictDetail["taskBinName"] as! NSDictionary
                let _id = taskBinName["_id"] as! Int
                edit.passId = _id
                self.present(edit, animated: true, completion: nil)
                
            case 2:
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "taskItemVC") as! taskItemVC
                edit.modalPresentationStyle = .fullScreen
                let taskBinName = self.passDictDetail["taskBinName"] as! NSDictionary
                let _id = taskBinName["_id"] as! Int
                edit.passId = _id
                self.present(edit, animated: true, completion: nil)
            case 3:
                let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
                let edit = story.instantiateViewController(withIdentifier: "taskNotesVC") as! taskNotesVC
                edit.modalPresentationStyle = .fullScreen
                edit.passDict = self.passDictDetail
                self.present(edit, animated: true, completion: nil)
            default:
                Globalfunc.print(object:"default")
            }
        }
    }
}

extension TaskViewController {
    
    @IBAction func clickOnBinDelBtn(_ sender : UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let _id = self.passDictDetail["_id"] as! Int
        let url = "\(Constant.task)?id=\(_id)"
            BaseApi.onResponseDeleteWithToken(url: url) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
    }
}
