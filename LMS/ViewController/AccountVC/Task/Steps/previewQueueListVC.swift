//
//  previewQueueListVC.swift
//  LMS
//
//  Created by Apple on 02/01/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class previewQueueListVC: UIViewController {
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    @IBOutlet weak var lblRecord : UILabel!
    var mainArrListData : [NSMutableArray] = []
    var queueid = 0
    
    @IBOutlet weak var gridCollectionView: UICollectionView! {
        didSet {
            gridCollectionView.bounces = false
        }
    }
    
    @IBOutlet weak var gridLayout: StickyGridCollectionViewLayout! {
        didSet {
            gridLayout.stickyColumnsCount = 1
        }
    }
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let id = userDef.value(forKey: "task_id") as? Int{
            self.lblHeadertitle.text = "Edit Task \(id)"
        }
        
        Globalfunc.showLoaderView(view: self.view)
        let url = "\(Constant.queue)/preview-queue?id=\(self.queueid)"
        self.callAccountListApi(strUrl: url)
        
    }
    
    @IBAction func clickONBtnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}



extension previewQueueListVC {
    
    
    func callAccountListApi(strUrl: String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    self.parseResponseAndSetData(dict: dict)
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

// MARK: - Collection view data source and delegate methods

extension previewQueueListVC: UICollectionViewDataSource , UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.mainArrListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let arr = self.mainArrListData[section]
        return arr.count //column
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collAccountCell", for: indexPath) as? collAccountCell else {
            return UICollectionViewCell()
        }
        
        
        if(self.mainArrListData.count > 0){
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary
            {
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    cell.lblLine.isHidden = true
                    cell.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                    cell.titleLabel.text = "\(arrInside["column"] as! String)"
                    cell.titleLabel.textColor = .white
                }
                else{
                    cell.lblLine.isHidden = false
                    cell.titleLabel.textColor = .black
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        cell.backgroundColor = .systemGroupedBackground
                        cell.titleLabel.text = "rrrr"
                    }
                    else{
                        Globalfunc.print(object:arrInside)
                        
                        if let val = arrInside["value"] as? String{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                        
                        if let val = arrInside["value"] as? Int{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                    }
                }
            }
        }
        
        return cell
    }
}

extension previewQueueListVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height = 0.0
        let width = (collectionView.frame.width) / 2
        
        if(self.mainArrListData.count > 0){
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary{
                
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    height = 30.0
                }
                else{
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        height = 10.0
                    }
                    else{
                        height = 50
                    }
                }
            }
        }
        return CGSize(width: Double(width), height: height)
    }
}




//MARK:
extension previewQueueListVC{
    
    func parseResponseAndSetData(dict : Any){
        
        if let dictResponse = dict as? NSDictionary {
            
            if let data = dictResponse["data"] as? NSDictionary{
                
                let defaultRecordsPage = data["defaultRecordsPage"] as! Int
                let totalrecord = data["totalrecord"] as! Int
                self.lblRecord.text = "# Of Preview Records: \(defaultRecordsPage) of \(totalrecord)"
                let arrHeading = data["heading"] as! NSMutableArray
                
                for i in 0..<arrHeading.count{
                    let new_arr : NSMutableArray = []
                    let arr = arrHeading.object(at: i) as! NSMutableArray
                    Globalfunc.print(object:arr)
                    for i in 0..<arr.count{
                        let dict = arr[i] as! NSDictionary
                        let arr = dict["attribute"] as! NSDictionary
                        let name = dict["name"] as? String
                        let columnname = arr["colname"] as? String
                        let dictt = ["column":columnname,"name":name]
                        new_arr.add(dictt)
                        
                    }
                    self.mainArrListData.append(new_arr)
                }
                
                let arrRowCount = data["loan"] as! NSMutableArray
                for i in 0..<arrRowCount.count
                {
                    let arr0 = arrRowCount.object(at: i) as! NSMutableArray
                    for j in 0..<arr0.count{
                        let arr1 = arr0[j] as! NSMutableArray
                        Globalfunc.print(object:arr1)
                        if(j == arr0.count-1){
                            let arrId = arr0[j] as! NSMutableArray
                            let dictId : NSMutableDictionary = [:]
                            let dd = arrId[0] as! NSDictionary
                            let val = dd["id"] as! Int
                            dictId.setValue(val, forKey: "id")
                            for _ in 0..<j-1{
                                let dict = ["id":""]
                                arr1.add(dict)
                            }
                            arr0.replaceObject(at: j, with: arr1)
                            arrRowCount.replaceObject(at: i, with: arr0)
                        }
                    }
                }
                for x in 0..<arrRowCount.count{
                    let arr0 = arrRowCount.object(at: x) as! NSMutableArray
                    Globalfunc.print(object:arr0)
                    let arr1 = arr0[arr0.count - 1] as! NSMutableArray
                    let dictId = arr1[0] as! NSDictionary
                    Globalfunc.print(object:dictId)
                    for y in 0..<arr0.count{
                        let newArr = arr0[y] as! NSMutableArray
                        for z in 0..<newArr.count{
                            let newDict = newArr[z] as! NSDictionary
                            let tempDic: NSMutableDictionary = newDict.mutableCopy() as! NSMutableDictionary
                            tempDic.setValue(dictId, forKey: "id_data")
                            Globalfunc.print(object:tempDic)
                            newArr.replaceObject(at: z, with: tempDic)
                        }
                        self.mainArrListData.append(newArr)
                    }
                }
                Globalfunc.print(object:self.mainArrListData)
                self.gridLayout.stickyRowsCount = arrHeading.count
                self.gridCollectionView.reloadData()
                
            }
            
        }
    }
}
