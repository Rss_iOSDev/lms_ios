//
//  stepsSendEmailVC.swift
//  LMS
//
//  Created by Apple on 29/12/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class stepsSendEmailVC: UIViewController {
    
    
    var step_id : Int?
    var pasDictstep : NSDictionary = [:]
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    var pssBinId : Int?
    
    @IBOutlet weak var btnContinue : UIButton!
    var isCheck = true
    
    @IBOutlet weak var txtStatus : UITextField!
    var pickerviewStatus : UIPickerView!
    var arrStatus : NSMutableArray = []
    var is_deleted = false
    
    var isEditForm = ""
    
    
    var dictDetail : NSDictionary = [:]
    var isTxtField = ""
    
    @IBOutlet weak var txtTemplate : UITextField!
    var pickerViewTemplate: UIPickerView!
    var arrTemplate : NSMutableArray = []
    var tempId = 0
    
    @IBOutlet weak var txtMEssage : UITextView!
    
    @IBOutlet weak var txtStepDescrptn : UITextField!
    @IBOutlet weak var txtSubject : UITextField!
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        print(pasDictstep)
        
        let dict_1 = ["name":"Active","value":false] as [String : Any]
        let dict_2 = ["name":"In-Active","value":true] as [String : Any]
        
        arrStatus.add(dict_1)
        arrStatus.add(dict_2)
        
        // Do any additional setup after loading the view.
        if let id = userDef.value(forKey: "task_id") as? Int{
            self.lblHeadertitle.text = "Edit Task \(id)"
        }
        self.btnContinue.isSelected = true
        
        Globalfunc.showLoaderView(view: self.view)
        
        let url = "\(Constant.getTemplate_url)&templateType=1"
        self.getTemplateList(strUrl: url, strTyp: "template")
        
        if(isEditForm == "true"){
            let _id = self.pasDictstep["_id"] as! Int
            let url_step = "\(Constant.task_step)/id?id=\(_id)"
            self.call_getApiwithParam(strUrl: url_step)
        }
        else if(isEditForm == "false"){
            let dict = self.arrStatus[0] as! NSDictionary
            self.txtStatus.text = (dict["name"] as! String)
            self.is_deleted = false
        }
    }
    
    @IBAction func clickONBtnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONBtnContinue(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            self.isCheck = false
        }else {
            sender.isSelected = true
            self.isCheck = true
        }
    }
}

extension stepsSendEmailVC {
    
    func getTemplateList(strUrl : String, strTyp: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if(strTyp == "template"){
                        if let response = dict as? NSDictionary{
                            if let responseDict = response["data"] as? NSMutableArray {
                                self.arrTemplate = responseDict
                            }
                        }
                    }
                    else if(strTyp == "temp_select"){
                        if let responseDict = dict as? NSDictionary {
                            if let data = responseDict["data"] as? NSDictionary {
                                if let htmlTemplate = data["htmlTemplate"] as? String{
                                    self.txtMEssage.attributedText = htmlTemplate.htmlToAttributedString
                                }
                            }
                            
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension stepsSendEmailVC {
    func call_getApiwithParam(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let response = dict as? NSDictionary {
                        
                        print(response)
                        
                        if let data = response["data"] as? NSDictionary {
                            self.dictDetail = data
                            if let caError = data["caError"] as? Bool {
                                self.isCheck = caError
                                if(caError == true){
                                    self.btnContinue.isSelected = true
                                }
                                else{
                                    self.btnContinue.isSelected = false
                                }
                            }
                            
                            if let description = data["description"] as? String {
                                self.txtStepDescrptn.text = description
                            }
                            
                            if let subject = data["subject"] as? String {
                                self.txtSubject.text = subject
                            }
                            
                            if let is_deleted = data["is_deleted"] as? Bool {
                                for i in 0..<self.arrStatus.count{
                                    let dict = self.arrStatus[i] as! NSDictionary
                                    let val = dict["value"] as! Bool
                                    if(is_deleted == val){
                                        self.txtStatus.text = (dict["name"] as! String)
                                        self.is_deleted = is_deleted
                                        break
                                    }
                                }
                            }
                            
                            if let emailTemplate = data["emailTemplate"] as? Int {
                                Globalfunc.showLoaderView(view: self.view)
                                for i in 0..<self.arrTemplate.count{
                                    let dict = self.arrTemplate[i] as! NSDictionary
                                    let val = dict["_id"] as! Int
                                    if(emailTemplate == val){
                                        self.txtTemplate.text = (dict["templateName"] as! String)
                                        break
                                    }
                                }
                                self.tempId = emailTemplate
                                let url = "\(Constant.getTemplate_layout_url)?id=\(emailTemplate)"
                                self.getTemplateList(strUrl: url, strTyp: "temp_select")
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension stepsSendEmailVC {
    
    @IBAction func clickONSAveBtn(_ sender: UIButton)
    {
        if(txtStatus.text == "" || txtStatus.text?.count == 0 || txtStatus.text == nil){
            txtStatus.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Status.")
        }
        else if(txtStepDescrptn.text == "" || txtStepDescrptn.text?.count == 0 || txtStepDescrptn.text == nil){
            txtStepDescrptn.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Enter Description.")
        }
        else if(txtSubject.text == "" || txtSubject.text?.count == 0 || txtSubject.text == nil){
            txtSubject.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Enter Subject.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            if(isEditForm == "true"){
                let _id = self.dictDetail["_id"] as! Int
                let prior = self.dictDetail["priority"] as! Int
                let step = self.dictDetail["step"] as! Int
                let task_id = userDef.value(forKey: "task_id") as? Int
                let params = [ "caError": self.isCheck,
                               "insid": (user?.institute_id!)!,
                               "description": self.txtStepDescrptn.text!,
                               "id": _id,
                               "emailTemplate": self.tempId,
                               "subject": self.txtSubject.text!,
                               "is_deleted": self.is_deleted,
                               "priority": prior,
                               "step": step,
                               "task": task_id!,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "put")
            }
            else if(isEditForm == "false"){
                let id = userDef.value(forKey: "task_id") as? Int
                let params = [ "caError": self.isCheck,
                               "insid": (user?.institute_id!)!,
                               "description": self.txtStepDescrptn.text!,
                               "emailTemplate": self.tempId,
                               "id": "",
                               "subject": self.txtSubject.text!,
                               "is_deleted": self.is_deleted,
                               "priority": priority,
                               "step": self.step_id!,
                               "task": id!,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "post")
            }
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.task_step, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.task_step, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
}

extension stepsSendEmailVC :  UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewStatus){
            return arrStatus.count
        }
        else if(pickerView == pickerViewTemplate){
            return arrTemplate.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewStatus){
            let dict = self.arrStatus[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        else if pickerView == pickerViewTemplate{
            let dict = arrTemplate[row] as! NSDictionary
            let strTitle = dict["templateName"] as! String
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewStatus){
            let dict = self.arrStatus[row] as! NSDictionary
            self.txtStatus.text = (dict["name"] as! String)
            self.is_deleted = dict["value"] as! Bool
        }
        else if pickerView == pickerViewTemplate{
            let dict = arrTemplate[row] as! NSDictionary
            self.txtTemplate.text = (dict["templateName"] as! String)
            self.txtTemplate.tag = row
            tempId = dict["_id"] as! Int
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtStatus){
            self.pickUp(self.txtStatus)
        }
        else if(textField == self.txtTemplate){
            self.pickUp(txtTemplate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtStatus){
            self.pickerviewStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewStatus.delegate = self
            self.pickerviewStatus.dataSource = self
            self.pickerviewStatus.backgroundColor = UIColor.white
            self.isTxtField = "status"
            textField.inputView = self.pickerviewStatus
        }
            
        else if(textField == self.txtTemplate){
            self.pickerViewTemplate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTemplate.delegate = self
            self.pickerViewTemplate.dataSource = self
            self.pickerViewTemplate.backgroundColor = UIColor.white
            self.isTxtField = "template"
            textField.inputView = self.pickerViewTemplate
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        if(self.isTxtField == "template"){
            txtTemplate.resignFirstResponder()
            let dict = arrTemplate[txtTemplate.tag] as! NSDictionary
            let _id = dict["_id"] as! Int
            let url = "\(Constant.getTemplate_layout_url)?id=\(_id)"
            Globalfunc.showLoaderView(view: self.view)
            self.getTemplateList(strUrl: url, strTyp: "temp_select")
        }
        else{
            txtStatus.resignFirstResponder()
        }
    }
}
