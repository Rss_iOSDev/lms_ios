//
//  stepsLoadTaskVC.swift
//  LMS
//
//  Created by Apple on 30/12/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class stepsLoadTaskVC: UIViewController {
    
    var step_id : Int?
    var pasDictstep : NSDictionary = [:]
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    var pssBinId : Int?
    
    @IBOutlet weak var btnContinue : UIButton!
    var isCheck = true
    
    @IBOutlet weak var txtStatus : UITextField!
    var pickerviewStatus : UIPickerView!
    var arrStatus : NSMutableArray = []
    var is_deleted = false
    
    var isEditForm = ""
    
    var dictDetail : NSDictionary = [:]
    
    @IBOutlet weak var txtQueue : UITextField!
    var arrQueue = NSMutableArray()
    var pickerQueue: UIPickerView!
    var queueId = 0
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        print(pasDictstep)
        
        let dict_1 = ["name":"Active","value":false] as [String : Any]
        let dict_2 = ["name":"In-Active","value":true] as [String : Any]
        
        arrStatus.add(dict_1)
        arrStatus.add(dict_2)
        
        // Do any additional setup after loading the view.
        if let id = userDef.value(forKey: "task_id") as? Int{
            self.lblHeadertitle.text = "Edit Task \(id)"
        }
        self.btnContinue.isSelected = true
        
        Globalfunc.showLoaderView(view: self.view)
        self.getQueueList(strUrl: Constant.queue)

    }
    
    @IBAction func clickONBtnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONBtnContinue(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            self.isCheck = false
        }else {
            sender.isSelected = true
            self.isCheck = true
        }
    }
    
    @IBAction func clickONPreviewBtn(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "taskingStory", bundle: nil)
        let edit = story.instantiateViewController(withIdentifier: "previewQueueListVC") as! previewQueueListVC
        edit.queueid = self.queueId
        edit.modalPresentationStyle = .fullScreen
        self.present(edit, animated: true, completion: nil)
    }
}

extension stepsLoadTaskVC {
    
    func getQueueList(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary]
                        {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrQueue.add(dictA)
                                }
                            }
                            
                            if(self.isEditForm == "true"){
                                let _id = self.pasDictstep["_id"] as! Int
                                let url_step = "\(Constant.task_step)/id?id=\(_id)"
                                self.call_getApiwithParam(strUrl: url_step)
                            }
                            else if(self.isEditForm == "false"){
                                let dict = self.arrStatus[0] as! NSDictionary
                                self.txtStatus.text = (dict["name"] as! String)
                                self.is_deleted = false
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension stepsLoadTaskVC {
    func call_getApiwithParam(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? NSDictionary {
                            self.dictDetail = data
                            if let caError = data["caError"] as? Bool {
                                self.isCheck = caError
                                if(caError == true){
                                    self.btnContinue.isSelected = true
                                }
                                else{
                                    self.btnContinue.isSelected = false
                                }
                            }
                            if let is_deleted = data["is_deleted"] as? Bool {
                                for i in 0..<self.arrStatus.count{
                                    let dict = self.arrStatus[i] as! NSDictionary
                                    let val = dict["value"] as! Bool
                                    if(is_deleted == val){
                                        self.txtStatus.text = (dict["name"] as! String)
                                        self.is_deleted = is_deleted
                                        break
                                    }
                                }
                            }
                            if let queueId = data["queue"] as? Int {
                                for i in 0..<self.arrQueue.count{
                                    let dict = self.arrQueue[i] as! NSDictionary
                                    let _id = dict["_id"] as! Int
                                    if(queueId == _id){
                                        let queue_name = dict["queue_name"] as! String
                                        var title = ""
                                        if let queue_group = dict["queue_group"] as? NSDictionary{
                                            title = queue_group["title"] as! String
                                        }
                                        let strTitle = "[\(title)] \(queue_name)"
                                        self.txtQueue.text = strTitle
                                        self.queueId = queueId
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension stepsLoadTaskVC {
    
    @IBAction func clickONSAveBtn(_ sender: UIButton)
    {
        if(txtStatus.text == "" || txtStatus.text?.count == 0 || txtStatus.text == nil){
            txtStatus.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Status.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            if(isEditForm == "true"){
                let _id = self.dictDetail["_id"] as! Int
                let prior = self.dictDetail["priority"] as! Int
                let step = self.dictDetail["step"] as! Int
                let task_id = userDef.value(forKey: "task_id") as? Int
                let params = [ "caError": self.isCheck,
                               "description": "Load Task Items",
                               "insid": (user?.institute_id!)!,
                               "id": _id,
                               "is_deleted": self.is_deleted,
                               "priority": prior,
                               "queue": self.queueId,
                               "step": step,
                               "task": task_id!,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "put")
            }
            else if(isEditForm == "false"){
                let id = userDef.value(forKey: "task_id") as? Int
                let params = [ "caError": self.isCheck,
                               "description": "Load Task Items",
                               "insid": (user?.institute_id!)!,
                               "id": "",
                               "is_deleted": self.is_deleted,
                               "priority": priority,
                               "queue": self.queueId,
                               "step": self.step_id!,
                               "task": id!,
                               "userid": (user?._id)!] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, strTyp: "post")
            }
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.task_step, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.task_step, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
}

extension stepsLoadTaskVC :  UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewStatus){
            return arrStatus.count
        }
        else if(pickerView == pickerQueue){
            return arrQueue.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewStatus){
            let dict = self.arrStatus[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        else if pickerView == pickerQueue{
            let dict = arrQueue[row] as! NSDictionary
            let queue_name = dict["queue_name"] as! String
            var title = ""
            if let queue_group = dict["queue_group"] as? NSDictionary{
                title = queue_group["title"] as! String
            }
            let strTitle = "[\(title)] \(queue_name)"
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewStatus){
            let dict = self.arrStatus[row] as! NSDictionary
            self.txtStatus.text = (dict["name"] as! String)
            self.is_deleted = dict["value"] as! Bool
        }
        else if pickerView == pickerQueue{
            let dict = arrQueue[row] as! NSDictionary
            let queue_name = dict["queue_name"] as! String
            var title = ""
            if let queue_group = dict["queue_group"] as? NSDictionary{
                title = queue_group["title"] as! String
            }
            let strTitle = "[\(title)] \(queue_name)"
            self.txtQueue.text = strTitle
            let _id = dict["_id"] as! Int
            self.queueId = _id
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtStatus){
            self.pickUp(self.txtStatus)
        }
        else if(textField == self.txtQueue){
            self.pickUp(txtQueue)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtStatus){
            self.pickerviewStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewStatus.delegate = self
            self.pickerviewStatus.dataSource = self
            self.pickerviewStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewStatus
        }
        else if(textField == self.txtQueue){
            self.pickerQueue = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerQueue.delegate = self
            self.pickerQueue.dataSource = self
            self.pickerQueue.backgroundColor = UIColor.white
            textField.inputView = self.pickerQueue
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtStatus.resignFirstResponder()
        txtQueue.resignFirstResponder()
    }
}
