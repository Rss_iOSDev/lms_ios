//
//  AutomatedTaskViewController.swift
//  LMS
//
//  Created by Apple on 14/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

var isOpenpageFrom = ""

class AutomatedTaskViewController: baseViewController {
    
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var tblBin : UITableView!
    
    @IBOutlet weak var txtFilter : UITextField!
    var pickerviewFilter : UIPickerView!
    
    var pickerFilterData : NSMutableArray = []
    
    var arrSelectId = NSMutableArray()
    var is_status_filter = ""
    var isSelectedPicker = ""
    
    var arrTaskList : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        self.tblBin.tableFooterView = UIView()
        
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        
        let dict_1 = ["name":"Active","value":"false"]
        let dict_2 = ["name":"Deleted","value":"true"]
        let dict_3 = ["name":"All Tasks","value":"all"]
        
        
        pickerFilterData.add(dict_1)
        pickerFilterData.add(dict_2)
        pickerFilterData.add(dict_3)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrTaskList = []
        self.tblBin.isHidden = true
        self.lblStatus.isHidden = true
        
        is_status_filter = "false"
        
        self.txtFilter.text = "Active"
        
        Globalfunc.showLoaderView(view: self.view)
        let url = "\(Constant.task)/status?status=\(is_status_filter)"
        self.callUserListApiwithstatus(strUrl: url)
        
    }
    
    
    
    func callUserListApiwithstatus(strUrl: String){
        
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary{
                        
                        if let data = response["data"] as? [NSDictionary]
                        {
                            if(data.count > 0){
                                for i in 0..<data.count{
                                    let dictA = data[i]
                                    self.arrTaskList.add(dictA)
                                }
                                self.tblBin.isHidden = false
                                self.lblStatus.isHidden = true
                                self.tblBin.reloadData()
                            }
                            else{
                                self.lblStatus.isHidden = false
                                self.tblBin.isHidden = true
                                self.arrTaskList = []
                            }
                            
                        }
                        
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension AutomatedTaskViewController {
    
    @IBAction func clikOnPlusBtn(_ sender: UIButton)
    {
        isOpenpageFrom = "new"
        userDef.removeObject(forKey: "task_id")
        self.presentViewControllerBasedOnIdentifier("TaskViewController", strStoryName: "taskingStory")
    }
}

extension AutomatedTaskViewController :  UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerviewFilter){
            return pickerFilterData.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            self.txtFilter.text = (dict["name"] as! String)
            is_status_filter = dict["value"] as! String
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtFilter){
            self.pickUp(self.txtFilter)
        }
        
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtFilter){
            self.pickerviewFilter = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewFilter.delegate = self
            self.pickerviewFilter.dataSource = self
            self.pickerviewFilter.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewFilter
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        self.arrTaskList = []
        Globalfunc.showLoaderView(view: self.view)
        txtFilter.resignFirstResponder()
        let url = "\(Constant.task)/status?status=\(is_status_filter)"
        self.callUserListApiwithstatus(strUrl: url)
    }
    
    @objc func cancelClick(){
        txtFilter.resignFirstResponder()
    }
}

extension AutomatedTaskViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTaskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblBin.dequeueReusableCell(withIdentifier: "tblTaskCell") as! tblTaskCell
        let dict = arrTaskList[indexPath.row] as! NSDictionary
        
        if let status = dict["Status"] as? Bool{
            if(status == false){
                // cell.lblstatus.text = "Delete"
            }
            else{
                // cell.lblstatus.text = "Active"
            }
        }
        
        if let Interval = dict["Interval"] as? String{
            cell.lblInterval.text = Interval
        }
        
        if let BinName = dict["BinName"] as? String{
            cell.lblBinname.text = BinName
        }
        
        if let TaskName = dict["TaskName"] as? String{
            cell.lblTaskname.text = TaskName
        }
        
        if let NextRunDate = dict["NextRunDate"] as? String{
            cell.lblRunDate.text = NextRunDate
        }
        
        if let recordType = dict["RecordType"] as? String{
            cell.lblRecordTyp.text = recordType
        }
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clickONBtnEdit(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        cell.btnPlay.tag = indexPath.row
        cell.btnPlay.addTarget(self, action: #selector(clickONBtnPlay(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnEdit(_ sender: UIButton)
    {
        let dict = arrTaskList[sender.tag] as! NSDictionary
        
        print(dict)
        
        let _id = dict["_id"] as! Int
        isOpenpageFrom = "edit"
        userDef.set(_id, forKey: "task_id")
        self.presentViewControllerBasedOnIdentifier("TaskViewController", strStoryName: "taskingStory")
        
    }
    
    @objc func clickONBtnDel(_ sender: UIButton)
    {
        let dict = arrTaskList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! Int
        
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.task)?id=\(_id)"
            self.deletFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    @objc func clickONBtnPlay(_ sender: UIButton)
    {
        let dict = arrTaskList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! Int
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to Run Task now", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.runtask)?id=\(_id)"
            self.runFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func runFlag(_ strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["data"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

class tblTaskCell : UITableViewCell
{
    @IBOutlet weak var lblTaskname : UILabel!
    @IBOutlet weak var lblBinname : UILabel!
    @IBOutlet weak var lblRecordTyp : UILabel!
    @IBOutlet weak var lblRunDate : UILabel!
    @IBOutlet weak var lblInterval : UILabel!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnPlay : UIButton!
}
