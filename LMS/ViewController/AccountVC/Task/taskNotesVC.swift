//
//  taskNotesVC.swift
//  LMS
//
//  Created by Apple on 26/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class taskNotesVC: UIViewController {
    
    
    @IBOutlet weak var tblNotes : UITableView!
    
    var arrNotesData : NSMutableArray = []
    var passDict : NSDictionary = [:]
    
    @IBOutlet weak var lblHeadertitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblNotes.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let _id = self.passDict["_id"] as! Int
        
        self.lblHeadertitle.text = "Edit Task \(_id)"

        
        let str_url = "\(Constant.task_notes)?taskId=\(_id)&notes_type=task-notes"
        self.callNotesApi(strUrl: str_url)
    }
    
    
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension taskNotesVC {
    
    func setDataonPage(_ dict : NSDictionary)
    {
        if let data = dict["data"] as? [NSDictionary]{
            if(data.count > 0){
                for i in 0..<data.count{
                    let dd = data[i]
                    self.arrNotesData.add(dd)
                }
                self.tblNotes.reloadData()
            }
        }
    }
}

//call getNotes Api
extension taskNotesVC {
    
    func callNotesApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let responseDict = dict as? NSDictionary {
                        self.setDataonPage(responseDict)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    
}

extension taskNotesVC : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrNotesData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblNotes.dequeueReusableCell(withIdentifier: "taskNotesTblCell") as! taskNotesTblCell
        
        let dict = self.arrNotesData[indexPath.section] as! NSDictionary
        if let user = dict["user"] as? String{
            cell.lblUser.text = user
        }
        
        if let note = dict["note"] as? [String]{
            var string = ""
            for value in note {
                string += value
            }
            cell.lblNote.text = string
        }
        
        if let created_at = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: created_at)
            let formatDate = formatter.string(from: date!)
            cell.lblDate.text = formatDate
        }
        return cell
    }
    
}

class taskNotesTblCell: UITableViewCell {
    
    @IBOutlet weak var lblNote : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblUser : UILabel!
    
}


