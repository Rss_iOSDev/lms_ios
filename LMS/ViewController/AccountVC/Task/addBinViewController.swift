

import UIKit

class addBinViewController: UIViewController {
    
    @IBOutlet weak var txtBtn: UITextField!
    @IBOutlet weak var txtRecordTyp: UITextField!
    @IBOutlet weak var txtStatus: UITextField!
    @IBOutlet weak var txtPrivate: UITextField!
    
    var arrRecirdTyp : [NSDictionary] = []
    var pickerRecordTyp: UIPickerView!
    
    var pickerStatus: UIPickerView!
    var arrStatus : [[String : Any]] = []
    
    var pickerPrivate: UIPickerView!
    var arrPrivate : [[String : Any]] = []
    
    
    
    var recTyp = 0
    var status = false
    var strPrivate = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let dict_1 = ["title":"Active","value":false] as [String : Any]
        let dict_2 = ["title":"Delete","value":true] as [String : Any]
        arrStatus.append(dict_1)
        arrStatus.append(dict_2)
        
        
        let dict_3 = ["title":"Yes","value":true] as [String : Any]
        let dict_4 = ["title":"No","value":false] as [String : Any]
        arrPrivate.append(dict_3)
        arrPrivate.append(dict_4)
        
        Globalfunc.showLoaderView(view: self.view)
        self.call_getRecordTyp()
        
        
        // Do any additional setup after loading the view.
    }
}

extension addBinViewController {
    
    func call_getRecordTyp()
    {
        BaseApi.callApiRequestForGet(url: Constant.record_type) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary]
                        {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrRecirdTyp.append(dictA)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.bin, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension addBinViewController {
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        if(txtBtn.text == "" || txtBtn.text?.count == 0 || txtBtn.text == nil){
            txtBtn.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Bin should not left blank.")
        }
        else if(txtRecordTyp.text == "" || txtRecordTyp.text?.count == 0 || txtRecordTyp.text == nil){
            txtRecordTyp.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Record Type.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            let params = [ "binName": self.txtBtn.text!,
                           "insid": (user?.institute_id!)!,
                           "isPrivate":self.strPrivate,
                           "recordType": self.recTyp,
                           "status": self.status,
                           "userid": (user?._id)!] as [String : Any]
            Globalfunc.print(object:params)
            self.callUpdateApi(param: params)
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
}

extension addBinViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerRecordTyp{
            return self.arrRecirdTyp.count
        }
        else if pickerView == pickerStatus{
            return self.arrStatus.count
        }
        else if pickerView == pickerPrivate{
            return self.arrPrivate.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView == pickerRecordTyp{
            let dict = arrRecirdTyp[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerPrivate{
            let dict = arrPrivate[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerRecordTyp{
            let dict = arrRecirdTyp[row]
            let strTitle = dict["title"] as! String
            self.txtRecordTyp.text = strTitle
            let _id = dict["_id"] as! Int
            self.recTyp = _id
        }
        else if pickerView == pickerStatus{
            let dict = arrStatus[row]
            let strTitle = dict["title"] as! String
            self.txtStatus.text = strTitle
            let value = dict["value"] as! Bool
            self.status = value
        }
        else if pickerView == pickerPrivate{
            let dict = arrPrivate[row]
            let strTitle = dict["title"] as! String
            self.txtPrivate.text = strTitle
            let value = dict["value"] as! Bool
            self.strPrivate = value
            
        }
        
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtRecordTyp){
            self.pickUp(txtRecordTyp)
        }
        else if(textField == self.txtStatus){
            self.pickUp(txtStatus)
        }
        else if(textField == self.txtPrivate){
            self.pickUp(txtPrivate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtRecordTyp){
            self.pickerRecordTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRecordTyp.delegate = self
            self.pickerRecordTyp.dataSource = self
            self.pickerRecordTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerRecordTyp
        }
        else if(textField == self.txtStatus){
            self.pickerStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerStatus.delegate = self
            self.pickerStatus.dataSource = self
            self.pickerStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerStatus
        }
            
        else if(textField == self.txtPrivate){
            self.pickerPrivate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPrivate.delegate = self
            self.pickerPrivate.dataSource = self
            self.pickerPrivate.backgroundColor = UIColor.white
            textField.inputView = self.pickerPrivate
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtRecordTyp.resignFirstResponder()
        txtStatus.resignFirstResponder()
        txtPrivate.resignFirstResponder()
    }
    
}
