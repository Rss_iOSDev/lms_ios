//
//  taskItemVC.swift
//  LMS
//
//  Created by Apple on 27/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class taskItemVC: UIViewController {
    
    var passDict : NSDictionary = [:]
    
    
    @IBOutlet weak var tblAccount: UITableView!
    
    @IBOutlet weak var viewTbl: UIView!
    @IBOutlet weak var viewRecodStatus: UIView!
    @IBOutlet weak var hgtViewRec: NSLayoutConstraint!
    
    @IBOutlet weak var lblbinRecord: UILabel!
    @IBOutlet weak var lblbinRecordCount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    
    var acctList = NSMutableArray()
    var passId : Int?
    var arrSelectId = NSMutableArray()
    var arrSelectAccId = NSMutableArray()
    
    var arrSelectIndex = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblAccount.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        
        
        if let id = userDef.value(forKey: "task_id") as? Int{
            self.lblTitle.text = "Edit Bin \(id)"
        }

        
        let url = "\(Constant.bin)/id?id=\(self.passId!)"
        self.call_getApiwithParam(strUrl: url)
        
        self.hgtViewRec.constant = 0
        self.viewRecodStatus.isHidden = true
        
    }
}

extension taskItemVC {
    
    @IBAction func clickONCheckBoxes(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
        else if(sender.tag == 20){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
        else if(sender.tag == 30){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
    }
}

extension taskItemVC {
    func call_getApiwithParam(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let data = dict as? NSDictionary {
                        
                        if let response = data["data"] as? NSDictionary
                        {
                            
                            
                            if let taskName = response["binName"] as? String
                            {
                                self.lblbinRecord.text = "Bin: \(taskName)"
                            }
                            
                            if let accounts = response["accounts"] as? NSMutableArray{
                                if(accounts.count > 0){
                                    
                                    self.hgtViewRec.constant = 0
                                    self.viewTbl.isHidden = false
                                    self.viewRecodStatus.isHidden = true
                                    for i in 0..<accounts.count{
                                        let dictA = accounts[i] as! NSMutableDictionary
                                        dictA.setValue(false, forKey: "is_deleted")
                                        self.acctList.add(dictA)
                                    }
                                    self.tblAccount.reloadData()
                                    self.lblbinRecordCount.isHidden = false
                                    self.lblbinRecordCount.text = "# Of Task Items: \(accounts.count)"
                                }
                                else{
                                    self.acctList = []
                                    self.hgtViewRec.constant = 55
                                    self.viewTbl.isHidden = true
                                    self.viewRecodStatus.isHidden = false
                                    self.lblbinRecordCount.isHidden = true
                                    
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String)
    {
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.bin, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        
    }
}

extension taskItemVC {
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let param = ["id":self.arrSelectId,
                     "acc_id": self.arrSelectAccId,
                     "index": self.arrSelectIndex,
                     "bin_id": self.passId!] as [String: Any]
        
        BaseApi.onResponseDeleteWithTP(url: Constant.remove_record, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}



extension taskItemVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return acctList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAccount.dequeueReusableCell(withIdentifier: "tblEditAcctCell") as! tblEditAcctCell
        let dict = acctList[indexPath.row] as! NSDictionary
        cell.btnDelSelect.isSelected = false
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.btnDelSelect.isSelected = false
            }
            else{
                cell.btnDelSelect.isSelected = true
            }
        }
        
        if let account_no = dict["account_no"] as? String {
            cell.lblTyp.text = "Account(\(account_no))"
        }
        
        let first_name = dict["first_name"] as! String
        let last_name = dict["last_name"] as! String
        cell.lblDetail.text = "\(first_name) \(last_name)"
        
        cell.btnViewRecord.tag = indexPath.row
        cell.btnViewRecord.addTarget(self, action: #selector(clickONViewRecord(_:)), for: .touchUpInside)
        
        cell.btnDelSelect.tag = indexPath.row
        cell.btnDelSelect.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnDel(_ sender: UIButton){
        
        let dict = acctList[sender.tag] as! NSMutableDictionary
        let _id = dict["id"] as! Int
        let acc_id = dict["acc_id"] as! Int
        
         let index = dict["index"] as! Int
        
        if(self.arrSelectId.contains(_id))
        {
            sender.isSelected = false
            self.arrSelectId.remove(_id)
            self.arrSelectAccId.remove(acc_id)
            self.arrSelectIndex.remove(index)
            dict.setValue(false, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
        else{
            sender.isSelected = true
            self.arrSelectId.add(_id)
            self.arrSelectAccId.add(acc_id)
            self.arrSelectIndex.add(index)
            dict.setValue(true, forKey: "is_deleted")
            self.acctList.replaceObject(at: sender.tag, with: dict)
            self.tblAccount.reloadData()
        }
    }
    
    @objc func clickONViewRecord(_ sender: UIButton)
    {
        let dict = acctList[sender.tag] as! NSDictionary
        let _id = dict["id"] as! Int
        
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let detail = story.instantiateViewController(identifier: "accountDetailPageController") as! accountDetailPageController
        detail.accId = _id
        detail.isDetailOpenfrom = "bin"
        // detail.passDict_LayoutId = self.dictLayout
        detail.modalPresentationStyle = .fullScreen
        self.present(detail, animated: true, completion: nil)
    }
}
