//
//  filterAccountViewController.swift
//  LMS
//
//  Created by Apple on 26/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

protocol filterAccountDelegate {
    func setFilteronPage(filter: [String: Any])
}


class filterAccountViewController: UIViewController {
    
    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var txtAccountStatus : UITextField!
    @IBOutlet weak var txtAcctType : UITextField!
    @IBOutlet weak var txtLocation : UITextField!
    @IBOutlet weak var txtFrstName : UITextField!
    @IBOutlet weak var txtLastNAme : UITextField!
    @IBOutlet weak var txtAcct_ : UITextField!
    @IBOutlet weak var txtLoan_ : UITextField!
    @IBOutlet weak var txtLast_cc : UITextField!
    @IBOutlet weak var txtSSn : UITextField!
    @IBOutlet weak var txtPhone : UITextField!
    @IBOutlet weak var txtStock_ : UITextField!
    @IBOutlet weak var txtVin : UITextField!
    @IBOutlet weak var txtFlags : UITextField!
    @IBOutlet weak var txtPortfolio : UITextField!
    
    @IBOutlet weak var txtdeviceEsn : UITextField!
    @IBOutlet weak var txtDl : UITextField!
    @IBOutlet weak var txtemail : UITextField!
    @IBOutlet weak var txtaddress : UITextField!
    
    @IBOutlet weak var viewEdit: UIView!
    @IBOutlet weak var txtTitle: UITextField!
    
    
    var delegate: filterAccountDelegate?
    
    var passDict_LayoutId : NSDictionary = [:]
    
    var arrAccountType : NSMutableArray = []
    var arrAccountStatus : NSMutableArray = []
    var arrPortfolio : NSMutableArray = []
    var arrFlag : NSMutableArray = []
    var arrDefBranch : NSMutableArray = []
    
    var pickerViewAccountTyp: UIPickerView!
    var pickerViewLocation: UIPickerView!
    var pickerViewAccountStatus: UIPickerView!
    var pickerViewPortfolio: UIPickerView!
    var pickerViewFlag: UIPickerView!
    
    var selectedAcctTypId = [String]()
    var selectedAcctTypTitle = [String]()
    
    var selectedDefBrnchId = [String]()
    var selectedDefBrnchTitle = [String]()
    
    var selectedAccountSatusId = [String]()
    var selectedAccountSatusTitle = [String]()
    
    var selectedPortfolioId = [String]()
    var selectedPortfolioTitle = [String]()
    
    var selectedFlagId = [String]()
    var selectedFlagTitle = [String]()
    
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewEdit.isHidden = true
        
        callApiToSetDataonPicker()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnFavBtn(_ sender: UIButton)
    {
        self.viewEdit.isHidden = false
    }
    
    
    @IBAction func clickOnSearchBtn(_ sender: UIButton){
        
        let queLayoutId = passDict_LayoutId["layout_id"] as! Int
        let defaultRecordsPage = passDict_LayoutId["defaultRecordsPage"] as! Int
        let order1 = passDict_LayoutId["order1"] as! String
        let order2 = passDict_LayoutId["order2"] as! String
        let order3 = passDict_LayoutId["order1"] as! String
        let sort1 = passDict_LayoutId["sort1"] as! String
        let sort2 = passDict_LayoutId["sort2"] as! String
        let sort3 = passDict_LayoutId["sort3"] as! String
        
        var straccount_status = ""
        if(selectedAccountSatusId.count > 0){
            straccount_status = selectedAccountSatusId.joined(separator: ",")
            url = "/lms/account/search-accounts?account_status=\(straccount_status)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var straccount_type = ""
        if(selectedAcctTypId.count > 0){
            straccount_type = selectedAcctTypId.joined(separator: ",")
            url = "/lms/account/search-accounts?account_type=\(straccount_type)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strchannel = ""
        if(selectedDefBrnchId.count > 0){
            strchannel = selectedDefBrnchId.joined(separator: ",")
            url = "/lms/account/search-accounts?channel=\(strchannel)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strportfolio = ""
        if(selectedPortfolioId.count > 0){
            strportfolio = selectedPortfolioId.joined(separator: ",")
            url = "/lms/account/search-accounts?portfolio=\(strportfolio)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strflags = ""
        if(selectedFlagId.count > 0){
            strflags = selectedFlagId.joined(separator: ",")
            
            url = "/lms/account/search-accounts?flags=\(strflags)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        let passDict = ["search": "\(txtSearch.text!)",
            "queueLayoutsId": queLayoutId,
            "currentPageNumber": 1,
            "account_status": straccount_status,
            "account_type": straccount_type,
            "channel": strchannel,
            "last_name": "\(txtLastNAme.text!)",
            "first_name": "\(txtFrstName.text!)",
            "account": "\(txtAcct_.text!)",
            "ssn": "\(txtSSn.text!)",
            "loan_no": "\(txtLoan_.text!)",
            "last_cc_checking": "\(txtLast_cc.text!)",
            "stock": "\(txtStock_.text!)",
            "vin": "\(txtVin.text!)",
            "flags": strflags,
            "phone": "\(txtPhone.text!)",
            "portfolio": strportfolio,
            "defaultRecordsPage": defaultRecordsPage,
            "sort1": "\(sort1)",
            "order1": "\(order1)",
            "sort2": "\(sort2)",
            "order2": "\(order2)",
            "sort3": "\(sort3)",
            "order3": "\(order3)",
            "isGet": false,
            "isNext": false,
            "address": self.txtaddress.text!,
            "device_esn": self.txtdeviceEsn.text!,
            "dl_no": self.txtDl.text!,
            "email": self.txtemail.text!] as [String : Any]
        
        Globalfunc.print(object:passDict)
        
        delegate?.setFilteronPage(filter: passDict)
    }
    
    
    @IBAction func clikcOnCrossBtn(_ sender : UIButton)
    {
        self.viewDidLoad()
    }
    
    @IBAction func clickOnOkBtn(_ sneder: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let params = [
            "insid": (user?.institute_id!)!,
            "title": self.txtTitle.text!,
            "type":"search-account",
            "url": url,
            "userid": (user?._id)!] as [String : Any]
        
        BaseApi.onResponsePostWithToken(url: Constant.favorite, controller: self, parms: params) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewDidLoad()
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}



extension filterAccountViewController{
    
    func callApiToSetDataonPicker(){
        
        BaseApi.callApiRequestForGet(url: Constant.master_url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary{
                        
                        if let data = response["data"] as? NSDictionary{
                            if let accountType = data["accountType"] as? [NSDictionary] {
                                if(accountType.count > 0){
                                    for i in 0..<accountType.count{
                                        let dictA = accountType[i]
                                        self.arrAccountType.add(dictA)
                                    }
                                }
                            }
                            if let accountStatus = data["accountStatus"] as? [NSDictionary] {
                                if(accountStatus.count > 0){
                                    for i in 0..<accountStatus.count{
                                        let dictA = accountStatus[i]
                                        self.arrAccountStatus.add(dictA)
                                    }
                                }
                            }
                            
                            
                            
                            if let portfolio = data["portfolio"] as? [NSDictionary] {
                                if(portfolio.count > 0){
                                    for i in 0..<portfolio.count{
                                        let dictA = portfolio[i]
                                        self.arrPortfolio.add(dictA)
                                    }
                                }
                            }
                            
                            
                            if let flag = data["flag"] as? [NSDictionary] {
                                if(flag.count > 0){
                                    for i in 0..<flag.count{
                                        let dictA = flag[i]
                                        self.arrFlag.add(dictA)
                                    }
                                }
                            }
                            
                            
                            if let location = data["location"] as? [NSDictionary] {
                                if(location.count > 0){
                                    for i in 0..<location.count{
                                        let dictA = location[i]
                                        self.arrDefBranch.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    
                    if let arr = dict as? NSDictionary {
                        if let msg = arr["message"] as? String{
                            if(msg == "Your token has expired."){
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
        
        
    }
}

extension filterAccountViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pickerViewAccountTyp{
            return self.arrAccountType.count
        }
        else if(pickerView == pickerViewLocation){
            return self.arrDefBranch.count
        }
            
        else if(pickerView == pickerViewAccountStatus){
            return self.arrAccountStatus.count
        }
            
        else if(pickerView == pickerViewPortfolio){
            return self.arrPortfolio.count
        }
        else if(pickerView == pickerViewFlag){
            return self.arrFlag.count
        }
            
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSMutableAttributedString()
        if pickerView == pickerViewAccountTyp{
            
            let dict = arrAccountType[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in  selectedAcctTypId{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        else if pickerView == pickerViewLocation{
            
            
            let dict = arrDefBranch[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedDefBrnchId{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewAccountStatus){
            
            let dict = arrAccountStatus[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedAccountSatusId {
                if item == "\(_id)"{
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewPortfolio){
            
            let dict = arrPortfolio[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedPortfolioId{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewFlag){
            
            let dict = arrFlag[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedFlagId {
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView == pickerViewAccountTyp){
            
            
            let dict = arrAccountType[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            
            
            if selectedAcctTypId.contains("\(_id)") {
                var index = 0
                for item in selectedAcctTypId {
                    if item == "\(_id)" {
                        selectedAcctTypId.remove(at: index)
                        selectedAcctTypTitle.remove(at: index)
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedAcctTypId.append("\(_id)")
                selectedAcctTypTitle.append(strTitle)
            }
            
            
            self.txtAcctType.text = selectedAcctTypTitle.joined(separator: ", ")    //selectedArray is array has selected item
            
            let queLayoutId = passDict_LayoutId["layout_id"] as! Int
            url = "/lms/account/search-accounts?account_type=\(txtAcctType.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
            
            self.pickerViewAccountTyp.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewLocation){
            
            let dict = arrDefBranch[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedDefBrnchId.contains("\(_id)") {
                var index = 0
                for item in selectedDefBrnchId {
                    if item == "\(_id)" {
                        
                        selectedDefBrnchId.remove(at: index)
                        selectedDefBrnchTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedDefBrnchId.append("\(_id)")
                selectedDefBrnchTitle.append(strTitle)
            }
            
            self.txtLocation.text = selectedDefBrnchTitle.joined(separator: ", ")
            self.pickerViewLocation.reloadAllComponents()
        }
            //
        else if(pickerView == pickerViewAccountStatus){
            
            let dict = arrAccountStatus[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedAccountSatusId.contains("\(_id)") {
                var index = 0
                for item in selectedAccountSatusId {
                    if item == "\(_id)" {
                        selectedAccountSatusId.remove(at: index)
                        selectedAccountSatusTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedAccountSatusId.append("\(_id)")
                selectedAccountSatusTitle.append(strTitle)
            }
            self.txtAccountStatus.text = selectedAccountSatusTitle.joined(separator: ", ")
            self.pickerViewAccountStatus.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewPortfolio){
            
            let dict = arrPortfolio[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedPortfolioId.contains("\(_id)") {
                var index = 0
                for item in selectedPortfolioId {
                    if item == "\(_id)" {
                        selectedPortfolioId.remove(at: index)
                        selectedPortfolioTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedPortfolioId.append("\(_id)")
                selectedPortfolioTitle.append(strTitle)
            }
            
            self.txtPortfolio.text = selectedPortfolioTitle.joined(separator: ", ")
            self.pickerViewPortfolio.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewFlag){
            
            let dict = arrFlag[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedFlagId.contains("\(_id)") {
                var index = 0
                for item in selectedFlagId {
                    if item == "\(_id)" {
                        selectedFlagId.remove(at: index)
                        selectedFlagTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedFlagId.append("\(_id)")
                selectedFlagTitle.append(strTitle)
            }
            
            self.txtFlags.text = selectedFlagTitle.joined(separator: ", ")
            self.pickerViewFlag.reloadAllComponents()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtAcctType){
            self.pickUp(txtAcctType)
        }
        else if(textField == self.txtLocation){
            self.pickUp(txtLocation)
        }
        else if(textField == self.txtAccountStatus){
            self.pickUp(txtAccountStatus)
        }
        else if(textField == self.txtPortfolio){
            self.pickUp(txtPortfolio)
        }
        else if(textField == self.txtFlags){
            self.pickUp(txtFlags)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let queLayoutId = passDict_LayoutId["layout_id"] as! Int
        if(textField == txtSearch){
            url = "/lms/account/search-accounts?search=\(txtSearch.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
    }
    
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAcctType){
            self.pickerViewAccountTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAccountTyp.delegate = self
            self.pickerViewAccountTyp.dataSource = self
            self.pickerViewAccountTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAccountTyp
        }
        else if(textField == self.txtLocation){
            self.pickerViewLocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewLocation.delegate = self
            self.pickerViewLocation.dataSource = self
            self.pickerViewLocation.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewLocation
        }
        else if(textField == self.txtAccountStatus){
            self.pickerViewAccountStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAccountStatus.delegate = self
            self.pickerViewAccountStatus.dataSource = self
            self.pickerViewAccountStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAccountStatus
        }
            
        else if(textField == self.txtPortfolio){
            self.pickerViewPortfolio = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPortfolio.delegate = self
            self.pickerViewPortfolio.dataSource = self
            self.pickerViewPortfolio.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPortfolio
            
        }
        else if(textField == self.txtFlags){
            self.pickerViewFlag = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewFlag.delegate = self
            self.pickerViewFlag.dataSource = self
            self.pickerViewFlag.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewFlag
            
        }
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtAcctType.resignFirstResponder()
        txtLocation.resignFirstResponder()
        txtAccountStatus.resignFirstResponder()
        txtPortfolio.resignFirstResponder()
        txtFlags.resignFirstResponder()
    }
}
