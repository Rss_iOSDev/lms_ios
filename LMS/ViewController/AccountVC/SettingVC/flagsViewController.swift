//
//  flagsViewController.swift
//  LMS
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class flagsViewController: baseViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var tblFlagList : UITableView!
    
    @IBOutlet weak var viewAddFlag : UIView!
    
    var arrFlagList : [NSDictionary] = []

    var pickerModule : UIPickerView!
    var arrModule = NSMutableArray()
    
    var pickerPriority : UIPickerView!
    var arrPriorty = ["Low","Medium","High","Informational"]
    
    var edit_flag_id : Int!
    var module_val = ""
    
    @IBOutlet weak var txtFlagname: UITextField!
    @IBOutlet weak var txtModule: UITextField!
    @IBOutlet weak var txtDescriptn: UITextField!
    @IBOutlet weak var txtPriority: UITextField!
    @IBOutlet weak var btnAddUpdate: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblFlagList.tableFooterView = UIView()
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        let dict1 = ["title":"Account Module","value":"A"]
        let dict2 = ["title":"Inventory","value":"I"]
        let dict3 = ["title":"Sales Module","value":"S"]
        let dict4 = ["title":"Accounting Class","value":"C"]
        let dict5 = ["title":"Accounting Location","value":"L"]
        
        self.arrModule.add(dict1)
        self.arrModule.add(dict2)
        self.arrModule.add(dict3)
        self.arrModule.add(dict4)
        self.arrModule.add(dict5)
        self.setUpData()
        
    }
    
    func setUpData()
    {
        self.viewAddFlag.alpha = 0
        Globalfunc.showLoaderView(view: self.view)
        self.getFlagListApi()
    }
    
    @IBAction func clickONAddBtn(_ sender: UIButton)
    {
        
        if(sender.tag == 10){
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewAddFlag.alpha = 1
            }) { _ in
                
                self.btnAddUpdate.setTitle("Add Flag", for: .normal)
            }
        }
        else if(sender.tag == 20){
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewAddFlag.alpha = 0
            }) { _ in
                
            }
        }
    }
}

extension flagsViewController {
    
    @IBAction func clickONPostFlagBtn(_ sender : UIButton)
    {
        if(txtFlagname.text == "" || txtFlagname.text?.count == 0 || txtFlagname.text == nil){
            txtFlagname.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Flag Name.")
        }
        else if(txtDescriptn.text == "" || txtDescriptn.text?.count == 0 || txtDescriptn.text == nil){
            txtDescriptn.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Description.")
        }
        else if(txtModule.text == "" || txtModule.text?.count == 0 || txtModule.text == nil){
                txtModule.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Module.")
        }
        else if(txtPriority.text == "" || txtPriority.text?.count == 0 || txtPriority.text == nil){
            txtPriority.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Priority.")
        }

        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            
            if let buttonTitle = self.btnAddUpdate.title(for: .normal) {
                
                if(buttonTitle == "Add Flag"){
                    Globalfunc.showLoaderView(view: self.view)
                    self.setParamtoAddFlag(strTyp: "add")
                }
                else if (buttonTitle == "Update"){
                    Globalfunc.showLoaderView(view: self.view)
                    self.setParamtoAddFlag(strTyp: "update")
                }
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }

    func setParamtoAddFlag(strTyp: String)
    {
        if(strTyp == "add"){
            let params = [
                "description": "\(txtDescriptn.text!)",
                "insid": (user?.institute_id)!,
                "module": "\(self.module_val)",
                "priority": "\(txtPriority.text!)",
                "title": "\(txtFlagname.text!)",
                "userid": (user?._id)!] as [String : Any]
            Globalfunc.print(object:params)
            self.sendDatatoPostApi(param: params, strTyp: "post")
        }
        else if(strTyp == "update"){
            
            let params = [
                "description": "\(txtDescriptn.text!)",
                "module": "\(self.module_val)",
                "priority": "\(txtPriority.text!)",
                "title": "\(txtFlagname.text!)",
                "id": self.edit_flag_id!] as [String : Any]
            Globalfunc.print(object:params)
            self.sendDatatoPostApi(param: params, strTyp: "put")

            
        }
    }
    
    func sendDatatoPostApi( param: [String : Any],strTyp : String)
    {
        if(strTyp == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.flag_url, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.setUpData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        else if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.flag_url, controller: self, parms: param as NSDictionary) { (dict, error) in
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.setUpData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }

            }
        }
    }
}

//call action api for all pages once
extension flagsViewController{
    
    func getFlagListApi()
    {
        self.arrFlagList = []
        BaseApi.callApiRequestForGet(url: Constant.flag_url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary]{
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i] as! NSMutableDictionary
                                    self.arrFlagList.append(dictA)
                                }
                                self.tblFlagList.isHidden = false
                                self.tblFlagList.reloadData()
                            }
                            else{
                                self.tblFlagList.isHidden = true
                            }
                    }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension flagsViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFlagList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblFlagList.dequeueReusableCell(withIdentifier: "tblFlagCell") as! tblFlagCell
        let dict = arrFlagList[indexPath.row]
        
        
        if let description = dict["description"] as? String
        {
            cell.lblDescription.text = description
            
        }
 
        if let module = dict["module"] as? String
        {
            cell.lblModule.text = "\(module)"
        }
 
        if let priority = dict["priority"] as? String
        {
            cell.lblPriority.text = "\(priority)"
        }
 
        if let title = dict["title"] as? String
        {
            cell.lblFlagName.text = "\(title)"
        }
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clcikOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnDel.tag = indexPath.row
        cell.btnDel.addTarget(self, action: #selector(clcikOnDeleteBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func clcikOnDeleteBtn(_ sender: UIButton)
    {
        let dict = arrFlagList[sender.tag]
        let id_del = (dict["_id"] as! Int)
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.flag_url)?id=\(id_del)"
            self.deletFlag(strUrl)

        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.setUpData()
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }

        }
    }
    
    
    @objc func clcikOnEditBtn(_ sender: UIButton)
    {
        let dict = arrFlagList[sender.tag]
        self.btnAddUpdate.setTitle("Update", for: .normal)
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewAddFlag.alpha = 1
        }) { _ in
             
            if let description = dict["description"] as? String
            {
                self.txtDescriptn.text = description
            }
            
            if let module = dict["module"] as? String
            {
                self.txtModule.text = "\(module)"
            }
            
            if let priority = dict["priority"] as? String
            {
                self.txtPriority.text = "\(priority)"
            }
            
            if let title = dict["title"] as? String
            {
                self.txtFlagname.text = "\(title)"
            }
            self.edit_flag_id = (dict["_id"] as! Int)
        }
    }
}

class tblFlagCell : UITableViewCell
{
    @IBOutlet weak var lblFlagName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblModule : UILabel!
    @IBOutlet weak var lblPriority : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDel : UIButton!
    
}

/*MARk - Textfeld and picker delegates*/

extension flagsViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerModule{
            return self.arrModule.count
        }
        else if(pickerView == pickerPriority){
            return self.arrPriorty.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerModule{
            let dict = arrModule[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerPriority){
            let strTitle = arrPriorty[row]
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerModule{
            let dict = arrModule[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtModule.text = strTitle
            self.module_val = dict["value"] as! String
        }
            
        else if(pickerView == pickerPriority){
            let strTitle = arrPriorty[row]
            self.txtPriority.text = strTitle
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtModule){
            self.pickUp(txtModule)
        }
        else if(textField == self.txtPriority){
            self.pickUp(txtPriority)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtModule){
            self.pickerModule = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerModule.delegate = self
            self.pickerModule.dataSource = self
            self.pickerModule.backgroundColor = UIColor.white
            textField.inputView = self.pickerModule
        }
            
        else if(textField == self.txtPriority){
            
            self.pickerPriority = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPriority.delegate = self
            self.pickerPriority.dataSource = self
            self.pickerPriority.backgroundColor = UIColor.white
            textField.inputView = self.pickerPriority
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    //MARK:- Button
    @objc func doneClick() {
        txtModule.resignFirstResponder()
        txtPriority.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtModule.resignFirstResponder()
        txtPriority.resignFirstResponder()
    }
    
    
}
