//
//  manageSettingViewController.swift
//  LMS
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class manageSettingViewController: baseViewController {
    
    @IBOutlet weak var tblManage : UITableView!
       
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!

    var arrList = ["Institution","Time Zone","User Role","User Permission","Channel Type","Account Status","Account Type","Data Source","Country","Payment Performance","Payment Type","Delivery Method","Payment Form","Add Defer Downpayment","Add Transaction Description","Add Consumer Info Indicator"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblManage.tableFooterView = UIView()
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)

        
    }
}

extension manageSettingViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblManage.dequeueReusableCell(withIdentifier: "tblManage") as! tblManage
        cell.lblTitle.text = self.arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("InstitutionVC", strStoryName: "SettingStory")
//            case 1:
//                self.presentViewControllerBasedOnIdentifier("remainingPayVC", strStoryName: "ActionStory")
//            case 2:
//                self.presentViewControllerBasedOnIdentifier("paymentPerformVC", strStoryName: "ActionStory")
//            case 3:
//                self.presentViewControllerBasedOnIdentifier("transactionSnapVC", strStoryName: "ActionStory")
//            case 4:
//                    self.presentViewControllerBasedOnIdentifier("notesSnapSV", strStoryName: "ActionStory")
//            case 5:
//                self.presentViewControllerBasedOnIdentifier("remainingPayVC", strStoryName: "ActionStory")
//            case 6:
//                self.presentViewControllerBasedOnIdentifier("paymentPerformVC", strStoryName: "ActionStory")
//            case 7:
//                self.presentViewControllerBasedOnIdentifier("transactionSnapVC", strStoryName: "ActionStory")
//            case 8:
//                self.presentViewControllerBasedOnIdentifier("notesSnapSV", strStoryName: "ActionStory")
            default:
                Globalfunc.print(object:"default")
            
        }
    }
}

class tblManage : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
