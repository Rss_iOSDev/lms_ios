//
//  InstitutionVC.swift
//  LMS
//
//  Created by Apple on 22/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class InstitutionVC: UIViewController {
    
    @IBOutlet weak var tblInstitutionList : UITableView!
    
    @IBOutlet weak var viewAddInstitition : UIView!
    
    var arrInstitutionList : [NSDictionary] = []
    var edit_flag_id : Int!
    
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var txtContctNo: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCode: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtzip: UITextField!
    @IBOutlet weak var txtcountry: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    
    var isEditForm = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblInstitutionList.tableFooterView = UIView()
        self.setUpData()
    }
    
    func setUpData()
    {
        self.viewAddInstitition.alpha = 0
        Globalfunc.showLoaderView(view: self.view)
        self.getInstitutionListApi(strTyp: "get", strUrl: Constant.institution_url)
    }
    
    @IBAction func clickONAddBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewAddInstitition.alpha = 1
            }) { _ in
                self.isEditForm = "Add"
            }
        }
        else if(sender.tag == 20){
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewAddInstitition.alpha = 0
            }) { _ in
            }
        }
    }
}

extension InstitutionVC {
    
    @IBAction func clickONSubmitBtn(_ sender : UIButton)
    {
        if(txtTitle.text == "" || txtTitle.text?.count == 0 || txtTitle.text == nil){
            txtTitle.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Title.")
        }
        else if(txtDescription.text == "" || txtDescription.text?.count == 0 || txtDescription.text == nil){
            txtDescription.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Description.")
        }
        else if(txtContctNo.text == "" || txtContctNo.text?.count == 0 || txtContctNo.text == nil){
            txtContctNo.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Contact Number.")
        }
        else if(txtAddress.text == "" || txtAddress.text?.count == 0 || txtAddress.text == nil){
            txtAddress.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Address.")
        }
        else if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            txtState.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter State.")
        }
        else if(txtCode.text == "" || txtCode.text?.count == 0 || txtCode.text == nil){
            txtCode.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter code.")
        }
        else if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Email.")
        }
        else if(txtzip.text == "" || txtzip.text?.count == 0 || txtzip.text == nil){
            txtzip.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Zip.")
        }
        else if(txtcountry.text == "" || txtcountry.text?.count == 0 || txtcountry.text == nil){
            txtcountry.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Country.")
        }
        else if(txtCity.text == "" || txtCity.text?.count == 0 || txtCity.text == nil){
            txtCity.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter city.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            
            if(isEditForm == "Add"){
                Globalfunc.showLoaderView(view: self.view)
                self.setParamtoAddFlag(strTyp: "add")
            }
            else if (isEditForm == "Edit"){
                Globalfunc.showLoaderView(view: self.view)
                self.setParamtoAddFlag(strTyp: "update")
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoAddFlag(strTyp: String)
    {
        if(strTyp == "add"){
            let params = [
                "address": "\(txtAddress.text!)",
                "city": "\(txtCity.text!)",
                "code": "\(self.txtCode.text!)",
                "contact": "\(txtContctNo.text!)",
                "country": "\(txtcountry.text!)",
                "description": "\(txtDescription.text!)",
                "email": "\(txtEmail.text!)",
                "state": "\(txtState.text!)",
                "title": "\(txtTitle.text!)",
                "zip": "\(txtzip.text!)",
                "userid": (user?._id)!] as [String : Any]
            Globalfunc.print(object:params)
            self.sendDatatoPostApi(param: params, strTyp: "post")
        }
        else if(strTyp == "update"){
            let params = [
                "address": "\(txtAddress.text!)",
                "city": "\(txtCity.text!)",
                "code": "\(self.txtCode.text!)",
                "contact": "\(txtContctNo.text!)",
                "country": "\(txtcountry.text!)",
                "description": "\(txtDescription.text!)",
                "email": "\(txtEmail.text!)",
                "id": self.edit_flag_id! ,
                "state": "\(txtState.text!)",
                "title": "\(txtTitle.text!)",
                "zip": "\(txtzip.text!)"] as [String : Any]
            
            Globalfunc.print(object:params)
            self.sendDatatoPostApi(param: params, strTyp: "put")
            
            
        }
    }
    
    func sendDatatoPostApi( param: [String : Any],strTyp : String)
    {
        if(strTyp == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.institution_url, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        print(dict)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.txtAddress.text = ""
                                self.txtCity.text = ""
                                self.txtCode.text = ""
                                self.txtContctNo.text = ""
                                self.txtcountry.text = ""
                                self.txtDescription.text = ""
                                self.txtEmail.text = ""
                                self.txtState.text = ""
                                self.txtTitle.text = ""
                                self.setUpData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
            
        else if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.institution_url, controller: self, parms: param as NSDictionary) { (dict, error) in
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                                self.txtAddress.text = ""
                                self.txtCity.text = ""
                                self.txtCode.text = ""
                                self.txtContctNo.text = ""
                                self.txtcountry.text = ""
                                self.txtDescription.text = ""
                                self.txtEmail.text = ""
                                self.txtState.text = ""
                                self.txtTitle.text = ""
                                
                                self.setUpData()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
        }
    }
}

//call action api for all pages once
extension InstitutionVC{
    
    func getInstitutionListApi(strTyp: String,strUrl: String)
    {
        if (strTyp == "get")
        {
            self.arrInstitutionList = []
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let response = dict as? [NSDictionary] {
                            if(response.count > 0){
                                for i in 0..<response.count{
                                    let dictA = response[i] as! NSMutableDictionary
                                    self.arrInstitutionList.append(dictA)
                                }
                                self.tblInstitutionList.isHidden = false
                                self.tblInstitutionList.reloadData()
                            }
                            else{
                                self.tblInstitutionList.isHidden = true
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "edit"){
            
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let responseDict = dict as? NSDictionary {
                            
                            print(responseDict)
                            
                            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseInOut, animations: {
                                self.viewAddInstitition.alpha = 1
                            }) { _ in
                                
                                self.isEditForm = "Edit"
                                
                                self.edit_flag_id = (responseDict["_id"] as! Int)
                                
                                if let address = responseDict["address"] as? String
                                {
                                    self.txtAddress.text = address
                                }
                                
                                if let city = responseDict["city"] as? String
                                {
                                    self.txtCity.text = city
                                }
                                
                                
                                if let code = responseDict["code"] as? String
                                {
                                    self.txtCode.text = code
                                }
                                
                                if let contact = responseDict["contact"] as? String
                                {
                                    self.txtContctNo.text = "\(contact)"
                                }
                                
                                if let country = responseDict["country"] as? String
                                {
                                    self.txtcountry.text = "\(country)"
                                }
                                
                                if let description = responseDict["description"] as? String
                                {
                                    self.txtDescription.text = "\(description)"
                                }
                                
                                if let email = responseDict["email"] as? String
                                {
                                    self.txtEmail.text = "\(email)"
                                }
                                
                                
                                if let state = responseDict["state"] as? String
                                {
                                    self.txtState.text = "\(state)"
                                }
                                
                                if let title = responseDict["title"] as? String
                                {
                                    self.txtTitle.text = "\(title)"
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        
        
    }
}

extension InstitutionVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInstitutionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblInstitutionList.dequeueReusableCell(withIdentifier: "tblFlagCell") as! tblFlagCell
        let dict = arrInstitutionList[indexPath.row]
        
        print(dict)
        
        if let description = dict["description"] as? String
        {
            cell.lblModule.text = description
        }
        
        if let code = dict["code"] as? String
        {
            cell.lblDescription.text = "\(code)"
        }
        
        if let email = dict["email"] as? String
        {
            cell.lblPriority.text = "\(email)"
        }
        
        if let title = dict["title"] as? String
        {
            cell.lblFlagName.text = "\(title)"
        }
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clcikOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnDel.tag = indexPath.row
        cell.btnDel.addTarget(self, action: #selector(clcikOnDeleteBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func clcikOnDeleteBtn(_ sender: UIButton)
    {
        let dict = arrInstitutionList[sender.tag]
        let id_del = (dict["_id"] as! Int)
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.institution_url)?id=\(id_del)"
            self.deletFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.setUpData()
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    @objc func clcikOnEditBtn(_ sender: UIButton)
    {
        let dict = arrInstitutionList[sender.tag]
        print(dict)
        let editId = (dict["_id"] as! Int)
        let strUrl = "\(Constant.institution_url)/id?id=\(editId)"
        self.getInstitutionListApi(strTyp: "edit", strUrl: strUrl)
    }
}
