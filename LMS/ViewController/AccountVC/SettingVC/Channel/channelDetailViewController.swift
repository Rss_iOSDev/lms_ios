//
//  channelDetailViewController.swift
//  LMS
//
//  Created by Apple on 29/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class channelDetailViewController: UIViewController {
    
    @IBOutlet weak var tblManage : UITableView!
    
    @IBOutlet weak var lblChnId : UILabel!
    @IBOutlet weak var lblChnTyp : UILabel!
    @IBOutlet weak var lblChnDesc : UILabel!
    @IBOutlet weak var lblChnStatus : UILabel!
    
    var arrList = [String]()
    
    var selectTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblManage.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        self.callGetChannelDetailApi()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension channelDetailViewController {
    
    func callGetChannelDetailApi()
    {
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.addChannel)/id?id=\(chnl_id)"
                self.callApi(strUrl: strUrl)
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in

            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {

                        if let responseDict = response["data"] as? NSDictionary {
                            let chnl_id = responseDict["_id"] as! Int
                            self.lblChnId.text = "\(chnl_id)"

                            let title = responseDict["title"] as! String
                            self.lblChnDesc.text = "\(title)"

//                            self.selectTitle = "\(title)"

                            let channel = responseDict["channeltype"] as? NSDictionary
                            self.selectTitle = channel?["title"] as! String

                            if let channeltype = responseDict["channeltype"] as? NSDictionary
                            {
                                let title = channeltype["title"] as! String
                                self.lblChnTyp.text = "\(title)"

                                if (title == "Sales Location/Branch") {
                                    self.arrList = ["Detail","Sales Defaults","Inventory Defaults","Buyer Guide Defaults","Seller Fees","Seller Taxes","After Market","Accesories","Tax Packages","Tax Info","Lender Defaults","Lead Defaults","Ecabinet","Notes"]
                                }
                                else if (title == "Bookkeeping Company") {
                                    self.arrList = ["Detail","Chart of Accounts","Class Code Map","AR Payment Methods","AP Payment Methods","AP Channels","Reason Codes","Notes"]
                                }
                                else if (title == "Finance Company/Bank") {
                                    self.arrList = ["Detail","Lender Defaults","Lead Defaults","Ecabinet","Notes"]
                                }
                                else if (title == "Source" || title == "Insurance Agent" || title == "Insurance Company" || title == "Dealer/Wholesaler/Auction" || title == "Repo Agent")
                                {
                                    self.arrList = ["Detail","Notes"]
                                }
                                else if (title == "Inventory Buyer" || title == "Document")
                                {
                                    self.arrList = ["Detail","Sales Defaults","Inventory Defaults","Buyer Guide Defaults","Seller Fees","Seller Taxes","After Market","Accesories","Tax Packages","Tax Info","Chart of Accounts","Class Code Map","AR Payment Methods","AP Payment Methods","AP Channels","Reason Codes","Lender Defaults","Lead Defaults","Ecabinet","Notes"]
                                }
                            }

                            if let is_deleted = responseDict["is_deleted"] as? Bool
                            {
                                if(is_deleted == false){
                                    self.lblChnStatus.text = "Active"
                                    self.lblChnStatus.textColor = .systemGreen
                                }
                                else{
                                    self.lblChnStatus.text = "Deleted"
                                    self.lblChnStatus.textColor = .systemRed
                                }
                            }
                        }
                        self.tblManage.reloadData()
                    }

                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension channelDetailViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblManage.dequeueReusableCell(withIdentifier: "tblManage") as! tblManage
        cell.lblTitle.text = self.arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        if (self.selectTitle == "Sales Location/Branch") {
            
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("CdetailVC", strStoryName: "channelStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("CsalesDefaultVC", strStoryName: "channelStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 3:
                self.presentViewControllerBasedOnIdentifier("channelBuyerDefVc", strStoryName: "channelStory")
            case 4:
                self.presentViewControllerBasedOnIdentifier("channelSellerFeeVc", strStoryName: "channelStory")
            case 5:
                self.presentViewControllerBasedOnIdentifier("CsellerTaxesVc", strStoryName: "channelStory")
            case 6:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 7:
                self.presentViewControllerBasedOnIdentifier("CaccessoriesVc", strStoryName: "channelStory")
            case 8:
                self.presentViewControllerBasedOnIdentifier("CtaxPackages", strStoryName: "channelStory")
            case 9:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 10:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 11:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 12:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 13:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            default:
                Globalfunc.print(object:"default")
            }


        }
        else if (self.selectTitle == "Bookkeeping Company") {

            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("CdetailBookKeepingVC", strStoryName: "channelStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("CsalesDefaultVC", strStoryName: "channelStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 3:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 4:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 5:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 6:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 7:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            default:
                Globalfunc.print(object:"default")
            }

        }
        else if (self.selectTitle == "Finance Company/Bank") {

            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("CdetailVC", strStoryName: "channelStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "channelStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 3:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 4:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")

            default:
                Globalfunc.print(object:"default")
            }
        }
        else if (self.selectTitle == "Source" || self.selectTitle == "Insurance Agent" || self.selectTitle == "Insurance Company" || self.selectTitle == "Dealer/Wholesaler/Auction" || self.selectTitle == "Repo Agent")
        {
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("CdetailVC", strStoryName: "channelStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("channelNotesVc", strStoryName: "channelStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
        else if (self.selectTitle == "Inventory Buyer" || title == "Document")
        {
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("CdetailVC", strStoryName: "channelStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("CsalesDefaultVC", strStoryName: "channelStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 3:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 4:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 5:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 6:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 7:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 8:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 9:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 10:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 11:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 12:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 13:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")

            case 14:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 15:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 16:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 17:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")

            case 18:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 19:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 20:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            case 21:
                self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
            default:
                Globalfunc.print(object:"default")
            }
        }
    }
}
