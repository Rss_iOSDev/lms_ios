//
//  CaccessoriesVc.swift
//  LMS
//
//  Created by Dr.Mac on 03/05/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CaccessoriesVc: UIViewController {
    @IBOutlet weak var accessoryTbl : UITableView!
    var arrAccessoriesList : NSMutableArray = []
    
    
   override func viewDidLoad() {
   super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.accessoryTbl.isHidden = true
        self.arrAccessoriesList = []
        
        Globalfunc.showLoaderView(view: self.view)
        self.callChannelDetailApi()
        
        
    }
    @IBAction func clickOnBackBtn (_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }

  

}
extension CaccessoriesVc{


        @IBAction func clickOnAddBtn(_ sender : UIButton){
             let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
             let edit = storyboard.instantiateViewController(identifier: "CaccessoriesAddEditVc") as! CaccessoriesAddEditVc
             edit.isOpenFromVc = "add"
             edit.modalPresentationStyle = .fullScreen
             self.present(edit , animated: true ,completion: nil)
        }
    }

extension CaccessoriesVc {
    func callChannelDetailApi(){
        do{
              let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                  let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.addChannel)/accessories?id=\(chnl_id)"
                self.callApi(strUrl : strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    func callApi(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict,error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let response = dict  as? NSDictionary{
                        if let arr = response["data"] as? [NSDictionary]{
                            if arr.count > 0 {
                                for i in 0..<arr.count{
                                let arrDict = arr[i]
                                self.arrAccessoriesList.add(arrDict)
                                }
                                self.accessoryTbl.isHidden = false
                                self.accessoryTbl.delegate = self
                                 self.accessoryTbl.dataSource = self
                                self.accessoryTbl.reloadData()
                            }
                            else{
                                OperationQueue.main.addOperation {
                                    Globalfunc.hideLoaderView(view: self.view)
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
extension CaccessoriesVc : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccessoriesList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = accessoryTbl.dequeueReusableCell(withIdentifier: "accessoriesTblCell") as! accessoriesTblCell
        let dict = arrAccessoriesList[indexPath.row] as! NSDictionary
        if let accessory_display_name = dict["accessory_display_name"] as? String{
            cell.lblName.text = accessory_display_name
        }
        if let accessory_desc = dict["accessory_desc"] as? String{
            cell.lblDesc.text = accessory_desc
        }
    //???
        if let accessory_price = dict["accessory_price"] as? String{
            cell.lblPrice.text = accessory_price
        }
        if let accessory_cost = dict["accessory_cost"] as? String{
            cell.lblCost.text = accessory_cost
        }
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.clickOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.clickOnDel(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc  func clickOnEditBtn(_ sender : UIButton){
        let dict = arrAccessoriesList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! String
    let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
           let edit = storyboard.instantiateViewController(identifier: "CaccessoriesAddEditVc") as! CaccessoriesAddEditVc
           edit.passAccessoryId = _id
           edit.isOpenFromVc = "edit"
           edit.modalPresentationStyle = .fullScreen
           self.present(edit , animated: true ,completion: nil)


    }
    
    @objc func clickOnDel(_ sender : UIButton){
        let dict = arrAccessoriesList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! String
        
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default){ (action:UIAlertAction!) in
            let strUrl = "\(Constant.addChannel)/accessories?id=\(_id)"
            self.deleteFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){
            UIAlertAction in
        }
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController,animated: true,completion: nil)
        
    }
    func deleteFlag(_ strUrl : String){
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary{
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OkAction = UIAlertAction(title: "Ok", style: .default){
                            (action :UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OkAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
class accessoriesTblCell : UITableViewCell{
    @IBOutlet weak var lblName  : UILabel!
    @IBOutlet weak var lblDesc  : UILabel!
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var lblCost  : UILabel!
    
    @IBOutlet weak var btnEdit   : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
}
//http://3.129.150.191:3000/api/channel/accessories?id=1
//{"data":[{"is_deleted":false,"_id":"60900b5fb048096008ac7ba4","accessory_display_name":"ios test","accessory_desc":"test","accessory_price":120,"accessory_cost":120},{"is_deleted":false,"_id":"60900bdeb048096008ac7bb4","accessory_display_name":"test","accessory_desc":"12","accessory_price":123,"accessory_cost":1234}],"success":true,"status":200}
