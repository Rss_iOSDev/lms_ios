//
//  channelAddFeeVc.swift
//  LMS
//
//  Created by Dr.Mac on 27/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CsellerTaxesVc: UIViewController {
    @IBOutlet weak var sellerTaxTbl : UITableView!
    var arrsellerTaxList : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.sellerTaxTbl.isHidden = true
        self.arrsellerTaxList = []
       Globalfunc.showLoaderView(view: self.view)
      self.callChannelDetailApi()
        
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CsellerTaxesVc{
    
    @IBAction func clickOnAddBtn(_ sender : UIButton){
         let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
         let edit = storyboard.instantiateViewController(identifier: "CSellerTaxAddEditVc") as! CSellerTaxAddEditVc
         edit.isOpenFromvc = "add"
         edit.modalPresentationStyle = .fullScreen
         self.present(edit , animated: true ,completion: nil)
    }
}
extension CsellerTaxesVc{
    func callChannelDetailApi(){
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                
                let strUrl = "\(Constant.addChannel)/sellerTaxes?id=\(chnl_id)"
                self.callApi(strUrl: strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    func callApi(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        
                        if let arr = response["data"] as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrsellerTaxList.add(dictA)
                                   
                                }
                                self.sellerTaxTbl.isHidden = false
                                self.sellerTaxTbl.delegate = self
                                self.sellerTaxTbl.dataSource = self
                                self.sellerTaxTbl.reloadData()
                            }
                            else{
                                self.arrsellerTaxList = []
                            }
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension CsellerTaxesVc : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrsellerTaxList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 600
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sellerTaxTbl.dequeueReusableCell(withIdentifier: "sellerTaxtblCell") as! sellerTaxtblCell
        let dict = arrsellerTaxList[indexPath.row] as! NSDictionary
        if let tax_state = dict["tax_state"] as? String{
            cell.lblState.text = tax_state
        }
        if let tax_category = dict["tax_category"] as? String{
                   cell.lblCategory.text = tax_category
               }
        if let tax_desc = dict["tax_desc"] as? String{
                   cell.lblDesc.text = tax_desc
               }
        if let flat_tax_amount = dict["flat_tax_amount"] as? String{
                   cell.lblRate.text = flat_tax_amount
               }
        if let tax_variable = dict["tax_variable"] as? String{
                   cell.lblTaxVariable.text = tax_variable
               }
        if let tax_fixed = dict["tax_fixed"] as? String{
                   cell.lblFixedRate.text = tax_fixed
               }
        if let tax_beg_range = dict["tax_beg_range"] as? String{
                   cell.lblBegRange.text = tax_beg_range
               }
        if let tax_end_range = dict["tax_end_range"] as? String{
                          cell.lblEndRange.text = tax_end_range
                      }
        if let can_edit_time_sale = dict["can_edit_time_sale"] as? String{
            cell.lblEditAtSale.text = can_edit_time_sale
        }
        if let can_edit_time_sale = dict["can_edit_time_sale"] as? String{
                   cell.lblEditAtSale.text = can_edit_time_sale
               }
        
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.clickOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
               cell.btnDelete.addTarget(self, action: #selector(self.clickOnDeleteBtn), for: .touchUpInside)
        return cell
       
    }
    @objc func clickOnEditBtn(_ sender : UIButton){
        let dict = arrsellerTaxList[sender.tag] as! NSDictionary
       let _id = dict["_id"] as! String
        let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
        let edit = storyboard.instantiateViewController(identifier: "CSellerTaxAddEditVc") as! CSellerTaxAddEditVc
        edit.passSellerTaxId = _id
       edit.isOpenFromvc = "edit"
        edit.modalPresentationStyle = .fullScreen
        self.present(edit , animated: true ,completion: nil)
        
    }

  @objc func clickOnDeleteBtn(_ sender: UIButton)
  {
      let dict = arrsellerTaxList[sender.tag] as! NSDictionary
      let _id = dict["_id"] as! String

      let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
      let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
          let strUrl = "\(Constant.addChannel)/sellerTaxes?id=\(_id)"
          self.deletFlag(strUrl)
      }
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
          UIAlertAction in
      }
    alertController.addAction(OKAction)
           alertController.addAction(cancelAction)
           self.present(alertController, animated: true, completion:nil)
}
    func deletFlag(_ strUrl: String)
       {
           BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
               if(error == ""){
                   OperationQueue.main.addOperation {
                       Globalfunc.hideLoaderView(view: self.view)
                       if let arr = dict as? NSDictionary {
                           let msg = arr["msg"] as! String
                           let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                           let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                               self.viewWillAppear(true)
                           }
                           alertController.addAction(OKAction)
                           self.present(alertController, animated: true, completion:nil)
                       }
                   }
               }
               else
               {
                   OperationQueue.main.addOperation {
                       Globalfunc.hideLoaderView(view: self.view)
                       Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                   }
               }
           }
       }
       
}
class sellerTaxtblCell : UITableViewCell{
    @IBOutlet weak var lblState :        UILabel!
    @IBOutlet weak var lblCategory :     UILabel!
    @IBOutlet weak var lblDesc :         UILabel!
    @IBOutlet weak var lblRate :         UILabel!
    @IBOutlet weak var lblTaxVariable :  UILabel!
    @IBOutlet weak var lblFixedRate :    UILabel!
    @IBOutlet weak var lblBegRange :     UILabel!
    @IBOutlet weak var lblEndRange :     UILabel!
    @IBOutlet weak var lblEditAtSale :   UILabel!
    @IBOutlet weak var lblDeleteTax :    UILabel!
    @IBOutlet weak var btnEdit :        UIButton!
    @IBOutlet weak var btnDelete:       UIButton!
    
}





