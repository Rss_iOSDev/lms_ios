//
//  channelSellerFeeVc.swift
//  LMS
//
//  Created by Dr.Mac on 27/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class channelSellerFeeVc: UIViewController {
    @IBOutlet weak var sellerFeeTbl : UITableView!
    var arrSellerFeeList : NSMutableArray = []
    
    var channel_id : Int?
    
    
    override func viewDidLoad() {
    super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.sellerFeeTbl.isHidden = true
        self.arrSellerFeeList = []
        
        Globalfunc.showLoaderView(view: self.view)
        self.callChannelDetailApi()
        
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
       {
           self.dismiss(animated: true, completion: nil)
       }
    
}
extension channelSellerFeeVc{
    
    @IBAction func clickOnAddBtn(_ sender : UIButton){
         let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
         let edit = storyboard.instantiateViewController(identifier: "CsellerEditFeeVc") as! CsellerEditFeeVc
         edit.isOpenFromvc = "add"
         edit.modalPresentationStyle = .fullScreen
         self.present(edit , animated: true ,completion: nil)
    }
}



extension channelSellerFeeVc{
    func callChannelDetailApi(){
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                
                let strUrl = "\(Constant.addChannel)/sellerFees?id=\(chnl_id)"
                self.callApi(strUrl: strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApi(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        
                        if let arr = response["data"] as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrSellerFeeList.add(dictA)
                                   
                                }
                                self.sellerFeeTbl.isHidden = false
                                self.sellerFeeTbl.delegate = self
                                self.sellerFeeTbl.dataSource = self
                                self.sellerFeeTbl.reloadData()
                            }
                            else{
                                self.arrSellerFeeList = []
                            }
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension channelSellerFeeVc : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSellerFeeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sellerFeeTbl.dequeueReusableCell(withIdentifier: "sellerTblCell") as! sellerTblCell
        let dict = arrSellerFeeList[indexPath.row] as! NSDictionary
        if let state = dict["state"] as? String{
            cell.lblState.text = state
        }
        if let category = dict["category"] as? String{
            cell.lblCategory.text = category
        }
        if let desc = dict["desc"] as? String{
            cell.lblDesc.text = desc
        }
        
        // amount label is of int type
        if let amount = dict["amount"] as? Int{
            cell.lblAmt.text = "\(amount)"
        }
        if let fee_type = dict["fee_type"] as? String{
            cell.lblFeeType.text = fee_type
        }
        
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.clickOnEditCellBtn(_:)), for: .touchUpInside)
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.clickOnDeleteBtn(_:)), for: .touchUpInside)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func clickOnEditCellBtn(_ sender: UIButton)
    {
       let dict = arrSellerFeeList[sender.tag] as! NSDictionary
       let _id = dict["_id"] as! String
        let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
        let edit = storyboard.instantiateViewController(identifier: "CsellerEditFeeVc") as! CsellerEditFeeVc
        edit.passSellerId = _id
        edit.isOpenFromvc = "edit"
        edit.modalPresentationStyle = .fullScreen
        self.present(edit , animated: true ,completion: nil)
    }
    
    @objc func clickOnDeleteBtn(_ sender: UIButton)
    {
        let dict = arrSellerFeeList[sender.tag] as! NSDictionary
        let _id = dict["_id"] as! String

        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.addChannel)/sellerFees?id=\(_id)"
            self.deletFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}

class sellerTblCell : UITableViewCell{
    @IBOutlet weak var lblState :     UILabel!
    @IBOutlet weak var lblCategory :  UILabel!
    @IBOutlet weak var lblDesc :      UILabel!
    @IBOutlet weak var lblAmt :       UILabel!
    @IBOutlet weak var lblFeeType :   UILabel!
    @IBOutlet weak var lblDeleteFee : UILabel!
    @IBOutlet weak var btnEdit :      UIButton!
    @IBOutlet weak var btnDelete :      UIButton!

}

