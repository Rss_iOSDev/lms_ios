//
//  channelBuyerDefVc.swift
//  LMS
//
//  Created by Dr.Mac on 26/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class channelBuyerDefVc: UIViewController {
    @IBOutlet weak var txtBuyrGuideOpt: UITextField!
    @IBOutlet weak var txtIsContractAvailable : UITextField!
    @IBOutlet weak var txtWarrantyType : UITextField!
    @IBOutlet weak var txtPercntOfLabour : UITextField!
    @IBOutlet weak var txtPercntOfParts : UITextField!
    
    var pvbuyrGuideOpt : UIPickerView!
    var pvIsContractAvailable : UIPickerView!
    var pvWarrantyType : UIPickerView!
    
    var arrUrl = [Constant.buyerGuide,Constant.addChannel]
    
//    var arrBuyerGuide = [NSDictionary]()
    var arrBuyerGuide = ["AS-IS","Warranty"]
    var arrIsServiceConAvailable = [ "Yes","No"]
    var arrWarrantyType = ["Full Warranty","Limited Warranty"]
     var channel_id : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.callGetChannelDetailApi( )

       
    }
    @IBAction func clickOnbackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    


}



extension channelBuyerDefVc {
    func callGetChannelDetailApi(){
        do{
            let decoded = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.buyerGuide)/id?id=\(chnl_id)"
                self.callApi(strUrl: strUrl)
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    func callApi(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if (error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let responseDict = dict as? NSDictionary{
                        if let data = responseDict["data"] as? NSDictionary{
                            if let _id = data["_id"] as? Int{
                                self.channel_id = _id
                            }
                       
                            
                            if let buyers_guide_option = data["buyers_guide_option"] as? String{
                                self.txtBuyrGuideOpt.text = buyers_guide_option
                            }
                            if let is_service_contract_available = data["is_service_contract_available"] as? String{
                                self.txtIsContractAvailable.text = is_service_contract_available
                            }
                            if let of_the_labor = data["of_the_labor"] as? String{
                                self.txtPercntOfLabour.text = of_the_labor
                            }
                            if let of_the_parts = data["of_the_parts"] as? String{
                                self.txtPercntOfParts.text = of_the_parts
                            }
                            if let warranty_type = data["warranty_type"] as? String{
                                self.txtWarrantyType.text = warranty_type
                            }
                            
                            
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension channelBuyerDefVc: UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pvbuyrGuideOpt){
            return self.arrBuyerGuide.count
        }
        else if(pickerView == pvIsContractAvailable){
            return self.arrIsServiceConAvailable.count
        }
        else if(pickerView == pvWarrantyType){
            return self.arrWarrantyType.count
        }
        return 0
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pvbuyrGuideOpt{
            let title = self.arrBuyerGuide[row]
            return title
        }
        else if (pickerView == pvIsContractAvailable){
            let title =  self.arrIsServiceConAvailable[row]
            return title
            
        }
        
        else if (pickerView == pvWarrantyType){
            let title = self.arrWarrantyType[row]
            return title
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == pvbuyrGuideOpt){
            let title = self.arrBuyerGuide[row]
            self.txtBuyrGuideOpt.text = title
        }
        else if(pickerView == pvIsContractAvailable){
            let title = self.arrIsServiceConAvailable[row]
            self.txtIsContractAvailable.text = title
        }
        else if (pickerView == pvWarrantyType){
            let title = self.arrWarrantyType[row]
            self.txtWarrantyType.text = title
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtBuyrGuideOpt){
            self.pickUp(txtBuyrGuideOpt)
        }
        else if(textField == pvIsContractAvailable){
            self.pickUp(txtIsContractAvailable)
        }
        else if(textField == pvWarrantyType){
            self.pickUp(txtWarrantyType)
        }
    }
    func pickUp(_ textField: UITextField){
        if (textField == self.txtBuyrGuideOpt){
            self.pvbuyrGuideOpt = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvbuyrGuideOpt.delegate = self
            self.pvbuyrGuideOpt.dataSource = self
            self.pvbuyrGuideOpt.backgroundColor = UIColor.white
            textField.inputView = self.pvbuyrGuideOpt
            
        }
        
        else if(textField == txtIsContractAvailable){
            self.pvIsContractAvailable = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvIsContractAvailable.delegate = self
            self.pvIsContractAvailable.dataSource = self
            self.pvIsContractAvailable.backgroundColor = UIColor.white
            textField.inputView = self.pvIsContractAvailable
            
        }
        
        else if(textField == txtWarrantyType){
            self.pvWarrantyType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvWarrantyType.delegate = self
            self.pvWarrantyType.dataSource = self
            self.pvWarrantyType.backgroundColor = UIColor.white
            textField.inputView = self.pvWarrantyType
            
        }
        
        
        let toolBar = UIToolbar()
               toolBar.barStyle = .default
               toolBar.isTranslucent = true
               toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
               toolBar.sizeToFit()
               
               // Adding Button ToolBar
               let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
               let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
               let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
               toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
               toolBar.isUserInteractionEnabled = true
               textField.inputAccessoryView = toolBar
               
    }
    
    @objc func doneClick( ){
        txtBuyrGuideOpt.resignFirstResponder()
        txtIsContractAvailable.resignFirstResponder()
        txtWarrantyType.resignFirstResponder()
    }
    @objc func cancelClick( ){
        txtBuyrGuideOpt.resignFirstResponder()
        txtIsContractAvailable.resignFirstResponder()
        txtWarrantyType.resignFirstResponder()
    }
    
    
    
}

extension channelBuyerDefVc{
    @IBAction func clickOnSaveBtn(_ sender :  UIButton){
        if (txtBuyrGuideOpt.text == "" || txtBuyrGuideOpt.text?.count == 0 || txtBuyrGuideOpt.text == nil){
            txtBuyrGuideOpt.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: " Buyer guide option should not be left blank")
        }
        else if(txtIsContractAvailable.text == "" || txtIsContractAvailable.text?.count == 0 || txtBuyrGuideOpt.text == nil){
            txtIsContractAvailable.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: " Is contract available should not be left blank")
            
        }
         else if(txtWarrantyType.text == "" || txtWarrantyType.text?.count == 0 || txtWarrantyType.text == nil){
             txtWarrantyType.shake()
             Globalfunc.showToastWithMsg(view: self.view, str: " Is contract available should not be left blank")
             
         }
         else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
             Globalfunc.showLoaderView(view: self.view)
             let params = [
                
                "buyers_guide_option": self.txtBuyrGuideOpt.text!,
                "id": self.channel_id!,
                "is_service_contract_available": self.txtIsContractAvailable.text!,
                "of_the_labor": self.txtPercntOfLabour.text!,
                "of_the_parts": self.txtPercntOfParts.text!,
            "warranty_type": self.txtWarrantyType.text!] as [String:Any]
                
             Globalfunc.print(object:params)
             self.callUpdateApi(param: params)
         }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
     func callUpdateApi(param: [String : Any]){
        let url = "\(Constant.buyerGuide)"
        BaseApi.onResponsePutWithToken(url: url, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
//http://3.129.150.191:3000/api/channel/buyersGuideDefaults/id?id=1
//http://3.129.150.191:3000/api/channel/id?id=1
