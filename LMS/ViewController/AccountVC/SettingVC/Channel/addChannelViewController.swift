//
//  addChannelViewController.swift
//  LMS
//
//  Created by Apple on 26/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class addChannelViewController: UIViewController {
    
    @IBOutlet weak var txtChanlTyp : UITextField!
    @IBOutlet weak var txtDescripname : UITextField!
    @IBOutlet weak var txtFedId : UITextField!
    @IBOutlet weak var txtLegalName : UITextField!
    @IBOutlet weak var txtAddress1 : UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtZip : UITextField!
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var txtState : UITextField!
    @IBOutlet weak var txtCounty : UITextField!
    @IBOutlet weak var txtCountry : UITextField!
    @IBOutlet weak var txtcontactPerson : UITextField!
    @IBOutlet weak var txtPhn1 : UITextField!
    @IBOutlet weak var txtPhn2 : UITextField!
    @IBOutlet weak var txtFax1 : UITextField!
    @IBOutlet weak var txtFax2 : UITextField!
    @IBOutlet weak var txtwebsite : UITextField!
    @IBOutlet weak var txtemail : UITextField!
    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var txtAssign : UITextField!
    @IBOutlet weak var txtAcountngClas : UITextField!
    
    @IBOutlet weak var lblAssgBook : UILabel!
    @IBOutlet weak var viewAssgBook : UIView!
    
    var arrUrl = [Constant.channeltype,Constant.accounting,Constant.channlId]
    
    var pickerViewCT: UIPickerView!
    var pickerViewAcoountng: UIPickerView!
    var pickerViewAssign: UIPickerView!
    
    var arrChanlTyp = [NSDictionary]()
    var arrAccountng = [NSDictionary]()
    var arrChnlId = [NSDictionary]()
    
    var chlTyp_id = ""
    var accnt_id = ""
    var assg_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAssgBook.isHidden = true
        self.viewAssgBook.isHidden = true
        
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerData()
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension addChannelViewController{
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        if(txtChanlTyp.text == "" || txtChanlTyp.text?.count == 0 || txtChanlTyp.text == nil){
            txtChanlTyp.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Channel Type.")
        }
        else if(txtDescripname.text == "" || txtDescripname.text?.count == 0 || txtDescripname.text == nil){
            txtDescripname.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Descriptive Name should not left blank.")
        }
        else if(txtLegalName.text == "" || txtLegalName.text?.count == 0 || txtLegalName.text == nil){
            txtLegalName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Legal Name should not left blank.")
        }
        else if(txtAddress1.text == "" || txtAddress1.text?.count == 0 || txtAddress1.text == nil){
            txtAddress1.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Address should not left blank.")
        }
        else if(txtZip.text == "" || txtZip.text?.count == 0 || txtZip.text == nil){
            txtZip.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Zip should not left blank.")
        }
        else if(txtCity.text == "" || txtCity.text?.count == 0 || txtCity.text == nil){
            txtCity.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "City should not left blank.")
        }
        else if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            txtState.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "State should not left blank.")
        }
        else if(txtCountry.text == "" || txtCountry.text?.count == 0 || txtCountry.text == nil){
            txtCountry.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Country should not left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            let params = [
                "accounting_class": self.accnt_id,
                "address1": self.txtAddress1.text!,
                "address2": self.txtAddress2.text!,
                "bk_company": self.assg_id,
                "channeltype": self.chlTyp_id,
                "city": self.txtCity.text!,
                "contact_person": self.txtcontactPerson.text!,
                "country": self.txtCountry.text!,
                "county": self.txtCounty.text!,
                "email": self.txtemail.text!,
                "fax1": self.txtFax1.text!,
                "fax2":self.txtFax2.text!,
                "fedid": self.txtFedId.text!,
                "insid": user?.institute_id ?? "",
                "legalname": self.txtLegalName.text!,
                "phone1": self.txtPhn1.text!,
                "phone2": self.txtPhn2.text!,
                "state": self.txtState.text!,
                "text_name": self.txtName.text!,
                "title": self.txtDescripname.text!,
                "userid": user?._id ?? "",
                "website": self.txtwebsite.text!,
                "zip": self.txtZip.text!] as [String : Any]
            self.sendDatatoPostApi(param: params)
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.addChannel, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}


//MARK: - Testfield delegate methods

extension addChannelViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewCT){
            return self.arrChanlTyp.count
        }
        else if(pickerView == pickerViewAcoountng){
            return self.arrAccountng.count
        }
        else if(pickerView == pickerViewAssign){
            return self.arrChnlId.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewCT){
            let dict = self.arrChanlTyp[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewAcoountng){
            let dict = self.arrAccountng[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewAssign){
            let dict = self.arrChnlId[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewCT){
            let dict = self.arrChanlTyp[row]
            let strTitle = dict["title"] as! String
            self.txtChanlTyp.text = strTitle
            let _id = dict["_id"] as! Int
            self.chlTyp_id = "\(_id)"
            if(_id == 1){
                self.lblAssgBook.isHidden = false
                self.viewAssgBook.isHidden = false
            }
            else{
                self.lblAssgBook.isHidden = true
                self.viewAssgBook.isHidden = true
            }
        }
        else if(pickerView == pickerViewAcoountng){
            let dict = self.arrAccountng[row]
            let strTitle = dict["title"] as! String
            self.txtAcountngClas.text = strTitle
            let _id = dict["_id"] as! Int
            self.accnt_id = "\(_id)"
        }
        else if(pickerView == pickerViewAssign){
            let dict = self.arrChnlId[row]
            let strTitle = dict["title"] as! String
            self.txtAssign.text = strTitle
            let _id = dict["_id"] as! Int
            self.assg_id = "\(_id)"
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtChanlTyp){
            self.pickUp(txtChanlTyp)
        }
        else if(textField == self.txtAcountngClas){
            self.pickUp(txtAcountngClas)
        }
        else if(textField == self.txtAssign){
            self.pickUp(txtAssign)
        }
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtChanlTyp){
            self.pickerViewCT = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewCT.delegate = self
            self.pickerViewCT.dataSource = self
            self.pickerViewCT.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewCT
        }
            
        else if(textField == self.txtAcountngClas){
            self.pickerViewAcoountng = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAcoountng.delegate = self
            self.pickerViewAcoountng.dataSource = self
            self.pickerViewAcoountng.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAcoountng
        }
            
        else if(textField == self.txtAssign){
            self.pickerViewAssign = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAssign.delegate = self
            self.pickerViewAssign.dataSource = self
            self.pickerViewAssign.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAssign
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtChanlTyp.resignFirstResponder()
        txtAssign.resignFirstResponder()
        txtAcountngClas.resignFirstResponder()
    }
    
    @objc func cancelClick(){
        txtChanlTyp.resignFirstResponder()
        txtAssign.resignFirstResponder()
        txtAcountngClas.resignFirstResponder()
    }
}

extension addChannelViewController {
    func callAllPickerData()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        
                        Globalfunc.hideLoaderView(view: self.view)
                        if(obj == Constant.channeltype){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrChanlTyp.append(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        if(obj == Constant.accounting){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrAccountng.append(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        if(obj == Constant.channlId){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrChnlId.append(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            if let msg = arr["msg"] as? String{
                                if(msg == "Your token has expired."){
                                    self.viewWillAppear(true)
                                }
                                else{
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                            }
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}
