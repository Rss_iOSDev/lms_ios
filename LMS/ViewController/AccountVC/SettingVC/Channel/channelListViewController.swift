//
//  channelListViewController.swift
//  LMS
//
//  Created by Apple on 18/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class channelListViewController: baseViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var tblChannelList : UITableView!
    
    @IBOutlet weak var txtChnlName : UITextField!
    @IBOutlet weak var txtChnlTyp : UITextField!
    
    @IBOutlet weak var btnActive : UIButton!
    @IBOutlet weak var btnDel : UIButton!
    
    @IBOutlet weak var txtPagination: UITextField!
    
    @IBOutlet weak var scrollViewPages: UIScrollView!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lblResultfound: UILabel!
    
    var arrChannelTyp : [NSDictionary] = []
    var pickerChanTyp : UIPickerView!
    
    var selectedChnlTypId = [String]()
    var selectedChnlTypTitle = [String]()
    
    var arrPAginationNo = ["10","25","50","75","100","200","250"]
    var pickerViewPages: UIPickerView!
    
    
    var arrchannelData = NSMutableArray()
    var isPickerselect = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        self.tblChannelList.tableFooterView = UIView()
        self.getChannlTypApi()
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        self.btnActive.isSelected = true
        self.btnDel.isSelected = false
        
        self.txtPagination.text = self.arrPAginationNo[0]
        
        
        
        
        Globalfunc.showLoaderView(view: self.view)
        let param = ["channelSearch":self.txtChnlName.text!,
                     "channelStatus": false,
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": 0,
                     "defaultRecordsPage": 10] as NSDictionary
        self.callUserListApiwithstatus(param: param)
    }
}

extension channelListViewController {
    func getChannlTypApi()
    {
        BaseApi.callApiRequestForGet(url: Constant.channeltype) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i] as! NSMutableDictionary
                                    self.arrChannelTyp.append(dictA)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension channelListViewController {
    
    @IBAction func clikOnCheckBox(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
        else if(sender.tag == 20){
            if sender.isSelected == true {
                sender.isSelected = false
            }else {
                sender.isSelected = true
            }
        }
    }
    
    @IBAction func clickONPlusBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("addChannelViewController", strStoryName: "SettingStory")
    }
    
    @IBAction func clickONSearchBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        self.arrchannelData = []
        var param = NSDictionary()
        
        if(btnActive.isSelected == false && btnDel.isSelected == false){
            
            param = ["channelSearch": self.txtChnlName.text!,
                     "channelStatus": "",
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": 0,
                     "defaultRecordsPage": 10] as NSDictionary
            
        }
        else if(btnActive.isSelected == true && btnDel.isSelected == true){
            
            param = ["channelSearch": self.txtChnlName.text!,
                     "channelStatus": "",
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": 0,
                     "defaultRecordsPage": 10] as NSDictionary
            
        }
        else if(btnActive.isSelected == true && btnDel.isSelected == false){
            
            param = ["channelSearch": self.txtChnlName.text!,
                     "channelStatus": false,
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": 0,
                     "defaultRecordsPage": 10] as NSDictionary
        }
        else if(btnActive.isSelected == false && btnDel.isSelected == true){
            param = ["channelSearch": self.txtChnlName.text!,
                     "channelStatus": true,
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": 0,
                     "defaultRecordsPage": 10] as NSDictionary
        }
        self.callUserListApiwithstatus(param: param)
    }
}

extension channelListViewController {
    
    func callUserListApiwithstatus(param: NSDictionary){
        
        BaseApi.onResponsePostWithToken(url: Constant.channels, controller: self, parms: param) { (dict, err) in
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                
                if let response = dict as? NSDictionary {
                    
                    if let dataDict = response["data"] as? NSDictionary {
                        
                        let currentPageNumber = dataDict["currentPageNumber"] as! Int
                        let defaultRecordsPage = dataDict["defaultRecordsPage"] as! Int
                        
                        let record = dataDict["totalrecord"] as! Int
                        self.lblResultfound.text = "\(record) RESULTS FOUND"
                        
                        let cpNo = CGFloat(currentPageNumber)
                        let pagSize = CGFloat(defaultRecordsPage)
                        let totalR = CGFloat(record)
                        
                        let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
                        let pagecount = Int(pageNo)
                        
                        let buttonPadding:CGFloat = 10
                        var xOffset:CGFloat = 10
                        
                        for i in 0..<pagecount {
                            if (i == 0){
                                self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
                            }
                            let button = UIButton()
                            button.tag = i+1
                            button.setTitle("\(i+1)", for: .normal)
                            button.layer.borderWidth = 1
                            button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
                            button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
                            button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
                            button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
                            button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
                            xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
                            self.scrollViewPages.addSubview(button)
                        }
                        self.scrollWidth.constant = xOffset
                        self.scrollViewPages.contentSize = CGSize(width: xOffset, height: self.scrollViewPages.frame.height)
                        
                        
                        if let channel = dataDict["channel"] as? [NSDictionary]
                        {
                            if(channel.count > 0){
                                for i in 0..<channel.count{
                                    let dictA = channel[i]
                                    self.arrchannelData.add(dictA)
                                }
                                self.tblChannelList.reloadData()
                            }
                            else{
                                self.arrchannelData = []
                                self.tblChannelList.reloadData()
                            }
                        }
                        
                        
                    }
                    
                    
                    
                }
            }
        }
    }
    
    @objc func btnClick(_ sender : UIButton){
        sender.pulsate()
        
        let pagNo = sender.titleLabel?.text!
        let pN = Int(pagNo!)
        self.arrchannelData = []
        Globalfunc.showLoaderView(view: self.view)
        let pagecount = Int(self.txtPagination.text!)
        let param = ["channelSearch": self.txtChnlName.text!,
                     "channelStatus": false,
                     "channelType": selectedChnlTypId,
                     "currentPageNumber": pN!,
                     "defaultRecordsPage": pagecount!] as NSDictionary
        self.callUserListApiwithstatus(param: param)
    }
    
}
extension channelListViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrchannelData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblChannelList.dequeueReusableCell(withIdentifier: "tblChannleCell") as! tblChannleCell
        let dict = arrchannelData[indexPath.row] as! NSDictionary
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.lblstatus.text = "Active"
            }
            else{
                cell.lblstatus.text = "Deleted"
            }
        }
        
        if let _id = dict["_id"] as? Int
        {
            cell.lblId.text = "\(_id)"
        }
        
        if let channeltype = dict["channeltype"] as? NSDictionary
        {
            let title = channeltype["title"] as! String
            cell.lblChnlTyp.text = title
        }
        
        
        let title = dict["title"] as! String
        cell.lblName.text = title
        
        let legalname = dict["legalname"] as! String
        cell.lblDescription.text = legalname
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrchannelData[indexPath.row] as! NSDictionary
        do {
            if #available(iOS 11.0, *) {
                let myData = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
                userDef.set(myData, forKey: "channelDetail")
            } else {
                let myData = NSKeyedArchiver.archivedData(withRootObject: dict)
                userDef.set(myData, forKey: "channelDetail")
            }
        } catch {
            Globalfunc.print(object: "Couldn't write file")
        }
        self.presentViewControllerBasedOnIdentifier("channelDetailViewController", strStoryName: "channelStory")
    }
}

class tblChannleCell : UITableViewCell
{
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblChnlTyp : UILabel!
    @IBOutlet weak var lblId : UILabel!
    @IBOutlet weak var lblstatus : UILabel!
}


extension channelListViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pickerChanTyp{
            return self.arrChannelTyp.count
        }
        else if(pickerView == pickerViewPages){
            return self.arrPAginationNo.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSMutableAttributedString()
        if pickerView == pickerChanTyp{
            
            let dict = arrChannelTyp[row]
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in  selectedChnlTypId{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewPages){
            let strTitle = self.arrPAginationNo[row]
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
        }
        
        
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView == pickerChanTyp){
            let dict = arrChannelTyp[row] as NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            if selectedChnlTypId.contains("\(_id)") {
                var index = 0
                for item in selectedChnlTypId {
                    if item == "\(_id)" {
                        selectedChnlTypId.remove(at: index)
                        selectedChnlTypTitle.remove(at: index)
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedChnlTypId.append("\(_id)")
                selectedChnlTypTitle.append(strTitle)
            }
            
            
            self.txtChnlTyp.text = selectedChnlTypTitle.joined(separator: ", ")    //selectedArray is array has selected item
            
            self.pickerChanTyp.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewPages){
            self.txtPagination.text = self.arrPAginationNo[row]
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtChnlTyp){
            self.pickUp(txtChnlTyp)
        }
        else if(textField == self.txtPagination){
            self.pickUp(txtPagination)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtChnlTyp){
            self.pickerChanTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerChanTyp.delegate = self
            self.pickerChanTyp.dataSource = self
            self.pickerChanTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerChanTyp
            isPickerselect = ""
        }
        else if(textField == self.txtPagination){
            self.pickerViewPages = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPages.delegate = self
            self.pickerViewPages.dataSource = self
            self.pickerViewPages.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPages
            isPickerselect = "page"
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtChnlTyp.resignFirstResponder()
        if(isPickerselect == "page"){
            txtPagination.resignFirstResponder()
            self.arrchannelData = []
            Globalfunc.showLoaderView(view: self.view)
            let pagecount = Int(self.txtPagination.text!)
            let param = ["channelSearch": self.txtChnlName.text!,
                         "channelStatus": false,
                         "channelType": selectedChnlTypId,
                         "currentPageNumber": 1,
                         "defaultRecordsPage": pagecount!] as NSDictionary
            self.callUserListApiwithstatus(param: param)
        }
    }
    
    @objc func cancelClick() {
        txtChnlTyp.resignFirstResponder()
        txtPagination.resignFirstResponder()
    }
}

extension UIViewController {
    
    func getPager(totalItems: CGFloat, currentPage: CGFloat, pageSize: CGFloat) -> CGFloat{
        // calculate total pages
        
        var cPage = currentPage
        
        let totalPages = ceil(totalItems / pageSize)
        
        // ensure current page isn't out of range
        if (currentPage < 1) {
            cPage = 1
        } else if (currentPage > totalPages) {
            cPage = totalPages
        }
        
        var startPage : CGFloat
        var endPage : CGFloat
        
        
        if (totalPages <= 10) {
            // console.log("totalPages");
            // console.log(totalPages);
            // less than 10 total pages so show all
            if (cPage == 1) {
                if(totalPages<6){
                    startPage = 1;
                    endPage = totalPages
                }else{
                    startPage = 1;
                    endPage = 6;
                }
                
            } else if (cPage == 2) {
                if(totalPages<6){
                    startPage = 1;
                    endPage = totalPages
                }else{
                    startPage = 1;
                    endPage = 7;
                }
            } else if (cPage == 3) {
                if(totalPages<6){
                    startPage = 1;
                    endPage = totalPages
                }else{
                    startPage = 1;
                    endPage = 8;
                }
            } else {
                startPage = 1;
                endPage = totalPages
            }
        } else {
            // more than 10 total pages so calculate start and end pages
            if (cPage == 1) {
                startPage = 1;
                endPage = totalPages;
            } else if (cPage == 2) {
                startPage = 1;
                endPage = totalPages;
            } else if (cPage == 3) {
                startPage = 1;
                endPage = totalPages;
            } else if (cPage <= 6) {
                startPage = 1;
                endPage = totalPages;
            } else if (cPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = cPage - 5;
                endPage = cPage + 4;
            }
        }
        let pages = (endPage + 1) - startPage
        return pages
    }
}
