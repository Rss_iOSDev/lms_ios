//
//  CdetailBookKeepingVC.swift
//  LMS
//
//  Created by Dr.Mac on 20/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CdetailBookKeepingVC: UIViewController {
    
    
    @IBOutlet weak var txtDescripname : UITextField!
    @IBOutlet weak var txtFedId : UITextField!
    @IBOutlet weak var txtLegalName : UITextField!
    @IBOutlet weak var txtAddress1 : UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtZip : UITextField!
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var txtState : UITextField!
    @IBOutlet weak var txtCounty : UITextField!
    @IBOutlet weak var txtCountry : UITextField!
    @IBOutlet weak var txtcontactPerson : UITextField!
    @IBOutlet weak var txtPhn1 : UITextField!
    @IBOutlet weak var txtPhn2 : UITextField!
    @IBOutlet weak var txtFax1 : UITextField!
    @IBOutlet weak var txtFax2 : UITextField!
    @IBOutlet weak var txtwebsite : UITextField!
    @IBOutlet weak var txtemail : UITextField!
    @IBOutlet weak var txtName : UITextField!
//    @IBOutlet weak var txtAccountingVersion : UITextField!
    @IBOutlet weak var txtAcountngClas : UITextField!
    @IBOutlet weak var txtDealer : UITextField!
//    @IBOutlet weak var txtAccountingServices:  UITextField!
    
     var arrCountry = NSMutableArray()
     var countryId = ""
    
     var arrAllState = NSMutableArray()
    var arrSelectState = NSMutableArray()
    
    var arrState = NSMutableArray()
    var stateId = ""
    
    var pickerViewAcoountng: UIPickerView!
    var pickerViewAssign: UIPickerView!
    var pvCountry:UIPickerView!
   
    
    var pvState: UIPickerView!
    
    
    var arrChanlId = [NSDictionary]()
    var arrAccountng = [NSDictionary]()
    
    var arrUrl = [Constant.accounting,Constant.channlId]
    
    var accnt_id = ""
    var assg_id : Int?
    
    var channel_id : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerData()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
extension CdetailBookKeepingVC{
    
    func callAllPickerData()
    {
        let group = DispatchGroup()
        self.arrUrl.forEach { obj in
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        
                        
                        if(obj == Constant.state){
                            
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrAllState.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                            
                        }
                          if(obj == Constant.country){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrCountry.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                        }
                        if(obj == Constant.accounting){
                            if let arr = dict as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrAccountng.append(dictA)
                                    }
                                }
                            }
                        }
                        if(obj == Constant.channlId){
                            if let arr = dict as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrChanlId.append(dictA)
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.callGetChannelDetailApi()
                        })
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}
extension CdetailBookKeepingVC{
    func callParticularSate()
       {
           let dict = arrCountry[0] as! NSDictionary
           let _id = dict["_id"] as! Int
           self.countryId = "\(_id)"
           let strGetUrl = "\(Constant.state)/country_id?id=\(_id)"
           self.callApi(strUrl: strGetUrl, strTyp: "state")
           DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
               self.callGetChannelDetailApi()
           }
       }
//
    func callGetChannelDetailApi()
    {
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.addChannel)/detail/id?id=\(chnl_id)"
                self.callApi(strUrl: strUrl, strTyp: "all")
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApi(strUrl : String,strTyp: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                
                if(strTyp == "state"){
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                    self.arrSelectState.add(dictA)
                                    }
                                }
                            }
                        }
                        
                    }
                }
                else if(strTyp == "all"){
                    OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        
                        if let data = responseDict["data"] as? NSDictionary{
                        if let _id = data["_id"] as? Int{
                            self.channel_id = _id
                        }
                        if let address1 = data["address1"] as? String{
                            self.txtAddress1.text = address1
                        }
                        
                        if let address2 = data["address2"] as? String{
                            self.txtAddress2.text = address2
                        }
                        if let city = data["city"] as? String{
                            self.txtCity.text = city
                        }
                        
                        if let contact_person = data["contact_person"] as? String{
                            self.txtcontactPerson.text = contact_person
                        }
                        
                        if let country = data["country"] as? String{
                            self.txtCountry.text = country
                        }
                        
                        if let email = data["email"] as? String{
                            self.txtemail.text = email
                        }
                        
                        if let fax1 = data["fax1"] as? String{
                            self.txtFax1.text = fax1
                        }
                        
                        if let fax2 = data["fax2"] as? String{
                            self.txtFax1.text = fax2
                        }
                        
                        if let fedid = data["fedid"] as? String{
                            self.txtFedId.text = fedid
                        }
                        
                        if let legalname = data["legalname"] as? String{
                            self.txtLegalName.text = legalname
                        }
                        
                        if let phone1 = data["phone1"] as? String{
                            self.txtPhn1.text = phone1
                        }
                        
                        if let phone2 = data["phone2"] as? String{
                            self.txtPhn2.text = phone2
                        }
                        
                        if let state = data["state"] as? String{
                            self.txtState.text = state
                        }
                        
                        if let text_name = data["text_name"] as? String{
                            self.txtName.text = text_name
                        }
                        
                        if let title = data["title"] as? String{
                            self.txtDescripname.text = title
                        }
                        
                        if let website = data["website"] as? String{
                            self.txtwebsite.text = website
                        }
                        
                        if let zip = data["zip"] as? String{
                            self.txtZip.text = zip
                        }
                        
//                        if let location_code = data["location_code"] as? String{
//                            self.txtLocationCode.text = location_code
//                        }
                        
                        if let accounting_class = data["accounting_class"] as? String{
                            for i in 0..<self.arrAccountng.count{
                                let dict = self.arrAccountng[i]
                                let _id = dict["_id"] as! Int
                                if(accounting_class == "\(_id)"){
                                    let strTitle = dict["title"] as! String
                                    self.txtAcountngClas.text = strTitle
                                    self.accnt_id = "\(_id)"
                                    break
                                }
                            }
                        }
                        
//                        if let assigned_bookkeeping_company = data["assigned_bookkeeping_company"] as? Int{
//                            for i in 0..<self.arrChanlId.count{
//                                let dict = self.arrChanlId[i]
//                                let _id = dict["_id"] as! Int
//                                if(assigned_bookkeeping_company == _id){
//                                    let strTitle = dict["title"] as! String
//                                    self.txtAssign.text = strTitle
//                                    self.assg_id = _id
//                                    break
//                                }
//                            }
//                        }
                    }
                }
            }
            }
                
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
}
extension CdetailBookKeepingVC : UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerViewAcoountng){
            return self.arrAccountng.count
        }
        else if(pickerView == pickerViewAssign){
            return self.arrChanlId.count
        }
        else if(pickerView == pvCountry){
            return  self.arrCountry.count
        }
        else if(pickerView == pvState){
            return self.arrState.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerViewAcoountng){
            let dict = self.arrAccountng[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewAssign){
            let dict = self.arrChanlId[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pvCountry ){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
        else if( pickerView == pvState){
            let dict = arrState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
               }

        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewAcoountng){
            let dict = self.arrAccountng[row]
            let strTitle = dict["title"] as! String
            self.txtAcountngClas.text = strTitle
            let _id = dict["_id"] as! Int
            self.accnt_id = "\(_id)"
        }
//        else if(pickerView == pickerViewAssign){
//            let dict = self.arrChanlId[row]
//            let strTitle = dict["title"] as! String
//            self.txtAssign.text = strTitle
//            let _id = dict["_id"] as! Int
//            self.assg_id = _id
//        }
        if(pickerView == pvCountry){
           let dict = arrCountry[row] as! NSDictionary
           let strTitle = dict["country_name"] as! String
           self.txtCountry.text = strTitle
           let _id = dict["_id"] as! Int
           self.countryId = "\(_id)"
                      
        }
        if( pickerView == pvState ){
            let dict = arrState[row] as!NSDictionary
            let strTitl = dict["state_name"] as! String
            self.txtState.text = strTitl
            let _id = dict["_id"] as! Int
            self.stateId = "_\(_id)"
            
        }
       
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtCountry){
            self.pickUp(txtCountry)
        }
        if(textField == self.txtAcountngClas){
                   self.pickUp(txtAcountngClas)
               }
        if(textField == self.txtState){
            self.pickUp(txtState)
        }
//        else if(textField == self.txtAssign){
//            self.pickUp(txtAssign)
//        }
    }
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAcountngClas){
            self.pickerViewAcoountng = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAcoountng.delegate = self
            self.pickerViewAcoountng.dataSource = self
            self.pickerViewAcoountng.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAcoountng
        }
        
//        else if(textField == self.txtAssign){
//            self.pickerViewAssign = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//            self.pickerViewAssign.delegate = self
//            self.pickerViewAssign.dataSource = self
//            self.pickerViewAssign.backgroundColor = UIColor.white
//            textField.inputView = self.pickerViewAssign
//        }
        else if(textField == self.txtCountry){
            self.pvCountry = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvCountry.delegate = self
            self.pvCountry.dataSource = self
            self.pvCountry.backgroundColor = UIColor.white
            textField.inputView = self.pvCountry
        }
        else if(textField == self.txtState){
            self.pvState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvState.delegate = self
            self.pvState.dataSource = self
            self.pvState.backgroundColor = UIColor.white
            textField.inputView = self.pvState
        }
    
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
     
        txtAcountngClas.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtState.resignFirstResponder()
    }
    
    @objc func cancelClick(){
       
        txtAcountngClas.resignFirstResponder()
        txtCountry.resignFirstResponder()
        txtState.resignFirstResponder()
    }
}
extension CdetailBookKeepingVC{
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        
        if(txtDescripname.text == "" || txtDescripname.text?.count == 0 || txtDescripname.text == nil){
            txtDescripname.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Descriptive Name should not left blank.")
        }
        else if(txtLegalName.text == "" || txtLegalName.text?.count == 0 || txtLegalName.text == nil){
            txtLegalName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Legal Name should not left blank.")
        }
        else if(txtAddress1.text == "" || txtAddress1.text?.count == 0 || txtAddress1.text == nil){
            txtAddress1.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Address should not left blank.")
        }
        else if(txtZip.text == "" || txtZip.text?.count == 0 || txtZip.text == nil){
            txtZip.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Zip should not left blank.")
        }
        else if(txtCity.text == "" || txtCity.text?.count == 0 || txtCity.text == nil){
            txtCity.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "City should not left blank.")
        }
        else if(txtState.text == "" || txtState.text?.count == 0 || txtState.text == nil){
            txtState.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "State should not left blank.")
        }
        else if(txtCountry.text == "" || txtCountry.text?.count == 0 || txtCountry.text == nil){
            txtCountry.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Country should not left blank.")
        }
        else if(txtCounty.text == "" || txtCounty.text?.count == 0 || txtCounty.text == nil){
            txtCounty.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "County should not left blank.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            

            
            
            
            Globalfunc.showLoaderView(view: self.view)
            let params = [
                "accounting_class": self.accnt_id,
                "address1": self.txtAddress1.text!,
                "address2": self.txtAddress2.text!,
                "bk_company": self.assg_id!,// ERROR
                "city": self.txtCity.text!,
                "contact_person": self.txtcontactPerson.text!,
                "country": self.txtCountry.text!,
                "county": self.txtCounty.text!,
                "dealer_number": self.txtDealer.text!,
                "email": self.txtemail.text!,
                "fax1": self.txtFax1.text!,
                "fax2": self.txtFax2.text!,
                "fedid": self.txtFedId.text!,
                "id": self.channel_id!,
                "legalname": self.txtLegalName.text!,
               
                "phone1": self.txtPhn1.text!,
                "phone2": self.txtPhn2.text!,
                "state": self.txtState.text!,
                "text_name": self.txtName.text!,
                "title": self.txtDescripname.text!,
                "website":  self.txtwebsite.text!,
                "zip": self.txtZip.text!] as [String : Any]
            Globalfunc.print(object:params)
            self.callUpdateApi(param: params)
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        let url = "\(Constant.addChannel)/detail"
        BaseApi.onResponsePutWithToken(url: url, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
