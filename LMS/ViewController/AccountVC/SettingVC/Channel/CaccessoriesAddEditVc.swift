//
//  CaccessoriesAddEditVc.swift
//  LMS
//
//  Created by Dr.Mac on 03/05/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CaccessoriesAddEditVc: UIViewController {
    @IBOutlet weak var txtAccessoryDisplayName : UITextField!
    @IBOutlet weak var txtDesc : UITextField!
    @IBOutlet weak var txtPrice : UITextField!
    @IBOutlet weak var txtCost : UITextField!
    
    @IBOutlet weak var lblTitle : UILabel!
    
    var passAccessoryId = ""
    var isOpenFromVc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    override func viewWillAppear(_ animated: Bool) {
        if(self.isOpenFromVc == "edit" ){
           // Globalfunc.showLoaderView(view: self.view)
           let strUrl = "\(Constant.addChannel)/sellerFees/id?id=\(self.passAccessoryId)"
            self.callApiToSetData(strUrl: strUrl)
            self.lblTitle.text = "Edit Accessories "
        }else{
            self.lblTitle.text = "Add Accessories "
        }
    }
    

    

}
extension CaccessoriesAddEditVc{
     func callApiToSetData(strUrl: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if (error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let responseDict = dict as? NSDictionary{
                        if let data = responseDict["data"] as? NSDictionary{
                            
                            
                            if let accessory_display_name = data["accessory_display_name"] as? String{
                                self.txtAccessoryDisplayName.text = accessory_display_name
                            }
                            if let accessory_desc = data["accessory_desc"] as? String{
                                self.txtDesc.text = accessory_desc
                            }
                            if let accessory_price = data["accessory_price"] as? String{
                                self.txtPrice.text = accessory_price
                            }
                            if let accessory_cost = data["accessory_cost"] as? String{
                                self.txtCost.text = accessory_cost
                            }
                           
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension CaccessoriesAddEditVc{
    
    
    @IBAction func clickonUpdateBtn(_ sender : UIButton){
        
        if(self.isOpenFromVc == "add"){
            
            do {
                let decoded  = userDef.object(forKey: "channelDetail") as! Data
                if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let chnl_id = cDict["_id"] as! Int
                    
                    let params = ["accessory_cost": self.txtCost.text!,
                                  "accessory_desc": self.txtDesc.text!,
                                  "accessory_display_name": self.txtAccessoryDisplayName.text!,
                                  "accessory_price": self.txtPrice.text!,
                    "id": chnl_id ]as [String : Any]
                    
                    Globalfunc.print(object:params)
                      self.callUpdateApi(param: params, strTyp: "post")
                }
            }
            catch{
                
            }
        }else {
            
            let params = ["accessory_cost": self.txtCost.text!,
                          "accessory_desc": self.txtDesc.text!,
                          "accessory_display_name": self.txtAccessoryDisplayName.text!,
                          "accessory_price": self.txtPrice.text!,
                          "id": self.passAccessoryId ]as [String : Any]
            
            Globalfunc.print(object:params)
              self.callUpdateApi(param: params, strTyp: "put")

        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String){
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.channelAccessory, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "post"){

            BaseApi.onResponsePostWithToken(url: Constant.channelAccessory, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }

}
//extension CaccessoriesAddEditVc : UITextFieldDelegate {
//
//}

//{"data":[{"is_deleted":false,"_id":"60900bdeb048096008ac7bb4","accessory_display_name":"test","accessory_desc":"12","accessory_price":123,"accessory_cost":1234},{"is_deleted":false,"_id":"6090409ab048096008ac7bbc","accessory_display_name":"","accessory_desc":"","accessory_price":null,"accessory_cost":null}],"success":true,"status":200}
