//
//  CsalesDefaultVC.swift
//  LMS
//
//  Created by Apple on 31/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class CsalesDefaultVC: UIViewController {
    
    @IBOutlet weak var txtCashPrtfolio : UITextField!
    @IBOutlet weak var txtDRebate : UITextField!
    @IBOutlet weak var txtMRebate : UITextField!
    @IBOutlet weak var txtWsPrtfolio : UITextField!
    @IBOutlet weak var txtRegsState: UITextField!
    @IBOutlet weak var txtDefPack : UITextField!
    @IBOutlet weak var txtTasCap : UITextField!
    
    
    var pickerCashPrtfolio: UIPickerView!
    var pickerWsPrtfolio: UIPickerView!
    var cashId : Int?
    var wholsesaleId : Int?
    
    var pickerDefState: UIPickerView!
    var arrDefsate = ["TX","NY"]
    
    var pickerDrebate: UIPickerView!
    var pickerMrebate: UIPickerView!
    
    var arrRebate = ["No","Yes"]
    var DRebate = false
    var MRebate = false
    
    
    var arrPortfolio = [NSDictionary]()
    
    var channel_id : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        self.callApi(strUrl: Constant.portfolio_url,type: "default")
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Testfield delegate methods

extension CsalesDefaultVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerCashPrtfolio){
            return self.arrPortfolio.count
        }
        else if(pickerView == pickerWsPrtfolio){
            return self.arrPortfolio.count
        }
        else if(pickerView == pickerDefState){
            return self.arrDefsate.count
        }
        else if(pickerView == pickerDrebate){
            return self.arrRebate.count
        }
        else if(pickerView == pickerMrebate){
            return self.arrRebate.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerCashPrtfolio){
            let dict = self.arrPortfolio[row]
            let strTitle = dict["portfolio_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerWsPrtfolio){
            let dict = self.arrPortfolio[row]
            let strTitle = dict["portfolio_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerDefState){
            let strTitle = self.arrDefsate[row]
            return strTitle
        }
        else if(pickerView == pickerDrebate){
            let strTitle = self.arrRebate[row]
            return strTitle
        }
        else if(pickerView == pickerMrebate){
            let strTitle = self.arrRebate[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerCashPrtfolio){
            let dict = self.arrPortfolio[row]
            let strTitle = dict["portfolio_name"] as! String
            self.txtCashPrtfolio.text = strTitle
            let _id = dict["_id"] as! Int
            self.cashId = _id
        }
        else if(pickerView == pickerWsPrtfolio){
            let dict = self.arrPortfolio[row]
            let strTitle = dict["portfolio_name"] as! String
            self.txtWsPrtfolio.text = strTitle
            let _id = dict["_id"] as! Int
            self.wholsesaleId = _id
        }
        else if(pickerView == pickerDefState){
            let strTitle = self.arrDefsate[row]
            self.txtRegsState.text = strTitle
        }
        else if(pickerView == pickerDrebate){
            let strTitle = self.arrRebate[row]
            self.txtDRebate.text = strTitle
            if(strTitle == "No"){
                self.DRebate = false
            }
            else{
                self.DRebate = true
            }
        }
        else if(pickerView == pickerMrebate){
            let strTitle = self.arrRebate[row]
            self.txtMRebate.text = strTitle
            if(strTitle == "No"){
                self.MRebate = false
            }
            else{
                self.MRebate = true
            }
            
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtCashPrtfolio){
            self.pickUp(txtCashPrtfolio)
        }
        else if(textField == self.txtWsPrtfolio){
            self.pickUp(txtWsPrtfolio)
        }
        else if(textField == self.txtRegsState){
            self.pickUp(txtRegsState)
        }
        else if(textField == self.txtDRebate){
            self.pickUp(txtDRebate)
        }
        else if(textField == self.txtMRebate){
            self.pickUp(txtMRebate)
        }
        
        
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtCashPrtfolio){
            self.pickerCashPrtfolio = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerCashPrtfolio.delegate = self
            self.pickerCashPrtfolio.dataSource = self
            self.pickerCashPrtfolio.backgroundColor = UIColor.white
            textField.inputView = self.pickerCashPrtfolio
        }
            
        else if(textField == self.txtWsPrtfolio){
            self.pickerWsPrtfolio = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWsPrtfolio.delegate = self
            self.pickerWsPrtfolio.dataSource = self
            self.pickerWsPrtfolio.backgroundColor = UIColor.white
            textField.inputView = self.pickerWsPrtfolio
        }
        else if(textField == self.txtRegsState){
            self.pickerDefState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDefState.delegate = self
            self.pickerDefState.dataSource = self
            self.pickerDefState.backgroundColor = UIColor.white
            textField.inputView = self.pickerDefState
        }
        else if(textField == self.txtDRebate){
            self.pickerDrebate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDrebate.delegate = self
            self.pickerDrebate.dataSource = self
            self.pickerDrebate.backgroundColor = UIColor.white
            textField.inputView = self.pickerDrebate
        }
            
        else if(textField == self.txtMRebate){
            self.pickerMrebate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerMrebate.delegate = self
            self.pickerMrebate.dataSource = self
            self.pickerMrebate.backgroundColor = UIColor.white
            textField.inputView = self.pickerMrebate
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtCashPrtfolio.resignFirstResponder()
        txtWsPrtfolio.resignFirstResponder()
        txtRegsState.resignFirstResponder()
        txtDRebate.resignFirstResponder()
        txtMRebate.resignFirstResponder()
    }
    
    @objc func cancelClick(){
        txtCashPrtfolio.resignFirstResponder()
        txtWsPrtfolio.resignFirstResponder()
        txtRegsState.resignFirstResponder()
        txtDRebate.resignFirstResponder()
        txtMRebate.resignFirstResponder()
        
    }
}

extension CsalesDefaultVC {
    func callGetChannelDetailApi()
    {
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.addChannel)/salesDefaults/id?id=\(chnl_id)"
                self.callApi(strUrl: strUrl,type: "sales")
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApi(strUrl : String, type: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                if(type == "default"){
                    OperationQueue.main.addOperation {
                        if let arr = dict as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrPortfolio.append(dictA)
                                }
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.callGetChannelDetailApi()
                        })
                    }
                }
                else if(type == "sales"){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let responseDict = dict as? NSDictionary {
                            
                            if let chID = responseDict["_id"] as? Int{
                                self.channel_id = chID
                            }
                            
                            if let salesDefaults = responseDict["salesDefaults"] as? NSDictionary{
                                
                                
                                if let trade_in_tax_cap = salesDefaults["trade_in_tax_cap"] as? String{
                                    self.txtTasCap.text = trade_in_tax_cap
                                }
                                
                                if let default_pack_fee = salesDefaults["default_pack_fee"] as? String{
                                    self.txtDefPack.text = default_pack_fee
                                }
                                
                                if let do_you_offer_dealer_rebates = salesDefaults["do_you_offer_dealer_rebates"] as? Bool{
                                    
                                    self.DRebate = do_you_offer_dealer_rebates
                                    
                                    if(do_you_offer_dealer_rebates == true){
                                        self.txtDRebate.text = self.arrRebate[1]
                                    }
                                    else{
                                        self.txtDRebate.text = self.arrRebate[0]
                                    }
                                }
                                
                                if let do_you_offer_manufacturer_rebates = salesDefaults["do_you_offer_manufacturer_rebates"] as? Bool{
                                    
                                    self.DRebate = do_you_offer_manufacturer_rebates
                                    
                                    if(do_you_offer_manufacturer_rebates == true){
                                        self.txtMRebate.text = self.arrRebate[1]
                                    }
                                    else{
                                        self.txtMRebate.text = self.arrRebate[0]
                                    }
                                }
                                
                                if let cash_sales_book_portfolio = salesDefaults["cash_sales_book_portfolio"] as? String{
                                    for i in 0..<self.arrPortfolio.count{
                                        let dict = self.arrPortfolio[i]
                                        let _id = dict["_id"] as! Int
                                        if(cash_sales_book_portfolio == "\(_id)"){
                                            let strTitle = dict["portfolio_name"] as! String
                                            self.txtCashPrtfolio.text = strTitle
                                            self.cashId = _id
                                            break
                                        }
                                    }
                                }
                                
                                if let wholesale_sales_book_portfolio = salesDefaults["wholesale_sales_book_portfolio"] as? Int{
                                    for i in 0..<self.arrPortfolio.count{
                                        let dict = self.arrPortfolio[i]
                                        let _id = dict["_id"] as! Int
                                        if(wholesale_sales_book_portfolio == _id){
                                            let strTitle = dict["portfolio_name"] as! String
                                            self.txtWsPrtfolio.text = strTitle
                                            self.wholsesaleId = _id
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension CsalesDefaultVC {
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        let params = [
            "cash_sales_book_portfolio": self.cashId ?? "",
            "default_contract_reg_state": self.txtRegsState.text!,
            "default_pack_fee": self.txtDefPack.text!,
            "do_you_offer_dealer_rebates": self.DRebate,
            "do_you_offer_manufacturer_rebates": self.MRebate,
            "id": self.channel_id!,
            "trade_in_tax_cap": self.txtTasCap.text!,
            "wholesale_sales_book_portfolio": self.wholsesaleId ?? ""] as [String : Any]
        Globalfunc.print(object:params)
        self.callUpdateApi(param: params)
        
    }
    
    func callUpdateApi(param: [String : Any])
    {
        let url = "\(Constant.addChannel)/salesDefaults"
        BaseApi.onResponsePutWithToken(url: url, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
