//
//  CtaxInfoVc.swift
//  LMS
//
//  Created by Dr.Mac on 05/05/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CtaxInfoVc: UIViewController {
    @IBOutlet weak var txtsalesTaxNo   : UITextField!
    @IBOutlet weak var txtTaxPayerName : UITextField!
    @IBOutlet weak var txtPhone1       : UITextField!
    @IBOutlet weak var txtFax1 : UITextField!
    @IBOutlet weak var txtAddress1 : UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtZip : UITextField!
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var txtState : UITextField!
    @IBOutlet weak var txtCounty : UITextField!
    @IBOutlet weak var txtCountry : UITextField!
    
    @IBOutlet weak var txtAccount : UITextField!
    @IBOutlet weak var txtNewAccount : UITextField!
    @IBOutlet weak var txtAppraisalPhone1 : UITextField!
    @IBOutlet weak var txtAppraisalAddress1 : UITextField!
    @IBOutlet weak var txtAppraisalAddress2: UITextField!
    @IBOutlet weak var txtAppraisalZip : UITextField!
    @IBOutlet weak var txtAppraisalCity : UITextField!
    @IBOutlet weak var txtAppraisalState : UITextField!
    @IBOutlet weak var txtAppraisalCounty : UITextField!
    @IBOutlet weak var txtAppraisalCountry : UITextField!
    
    
    @IBOutlet weak var txtCollectorName : UITextField!
    @IBOutlet weak var txtCollectorPhone1 : UITextField!
    @IBOutlet weak var txtCollectorAddress1 : UITextField!
    @IBOutlet weak var txtCollectorAddress2: UITextField!
    @IBOutlet weak var txtCollectorZip : UITextField!
    @IBOutlet weak var txtCollectorCity : UITextField!
    @IBOutlet weak var txtCollectorState : UITextField!
    @IBOutlet weak var txtCollectorCounty : UITextField!
    @IBOutlet weak var txtCollectorCountry : UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    

}
//http://3.129.150.191:3000/api/channel/taxInfo/id?id=1
