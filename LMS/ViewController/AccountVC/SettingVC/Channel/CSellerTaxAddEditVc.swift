//
//  CsellerTaxEditVc.swift
//  LMS
//
//  Created by Dr.Mac on 01/05/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CSellerTaxAddEditVc: UIViewController {
    @IBOutlet weak var txtCalcType      :   UITextField!
    @IBOutlet weak var txtTaxState      :   UITextField!
    @IBOutlet weak var txtTaxCategory   :   UITextField!
    @IBOutlet weak var txtTaxDesc       :   UITextField!
    @IBOutlet weak var txtFlatTaxAmount :   UITextField!
    @IBOutlet weak var txtTaxVariable   :   UITextField!
    @IBOutlet weak var txtTaxFixed      :   UITextField!
    @IBOutlet weak var txtFromTaxRange  :   UITextField!
    @IBOutlet weak var txToTaxRange     :   UITextField!
    @IBOutlet weak var txtRateIndicator :   UITextField!
    @IBOutlet weak var txtEditAtSale    :   UITextField!
    
    @IBOutlet weak var lblTitle         :   UILabel!
    
    var pvCalcType      : UIPickerView!
    var pvTaxState      : UIPickerView!
    var pvTaxCategory   : UIPickerView!
    var pvTaxvariable   : UIPickerView!
    var pvtaxFixed      : UIPickerView!
    var pvEditAtSale    : UIPickerView!
    var pvRateIndicator : UIPickerView!
    
    
    
    var arrCalcType    = ["Registration State" ,
                         "Contract State"]
    var arrTaxState    =  ["TX" ,
                         "NY"]
    var arrTaxCategory = ["Document Fee",
                          "Title Fee",
                          "License Fee",
                          "Registration Fee",
                          "Inspection Fee"]
    var arrTaxVariable =  ["Flat Tax - No Tax Variable" ,
                           "SalesPrice"]
    var arrEditAtSale  =  ["Yes" ,
                          "No"]
    var arrRateIndicator = ["Fixed",
                            "Dynamic"]
    var passSellerTaxId = ""
    var isOpenFromvc    = ""
    var arrSellerTaxDetail = NSMutableArray()
   
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
            
         if(self.isOpenFromvc == "edit"){
                Globalfunc.showLoaderView(view: self.view)
                let strUrl = "\(Constant.addChannel)/sellerTaxes/id?id=\(self.passSellerTaxId)"
                self.callApiToSetData(strUrl: strUrl)
            
            self.lblTitle.text = "Edit Tax"
         }else{
            self.lblTitle.text = "Add Tax"
         }

        
    }
  @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }

   
}


extension CSellerTaxAddEditVc{
   
    func callApiToSetData(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
           if (error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let responseDict = dict as? NSDictionary{
                        if let data = responseDict["data"] as? NSDictionary{
                            
                            
                            if let calc_type = data["calc_type"] as? String{
                                self.txtCalcType.text = calc_type
                            }
                            if let tax_state = data["tax_state"] as? String{
                                self.txtTaxState.text = tax_state
                            }
                            if let tax_category = data["tax_category"] as? String{
                                self.txtTaxCategory.text = tax_category
                            }
                            if let tax_desc = data["tax_desc"] as? String{
                                self.txtTaxDesc.text = tax_desc
                            }
                            if let fee_type = data["fee_type"] as? String{
                                self.txtCalcType.text = fee_type
                            }
                            
                            if let tax_desc = data["tax_desc"] as? String{
                                self.txtTaxDesc.text = tax_desc
                            }
                            if let flat_tax_amount = data["flat_tax_amount"] as? String{
                                self.txtFlatTaxAmount.text = flat_tax_amount
                            }
                            
                            if let tax_variable = data["tax_variable"] as? String{
                                self.txtTaxVariable.text = tax_variable
                            }
                            if let tax_fixed = data["tax_fixed"] as? String{
                                self.txtTaxFixed.text = tax_fixed
                            }
                            if let tax_beg_range = data["tax_beg_range"] as? String{
                                self.txtFromTaxRange.text = tax_beg_range
                            }
                            if let tax_end_range = data["tax_end_range"] as? String{
                                self.txToTaxRange.text = tax_end_range
                            }
                            if let can_edit_time_sale = data["can_edit_time_sale"] as? String{
                                self.txtEditAtSale.text = can_edit_time_sale
                            }
                            if let rate_indicator = data["rate_indicator"] as? String{
                                self.txtRateIndicator.text = rate_indicator
                            }
                          
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
            
        }
    }
}
extension CSellerTaxAddEditVc: UITextFieldDelegate , UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       if (pickerView == pvCalcType){
            return arrCalcType.count
        }
        else if (pickerView == pvTaxState){
            return arrTaxState.count
        }
        
        else if (pickerView == pvTaxCategory){
            return arrTaxCategory.count
        }
        else if (pickerView == pvTaxvariable){
            return arrTaxVariable.count
        }
        else if (pickerView == pvEditAtSale){
               return arrEditAtSale.count
       }
       else if(pickerView == pvRateIndicator){
            return arrRateIndicator.count
       }
        
       
        return 0
    }
   
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == pvCalcType){
            let title = arrCalcType[row]
            return title
        }
        else if (pickerView == pvTaxState){
            let title = arrTaxState[row]
            return title
        }
        else if (pickerView == pvTaxCategory){
            let title = arrTaxCategory[row]
            return title
        }
        else if (pickerView == pvTaxvariable){
                   let title = arrTaxVariable[row]
                   return title
               }
        else if (pickerView == pvEditAtSale){
                   let title = arrEditAtSale[row]
                   return title
         }
        else if (pickerView == pvRateIndicator){
                     let title = arrRateIndicator[row]
                     return title
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if (pickerView == pvCalcType){
            let title = arrCalcType[row]
            self.txtCalcType.text = title
        }
        else if (pickerView == pvTaxState){
            let title = arrTaxState[row]
            self.txtTaxState.text = title
        }
        else if (pickerView == pvTaxCategory){
            let title = arrTaxCategory[row]
            self.txtTaxCategory.text = title
        }
        
        else if (pickerView == pvTaxvariable){
            let title = arrTaxVariable[row]
            self.txtTaxVariable.text = title
            
        }
        else if (pickerView == pvEditAtSale){
            let title = arrEditAtSale[row]
            self.txtEditAtSale.text = title
        }
        
        else if (pickerView == pvRateIndicator){
            let title = arrRateIndicator[row]
            self.txtRateIndicator.text = title
        }
       
    }
    func pickUp(_ textField : UITextField){
        if(textField == self.txtCalcType){
            self.pvCalcType = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvCalcType.delegate = self
            self.pvCalcType.dataSource = self
            self.pvCalcType.backgroundColor = UIColor.white
            textField.inputView = self.pvCalcType
        }
        else if(textField == self.txtTaxState){
            self.pvTaxState = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvTaxState.delegate = self
            self.pvTaxState.dataSource = self
            self.pvTaxState.backgroundColor = UIColor.white
            textField.inputView = self.pvTaxState
        }
        else if(textField == self.txtTaxCategory){
            self.pvTaxCategory = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvTaxCategory.delegate = self
            self.pvTaxCategory.dataSource = self
            self.pvTaxCategory.backgroundColor = UIColor.white
            textField.inputView = self.pvTaxCategory
        }
        
        else if(textField == self.txtTaxVariable){
            self.pvTaxvariable = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvTaxvariable.delegate = self
            self.pvTaxvariable.dataSource = self
            self.pvTaxvariable.backgroundColor = UIColor.white
            textField.inputView = self.pvTaxvariable
        }
        else if(textField == self.txtEditAtSale){
            self.pvEditAtSale = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvEditAtSale.delegate = self
            self.pvEditAtSale.dataSource = self
            self.pvEditAtSale.backgroundColor = UIColor.white
            textField.inputView = self.pvEditAtSale
        }
        else if(textField == self.txtRateIndicator){
            self.pvRateIndicator = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvRateIndicator.delegate = self
            self.pvRateIndicator.dataSource = self
            self.pvRateIndicator.backgroundColor = UIColor.white
            textField.inputView = self.pvRateIndicator
        }
       
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtTaxState.resignFirstResponder()
        txtTaxCategory.resignFirstResponder()
        txtCalcType.resignFirstResponder()
        txtTaxVariable.resignFirstResponder()
        txtEditAtSale.resignFirstResponder()
        txtRateIndicator.resignFirstResponder()
    }
    @objc func cancelClick(){
        txtTaxState.resignFirstResponder()
        txtTaxCategory.resignFirstResponder()
        txtCalcType.resignFirstResponder()
        txtTaxVariable.resignFirstResponder()
        txtEditAtSale.resignFirstResponder()
        txtRateIndicator.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtCalcType){
            self.pickUp(txtCalcType)
        }
        else if(textField == self.txtTaxState){
            self.pickUp(txtTaxState)
        }
        else if(textField == self.txtTaxCategory){
            self.pickUp(txtTaxCategory)
        }
        else if(textField == self.txtTaxVariable){
                  self.pickUp(txtTaxVariable)
              }
              else if(textField == self.txtEditAtSale){
                  self.pickUp(txtEditAtSale)
              }
        else if(textField == self.txtRateIndicator){
                  self.pickUp(txtRateIndicator)
              }
              
    }
}

extension CSellerTaxAddEditVc{
    
    
    @IBAction func clickonUpdateBtn(_ sender : UIButton){
        
        if(self.isOpenFromvc == "add"){

            do {
                let decoded  = userDef.object(forKey: "channelDetail") as! Data
                if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let chnl_id = cDict["_id"] as! Int
                    
                    let params = ["calc_type": self.txtCalcType.text!,
                                  "can_edit_time_sale": self.txtEditAtSale.text!,
                                  "flat_tax_amount": self.txtFlatTaxAmount.text!,
                                  "id": chnl_id,
                                  "rate_indicator": self.txtRateIndicator.text!,
                                  "tax_beg_range": self.txtFromTaxRange!,
                                  "tax_category": self.txtTaxCategory.text!,
                                  "tax_desc": self.txtTaxDesc.text!,
                                  "tax_end_range": self.txToTaxRange.text!,
                                  "tax_fixed": self.txtTaxFixed.text!,
                                  "tax_state": self.txtTaxState.text!,
                                  "tax_variable": self.txtTaxVariable.text!
                        ]as [String : Any]
                    
                    Globalfunc.print(object:params)
                      self.callUpdateApi(param: params, strTyp: "post")
                }
            }
            
            catch{
                
            }
        }else {
            
            let params = ["calc_type": self.txtCalcType.text!,
                          "can_edit_time_sale": self.txtEditAtSale.text!,
                          "flat_tax_amount": self.txtFlatTaxAmount.text!,
                          "id": self.passSellerTaxId,
                          "rate_indicator": self.txtRateIndicator.text!,
                          "tax_beg_range": self.txtFromTaxRange!,
                          "tax_category": self.txtTaxCategory.text!,
                          "tax_desc": self.txtTaxDesc.text!,
                          "tax_end_range": self.txToTaxRange.text!,
                          "tax_fixed": self.txtTaxFixed.text!,
                          "tax_state": self.txtTaxState.text!,
                          "tax_variable": self.txtTaxVariable.text!
            ] as [String : Any]
            
            Globalfunc.print(object:params)
              self.callUpdateApi(param: params, strTyp: "put")

        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String){
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.channelSellerTaxes, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "post"){

            BaseApi.onResponsePostWithToken(url: Constant.channelSellerTaxes, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                                self.dismiss(animated: true, completion: nil)
                                }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }

}

