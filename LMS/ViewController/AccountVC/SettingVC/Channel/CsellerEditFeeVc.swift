//
//  channelBuyerEditVc.swift
//  LMS
//
//  Created by Dr.Mac on 27/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CsellerEditFeeVc: UIViewController {
    @IBOutlet weak var txtFeeState :    UITextField!
    @IBOutlet weak var txtFeeCategory : UITextField!
    @IBOutlet weak var txtFeeDesc :     UITextField!
    @IBOutlet weak var txtFeeAmount :   UITextField!
    @IBOutlet weak var txtCalcType:     UITextField!
    
    
    @IBOutlet weak var lblTitle : UILabel!
       
    var pvFeeState :    UIPickerView!
    var pvFeeCategory : UIPickerView!
    var pvCalcType:     UIPickerView!
    
    var arrFeeState    = ["TX" ,
                          "NY"]
    var arrFeeCategory = ["Document Fee",
                          "Title Fee",
                          "License Fee",
                          "Registration Fee",
                          "Inspection Fee"]
    
    var arrCalctype    = ["Registration State",
                           "Contract State"]
    var arrSellerFeeDetail = NSMutableArray()
    
    var passSellerId = ""
    var passChannelId : Int?
    
    var isOpenFromvc = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
     if(self.isOpenFromvc == "edit"){
            Globalfunc.showLoaderView(view: self.view)
            let strUrl = "\(Constant.addChannel)/sellerFees/id?id=\(self.passSellerId)"
            self.callApiToSetData(strUrl: strUrl)
        
        self.lblTitle.text = "Edit Fee"
     }else{
        self.lblTitle.text = "Add Fee"
     }

    }
   
    
    @IBAction func clickOnCloseBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension CsellerEditFeeVc{
   
    func callApiToSetData(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if (error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let responseDict = dict as? NSDictionary{
                        if let data = responseDict["data"] as? NSDictionary{
                            
                            
                            if let state = data["state"] as? String{
                                self.txtFeeState.text = state
                            }
                            if let category = data["category"] as? String{
                                self.txtFeeCategory.text = category
                            }
                            if let desc = data["desc"] as? String{
                                self.txtFeeDesc.text = desc
                            }
                            if let amount = data["amount"] as? String{
                                self.txtFeeAmount.text = amount
                            }
                            if let fee_type = data["fee_type"] as? String{
                                self.txtCalcType.text = fee_type
                            }
                        }
                    }
                }
            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension CsellerEditFeeVc : UITextFieldDelegate , UIPickerViewDelegate ,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView == pvFeeState){
            return arrFeeState.count
        }
        else if (pickerView == pvFeeCategory){
            return arrFeeCategory.count
        }
        else if (pickerView == pvCalcType){
            return arrCalctype.count
        }
        return 0
    }
   
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView == pvFeeState){
            let title = arrFeeState[row]
            return title
        }
        else if (pickerView == pvFeeCategory){
            let title = arrFeeCategory[row]
            return title
        }
        else if (pickerView == pvCalcType){
            let title = arrCalctype[row]
            return title
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if (pickerView == pvFeeState){
            let title = arrFeeState[row]
            self.txtFeeState.text = title
        }
        else if (pickerView == pvFeeCategory){
            let title = arrFeeCategory[row]
            self.txtFeeCategory.text = title
        }
        else if (pickerView == pvCalcType){
            let title = arrCalctype[row]
            self.txtCalcType.text = title
        }
    }
    func pickUp(_ textField : UITextField){
        if(textField == self.txtFeeState){
            self.pvFeeState = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvFeeState.delegate = self
            self.pvFeeState.dataSource = self
            self.pvFeeState.backgroundColor = UIColor.white
            textField.inputView = self.pvFeeState
        }
        else if(textField == self.txtFeeCategory){
            self.pvFeeCategory = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvFeeCategory.delegate = self
            self.pvFeeCategory.dataSource = self
            self.pvFeeCategory.backgroundColor = UIColor.white
            textField.inputView = self.pvFeeCategory
        }
        else if(textField == self.txtCalcType){
            self.pvCalcType = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvCalcType.delegate = self
            self.pvCalcType.dataSource = self
            self.pvCalcType.backgroundColor = UIColor.white
            textField.inputView = self.pvCalcType
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtFeeState.resignFirstResponder()
        txtFeeCategory.resignFirstResponder()
        txtCalcType.resignFirstResponder()
    }
    @objc func cancelClick(){
        txtFeeState.resignFirstResponder()
        txtFeeCategory.resignFirstResponder()
        txtCalcType.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtFeeState){
            self.pickUp(txtFeeState)
        }
        else if(textField == self.txtFeeCategory){
            self.pickUp(txtFeeCategory)
        }
        else if(textField == self.txtCalcType){
            self.pickUp(txtCalcType)
        }
    }
}

extension CsellerEditFeeVc {
    
    
    @IBAction func clickonUpdateBtn(_ sender : UIButton){
        
        if(self.isOpenFromvc == "add"){
            
            do {
                let decoded  = userDef.object(forKey: "channelDetail") as! Data
                if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let chnl_id = cDict["_id"] as! Int
                    
                    let params = [ "amount": self.txtFeeAmount.text!,
                                   "category": self.txtFeeCategory.text!,
                                   "id": chnl_id,
                        "desc": self.txtFeeDesc.text!,
                        "fee_type": self.txtCalcType.text!,
                        "state": self.txtFeeState.text!] as [String : Any]
                    
                    Globalfunc.print(object:params)
                      self.callUpdateApi(param: params, strTyp: "post")
                }
            }
            catch{
                
            }
        }else {
            
            let params = [ "amount": self.txtFeeAmount.text!,
                           "category": self.txtFeeCategory.text!,
                "desc": self.txtFeeDesc.text!,
                "fee_type": self.txtCalcType.text!,
                "id": self.passSellerId,
                "state": self.txtFeeState.text!] as [String : Any]
            
            Globalfunc.print(object:params)
              self.callUpdateApi(param: params, strTyp: "put")

        }
    }
    
    func callUpdateApi(param: [String : Any], strTyp: String){
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.channelSellerFees, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(strTyp == "post"){

            BaseApi.onResponsePostWithToken(url: Constant.channelSellerFees, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }

}
