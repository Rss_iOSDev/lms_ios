//
//  CtaxPackages.swift
//  LMS
//
//  Created by Dr.Mac on 05/05/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class CtaxPackages: UIViewController {
    @IBOutlet weak var taxPackageTbl : UITableView!
    var arrTaxpackageList : NSMutableArray = []
    override func viewDidLoad() {
        super.viewDidLoad()
 }
    override func viewWillAppear(_ animated: Bool) {
        self.taxPackageTbl.isHidden = true
        self.arrTaxpackageList = []
        
        Globalfunc.showLoaderView(view: view)
        self.callChannelDetailApi()
        
    }
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    

}
extension CtaxPackages{
    
    @IBAction func clickOnAddBtn(_ sender : UIButton){
         let storyboard = UIStoryboard.init(name:"channelStory" , bundle: nil)
         let edit = storyboard.instantiateViewController(identifier: "CtaxPackAddEditVc") as! CtaxPackAddEditVc
         edit.isOpenFromvc = "add"
         edit.modalPresentationStyle = .fullScreen
         self.present(edit , animated: true ,completion: nil)
    }
}
extension CtaxPackages {
    func callChannelDetailApi (){
        do {
            let decoded = userDef.object(forKey: "channelDetail") as! Data
            if let dict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let _id = dict["_id"] as! Int
                let strurl = "\(Constant.addChannel)/taxPackages?id=\(_id)"
                self.callApi(strurl: strurl)
            }
        }
            catch{
                Globalfunc.print(object: "Couldn't read file." )
            }
        }
    func callApi(strurl : String)  {
        BaseApi.callApiRequestForGet(url: strurl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let response = dict as? NSDictionary{
                        if let arr = response["data"] as? [NSDictionary] {
                            if  arr.count > 0{
                                for i in 0..<arr.count{
                                    let arrDict = arr[i]
                                    self.arrTaxpackageList.add(arrDict)
                                }
                                self.taxPackageTbl.isHidden = false
                                self.taxPackageTbl.delegate = self
                                self.taxPackageTbl.dataSource = self
                                self.taxPackageTbl.reloadData()
                            }
                            else {
                                self.arrTaxpackageList = []
                            }
                        }
                        
                    }
                }
            }
            else {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
extension CtaxPackages : UITableViewDelegate ,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arrTaxpackageList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = taxPackageTbl.dequeueReusableCell(withIdentifier: "taxPackageTblCell") as! taxPackageTblCell
        let dict = arrTaxpackageList[indexPath.row] as! NSDictionary
        if let tax_pack_desc = dict["tax_pack_desc"] as? String{
            cell.lblTaxPackageName.text = tax_pack_desc
        }
        if let is_default_tax_pack = dict["is_default_tax_pack"] as? String{
            cell.lblDefault.text = is_default_tax_pack
        }
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(self.clickOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnDel.tag = indexPath.row
        cell.btnDel.addTarget(self, action: #selector(self.clickOnBtnDel(_:)), for: .touchUpInside)
        return cell
    }
 @objc func clickOnEditBtn(_ sender : UIButton){
       let dict = arrTaxpackageList[sender.tag] as! NSDictionary
       let _id = dict["_id"] as! String
        let storyboard = UIStoryboard.init(name: "channelStory", bundle: nil)
       let edit = storyboard.instantiateViewController(withIdentifier: "CtaxPackAddEditVc") as! CtaxPackAddEditVc
//    edit.passTaxPackageId = _id
//    edit.isOpenFromvc = "edit"
    edit.modalPresentationStyle = .fullScreen
    self.present(edit , animated:  true,completion: nil)
       
   }
    @objc func clickOnBtnDel(_ sender : UIButton){
           let dict = arrTaxpackageList[sender.tag] as! NSDictionary
           let _id = dict["_id"] as! String
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "Delete", style: .default){(action : UIAlertAction!)
        in
        let strUrl = "\(Constant.addChannel)/taxPackages?id=\(_id)"
        self.deletFlag(strUrl)
       }
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
               UIAlertAction in
   }
        // Add the actions
        alertController.addAction(OkAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
}
    func deletFlag(_ strUrl: String){
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
class taxPackageTblCell : UITableViewCell{
    @IBOutlet weak var lblTaxPackageName : UILabel!
    @IBOutlet weak var lblDefault : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDel : UIButton!
    
    
}
//{"data":[{"tax_rates":[],"is_deleted":false,"_id":"60919d62b048096008ac7bec","tax_pack_desc":"","is_default_tax_pack":true}],"success":true,"status":200}
//http://3.129.150.191:3000/api/channel/taxPackages?id=1

