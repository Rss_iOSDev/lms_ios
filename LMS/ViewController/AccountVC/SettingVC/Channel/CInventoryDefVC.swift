//
//  CInventoryDefVC.swift
//  LMS
//
//  Created by Apple on 01/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class CInventoryDefVC: UIViewController {
    
    @IBOutlet weak var txtDefRegion : UITextField!
    @IBOutlet weak var txtAvailable : UITextField!
    @IBOutlet weak var txtEvaluating : UITextField!
    @IBOutlet weak var txtOnhold : UITextField!
    @IBOutlet weak var txtRepair: UITextField!
    @IBOutlet weak var txtChckIn : UITextField!
    @IBOutlet weak var txtTransit : UITextField!
    @IBOutlet weak var txtPendngSale : UITextField!
    @IBOutlet weak var txtsold : UITextField!
    @IBOutlet weak var txtDeleted : UITextField!
    @IBOutlet weak var txtNotforSale : UITextField!
    @IBOutlet weak var txtRecon: UITextField!
    @IBOutlet weak var txtDelivered : UITextField!
    
    var pickerDefRegion: UIPickerView!
    var arrDefRegion = NSMutableArray()
    var strDefReg = ""
    
    var pickerAvailable: UIPickerView!
    var pickerEvaluate: UIPickerView!
    var pickerOnhold: UIPickerView!
    var pickerRepair: UIPickerView!
    var pickerCheckin: UIPickerView!
    var pickerTransit: UIPickerView!
    var pickerPendingsale: UIPickerView!
    var pickersold: UIPickerView!
    var pickerDeleted: UIPickerView!
    var pickerNotforSale: UIPickerView!
    var pickerRecon: UIPickerView!
    var pickerDelivered: UIPickerView!
    
    var arrPickerVal = ["Expense/Make Ready","Asset/Inventory Improvement"]
    
    var channel_id : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        
        let dict1 = ["title":"National","value":"national"]
        let dict2 = ["title":"Southeast","value":"southeast"]
        let dict3 = ["title":"Northeast","value":"northeast"]
        let dict4 = ["title":"Midwest","value":"midwest"]
        let dict5 = ["title":"Southwest","value":"southwest"]
        let dict6 = ["title":"West Coast","value":"westcoast"]
        
        self.arrDefRegion.add(dict1)
        self.arrDefRegion.add(dict2)
        self.arrDefRegion.add(dict3)
        self.arrDefRegion.add(dict4)
        self.arrDefRegion.add(dict5)
        self.arrDefRegion.add(dict6)
        
        self.callGetChannelDetailApi()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Testfield delegate methods

extension CInventoryDefVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerDefRegion){
            return self.arrDefRegion.count
        }
        else if(pickerView == pickerAvailable || pickerView == pickerEvaluate || pickerView == pickerOnhold || pickerView == pickerRepair || pickerView == pickerCheckin ||  pickerView == pickerTransit || pickerView == pickerPendingsale || pickerView == pickersold ||  pickerView == pickerDeleted || pickerView == pickerNotforSale ||  pickerView == pickerRecon || pickerView == pickerDelivered)
        {
            return self.arrPickerVal.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerDefRegion){
            let dict = self.arrDefRegion[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerAvailable || pickerView == pickerEvaluate || pickerView == pickerOnhold || pickerView == pickerRepair || pickerView == pickerCheckin ||  pickerView == pickerTransit || pickerView == pickerPendingsale || pickerView == pickersold ||  pickerView == pickerDeleted || pickerView == pickerNotforSale ||  pickerView == pickerRecon || pickerView == pickerDelivered)
        {
            let strTitle = self.arrPickerVal[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerDefRegion){
            let dict = self.arrDefRegion[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtDefRegion.text = strTitle
            let value = dict["value"] as! String
            self.strDefReg = value
        }
            
        else if(pickerView == pickerAvailable || pickerView == pickerEvaluate || pickerView == pickerOnhold || pickerView == pickerRepair || pickerView == pickerCheckin ||  pickerView == pickerTransit || pickerView == pickerPendingsale || pickerView == pickersold ||  pickerView == pickerDeleted || pickerView == pickerNotforSale ||  pickerView == pickerRecon || pickerView == pickerDelivered)
        {
            let item = self.arrPickerVal[row]
            
            if(pickerView == pickerAvailable){
                self.txtAvailable.text = item
            }
            else if(pickerView == pickerEvaluate){
                self.txtEvaluating.text = item
            }
            else if(pickerView == pickerOnhold){
                self.txtOnhold.text = item
            }
            else if(pickerView == pickerRepair){
                self.txtRepair.text = item
            }
            else if(pickerView == pickerCheckin){
                self.txtChckIn.text = item
            }
            else if(pickerView == pickerTransit){
                self.txtTransit.text = item
            }
            else if(pickerView == pickerPendingsale){
                self.txtPendngSale.text = item
            }
            else if(pickerView == pickersold){
                self.txtsold.text = item
            }
            else if(pickerView == pickerDeleted){
                self.txtDeleted.text = item
            }
            else if(pickerView == pickerNotforSale){
                self.txtNotforSale.text = item
            }
            else if(pickerView == pickerRecon){
                self.txtRecon.text = item
            }
            else if(pickerView == pickerDelivered){
                self.txtDelivered.text = item
            }
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtDefRegion){
            self.pickUp(txtDefRegion)
        }
        else if(textField == self.txtAvailable){
            self.pickUp(txtAvailable)
        }
            
        else if(textField == self.txtEvaluating){
            self.pickUp(txtEvaluating)
        }
        else if(textField == self.txtOnhold){
            self.pickUp(txtOnhold)
        }
        else if(textField == self.txtRepair){
            self.pickUp(txtRepair)
        }
        else if(textField == self.txtChckIn){
            self.pickUp(txtChckIn)
        }
        else if(textField == self.txtTransit){
            self.pickUp(txtTransit)
        }
        else if(textField == self.txtPendngSale){
            self.pickUp(txtPendngSale)
        }
        else if(textField == self.txtsold){
            self.pickUp(txtsold)
        }
        else if(textField == self.txtDeleted){
            self.pickUp(txtDeleted)
        }
        else if(textField == self.txtNotforSale){
            self.pickUp(txtNotforSale)
        }
        else if(textField == self.txtRecon){
            self.pickUp(txtRecon)
        }
        else if(textField == self.txtDelivered){
            self.pickUp(txtDelivered)
        }
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtDefRegion){
            self.pickerDefRegion = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDefRegion.delegate = self
            self.pickerDefRegion.dataSource = self
            self.pickerDefRegion.backgroundColor = UIColor.white
            textField.inputView = self.pickerDefRegion
        }
        
        if(textField == self.txtAvailable){
            self.pickerAvailable = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAvailable.delegate = self
            self.pickerAvailable.dataSource = self
            self.pickerAvailable.backgroundColor = UIColor.white
            textField.inputView = self.pickerAvailable
        }
        
        if(textField == self.txtEvaluating){
            self.pickerEvaluate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEvaluate.delegate = self
            self.pickerEvaluate.dataSource = self
            self.pickerEvaluate.backgroundColor = UIColor.white
            textField.inputView = self.pickerEvaluate
        }
        
        if(textField == self.txtOnhold){
            self.pickerOnhold = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerOnhold.delegate = self
            self.pickerOnhold.dataSource = self
            self.pickerOnhold.backgroundColor = UIColor.white
            textField.inputView = self.pickerOnhold
        }
        
        if(textField == self.txtRepair){
            self.pickerRepair = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRepair.delegate = self
            self.pickerRepair.dataSource = self
            self.pickerRepair.backgroundColor = UIColor.white
            textField.inputView = self.pickerRepair
        }
        
        if(textField == self.txtChckIn){
            self.pickerCheckin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerCheckin.delegate = self
            self.pickerCheckin.dataSource = self
            self.pickerCheckin.backgroundColor = UIColor.white
            textField.inputView = self.pickerCheckin
        }
        
        if(textField == self.txtTransit){
            self.pickerTransit = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTransit.delegate = self
            self.pickerTransit.dataSource = self
            self.pickerTransit.backgroundColor = UIColor.white
            textField.inputView = self.pickerTransit
        }
        
        if(textField == self.txtPendngSale){
            self.pickerPendingsale = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPendingsale.delegate = self
            self.pickerPendingsale.dataSource = self
            self.pickerPendingsale.backgroundColor = UIColor.white
            textField.inputView = self.pickerPendingsale
        }
        
        if(textField == self.txtsold){
            self.pickersold = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersold.delegate = self
            self.pickersold.dataSource = self
            self.pickersold.backgroundColor = UIColor.white
            textField.inputView = self.pickersold
        }
        
        if(textField == self.txtDeleted){
            self.pickerDeleted = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDeleted.delegate = self
            self.pickerDeleted.dataSource = self
            self.pickerDeleted.backgroundColor = UIColor.white
            textField.inputView = self.pickerDeleted
        }
        
        if(textField == self.txtNotforSale){
            self.pickerNotforSale = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerNotforSale.delegate = self
            self.pickerNotforSale.dataSource = self
            self.pickerNotforSale.backgroundColor = UIColor.white
            textField.inputView = self.pickerNotforSale
        }
        
        if(textField == self.txtRecon){
            self.pickerRecon = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRecon.delegate = self
            self.pickerRecon.dataSource = self
            self.pickerRecon.backgroundColor = UIColor.white
            textField.inputView = self.pickerRecon
        }
        
        if(textField == self.txtDelivered){
            self.pickerDelivered = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDelivered.delegate = self
            self.pickerDelivered.dataSource = self
            self.pickerDelivered.backgroundColor = UIColor.white
            textField.inputView = self.pickerDelivered
        }
        
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtDefRegion.resignFirstResponder()
        txtAvailable.resignFirstResponder()
        txtEvaluating.resignFirstResponder()
        txtOnhold.resignFirstResponder()
        txtRepair.resignFirstResponder()
        txtChckIn.resignFirstResponder()
        txtTransit.resignFirstResponder()
        txtPendngSale.resignFirstResponder()
        txtsold.resignFirstResponder()
        txtDeleted.resignFirstResponder()
        txtNotforSale.resignFirstResponder()
        txtRecon.resignFirstResponder()
        txtDelivered.resignFirstResponder()
    }
}

extension CInventoryDefVC {
    func callGetChannelDetailApi()
    {
        do {
            let decoded  = userDef.object(forKey: "channelDetail") as! Data
            if let cDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let chnl_id = cDict["_id"] as! Int
                let strUrl = "\(Constant.addChannel)/inventoryDefaults/id?id=\(chnl_id)"
                self.callApi(strUrl: strUrl)
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        
                        if let chID = responseDict["_id"] as? Int{
                            self.channel_id = chID
                        }
                        
                        
                        if let inventoryDefaults = responseDict["inventoryDefaults"] as? NSDictionary{
                            
                            
                            if let valuationSettings = inventoryDefaults["valuationSettings"] as? NSDictionary{
                                
                                if let mmr_default_region = valuationSettings["mmr_default_region"] as? String{
                                    
                                    for i in 0..<self.arrDefRegion.count{
                                        let dict = self.arrDefRegion[i] as! NSDictionary
                                        let value = dict["value"] as! String
                                        if(mmr_default_region == value){
                                            let strTitle = dict["title"] as! String
                                            self.txtDefRegion.text = strTitle
                                            self.strDefReg = mmr_default_region
                                            break
                                        }
                                    }
                                    
                                }
                            }
                            
                            
                            if let defaultExpenseClassCode = inventoryDefaults["defaultExpenseClassCode"] as? NSDictionary{
                                
                                if let available = defaultExpenseClassCode["available"] as? String{
                                    self.txtAvailable.text = available
                                }
                                
                                if let check_in = defaultExpenseClassCode["check_in"] as? String{
                                    self.txtChckIn.text = check_in
                                }
                                
                                if let deleted = defaultExpenseClassCode["deleted"] as? String{
                                    self.txtDeleted.text = deleted
                                }
                                
                                
                                if let evaluating = defaultExpenseClassCode["evaluating"] as? String{
                                    self.txtEvaluating.text = evaluating
                                }
                                
                                if let in_transit = defaultExpenseClassCode["in_transit"] as? String{
                                    self.txtTransit.text = in_transit
                                }
                                
                                if let not_for_sale = defaultExpenseClassCode["not_for_sale"] as? String{
                                    self.txtNotforSale.text = not_for_sale
                                }
                                
                                if let on_hold = defaultExpenseClassCode["on_hold"] as? String{
                                    self.txtOnhold.text = on_hold
                                }
                                
                                if let pending_sale = defaultExpenseClassCode["pending_sale"] as? String{
                                    self.txtPendngSale.text = pending_sale
                                }
                                
                                if let recon_detail = defaultExpenseClassCode["recon_detail"] as? String{
                                    self.txtRecon.text = recon_detail
                                }
                                
                                if let repair_service = defaultExpenseClassCode["repair_service"] as? String{
                                    self.txtRepair.text = repair_service
                                }
                                
                                if let sold = defaultExpenseClassCode["sold"] as? String{
                                    self.txtsold.text = sold
                                }
                                
                                if let spot_delivered = defaultExpenseClassCode["spot_delivered"] as? String{
                                    self.txtDelivered.text = spot_delivered
                                }
                            }
                        }
                    }
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension CInventoryDefVC {
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        let params = ["expense_class_code_available": self.txtAvailable.text!,
                      "expense_class_code_check_in": self.txtChckIn.text!,
                      "expense_class_code_deleted": self.txtDeleted.text!,
                      "expense_class_code_evaluating": self.txtEvaluating.text!,
                      "expense_class_code_in_transit":self.txtTransit.text!,
                      "expense_class_code_not_for_sale": self.txtNotforSale.text!,
                      "expense_class_code_on_hold": self.txtOnhold.text!,
                      "expense_class_code_pending_sale": self.txtPendngSale.text!,
                      "expense_class_code_recon_detail": self.txtRecon.text!,
                      "expense_class_code_repair_service": self.txtRepair.text!,
                      "expense_class_code_sold": self.txtsold.text!,
                      "expense_class_code_spot_delivered": self.txtDelivered.text!,
                      "id": self.channel_id!,
                      "valuation_sett_mmr_default_region": self.strDefReg] as [String : Any]
        self.callUpdateApi(param: params)
    }
    
    func callUpdateApi(param: [String : Any])
    {
        let url = "\(Constant.addChannel)/inventoryDefaults"
        BaseApi.onResponsePutWithToken(url: url, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
