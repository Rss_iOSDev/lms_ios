//
//  addRefrenceVC.swift
//  LMS
//
//  Created by Apple on 04/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class addRefrenceVC: UIViewController {
    
    @IBOutlet weak var txtSSn : UITextField!
    @IBOutlet weak var txtDob : UITextField!
    @IBOutlet weak var txtFname : UITextField!
    @IBOutlet weak var txtMname : UITextField!
    @IBOutlet weak var txtLname : UITextField!
    @IBOutlet weak var txtsuffix : UITextField!
    @IBOutlet weak var txtGender : UITextField!
    @IBOutlet weak var txtActiveServc : UITextField!
    
    @IBOutlet weak var txtRelation : UITextField!
    var pickerRelation : UIPickerView!
    var arrRelation = NSMutableArray()
    var relation_id = ""
    
    var pickerDob : UIDatePicker!
    
    
    var arrSuffix = ["Sr.","Jr.","I","II","III","IV","V"]
    var arrGender = ["Male","Female"]
    var arrServcMember = ["Yes","No"]
    
    
    var pickersuffix: UIPickerView!
    var pickerGender : UIPickerView!
    var pickerServcMember: UIPickerView!
    
    var sericerMmber = ""
    
    
    @IBOutlet weak var viewIdentity : UIView!
    @IBOutlet weak var heightViewIDentity : NSLayoutConstraint!
    
    @IBOutlet weak var tblIDentity : UITableView!
    
    @IBOutlet weak var lblname : UILabel!
    @IBOutlet weak var lbltitle : UILabel!
    
    
    var arrDetail : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblIDentity.tableFooterView = UIView()
        self.viewIdentity.isHidden = true
        self.heightViewIDentity.constant = 0
        self.callGetPickerApi()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clcikOnSubmitBtn(_ sender: UIButton)
    {
        if(txtRelation.text == "" || txtRelation.text?.count == 0 || txtRelation.text == nil){
            txtRelation.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill relation field.")
        }
        if(txtFname.text == "" || txtFname.text?.count == 0 || txtFname.text == nil){
            txtFname.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill First name field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            self.lblname.text = "\(self.txtFname.text!) \(self.txtLname.text!)"
            self.lbltitle.text = "\(String((self.txtFname.text?.first)!))"
            
            let params = [
                "activeServiceMember": self.sericerMmber,
                "dob": "\(self.txtDob.text!)",
                "fname": "\(self.txtFname.text!)",
                "gender": self.txtGender.text!,
                "lname": "\(self.txtLname.text!)",
                "mname": "\(self.txtMname.text!)",
                "ssn": "\(self.txtSSn.text!)",
                "suffix": "\(self.txtsuffix.text!)"] as [String : Any]
            Globalfunc.print(object:params)
            self.callUpdateApi(param: params, StrUrl: Constant.get_reference, type: "post_get")
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callUpdateApi(param: [String : Any], StrUrl : String, type: String)
    {
        if(type == "put"){
            BaseApi.onResponsePutWithToken(url: StrUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        else if(type == "post_get"){
            BaseApi.onResponsePostWithToken(url: StrUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arrResponse = dict as? [NSDictionary] {
                            
                            if(arrResponse.count > 0){
                                for i in 0..<arrResponse.count{
                                    let dictA = arrResponse[i]
                                    self.arrDetail.append(dictA)
                                }
                                self.viewIdentity.isHidden = false
                                self.heightViewIDentity.constant = 670
                                self.tblIDentity.isHidden = false
                                self.tblIDentity.reloadData()
                            }
                            else{
                                self.viewIdentity.isHidden = false
                                self.heightViewIDentity.constant = 200
                                self.arrDetail = []
                                self.tblIDentity.isHidden = true
                            }
                            
                            
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
        }
        else if(type == "select"){
            BaseApi.onResponsePostWithToken(url: StrUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arrResponse = dict as? NSDictionary {
                            let msg = arrResponse["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
        }
        else if(type == "add")
        {
            BaseApi.onResponsePostWithToken(url: StrUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arrResponse = dict as? NSDictionary {
                            let msg = arrResponse["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
        }
    }
    
    
}

//call action api for all pages once
extension addRefrenceVC{
    
    func callGetPickerApi()
    {
        BaseApi.callApiRequestForGet(url: Constant.relationship) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let arr = dict as? [NSDictionary] {
                        if(arr.count > 0){
                            for i in 0..<arr.count{
                                let dictA = arr[i]
                                self.arrRelation.add(dictA)
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}

/*MARk - Textfeld and picker delegates*/

extension addRefrenceVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickersuffix){
            return self.arrSuffix.count
        }
        else if(pickerView == pickerGender){
            return self.arrGender.count
        }
        else if(pickerView == pickerServcMember){
            return arrServcMember.count
        }
        else if(pickerView == pickerRelation){
            return arrRelation.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            return strTitle
        }
            
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            return strTitle
        }
            
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            return strTitle
        }
            
        else if(pickerView == pickerRelation){
            let dict = arrRelation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            self.txtsuffix.text = strTitle
        }
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            self.txtGender.text = strTitle
        }
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            self.txtActiveServc.text = strTitle
            if(strTitle == "Yes"){
                self.sericerMmber = "true"
            }
            else{
                self.sericerMmber = "false"
            }
        }
            
        else if(pickerView == pickerRelation){
            let dict = arrRelation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtRelation.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.relation_id = "\(_id)"
        }
    }
    //MARK:- TextFiled Delegate
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        self.viewIdentity.isHidden = true
        self.heightViewIDentity.constant = 0
        
        
        if(textField == self.txtsuffix){
            self.pickUp(txtsuffix)
        }
            
        else if(textField == self.txtGender){
            self.pickUp(txtGender)
        }
            
        else if(textField == self.txtActiveServc){
            self.pickUp(txtActiveServc)
        }
            
        else if(textField == self.txtRelation){
            self.pickUp(txtRelation)
        }
            
        else if(textField == self.txtDob){
            self.pickUpDateTime(txtDob)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtsuffix){
            self.pickersuffix = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersuffix.delegate = self
            self.pickersuffix.dataSource = self
            self.pickersuffix.backgroundColor = UIColor.white
            textField.inputView = self.pickersuffix
        }
            
        else if(textField == self.txtGender){
            self.pickerGender = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGender.delegate = self
            self.pickerGender.dataSource = self
            self.pickerGender.backgroundColor = UIColor.white
            textField.inputView = self.pickerGender
        }
            
        else if(textField == self.txtActiveServc){
            self.pickerServcMember = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerServcMember.delegate = self
            self.pickerServcMember.dataSource = self
            self.pickerServcMember.backgroundColor = UIColor.white
            textField.inputView = self.pickerServcMember
        }
            
        else if(textField == self.txtRelation){
            self.pickerRelation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRelation.delegate = self
            self.pickerRelation.dataSource = self
            self.pickerRelation.backgroundColor = UIColor.white
            textField.inputView = self.pickerRelation
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtActiveServc.resignFirstResponder()
        txtRelation.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtActiveServc.resignFirstResponder()
        txtRelation.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDob){
            self.pickerDob = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDob.datePickerMode = .date
            self.pickerDob.backgroundColor = UIColor.white
            textField.inputView = self.pickerDob
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtDob.text = formatter.string(from: pickerDob.date)
        self.txtDob.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txtDob.resignFirstResponder()
    }
}

extension addRefrenceVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblIDentity.dequeueReusableCell(withIdentifier: "tblIdentityCell") as! tblIdentityCell
        let dict = self.arrDetail[indexPath.row]
        
        Globalfunc.print(object:dict)
        
        var fname = ""
        var lname = ""
        
        if let first_name = dict["first_name"] as? String
        {
            fname = first_name
        }
        if let last_name = dict["last_name"] as? String
        {
            lname = last_name
        }
        cell.lblname.text = "\(fname) \(lname)"
        
        if(fname != "" && lname != ""){
            cell.lbltitle.text = "\(String(fname.first!))\(String(lname.first!))"
        }
        else{
            cell.lbltitle.text = "\(String(fname.first!))"
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(self.clickOnSelectIdentityBtn(_:)), for: .touchUpInside)
        return cell
    }
}

extension addRefrenceVC {
    
    @IBAction func clickOnAddIdentityBtn(_ sender: UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                
                let params = [
                    "account_id": accId_selected,
                    "active_servicemember": self.sericerMmber,
                    "ref_type": "New",
                    "relationship": relation_id,
                    "customer_ssn": "\(self.txtSSn.text!)",
                    "date_of_birth": "\(self.txtDob.text!)",
                    "first_name": "\(self.txtFname.text!)",
                    "gender": "\(self.txtGender.text!)",
                    "last_name": "\(self.txtLname.text!)",
                    "middle_name": "\(self.txtMname.text!)",
                    "suffix": "\(txtsuffix.text!)",
                    "userid":userid
                    ] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, StrUrl: Constant.add_reference, type: "add")
            }
        }
        catch {
        }
    }
    
    @objc func clickOnSelectIdentityBtn(_ sender: UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let dict = self.arrDetail[sender.tag]
                let borrower_accid = dict["account_id"] as! Int
                let params = [
                    "account_id": accId_selected,
                    "borrower_accid": borrower_accid,
                    "ref_type": "reference",
                    "relationship": relation_id,
                    "userid":userid
                    ] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params, StrUrl: Constant.add_reference, type: "select")
            }
        }
        catch {
        }
    }
}

class tblIdentityCell : UITableViewCell
{
    @IBOutlet weak var lblname : UILabel!
    @IBOutlet weak var lbladdress : UILabel!
    @IBOutlet weak var lbltitle : UILabel!
    @IBOutlet weak var btnSelect : UIButton!
}
