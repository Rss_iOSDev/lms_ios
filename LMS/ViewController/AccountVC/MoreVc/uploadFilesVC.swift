//
//  uploadFilesVC.swift
//  LMS
//
//  Created by Apple on 06/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import Photos
import Alamofire

class uploadFilesVC: UIViewController {
    
    @IBOutlet weak var btnUpload : UIButton!
    @IBOutlet weak var collImages : UICollectionView!
    
    var arrimages : [Data] = []
    var arrDes = [String]()
    
    var arrDoc : [Data] = []
    var arrDesDoc = [String]()

    
    var asset : PHAsset!
    
    var imagePicker: ImagePicker!
    
    var estimateWidth = 160
    var cellMarginSize = 16
    
    var arrCollTyp : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnUpload.isHidden = true
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.collImages.register(UINib(nibName: "uploadImgCell", bundle: nil), forCellWithReuseIdentifier: "uploadImgCell")
        // SetupGrid view
        self.setupGridView()
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clikcONAddMoreBtn(_ sender: UIButton)
    {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Add File", style: .default , handler:{ (UIAlertAction)in
            
            let doc = TODocumentPickerViewController.init(filePath: nil)
            doc?.dataSource = TODocumentsDataSource()
            doc?.documentPickerDelegate = self
            
            let controller = UINavigationController(rootViewController: doc!)
            controller.modalPresentationStyle = .formSheet
            self.present(controller, animated: true)

        }))
        
        alert.addAction(UIAlertAction(title: "Add Image", style: .default , handler:{ (UIAlertAction)in
            self.imagePicker.present(from: sender)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    func setupGridView() {
        let flow = collImages.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumInteritemSpacing = CGFloat(self.cellMarginSize)
        flow.minimumLineSpacing = CGFloat(self.cellMarginSize)
    }
}

extension uploadFilesVC : TODocumentPickerViewControllerDelegate {
    
    func documentPickerViewController(_ documentPicker: TODocumentPickerViewController, didSelect items: [TODocumentPickerItem], inFilePath filePath: String?) {
        
        self.dismiss(animated: true, completion: nil)
        
        let absoluteItemPaths: NSMutableArray = []
        for item in items {
            let absoluteFilePath = URL(fileURLWithPath: filePath!).appendingPathComponent(item.fileName).path
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0]
            let finalPath = "\(documentDirectory)\(absoluteFilePath)"
            absoluteItemPaths.add(finalPath)
            
            self.arrDesDoc.append(absoluteFilePath)
        }
        
        
        print(absoluteItemPaths)
        
        for i in 0..<absoluteItemPaths.count{
            
            let file = absoluteItemPaths[i] as! String
            let fileData = NSData(contentsOfFile: file)
            self.arrDoc.append(fileData! as Data)
            
            let dict = ["type":"doc","data":fileData! as Data] as [String : Any]
            self.arrCollTyp.add(dict)
            
        }
        
        self.collImages.reloadData()
        self.btnUpload.isHidden = false
        self.btnUpload.setTitle("Upload File", for: .normal)

    }
}

extension uploadFilesVC {
    
    @IBAction func clickONUploadBtn(_ sender: UIButton)
    {
        if(sender.titleLabel?.text == "Upload Image"){
            Globalfunc.showLoaderView(view: self.view)
            uploadImageToServer()
        }
        else if(sender.titleLabel?.text == "Upload File"){
            Globalfunc.showLoaderView(view: self.view)
            self.uploadFileToServer()
        }
    }
}

extension uploadFilesVC : ImagePickerDelegate {
    
    func didSelect(image: UIImage?, imgUrl: URL) {
        
        let imgData = image?.jpegData(compressionQuality: 0.2)!
        let result = PHAsset.fetchAssets(withALAssetURLs: [imgUrl], options: nil)
        let asset = result.firstObject
        
        var photoString = ""
        
        if(asset==nil){
            photoString="FirstImg.png"
        }
        else{
            let fname = (asset?.value(forKey: "filename"))!
            let words = (fname as! String).components(separatedBy: ".")
            let fnm=words[0]
            photoString = "\(fnm).png"
        }
        
        let dict = ["type":"image","data":imgData!] as [String : Any]
        self.arrCollTyp.add(dict)
        
        self.arrimages.append(imgData!)
        self.arrDes.append(photoString)
        
        self.collImages.reloadData()
        self.btnUpload.isHidden = false
        self.btnUpload.setTitle("Upload Image", for: .normal)
    }
}

//coll datasource delegate
extension uploadFilesVC : UICollectionViewDelegate , UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCollTyp.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collImages.dequeueReusableCell(withReuseIdentifier: "uploadImgCell", for: indexPath) as! uploadImgCell
        let dict = self.arrCollTyp[indexPath.row] as! NSDictionary
        
        let type = dict["type"] as! String
        if(type == "image"){
            let img = dict["data"] as! Data
            cell.imgSelected.image = UIImage(data: img)
        }
        else if(type == "doc"){
            cell.imgSelected.image = UIImage(named: "pdf")
        }
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clickonRemoveBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func clickonRemoveBtn(_ sender : UIButton)
    {
    }
}

extension uploadFilesVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: width)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        let margin = CGFloat(cellMarginSize * 5)
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - margin) / cellCount
        return width
    }
}



class uploadImgCell : UICollectionViewCell
{
    @IBOutlet weak var imgSelected : UIImageView!
    @IBOutlet weak var btnRemove : UIButton!
}

extension uploadFilesVC {
    
    func uploadImageToServer()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                let formattedArray = (self.arrDes.map{String($0)}).joined(separator: ",")
                
                let dict1 = ["description" : formattedArray]
                let dict2 = ["account_id" : accId_selected]
                let dict3 = ["action_type": isUpload]
                let dict4 = ["insid": insID]
                let dict5 = ["userid": userid]
                
                let arrParam = NSMutableArray()
                arrParam.insert(dict1, at: 0)
                arrParam.insert(dict2, at: 1)
                arrParam.insert(dict3, at: 2)
                arrParam.insert(dict4, at: 3)
                arrParam.insert(dict5, at: 4)
                
                print(arrParam)

                let authToken = userDef.value(forKey: "authToken") as! String
                let headers: HTTPHeaders = [
                    "Authorization": authToken,
                    "Content-type": "multipart/form-data"
                ]
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    // import image to request
                    for imageData in self.arrimages {
                        multipartFormData.append(imageData, withName: "file[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                    }
                    for i in 0..<arrParam.count{
                        let parameters  = arrParam[i] as! [String:Any]
                        for (key, value) in parameters {
                            let newVal = "\(value)"
                            multipartFormData.append((newVal).data(using: String.Encoding.utf8)!, withName: key)
                        }
                    }
                }, to: Constant.file_url,method:HTTPMethod.post,
                   headers:headers,
                   encodingCompletion: { encodingResult in
                    DispatchQueue.main.async {
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                Globalfunc.hideLoaderView(view: self.view)
                                print(response)
                                if let dict = response.result.value as? NSDictionary{
                                    let msg = dict["msg"] as! String
                                    let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }
                })
            }
        }
        catch {
        }
    }
}

extension uploadFilesVC {
    
    func uploadFileToServer()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                let formattedArray = (self.arrDesDoc.map{String($0)}).joined(separator: ",")
                
                let dict1 = ["description" : formattedArray]
                let dict2 = ["account_id" : accId_selected]
                let dict3 = ["action_type": isUpload]
                let dict4 = ["insid": insID]
                let dict5 = ["userid": userid]
                
                let arrParam = NSMutableArray()
                arrParam.insert(dict1, at: 0)
                arrParam.insert(dict2, at: 1)
                arrParam.insert(dict3, at: 2)
                arrParam.insert(dict4, at: 3)
                arrParam.insert(dict5, at: 4)
                
                print(arrParam)

                let authToken = userDef.value(forKey: "authToken") as! String
                let headers: HTTPHeaders = [
                    "Authorization": authToken,
                    "Content-type": "multipart/form-data"
                ]
                
                
                
                
                Alamofire.upload(multipartFormData: { multipartFormData in
                    // import image to request
                    for imageData in self.arrDoc {
                        for file in self.arrDesDoc{
                            
                            let Filename = URL(fileURLWithPath: file).lastPathComponent
                            let mimeTyp = Filename.components(separatedBy: ".")
                             multipartFormData.append(imageData, withName: "file[]", fileName: Filename, mimeType: mimeTyp[1])
                        }
                    }
                    
                    for i in 0..<arrParam.count{
                        let parameters  = arrParam[i] as! [String:Any]
                        for (key, value) in parameters {
                            let newVal = "\(value)"
                            multipartFormData.append((newVal).data(using: String.Encoding.utf8)!, withName: key)
                        }
                    }
                }, to: Constant.file_url,method:HTTPMethod.post,
                   headers:headers,
                   encodingCompletion: { encodingResult in
                    DispatchQueue.main.async {
                        
                        switch encodingResult {
                        case .success(let upload, _, _):
                            upload.responseJSON { response in
                                
                                Globalfunc.hideLoaderView(view: self.view)
                                print(response)
                                if let dict = response.result.value as? NSDictionary{
                                    let msg = dict["msg"] as! String
                                    let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        self.dismiss(animated: true, completion: nil)
                                    }
                                    alertController.addAction(OKAction)
                                    self.present(alertController, animated: true, completion:nil)
                                }
                            }
                        case .failure(let error):
                            print(error)
                        }
                    }
                })
            }
        }
        catch {
        }
    }
}
