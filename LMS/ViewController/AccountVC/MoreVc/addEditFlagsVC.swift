//
//  addEditFlagsVC.swift
//  LMS
//
//  Created by Apple on 01/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class addEditFlagsVC: UIViewController {
    
    @IBOutlet weak var tblFlagList : UITableView!
    @IBOutlet weak var viewAdd : UIView!
    
    @IBOutlet weak var viewSearchBtn : UIView!
    @IBOutlet weak var lblSearch : UILabel!
    @IBOutlet weak var btnSearch : UIButton!
    
    @IBOutlet weak var viewAddBtn : UIView!
    @IBOutlet weak var lblAdd : UILabel!
    @IBOutlet weak var btnAdd : UIButton!
    
    @IBOutlet weak var viewSaveBtn : UIView!
    
    @IBOutlet weak var txtFlag : UITextField!
    @IBOutlet weak var txtdescription : UITextField!
    @IBOutlet weak var txtPriority : UITextField!
    
    var arrflags = NSMutableArray()
    var arrSelectFlag = NSMutableArray()
    
    var arrPriority = ["Low","Medium","High","Informational"]
    var pickerPriority : UIPickerView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.tblFlagList.tableFooterView = UIView()
        self.setDefault()
    }
    
    func getSelectedFlagListapi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acctId = dataD["_id"] as! Int
                let Str_url = "\(Constant.account_flag)?id=\(acctId)"
                self.call_getFlagList(strUrl: Str_url, strTyp: "getSelect")
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func setDefault()
    {
        self.tblFlagList.isHidden = false
        self.viewAdd.isHidden = true
        self.viewSaveBtn.isHidden = false
        
        self.viewSearchBtn.backgroundColor = UIColorFromHex(rgbValue: 0xEBEBEB, alpha: 1.0)
        self.viewAddBtn.backgroundColor = UIColor.white
        
        self.lblSearch.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
        self.lblAdd.backgroundColor = .darkGray
        
        self.btnSearch.setTitleColor(UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
        self.btnAdd.setTitleColor(UIColor.darkGray, for: .normal)
        
        self.call_getFlagList(strUrl: Constant.flag_url, strTyp: "get")
    }
    
    @IBAction func clickOnSelectViewBtn(_ sender : UIButton)
    {
        if(sender.tag == 100)
        {
            self.setDefault()
        }
        else if(sender.tag == 200)
        {
            self.tblFlagList.isHidden = true
            self.viewAdd.isHidden = false
            self.viewSaveBtn.isHidden = true
            
            self.viewSearchBtn.backgroundColor = UIColor.white
            self.viewAddBtn.backgroundColor = UIColorFromHex(rgbValue: 0xEBEBEB, alpha: 1.0)
            
            self.lblAdd.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            self.lblSearch.backgroundColor = .darkGray
            
            self.btnAdd.setTitleColor(UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
            self.btnSearch.setTitleColor(UIColor.darkGray, for: .normal)
        }
    }
    
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                
                for i in 0..<self.arrSelectFlag.count
                {
                    let dict = self.arrSelectFlag[i] as! NSDictionary
                    let _id = dict["_id"] as! Int
                    let title = dict["title"] as! String
                    let priority = dict["priority"] as! String
                    
                    let dictAdd : NSMutableDictionary = [:]
                    dictAdd.setValue(_id, forKey: "_id")
                    dictAdd.setValue(title, forKey: "title")
                    dictAdd.setValue(priority, forKey: "priority")
                    self.arrSelectFlag.replaceObject(at: i, with: dictAdd)
                    
                }
                print(self.arrSelectFlag)
                let params = [
                    "flags": self.arrSelectFlag,
                    "id": accId_selected] as [String : Any]
                
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params)
            }
            
        }
        catch {
            Globalfunc.print(object: "error")
        }
    }
    
    @IBAction func clickONUpdateFlagBtn(_ sender: UIButton)
    {
        if(sender.tag == 10)
        {
            self.txtFlag.text = ""
            self.txtdescription.text = ""
            txtPriority.text = ""
        }
        else if(sender.tag == 20){
            if(txtFlag.text == "" || txtFlag.text?.count == 0 || txtFlag.text == nil){
                txtFlag.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Flag.")
            }
            else  if(txtdescription.text == "" || txtdescription.text?.count == 0 || txtdescription.text == nil){
                txtdescription.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Flag Description.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                Globalfunc.showLoaderView(view: self.view)
                self.setParamtoCallApi()
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
    }
    
    func setParamtoCallApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                let params = [
                    "description": "\(self.txtdescription.text!)",
                    "module": "A",
                    "priority": "\(txtPriority.text!)",
                    "title": "\(txtFlag.text!)",
                    "userid": userid,
                    "insid": insID] as [String : Any]
                Globalfunc.print(object:params)
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.flag_url, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension addEditFlagsVC {
    
    func call_getFlagList(strUrl : String,strTyp : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(strTyp == "get"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let response = dict as? NSDictionary {
                            if let arrResponse = response["data"] as? [NSDictionary] {
                                if(arrResponse.count > 0){
                                    for i in 0..<arrResponse.count{
                                        let dictA = arrResponse[i]
                                        self.arrflags.add(dictA)
                                    }
                                    for i in 0..<self.arrflags.count
                                    {
                                        let dictA = self.arrflags[i] as! NSMutableDictionary
                                        dictA.setValue("false", forKey: "selected")
                                        self.arrflags.replaceObject(at: i, with: dictA)
                                    }
                                    Globalfunc.print(object:self.arrflags)
                                }
                                self.getSelectedFlagListapi()
                            }
                        }

                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
            else if(strTyp == "getSelect"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let arrResponse = dict as? [NSDictionary] {
                            let dictObj = arrResponse[0]
                            if let flags = dictObj["flags"] as? [NSDictionary] {
                                if(flags.count > 0){
                                    for i in 0..<self.arrflags.count{
                                        let dictA = self.arrflags[i] as! NSMutableDictionary
                                        for j in 0..<flags.count{
                                            let dictB = flags[j]
                                            let _id = dictA["_id"] as! Int
                                            let new_id = dictB["_id"] as! Int
                                            if(new_id == _id){
                                                dictA.setValue("true", forKey: "selected")
                                                self.arrflags.replaceObject(at: i, with: dictA)
                                            }
                                        }
                                    }
                                    for i in 0..<self.arrflags.count{
                                        
                                        let dict = self.arrflags[i] as! NSDictionary
                                        let selected = dict["selected"] as! String
                                        if(selected == "true"){
                                            let _id = dict["_id"] as! Int
                                            let title = dict["title"] as! String
                                            let priority = dict["priority"] as! String
                                            let dictAdd : NSMutableDictionary = [:]
                                            dictAdd.setValue(_id, forKey: "_id")
                                            dictAdd.setValue(title, forKey: "title")
                                            dictAdd.setValue(priority, forKey: "priority")
                                            self.arrSelectFlag.add(dictAdd)
                                        }
                                    }
                                    self.tblFlagList.reloadData()
                                }
                                else{
                                    self.tblFlagList.reloadData()
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePutWithToken(url: Constant.account_flag, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension addEditFlagsVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrflags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblFlagList.dequeueReusableCell(withIdentifier: "tblFlagListCell") as! tblFlagListCell
        let dict = self.arrflags[indexPath.row] as! NSDictionary
        let selected = dict["selected"] as! String
        if(selected == "true"){
            cell.btnCheckFlag.isSelected = true
        }
        else if(selected == "false"){
            cell.btnCheckFlag.isSelected = false
        }
        
        let description = dict["description"] as! String
        let title = dict["title"] as! String
        
        cell.lblFlagName.text = "\(title) \(description)"
        
        cell.btnCheckFlag.tag = indexPath.row
        cell.btnCheckFlag.addTarget(self, action: #selector(clickONCheckBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONCheckBtn(_ sender: UIButton)
    {
        let dict = self.arrflags[sender.tag] as! NSMutableDictionary
        if (self.arrSelectFlag.contains(dict))
        {
            self.arrSelectFlag.remove(dict)
            dict.setValue("false", forKey: "selected")
            self.arrflags.replaceObject(at: sender.tag, with: dict)
            self.tblFlagList.reloadData()
        }
        else{
            self.arrSelectFlag.add(dict)
            dict.setValue("true", forKey: "selected")
            self.arrflags.replaceObject(at: sender.tag, with: dict)
            self.tblFlagList.reloadData()
        }
    }
}

class tblFlagListCell : UITableViewCell
{
    @IBOutlet weak var lblFlagName : UILabel!
    @IBOutlet weak var btnCheckFlag : UIButton!
}


extension addEditFlagsVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerPriority{
            return self.arrPriority.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerPriority{
            let strTitle = arrPriority[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerPriority{
            let strTitle = arrPriority[row]
            self.txtPriority.text = strTitle
        }
            
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtPriority){
            self.pickUp(txtPriority)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtPriority){
            self.pickerPriority = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPriority.delegate = self
            self.pickerPriority.dataSource = self
            self.pickerPriority.backgroundColor = UIColor.white
            textField.inputView = self.pickerPriority
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtPriority.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtPriority.resignFirstResponder()
    }
}

