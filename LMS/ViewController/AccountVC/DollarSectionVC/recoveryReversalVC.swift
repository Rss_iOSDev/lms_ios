//
//  recoveryReversalVC.swift
//  LMS
//
//  Created by Apple on 12/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class recoveryReversalVC: UIViewController {


    @IBOutlet weak var txtComment : UITextField!
    @IBOutlet weak var tblRecovRevers : UITableView!
    
    
    var arrPostList : NSMutableArray = []
    
    var arrCheckId : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblRecovRevers.tableFooterView = UIView()
        //Globalfunc.showLoaderView(view: self.view)
        self.getReverseTransApi()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

}

extension recoveryReversalVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
//        Globalfunc.showLoaderView(view: self.view)
//        do {
//            let decoded  = userDef.object(forKey: "dataDict") as! Data
//            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
//
//                Globalfunc.print(object:dataD)
//
//                Globalfunc.print(object:self.arrCheckId)
//
//                var arrId = [String]()
//
//                for i in 0..<self.arrCheckId.count
//                {
//                    let dict = self.arrCheckId[i] as! NSDictionary
//                    let _id = dict["_id"] as! Int
//                    arrId.append("\(_id)")
//                }
//
//                Globalfunc.print(object:arrId)
//
//                let accId_selected = dataD["_id"] as! Int
//                let insID = dataD["insid"] as! Int
//                let userid = dataD["userid"] as! Int
//
//                let balanceType = (dataD)["balanceType"] as! NSDictionary
//                let balarr = balanceType[0] as! NSDictionary
//                let balId = balarr["_id"] as! Int
//
//
//                let params = [
//                    "account_id": accId_selected,
//                    "comment": "\(txtComment.text!)",
//                    "nsfData": arrId,
//                    "nsfFee":"\(txtNSFee.text!)",
//                    "balanceType": balId,
//                    "userid": userid,
//                    "insid": insID ] as [String : Any]
//                self.sendDatatoPostApi(param: params, StrCheck: "post", strUrl: Constant.post_nsf)
//            }
//        }
//        catch {
//        }
    }
    
    func sendDatatoPostApi( param: [String : Any], StrCheck: String,strUrl : String)
    {
        BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
            
            if(StrCheck == "post"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
            if(StrCheck == "email"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewDidLoad()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
    
    @IBAction func clickONEmailBtn(_ sender : UIButton)
    {
        let alertController = UIAlertController(title: "Add New Name", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email Address"
        }
        let saveAction = UIAlertAction(title: "Update", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            Globalfunc.showLoaderView(view: self.view)
            self.updateemailApi(firstTextField.text!)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func updateemailApi(_ strText : String)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary{
                
                let contact_info = dataD["contact_info"] as! NSMutableDictionary
                contact_info.setValue(strText, forKey: "email")
                dataD.setValue(contact_info, forKey: "contact_info")
                do {
                    if #available(iOS 11.0, *) {
                        let myData = try NSKeyedArchiver.archivedData(withRootObject: dataD, requiringSecureCoding: false)
                        userDef.set(myData, forKey: "dataDict")
                    } else {
                        let myData = NSKeyedArchiver.archivedData(withRootObject: dataD)
                        userDef.set(myData, forKey: "dataDict")
                    }
                } catch {
                    Globalfunc.print(object: "Couldn't write file")
                }
                
                
                let accId_selected = dataD["_id"] as! Int
                let params = [
                    "email": strText,
                    "id": accId_selected] as [String : Any]
                self.sendDatatoPostApi(param: params, StrCheck: "email",strUrl: Constant.detail_contact_info)
            }
        }
        catch{
        }
    }
}

//call action api for all pages once
extension recoveryReversalVC{
    
    func getReverseTransApi()
    {
//        do {
//            let decoded  = userDef.object(forKey: "dataDict") as! Data
//            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
//
//                Globalfunc.print(object:dataD)
//                let accId_selected = dataD["_id"] as! Int
//                let Str_url = "\(Constant.post_nsf)?account_id=\(accId_selected)"
//                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
//
//                    if(error == ""){
//                        OperationQueue.main.addOperation {
//                            Globalfunc.hideLoaderView(view: self.view)
//                            Globalfunc.print(object:dict)
//                            if let response = dict as? NSDictionary {
//                                if let arr = response["data"] as? [NSDictionary]{
//                                    if(arr.count > 0){
//                                        for i in 0..<arr.count{
//                                            let dictA = arr[i] as! NSMutableDictionary
//                                            dictA.setValue(false, forKey: "selected")
//                                            self.arrPostList.add(dictA)
//                                        }
//                                      //  self.tblReversTrans.isHidden = false
//                                        //  self.lblStatus.text = "INSURANCE HISTORY"
//                                        self.tblReversTrans.reloadData()
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    else
//                    {
//                        OperationQueue.main.addOperation {
//                            Globalfunc.hideLoaderView(view: self.view)
//                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
//                        }
//                    }
//                }
//            }
//        }
//        catch
//        {
//        }
    }
}



extension recoveryReversalVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblRecovRevers.dequeueReusableCell(withIdentifier: "tblReverseTransCell") as! tblReverseTransCell
        let dict = arrPostList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let selected = dict["selected"] as? Bool
        {
            if(selected == true){
                cell.btnRadio.isSelected = true
            }
            else{
                cell.btnRadio.isSelected = false
            }
            
            
        }
        
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(clickONBtnRadio(_:)), for: .touchUpInside)
        
        if let postDate = dict["postDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: postDate)
            cell.lblPostDate.text = formatter.string(from: date!)
        }
        
        if let paymentFrom = dict["paymentFrom"] as? NSDictionary{
            if let title = paymentFrom["title"] as? String{
                cell.lblForm.text = title
            }
        }
        
        if let _id = dict["_id"] as? NSNumber
        {
            cell.lblId.text = "\(_id)"
        }
        
        if let transactionDesc = dict["transactionDesc"] as? NSNumber
        {
            if(transactionDesc == 7){
                cell.lblTranscode.text = "1-Payment"
            }
        }
        
        if let amountReceived = dict["amountReceived"] as? NSNumber
        {
            cell.lblAmt.text = "$ \(amountReceived)"
        }
        return cell
    }
    
    @objc func clickONBtnRadio(_ sender: UIButton)
    {
        let dict = arrPostList[sender.tag] as! NSMutableDictionary
        dict.setValue(true, forKey: "selected")
        self.arrCheckId.add(dict)
        self.arrPostList.replaceObject(at: sender.tag, with: dict)
        self.tblRecovRevers.reloadData()
    }
}
