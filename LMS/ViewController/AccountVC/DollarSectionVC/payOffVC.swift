//
//  payOffVC.swift
//  LMS
//
//  Created by Apple on 13/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class payOffVC: UIViewController {
    
    
    @IBOutlet weak var lblAcountTyp : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblPromise : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var viewPromise : UIView!
    
    @IBOutlet weak var txtBalType : UITextField!
    @IBOutlet weak var txtDelivryMthd : UITextField!
    @IBOutlet weak var txtEffectiveDate : UITextField!
    @IBOutlet weak var txtAmtPaying : UITextField!
    @IBOutlet weak var txtAmtReceiving : UITextField!
    @IBOutlet weak var txtChngDue : UITextField!
    @IBOutlet weak var txtRefNo : UITextField!
    @IBOutlet weak var txtCommnts : UITextField!
    @IBOutlet weak var txtPay_form : UITextField!
    @IBOutlet weak var txtWriteOf : UITextField!
    
    @IBOutlet weak var viewDetail_1 : UIView!
    @IBOutlet weak var viewDetail_2 : UIView!
    
    @IBOutlet weak var viewValidation : UIView!
    @IBOutlet weak var viewBtn : UIView!
    
    
    @IBOutlet weak var lblDownPayBal : UILabel!
    @IBOutlet weak var lblPrinBal : UILabel!
    @IBOutlet weak var lblSalesTaxBal : UILabel!
    @IBOutlet weak var lblNonEarnPrinBal : UILabel!
    @IBOutlet weak var lblAccIntrstBal : UILabel!
    @IBOutlet weak var lblLateFeeBal : UILabel!
    @IBOutlet weak var lblMiscFeeBal : UILabel!
    @IBOutlet weak var lblPayoff : UILabel!
    @IBOutlet weak var lblPerDiem : UILabel!
    
    
    var pickerBalType : UIPickerView!
    var arrBalanceTyp = NSMutableArray()
    
    var pickerDeliveryMthd: UIPickerView!
    var pickerEffDate : UIDatePicker!
    var pickerPayForm: UIPickerView!
    
    var arrDeliveryMthd = NSMutableArray()
    var arrPaymnetForm = NSMutableArray()
    
    var pickerWriteof: UIPickerView!
    var arrWriteOf = ["No","Yes"]
    
    var balTypId = ""
    var sideNoteTyp = ""
    var deliveryMthdId : Int!
    var payFormId : Int!
    
    var balDetailDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension payOffVC {
    
    func setData()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:dataD)
                if let account_type = dataD["account_type"] as? String{
                    self.lblAcountTyp.text = "Account Type: \(account_type)"
                }
                
                if let account_status = dataD["account_status"] as? String{
                    self.lblStatus.text = "Status: \(account_status)"
                }
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblStock.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                }
                
                
                if let contact_info = dataD["contact_info"] as? NSDictionary{
                    let email = contact_info["email"] as! String
                    self.lblEmail.text = "Email: \(email)"
                }
                
                if let promiseToPay = dataD["promiseToPay"] as? NSDictionary{
                    
                    let promiseAmount = promiseToPay["promiseAmount"] as! NSNumber
                    let promiseDate = promiseToPay["promiseDate"] as! String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM-dd-yyyy"
                    let date = Date.dateFromISOString(string: promiseDate)
                    let strDate = formatter.string(from: date!)
                    let promiseStatus = promiseToPay["promiseStatus"] as! String
                    if(promiseStatus == "Kept"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Kept Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                    else if(promiseStatus == "Open"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Open Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                    else if(promiseStatus == "Broken"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Broken Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                }
                
                let dict1 =  ["description" :"All Balances","_id": "0", "sideNoteType": "0"]
                arrBalanceTyp.add(dict1)
                
                if let balanceType = dataD["balanceType"] as? [NSDictionary]{
                    for i in 0..<balanceType.count{
                        let dict = balanceType[i]
                        self.arrBalanceTyp.add(dict)
                    }
                }
                
                let dict = self.arrBalanceTyp[0] as! NSDictionary
                let strTitle = dict["description"] as! String
                self.txtBalType.text = strTitle
                if let _id = dict["_id"] as? String{
                    self.balTypId = _id
                }
                if let sideNoteType = dict["sideNoteType"] as? String{
                    self.sideNoteTyp = sideNoteType
                }
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                let result = formatter.string(from: date)
                self.txtEffectiveDate.text = result
                
                
                self.viewDetail_1.isHidden = true
                self.viewDetail_2.isHidden = true
                
                self.viewBtn.isHidden = true
                self.viewValidation.isHidden = false
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.callGetApi(strUrl: Constant.delivery_method_url, strCheck: "delivery")
                })
            }
        }
        catch {
            
        }
    }
}

extension payOffVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        if(txtAmtPaying.text == "" || txtAmtPaying.text?.count == 0 || txtAmtPaying.text == nil){
            txtAmtPaying.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Amount.")
        }
        else if(txtAmtReceiving.text == "" || txtAmtReceiving.text?.count == 0 || txtAmtReceiving.text == nil){
            txtAmtReceiving.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Amount.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoCallApi()
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                let result = formatter.string(from: date)
                
                let prin = self.balDetailDict["prin"] as! NSNumber
                let salesTax = self.balDetailDict["salesTax"] as! NSNumber
                
                let params = [
                    "account_id": accId_selected,
                    "amountPaying": "\(txtAmtPaying.text!)",
                    "amountReceived": "\(txtAmtReceiving.text!)",
                    "balanceType": self.balTypId,
                    "changeDue": "\(txtChngDue.text!)",
                    "comments": "\(txtCommnts.text!)",
                    "deliveryMethod":self.deliveryMthdId!,
                    "effectiveDate":"\(self.txtEffectiveDate.text!)",
                    "paymentFrom":self.payFormId!,
                    "postDate":result,
                    "sideNoteType": self.sideNoteTyp,
                    "principalAmount":prin,
                    "refrenceNumber":"\(self.txtRefNo.text!)",
                    "scheduleTax":salesTax,
                    "writeoff":"\(txtWriteOf.text!)",
                    "userid":userid,
                    "insid":insID,
                    "accountData":self.balDetailDict] as [String : Any]
                Globalfunc.print(object: params)
                self.sendDatatoPostApi(param: params, StrCheck: "post", strUrl: Constant.post_payoff)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any], StrCheck: String,strUrl : String)
    {
        BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
            
            if(StrCheck == "post"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
            if(StrCheck == "email"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewDidLoad()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
    
    @IBAction func clickONEmailBtn(_ sender : UIButton)
    {
        let alertController = UIAlertController(title: "Edit Email Address", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email Address"
        }
        let saveAction = UIAlertAction(title: "Edit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            Globalfunc.showLoaderView(view: self.view)
            self.updateemailApi(firstTextField.text!)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func updateemailApi(_ strText : String)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary{
                
                let contact_info = dataD["contact_info"] as! NSMutableDictionary
                contact_info.setValue(strText, forKey: "email")
                dataD.setValue(contact_info, forKey: "contact_info")
                do {
                    if #available(iOS 11.0, *) {
                        let myData = try NSKeyedArchiver.archivedData(withRootObject: dataD, requiringSecureCoding: false)
                        userDef.set(myData, forKey: "dataDict")
                    } else {
                        let myData = NSKeyedArchiver.archivedData(withRootObject: dataD)
                        userDef.set(myData, forKey: "dataDict")
                    }
                } catch {
                    Globalfunc.print(object: "Couldn't write file")
                }
                
                
                let accId_selected = dataD["_id"] as! Int
                let params = [
                    "email": strText,
                    "id": accId_selected] as [String : Any]
                self.sendDatatoPostApi(param: params, StrCheck: "email",strUrl: Constant.detail_contact_info)
            }
        }
        catch{
        }
    }
}

/*MARk - Textfeld and picker delegates*/

extension payOffVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerBalType{
            return self.arrBalanceTyp.count
        }
        else if(pickerView == pickerDeliveryMthd){
            return self.arrDeliveryMthd.count
        }
        else if(pickerView == pickerPayForm){
            return self.arrPaymnetForm.count
        }
        else if(pickerView == pickerWriteof){
            return self.arrWriteOf.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerDeliveryMthd){
            let dict = self.arrDeliveryMthd[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerPayForm){
            if(arrPaymnetForm.count > 0){
                let dict = arrPaymnetForm[row] as! NSDictionary
                let strTitle = dict["title"] as! String
                return strTitle
            }
            return "Select Delivery Method"
        }
        else if(pickerView == pickerWriteof){
            let strTile = self.arrWriteOf[row]
            return strTile
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            self.txtBalType.text = strTitle
            if let _id = dict["_id"] as? String{
                self.balTypId = _id
            }
            if let _id = dict["_id"] as? Int{
                self.balTypId = "\(_id)"
            }
            
            if let sideNoteType = dict["sideNoteType"] as? Int{
                self.sideNoteTyp = "\(sideNoteType)"
            }
            
            txtBalType.resignFirstResponder()
            self.callGertDetailApi()
        }
            
            
        else if(pickerView == pickerDeliveryMthd){
            let dict = self.arrDeliveryMthd[row] as! NSDictionary
            let _id = dict["_id"] as! Int
            self.deliveryMthdId = _id
            let strTitle = dict["title"] as! String
            self.txtDelivryMthd.text = strTitle
            self.txtDelivryMthd.resignFirstResponder()
            self.callGetApi(strUrl: "\(Constant.pay_form_url)?id=\(_id)", strCheck: "paymentForm")
        }
            
        else if(pickerView == pickerPayForm){
            let dict = arrPaymnetForm[row] as! NSDictionary
            let _id = dict["_id"] as! Int
            self.payFormId = _id
            let strTitle = dict["title"] as! String
            self.txtPay_form.text = strTitle
            self.txtPay_form.resignFirstResponder()
            self.callGertDetailApi()
        }
            
        else if(pickerView == pickerWriteof){
            self.txtWriteOf.text = self.arrWriteOf[row]
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtBalType){
            self.pickUp(txtBalType)
        }
            
        else if(textField == self.txtDelivryMthd){
            self.pickUp(txtDelivryMthd)
        }
            
        else if(textField == self.txtWriteOf){
            self.pickUp(txtWriteOf)
        }
            
        else if(textField == self.txtPay_form){
            self.pickUp(txtPay_form)
        }
        else if(textField == self.txtEffectiveDate){
            self.pickUpDateTime(txtEffectiveDate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBalType){
            
            self.pickerBalType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBalType.delegate = self
            self.pickerBalType.dataSource = self
            self.pickerBalType.backgroundColor = UIColor.white
            textField.inputView = self.pickerBalType
        }
            
        else if(textField == self.txtDelivryMthd){
            
            self.pickerDeliveryMthd = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDeliveryMthd.delegate = self
            self.pickerDeliveryMthd.dataSource = self
            self.pickerDeliveryMthd.backgroundColor = UIColor.white
            textField.inputView = self.pickerDeliveryMthd
        }
            
        else if(textField == self.txtPay_form){
            
            self.pickerPayForm = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPayForm.delegate = self
            self.pickerPayForm.dataSource = self
            self.pickerPayForm.backgroundColor = UIColor.white
            textField.inputView = self.pickerPayForm
        }
            
        else if(textField == self.txtWriteOf){
            
            self.pickerWriteof = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWriteof.delegate = self
            self.pickerWriteof.dataSource = self
            self.pickerWriteof.backgroundColor = UIColor.white
            textField.inputView = self.pickerWriteof
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtBalType.resignFirstResponder()
        
        txtDelivryMthd.resignFirstResponder()
        txtPay_form.resignFirstResponder()
        self.txtWriteOf.resignFirstResponder()
        
        if(self.txtWriteOf.text != ""){
            self.viewValidation.isHidden = true
            self.viewBtn.isHidden = false
        }
        
    }
    
    @objc func cancelClick() {
        
        txtBalType.resignFirstResponder()
        txtDelivryMthd.resignFirstResponder()
        txtPay_form.resignFirstResponder()
        self.txtWriteOf.resignFirstResponder()
        
        
        
        
    }
    
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtEffectiveDate){
            self.pickerEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEffDate.datePickerMode = .date
            self.pickerEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerEffDate
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtEffectiveDate.text = formatter.string(from: pickerEffDate.date)
        self.txtEffectiveDate.resignFirstResponder()
        self.callGertDetailApi()
    }
    
    @objc func cancelClickDate() {
        txtEffectiveDate.resignFirstResponder()
    }
    
    func callGertDetailApi(){
        Globalfunc.showLoaderView(view: self.view)
        self.viewDetail_1.isHidden = false
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_id = dataD["_id"] as! Int
                let strUrl = "\(Constant.account_details)?account_id=\(account_id)&balance_type=\(self.balTypId)&effectiveDate=\(self.txtEffectiveDate.text!)&sideNoteType=\(self.sideNoteTyp)"
                Globalfunc.print(object:strUrl)
                self.callGetApi(strUrl: strUrl, strCheck: "getData")
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONPostPayoffBtn(_ sender : UIButton)
    {
        self.viewDetail_2.isHidden = false
    }
    //    func callFilterApi(){
    //        Globalfunc.showLoaderView(view: self.view)
    //        do {
    //            let decoded  = userDef.object(forKey: "dataDict") as! Data
    //            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
    //            {
    //                let account_id = dataD["_id"] as! Int
    //                let strUrl = "\(Constant.payment_url)?id=\(account_id)&balance_type=\(self.balTypId)&payment_type=\(self.paymntTypId)&action_type=\(self.actionTypId)"
    //                Globalfunc.print(object:strUrl)
    //                self.callGetApi(strUrl: strUrl, strCheck: "filter")
    //            }
    //        }
    //        catch{
    //            Globalfunc.print(object: "Couldn't read file.")
    //        }
    //    }
}

extension payOffVC {
    
    func callGetApi(strUrl: String, strCheck: String){
        
        
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(strCheck == "delivery")
            {
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if let arr = dict as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrDeliveryMthd.add(dictA)
                                }
                            }
                            else{
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["message"] as! String
                            if(msg == "Your token has expired."){
                            }
                            else{
                            }
                        }
                    }
                }
            }
            else if(strCheck == "paymentForm")
            {
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if let responseDict = dict as? NSDictionary {
                            if let arr = responseDict["paymentForm"] as? [NSDictionary] {
                                Globalfunc.print(object:arr)
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrPaymnetForm.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(strCheck == "getData"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let responseDict = dict as? NSDictionary {
                            
                            self.balDetailDict = responseDict
                            
                            if let downPayment = responseDict["downPayment"] as? NSNumber{
                                self.lblDownPayBal.text = "Down Payment Balance: $\(Double(truncating: downPayment).rounded(toPlaces: 3))"
                            }
                            
                            if let lateFees = responseDict["lateFees"] as? NSNumber{
                                self.lblLateFeeBal.text = "Late Fees Balance: $\(Double(truncating: lateFees).rounded(toPlaces: 3))"
                            }
                            
                            if let prin = responseDict["prin"] as? NSNumber{
                                self.lblPrinBal.text = "Principal Balance: $\(Double(truncating: prin).rounded(toPlaces: 3))"
                            }
                            
                            if let salesTax = responseDict["salesTax"] as? NSNumber{
                                self.lblSalesTaxBal.text = "Sales Tax Balance: $\(Double(truncating: salesTax).rounded(toPlaces: 3))"
                            }
                            
                            if let nonEarnPrin = responseDict["nonEarnPrin"] as? NSNumber{
                                self.lblNonEarnPrinBal.text = "Non Interest Earning Balance: $\(Double(truncating: nonEarnPrin).rounded(toPlaces: 3))"
                            }
                            
                            if let interest = responseDict["interest"] as? NSNumber{
                                self.lblAccIntrstBal.text = "Accrued Interest Balance: $\(Double(truncating: interest).rounded(toPlaces: 3))"
                            }
                            
                            if let miscFees = responseDict["miscFees"] as? NSNumber{
                                self.lblMiscFeeBal.text = "Misc Fees Balance: $\(Double(truncating: miscFees).rounded(toPlaces: 3))"
                            }
                            
                            if let total = responseDict["total"] as? NSNumber{
                                self.lblPayoff.text = "Payoff: $\(Double(truncating: total).rounded(toPlaces: 3))"
                            }
                            
                            if let diem = responseDict["diem"] as? NSNumber{
                                self.lblPerDiem.text = "Per Diem: $\(Double(truncating: diem).rounded(toPlaces: 3))"
                            }
                        }
                    }
                }
            }
        }
    }
}
