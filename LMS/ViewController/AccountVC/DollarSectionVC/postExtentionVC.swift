//
//  postExtentionVC.swift
//  LMS
//
//  Created by Apple on 02/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class postExtentionVC: UIViewController {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblAcount : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblFlag : UILabel!
    @IBOutlet weak var txtComment : UITextField!
    
    @IBOutlet weak var tblPostList : UITableView!
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var btnPost: UIButton!
    
    var arrPostList : NSMutableArray = []
    
    var selectObject : NSMutableDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnPost.isEnabled = false
        self.btnPost.alpha = 0.5
        
        
        self.tblPostList.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        self.getPstExtensionapiApi()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension postExtentionVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let schedTax = self.selectObject["schedTax"] as! Int
                let appliedTax = self.selectObject["appliedTax"] as! Int
                let schedTax_cal = (schedTax*1) - (appliedTax*1)
                
                let schedInterest = self.selectObject["schedInterest"] as! Int
                let appliedInterest = self.selectObject["appliedInterest"] as! Int
                let schedInt = (schedInterest*1) - (appliedInterest*1)
                
                let schedPrincipal = self.selectObject["schedPrincipal"] as! Int
                let appliedPrincipal = self.selectObject["appliedPrincipal"] as! Int
                let schedprin = (schedPrincipal*1) - (appliedPrincipal*1)
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let sech_id = self.selectObject["_id"] as! Int
                
                let regularDue = self.selectObject["regularDue"] as! Int
                let dueDate = self.selectObject["dueDate"] as! String
                
                let paymentType = self.selectObject["paymentType"] as! NSDictionary
                let payid = paymentType["_id"] as! Int
                
                let balanceType = self.selectObject["balanceType"] as! NSDictionary
                let balid = balanceType["_id"] as! Int
                
                let paymentSubType = self.selectObject["paymentSubType"] as! Int
                let loanType = self.selectObject["loanType"] as! Int
                let stillDue = self.selectObject["stillDue"] as! NSNumber
                
                let params = [
                    "account_id": accId_selected,
                    "schedule_id": sech_id,
                    "comment": "\(txtComment.text!)",
                    "paymentType": payid,
                    "dueDate": dueDate,
                    "regularDue": regularDue,
                    "schedPrincipal": schedprin,
                    "schedInterest": schedInt,
                    "schedTax": schedTax_cal,
                    "stillDue": stillDue,
                    "extendedFromDate": dueDate,
                    "balanceType":balid,
                    "loanType": loanType,
                    "paymentSubType":paymentSubType,
                    "userid": userid,
                    "insid": insID ] as [String : Any]
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.post_extension, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//call action api for all pages once
extension postExtentionVC{
    
    func getPstExtensionapiApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblAcount.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                    self.lblStock.text = "Stock #: \(loan_no)"
                }
                
                if let identity_info = dataD["identity_info"] as? NSDictionary{
                    let first_name = identity_info["first_name"] as! String
                    let last_name = identity_info["last_name"] as! String
                    self.lblName.text = "\(first_name) \(last_name)"
                }
                
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.post_extension)?id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            if let response = dict as? NSDictionary {
                                if let arr = response["paymentSchedule"] as? [NSDictionary]{
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i] as! NSMutableDictionary
                                            dictA.setValue(false, forKey: "selected")
                                            self.arrPostList.add(dictA)
                                        }
                                        self.tblPostList.isHidden = false
                                        //  self.lblStatus.text = "INSURANCE HISTORY"
                                        self.tblPostList.reloadData()
                                    }
                                    else{
                                        self.tblPostList.isHidden = true
                                        //  self.lblStatus.text = "INSURANCE HISTORY \n \n No Records found"
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch
        {
        }
    }
}

extension postExtentionVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrPostList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblPostList.dequeueReusableCell(withIdentifier: "tblpostExtnionCell") as! tblpostExtnionCell
        let dict = arrPostList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let selected = dict["selected"] as? Bool
        {
            if(selected == true){
                cell.btnRadio.isSelected = true
            }
            else{
                cell.btnRadio.isSelected = false
            }
        }
        
        if let dueDate = dict["dueDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            cell.lblDueDate.text = formatter.string(from: date!)
        }
        
        let extendedFromDate = dict["extendedFromDate"] != nil
        if(extendedFromDate){
            if let extendedFromDate = dict["extendedFromDate"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: extendedFromDate)
                let strDate = formatter.string(from: date!)
                cell.lblPayTyp.text = "Extended Payment from \(strDate)"
            }
        }
        else{
            if let paymentType = dict["paymentType"] as? NSDictionary{
                if let title = paymentType["desc"] as? String{
                    cell.lblPayTyp.text = title
                }
            }
        }
        
        if let lateFee = dict["lateFee"] as? NSNumber
        {
            cell.lblLatefee.text = "$ \(lateFee)"
        }
        
        if let stillDue = dict["stillDue"] as? NSNumber
        {
            cell.lblTotaldue.text = "$ \(stillDue)"
        }
        
        if let regularDue = dict["regularDue"] as? NSNumber
        {
            cell.lblPayAmt.text = "$ \(regularDue)"
        }
        
        if let autoPay = dict["autoPay"] as? Bool
        {
            if(autoPay == true){
                cell.imgAutopay.image = UIImage(named: "check")
            }
            else{
                cell.imgAutopay.image = UIImage(named: "uncheck")
            }
        }
        
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(clickONBtnRadio(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnRadio(_ sender: UIButton)
    {
        self.selectObject = [:]
        self.btnPost.isEnabled = true
        self.btnPost.alpha = 1.0
        for i in 0..<arrPostList.count{
            let dictA = arrPostList[i] as! NSMutableDictionary
            dictA.setValue(false, forKey: "selected")
            self.arrPostList.replaceObject(at: i, with: dictA)
        }
        let dict = arrPostList[sender.tag] as! NSMutableDictionary
        dict.setValue(true, forKey: "selected")
        self.selectObject = dict
        self.arrPostList.replaceObject(at: sender.tag, with: dict)
        self.tblPostList.reloadData()
    }
}

class tblpostExtnionCell : UITableViewCell
{
    @IBOutlet weak var lblDueDate : UILabel!
    @IBOutlet weak var lblPayTyp : UILabel!
    @IBOutlet weak var lblPayAmt : UILabel!
    @IBOutlet weak var lblLatefee : UILabel!
    @IBOutlet weak var lblTotaldue : UILabel!
    @IBOutlet weak var imgAutopay : UIImageView!
    @IBOutlet weak var btnRadio : UIButton!
}
