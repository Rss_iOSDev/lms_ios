//
//  skipAutpPayVC.swift
//  LMS
//
//  Created by Apple on 02/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class skipAutpPayVC: UIViewController {
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblAcount : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblFlag : UILabel!
    @IBOutlet weak var txtComment : UITextField!
    
    
    @IBOutlet weak var tblSkip : UITableView!
    
    @IBOutlet weak var btnSkip: UIButton!
    
    var arrSkipPayList : NSMutableArray = []
    
    var selectObject : NSMutableDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnSkip.isEnabled = false
        self.btnSkip.alpha = 0.5
        
        
        self.tblSkip.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        self.getskipAutoPayApi()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension skipAutpPayVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                let sech_id = self.selectObject["_id"] as! Int
                let params = [
                    "account_id": accId_selected,
                    "schedule_id": sech_id,
                    "comment": "\(txtComment.text!)",
                    "userid": userid,
                    "insid": insID ] as [String : Any]
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.skip_auto_pay, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//call action api for all pages once
extension skipAutpPayVC{
    
    func getskipAutoPayApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblAcount.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                    self.lblStock.text = "Stock #: \(loan_no)"
                }
                
                if let identity_info = dataD["identity_info"] as? NSDictionary{
                    let first_name = identity_info["first_name"] as! String
                    let last_name = identity_info["last_name"] as! String
                    self.lblName.text = "\(first_name) \(last_name)"
                }
                
                
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.skip_auto_pay)?id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            if let response = dict as? NSDictionary {
                                if let arr = response["paymentSchedule"] as? [NSDictionary]{
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i] as! NSMutableDictionary
                                            dictA.setValue(false, forKey: "selected")
                                            self.arrSkipPayList.add(dictA)
                                        }
                                        self.tblSkip.isHidden = false
                                        //  self.lblStatus.text = "INSURANCE HISTORY"
                                        self.tblSkip.reloadData()
                                    }
                                    else{
                                        self.tblSkip.isHidden = true
                                        //  self.lblStatus.text = "INSURANCE HISTORY \n \n No Records found"
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch
        {
        }
    }
}

extension skipAutpPayVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSkipPayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblSkip.dequeueReusableCell(withIdentifier: "tblpostExtnionCell") as! tblpostExtnionCell
        let dict = arrSkipPayList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let selected = dict["selected"] as? Bool
        {
            if(selected == true){
                cell.btnRadio.isSelected = true
            }
            else{
                cell.btnRadio.isSelected = false
            }
        }
        
        if let dueDate = dict["dueDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            cell.lblDueDate.text = formatter.string(from: date!)
        }
        
        let extendedFromDate = dict["extendedFromDate"] != nil
        if(extendedFromDate){
            if let extendedFromDate = dict["extendedFromDate"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: extendedFromDate)
                let strDate = formatter.string(from: date!)
                cell.lblPayTyp.text = "Extended Payment from \(strDate)"
            }
        }
        else{
            if let paymentType = dict["paymentType"] as? NSDictionary{
                if let title = paymentType["title"] as? String{
                    cell.lblPayTyp.text = title
                }
            }
        }
        
        if let lateFee = dict["lateFee"] as? NSNumber
        {
            cell.lblLatefee.text = "$ \(lateFee)"
        }
        
        if let stillDue = dict["stillDue"] as? NSNumber
        {
            cell.lblTotaldue.text = "$ \(stillDue)"
        }
        
        if let regularDue = dict["regularDue"] as? NSNumber
        {
            cell.lblPayAmt.text = "$ \(regularDue)"
        }
        
        if let autoPay = dict["autoPay"] as? Bool
        {
            if(autoPay == true){
                cell.imgAutopay.image = UIImage(named: "check")
            }
            else{
                cell.imgAutopay.image = UIImage(named: "uncheck")
            }
        }
        
        cell.btnRadio.tag = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(clickONBtnRadio(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnRadio(_ sender: UIButton)
    {
        self.selectObject = [:]
        self.btnSkip.isEnabled = true
        self.btnSkip.alpha = 1.0
        for i in 0..<arrSkipPayList.count{
            let dictA = arrSkipPayList[i] as! NSMutableDictionary
            dictA.setValue(false, forKey: "selected")
            self.arrSkipPayList.replaceObject(at: i, with: dictA)
        }
        let dict = arrSkipPayList[sender.tag] as! NSMutableDictionary
        dict.setValue(true, forKey: "selected")
        self.selectObject = dict
        self.arrSkipPayList.replaceObject(at: sender.tag, with: dict)
        self.tblSkip.reloadData()
    }
}
