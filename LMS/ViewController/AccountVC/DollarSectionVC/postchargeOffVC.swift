//
//  chargeOffVC.swift
//  LMS
//
//  Created by Apple on 14/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class postchargeOffVC: UIViewController {
    
    @IBOutlet weak var lblAcountTyp : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var txtRelease : UITextField!
    @IBOutlet weak var txtwishPost : UITextField!
    @IBOutlet weak var txtCommnts : UITextField!
    
    @IBOutlet weak var viewSkip : UIView!
    @IBOutlet weak var viewChargeOff : UIView!
    @IBOutlet weak var viewInventory : UIView!
    
    @IBOutlet weak var lblDownPayBal : UILabel!
    @IBOutlet weak var lblPrinBal : UILabel!
    @IBOutlet weak var lblSalesTaxBal : UILabel!
    @IBOutlet weak var lblNonEarnPrinBal : UILabel!
    @IBOutlet weak var lblAccIntrstBal : UILabel!
    @IBOutlet weak var lblLateFeeBal : UILabel!
    @IBOutlet weak var lblMiscFeeBal : UILabel!
    @IBOutlet weak var lblPayoff : UILabel!
    
    @IBOutlet weak var txtSales : UITextField!
    @IBOutlet weak var txtMileague : UITextField!
    @IBOutlet weak var txtMileagueStatus : UITextField!
    @IBOutlet weak var txtColteralAcv : UITextField!
    @IBOutlet weak var txtforclosure : UITextField!
    @IBOutlet weak var txtCommntsInvntry : UITextField!
    
    
    @IBOutlet weak var txtSkipChrge : UITextField!
    @IBOutlet weak var txtAddInventory : UITextField!
    
    var pickerSkipChrge: UIPickerView!
    var arrSkipChrge = ["Skip","Charge Off"]
    
    var pickerRelease: UIPickerView!
    var arrRealease = ["No","Yes"]
    var defBal : Bool!
    
    var pickerAddInventory: UIPickerView!
    var arrAddInventory = ["No","Yes"]
    var invetrySelect : Bool!
    
    
    var pickerForcloseur: UIPickerView!
    var arrForcloseur = ["No","Yes"]
    var forClose : Bool!
    
    var pickerVehLot: UIPickerView!
    var id_Sales = ""
    
    var pickerMilStatus: UIPickerView!
    var arrMilStatus = ["Actual Miles","True Mileage Unknown","Beyond Mechnical Limits","Exempt"]
    
    var balDetailDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension postchargeOffVC {
    
    func setData()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:dataD)
                if let account_type = dataD["account_type"] as? String{
                    self.lblAcountTyp.text = "Account Type: \(account_type)"
                }
                
                if let account_status = dataD["account_status"] as? String{
                    self.lblStatus.text = "Status: \(account_status)"
                }
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblStock.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                }
                
                
                
                self.txtRelease.text = self.arrRealease[0]
                defBal = false
                
                self.txtforclosure.text = self.arrForcloseur[0]
                forClose = false
                
                self.txtSkipChrge.text = self.arrSkipChrge[0]
                
                self.txtAddInventory.text = self.arrAddInventory[0]
                self.invetrySelect = false
                
                self.viewSkip.isHidden = false
                self.viewChargeOff.isHidden = true
                self.viewInventory.isHidden = true
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.self.callGertDetailApi()
                })
            }
        }
        catch {
            
        }
    }
}

extension postchargeOffVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        self.setParamtoCallApi(tag: sender.tag)
    }
    
    func setParamtoCallApi(tag : Int)
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                if(tag == 10){
                    let params = [
                        "account_id": accId_selected,
                        "skipOrChargeOf": "S",
                        "comments": "\(txtCommnts.text!)",
                        "userid":userid,
                        "insid":insID,
                        "accountData":self.balDetailDict] as [String : Any]
                    Globalfunc.print(object: params)
                    self.sendDatatoPostApi(param: params,strUrl: Constant.post_chargeOff)
                }
                
                else if(tag == 20){
                    let params = [
                        "account_id": accId_selected,
                        "skipOrChargeOf": "C",
                        "addToInventory": false,
                        "postARecovery": "\(txtwishPost.text!)",
                        "deficiencyBalance": self.defBal!,
                        "comments": "\(txtCommnts.text!)",
                        "userid":userid,
                        "insid":insID,
                        "accountData":self.balDetailDict] as [String : Any]
                    Globalfunc.print(object: params)
                    self.sendDatatoPostApi(param: params,strUrl: Constant.post_chargeOff)
                }
                
                else if(tag == 30){
                    
                    if(txtMileague.text == "" || txtMileague.text?.count == 0 || txtMileague.text == nil){
                        txtMileague.shake()
                        Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Field.")
                    }
                    else if(txtMileagueStatus.text == "" || txtMileagueStatus.text?.count == 0 || txtMileagueStatus.text == nil){
                        txtMileagueStatus.shake()
                        Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Field.")
                    }
                    else if(txtColteralAcv.text == "" || txtColteralAcv.text?.count == 0 || txtColteralAcv.text == nil){
                            txtColteralAcv.shake()
                            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Field.")
                    }
                        
                    else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                        
                        let params = [
                            "account_id": accId_selected,
                            "skipOrChargeOf": "C",
                            "addToInventory": true,
                            "salesLocation" : self.id_Sales,
                            "mileage": "\(txtMileague.text!)",
                            "mileageStatus": "\(txtMileagueStatus.text!)",
                            "collateralACV": "\(txtColteralAcv.text!)",
                            "strictForeclosure":forClose!,
                            "comments": "\(txtCommntsInvntry.text!)",
                            "userid":userid,
                            "insid":insID,
                            "accountData":self.balDetailDict] as [String : Any]
                        Globalfunc.print(object: params)
                        self.sendDatatoPostApi(param: params,strUrl: Constant.post_chargeOff)
                    }
                    else{
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                    }
                }
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any],strUrl : String)
    {
        BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
        }
    }
}

/*MARk - Textfeld and picker delegates*/

extension postchargeOffVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerSkipChrge){
            return self.arrSkipChrge.count
        }
        else if(pickerView == pickerAddInventory){
            return self.arrAddInventory.count
        }
        else if(pickerView == pickerRelease){
            return self.arrRealease.count
        }
        else if(pickerView == pickerForcloseur){
            return self.arrForcloseur.count
        }
        else if(pickerView == pickerVehLot){
            return consArrays.arrDefaultBranch.count
        }
        else if(pickerView == pickerMilStatus){
            return arrMilStatus.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerSkipChrge){
            let strTile = self.arrSkipChrge[row]
            return strTile
        }
        else if(pickerView == pickerAddInventory){
            let strTile = self.arrAddInventory[row]
            return strTile
        }
        else if(pickerView == pickerRelease){
            let strTile = self.arrRealease[row]
            return strTile
        }
        else if(pickerView == pickerForcloseur){
            let strTtle = self.arrForcloseur[row]
            return strTtle
        }
        else if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        else if(pickerView == pickerMilStatus){
            let strTtle = self.arrMilStatus[row]
            return strTtle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerSkipChrge){
            self.txtSkipChrge.text = self.arrSkipChrge[row]
            
            if(self.txtSkipChrge.text == "Skip")
            {
                self.viewSkip.isHidden = false
                self.viewChargeOff.isHidden = true
            }
            else if(self.txtSkipChrge.text == "Charge Off"){
                self.viewSkip.isHidden = true
                self.viewChargeOff.isHidden = false
            }
            
        }
        else if(pickerView == pickerAddInventory){
            self.txtAddInventory.text = self.arrAddInventory[row]
            if(self.txtAddInventory.text == "Yes")
            {
                self.invetrySelect = true
                self.viewSkip.isHidden = false
                self.viewChargeOff.isHidden = false
                self.viewInventory.isHidden = false
            }
            else if(self.txtAddInventory.text == "No"){
                self.invetrySelect = false
                self.viewSkip.isHidden = false
                self.viewChargeOff.isHidden = false
                self.viewInventory.isHidden = true
            }
            
        }
        else if(pickerView == pickerRelease){
            self.txtRelease.text = self.arrRealease[row]
            
            if(self.txtRelease.text == "Yes")
            {
                self.defBal = true
            }
            else if(self.txtAddInventory.text == "No"){
                self.defBal = false
            }
        }
        else if(pickerView == pickerForcloseur){
            self.txtforclosure.text =  self.arrForcloseur[row]
            
            if(self.txtforclosure.text == "Yes")
            {
                self.forClose = true
            }
            else if(self.txtforclosure.text == "No"){
                self.forClose = false
            }
            
        }
        else if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            
            let item = dict["title"] as! String
            self.txtSales.text = item
            
            let _id = dict["_id"] as! Int
            id_Sales = "\(_id)"
        }
        
        else if(pickerView == pickerMilStatus){
            self.txtMileagueStatus.text = self.arrMilStatus[row]
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtSkipChrge){
            self.pickUp(txtSkipChrge)
        }
        else if(textField == self.txtAddInventory){
            self.pickUp(txtAddInventory)
        }
        else if(textField == self.txtRelease){
            self.pickUp(txtRelease)
        }
        else if(textField == self.txtforclosure){
            self.pickUp(txtforclosure)
        }
        else if(textField == self.txtSales){
            self.pickUp(txtSales)
        }
        else if(textField == self.txtMileagueStatus){
            self.pickUp(txtMileagueStatus)
        }
               
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSkipChrge){
            self.pickerSkipChrge = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSkipChrge.delegate = self
            self.pickerSkipChrge.dataSource = self
            self.pickerSkipChrge.backgroundColor = UIColor.white
            textField.inputView = self.pickerSkipChrge
        }
        else if(textField == self.txtAddInventory){
            self.pickerAddInventory = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAddInventory.delegate = self
            self.pickerAddInventory.dataSource = self
            self.pickerAddInventory.backgroundColor = UIColor.white
            textField.inputView = self.pickerAddInventory
        }
        else if(textField == self.txtRelease){
            self.pickerRelease = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRelease.delegate = self
            self.pickerRelease.dataSource = self
            self.pickerRelease.backgroundColor = UIColor.white
            textField.inputView = self.pickerRelease
        }
        else if(textField == self.txtforclosure){
            self.pickerForcloseur = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerForcloseur.delegate = self
            self.pickerForcloseur.dataSource = self
            self.pickerForcloseur.backgroundColor = UIColor.white
            textField.inputView = self.pickerForcloseur
        }
        else if(textField == self.txtSales){
            self.pickerVehLot = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerVehLot.delegate = self
            self.pickerVehLot.dataSource = self
            self.pickerVehLot.backgroundColor = UIColor.white
            textField.inputView = self.pickerVehLot
        }
        else if(textField == self.txtMileagueStatus){
            self.pickerMilStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerMilStatus.delegate = self
            self.pickerMilStatus.dataSource = self
            self.pickerMilStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerMilStatus
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        self.txtSkipChrge.resignFirstResponder()
        self.txtAddInventory.resignFirstResponder()
        self.txtRelease.resignFirstResponder()
        self.txtforclosure.resignFirstResponder()
        self.txtSales.resignFirstResponder()
        self.txtMileagueStatus.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        
        txtSkipChrge.resignFirstResponder()
        self.txtAddInventory.resignFirstResponder()
        self.txtRelease.resignFirstResponder()
        self.txtforclosure.resignFirstResponder()
        self.txtSales.resignFirstResponder()
        self.txtMileagueStatus.resignFirstResponder()
        
    }
    
    func callGertDetailApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let account_id = dataD["_id"] as! Int
                let strUrl = "\(Constant.account_details)?account_id=\(account_id)&balance_type=0"
                self.callGetApi(strUrl: strUrl, strCheck: "getData")
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension postchargeOffVC {
    
    func callGetApi(strUrl: String, strCheck: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(strCheck == "getData"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let responseDict = dict as? NSDictionary {
                            
                            self.balDetailDict = responseDict
                            
                            if let downPayment = responseDict["downPayment"] as? NSNumber{
                                self.lblDownPayBal.text = "Down Payment Balance: $\(Double(truncating: downPayment).rounded(toPlaces: 3))"
                            }
                            
                            if let lateFees = responseDict["lateFees"] as? NSNumber{
                                self.lblLateFeeBal.text = "Late Fees Balance: $\(Double(truncating: lateFees).rounded(toPlaces: 3))"
                            }
                            
                            if let prin = responseDict["prin"] as? NSNumber{
                                self.lblPrinBal.text = "Principal Balance: $\(Double(truncating: prin).rounded(toPlaces: 3))"
                            }
                            
                            if let salesTax = responseDict["salesTax"] as? NSNumber{
                                self.lblSalesTaxBal.text = "Sales Tax Balance: $\(Double(truncating: salesTax).rounded(toPlaces: 3))"
                            }
                            
                            if let nonEarnPrin = responseDict["nonEarnPrin"] as? NSNumber{
                                self.lblNonEarnPrinBal.text = "Non Interest Earning Balance: $\(Double(truncating: nonEarnPrin).rounded(toPlaces: 3))"
                            }
                            
                            if let interest = responseDict["interest"] as? NSNumber{
                                self.lblAccIntrstBal.text = "Accrued Interest Balance: $\(Double(truncating: interest).rounded(toPlaces: 3))"
                            }
                            
                            if let miscFees = responseDict["miscFees"] as? NSNumber{
                                self.lblMiscFeeBal.text = "Misc Fees Balance: $\(Double(truncating: miscFees).rounded(toPlaces: 3))"
                            }
                            
                            if let total = responseDict["total"] as? NSNumber{
                                self.lblPayoff.text = "Payoff: $\(Double(truncating: total).rounded(toPlaces: 3))"
                            }
                        }
                    }
                }
            }
        }
    }
}
