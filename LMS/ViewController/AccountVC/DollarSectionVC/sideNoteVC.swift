//
//  sideNoteVC.swift
//  LMS
//
//  Created by Apple on 12/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class sideNoteVC: UIViewController {
    
    @IBOutlet weak var lblAcountTyp : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblPromise : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var viewPromise : UIView!
    
    @IBOutlet weak var txtDescription : UITextField!
    @IBOutlet weak var txtsideNoteTyp : UITextField!
    @IBOutlet weak var txtLoanamt : UITextField!
    @IBOutlet weak var txtLoanDate : UITextField!
    @IBOutlet weak var txtLoanRate : UITextField!
    
    @IBOutlet weak var viewRecurng : UIView!
    @IBOutlet weak var txtRecurngAmt: UITextField!
    @IBOutlet weak var txtRecurngDate : UITextField!
    @IBOutlet weak var txtRecurngFrequncy : UITextField!
    
    @IBOutlet weak var viewSchd : UIView!
    @IBOutlet weak var viewDisclosure : UIView!
    @IBOutlet weak var lblTitle1 : UILabel!
    @IBOutlet weak var lblTitle2 : UILabel!
    @IBOutlet weak var btnCalculate : UIButton!
    @IBOutlet weak var viewValidation : UIView!
    
    
    //row 1
    @IBOutlet weak var txtNoPaymnt_1 : UITextField!
    @IBOutlet weak var txtDesiredPaymnt_1 : UITextField!
    @IBOutlet weak var txtFreq_1 : UITextField!
    @IBOutlet weak var txtPAyDate_1 : UITextField!
    @IBOutlet weak var lblFinlAmt_1 : UILabel!
    
    
    @IBOutlet weak var lblFinlAmt_2 : UILabel!
    @IBOutlet weak var lblFinlAmt_3 : UILabel!
    
    @IBOutlet weak var lblApr : UILabel!
    @IBOutlet weak var lblFincnChrg : UILabel!
    @IBOutlet weak var lblAmtFinance : UILabel!
    @IBOutlet weak var lblTotlPay : UILabel!
    @IBOutlet weak var lblFinalPay : UILabel!

    
    var schdPaymtArray = NSMutableArray()
    var loanDatePicker : UIDatePicker!
    var recurDatePicker : UIDatePicker!
    
    var pickerSideNoteTyp : UIPickerView!
    var arrSideNoteTyp = NSMutableArray()
    
    var pickerFreq : UIPickerView!
    var arrFreq = NSMutableArray()
    
    var pickerFreq_1 : UIPickerView!
    
    var sideNoteTypId = ""
    var payFreq = ""
    var payFreq_1 = ""
    var isDateSelected = ""
    var tag = 0
    
    override func viewDidLoad(){
        super.viewDidLoad()
        self.setData()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension sideNoteVC {
    
    func setData()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:dataD)
                if let account_type = dataD["account_type"] as? String{
                    self.lblAcountTyp.text = "Account Type: \(account_type)"
                }
                
                if let account_status = dataD["account_status"] as? String{
                    self.lblStatus.text = "Status: \(account_status)"
                }
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblStock.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                }
                
                
                if let contact_info = dataD["contact_info"] as? NSDictionary{
                    let email = contact_info["email"] as! String
                    self.lblEmail.text = "Email: \(email)"
                }
                
                if let promiseToPay = dataD["promiseToPay"] as? NSDictionary{
                    
                    let promiseAmount = promiseToPay["promiseAmount"] as! NSNumber
                    let promiseDate = promiseToPay["promiseDate"] as! String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM-dd-yyyy"
                    let date = Date.dateFromISOString(string: promiseDate)
                    let strDate = formatter.string(from: date!)
                    let promiseStatus = promiseToPay["promiseStatus"] as! String
                    if(promiseStatus == "Kept"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Kept Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                    else if(promiseStatus == "Open"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Open Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                    else if(promiseStatus == "Broken"){
                        viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                        lblPromise.textColor = .white
                        lblPromise.text = "Broken Promise To Pay $\(promiseAmount) by \(strDate)"
                    }
                }
                
                let dict1 = ["title":"Side Note","_id": "2"]
                let dict2 = ["title":"CPI Balance","_id": "3"]
                let dict3 = ["title":"Recurring CPI","_id": "4"]
                
                self.arrSideNoteTyp.add(dict1)
                self.arrSideNoteTyp.add(dict2)
                self.arrSideNoteTyp.add(dict3)
                
                
                let dict_1 = ["title":"Weekly","_id": "W"]
                let dict_2 = ["title":"Bi-Weekly","_id": "B"]
                let dict_3 = ["title":"Monthly","_id": "M"]
                
                self.arrFreq.add(dict_1)
                self.arrFreq.add(dict_2)
                self.arrFreq.add(dict_3)
                
                self.viewRecurng.isHidden = true
                
                self.viewSchd.isHidden = false
                self.viewDisclosure.isHidden = false
                self.lblTitle1.isHidden = false
                self.lblTitle2.isHidden = false
                self.btnCalculate.isHidden = false
                
                let date = Date()
                
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                
                let curDate = formatter.string(from: date)
                self.txtLoanDate.text = curDate
                
                let calendar = Calendar.current
                let addOneWeekToCurrentDate = calendar.date(byAdding: .weekOfYear, value: 1, to: Date())
                let weekDate = formatter.string(from: addOneWeekToCurrentDate!)
                self.txtPAyDate_1.text = weekDate
                
                let dict = arrFreq[0] as! NSDictionary
                self.txtFreq_1.text = (dict["title"] as! String)
                self.payFreq_1 = dict["_id"] as! String

                
                self.viewValidation.isHidden = true
            }
        }
        catch {
        }
    }
}

extension sideNoteVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        if(tag == 10){
            
            if(txtDescription.text == "" || txtDescription.text?.count == 0 || txtDescription.text == nil){
                txtDescription.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Description.")
            }
            else if(txtRecurngAmt.text == "" || txtRecurngAmt.text?.count == 0 || txtRecurngAmt.text == nil){
                txtRecurngAmt.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Amount.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                Globalfunc.showLoaderView(view: self.view)
                self.setParamtoCallApi()
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else if(tag == 20){
            
            
        }
        
        
    }
    
    
    
    
    func setParamtoCallApi()
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                if(tag == 10){
                    let params = [
                        "account_id": accId_selected,
                        "description": "\(txtDescription.text!)",
                        "loanAmount": "\(txtRecurngAmt.text!)",
                        "userid":userid,
                        "insid":insID,
                        "loanDate": "\(txtRecurngDate.text!)",
                        "sideNoteType": "4",
                        "paymentFrequency":payFreq] as [String : Any]
                    Globalfunc.print(object: params)
                    self.sendDatatoPostApi(param: params, StrCheck: "post", strUrl: Constant.side_note)
                }
                else if(tag == 20){
                    
                }
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any], StrCheck: String,strUrl : String)
    {
        BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
            
            if(StrCheck == "post"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
            if(StrCheck == "email"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewDidLoad()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            if(StrCheck == "calc"){
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        
                        if let respnseDict = dict as? NSDictionary {
                            
                            print(respnseDict)
                            
                            self.lblApr.backgroundColor = .systemGreen
                            self.lblFincnChrg.backgroundColor = .systemGreen
                            self.lblAmtFinance.backgroundColor = .systemGreen
                            self.lblTotlPay.backgroundColor = .systemGreen
                            self.lblFinalPay.backgroundColor = .systemGreen
                            self.lblFinlAmt_1.backgroundColor = .systemGreen
                            
                            self.lblFinlAmt_2.backgroundColor = .systemGreen
                            self.lblFinlAmt_3.backgroundColor = .systemGreen
                            
                            if let apr = respnseDict["apr"] as? NSNumber{
                                self.lblApr.text = "\(apr)"
                            }
                            
                            if let financeCharged = respnseDict["financeCharged"] as? NSNumber{
                                self.lblFincnChrg.text = "\(financeCharged)"
                            }
                            
                            if let amountFinanced = respnseDict["amountFinanced"] as? NSNumber{
                                self.lblAmtFinance.text = "$\(amountFinanced)"
                            }
                            
                            if let totalPayment = respnseDict["totalPayment"] as? NSNumber{
                                self.lblTotlPay.text = "$\(totalPayment)"
                            }
                            
                            if let finalPayment = respnseDict["finalPayment"] as? NSNumber{
                                self.lblFinalPay.text = "$\(finalPayment)"
                            }
                            
                            if let calculatedPayment = respnseDict["calculatedPayment"] as? NSArray{
                                let dict = calculatedPayment[0] as! NSDictionary
                                if let payment = dict["payment"] as? NSNumber{
                                    self.lblFinlAmt_1.text = "$\(payment)"
                                }
                            }
                            
                            
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }

            }
        }
    }
    
    
    @IBAction func clickONEmailBtn(_ sender : UIButton)
    {
        let alertController = UIAlertController(title: "Edit Email Address", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email Address"
        }
        let saveAction = UIAlertAction(title: "Edit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            Globalfunc.showLoaderView(view: self.view)
            self.updateemailApi(firstTextField.text!)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func updateemailApi(_ strText : String)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary{
                
                let contact_info = dataD["contact_info"] as! NSMutableDictionary
                contact_info.setValue(strText, forKey: "email")
                dataD.setValue(contact_info, forKey: "contact_info")
                do {
                    if #available(iOS 11.0, *) {
                        let myData = try NSKeyedArchiver.archivedData(withRootObject: dataD, requiringSecureCoding: false)
                        userDef.set(myData, forKey: "dataDict")
                    } else {
                        let myData = NSKeyedArchiver.archivedData(withRootObject: dataD)
                        userDef.set(myData, forKey: "dataDict")
                    }
                } catch {
                    Globalfunc.print(object: "Couldn't write file")
                }
                
                
                let accId_selected = dataD["_id"] as! Int
                let params = [
                    "email": strText,
                    "id": accId_selected] as [String : Any]
                self.sendDatatoPostApi(param: params, StrCheck: "email",strUrl: Constant.detail_contact_info)
            }
        }
        catch{
        }
    }
}

/*MARk - Textfeld and picker delegates*/

extension sideNoteVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerSideNoteTyp{
            return self.arrSideNoteTyp.count
        }
        else if pickerView == pickerFreq{
            return self.arrFreq.count
        }
        else if pickerView == pickerFreq_1{
            return self.arrFreq.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerSideNoteTyp{
            let dict = arrSideNoteTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
            
        }
        else if pickerView == pickerFreq{
            let dict = arrFreq[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if pickerView == pickerFreq_1{
                let dict = arrFreq[row] as! NSDictionary
                let strTitle = dict["title"] as! String
                return strTitle
        }
       
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerSideNoteTyp{
            let dict = self.arrSideNoteTyp[row] as! NSDictionary
            self.sideNoteTypId = dict["_id"] as! String
            let strTitle = dict["title"] as! String
            self.txtsideNoteTyp.text = strTitle
            if(strTitle == "Recurring CPI"){
                tag = 10
                self.viewRecurng.isHidden = false
                self.viewSchd.isHidden = true
                self.viewDisclosure.isHidden = true
                self.lblTitle1.isHidden = true
                self.lblTitle2.isHidden = true
                self.btnCalculate.isHidden = true
            }
            else{
                tag = 20
                self.viewRecurng.isHidden = true
                self.viewSchd.isHidden = false
                self.viewDisclosure.isHidden = false
                self.lblTitle1.isHidden = false
                self.lblTitle2.isHidden = false
                self.btnCalculate.isHidden = false
            }
        }
        else if pickerView == pickerFreq{
            let dict = arrFreq[row] as! NSDictionary
            self.payFreq = dict["_id"] as! String
            self.txtRecurngFrequncy.text = (dict["title"] as! String)
        }
        else if pickerView == pickerFreq_1{
                let dict = arrFreq[row] as! NSDictionary
                let strTitle = dict["title"] as! String
            self.txtFreq_1.text = strTitle
            self.payFreq_1 = dict["_id"] as! String
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtsideNoteTyp){
            self.pickUp(txtsideNoteTyp)
        }
        else if(textField == self.txtRecurngFrequncy){
            self.pickUp(txtRecurngFrequncy)
        }
        else if(textField == self.txtFreq_1){
                self.pickUp(txtFreq_1)
        }
        else if(textField == self.txtLoanDate){
            self.pickUpDateTime(txtLoanDate)
        }
        else if(textField == self.txtRecurngDate){
            self.pickUpDateTime(txtRecurngDate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtsideNoteTyp){
            self.pickerSideNoteTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSideNoteTyp.delegate = self
            self.pickerSideNoteTyp.dataSource = self
            self.pickerSideNoteTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerSideNoteTyp
        }
        else if(textField == self.txtRecurngFrequncy){
            self.pickerFreq = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerFreq.delegate = self
            self.pickerFreq.dataSource = self
            self.pickerFreq.backgroundColor = UIColor.white
            textField.inputView = self.pickerFreq
        }
        else if(textField == self.txtFreq_1){
            self.pickerFreq_1 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerFreq_1.delegate = self
            self.pickerFreq_1.dataSource = self
            self.pickerFreq_1.backgroundColor = UIColor.white
            textField.inputView = self.pickerFreq_1
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtsideNoteTyp.resignFirstResponder()
        txtRecurngFrequncy.resignFirstResponder()
        txtFreq_1.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtsideNoteTyp.resignFirstResponder()
        txtRecurngFrequncy.resignFirstResponder()
        txtFreq_1.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        if(textField == self.txtLoanDate){
            self.loanDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.loanDatePicker.datePickerMode = .date
            self.loanDatePicker.backgroundColor = UIColor.white
            textField.inputView = self.loanDatePicker
            isDateSelected = "loan"
        }
        else if(textField == self.txtRecurngDate){
            self.recurDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.recurDatePicker.datePickerMode = .date
            self.recurDatePicker.backgroundColor = UIColor.white
            textField.inputView = self.recurDatePicker
            isDateSelected = "recur"
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        if(isDateSelected == "loan"){
            self.txtLoanDate.text = formatter.string(from: loanDatePicker.date)
            self.txtLoanDate.resignFirstResponder()
        }
        else if(isDateSelected == "recur"){
            self.txtRecurngDate.text = formatter.string(from: recurDatePicker.date)
            self.txtRecurngDate.resignFirstResponder()
        }
    }
    
    @objc func cancelClickDate() {
        txtLoanDate.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if(textField == self.txtLoanamt || textField == txtNoPaymnt_1 || textField == txtDesiredPaymnt_1){
            
            self.lblApr.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblFincnChrg.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblAmtFinance.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblTotlPay.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblFinalPay.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblFinlAmt_1.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            
            self.lblFinlAmt_2.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
            self.lblFinlAmt_3.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)

            if let text = textField.text,
                let textRange = Range(range, in: text) {
                let _ = text.replacingCharacters(in: textRange,
                                                       with: string)
                self.viewValidation.isHidden = true
            }
        }
        return true
    }
        
}

//calculation
extension sideNoteVC {
    
    @IBAction func btnCalculate(_ sender: UIButton)
    {
        if(txtLoanamt.text == ""){
            self.viewValidation.isHidden = false
        }
       else if(txtNoPaymnt_1.text == "" && txtDesiredPaymnt_1.text == ""){
          self.viewValidation.isHidden = false
       }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.calculationFunc()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func calculationFunc()
    {
        self.schdPaymtArray = []
        
        let desiredPay = Double(txtDesiredPaymnt_1.text!)
        let payFreq = self.payFreq_1
        let firstPayDate = self.txtPAyDate_1.text
        let loanAmt = Double(txtLoanamt.text!)
        var loanRate = Double(txtLoanRate.text!)
        
        if(loanRate == nil){
            loanRate = 0
        }
        var calcNoOfPay = 0.0
        
        if(desiredPay == nil)
        {
            txtDesiredPaymnt_1.text = "0"
            let arrDict : NSMutableDictionary = [:]
            arrDict.setValue(Double(txtNoPaymnt_1.text!), forKey: "payment_term")
            arrDict.setValue(0, forKey: "desired_payment")
            arrDict.setValue(payFreq, forKey: "payment_frequency")
            arrDict.setValue(firstPayDate, forKey: "first_payment_date")
            self.schdPaymtArray.add(arrDict)
        }
        else{
            calcNoOfPay = loanAmt! / desiredPay!
                //payedLoanAmount = desiredPay! * calcNoOfPay
            let calPtunc = NSNumber(value: calcNoOfPay)
            calcNoOfPay = Double(truncating: calPtunc).rounded(toPlaces: 2)
            
            let arrDict : NSMutableDictionary = [:]
            arrDict.setValue(calcNoOfPay, forKey: "payment_term")
            arrDict.setValue(desiredPay, forKey: "desired_payment")
            arrDict.setValue(payFreq, forKey: "payment_frequency")
            arrDict.setValue(firstPayDate, forKey: "first_payment_date")
            self.schdPaymtArray.add(arrDict)
            
            self.txtNoPaymnt_1.text = "\(calcNoOfPay)"

        }
            
            let param = ["principal" : loanAmt!,
                         "loan_rate" :loanRate!,
                         "loan_date" :"\(txtLoanDate.text!)",
                         "sales_tax" :0,
                         "payment_sched": self.schdPaymtArray] as [String : Any]
            
            print(param)
            
            self.sendDatatoPostApi(param: param, StrCheck: "calc", strUrl: Constant.side_note_calc)
    }
}

