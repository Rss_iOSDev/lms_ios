//
//  chargeOffReversalVC.swift
//  LMS
//
//  Created by Apple on 12/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class chargeOffReversalVC: UIViewController {

    @IBOutlet weak var txtNotes : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension chargeOffReversalVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoCallApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let params = [
                    "account_id": accId_selected,
                    "notes": "\(txtNotes.text!)",
                    "userid": userid,
                    "insid": insID] as [String : Any]
                
                Globalfunc.print(object:params)
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.reverse_chargeOff, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
