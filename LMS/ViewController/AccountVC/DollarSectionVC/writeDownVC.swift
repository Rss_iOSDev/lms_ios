//
//  writeDownVC.swift
//  LMS
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class writeDownVC: UIViewController {

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblAcountTyp : UILabel!
    @IBOutlet weak var lblLoan : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblTags : UILabel!
    
    @IBOutlet weak var txtBalType : UITextField!
    @IBOutlet weak var txtCommnts : UITextField!
    
    
    
    @IBOutlet weak var lblDownPayBal : UILabel!
    @IBOutlet weak var lblPrinBal : UILabel!
    @IBOutlet weak var lblSalesTaxBal : UILabel!
    @IBOutlet weak var lblNonEarnPrinBal : UILabel!
    @IBOutlet weak var lblAccIntrstBal : UILabel!
    @IBOutlet weak var lblLateFeeBal : UILabel!
    @IBOutlet weak var lblMiscFeeBal : UILabel!
    @IBOutlet weak var lblPayoff : UILabel!
    
    
    var pickerBalType : UIPickerView!
    var arrBalanceTyp = NSMutableArray()
    
    var balTypId = ""
    var sideNoteTyp = ""
    
    var balDetailDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension writeDownVC {
    
    func setData()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:dataD)
                if let account_type = dataD["account_type"] as? String{
                    self.lblAcountTyp.text = "Account Type: \(account_type)"
                }
                
                if let account_status = dataD["account_no"] as? String{
                    self.lblStatus.text = "Account: \(account_status)"
                }
                
                if let account_no = dataD["account_no"] as? String{
                    self.lblStock.text = "Account #: \(account_no)"
                }
                
                if let loan_no = dataD["loan_no"] as? String{
                    self.lblLoan.text = "Loan #: \(loan_no)"
                }
                
                self.lblTags.text = "Tags: "
               
                
                
                let dict1 =  ["description" :"All Balances","_id": "0", "sideNoteType": "0"]
                arrBalanceTyp.add(dict1)
                
                if let balanceType = dataD["balanceType"] as? [NSDictionary]{
                    for i in 0..<balanceType.count{
                        let dict = balanceType[i]
                        self.arrBalanceTyp.add(dict)
                    }
                }
                
                let dict = self.arrBalanceTyp[0] as! NSDictionary
                let strTitle = dict["description"] as! String
                self.txtBalType.text = strTitle
                if let _id = dict["_id"] as? String{
                    self.balTypId = _id
                }
                if let sideNoteType = dict["sideNoteType"] as? String{
                    self.sideNoteTyp = sideNoteType
                }
                
                if let identity_info = dataD["identity_info"] as? NSDictionary{
                    let first_name = identity_info["first_name"] as! String
                    let last_name = identity_info["last_name"] as! String
                    self.lblName.text = "\(first_name) \(last_name)"
                }
                               
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    self.callGertDetailApi()
                })
            }
        }
        catch {
            
        }
    }
}

extension writeDownVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
       if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoCallApi()
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let params = [
                    "account_id": accId_selected,
                    "balanceType": self.balTypId,
                    "comments": "\(txtCommnts.text!)",
                    "sideNoteType": self.sideNoteTyp,
                    "userid":userid,
                    "insid":insID,
                    "accountData":self.balDetailDict] as [String : Any]
                Globalfunc.print(object: params)
                self.sendDatatoPostApi(param: params, strUrl: Constant.post_writedown)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any],strUrl : String)
    {
        BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
            
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
        }
    }
}

/*MARk - Textfeld and picker delegates*/

extension writeDownVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerBalType{
            return self.arrBalanceTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            self.txtBalType.text = strTitle
            if let _id = dict["_id"] as? String{
                self.balTypId = _id
            }
            if let _id = dict["_id"] as? Int{
                self.balTypId = "\(_id)"
            }
            
            if let sideNoteType = dict["sideNoteType"] as? Int{
                self.sideNoteTyp = "\(sideNoteType)"
            }
            
            txtBalType.resignFirstResponder()
            self.callGertDetailApi()
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtBalType){
            self.pickUp(txtBalType)
        }
            
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBalType){
            
            self.pickerBalType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBalType.delegate = self
            self.pickerBalType.dataSource = self
            self.pickerBalType.backgroundColor = UIColor.white
            textField.inputView = self.pickerBalType
        }
            
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtBalType.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtBalType.resignFirstResponder()
    }
    
    
    func callGertDetailApi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_id = dataD["_id"] as! Int
                let strUrl = "\(Constant.account_details)?account_id=\(account_id)&balance_type=\(self.balTypId)&sideNoteType=\(self.sideNoteTyp)"
                Globalfunc.print(object:strUrl)
                self.callGetApi(strUrl: strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension writeDownVC {
    
    func callGetApi(strUrl: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let responseDict = dict as? NSDictionary {
                            
                            self.balDetailDict = responseDict
                            
                            if let downPayment = responseDict["downPayment"] as? NSNumber{
                                self.lblDownPayBal.text = "Down Payment Balance: $\(Double(truncating: downPayment).rounded(toPlaces: 3))"
                            }
                            
                            if let lateFees = responseDict["lateFees"] as? NSNumber{
                                self.lblLateFeeBal.text = "Late Fees Balance: $\(Double(truncating: lateFees).rounded(toPlaces: 3))"
                            }
                            
                            if let prin = responseDict["prin"] as? NSNumber{
                                self.lblPrinBal.text = "Principal Balance: $\(Double(truncating: prin).rounded(toPlaces: 3))"
                            }
                            
                            if let salesTax = responseDict["salesTax"] as? NSNumber{
                                self.lblSalesTaxBal.text = "Sales Tax Balance: $\(Double(truncating: salesTax).rounded(toPlaces: 3))"
                            }
                            
                            if let nonEarnPrin = responseDict["nonEarnPrin"] as? NSNumber{
                                self.lblNonEarnPrinBal.text = "Non Interest Earning Balance: $\(Double(truncating: nonEarnPrin).rounded(toPlaces: 3))"
                            }
                            
                            if let interest = responseDict["interest"] as? NSNumber{
                                self.lblAccIntrstBal.text = "Accrued Interest Balance: $\(Double(truncating: interest).rounded(toPlaces: 3))"
                            }
                            
                            if let miscFees = responseDict["miscFees"] as? NSNumber{
                                self.lblMiscFeeBal.text = "Misc Fees Balance: $\(Double(truncating: miscFees).rounded(toPlaces: 3))"
                            }
                            
                            if let total = responseDict["total"] as? NSNumber{
                                self.lblPayoff.text = "Total Writedown: $\(Double(truncating: total).rounded(toPlaces: 3))"
                            }
                        }
                    }
                }
        }
    }
}
