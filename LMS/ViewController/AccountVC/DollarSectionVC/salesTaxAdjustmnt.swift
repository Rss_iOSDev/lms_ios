//
//  salesTaxAdjustmnt.swift
//  LMS
//
//  Created by Apple on 06/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class salesTaxAdjustmnt: UIViewController {
    
    @IBOutlet weak var txtDescription : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension salesTaxAdjustmnt {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        if(txtDescription.text == "" || txtDescription.text?.count == 0 || txtDescription.text == nil){
            txtDescription.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoCallApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                Globalfunc.print(object:dataD)
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let balanceType = dataD["balanceType"] as! NSArray
                let bal_dict = balanceType[0] as! NSDictionary
                let bal_id = bal_dict["_id"] as! Int
                
                let params = [
                    "account_id": accId_selected,
                    "paymentType": 2,
                    "paymentSubType": 1,
                    "balanceType": bal_id,
                    "description": "\(txtDescription.text!)",
                    "adjustment": 5,
                    "userid": userid,
                    "insid": insID ] as [String : Any]
                
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.post_adjustment, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
