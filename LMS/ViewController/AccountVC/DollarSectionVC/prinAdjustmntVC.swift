//
//  prinAdjustmntVC.swift
//  LMS
//
//  Created by Apple on 07/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class prinAdjustmntVC: UIViewController {
    
    @IBOutlet weak var txtBalTyp : UITextField!
    @IBOutlet weak var txtAdjustmntTyp : UITextField!
    @IBOutlet weak var txtDescription : UITextField!
    @IBOutlet weak var txtAdjustmntamt : UITextField!
    @IBOutlet weak var txtReasonCode : UITextField!
    
    @IBOutlet weak var lblMsgAlert : UILabel!
    @IBOutlet weak var btnPost : UIButton!
    @IBOutlet weak var btnAdjustDisable : UIButton!
    
    
    
    var pickerBalType : UIPickerView!
    var arrBalanceTyp = NSMutableArray()
    var balTypId = ""
    
    var pickerAdjustmtTyp : UIPickerView!
    var arrAdjystTyp = NSMutableArray()
    var adjustmntId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBalTypArr()
    }
    
    @IBAction func clickOnBack(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension prinAdjustmntVC {
    
    func setBalTypArr()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                Globalfunc.print(object:dataD)
                
                self.lblMsgAlert.isHidden = true
                self.btnPost.isHidden = false
                self.btnAdjustDisable.isHidden = true
                
                
                let dict1 =  ["title" :"Increase","_id": "1"]
                let dict2 =  ["title" :"Decrease","_id": "2"]
                
                self.arrAdjystTyp.add(dict1)
                self.arrAdjystTyp.add(dict2)
                
                if let balanceType = dataD["balanceType"] as? [NSDictionary]{
                    
                    if(balanceType.count > 0){
                        for i in 0..<balanceType.count{
                            let dict = balanceType[i]
                            self.arrBalanceTyp.add(dict)
                        }
                        let dict = self.arrBalanceTyp[0] as! NSDictionary
                        let strTitle = dict["description"] as! String
                        self.txtBalTyp.text = strTitle
                        if let _id = dict["_id"] as? String{
                            self.balTypId = _id
                        }
                    }
                }
            }
        }
        catch{
            
        }
    }
    
    func setDecraeseIncreaseVal(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let totalPrincipal = dataD["totalPrincipal"] as! NSNumber
                if(totalPrincipal == 0.0){
                    self.lblMsgAlert.isHidden = false
                    self.btnPost.isHidden = true
                    self.btnAdjustDisable.isHidden = false
                }
                else{
                    self.lblMsgAlert.isHidden = true
                    self.btnPost.isHidden = false
                    self.btnAdjustDisable.isHidden = true
                }
            }
            
        }
        catch{
            
        }
    }
    
    
}


/*MARk - Textfeld and picker delegates*/

extension prinAdjustmntVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerBalType{
            return self.arrBalanceTyp.count
        }
        else if(pickerView == pickerAdjustmtTyp){
            return self.arrAdjystTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerAdjustmtTyp){
            let dict = arrAdjystTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            self.txtBalTyp.text = strTitle
            if let _id = dict["_id"] as? String{
                self.balTypId = _id
            }
            if let _id = dict["_id"] as? Int{
                self.balTypId = "\(_id)"
            }
        }
            
        else if(pickerView == pickerAdjustmtTyp){
            let dict = arrAdjystTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            
            if let _id = dict["_id"] as? String{
                self.adjustmntId = _id
            }
            
            if let _id = dict["_id"] as? Int{
                self.adjustmntId = "\(_id)"
            }
            self.txtAdjustmntTyp.text = strTitle
            if(strTitle == "Decrease"){
                self.setDecraeseIncreaseVal()
            }
            else if(strTitle == "Increase"){
                self.lblMsgAlert.isHidden = true
                self.btnPost.isHidden = false
                self.btnAdjustDisable.isHidden = true
            }
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtBalTyp){
            self.pickUp(txtBalTyp)
        }
            
        else if(textField == self.txtAdjustmntTyp){
            self.pickUp(txtAdjustmntTyp)
        }
        
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBalTyp){
            
            self.pickerBalType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBalType.delegate = self
            self.pickerBalType.dataSource = self
            self.pickerBalType.backgroundColor = UIColor.white
            textField.inputView = self.pickerBalType
        }
            
        else if(textField == self.txtAdjustmntTyp){
            
            self.pickerAdjustmtTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAdjustmtTyp.delegate = self
            self.pickerAdjustmtTyp.dataSource = self
            self.pickerAdjustmtTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerAdjustmtTyp
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    //MARK:- Button
    @objc func doneClick() {
        txtBalTyp.resignFirstResponder()
        txtAdjustmntTyp.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtBalTyp.resignFirstResponder()
        txtAdjustmntTyp.resignFirstResponder()
    }
}

extension prinAdjustmntVC {
    
    @IBAction func clickOnPstBtn(_ sender: UIButton)
    {
        if(txtAdjustmntamt.text == "" || txtAdjustmntamt.text?.count == 0 || txtAdjustmntamt.text == nil){
            txtAdjustmntamt.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Enter Adjustment Amount.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func setParamtoCallApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                Globalfunc.print(object:dataD)
                
                let accId_selected = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                let result = formatter.string(from: date)
                
                let params = [
                    "account_id": accId_selected,
                    "paymentType": 6,
                    "adjustment": 6,
                    "dueDate": result,
                    "paymentSubType": 1,
                    "balanceType": self.balTypId,
                    "adjustmentType": self.adjustmntId,
                    "description": "\(txtDescription.text!)",
                    "adjustmentAmount": "\(txtAdjustmntamt.text!)",
                    "reasonCode": "\(txtReasonCode.text!)",
                    "userid": userid,
                    "insid": insID] as [String : Any]
                
                Globalfunc.print(object:params)
                
                self.sendDatatoPostApi(param: params)
            }
        }
        catch {
        }
    }
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.post_adjustment, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
