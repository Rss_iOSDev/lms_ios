//
//  text_sendVC.swift
//  LMS
//
//  Created by Apple on 07/01/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class text_sendVC: UIViewController {
    
    
    @IBOutlet weak var txtSchldDate : UITextField!
    @IBOutlet weak var txtTemplate : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    
    
    var pickerViewTemplate: UIPickerView!
    var arrTemplate : NSMutableArray = []
    
    var pvEffDate : UIDatePicker!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var phoneNo = ""
    var account_no : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAccountNo.text = "Account No: \(account_no)"
        Globalfunc.showLoaderView(view: self.view)
        
        let url = "\(Constant.getTemplate_url)&templateType=2"
        self.getTemplateList(strUrl: url, strTyp: "template")
    }
    
    
    
    @IBAction func clikcONSendEmail(_ sender: UIButton){
        if(sender.tag == 50){
            Globalfunc.showLoaderView(view: self.view)
            self.postApiToSendEmail()
        }
        else if(sender.tag == 60){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func clikcONBackbtn(_ sender: UIButton){
        self.view.window!.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
}

extension text_sendVC {
    
    func getTemplateList(strUrl : String, strTyp: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if(strTyp == "template"){
                        if let response = dict as? NSDictionary{
                            if let responseDict = response["data"] as? NSMutableArray {
                                self.arrTemplate = responseDict
                            }
                        }
                    }
                    else if(strTyp == "temp_select"){
                        if let responseDict = dict as? NSDictionary {
                            if let response = responseDict["data"] as? NSDictionary {
                                if let htmlTemplate = response["htmlTemplate"] as? String{
                                    self.txtNotes.attributedText = htmlTemplate.htmlToAttributedString
                                    self.txtNotes.isUserInteractionEnabled = true
                                    self.txtNotes.isEditable = true
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func postApiToSendEmail(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acctId = dataD["_id"] as! Int
                
                //                let param = ["account_id": acctId,
                //                             "email": "\(email)",
                //                    "htmlString": "\(txtMEssage.text!)",
                //                    "attachment":attachFile,
                //                    "subject": "\(self.txtEmailSubject.text!)"] as [String : Any]
                //                Globalfunc.print(object:param)
                //                self.sendDatatoPost(param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func sendDatatoPost(_ param: [String: Any])
    {
        print(Constant.send_url)
        BaseApi.onResponsePostWithToken(url: Constant.send_url, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        let msg = responseDict["msg"] as! String
                        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true) {
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}



extension text_sendVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewTemplate{
            return self.arrTemplate.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerViewTemplate{
            let dict = arrTemplate[row] as! NSDictionary
            let strTitle = dict["templateName"] as! String
            return strTitle
        }
        
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerViewTemplate{
            let dict = arrTemplate[row] as! NSDictionary
            self.txtTemplate.text = (dict["templateName"] as! String)
            self.txtTemplate.tag = row
            
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtTemplate){
            self.pickUp(txtTemplate)
        }
        else if(textField == self.txtSchldDate){
            self.pickUpDateTime(txtSchldDate)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtTemplate){
            self.pickerViewTemplate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTemplate.delegate = self
            self.pickerViewTemplate.dataSource = self
            self.pickerViewTemplate.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTemplate
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtSchldDate){
            self.pvEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvEffDate.datePickerMode = .date
            self.pvEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pvEffDate
        }
    }
    
    
    //MARK:- Button
    @objc func doneClick(){
        txtTemplate.resignFirstResponder()
        
        let dict = arrTemplate[txtTemplate.tag] as! NSDictionary
        let _id = dict["_id"] as! Int
        let url = "\(Constant.getTemplate_layout_url)?id=\(_id)"
        Globalfunc.showLoaderView(view: self.view)
        self.getTemplateList(strUrl: url, strTyp: "temp_select")
        
    }
    
    @objc func cancelClick() {
        txtTemplate.resignFirstResponder()
        txtTemplate.text = ""
    }
    
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtSchldDate.text = formatter.string(from: pvEffDate.date)
        self.txtSchldDate.resignFirstResponder()
    }
    
    @objc func cancelClickDate() {
        txtSchldDate.resignFirstResponder()
    }
    
}
