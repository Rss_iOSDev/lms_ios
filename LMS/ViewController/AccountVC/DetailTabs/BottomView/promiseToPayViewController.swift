//
//  promiseToPayViewController.swift
//  LMS
//
//  Created by Apple on 07/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

var arrAddPromise : NSMutableArray = []
var arrBalanceTyp : NSMutableArray = []
var arr_btn : [UIButton] = []



class promiseToPayViewController: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    @IBOutlet weak var lblSummary : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblPaymnt : UILabel!
    @IBOutlet weak var lblDue : UILabel!
    @IBOutlet weak var txtContact : UITextField!
    
    var arrContactTyp : NSMutableArray = []
    var pickerContact : UIPickerView!
    var acct_id : Int?
    
    @IBOutlet weak var viewTable : UIView!
    @IBOutlet weak var tblAddPromise : UITableView!
    @IBOutlet weak var viewAlert : UIView!
    @IBOutlet weak var lblMsg : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrAddPromise = []
        self.viewAlert.isHidden = true
        
        let dict1 = ["description" :"All Balances","_id": "0"]
        arrBalanceTyp.add(dict1)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let account_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account No: \(account_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        Globalfunc.showLoaderView(view: self.view)
        self.callPromiseToPayApi()
    }
    
    @IBAction func clickONAddEpsBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("epsViewController", strStoryName: "ActionStory")
    }
    
    @IBAction func clickONAddBtn(_ sender: UIButton)
    {
        let dictTblCell : NSMutableDictionary = [:]
        dictTblCell.setValue("", forKey: "account")
        dictTblCell.setValue("", forKey: "account_id")
        dictTblCell.setValue("", forKey: "amount")
        dictTblCell.setValue("", forKey: "balanceType")
        dictTblCell.setValue("", forKey: "note")
        dictTblCell.setValue("", forKey: "postDate")
        
        let prmisetyp : NSMutableDictionary = [:]
        prmisetyp.setValue("1", forKey: "id")
        prmisetyp.setValue("Payment", forKey: "title")
        dictTblCell.setValue(prmisetyp, forKey: "promiseType")
        
        let del_mthd : NSMutableDictionary = [:]
        del_mthd.setValue("", forKey: "id")
        del_mthd.setValue("", forKey: "title")
        dictTblCell.setValue(del_mthd, forKey: "deliveryMethod")
        
        let pay_form : NSMutableDictionary = [:]
        pay_form.setValue("", forKey: "id")
        pay_form.setValue("", forKey: "title")
        dictTblCell.setValue(pay_form, forKey: "paymentFrom")
        
        dictTblCell.setValue("", forKey: "isGetData")
        dictTblCell.setValue("", forKey: "_id")
        
        dictTblCell.setValue("", forKey: "promiseStatus")
        
        
        if(arrAddPromise.count == 0){
            arrAddPromise.add(dictTblCell)
        }
        else{
            arrAddPromise.insert(dictTblCell, at: 0)
        }
        
        for btn in arr_btn{
            btn.isHidden = true
        }
        
        self.tblAddPromise.reloadData()
    }
    
    @IBAction func clinOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        for i in 0..<arrAddPromise.count{
            
            let dict = arrAddPromise[i] as! NSMutableDictionary
            
            Globalfunc.print(object:dict)
            
            let isGetData = dict["isGetData"] as! String
            if(isGetData == ""){
                
                let amount = dict["amount"] as! String
                let deliveryMethod = dict["deliveryMethod"] as! NSDictionary
                let Delivrytitle = deliveryMethod["title"] as! String
                
                let paymentFrom = dict["paymentFrom"] as! NSDictionary
                let _ = paymentFrom["title"] as! String
                
                if(amount == ""){
                    Globalfunc.showToastWithMsg(view: self.view, str: "Please fill Amount.")
                }
                else if(Delivrytitle == ""){
                    Globalfunc.showToastWithMsg(view: self.view, str: "Please select Delivery Method.")
                }
                    //else if(Paymnttitle == ""){
                    //    Globalfunc.showToastWithMsg(view: self.view, str: "Please select Payment Form.")
                    //}
                else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                    Globalfunc.showLoaderView(view: self.view)
                    callPostApitoSavePromise()
                }
                else{
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
                }
            }
        }
        //        else if(!(Globalfunc.isValidEmail(testStr: txtUSername.text!))){
        //            txtUSername.shake()
        //            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
        //        }
        //        else if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
        //            txtPassword.shake()
        //            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill password field.")
        //        }
        
        
        
    }
    
    func callPostApitoSavePromise(){
        
        let arrParam : NSMutableArray = []
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                
                let insid = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                
                for i in 0..<arrAddPromise.count{
                    
                    let dict = arrAddPromise[i] as! NSMutableDictionary
                    
                    Globalfunc.print(object:dict)
                    let isGetData = dict["isGetData"] as! String
                    if(isGetData == ""){
                        let deliveryMethod = dict["deliveryMethod"] as! NSMutableDictionary
                        if let id = deliveryMethod["id"] as? Int{
                            dict.removeObject(forKey: "deliveryMethod")
                            dict.setValue(id, forKey: "deliveryMethod")
                        }
                        if let id = deliveryMethod["id"] as? String{
                            dict.removeObject(forKey: "deliveryMethod")
                            dict.setValue(id, forKey: "deliveryMethod")
                        }
                        let paymentFrom = dict["paymentFrom"] as! NSMutableDictionary
                        if let pay_id = paymentFrom["id"] as? Int{
                            dict.removeObject(forKey: "paymentFrom")
                            dict.setValue(pay_id, forKey: "paymentFrom")
                        }
                        if let pay_id = paymentFrom["id"] as? String{
                            dict.removeObject(forKey: "paymentFrom")
                            dict.setValue(pay_id, forKey: "paymentFrom")
                        }
                        if let promTyp = dict["promiseType"] as? NSMutableDictionary{
                            let promise_id = promTyp["id"] as! String
                            dict.removeObject(forKey: "promiseType")
                            dict.setValue(promise_id, forKey: "promiseType")
                        }
                        else{
                            let promiseType = dict["promiseType"] as! Int
                            dict.setValue(promiseType, forKey: "promiseType")
                        }
                        dict.removeObject(forKey: "isGetData")
                        dict.removeObject(forKey: "_id")
                        dict.removeObject(forKey: "promiseStatus")
                        
                        let account_id = dataD["_id"] as! Int
                        dict.setValue(account_id, forKey: "account_id")
                        
                        arrParam.add(dict)
                    }
                }
                Globalfunc.print(object:arrParam)
                let param = ["insid": "\(insid)",
                    "userid": userid,
                    "contact": self.acct_id!,
                    "promiseToPay":arrParam] as [String : Any]
                Globalfunc.print(object:param)
                BaseApi.onResponsePostWithToken(url: Constant.promise_pay_url, controller: self, parms: param) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            if let responseDict = dict as? NSDictionary {
                                let msg = responseDict["msg"] as! String
                                
                                let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}
//call Api
extension promiseToPayViewController{
    
    
    func callPromiseToPayApi(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.promise_url)?account_id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            
                            if let response = dict as? NSDictionary {
                                
                                if let responseDict = response["data"] as? NSDictionary {
                                    let broken = responseDict["broken"] as? Int
                                    let kept = responseDict["kept"] as? Int
                                    let pending = responseDict["pending"] as? Int
                                    let total = responseDict["total"] as? Int
                                    self.lblSummary.text = "Summary:\(kept!) Kept/ \(broken!) Broken/ \(pending!) Pending \(total!) Total/ $0.00"
                                    
                                    let lastPaidAmount = responseDict["lastPaidAmount"] as? NSNumber
                                    if let lastPaidDate = responseDict["lastPaidDate"] as? String{
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "MM-dd-yyyy HH:mm a"
                                        let date = Date.dateFromISOString(string: lastPaidDate)
                                        let createFormat = formatter.string(from: date!)
                                        self.lblDate.text = "Last Paid Date: \(createFormat) for $\(lastPaidAmount!)"
                                    }
                                    
                                    
                                    
                                    let regularAmount = responseDict["regularAmount"] as? NSNumber
                                    let paymentFrequency = responseDict["paymentFrequency"] as? String
                                    
                                    if(paymentFrequency == "M"){
                                        self.lblPaymnt.text = "Payment: $\(regularAmount!) Monthly"
                                    }
                                    else if(paymentFrequency == "W"){
                                        self.lblPaymnt.text = "Payment: $\(regularAmount!) Weekly"
                                    }
                                    else if(paymentFrequency == "B"){
                                        self.lblPaymnt.text = "Payment: $\(regularAmount!) Bi-Weekly"
                                    }
                                    
                                    let pastDue = responseDict["pastDue"] as? NSNumber
                                    self.lblDue.text = "Past Due: $\(pastDue!) Promised: $0.00 Need: $\(pastDue!)"
                                    
                                    if let balanceType = responseDict["balanceType"] as? [NSDictionary]{
                                        
                                        if(balanceType.count > 0){
                                            for i in 0..<balanceType.count{
                                                let dict = balanceType[i]
                                                arrBalanceTyp.add(dict)
                                            }
                                        }
                                    }
                                    
                                    if let contact = responseDict["contact"] as? [NSDictionary]{
                                        
                                        userDef.set(contact, forKey: "contact")
                                        
                                        if(contact.count > 0){
                                            for i in 0..<contact.count{
                                                let dict = contact[i]
                                                self.arrContactTyp.add(dict)
                                            }
                                            
                                            let dict = self.arrContactTyp[0] as! NSDictionary
                                            let first_name = dict["first_name"] as! String
                                            let last_name = dict["last_name"] as! String
                                            let strTitle = "\(first_name) \(last_name)"
                                            self.txtContact.text = strTitle
                                            let account_id = dict["account_id"] as! Int
                                            self.acct_id = account_id
                                        }
                                    }
                                    
                                    if let promiseToPay = responseDict["promiseToPay"] as? [NSDictionary]{
                                        if(promiseToPay.count > 0){
                                            for i in 0..<promiseToPay.count{
                                                let dictTblCell : NSMutableDictionary = [:]
                                                let dictP = promiseToPay[i]
                                                
                                                if let _id = dictP["_id"] as? Int{
                                                    dictTblCell.setValue(_id, forKey: "_id")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "_id")
                                                }
                                                
                                                if let account = dictP["account"] as? String{
                                                    dictTblCell.setValue(account, forKey: "account")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "account")
                                                }
                                                if let promiseType = dictP["promiseType"] as? Int{
                                                    dictTblCell.setValue(promiseType, forKey: "promiseType")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "promiseType")
                                                }
                                                if let account_id = dictP["account_id"] as? String{
                                                    dictTblCell.setValue(account_id, forKey: "account_id")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "account_id")
                                                }
                                                
                                                if let amount = dictP["amount"] as? Int{
                                                    dictTblCell.setValue(amount, forKey: "amount")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "amount")
                                                }
                                                
                                                if let balanceType = dictP["balanceType"] as? String{
                                                    dictTblCell.setValue(balanceType, forKey: "balanceType")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "balanceType")
                                                }
                                                
                                                if let note = dictP["note"] as? String{
                                                    dictTblCell.setValue(note, forKey: "note")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "note")
                                                }
                                                
                                                if let postDate = dictP["postDate"] as? String{
                                                    let formatter = DateFormatter()
                                                    formatter.dateFormat = "MM/dd/yyyy"
                                                    let date = Date.dateFromISOString(string: postDate)
                                                    let createDate_str = formatter.string(from: date!)
                                                    dictTblCell.setValue(createDate_str, forKey: "postDate")
                                                }
                                                else{
                                                    dictTblCell.setValue("", forKey: "postDate")
                                                }
                                                
                                                if let deliveryMethod = dictP["deliveryMethod"] as? NSDictionary{
                                                    dictTblCell.setValue(deliveryMethod, forKey: "deliveryMethod")
                                                }
                                                else{
                                                    let del_mthd : NSMutableDictionary = [:]
                                                    del_mthd.setValue("", forKey: "id")
                                                    del_mthd.setValue("", forKey: "title")
                                                    dictTblCell.setValue(del_mthd, forKey: "deliveryMethod")
                                                }
                                                
                                                if let paymentFrom = dictP["paymentFrom"] as? NSDictionary{
                                                    dictTblCell.setValue(paymentFrom, forKey: "paymentFrom")
                                                }
                                                else{
                                                    let pay_form : NSMutableDictionary = [:]
                                                    pay_form.setValue("", forKey: "id")
                                                    pay_form.setValue("", forKey: "title")
                                                    dictTblCell.setValue(pay_form, forKey: "paymentFrom")
                                                }
                                                
                                                dictTblCell.setValue("true", forKey: "isGetData")
                                                
                                                if let promiseStatus = dictP["promiseStatus"] as? NSString{
                                                    dictTblCell.setValue(promiseStatus, forKey: "promiseStatus")
                                                }
                                                
                                                arrAddPromise.add(dictTblCell)
                                            }
                                            Globalfunc.print(object:arrAddPromise)
                                            self.tblAddPromise.reloadData()
                                        }
                                        else{
                                            arrAddPromise = []
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}


/*MARk - Textfeld and picker delegates*/

extension promiseToPayViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerContact{
            return self.arrContactTyp.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerContact){
            let dict = arrContactTyp[row] as! NSDictionary
            let first_name = dict["first_name"] as! String
            let last_name = dict["last_name"] as! String
            let strTitle = "\(first_name) \(last_name)"
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerContact){
            let dict = arrContactTyp[row] as! NSDictionary
            let first_name = dict["first_name"] as! String
            let last_name = dict["last_name"] as! String
            let strTitle = "\(first_name) \(last_name)"
            self.txtContact.text = strTitle
            
            let account_id = dict["account_id"] as! Int
            self.acct_id = account_id
            
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtContact){
            self.pickUp(self.txtContact)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtContact){
            self.pickerContact = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerContact.delegate = self
            self.pickerContact.dataSource = self
            self.pickerContact.backgroundColor = UIColor.white
            textField.inputView = self.pickerContact
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        self.txtContact.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        self.txtContact.resignFirstResponder()
    }
    
    
}
extension promiseToPayViewController : UITableViewDataSource , UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrAddPromise.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAddPromise.dequeueReusableCell(withIdentifier: "tblPRomiseCell") as! tblPRomiseCell
        cell.tblViewPromise = tblAddPromise
        let dict = arrAddPromise[indexPath.section] as! NSDictionary
        Globalfunc.print(object: dict)
        
        let isGetData = dict["isGetData"] as! String
        if(isGetData == "true"){
            cell.viewBlur.isHidden = false
            cell.btnCross.isHidden = true
            
            
            let note = dict["note"] as! String
            if(note == ""){
                cell.viewNotes.isHidden = true
                cell.lblNotesTitle.isHidden = true
            }
            else{
                cell.viewNotes.isHidden = false
                cell.txtNotes.text = note
                cell.lblNotesTitle.isHidden = false
            }
        }
        else{
            cell.viewBlur.isHidden = true
            cell.btnCross.isHidden = false
            
            cell.viewNotes.isHidden = false
            cell.lblNotesTitle.isHidden = false
            
            let note = dict["note"] as! String
            cell.txtNotes.text = note
            
        }
        
        if let promiseStatus = dict["promiseStatus"] as? String
        {
            if(promiseStatus == "Kept"){
                cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0x349638, alpha: 1.0)
                cell.imgP.layer.borderColor = UIColorFromHex(rgbValue: 0x349638, alpha: 1.0).cgColor
                cell.lblTitle.textColor = UIColorFromHex(rgbValue: 0x349638, alpha: 1.0)
                cell.btnDelete.isHidden = true
            }
            else if(promiseStatus == "Open"){
                cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                cell.imgP.layer.borderColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0).cgColor
                cell.lblTitle.textColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                cell.btnDelete.isHidden = false
                
            }
            else if(promiseStatus == "Broken"){
                cell.lblLine.backgroundColor = .red
                cell.imgP.layer.borderColor = (UIColor.red).cgColor
                cell.lblTitle.textColor = .red
                cell.btnDelete.isHidden = true
            }
                
            else if(promiseStatus == ""){
                cell.lblLine.backgroundColor = .black
                cell.imgP.layer.borderColor = (UIColor.black).cgColor
                cell.lblTitle.textColor = .black
                cell.lblTitle.text = ""
                cell.btnDelete.isHidden = true
            }
        }
        
        
        if let promTyp = dict["promiseType"] as? NSDictionary{
            
            let strTitle = promTyp["title"] as! String
            cell.txtPromiseTyp.text = (promTyp["title"] as! String)
            if(strTitle == "Payment"){
                cell.btnBalTypDisable.isHidden = true
                cell.btnAmtDisable.isHidden = true
                cell.btnPaymntypDisable.isHidden = true
                cell.btnAccntDisable.isHidden = true
                
                cell.lblTitle.text = "P"
            }
            else if(strTitle == "Insurance"){
                cell.btnBalTypDisable.isHidden = false
                cell.btnAmtDisable.isHidden = false
                cell.btnPaymntypDisable.isHidden = false
                cell.btnAccntDisable.isHidden = false
                cell.lblTitle.text = "I"
            }
        }
        else{
            let promiseType = dict["promiseType"] as! Int
            if (promiseType == 1){
                cell.txtPromiseTyp.text = "Payment"
                cell.lblTitle.text = "P"
                
                cell.btnBalTypDisable.isHidden = true
                cell.btnAmtDisable.isHidden = true
                cell.btnPaymntypDisable.isHidden = true
                cell.btnAccntDisable.isHidden = true
                
                
            }else if (promiseType == 2){
                cell.txtPromiseTyp.text = "Insurance"
                cell.lblTitle.text = "I"
                
                cell.btnBalTypDisable.isHidden = false
                cell.btnAmtDisable.isHidden = false
                cell.btnPaymntypDisable.isHidden = false
                cell.btnAccntDisable.isHidden = false
                
            }
        }
        
        
        
        
        let balncTyp = dict["balanceType"] as! String
        for i in 0..<arrBalanceTyp.count{
            let dict = arrBalanceTyp[i] as! NSDictionary
            if let _id = dict["_id"] as? Int{
                if("\(_id)" == balncTyp){
                    let description = dict["description"] as! String
                    cell.txtBalanceType.text = description
                }
            }
            if let _id = dict["_id"] as? String{
                if(_id == balncTyp){
                    let description = dict["description"] as! String
                    cell.txtBalanceType.text = description
                }
            }
        }
        
        let postDate = dict["postDate"] as! String
        cell.txtDate.text = postDate
        
        if let amount = dict["amount"] as? String{
            cell.txtAmt.text = amount
        }
        else{
            if let amount = dict["amount"] as? Int{
                cell.txtAmt.text = "\(amount)"
            }
        }
        let account = dict["account"] as! String
        cell.txtAcctount.text = account
        
        let deliveryMethod = dict["deliveryMethod"] as! NSDictionary
        cell.txtDelivey.text = (deliveryMethod["title"] as! String)
        
        let paymentFrom = dict["paymentFrom"] as! NSDictionary
        cell.txtPay_form.text = (paymentFrom["title"] as! String)
        
        cell.btnCross.tag = indexPath.section
        cell.btnDelete.tag = indexPath.section
        cell.btnDelete.addTarget(self, action: #selector(clickONDeleteBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    
    func callDeleteCellApi(tag : Int){
        
        let dict = arrAddPromise[tag] as! NSDictionary
        Globalfunc.print(object:dict)
        
        let id = dict["_id"] as! Int
        
        //     http://18.191.178.120:3000/api/promise-to-pay?id=30
        
        BaseApi.onResponseDeleteWithToken(url: "\(Constant.promise_pay_url)?id=\(id)") { (dict, error) in
            
            if(error == ""){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation {
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        
                        self.viewAlert.isHidden = false
                        self.lblMsg.text = msg
                        
                    }
                }
            }
        }
        
    }
    
    @objc func clickONDeleteBtn(_ sender: UIButton){
        if(arrAddPromise.count > 0){
            self.callDeleteCellApi(tag: sender.tag)
        }
    }
    
    @IBAction func clickONOKBtn(_ sender: UIButton)
    {
        self.viewAlert.isHidden = true
        self.viewDidLoad()
    }
}

class tblPRomiseCell : UITableViewCell , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate
{
    var tblViewPromise : UITableView!
    
    @IBOutlet weak var txtPromiseTyp : UITextField!
    @IBOutlet weak var txtDate : UITextField!
    @IBOutlet weak var txtBalanceType : UITextField!
    @IBOutlet weak var txtAmt : UITextField!
    @IBOutlet weak var txtDelivey : UITextField!
    @IBOutlet weak var txtPay_form : UITextField!
    @IBOutlet weak var txtAcctount : UITextField!
    @IBOutlet weak var txtNotes : UITextField!
    @IBOutlet weak var lblNotesTitle : UILabel!
    @IBOutlet weak var viewNotes : UIView!
    
    @IBOutlet weak var btnCross : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblLine : UILabel!
    @IBOutlet weak var imgP : UIImageView!
    
    
    var pickerDate : UIDatePicker!
    var pickerViewPromiseTyp: UIPickerView!
    var pickerViewBalanceTyp: UIPickerView!
    var pickerViewDelivery: UIPickerView!
    var pickerViewPaymentForm: UIPickerView!
    var pickerViewPaymentAccount: UIPickerView!
    
    @IBOutlet weak var viewData : UIView!
    @IBOutlet weak var viewBlur : UIView!
    
    @IBOutlet weak var btnBalTypDisable : UIButton!
    @IBOutlet weak var btnAmtDisable : UIButton!
    @IBOutlet weak var btnPaymntypDisable : UIButton!
    @IBOutlet weak var btnAccntDisable : UIButton!
    
    
    var arrPromiseTyp : NSMutableArray = []
    
    var arrPaymnetForm : NSMutableArray = []
    
    
    var acrrAcount = [""]
    
    override func awakeFromNib() {
        
        self.callDeliveryMEthodApi(url: Constant.delivery_method_url)
        
        self.txtPromiseTyp.delegate = self
        self.txtDate.delegate = self
        self.txtBalanceType.delegate = self
        self.txtDelivey.delegate = self
        self.txtPay_form.delegate = self
        self.txtAcctount.delegate = self
        self.txtAmt.delegate = self
        self.txtNotes.delegate = self
        
        let dict1 =  ["title" :"Payment","id": "1"]
        let dict2 =  ["title" :"Insurance","id": "2"]
        
        arrPromiseTyp.add(dict1)
        arrPromiseTyp.add(dict2)
        
        arr_btn.append(btnBalTypDisable)
        arr_btn.append(btnAmtDisable)
        arr_btn.append(btnPaymntypDisable)
        arr_btn.append(btnAccntDisable)
        
    }
    
    @IBAction func clickONCrossBtn(_ sender: UIButton){
        
        if(arrAddPromise.count > 0){
            arrAddPromise.removeObject(at: sender.tag)
            tblViewPromise.reloadData()
        }
        else{
            tblViewPromise.isHidden = true
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewPromiseTyp{
            return self.arrPromiseTyp.count
        }
        else if(pickerView == pickerViewBalanceTyp){
            return arrBalanceTyp.count
        }
        else if(pickerView == pickerViewDelivery){
            return Constant.arrDElivryMethod.count
        }
        else if(pickerView == pickerViewPaymentForm){
            return self.arrPaymnetForm.count
        }
        else if(pickerView == pickerViewPaymentAccount){
            return self.acrrAcount.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerViewPromiseTyp){
            let dict = arrPromiseTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewBalanceTyp){
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewDelivery){
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewPaymentForm){
            if(arrPaymnetForm.count > 0){
                let dict = arrPaymnetForm[row] as! NSDictionary
                let strTitle = dict["title"] as! String
                return strTitle
            }
            //let strTitle = arrResult[row]
            return "Select Delivery Method"
        }
        else if(pickerView == pickerViewPaymentAccount){
            let strTitle = acrrAcount[row]
            return strTitle
        }
        
        return ""
    }
    
    func PassTextfielf(txtField : UITextField) -> IndexPath{
        
        let pointInTable = txtField.convert(txtField.bounds.origin, to: tblViewPromise)
        let textFieldIndexPath = self.tblViewPromise.indexPathForRow(at: pointInTable)
        return textFieldIndexPath!
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewPromiseTyp){
            
            let dict = arrPromiseTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            
            if(strTitle == "Payment"){
                btnBalTypDisable.isHidden = true
                btnAmtDisable.isHidden = true
                btnPaymntypDisable.isHidden = true
                btnAccntDisable.isHidden = true
            }
            else if(strTitle == "Insurance"){
                btnBalTypDisable.isHidden = false
                btnAmtDisable.isHidden = false
                btnPaymntypDisable.isHidden = false
                btnAccntDisable.isHidden = false
                
            }
            
            
            let _id = dict["id"] as! String
            
            self.txtPromiseTyp.text = strTitle
            
            let indexPath = PassTextfielf(txtField: self.txtPromiseTyp)
            Globalfunc.print(object:indexPath.section)
            
            let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
            let promisetypDict = dictMain["promiseType"] as! NSMutableDictionary
            promisetypDict.setValue("\(self.txtPromiseTyp.text!)", forKey: "title")
            promisetypDict.setValue("\(_id)", forKey: "id")
            
            dictMain.setValue(promisetypDict, forKey: "promiseType")
            arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
            
            tblViewPromise.reloadData()
            
        }
        else if(pickerView == pickerViewBalanceTyp){
            
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            
            
            self.txtBalanceType.text = strTitle
            
            let indexPath = PassTextfielf(txtField: self.txtPromiseTyp)
            Globalfunc.print(object:indexPath.section)
            let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
            
            if let _id = dict["_id"] as? Int{
                dictMain.setValue("\(_id)", forKey: "balanceType")
            }
            
            if let _id = dict["_id"] as? String{
                dictMain.setValue("\(_id)", forKey: "balanceType")
            }
            
            arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
            
            tblViewPromise.reloadData()
        }
        else if(pickerView == pickerViewDelivery){
            
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let _id = dict["_id"] as! Int
            
            self.txtDelivey.text = (dict["title"] as! String)
            
            let indexPath = PassTextfielf(txtField: self.txtDelivey)
            
            let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
            let deliveryDict = dictMain["deliveryMethod"] as! NSMutableDictionary
            
            deliveryDict.setValue("\(self.txtDelivey.text!)", forKey: "title")
            deliveryDict.setValue(_id, forKey: "id")
            
            dictMain.setValue(deliveryDict, forKey: "deliveryMethod")
            arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
            
            self.arrPaymnetForm = []
            self.txtPay_form.text =  ""
            if let arr = dict["paymentForm"] as? [NSDictionary]{
                if(arr.count > 0){
                    for i in 0..<arr.count{
                        let dictA = arr[i]
                        self.arrPaymnetForm.add(dictA)
                    }
                }
            }
            tblViewPromise.reloadData()
        }
        else if(pickerView == pickerViewPaymentForm){
            
            if(arrPaymnetForm.count > 0){
                let dict = arrPaymnetForm[row] as! NSDictionary
                let strTitle = dict["title"] as! String
                let _id = dict["_id"] as! Int
                
                self.txtPay_form.text = strTitle
                
                
                let indexPath = PassTextfielf(txtField: self.txtPay_form)
                
                let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
                let deliveryDict = dictMain["paymentFrom"] as! NSMutableDictionary
                
                deliveryDict.setValue("\(self.txtPay_form.text!)", forKey: "title")
                deliveryDict.setValue(_id, forKey: "id")
                
                dictMain.setValue(deliveryDict, forKey: "paymentFrom")
                arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
                tblViewPromise.reloadData()
                
                
            }
            
        }
            
        else if(pickerView == pickerViewPaymentAccount){
            self.txtAcctount.text = acrrAcount[row]
            
            let indexPath = PassTextfielf(txtField: self.txtAcctount)
            Globalfunc.print(object:indexPath.section)
            let dict = arrAddPromise[indexPath.section] as! NSMutableDictionary
            dict.setValue("\(self.txtAcctount.text!)", forKey: "account")
            arrAddPromise.replaceObject(at: indexPath.section, with: dict)
            
            tblViewPromise.reloadData()
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtPromiseTyp){
            self.pickUp(txtPromiseTyp)
        }
            
        else if(textField == self.txtDate){
            self.pickUpDateTime(txtDate)
        }
            
        else if(textField == self.txtBalanceType){
            self.pickUp(txtBalanceType)
        }
            
        else if(textField == self.txtDelivey){
            self.pickUp(txtDelivey)
        }
            
        else if(textField == self.txtPay_form){
            self.pickUp(txtPay_form)
        }
            
        else if(textField == self.txtAcctount){
            self.pickUp(txtAcctount)
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            
            if(textField == self.txtAmt){
                let indexPath = PassTextfielf(txtField: self.txtAmt)
                let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
                dictMain.setValue("\(updatedText)", forKey: "amount")
                arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
                // tblViewPromise.reloadData()
                
            }
                
            else if(textField == self.txtNotes){
                
                let indexPath = PassTextfielf(txtField: self.txtNotes)
                let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
                dictMain.setValue("\(updatedText)", forKey: "note")
                arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
                // tblViewPromise.reloadData()
            }
        }
        return true
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDate){
            self.pickerDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerDate.datePickerMode = .date
            self.pickerDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerDate
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtPromiseTyp){
            
            self.pickerViewPromiseTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerViewPromiseTyp.delegate = self
            self.pickerViewPromiseTyp.dataSource = self
            self.pickerViewPromiseTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPromiseTyp
        }
            
        else if(textField == self.txtBalanceType){
            
            self.pickerViewBalanceTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerViewBalanceTyp.delegate = self
            self.pickerViewBalanceTyp.dataSource = self
            self.pickerViewBalanceTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewBalanceTyp
        }
            
        else if(textField == self.txtDelivey){
            
            self.pickerViewDelivery = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerViewDelivery.delegate = self
            self.pickerViewDelivery.dataSource = self
            self.pickerViewDelivery.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewDelivery
        }
            
        else if(textField == self.txtPay_form){
            
            self.pickerViewPaymentForm = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerViewPaymentForm.delegate = self
            self.pickerViewPaymentForm.dataSource = self
            self.pickerViewPaymentForm.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPaymentForm
        }
            
        else if(textField == self.txtAcctount){
            
            self.pickerViewPaymentAccount = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.frame.size.width, height: 216))
            self.pickerViewPaymentAccount.delegate = self
            self.pickerViewPaymentAccount.dataSource = self
            self.pickerViewPaymentAccount.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPaymentAccount
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtDate.text = formatter.string(from: pickerDate.date)
        self.txtDate.resignFirstResponder()
        
        let indexPath = PassTextfielf(txtField: self.txtDate)
        Globalfunc.print(object:indexPath.section)
        
        let dictMain = arrAddPromise[indexPath.section] as! NSMutableDictionary
        dictMain.setValue("\(self.txtDate.text!)", forKey: "postDate")
        arrAddPromise.replaceObject(at: indexPath.section, with: dictMain)
        
        tblViewPromise.reloadData()
    }
    
    @objc func cancelClickDate() {
        txtDate.resignFirstResponder()
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtPromiseTyp.resignFirstResponder()
        txtBalanceType.resignFirstResponder()
        txtPay_form.resignFirstResponder()
        txtDelivey.resignFirstResponder()
        txtAcctount.resignFirstResponder()
        
    }
    
    
    func callDeliveryMEthodApi(url : String){
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                Globalfunc.print(object:dict)
                OperationQueue.main.addOperation {
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary]
                        {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    Constant.arrDElivryMethod.add(dictA)
                                }
                            }
                            let dictArr = Constant.arrDElivryMethod[0] as! NSDictionary
                            self.arrPaymnetForm = []
                            self.txtPay_form.text =  ""
                            if let arr = dictArr["paymentForm"] as? [NSDictionary]{
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrPaymnetForm.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
