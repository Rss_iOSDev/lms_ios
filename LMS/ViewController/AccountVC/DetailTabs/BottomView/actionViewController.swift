//
//  actionViewController.swift
//  LMS
//
//  Created by Apple on 23/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

var isRepo = ""
var isDisbale = ""

class actionViewController: UIViewController {
    

    @IBOutlet weak var btnOutForRepo : UIButton!
    @IBOutlet weak var btnCancelRepo : UIButton!
    @IBOutlet weak var btnUpdateRecordRepo : UIButton!
    @IBOutlet weak var btnRedeemRepo : UIButton!
    @IBOutlet weak var btnAddNote : UIButton!
    @IBOutlet weak var btnUpdateAlert : UIButton!
    @IBOutlet weak var btnAssgnTask : UIButton!
    @IBOutlet weak var btnBankrutcy : UIButton!
    @IBOutlet weak var btnUpdateIns : UIButton!
    @IBOutlet weak var btnUpdateRtc : UIButton!
    @IBOutlet weak var btncreditRepotng : UIButton!
    @IBOutlet weak var btnLock : UIButton!
    @IBOutlet weak var btnPayGps : UIButton!
    
    var lockStatus : Bool = false
    
    @IBOutlet weak var lblActtNo : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}

        self.setBtnStatus()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBtnStatus()
    {
         do {
             let decoded  = userDef.object(forKey: "action_dict") as! Data
             if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                 
                     Globalfunc.print(object:responseDict)
                     OperationQueue.main.addOperation {
                        
                        if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                            
                            let lockSts = loanAccount["lock_status"] as! Bool
                            if(lockSts == true){
                                self.lockStatus = false
                                self.btnLock.setTitle("UNLOCK", for: UIControl.State.normal)
                            }
                            else{
                                self.lockStatus = true
                                self.btnLock.setTitle("LOCK", for: UIControl.State.normal)
                            }
                            
                            let account_status = loanAccount["account_status"] as! Int
                            Globalfunc.print(object:account_status)
                            if(account_status == 7 || account_status == 6 || account_status == 10 || account_status == 3)
                            {
                                self.btnOutForRepo.isEnabled = false
                                self.btnOutForRepo.alpha = 0.5
                            }
                            
                            if(account_status == 7 || account_status == 1 || account_status == 10  || account_status == 3)
                            {
                                self.btnCancelRepo.isEnabled = false
                                self.btnCancelRepo.alpha = 0.5
                            }
                            
                            if(account_status == 10  || account_status == 3)
                            {
                                self.btnUpdateRecordRepo.isEnabled = false
                            }
                            if(account_status == 1 || account_status == 6 || account_status == 10  || account_status == 3)
                            {
                                self.btnRedeemRepo.isEnabled = false
                                self.btnRedeemRepo.alpha = 0.5
                            }
                            
                            if(account_status != 7)
                            {
                                isDisbale = "false"
                                self.btnUpdateRecordRepo.setTitle("RECORD REPO", for: UIControl.State.normal)
                            }
                             else if(account_status == 7)
                             {
                                isDisbale = "true"
                                 self.btnUpdateRecordRepo.setTitle("UPDATE REPO", for: UIControl.State.normal)
                             }
                        }
                     }
             }
         } catch {
             Globalfunc.print(object: "Couldn't read file.")
         }
    }
}

//open action tabs
extension actionViewController {
    
    @IBAction func clickONBtn(_ sender : UIButton)
    {
        if(sender.tag == 10)
        {
            self.presentViewControllerBasedOnIdentifier("outForRepoVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 20)
        {
            isRepo = "cancel"
            self.presentViewControllerBasedOnIdentifier("redemRepoVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 30)
        {
            self.presentViewControllerBasedOnIdentifier("recordUpdateRepoVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 40)
        {
            isRepo = "redeem"
            self.presentViewControllerBasedOnIdentifier("redemRepoVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 50)
        {
            self.presentViewControllerBasedOnIdentifier("addNoteVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 60)
        {
            self.presentViewControllerBasedOnIdentifier("updateAlertVC", strStoryName: "subActionStory")
        }
        else if(sender.tag == 70)
        {
            self.presentViewControllerBasedOnIdentifier("AssignTaskVC", strStoryName: "subActionStory")
        }
        
        else if(sender.tag == 80)
        {
             self.presentViewControllerBasedOnIdentifier("bankruptcyVC", strStoryName: "subActionStory")
        }
        
        else if(sender.tag == 90)
        {
             self.presentViewControllerBasedOnIdentifier("updateInsVC", strStoryName: "subActionStory")
        }
        
        else if(sender.tag == 100)
        {
            self.presentViewControllerBasedOnIdentifier("updateRtcVC", strStoryName: "subActionStory")
        }
        
        else if(sender.tag == 110)
        {
            self.presentViewControllerBasedOnIdentifier("creditReportngVC", strStoryName: "subActionStory")
        }
        
        else if(sender.tag == 120)
        {
             Globalfunc.showLoaderView(view: self.view)
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                                  
                    let accId = responseDict["_id"] as! Int
                   
                    let param = ["id": accId, //act_id dynamic
                                "lock": self.lockStatus] as [String : Any]
                    Globalfunc.print(object:param)
                    self.callLockApi(params: param)
                                  
                    }
                 }
            catch{
                     Globalfunc.print(object: "Couldn't read file.")
            }
            
           
        }
        
        else if(sender.tag == 130)
        {
            
        }
        else if(sender.tag == 140)
        {
            self.presentViewControllerBasedOnIdentifier("gapInsVC", strStoryName: "subActionStory")
        }
    }
    
    
    func callLockApi(params : [String : Any]){
        
        BaseApi.onResponsePutWithToken(url: Constant.action_acctLock, controller: self, parms: params as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
