//
//  AssignTaskVC.swift
//  LMS
//
//  Created by Apple on 06/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class AssignTaskVC: UIViewController {
    
    @IBOutlet weak var txtUSer : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    var pv_USer: UIPickerView!
    var arrUSer : NSMutableArray = []
    
    var user_id : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}
        
        self.callApiToSetData()
        
    }
}

//Api post
extension AssignTaskVC {
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                Globalfunc.print(object:dataD)
                let acct_id = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let insID = dataD["insid"] as! Int
                
                let param = ["account_id": acct_id, //act_id dynamic
                    "insid": insID,
                    "user_id": self.user_id!,
                    "note": "\(txtNotes.text!)",
                    "userid": userid] as [String : Any]
                
                self.sendFormDatatoPost(params: param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.action_assgn_task, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//Api get
extension AssignTaskVC {
    
    func callApiToSetData(){
        
        BaseApi.callApiRequestForGet(url: Constant.assign_getUSer) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let response = dict as? NSDictionary {
                        if let responseArr = response["data"] as? [NSDictionary] {
                            if(responseArr.count > 0){
                                for i in 0..<responseArr.count{
                                    let dictA = responseArr[i]
                                    self.arrUSer.add(dictA)
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension AssignTaskVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pv_USer{
            return self.arrUSer.count
        }
            
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pv_USer{
            //  let strTitle = arrUSer[row]
            let dict = self.arrUSer[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            
            return "\(fname) \(lname)"
            
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if pickerView == pv_USer{
            
            let dict = self.arrUSer[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            
            self.txtUSer.text = "\(fname) \(lname)"
            
            self.user_id = (dict["_id"] as! Int)
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtUSer){
            self.pickUp(txtUSer)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtUSer){
            self.pv_USer = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_USer.delegate = self
            self.pv_USer.dataSource = self
            self.pv_USer.backgroundColor = UIColor.white
            textField.inputView = self.pv_USer
            
            
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtUSer.resignFirstResponder()
    }
    
    
    @objc func cancelClick() {
        
        txtUSer.resignFirstResponder()
    }
    
}
