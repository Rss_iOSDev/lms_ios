//
//  outForRepoVC.swift
//  LMS
//
//  Created by Apple on 24/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class outForRepoVC: UIViewController {
    
    @IBOutlet weak var viewMAin: UIView!
    
    @IBOutlet weak var txtAgent : UITextField!
    @IBOutlet weak var txtReason : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    
    
    var arrAgentList : NSMutableArray = []
    var pickerViewAgentList: UIPickerView!
    
    
    var arrReason = ["Past Due Payment","Insurance","Other/See Notes"]
    var pickerViewReasonList: UIPickerView!
    
    var isPickerSelected = ""
    
    
    
    @IBOutlet weak var lblActtNo : UILabel!
    //agent list data outlets
    @IBOutlet weak var lblChannelName : UILabel!
    @IBOutlet weak var lblContact : UILabel!
    @IBOutlet weak var lblPhone_1 : UILabel!
    @IBOutlet weak var lblPhone_2 : UILabel!
    @IBOutlet weak var lblAddress_1 : UILabel!
    @IBOutlet weak var lblAddress_2 : UILabel!
    @IBOutlet weak var lblcity : UILabel!
    @IBOutlet weak var lblState : UILabel!
    @IBOutlet weak var lblZipcode : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    
    @IBOutlet weak var lblStack : UIStackView!
    @IBOutlet weak var imgChannel : UIImageView!
    
    @IBOutlet weak var viewChannelHgt : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}

        
        Globalfunc.showLoaderView(view: self.view)
        self.callGetApitoAgentList()
        
        self.imgChannel.isHidden = true
        self.lblStack.isHidden = true
        self.viewChannelHgt.constant = 0
        
    }
    
    @IBAction func clickOnCloseBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            self.dismiss(animated: true, completion: nil)
            
        }else if(sender.tag == 20){
            self.sendFormDatatoPost()
        }
    }
    
    
}
//MARK: - Testfield delegate methods

extension outForRepoVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewAgentList){
            return self.arrAgentList.count
        }
        else if(pickerView == pickerViewReasonList){
            return self.arrReason.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerViewAgentList){
            
            let dict = arrAgentList[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            //  let _id = dict["_id"] as! Int
            return strTitle
        }
            
        else if(pickerView == pickerViewReasonList){
            return self.arrReason[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewAgentList){
            let dict = arrAgentList[row] as! NSDictionary
            self.txtAgent.text = dict["title"] as? String
            self.txtAgent.tag = row
            
        }
            
        else if(pickerView == pickerViewReasonList){
            self.txtReason.text = self.arrReason[row]
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtAgent){
            self.pickUp(txtAgent)
        }
        else if(textField == self.txtReason){
            self.pickUp(txtReason)
        }
        
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAgent){
            
            isPickerSelected = "agent"
            self.pickerViewAgentList = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAgentList.delegate = self
            self.pickerViewAgentList.dataSource = self
            self.pickerViewAgentList.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAgentList
        }
            
        else if(textField == self.txtReason){
            
            isPickerSelected = "reason"
            
            self.pickerViewReasonList = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewReasonList.delegate = self
            self.pickerViewReasonList.dataSource = self
            self.pickerViewReasonList.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewReasonList
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtAgent.resignFirstResponder()
        txtReason.resignFirstResponder()
        
        
        if(isPickerSelected == "agent"){
            self.imgChannel.isHidden = false
            self.lblStack.isHidden = false
            self.viewChannelHgt.constant = 262
            
            let dict = arrAgentList[self.txtAgent.tag] as! NSDictionary
            Globalfunc.print(object:dict)
            
            let channlNAme = dict["legalname"] as! String
            self.lblChannelName.text = "Channel Name: \(channlNAme)"
            
            let contact_person = dict["contact_person"] as! String
            self.lblContact.text = "Contact: \(contact_person)"
            
            let phone2 = dict["phone2"] as! String
            self.lblPhone_2.text = "Phone 2: \(phone2)"
            
            let phone1 = dict["phone1"] as! String
            self.lblPhone_1.text = "Phone 1: \(phone1)"
            
            let address1 = dict["address1"] as! String
            self.lblAddress_1.text = "Address 1: \(address1)"
            
            let address2 = dict["address2"] as! String
            self.lblAddress_2.text = "Address 2: \(address2)"
            
            let city = dict["city"] as! String
            self.lblcity.text = "City: \(city)"
            
            let state = dict["state"] as! String
            self.lblState.text = "State: \(state)"
            
            let zip = dict["zip"] as! String
            self.lblZipcode.text = "Zip: \(zip)"
            
            let email = dict["email"] as! String
            self.lblEmail.text = "Email: \(email)"
            
        }
        
    }
    
    @objc func cancelClick() {
        txtAgent.text = ""
        txtAgent.resignFirstResponder()
        
        txtReason.text = ""
        txtReason.resignFirstResponder()
        
        if(isPickerSelected == "agent"){
            self.imgChannel.isHidden = true
            self.lblStack.isHidden = true
            self.viewChannelHgt.constant = 0
        }
    }
}
//


//MARK: - Api methods

extension outForRepoVC
{
    func callGetApitoAgentList(){
        
        BaseApi.callApiRequestForGet(url: Constant.get_agentList) { (dict, error) in
            
            if(error == ""){
                
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        
                        if let arr = response["data"] as? [NSDictionary]
                        {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrAgentList.add(dictA)
                                }
                            }
                        }
                    }
                    
                    if let arr = dict as? NSDictionary {
                        if let msg = arr["message"] as? String{
                            if(msg == "Your token has expired."){
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func sendFormDatatoPost(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let dict = arrAgentList[self.txtAgent.tag] as! NSDictionary
                let agntId = dict["_id"] as! Int
                
                let param = ["account_id": acct_id, //act_id dynamic
                    "insid": insID,
                    "repoAgent": agntId,
                    "repoNotes": "\(txtNotes.text!)",
                    "repoReason": "\(txtReason.text!)",
                    "userid": userid] as [String : Any]
                
                Globalfunc.print(object:param)
                
                BaseApi.onResponsePostWithToken(url: Constant.post_OutforRepo, controller: self, parms: param) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            
                            if let arr = dict as? NSDictionary {
                                let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    
                                    self.dismiss(animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}
