//
//  updateAlertVC.swift
//  LMS
//
//  Created by Apple on 25/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import Colorful

class updateAlertVC: UIViewController {
    
    @IBOutlet weak var colorView : UIView!
    @IBOutlet weak var colorFront : ColorPicker!
    
    
    @IBOutlet weak var txtMsg : UITextField!
    
    @IBOutlet weak var lblFrontColor : UILabel!
    @IBOutlet weak var lblBgColor : UILabel!
    
    @IBOutlet weak var btnDefault : UIButton!
    @IBOutlet weak var lblFrntcolo : UILabel!
    @IBOutlet weak var view1 : UIView!
    
    @IBOutlet weak var lblBgcolo : UILabel!
    @IBOutlet weak var view2 : UIView!
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    var isSelectBtn = ""
    
    var pickedColor : UIColor?
    var colorSpace: HRColorSpace = .sRGB
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        colorView.isHidden = true
        btnDefault.isSelected = true
        self.lblFrntcolo.isHidden = true
        self.lblBgcolo.isHidden = true
        self.view1.isHidden = true
        self.view2.isHidden = true
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}
        
        colorFront.addTarget(self, action: #selector(self.handleColorChanged(picker:)), for: .valueChanged)
        colorFront.set(color: UIColor(displayP3Red: 1.0, green: 1.0, blue: 0, alpha: 1), colorSpace: colorSpace)
        handleColorChanged(picker: colorFront)
    }
    
    @objc func handleColorChanged(picker: ColorPicker) {
        if(isSelectBtn == "font"){
            self.lblFrontColor.backgroundColor = picker.color
            self.lblFrontColor.textColor = .white
            self.lblFrontColor.text = picker.color.hexStringFromColor()
        }
        else if(isSelectBtn == "bg"){
            self.lblBgColor.backgroundColor = picker.color
            self.lblBgColor.textColor = .white
            self.lblBgColor.text = picker.color.hexStringFromColor()
        }
    }
    
    @IBAction func openColorView(_ sender: UIButton)
    {
        if(sender.tag == 10){
            isSelectBtn = "font"
            colorView.isHidden = false
        }
        else if(sender.tag == 30){
            isSelectBtn = "bg"
            colorView.isHidden = false
        }
        else if(sender.tag == 20){
            isSelectBtn = ""
            colorView.isHidden = true
        }
    }
    
    @IBAction func SelectDefaultBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            self.lblFrntcolo.isHidden = false
            self.lblBgcolo.isHidden = false
            self.view1.isHidden = false
            self.view2.isHidden = false
        }else {
            sender.isSelected = true
            self.lblFrntcolo.isHidden = true
            self.lblBgcolo.isHidden = true
            self.view1.isHidden = true
            self.view2.isHidden = true
        }
    }
}

//Api calling
extension updateAlertVC
{
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_id = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let param = ["account_id": acct_id, //act_id dynamic
                    "default_style": btnDefault.isSelected,
                    "font_color": "\(self.lblFrontColor.text!)",
                    "back_color": "\(self.lblBgColor.text!)",
                    "message": "\(txtMsg.text!)",
                    "userid": userid] as [String : Any]
                Globalfunc.print(object: param)
                self.sendFormDatatoPost(params: param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func sendFormDatatoPost(params : [String : Any]){
        BaseApi.onResponsePostWithToken(url: Constant.action_update_alert, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.txtMsg.text = ""
                            self.lblFrontColor.text = ""
                            self.lblBgColor.text = ""
                            self.lblFrontColor.backgroundColor = .clear
                            self.lblBgColor.backgroundColor = .clear
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    func hexStringFromColor() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}
