//
//  addNoteVC.swift
//  LMS
//
//  Created by Apple on 24/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class addNoteVC: UIViewController {
    
    @IBOutlet weak var txtNotes : UITextView!
    @IBOutlet weak var lblActtNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}

    }
    
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let insID = dataD["insid"] as! Int
                let userid = dataD["userid"] as! Int
                
                let strFname = user?.fname
                let strLname = user?.lname
                
                let name = String((strFname?.first)!) + String((strLname?.first)!)
                let initials = name + String(userid)
                
                let param = ["account_id": acct_id, //act_id dynamic
                    "insid": insID,
                    "notes_type": "bottom_navigation",
                    "sub_type": "add_notes",
                    "action": "Notes",
                    "user": "Contact Note",
                    "result": "",
                    "promiseType": "",
                    "initials": initials,
                    "description": "\(txtNotes.text!)",
                    "userid": userid] as [String : Any]
                
                
                Globalfunc.print(object:param)
                
                self.sendFormDatatoPost(params: param)
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension addNoteVC {
    
    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.action_notes, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
