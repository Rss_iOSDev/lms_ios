//
//  creditReportngVC.swift
//  LMS
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class creditReportngVC: UIViewController {
    
    @IBOutlet weak var txtCredit : UITextField!
    var isCredit : Bool = true
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    
    var pv_Credit: UIPickerView!
    var arrCredti = ["Yes","No"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let string = self.arrCredti[0]
        self.txtCredit.text = string
        
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}



        // Do any additional setup after loading the view.
    }
}




extension creditReportngVC {
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let param = ["account_id": acct_id, //act_id dynamic
                   "report_credit": isCredit] as [String : Any]
                   
                self.sendFormDatatoPost(params: param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
      }
      
      @IBAction func clickOnCancelBtn(_ sender: UIButton)
      {
          self.dismiss(animated: true, completion: nil)
      }
      

    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.action_update_cr, controller: self, parms: params) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    self.dismiss(animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
    }

}
//picker

extension creditReportngVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pv_Credit){
            return self.arrCredti.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pv_Credit){
            let string = self.arrCredti[row]
            return string
        }
        
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pv_Credit){
            let string = self.arrCredti[row]
            self.txtCredit.text = string
            
            if(string == "Yes"){
                self.isCredit = true
            }
            else
            {
                self.isCredit = false
            }
        }
            
        
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtCredit){
            self.pickUp(txtCredit)
        }
        
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtCredit){
            self.pv_Credit = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Credit.delegate = self
            self.pv_Credit.dataSource = self
            self.pv_Credit.backgroundColor = UIColor.white
            textField.inputView = self.pv_Credit
        }
            
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtCredit.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtCredit.resignFirstResponder()
    }
}
