//
//  updateRtcVC.swift
//  LMS
//
//  Created by Apple on 09/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class updateRtcVC: UIViewController {
    
    @IBOutlet weak var txtCureDate : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    @IBOutlet weak var lblActtNo : UILabel!
    
    var pvEffDate : UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{
            
        }


    }
    
    
}

extension updateRtcVC {
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let insID = dataD["insid"] as! Int
                
                let param = ["account_id": acct_id, //act_id dynamic
                    "insid": insID,
                    "cure_date": "\(txtCureDate.text!)",
                    "note": "\(txtNotes.text!)",
                    "userid": userid] as [String : Any]
                
                self.sendFormDatatoPost(params: param)
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.action_update_rtc, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}



//picker

extension updateRtcVC : UITextFieldDelegate
{
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtCureDate){
            self.pickUpDateTime(self.txtCureDate)
        }
        
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtCureDate){
            
            self.pvEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvEffDate.datePickerMode = .date
            self.pvEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pvEffDate
        }
        
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtCureDate.text = formatter.string(from: pvEffDate.date)
        self.txtCureDate.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txtCureDate.resignFirstResponder()
    }
    
    //MARK:- Button
}
