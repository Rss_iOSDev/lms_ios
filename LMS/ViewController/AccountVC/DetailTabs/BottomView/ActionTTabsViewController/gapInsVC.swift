//
//  gapInsVC.swift
//  LMS
//
//  Created by Apple on 05/01/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class gapInsVC: UIViewController {
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    @IBOutlet weak var txtAction : UITextField!
    @IBOutlet weak var txtInsCmpny : UITextField!
    @IBOutlet weak var txtPloicyNo : UITextField!
    @IBOutlet weak var txtInsAgent : UITextField!
    @IBOutlet weak var txtEfftDate : UITextField!
    @IBOutlet weak var txtExpireDate : UITextField!
    @IBOutlet weak var txtColl_Deduct : UITextField!
    @IBOutlet weak var txtComp_Deduct : UITextField!
    
    @IBOutlet weak var viewCancelDate : UIView!
    @IBOutlet weak var viewDisableField : UIView!
    @IBOutlet weak var txtCancelEfftDate : UITextField!
    @IBOutlet weak var hgtCancelView : NSLayoutConstraint!
    
    var arrUrl = [Constant.ins_Agt,Constant.ins_test]
    
    var pv_Action: UIPickerView!
    var arrAction = ["Update","Cancel","Renew Policy","New Policy"]
    
    var pv_InsAgnt: UIPickerView!
    var arrAgnt : NSMutableArray = []
    
    var pv_Test: UIPickerView!
    var arrTest : NSMutableArray = []
    
    var pvEffDate : UIDatePicker!
    var pvExpireDate : UIDatePicker!
    var pvCancelEfftDate : UIDatePicker!
    
    var strChooseDate = ""
    
    var isType = ""
    
    var getId = ""
    var cmpnyId : Int!
    var AgntId : Int!
    
    var isCancel = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewDisableField.isHidden = true
        self.viewCancelDate.isHidden = true
        self.hgtCancelView.constant = 0
        
        Globalfunc.showLoaderView(view: self.view)
        self.call_getIns_HistoryApi()
        self.callAllPickerapi()
        
        // Do any additional setup after loading the view.
    }
}

////Api post
extension gapInsVC {
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        if(self.isCancel == true){
            if(txtCancelEfftDate.text == "" || txtCancelEfftDate.text?.count == 0 || txtCancelEfftDate.text == nil){
                txtCancelEfftDate.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                self.callInsuranceApi()
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else{
            if(txtAction.text == "" || txtAction.text?.count == 0 || txtAction.text == nil){
                txtAction.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtInsCmpny.text == "" || txtInsCmpny.text?.count == 0 || txtInsCmpny.text == nil){
                txtInsCmpny.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtPloicyNo.text == "" || txtPloicyNo.text?.count == 0 || txtPloicyNo.text == nil){
                txtPloicyNo.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtInsAgent.text == "" || txtInsAgent.text?.count == 0 || txtInsAgent.text == nil){
                txtInsAgent.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtEfftDate.text == "" || txtEfftDate.text?.count == 0 || txtEfftDate.text == nil){
                txtEfftDate.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtExpireDate.text == "" || txtExpireDate.text?.count == 0 || txtExpireDate.text == nil){
                txtExpireDate.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtColl_Deduct.text == "" || txtColl_Deduct.text?.count == 0 || txtColl_Deduct.text == nil){
                txtColl_Deduct.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtComp_Deduct.text == "" || txtComp_Deduct.text?.count == 0 || txtComp_Deduct.text == nil){
                txtComp_Deduct.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                self.callInsuranceApi()
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
            
        }
        
    }
    
    func callInsuranceApi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let insID = dataD["insid"] as! Int
                
                
                
                if(self.isCancel == true){
                    
                    let coll = String(txtColl_Deduct.text!.split(separator: "$").first!)
                    let comp = String(txtComp_Deduct.text!.split(separator: "$").first!)
                    
                    let param = ["account_id": acct_id, //act_id dynamic
                        "action": "\(txtAction.text!)",
                        "insurance_company" : cmpnyId!,
                        "policy_number" : "\(txtPloicyNo.text!)",
                        "cancel_effective_date": "\(txtCancelEfftDate.text!)",
                        "insurance_agent" : AgntId!,
                        "effective_date" : "\(txtEfftDate.text!)",
                        "expire_date" : "\(txtExpireDate.text!)",
                        "collision_deduct" : "\(coll)",
                        "comprehensive_deduct" : "\(comp)",
                        "insid":insID,
                        "userid":userid,
                        "id": getId] as [String : Any]
                    Globalfunc.print(object:param)
                    self.sendFormDatatoPost(strCheck: self.isType, params: param)
                }
                else if(self.isCancel == false){
                    
                    let coll = String(txtColl_Deduct.text!.split(separator: "$").first!)
                    let comp = String(txtComp_Deduct.text!.split(separator: "$").first!)
                    
                    let param = ["account_id": acct_id, //act_id dynamic
                        "action": "\(txtAction.text!)",
                        "insurance_company" : cmpnyId!,
                        "policy_number" : "\(txtPloicyNo.text!)",
                        "insurance_agent" : AgntId!,
                        "effective_date" : "\(txtEfftDate.text!)",
                        "expire_date" : "\(txtExpireDate.text!)",
                        "collision_deduct" : "\(coll)",
                        "comprehensive_deduct" : "\(comp)",
                        "insid":insID,
                        "userid":userid,
                        "id": getId] as [String : Any]
                    Globalfunc.print(object:param)
                    self.sendFormDatatoPost(strCheck: self.isType, params: param)
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func sendFormDatatoPost(strCheck : String, params : [String : Any]){
        
        
        if(strCheck == "put"){
            
            BaseApi.onResponsePutWithToken(url: Constant.gap_insurance_url, controller: self, parms: params as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
            
        }
        else if(strCheck == "post"){
            BaseApi.onResponsePostWithToken(url: Constant.gap_insurance_url, controller: self, parms: params) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
    
}


//Call Api for common
extension gapInsVC
{
    
    func call_getIns_HistoryApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"

                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.gap_insurance_url)/accid?account_id=\(accId_selected)"
                
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.print(object:dict)
                            
                            if let response = dict as? NSDictionary{
                                
                                if let resparr = response["data"] as? NSMutableArray{
                                    let resp = resparr.lastObject as! NSDictionary
                                    print(resp)
                                    self.isType = "put"
                                    let _id = resp["_id"] as! Int
                                    self.getId = "\(_id)"
                                    
                                    let string = self.arrAction[0]
                                    self.txtAction.text = string
                                    
                                    let agentTitle = resp["agentTitle"] as! String
                                    self.txtInsAgent.text = agentTitle
                                    
                                    let companyTitle = resp["companyTitle"] as! String
                                    self.txtInsCmpny.text = companyTitle
                                    
                                    let policy_number = resp["policy_number"] as! String
                                    self.txtPloicyNo.text = policy_number
                                    
                                    let collision_deduct = resp["collision_deduct"] as! Int
                                    self.txtColl_Deduct.text = "$ \(collision_deduct)"
                                    
                                    let comprehensive_deduct = resp["comprehensive_deduct"] as! Int
                                    self.txtComp_Deduct.text = "$ \(comprehensive_deduct)"
                                    
                                    if let effective_date = resp["effective_date"] as? String
                                    {
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "MM/dd/yyyy"
                                        let date = Date.dateFromISOString(string: effective_date)
                                        self.txtEfftDate.text = formatter.string(from: date!)
                                    }
                                    
                                    if let expire_date = resp["expire_date"] as? String
                                    {
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "MM/dd/yyyy"
                                        let date = Date.dateFromISOString(string: expire_date)
                                        self.txtExpireDate.text = formatter.string(from: date!)
                                    }
                                    
                                    
                                    self.cmpnyId = (resp["insurance_company"] as! Int)
                                    self.AgntId = (resp["insurance_agent"] as! Int)
                                    
                                }
                            }
                                
                            else{
                                self.isType = "post"
                                
                                let string = self.arrAction[2]
                                self.txtAction.text = string
                                
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
                
                
                
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
    }
    
    func callAllPickerapi()
    {
        let group = DispatchGroup() // initialize
        arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        
                        if(obj == Constant.ins_Agt){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrAgnt.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(obj == Constant.ins_test){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrTest.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}

//picker

extension gapInsVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pv_Action){
            return self.arrAction.count
        }
            
        else  if(pickerView == pv_InsAgnt){
            return self.arrAgnt.count
        }
            
        else  if(pickerView == pv_Test){
            return self.arrTest.count
        }
        
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pv_Action){
            let string = self.arrAction[row]
            return string
        }
            
        else  if(pickerView == pv_InsAgnt){
            let dict = self.arrAgnt[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
            
        }
            
        else  if(pickerView == pv_Test){
            let dict = self.arrTest[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pv_Action){
            let string = self.arrAction[row]
            self.txtAction.text = string
            if(string == "Cancel"){
                self.isCancel = true
                self.viewCancelDate.isHidden = false
                self.viewDisableField.isHidden = false
                self.hgtCancelView.constant = 85
            }
            else{
                self.isCancel = false
                self.viewCancelDate.isHidden = true
                self.viewDisableField.isHidden = true
                self.hgtCancelView.constant = 0
            }
        }
            
        else  if(pickerView == pv_InsAgnt){
            let dict = self.arrAgnt[row] as! NSDictionary
            let title = dict["title"] as! String
            self.txtInsAgent.text = title
            AgntId = (dict["_id"] as! Int)
        }
            
        else  if(pickerView == pv_Test){
            let dict = self.arrTest[row] as! NSDictionary
            let title = dict["title"] as! String
            self.txtInsCmpny.text = title
            cmpnyId = (dict["_id"] as! Int)
        }
        
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtAction){
            self.pickUp(txtAction)
        }
        else if(textField == self.txtInsAgent){
            self.pickUp(txtInsAgent)
        }
        else if(textField == self.txtInsCmpny){
            self.pickUp(txtInsCmpny)
        }
        else if(textField == self.txtEfftDate){
            self.pickUpDateTime(txtEfftDate)
        }
            
        else if(textField == self.txtExpireDate){
            self.pickUpDateTime(txtExpireDate)
        }
            
        else if(textField == self.txtCancelEfftDate){
            self.pickUpDateTime(txtCancelEfftDate)
        }
        
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtEfftDate){
            self.pvEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvEffDate.datePickerMode = .date
            self.pvEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pvEffDate
            strChooseDate = "effect"
        }
            
        else if(textField == self.txtExpireDate){
            self.pvExpireDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvExpireDate.datePickerMode = .date
            self.pvExpireDate.backgroundColor = UIColor.white
            textField.inputView = self.pvExpireDate
            strChooseDate = "expire"
        }
            
        else if(textField == self.txtCancelEfftDate){
            self.pvCancelEfftDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvCancelEfftDate.datePickerMode = .date
            self.pvCancelEfftDate.backgroundColor = UIColor.white
            textField.inputView = self.pvCancelEfftDate
            strChooseDate = "cancel"
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtAction){
            self.pv_Action = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Action.delegate = self
            self.pv_Action.dataSource = self
            self.pv_Action.backgroundColor = UIColor.white
            textField.inputView = self.pv_Action
        }
            
        else if(textField == self.txtInsCmpny){
            self.pv_Test = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Test.delegate = self
            self.pv_Test.dataSource = self
            self.pv_Test.backgroundColor = UIColor.white
            textField.inputView = self.pv_Test
        }
            
        else if(textField == self.txtInsAgent){
            self.pv_InsAgnt = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_InsAgnt.delegate = self
            self.pv_InsAgnt.dataSource = self
            self.pv_InsAgnt.backgroundColor = UIColor.white
            textField.inputView = self.pv_InsAgnt
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        if(strChooseDate == "effect"){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtEfftDate.text = formatter.string(from: pvEffDate.date)
            self.txtEfftDate.resignFirstResponder()
        }else if(strChooseDate == "expire"){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtExpireDate.text = formatter.string(from: pvExpireDate.date)
            self.txtExpireDate.resignFirstResponder()
        }
        else if(strChooseDate == "cancel"){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtCancelEfftDate.text = formatter.string(from: pvCancelEfftDate.date)
            self.txtCancelEfftDate.resignFirstResponder()
        }
        
    }
    
    @objc func cancelClickDate() {
        txtEfftDate.resignFirstResponder()
        txtExpireDate.resignFirstResponder()
        txtCancelEfftDate.resignFirstResponder()
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtAction.resignFirstResponder()
        txtInsCmpny.resignFirstResponder()
        txtInsAgent.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtAction.resignFirstResponder()
        txtInsCmpny.resignFirstResponder()
        txtInsAgent.resignFirstResponder()
        
    }
}
