//
//  redemRepoVC.swift
//  LMS
//
//  Created by Apple on 04/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.

import UIKit


class redemRepoVC: UIViewController {
    
    @IBOutlet weak var txtNotes: UITextView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblActtNo : UILabel!

     override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
            }
        }
        catch{}

        
        if(isRepo ==  "cancel"){
            self.lblTitle.text = "CANCEL REPO"
        }
        else if(isRepo ==  "redeem"){
            self.lblTitle.text = "REDEEM REPO"
        }
    }
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                if(isRepo ==  "cancel"){
                     let acct_id = dataD["_id"] as! Int
                      let param = ["account_id": acct_id, //act_id dynamic
                        "repoCancelNotes": "\(txtNotes.text!)"] as [String : Any]
                        self.sendFormDatatoPost(url: Constant.action_calcelRepo,params: param, strCheck: "cancel")
                }
                else if(isRepo ==  "redeem"){
                        let acct_id = dataD["_id"] as! Int
                        let insID = dataD["insid"] as! Int
                        let userid = dataD["userid"] as! Int
                        let param = ["account_id": acct_id, //act_id dynamic
                        "insid": insID,
                        "repoNotes": "\(txtNotes.text!)",
                        "userid": userid] as [String : Any]
                        self.sendFormDatatoPost(url: Constant.action_redeemRepo, params: param, strCheck: "redeem")
                    
                }
            }
        }
        catch{
                 Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        isRepo = ""
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension redemRepoVC {
    
    
    func sendFormDatatoPost(url : String, params : [String : Any],strCheck : String){
        
        if(strCheck == "redeem"){
            BaseApi.onResponsePostWithToken(url: url, controller: self, parms: params) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    self.dismiss(animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        else if(strCheck == "cancel"){
            
            BaseApi.onResponsePutWithToken(url: url, controller: self, parms: params as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    
                                    self.dismiss(animated: true, completion: nil)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                        }
                        
                        //response set
                       
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
}

