//
//  recordUpdateRepoVC.swift
//  LMS
//
//  Created by Apple on 10/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class recordUpdateRepoVC: UIViewController {
    
    
    @IBOutlet weak var txt_repo_EfftDate : UITextField!
    @IBOutlet weak var txt_repo_obtain : UITextField!
    @IBOutlet weak var txt_repo_charge : UITextField!
    @IBOutlet weak var txt_repo_vehLoc : UITextField!
    @IBOutlet weak var txt_repo_recoverBy : UITextField!
    @IBOutlet weak var txt_repo_recoverAt : UITextField!
    @IBOutlet weak var txt_repo_wit1 : UITextField!
    @IBOutlet weak var txt_repo_wit2 : UITextField!
    
    @IBOutlet weak var txtAddress1 : UITextField!
    @IBOutlet weak var txtAddress2 : UITextField!
    @IBOutlet weak var txtCity : UITextField!
    @IBOutlet weak var txtstate : UITextField!
    @IBOutlet weak var txtZipCode : UITextField!
    @IBOutlet weak var txtv_Notes : UITextView!
    @IBOutlet weak var txtPrsnlProp : UITextField!
    @IBOutlet weak var txtNotify : UITextField!
    @IBOutlet weak var txt_on : UITextField!
    @IBOutlet weak var txtClaimDays : UITextField!
    @IBOutlet weak var txtClaimDeadline : UITextField!
    @IBOutlet weak var txtDescription : UITextField!
    
    @IBOutlet weak var viewDisable : UIView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var balTyp = 0
    
    
    var arrUrl = [Constant.recover_rec,Constant.assign_getUSer,Constant.state]
    
    var pv_recoverBy: UIPickerView!
    var arr_recoverBy : NSMutableArray = []
    
    var pv_VehLoc: UIPickerView!
    var arrVehLoc : NSMutableArray = []
    
    var pv_witness1: UIPickerView!
    var pv_witness2: UIPickerView!
    var arrUser : NSMutableArray = []
    
    var pv_State: UIPickerView!
    var arrState : NSMutableArray = []
    
    var pv_repoObtaind: UIPickerView!
    var arrRepoObtained = ["Voluntary Repo","Involuntary Repo"]
    
    var pv_recoverAt: UIPickerView!
    var arrRecoverAt = ["Customer's Current Address","Customer's Employment Address","Other"]
    
    var pv_property: UIPickerView!
    var arrProp = [true,false]
    
    var pv_notified: UIPickerView!
    
    var pvEffDate : UIDatePicker!
    
    var veh_id = ""
    var rec_by_id = ""
    var wit1_d : Int!
    var wit2_d : Int!
    var state_id = ""
    var isProp : Bool = false
    var isNotify : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.call_getDetailsApi()
        self.callAllPickerapi()
        self.callVehicleLocationApi()
        
        self.txtPrsnlProp.text = "No"
        self.txtNotify.text = "No"
        
        if(isDisbale == "false"){
            self.viewDisable.isHidden = true
        }else if(isDisbale == "true"){
            self.viewDisable.isHidden = false
        }
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account no: \(acct_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
}

extension recordUpdateRepoVC {
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        if(isDisbale == "true"){
            self.dismiss(animated: true, completion: nil)
            
        }else if(isDisbale == "false"){
            
            
            if(txt_repo_EfftDate.text == "" || txt_repo_EfftDate.text?.count == 0 || txt_repo_EfftDate.text == nil){
                txt_repo_EfftDate.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txt_repo_charge.text == "" || txt_repo_charge.text?.count == 0 || txt_repo_charge.text == nil){
                txt_repo_charge.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            
            if(txtClaimDays.text == "" || txtClaimDays.text?.count == 0 || txtClaimDays.text == nil){
                txtClaimDays.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                Globalfunc.showLoaderView(view: self.view)
                do {
                    let decoded  = userDef.object(forKey: "dataDict") as! Data
                    if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                        
                        let acct_id = dataD["_id"] as! Int
                        let userid = dataD["userid"] as! Int
                        let insID = dataD["insid"] as! Int
                        
                        let param = ["account_id": acct_id, //act_id dynamic
                            "insid": insID,
                            "userid": userid,
                            "id": "",
                            "balanceType": self.balTyp,
                            "repoNotes": "\(txtv_Notes.text!)",
                            "effectiveDate": "\(txt_repo_EfftDate.text!)",
                            "repoObtained": "\(self.txt_repo_obtain.text!)",
                            "repoCharge": "\(txt_repo_charge.text!)",
                            "vehicleLocation": veh_id,
                            "recoveredBy": rec_by_id,
                            "recoveredAt": "\(txt_repo_recoverAt.text!)",
                            "witness1": wit1_d,
                            "witness2": wit2_d,
                            "address1": "\(txtAddress1.text!)",
                            "address2": "\(txtAddress2.text!)",
                            "city": "\(txtCity.text!)",
                            "state": self.state_id,
                            "zipCode": "\(txtZipCode.text!)",
                            "personalProperty": isProp,
                            "notified": isNotify,
                            "notifiedOn": "\(txt_on.text!)",
                            "daysToClaim": "\(txtClaimDays.text!)",
                            "claimDeadline": "\(txtClaimDeadline.text!)",
                            "description": "\(txtDescription.text!)"] as [String : Any]
                        print(param)
                        self.sendFormDatatoPost(params: param)
                    }
                }
                catch{
                    Globalfunc.print(object: "Couldn't read file.")
                }
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.get_repo_record, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
}


//Call Api for common
extension recordUpdateRepoVC
{
    func call_getDetailsApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.get_repo_record)?account_id=\(accId_selected)"
                
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.print(object:dict)
                            
                            if let response = dict as? NSDictionary{
                                if let resp = response["data"] as? NSDictionary{
                                    if let _ = resp["_id"] as? Int{
                                        
                                    }
                                    if let address1 = resp["address1"] as? String{
                                        self.txtAddress1.text = address1
                                    }
                                    if let address2 = resp["address2"] as? String{
                                        self.txtAddress2.text = address2
                                    }
                                    if let city = resp["city"] as? String{
                                        self.txtCity.text = city
                                    }
                                    if let claimDeadline = resp["claimDeadline"] as? String{
                                        self.txtClaimDeadline.text = claimDeadline
                                    }
                                    if let daysToClaim = resp["daysToClaim"] as? String{
                                        self.txtClaimDays.text = daysToClaim
                                    }
                                    if let description = resp["description"] as? String{
                                        self.txtDescription.text = description
                                    }
                                    if let zipCode = resp["zipCode"] as? String{
                                        self.txtZipCode.text = zipCode
                                    }
                                    if let repoReason = resp["repoReason"] as? String{
                                        self.txt_repo_obtain.text = repoReason
                                    }
                                    if let repoCharge = resp["repoCharge"] as? String{
                                        self.txt_repo_charge.text = repoCharge
                                    }
                                    if let repoNotes = resp["repoNotes"] as? String{
                                        self.txtv_Notes.text = repoNotes
                                    }
                                    if let notified = resp["notified"] as? Bool{
                                        self.isNotify = notified
                                        if(notified == false)
                                        {
                                            self.txtNotify.text = "No"
                                        }
                                        else{
                                            self.txtNotify.text = "Yes"
                                        }
                                    }
                                    if let personalProperty = resp["personalProperty"] as? Bool{
                                        self.isProp = personalProperty
                                        if(personalProperty == false)
                                        {
                                            self.txtPrsnlProp.text = "No"
                                        }
                                        else{
                                            self.txtPrsnlProp.text = "Yes"
                                        }
                                    }
                                    
                                    if let recoveredAt = resp["recoveredAt"] as? String{
                                        self.txt_repo_recoverAt.text = recoveredAt
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
    }
    
    func callAllPickerapi()
    {
        let group = DispatchGroup() // initialize
        arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if(obj == Constant.recover_rec){
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? [NSDictionary] {
                                    if(data.count > 0){
                                        for i in 0..<data.count{
                                            let dictA = data[i]
                                            self.arr_recoverBy.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(obj == Constant.assign_getUSer){
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? [NSDictionary] {
                                    if(data.count > 0){
                                        for i in 0..<data.count{
                                            let dictA = data[i]
                                            self.arrUser.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(obj == Constant.state){
                            if let response = dict as? NSDictionary {
                                
                                if let data = response["data"] as? [NSDictionary] {
                                    if(data.count > 0){
                                        for i in 0..<data.count{
                                            let dictA = data[i]
                                            self.arrState.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
    
    func callVehicleLocationApi()
    {
        let arrId = [9]
        let params = ["ct": arrId] as [String : Any]
        
        BaseApi.onResponsePostWithToken(url: Constant.vehicle_loc, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let resp = dict as? NSDictionary {
                        if let arr = resp["data"] as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrVehLoc.add(dictA)
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            self.callApiforBalTyp()
                        })
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    func callApiforBalTyp(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let account_id = dataD["_id"] as! Int
                let strUrl = "\(Constant.payment_url)?id=\(account_id)&balance_type=0&payment_type=0"
                Globalfunc.print(object:strUrl)
                self.callGetApi(strUrl: strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension recordUpdateRepoVC {
    
    func callGetApi(strUrl: String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    if let response = dict as? NSDictionary{
                        Globalfunc.print(object:response)
                        if let dictResponse = response["data"] as? NSDictionary{
                            if let balanceType = dictResponse["balanceType"] as? [NSDictionary]{
                                if(balanceType.count > 0){
                                    let dict = balanceType[0]
                                    print(dict)
                                    if let _id = dict["_id"] as? Int{
                                        self.balTyp = _id
                                    }
                                }
                            }
                        }
                    }
                    if let arr = dict as? NSDictionary{
                        if let msg = arr["message"] as? String
                        {
                            if(msg == "Your token has expired."){
                                self.viewWillAppear(true)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
            }
        }
    }
}

extension recordUpdateRepoVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        
        if pickerView == pv_repoObtaind{
            return self.arrRepoObtained.count
        }
            
        else if pickerView == pv_VehLoc{
            return self.arrVehLoc.count
        }
            
        else if pickerView == pv_recoverBy{
            return self.arr_recoverBy.count
        }
            
        else if pickerView == pv_recoverAt{
            return self.arrRecoverAt.count
        }
            
        else if pickerView == pv_witness1{
            return self.arrUser.count
        }
            
        else if pickerView == pv_witness2{
            return self.arrUser.count
        }
            
        else if pickerView == pv_property{
            return self.arrProp.count
        }
            
        else if pickerView == pv_notified{
            return self.arrProp.count
        }
            
        else if pickerView == pv_State{
            return self.arrState.count
        }
            
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pv_repoObtaind{
            let strTitle = arrRepoObtained[row]
            return strTitle
        }
            
        else if pickerView == pv_VehLoc{
            let dict = arrVehLoc[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
            
        }
            
        else if pickerView == pv_recoverBy{
            let dict = arr_recoverBy[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
            
        }
            
        else if pickerView == pv_recoverAt{
            let strTitle = arrRecoverAt[row]
            return strTitle
            
        }
            
        else if pickerView == pv_witness1{
            //  let strTitle = arrUSer[row]
            let dict = self.arrUser[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            
            return "\(fname) \(lname)"
            
        }
            
        else if pickerView == pv_witness2{
            //  let strTitle = arrUSer[row]
            let dict = self.arrUser[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            
            return "\(fname) \(lname)"
            
        }
            
        else if pickerView == pv_property{
            
            var str = ""
            let isSelect = arrProp[row]
            if(isSelect == true){
                str = "Yes"
            }else{
                str = "No"
            }
            return str
        }
            
        else if pickerView == pv_notified{
            
            var str = ""
            let isSelect = arrProp[row]
            if(isSelect == true){
                str = "Yes"
            }else{
                str = "No"
            }
            return str
        }
            
        else if pickerView == pv_State{
            
            let dict = arrState[row] as! NSDictionary
            let state_name = dict["state_name"] as! String
            return state_name
            
            
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if pickerView == pv_repoObtaind{
            self.txt_repo_obtain.text = self.arrRepoObtained[row]
        }
            
        else if pickerView == pv_VehLoc{
            
            let dict = arrVehLoc[row] as! NSDictionary
            self.txt_repo_vehLoc.text = (dict["title"] as! String)
            veh_id = "\(dict["_id"] as! Int)"
            
        }
            
        else if pickerView == pv_recoverBy{
            
            let dict = arr_recoverBy[row] as! NSDictionary
            self.txt_repo_recoverBy.text = (dict["title"] as! String)
            rec_by_id = "\(dict["_id"] as! Int)"
            
        }
            
        else if pickerView == pv_recoverAt{
            let strTitle = arrRecoverAt[row]
            self.txt_repo_recoverAt.text = strTitle
            
        }
            
        else if pickerView == pv_witness1{
            
            let dict = self.arrUser[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            self.txt_repo_wit1.text = "\(fname) \(lname)"
            
            self.wit1_d = (dict["_id"] as! Int)
            
        }
            
        else if pickerView == pv_witness2{
            
            let dict = self.arrUser[row] as! NSDictionary
            let fname = dict["fname"] as! String
            let lname = dict["lname"] as! String
            self.txt_repo_wit2.text = "\(fname) \(lname)"
            
            self.wit2_d = (dict["_id"] as! Int)
            
        }
            
        else if pickerView == pv_property{
            
            let isSelect = arrProp[row]
            isProp = isSelect
            
            if(isSelect == true){
                self.txtPrsnlProp.text = "Yes"
            }else{
                self.txtPrsnlProp.text = "No"
            }
        }
            
        else if pickerView == pv_notified{
            
            let isSelect = arrProp[row]
            isNotify = isSelect
            
            if(isSelect == true){
                self.txtNotify.text = "Yes"
            }else{
                self.txtNotify.text = "No"
            }
            
        }
            
        else if pickerView == pv_State{
            
            let dict = arrState[row] as! NSDictionary
            let state_name = dict["state_name"] as! String
            self.txtstate.text = state_name
            self.state_id = "\(dict["_id"] as! Int)"
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        
        if(textField == self.txt_repo_obtain){
            self.pickUp(txt_repo_obtain)
        }
        else if(textField == self.txt_repo_vehLoc){
            self.pickUp(txt_repo_vehLoc)
        }
        else if(textField == self.txt_repo_recoverBy){
            self.pickUp(txt_repo_recoverBy)
        }
        else if(textField == self.txt_repo_recoverAt){
            self.pickUp(txt_repo_recoverAt)
        }
        else if(textField == self.txt_repo_wit1){
            self.pickUp(txt_repo_wit1)
        }
        else if(textField == self.txt_repo_wit2){
            self.pickUp(txt_repo_wit2)
        }
        else if(textField == self.txt_repo_EfftDate){
            self.pickUpDateTime(txt_repo_EfftDate)
        }
        else if(textField == self.txtPrsnlProp){
            self.pickUp(txtPrsnlProp)
        }
        else if(textField == self.txtNotify){
            self.pickUp(txtNotify)
        }
        else if(textField == self.txtstate){
            self.pickUp(txtstate)
        }
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txt_repo_EfftDate){
            self.pvEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pvEffDate.datePickerMode = .date
            self.pvEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pvEffDate
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txt_repo_obtain){
            self.pv_repoObtaind = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_repoObtaind.delegate = self
            self.pv_repoObtaind.dataSource = self
            self.pv_repoObtaind.backgroundColor = UIColor.white
            textField.inputView = self.pv_repoObtaind
            
        }
            
        else if(textField == self.txt_repo_vehLoc){
            self.pv_VehLoc = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_VehLoc.delegate = self
            self.pv_VehLoc.dataSource = self
            self.pv_VehLoc.backgroundColor = UIColor.white
            textField.inputView = self.pv_VehLoc
            
        }
            
        else if(textField == self.txt_repo_recoverBy){
            self.pv_recoverBy = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_recoverBy.delegate = self
            self.pv_recoverBy.dataSource = self
            self.pv_recoverBy.backgroundColor = UIColor.white
            textField.inputView = self.pv_recoverBy
            
        }
            
        else if(textField == self.txt_repo_recoverAt){
            self.pv_recoverAt = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_recoverAt.delegate = self
            self.pv_recoverAt.dataSource = self
            self.pv_recoverAt.backgroundColor = UIColor.white
            textField.inputView = self.pv_recoverAt
        }
            
        else if(textField == self.txt_repo_wit1){
            self.pv_witness1 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_witness1.delegate = self
            self.pv_witness1.dataSource = self
            self.pv_witness1.backgroundColor = UIColor.white
            textField.inputView = self.pv_witness1
        }
            
        else if(textField == self.txt_repo_wit2){
            self.pv_witness2 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_witness2.delegate = self
            self.pv_witness2.dataSource = self
            self.pv_witness2.backgroundColor = UIColor.white
            textField.inputView = self.pv_witness2
        }
            
        else if(textField == self.txtPrsnlProp){
            self.pv_property = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_property.delegate = self
            self.pv_property.dataSource = self
            self.pv_property.backgroundColor = UIColor.white
            textField.inputView = self.pv_property
        }
        else if(textField == self.txtNotify){
            self.pv_notified = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_notified.delegate = self
            self.pv_notified.dataSource = self
            self.pv_notified.backgroundColor = UIColor.white
            textField.inputView = self.pv_notified
        }
        else if(textField == self.txtstate){
            self.pv_State = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_State.delegate = self
            self.pv_State.dataSource = self
            self.pv_State.backgroundColor = UIColor.white
            textField.inputView = self.pv_State
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txt_repo_EfftDate.text = formatter.string(from: pvEffDate.date)
        self.txt_repo_EfftDate.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txt_repo_EfftDate.resignFirstResponder()
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txt_repo_obtain.resignFirstResponder()
        txt_repo_vehLoc.resignFirstResponder()
        txt_repo_recoverBy.resignFirstResponder()
        txt_repo_recoverAt.resignFirstResponder()
        txt_repo_wit1.resignFirstResponder()
        txt_repo_wit2.resignFirstResponder()
        
        txtNotify.resignFirstResponder()
        txtPrsnlProp.resignFirstResponder()
        txtstate.resignFirstResponder()
    }
    
    
    @objc func cancelClick() {
        
        txt_repo_obtain.resignFirstResponder()
        txt_repo_vehLoc.resignFirstResponder()
        txt_repo_recoverBy.resignFirstResponder()
        txt_repo_recoverAt.resignFirstResponder()
        txt_repo_wit1.resignFirstResponder()
        txt_repo_wit2.resignFirstResponder()
        
        txtNotify.resignFirstResponder()
        txtPrsnlProp.resignFirstResponder()
        txtstate.resignFirstResponder()
        
        
    }
}
