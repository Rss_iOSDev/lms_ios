//
//  bankruptcyVC.swift
//  LMS
//
//  Created by Apple on 08/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class bankruptcyVC: UIViewController {
    
    @IBOutlet weak var txtConId : UITextField!
    @IBOutlet weak var txtDate : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    
    @IBOutlet weak var lblBankRpt : UILabel!
    
    @IBOutlet weak var btnConID : UIButton!
    @IBOutlet weak var btnDate : UIButton!
    @IBOutlet weak var btnNotes : UIButton!
    @IBOutlet weak var lblActtNo : UILabel!
    
    @IBOutlet weak var btnChecbox : UIButton!
    
    var pv_cid: UIPickerView!
    var arrCid : NSMutableArray = []
    var pv_date: UIDatePicker!
    
    var id_cons = ""
    var bankstatus = false
    var bank_ruptcy = false
    var isBankrupt = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
               let decoded  = userDef.object(forKey: "dataDict") as! Data
                   if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                       let acct_no = dataD["account_no"] as! String
                       self.lblActtNo.text = "Account no: \(acct_no)"
                   }
        }
        catch{}

        Globalfunc.showLoaderView(view: self.view)
        self.callApiToSetData(strCheck: "cid")
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnCancelBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnCheckBoxBtn(_ sender: UIButton)
    {
        if(isBankrupt == ""){
            if self.btnChecbox.isSelected == true {
                self.btnChecbox.isSelected = false
                self.btnChecbox.setImage(UIImage(named : "uncheck"), for: .normal)
                self.btnDate.isHidden = false
                self.btnConID.isHidden = false
                self.btnNotes.isHidden = false
                self.bank_ruptcy = true
                
            }else {
                self.btnChecbox.isSelected = true
                self.btnChecbox.setImage(UIImage(named : "check"), for: .normal)
                self.btnDate.isHidden = true
                self.btnConID.isHidden = true
                self.btnNotes.isHidden = true
                self.bank_ruptcy = false
            }
        }else{
            if(self.bank_ruptcy == false){
                self.btnChecbox.isSelected = true
                self.btnChecbox.setImage(UIImage(named : "check"), for: .normal)
                self.bank_ruptcy = true
            }
            else if(self.bank_ruptcy == true){
                self.btnChecbox.isSelected = false
                self.btnChecbox.setImage(UIImage(named : "uncheck"), for: .normal)
                self.bank_ruptcy = false
            }
        }
    }
}

extension bankruptcyVC{
    
    @IBAction func clickONPostBtn(_ sender: UIButton)
    {
        
        if(txtConId.text == "" || txtConId.text?.count == 0 || txtConId.text == nil){
            txtConId.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        if(txtDate.text == "" || txtDate.text?.count == 0 || txtDate.text == nil){
            txtDate.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            
            Globalfunc.showLoaderView(view: self.view)
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let acct_id = dataD["_id"] as! Int
                    let insID = dataD["insid"] as! Int
                    let userid = dataD["userid"] as! Int
                    let bankRupcy : Bool!
                    if(self.bankstatus == true){
                        bankRupcy = false
                    }
                    else{
                        bankRupcy = true
                    }
                    let param = ["account_id": acct_id,
                                 "insid": insID,
                                 "userid": userid,
                                 "notes": "\(txtNotes.text!)",
                        "petition_date": "\(txtDate.text!)",
                        "channel" : Int(id_cons)!,
                        "bank_ruptcy" : bankRupcy!] as [String : Any]
                    Globalfunc.print(object:param)
                    self.sendFormDatatoPost(params: param)
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
        
    }
}

extension bankruptcyVC {
    
    func sendFormDatatoPost(params : [String : Any]){
        
        BaseApi.onResponsePostWithToken(url: Constant.action_bankRuptcy, controller: self, parms: params) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                            // self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension bankruptcyVC
{
    func getBankRuptcyStatus()
    {
        do {
            let decoded  = userDef.object(forKey: "action_dict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                OperationQueue.main.addOperation {
                    
                    if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                        
                        print(loanAccount)
                        
                        self.bankstatus = loanAccount["bankRuptcy_status"] as! Bool
                        if(self.bankstatus == true){
                            self.btnDate.isHidden = true
                            self.btnConID.isHidden = true
                            self.btnNotes.isHidden = true
                            self.isBankrupt = "get"
                            self.callApiToSetData(strCheck: "action_bank")
                        }
                        else{
                            self.isBankrupt = ""
                            self.btnDate.isHidden = false
                            self.btnConID.isHidden = false
                            self.btnNotes.isHidden = false
                            
                        }
                        
                    }
                    
                }
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
}

//Api get
extension bankruptcyVC {
    
    func callApiToSetData(strCheck : String){
        
        if(strCheck == "cid"){
            BaseApi.callApiRequestForGet(url: Constant.action_cons_ind_info) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        
                        if let response = dict as? NSDictionary {
                            if let responseArr = response["data"] as? [NSDictionary] {
                                if(responseArr.count > 0){
                                    for i in 0..<responseArr.count{
                                        let dictA = responseArr[i]
                                        self.arrCid.add(dictA)
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            // your code here
                            self.getBankRuptcyStatus()
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
            
        else if(strCheck == "action_bank"){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let acct_id = dataD["_id"] as! Int
                    
                    let strUrl = "\(Constant.action_bankRuptcy)?account_id=\(acct_id)"
                    BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                        if(error == ""){
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.print(object: dict)
                                
                                if let response = dict as? NSDictionary {
                                    if let responseArr = response["data"] as? [NSDictionary]{
                                        let B_dict = responseArr[0]
                                        if let bankRuptcy = B_dict["bankRuptcy"] as? NSDictionary{
                                            Globalfunc.print(object:bankRuptcy)
                                            
                                            self.bank_ruptcy = bankRuptcy["bank_ruptcy"] as! Bool
                                            if(self.bank_ruptcy == false){
                                                self.btnChecbox.isSelected = true
                                                self.btnChecbox.setImage(UIImage(named : "check"), for: .normal)
                                                self.bank_ruptcy = true
                                                self.lblBankRpt.text = "Move To Bankruptcy"
                                                
                                            }
                                            else if(self.bank_ruptcy == true){
                                                self.btnChecbox.isSelected = false
                                                self.btnChecbox.setImage(UIImage(named : "uncheck"), for: .normal)
                                                self.bank_ruptcy = false
                                                self.lblBankRpt.text = "Take Off Bankruptcy"
                                            }
                                            
                                            let channel =  bankRuptcy["channel"] as! Int
                                            for i in 0..<self.arrCid.count{
                                                if(i == channel){
                                                    if(i > 0){
                                                        let dict = self.arrCid[i-1] as! NSDictionary
                                                        let title = dict["title"] as! String
                                                        self.txtConId.text = title
                                                        self.id_cons = "\(dict["_id"] as! Int)"
                                                        break
                                                    }
                                                }
                                            }
                                            
                                            if let notes =  bankRuptcy["notes"] as? String{
                                                self.txtNotes.text = notes
                                            }
                                            
                                            if let petition_date =  bankRuptcy["petition_date"] as? String
                                            {
                                                let formatter = DateFormatter()
                                                formatter.dateFormat = "MM/dd/yyyy"
                                                let date = Date.dateFromISOString(string: petition_date)
                                                self.txtDate.text = formatter.string(from: date!)
                                            }
                                        }
                                    }
                                    
                                    
                                }
                            }
                        }
                        else
                        {
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                            }
                        }
                    }
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
        }
    }
}


extension bankruptcyVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pv_cid){
            return self.arrCid.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pv_cid){
            let dict = self.arrCid[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
            
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pv_cid){
            let dict = self.arrCid[row] as! NSDictionary
            let title = dict["title"] as! String
            self.txtConId.text = title
            id_cons = "\(dict["_id"] as! Int)"
        }
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtConId){
            self.pickUp(txtConId)
        }
        else if(textField == self.txtDate){
            self.pickUpDateTime(txtDate)
        }
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDate){
            self.pv_date = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_date.datePickerMode = .date
            self.pv_date.backgroundColor = UIColor.white
            textField.inputView = self.pv_date
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtConId){
            self.pv_cid = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_cid.delegate = self
            self.pv_cid.dataSource = self
            self.pv_cid.backgroundColor = UIColor.white
            textField.inputView = self.pv_cid
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtDate.text = formatter.string(from: pv_date.date)
        self.txtDate.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txtDate.resignFirstResponder()
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtConId.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtConId.resignFirstResponder()        
    }
}
