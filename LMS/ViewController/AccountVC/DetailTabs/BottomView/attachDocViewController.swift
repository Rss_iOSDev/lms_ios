//
//  attachDocViewController.swift
//  LMS
//
//  Created by Apple on 18/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import Alamofire

protocol AttchmntDelegate {
    func sendAttchnmnt(attachmntArr: [NSDictionary])
}


class attachDocViewController: UIViewController {
    
    @IBOutlet weak var txtReqDesc: UITextField!
    
    @IBOutlet weak var tblTempList: UITableView!
    
    var arrTempListData = NSMutableArray()
    var arrSelectList = NSMutableArray()
    
    var delegate: AttchmntDelegate?
    var attachDocArrList : [NSDictionary] = []
    
    var acctNo = ""
    var isOpenFrom = ""
    
    var isFromInventory = ""
    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if(isFromInventory == "true"){
            
            if let _id  = userDef.object(forKey: "inventoryId") as? Int {
                self.txtReqDesc.text = "Inventory Documents for Inventory ID: \(_id)"
            }
            
        }else{
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let accNo = responseDict["account_no"] as! String
                    self.acctNo = accNo
                    self.txtReqDesc.text = "Account Documents for Acct ID: \(accNo)"
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }

        }
        
        
        tblTempList.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        self.getTemplateList()
    }
    
    @IBAction func clickOnFaxCheck(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            
        }else {
            sender.isSelected = true
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension attachDocViewController {
    
    func getTemplateList(){
        
        var strUrl = ""
        if(isFromInventory == "true"){
            strUrl = Constant.inventory_docTemplate
            
        }else{
             strUrl = Constant.docTemplate
        }

        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary{
                        if let arrData = response["data"] as? [NSDictionary] {
                            if(arrData.count > 0){
                                for i in 0..<arrData.count{
                                    let dict = arrData[i]
                                    self.arrTempListData.add(dict)
                                }
                                for i in 0..<self.arrTempListData.count
                                {
                                    let dictA = self.arrTempListData[i] as! NSMutableDictionary
                                    dictA.setValue("false", forKey: "selected")
                                    self.arrTempListData.replaceObject(at: i, with: dictA)
                                }
                                self.tblTempList.reloadData()
                            }
                            else{
                                self.arrTempListData = []
                                self.tblTempList.isHidden = true
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension attachDocViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTempListData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblTempList.dequeueReusableCell(withIdentifier: "tblTempListCell") as! tblTempListCell
        let dict = self.arrTempListData[indexPath.row] as! NSDictionary
        
        let selected = dict["selected"] as! String
        if(selected == "true"){
            cell.btnCheckFlag.isSelected = true
        }
        else if(selected == "false"){
            cell.btnCheckFlag.isSelected = false
        }
        
        let description = dict["templateName"] as! String
        cell.lblTempName.text = "\(description)"
        
        cell.btnCheckFlag.tag = indexPath.row
        cell.btnCheckFlag.addTarget(self, action: #selector(clickONCheckBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONCheckBtn(_ sender: UIButton)
    {
        let dict = self.arrTempListData[sender.tag] as! NSMutableDictionary
        if (self.arrSelectList.contains(dict))
        {
            self.arrSelectList.remove(dict)
            dict.setValue("false", forKey: "selected")
            self.arrTempListData.replaceObject(at: sender.tag, with: dict)
            self.tblTempList.reloadData()
        }
        else{
            self.arrSelectList.add(dict)
            dict.setValue("true", forKey: "selected")
            self.arrTempListData.replaceObject(at: sender.tag, with: dict)
            self.tblTempList.reloadData()
        }
    }
}

class tblTempListCell : UITableViewCell
{
    @IBOutlet weak var lblTempName : UILabel!
    @IBOutlet weak var btnCheckFlag : UIButton!
}

extension attachDocViewController {
    
    @IBAction func clickOnGeneratePdfBtn(_ sender: UIButton)
    {
        if(self.arrSelectList.count == 0)
        {
            Globalfunc.showToastWithMsg(view: self.view, str: "Select document to generate.")
        }
        else{
            Globalfunc.showLoaderView(view: self.view)
            self.postApiToSendEmail()
        }
    }
    
    func postApiToSendEmail(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                for i in 0..<self.arrSelectList.count{
                    let dict = self.arrSelectList[i] as! NSDictionary
                    let _id = dict["_id"] as! Int
                    let templateName = dict["templateName"] as! String
                    let htmlTemplate = dict["htmlTemplate"] as! String
                    
                    let dictAdd : NSMutableDictionary = [:]
                    dictAdd.setValue(_id, forKey: "docId")
                    dictAdd.setValue(templateName, forKey: "docTitle")
                    dictAdd.setValue(htmlTemplate, forKey: "htmlString")
                    self.arrSelectList.replaceObject(at: i, with: dictAdd)
                }
                let acctId = dataD["_id"] as! Int
                let param = ["account_id": acctId,
                             "docs": self.arrSelectList] as [String : Any]
                Globalfunc.print(object:param)
                self.sendDatatoPost(param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func sendDatatoPost(_ param: [String: Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.generate_pdf, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        if let htmlData = responseDict["htmlData"] as? [NSDictionary]
                        {
                            let dict = htmlData[0]
                            let docTitle = dict["docTitle"] as! String
                            let pdfData = self.generatePdfFromHtml(Htmldict: dict)
                            var fileSize : UInt64?
                            let dd = self.attachDocArrList[0]
                            let file_path = dd["file_path"] as! String
                            do {
                                let attr = try FileManager.default.attributesOfItem(atPath: file_path)
                                fileSize = (attr[FileAttributeKey.size] as! UInt64)
                                let dict = attr as NSDictionary
                                fileSize = dict.fileSize()
                            } catch {
                                print("Error: \(error)")
                            }
                            
                            if(self.isFromInventory == "true"){
                                
                                if let _id  = userDef.object(forKey: "inventoryId") as? Int {
                                    
                                    let strFname = user?.fname
                                    let strLname = user?.lname
                                    let onwner = String((strFname?.first)!) + String((strLname?.first)!)

                                    let dict1 = ["inventory_id" : _id]
                                    let dict2 = ["inventory_no" : _id]
                                    let dict3 = ["owner": "\(onwner)\((user?._id)!)"]
                                    let dict4 = ["fileSize": fileSize]
                                    let dict5 = ["customer": ""]
                                    let dict6 = ["included_forms": docTitle]
                                    let dict7 = ["userid": (user?._id)!]
                                    let dict8 = ["insid": (user?.institute_id!)!]
                                    
                                    let arrParam = NSMutableArray()
                                    
                                    arrParam.insert(dict1, at: 0)
                                    arrParam.insert(dict2, at: 1)
                                    arrParam.insert(dict3, at: 2)
                                    arrParam.insert(dict4, at: 3)
                                    arrParam.insert(dict5, at: 4)
                                    arrParam.insert(dict6, at: 5)
                                    arrParam.insert(dict7, at: 6)
                                    arrParam.insert(dict8, at: 7)
                                    
                                    self.uploadPdfFileServer(pdfData: pdfData, fileName: docTitle, fileSize: fileSize!, arrParam: arrParam)

                                }
                            }
                            else{
                                do {
                                    let decoded  = userDef.object(forKey: "dataDict") as! Data
                                    if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                                        let accId_selected = dataD["_id"] as! Int
                                        let insID = dataD["insid"] as! Int
                                        let userid = dataD["userid"] as! Int
                                        
                                        
                                        let borrower_dict = dataD["loanBorrower"] as! NSDictionary
                                        let first_name = borrower_dict["first_name"] as! String
                                        let last_name = borrower_dict["last_name"] as! String
                                        
                                        let strFname = user?.fname
                                        let strLname = user?.lname
                                        let onwner = String((strFname?.first)!) + String((strLname?.first)!)
                                        
                                        let dict1 = ["account_id" : accId_selected]
                                        let dict2 = ["account_no" : self.acctNo]
                                        let dict3 = ["owner": "\(onwner)\(userid)"]
                                        let dict4 = ["fileSize": fileSize]
                                        let dict5 = ["customer": "\(first_name) \(last_name)"]
                                        let dict6 = ["included_forms": docTitle]
                                        let dict7 = ["userid": userid]
                                        let dict8 = ["insid": insID]
                                        
                                        let arrParam = NSMutableArray()
                                        
                                        arrParam.insert(dict1, at: 0)
                                        arrParam.insert(dict2, at: 1)
                                        arrParam.insert(dict3, at: 2)
                                        arrParam.insert(dict4, at: 3)
                                        arrParam.insert(dict5, at: 4)
                                        arrParam.insert(dict6, at: 5)
                                        arrParam.insert(dict7, at: 6)
                                        arrParam.insert(dict8, at: 7)
                                        
                                        self.uploadPdfFileServer(pdfData: pdfData, fileName: docTitle, fileSize: fileSize!, arrParam: arrParam)
                                    }
                                }
                                catch {
                                }

                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    
    func generatePdfFromHtml(Htmldict: NSDictionary) -> Data
    {
        let strHtml = Htmldict["htmlString"] as! String
        
        let docTitle = Htmldict["docTitle"] as! String
        
        let fmt = UIMarkupTextPrintFormatter(markupText: strHtml)
        
        // 2. Assign print formatter to UIPrintPageRenderer
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(fmt, startingAtPageAt: 0)
        
        // 3. Assign paperRect and printableRect+
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        let printable = page.insetBy(dx: 0, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect(x: 0, y: 0, width: 0, height: 0), nil)
        
        for i in 1...render.numberOfPages
        {
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        // 5. Save PDF file
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = "\(documentsPath)/\(docTitle).pdf"
        
        print(filePath)
        
        pdfData.write(toFile: filePath, atomically: true)
        
        let dict : NSMutableDictionary = [:]
        dict.setValue(self.acctNo, forKey: "accountNo")
        dict.setValue("doc", forKey: "type")
        dict.setValue(filePath, forKey: "file_path")
        
        
        self.attachDocArrList.append(dict)
        return pdfData as Data
        
    }
    
    
    func uploadPdfFileServer(pdfData: Data, fileName: String, fileSize: UInt64, arrParam: NSMutableArray)
    {
        
        let authToken = userDef.value(forKey: "authToken") as! String
        let headers: HTTPHeaders = [
            "Authorization": authToken,
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            // import image to request
            multipartFormData.append(pdfData, withName: "file", fileName: fileName, mimeType: "application/pdf")
            for i in 0..<arrParam.count{
                let parameters  = arrParam[i] as! [String:Any]
                for (key, value) in parameters {
                    let newVal = "\(value)"
                    multipartFormData.append((newVal).data(using: String.Encoding.utf8)!, withName: key)
                }
            }
        }, to: Constant.up_document,method:HTTPMethod.post,
           headers:headers,
           encodingCompletion: { encodingResult in
            DispatchQueue.main.async {
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        Globalfunc.hideLoaderView(view: self.view)
                        if let dict = response.result.value as? NSDictionary{
                            let msg = dict["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                                if(self.isOpenFrom == "email"){
                                    self.dismiss(animated: true){
                                        self.delegate?.sendAttchnmnt(attachmntArr: self.attachDocArrList)
                                    }
                                }
                                else if(self.isOpenFrom == "document"){
                                    let dict = self.attachDocArrList[0]
                                    let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
                                    let vc = story.instantiateViewController(withIdentifier: "pdfDocViewController") as! pdfDocViewController
                                    vc.modalPresentationStyle = .fullScreen
                                    vc.passDict = dict
                                    vc.isFrom = "document"
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                }
            }
        })
    }
}

