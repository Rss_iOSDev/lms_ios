//
//  email_sendVC.swift
//  LMS
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import MessageUI
import WebKit

var arrAtchmntList = NSMutableArray()

class email_sendVC: UIViewController , MFMailComposeViewControllerDelegate, AttchmntDelegate{
    
    @IBOutlet weak var txtEmailSubject : UITextField!
    @IBOutlet weak var txtTemplate : UITextField!
    
    @IBOutlet weak var txtMEssage : UITextView!
    
    @IBOutlet weak var tblAttach : UITableView!
    @IBOutlet weak var hghtTbl : NSLayoutConstraint!
    
    @IBOutlet weak var docView : UIView!
    @IBOutlet weak var webView: WKWebView!
    
    var pickerViewTemplate: UIPickerView!
    var arrTemplate : NSMutableArray = []
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var email = ""
    var account_no : String = ""
    var isFromReceipt = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.docView.isHidden = true
        self.setAttachDocDataonlist()
        self.lblAccountNo.text = "Account No: \(account_no)"
        Globalfunc.showLoaderView(view: self.view)
        
        let url = "\(Constant.getTemplate_url)&templateType=1"
        self.getTemplateList(strUrl: url, strTyp: "template")
        if(isFromReceipt == true){
            let attach =  userDef.object(forKey: "attach_receipt") as! [NSDictionary]
            sendAttchnmnt(attachmntArr: attach)
        }
    }
    
    func sendAttchnmnt(attachmntArr: [NSDictionary]) {
        for i in 0..<attachmntArr.count{
            let dict = attachmntArr[i]
            arrAtchmntList.add(dict)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.setAttachDocDataonlist()
        })
    }
    
    func setAttachDocDataonlist(){
        if(arrAtchmntList.count > 0){
            self.tblAttach.tableFooterView = UIView()
            self.tblAttach.delegate = self
            self.tblAttach.dataSource = self
            self.hghtTbl.constant = 100
            self.tblAttach.reloadData()
        }
        else{
            self.tblAttach.tableFooterView = UIView()
            self.hghtTbl.constant = 0
        }
    }
    
    @IBAction func clikcONSendEmail(_ sender: UIButton){
        if(sender.tag == 50){
            Globalfunc.showLoaderView(view: self.view)
            self.postApiToSendEmail()
        }
        else if(sender.tag == 60){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func clikcONBackbtn(_ sender: UIButton){
        arrAtchmntList = []
        userDef.removeObject(forKey: "attach_receipt")
        self.view.window!.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONCros(_ sender : UIButton)
    {
        self.docView.isHidden = true
    }
    
}

extension email_sendVC {
    
    func getTemplateList(strUrl : String, strTyp: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if(strTyp == "template"){
                        if let response = dict as? NSDictionary{
                            if let responseDict = response["data"] as? NSMutableArray {
                                self.arrTemplate = responseDict
                            }
                        }
                    }
                    else if(strTyp == "temp_select"){
                        if let responseDict = dict as? NSDictionary {
                            if let htmlTemplate = responseDict["htmlTemplate"] as? String{
                                self.txtMEssage.attributedText = htmlTemplate.htmlToAttributedString
                                self.txtMEssage.isUserInteractionEnabled = true
                                self.txtMEssage.isEditable = true
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func postApiToSendEmail(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acctId = dataD["_id"] as! Int
                
                let attachFile = NSMutableArray()
                
                for i in 0..<arrAtchmntList.count{
                    
                    let newdict : NSMutableDictionary = [:]
                    
                    let dict = arrAtchmntList[i] as! NSDictionary
                    let accountNo = dict["accountNo"] as! String
                    let str = "Account Documents for Account ID:\(accountNo)"
                    newdict.setValue(str, forKey: "attach_name")
                    
                    let type = dict["type"] as! String
                    let file_path = dict["file_path"] as! String
                    
                    var final_url : URL?
                    if(type == "doc"){
                        final_url = NSURL(fileURLWithPath: file_path) as URL
                    }
                    else if(type == "ecab"){
                        final_url = URL(string: file_path)!
                    }
                    do {
                        let fileData = try Data.init(contentsOf: final_url!)
                        let fileStream:String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
                        newdict.setValue(fileStream, forKey: "attach_file")
                    }
                    catch { }
                    attachFile.add(newdict)
                }
                print(attachFile)
                let param = ["account_id": acctId,
                             "email": "\(email)",
                    "htmlString": "\(txtMEssage.text!)",
                    "attachment":attachFile,
                    "subject": "\(self.txtEmailSubject.text!)"] as [String : Any]
                Globalfunc.print(object:param)
                self.sendDatatoPost(param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func sendDatatoPost(_ param: [String: Any])
    {
        print(Constant.send_url)
        BaseApi.onResponsePostWithToken(url: Constant.send_url, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        let msg = responseDict["msg"] as! String
                        let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.dismiss(animated: true) {
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension email_sendVC {
    
    @IBAction func clickonAttachNewDocBtn(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "attachDocViewController") as! attachDocViewController
        dest.modalPresentationStyle = .fullScreen
        dest.delegate = self
        dest.isOpenFrom = "email"
        self.present(dest, animated: true, completion: nil)
    }
    
    @IBAction func clickonAttachEcabBtn(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "attachEcabViewController") as! attachEcabViewController
        dest.modalPresentationStyle = .fullScreen
        dest.delegate = self
        self.present(dest, animated: true, completion: nil)
    }
}

extension email_sendVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewTemplate{
            return self.arrTemplate.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerViewTemplate{
            
            let dict = arrTemplate[row] as! NSDictionary
            let strTitle = dict["templateName"] as! String
            return strTitle
        }
        
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerViewTemplate{
            let dict = arrTemplate[row] as! NSDictionary
            self.txtTemplate.text = (dict["templateName"] as! String)
            self.txtTemplate.tag = row
            
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtTemplate){
            self.pickUp(txtTemplate)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtTemplate){
            self.pickerViewTemplate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTemplate.delegate = self
            self.pickerViewTemplate.dataSource = self
            self.pickerViewTemplate.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTemplate
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick(){
        txtTemplate.resignFirstResponder()
        
        let dict = arrTemplate[txtTemplate.tag] as! NSDictionary
        let _id = dict["_id"] as! Int
        let url = "\(Constant.getTemplate_layout_url)?id=\(_id)"
        Globalfunc.showLoaderView(view: self.view)
        self.getTemplateList(strUrl: url, strTyp: "temp_select")
        
    }
    
    @objc func cancelClick() {
        txtTemplate.resignFirstResponder()
        txtTemplate.text = ""
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


extension email_sendVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAtchmntList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblAttach.dequeueReusableCell(withIdentifier: "tblAttachcell") as! tblAttachcell
        let dict = arrAtchmntList[indexPath.row] as! NSDictionary
        let accountNo = dict["accountNo"] as! String
        cell.lblFileNam.text = "Account Documents for Account ID:\(accountNo)"
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(clickONCheckBtn(_:)), for: .touchUpInside)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clickONRemoveBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONCheckBtn(_ sender: UIButton)
    {
        let dict = arrAtchmntList[sender.tag] as! NSDictionary
        let type = dict["type"] as! String
        let file_path = dict["file_path"] as! String
        self.docView.isHidden = false
        if(type == "doc"){
            let url = NSURL(fileURLWithPath: file_path)
            let urlRequest = URLRequest(url: url as URL)
            webView.load(urlRequest)
            webView.allowsBackForwardNavigationGestures = true
        }
        else if(type == "ecab"){
            let url = URL(string: file_path)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
        }
    }
    
    
    @objc func clickONRemoveBtn(_ sender: UIButton)
    {
        let dict = arrAtchmntList[sender.tag] as! NSDictionary
        for i in 0..<arrAtchmntList.count{
            let dd = arrAtchmntList[i] as! NSDictionary
            if(dd == dict){
                arrAtchmntList.remove(dd)
                if(arrAtchmntList.count == 0){
                    self.hghtTbl.constant = 0
                }
                self.tblAttach.reloadData()
            }
        }
    }
}

class tblAttachcell : UITableViewCell
{
    @IBOutlet weak var lblFileNam : UILabel!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var btnRemove : UIButton!
}
