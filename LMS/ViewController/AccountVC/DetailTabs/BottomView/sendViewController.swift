//
//  sendViewController.swift
//  LMS
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class sendViewController: UIViewController {
    
    @IBOutlet weak var viewSend : UIView!
    @IBOutlet weak var lbltitle : UILabel!
    @IBOutlet weak var btnBack : UIButton!
    
    
    
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDetail : UILabel!
    
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    @IBOutlet weak var tblList : UITableView!
    var arrRefrence = NSMutableArray()
    @IBOutlet weak var lblTitle : UILabel!
    
    var isSelectForm = ""
    
    var acctNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewSend.isHidden = true
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let account_no = dataD["account_no"] as! String
                self.acctNo = account_no
                self.lblAccountNo.text = "Account No: \(account_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        
        
    }
    
    @IBAction func clikOnBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clikcOnSendBtn(_ sender : UIButton){
        
        self.lblDetail.text = ""
        
        if(sender.tag == 10){
            self.viewSend.isHidden = false
            isSelectForm = "email"
            lbltitle.text = "SEND EMAIL"
            self.getCallDetailsApi()
        }
        else if(sender.tag == 20){
            self.viewSend.isHidden = false
            isSelectForm = "text"
            lbltitle.text = "SEND TEXT"
            self.getCallDetailsApi()
        }
        else if(sender.tag == 30){
            
        }
        
    }
    
    @IBAction func clikcOnCancelBtn(_ sender : UIButton){
        self.viewSend.isHidden = true
        btnBack.isHidden = false
    }
}


extension sendViewController{
    
    func getCallDetailsApi()
    {
        do {
            let decoded  = userDef.object(forKey: "action_dict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:responseDict)
                OperationQueue.main.addOperation {
                    //  Globalfunc.hideLoaderView(view: self.view)
                    
                    if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                        
                        let first_name = loanAccount["firstName"] as? String
                        let last_name = loanAccount["lastName"] as? String
                        
                        self.lblName.text = "\(first_name!) \(last_name!) [PRIMARY]"
                        let fLeter = first_name?.trimmingCharacters(in: .whitespaces).first
                        let lLeter = last_name?.trimmingCharacters(in: .whitespaces).first
                        self.lblTitle.text = "\(fLeter!)\(lLeter!)"
                        
                        if(self.isSelectForm == "text"){
                            
                            if let cellPhone = loanAccount["cellPhone"] as? String{
                                self.lblDetail.text = "Cell Phone: \(cellPhone)"
                            }
                            else{
                                self.lblDetail.text = "Cell Phone: (832)893-5153"
                            }
                        }
                        else if(self.isSelectForm == "email"){
                            if let email = loanAccount["email"] as? String{
                                self.lblDetail.text = "Email: \(email)"
                                if(email != ""){
                                    self.setupLabelTap()
                                }
                                else{
                                }
                            }
                        }
                    }
                    
                    if let reference = responseDict["reference"] as? [NSDictionary]{
                        for i in 0..<reference.count{
                            let dictA = reference[i]
                            self.arrRefrence.add(dictA)
                        }
                        self.tblList.reloadData()
                    }
                }
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func setupLabelTap() {
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
        self.lblDetail.isUserInteractionEnabled = true
        self.lblDetail.addGestureRecognizer(labelTap)
    }
    
    @objc func labelTapped(_ sender: UITapGestureRecognizer) {
        
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "email_sendVC") as! email_sendVC
        dest.account_no = self.acctNo
        dest.email = self.lblDetail.text!
        dest.isFromReceipt = false
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true, completion: nil)
        
    }
    
    @objc func labelTappedText(_ sender: UITapGestureRecognizer) {
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "text_sendVC") as! text_sendVC
        dest.account_no = self.acctNo
        dest.phoneNo = self.lblDetail.text!
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true, completion: nil)
    }
}

class tblEmailUserCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDetail : UILabel!
}


extension sendViewController : UITableViewDataSource, UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrRefrence.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblList.dequeueReusableCell(withIdentifier: "tblEmailUserCell") as! tblEmailUserCell
        let dict = self.arrRefrence[indexPath.section] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let borrower_id = dict["borrower_id"] as? NSDictionary{
            Globalfunc.print(object:borrower_id)
            if let identity_info = borrower_id["identity_info"] as? NSDictionary{
                let first_name = identity_info["first_name"] as? String
                let last_name = identity_info["last_name"] as? String
                
                let fLeter = first_name?.trimmingCharacters(in: .whitespaces).first
                let lLeter = last_name?.trimmingCharacters(in: .whitespaces).first
                self.lblTitle.text = "\(fLeter!)\(lLeter!)"
                
                let relationship = dict["relationship"] as! NSDictionary
                let title = relationship["title"] as? String
                
                cell.lblName.text = "\(first_name!) \(last_name!) [\(title!.uppercased())]"
            }
            
            if let contact_info = borrower_id["contact_info"] as? NSDictionary{
                
                if(self.isSelectForm == "text"){
                    if let cellPhone = contact_info["cellPhone"] as? String{
                        cell.lblDetail.text = "Cell Phone: \(cellPhone)"
                    }
                    else{
                        cell.lblDetail.text = "Cell Phone: (832)893-5153"
                    }
                    let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTappedText(_:)))
                    cell.lblDetail.isUserInteractionEnabled = true
                    cell.lblDetail.addGestureRecognizer(labelTap)
                    
                }
                else if(self.isSelectForm == "email"){
                    if let email = contact_info["email"] as? String{
                        cell.lblDetail.text = "Email: \(email)"
                        if(email != ""){
                            let labelTap = UITapGestureRecognizer(target: self, action: #selector(self.labelTapped(_:)))
                            cell.lblDetail.isUserInteractionEnabled = true
                            cell.lblDetail.addGestureRecognizer(labelTap)
                        }
                        else{
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}

extension StringProtocol {
    var firstUppercased: String { return prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { return prefix(1).capitalized + dropFirst() }
}
