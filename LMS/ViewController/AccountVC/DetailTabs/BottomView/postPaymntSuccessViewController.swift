//
//  postPaymntSuccessViewController.swift
//  LMS
//
//  Created by Apple on 02/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class postPaymntSuccessViewController: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    var actNo = ""
    var email_str = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.actNo = acct_no
                self.lblAccountNo.text = "Account no: \(acct_no)"
            }
        }
        catch{
            
        }
        do {
            let decoded  = userDef.object(forKey: "action_dict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                Globalfunc.print(object:responseDict)
                OperationQueue.main.addOperation {
                    if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                        if let email = loanAccount["email"] as? String{
                            if(email != ""){
                                self.email_str = email
                            }
                        }
                    }
                }
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBaclBtn(_ sender: UIButton)
    {
        self.view.window!.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
}

extension postPaymntSuccessViewController {
    
    @IBAction func clickONReceiptBtn(_ sender: UIButton)
    {
        isReceiptTyp = "payment"
        isEmailAttach = "false"
        self.presentViewControllerasPopup("receiptViewController", strStoryName: "colletralStory")
    }
    
    @IBAction func clickONSendEmail(_ sender: UIButton)
    {
        userDef.removeObject(forKey: "attach_receipt")
        isReceiptTyp = "payment"
        isEmailAttach = "true"
        
        let story = UIStoryboard.init(name: "colletralStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "receiptViewController") as! receiptViewController
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true,completion: nil)
        
    }
    
    @IBAction func clickONPostPayBtn(_ sender: UIButton)
    {
        self.presentViewControllerBasedOnIdentifier("postPaymentViewController", strStoryName: "ActionStory")
    }
    
}
