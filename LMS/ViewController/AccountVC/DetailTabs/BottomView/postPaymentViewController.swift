//
//  postPaymentViewController.swift
//  LMS
//
//  Created by Apple on 14/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class postPaymentViewController: UIViewController {
    
    @IBOutlet weak var mainView : UIView!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    @IBOutlet weak var lblDue : UILabel!
    @IBOutlet weak var lblDueDate : UILabel!
    @IBOutlet weak var lblPostDate : UILabel!
    @IBOutlet weak var txtBalType : UITextField!
    @IBOutlet weak var txtPaymntTyp : UITextField!
    
    
    @IBOutlet weak var txtActionTyp : UITextField!
    @IBOutlet weak var txtDelivryMthd : UITextField!
    @IBOutlet weak var txtPay_form : UITextField!
    @IBOutlet weak var txtEffectiveDate : UITextField!
    @IBOutlet weak var txtAmtPaying : UITextField!
    @IBOutlet weak var txtAmtReceiving : UITextField!
    @IBOutlet weak var txtChngDue : UITextField!
    @IBOutlet weak var txtRefNo : UITextField!
    @IBOutlet weak var txtCommnts : UITextField!
    
    
    @IBOutlet weak var txteps : UITextField!
    @IBOutlet weak var viewEps : UIView!
    @IBOutlet weak var hgtEpsView : NSLayoutConstraint!
    
    
    @IBOutlet weak var tblSchedule : UITableView!
    
    @IBOutlet weak var collOtherBal : UICollectionView!
    @IBOutlet weak var collTotalBal : UICollectionView!
    @IBOutlet weak var hgtCollOther : NSLayoutConstraint!
    
    
    @IBOutlet weak var btnPost : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    
    @IBOutlet weak var chargeOffView : UIView!
    @IBOutlet weak var Charg_lblDue : UILabel!
    @IBOutlet weak var Charg_lblDueDate : UILabel!
    @IBOutlet weak var Charg_collTotalBal : UICollectionView!
    
    @IBOutlet weak var viewAlertMsg : UIView!
    @IBOutlet weak var lblMsg : UILabel!
    @IBOutlet weak var hgtAlertview : NSLayoutConstraint!
    
    
    
    var arrBalanceTyp = NSMutableArray()
    var arrPaymntTyp = NSMutableArray()
    
    var arrPaymntSchedule = NSMutableArray()
    
    var arrEpsAct = NSMutableArray()
    
    var pickerBalType : UIPickerView!
    var pickerPAymentType: UIPickerView!
    
    var pickerActionTyp : UIPickerView!
    var pickerDeliveryMthd: UIPickerView!
    var pickerEffDate : UIDatePicker!
    var pickerPayForm: UIPickerView!
    
    var pickerEpsAct : UIPickerView!
    
    var arrActionTyp : NSMutableArray = []
    var arrPaymnetForm = NSMutableArray()
    
    var arrTotalBal : NSMutableArray = []
    var arrOtherBal : NSMutableArray = []
    
    var calculatedArry : NSMutableArray = []
    
    
    var balTypId = ""
    var paymntTypId = ""
    var deliveryMthdId : Int!
    var payFormId : Int!
    var actionTypId = ""
    var epsId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI(){
        
        self.hgtCollOther.constant = 50
        self.collOtherBal.isHidden = true
        
        self.mainView.isHidden = true
        Globalfunc.showLoaderView(view: self.view)
        
        self.btnPost.isEnabled = false
        self.btnPost.alpha = 0.5
        
        self.btnCancel.isEnabled = false
        self.btnCancel.alpha = 0.5
        
        self.hgtEpsView.constant = 0
        self.viewEps.isHidden = true
        
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                
                
                Globalfunc.print(object:dataD)
                
                let account_status = dataD["account_status"] as! String
                if(account_status == "Charge Off"){
                    self.chargeOffView.isHidden = false
                    self.Charg_lblDue.text = "$ 0.00"
                    
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM/dd/yyyy"
                    let result = formatter.string(from: date)
                    self.Charg_lblDueDate.text = result
                }
                else{
                    self.chargeOffView.isHidden = true
                }
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        let dict1 =  ["description" :"All Balances","_id": "0"]
        arrBalanceTyp.add(dict1)
        
        
        let dict3 =  ["title" :"All Payments","_id": "0"]
        self.arrPaymntTyp.add(dict3)
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let result = formatter.string(from: date)
        self.lblPostDate.text = result
        
        self.txtEffectiveDate.text = result
        
        let dict_act1 =  ["title" :"Take a Payment","_id": "1"]
        let dict_act2 =  ["title" :"Waive Late Fees Only","_id": "2"]
        
        self.arrActionTyp.add(dict_act1)
        self.arrActionTyp.add(dict_act2)
        
        let dict_1 = arrPaymntTyp[0] as! NSDictionary
        self.txtPaymntTyp.text = dict_1["title"] as? String
        self.paymntTypId = dict_1["_id"] as! String
        
        let dict_2 = arrActionTyp[0] as! NSDictionary
        self.txtActionTyp.text = dict_2["title"] as? String
        self.actionTypId = dict_2["_id"] as! String
        
        let total_dict = ["name":"Total Appld Late Fee","value":"$0.00"]
        let total_dict1 = ["name":"Total Appld Pmt","value":"$0.00"]
        let total_dict2 = ["name":"Total Appld","value":"$0.00"]
        let total_dict3 = ["name":"Remaining Amount","value":"$0.00"]
        
        arrTotalBal.add(total_dict)
        arrTotalBal.add(total_dict1)
        arrTotalBal.add(total_dict2)
        arrTotalBal.add(total_dict3)
        
        let other_dict = ["name":"Description","value":"PL Primary Loan"]
        let other_dict1 = ["name":"Prin Bal","value":"$0.00"]
        let other_dict2 = ["name":"Prin Amt","value":"$0.00"]
        let other_dict3 = ["name":"Tot Appld","value":"$0.00"]
        let other_dict4 = ["name":"Bal Remain","value":"$0.00"]
        
        arrOtherBal.add(other_dict)
        arrOtherBal.add(other_dict1)
        arrOtherBal.add(other_dict2)
        arrOtherBal.add(other_dict3)
        arrOtherBal.add(other_dict4)
        
        self.callGetApi(strUrl: Constant.payment_type_url, strCheck: "PaymentType")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.callGetApi(strUrl: Constant.delivery_method_url, strCheck: "delivery")
            
        })
    }
    
    func callSetDataOnView(pay_id : String){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_id = dataD["_id"] as! Int
                let dict = arrBalanceTyp[0] as! NSDictionary
                let bal_id = dict["_id"] as! String
                let strUrl = "\(Constant.payment_url)?id=\(account_id)&balance_type=\(bal_id)&payment_type=\(pay_id)"
                Globalfunc.print(object:strUrl)
                self.callGetApi(strUrl: strUrl, strCheck: "all")
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    let url = "\(Constant.eps_account)/accid?account_id=\(account_id)"
                    self.callGetApi(strUrl: url, strCheck: "eps")
                })
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONBaclBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

/*MARk - Call APi*/
extension postPaymentViewController {
    
    func callGetApi(strUrl: String, strCheck: String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(strCheck == "PaymentType"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        
                        Globalfunc.hideLoaderView(view: self.view)
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrPaymntTyp.add(dictA)
                                    }
                                    let dict = self.arrPaymntTyp[0] as! NSDictionary
                                    let strTitle = dict["title"] as! String
                                    self.txtPaymntTyp.text = strTitle
                                    let pay_id = dict["_id"] as! String
                                    self.callSetDataOnView(pay_id: pay_id)
                                }
                                else{
                                }
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            if let msg = arr["message"] as? String
                            {
                                if(msg == "Your token has expired."){
                                    
                                }
                                else{
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            else if(strCheck == "all"){
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        
                        if let response = dict as? NSDictionary {
                            Globalfunc.print(object:response)
                            
                            if let dictResponse = response["data"] as? NSDictionary{
                                if let alertMessage = dictResponse["alertMessage"] as? NSDictionary{
                                    
                                    self.viewAlertMsg.isHidden = false
                                    self.hgtAlertview.constant = 60
                                    
                                    let message = alertMessage["message"] as! String
                                    self.lblMsg.text = message
                                    
                                    let default_style = alertMessage["default_style"] as! Bool
                                    if(default_style == true){
                                        self.viewAlertMsg.backgroundColor = self.UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                                        self.lblMsg.textColor = .white
                                    }
                                    else{
                                        
                                        let back_color = alertMessage["back_color"] as! String
                                        let font_color = alertMessage["font_color"] as! String
                                        
                                        let replaced = back_color.replacingOccurrences(of: "#", with: "0x")
                                        let result = UInt32(String(replaced.dropFirst(2)), radix: 16)
                                        self.viewAlertMsg.backgroundColor = self.UIColorFromHex(rgbValue: result!, alpha: 1.0)
                                        
                                        let fReplace = font_color.replacingOccurrences(of: "#", with: "0x")
                                        let result_1 = UInt32(String(fReplace.dropFirst(2)), radix: 16)
                                        self.lblMsg.textColor = self.UIColorFromHex(rgbValue: result_1!, alpha: 1.0)
                                        
                                    }
                                }
                                else{
                                    
                                    self.viewAlertMsg.isHidden = true
                                    self.hgtAlertview.constant = 0
                                    
                                }
                                
                                
                                
                                
                                if let balanceType = dictResponse["balanceType"] as? [NSDictionary]{
                                    
                                    if(balanceType.count > 0){
                                        for i in 0..<balanceType.count{
                                            let dict = balanceType[i]
                                            self.arrBalanceTyp.add(dict)
                                        }
                                        let dict = self.arrBalanceTyp[0] as! NSDictionary
                                        let strTitle = dict["description"] as! String
                                        self.txtBalType.text = strTitle
                                        
                                        if let _id = dict["_id"] as? String{
                                            self.balTypId = _id
                                        }
                                    }
                                }
                                
                                if let due = dictResponse["due"] as? String{
                                    if(due == "0"){
                                        self.lblDue.text = "Next Due"
                                    }
                                    else if(due == "1"){
                                        self.lblDue.text = "Due Now"
                                    }
                                }
                                
                                if let due = dictResponse["due"] as? Int{
                                    if(due == 0){
                                        self.lblDue.text = "Next Due"
                                    }
                                    else if(due == 1){
                                        self.lblDue.text = "Due Now"
                                    }
                                }
                                
                                let nextDue = dictResponse["nextDue"] as! NSNumber
                                self.lblDueDate.text = "\(nextDue)"
                                
                                let paymentSchedule = dictResponse["paymentSchedule"] as! [NSDictionary]
                                
                                if(paymentSchedule.count > 0){
                                    for i in 0..<paymentSchedule.count{
                                        let dictA = paymentSchedule[i]
                                        self.arrPaymntSchedule.add(dictA)
                                    }
                                    //reload table
                                    self.tblSchedule.reloadData()
                                    self.mainView.isHidden = false
                                }
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            if let msg = arr["message"] as? String
                            {
                                if(msg == "Your token has expired."){
                                    self.viewWillAppear(true)
                                }
                                else{
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                            }
                        }
                    }
                }
            }
            else if(strCheck == "delivery")
            {
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        Constant.arrDElivryMethod.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(strCheck == "paymentForm")
            {
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if let responseDict = dict as? NSDictionary {
                            self.arrPaymnetForm = []
                            self.txtPay_form.text = ""
                            if let arr = responseDict["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrPaymnetForm.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if(strCheck == "filter"){
                self.arrPaymntSchedule = []
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let response = dict as? NSDictionary {
                            Globalfunc.print(object:response)
                            if let dictResponse = response["data"] as? NSDictionary{
                                let nextDue = dictResponse["nextDue"] as! NSNumber
                                self.lblDueDate.text = "\(nextDue)"
                                
                                
                                let paymentSchedule = dictResponse["paymentSchedule"] as! [NSDictionary]
                                
                                if(paymentSchedule.count > 0){
                                    for i in 0..<paymentSchedule.count{
                                        let dictA = paymentSchedule[i]
                                        self.arrPaymntSchedule.add(dictA)
                                    }
                                    //reload table
                                    self.tblSchedule.reloadData()
                                    self.mainView.isHidden = false
                                }
                                
                                
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            if let msg = arr["message"] as? String
                            {
                                if(msg == "Your token has expired."){
                                    self.viewWillAppear(true)
                                }
                                else{
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                                }
                            }
                        }
                    }
                }
            }
            else if(strCheck == "eps"){
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let response = dict as? NSDictionary {
                            Globalfunc.print(object:response)
                            if let arr = response["data"] as? [NSDictionary]{
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrEpsAct.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

extension postPaymentViewController {
    
    func sendCalculatedAmtByPost()
    {
        
        
        for i in 0..<self.calculatedArry.count
        {
            let dict = self.calculatedArry[i] as! NSMutableDictionary
            
            dict.removeObject(forKey: "autoPay")
            dict.removeObject(forKey: "daysPastDue")
            dict.removeObject(forKey: "desc")
            dict.removeObject(forKey: "is_deleted")
            dict.removeObject(forKey: "account_id")
            dict.removeObject(forKey: "__v")
            
            dict.removeObject(forKey: "loanType")
            dict.removeObject(forKey: "paymentSubType")
            dict.removeObject(forKey: "postDate")
            dict.removeObject(forKey: "transaction")
            dict.removeObject(forKey: "reasonCode")
            
            let payTypDict = dict["paymentType"] as! NSDictionary
            let payId = payTypDict["_id"] as! Int
            dict.setValue(payId, forKey: "paymentType")
            
            let _id = dict["_id"] as! Int
            dict.setValue(_id, forKey: "id")
            
            dict.removeObject(forKey: "_id")
            
            let balanceType = dict["balanceType"] as! NSDictionary
            dict.setValue(balanceType, forKey: "balanceType")
            
            self.calculatedArry.replaceObject(at: i, with: dict)
        }
        
        print(self.calculatedArry)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                
                let acct_id = dataD["_id"] as! Int
                let userid = dataD["userid"] as! Int
                let insID = dataD["insid"] as! Int
                
                let amtPay = Double(self.txtAmtPaying.text!)
                let amtReceive = Double(self.txtAmtReceiving.text!)
                
                
                let parsed = self.txtChngDue.text!.replacingOccurrences(of: "$", with: "")
                let chngDue = Double(parsed)
                let dueAmt = Double(self.lblDueDate.text!)
                
                var params = [String : Any]()
                
                if(self.payFormId == 17){
                    params = [ "account_id": acct_id,
                               "actionType": "\(self.actionTypId)",
                        "amountPaying": amtPay!,
                        "amountReceived": amtReceive!,
                        "balanceType": "\(self.balTypId)",
                        "changeDue": chngDue!,
                        "comments": "\(self.txtCommnts.text!)",
                        "deliveryMethod": "\(self.deliveryMthdId!)",
                        "dueAmount": dueAmt!,
                        "effectiveDate": "\(self.txtEffectiveDate.text!)" ,
                        "insid": insID,
                        "paymentFrom": "\(self.payFormId!)",
                        "eps_account": self.epsId!,
                        "paymentSchedule": self.calculatedArry,
                        "paymentType": "\(self.paymntTypId)",
                        "postDate": "\(self.lblPostDate.text!)",
                        "refrenceNumber": "\(self.txtRefNo.text!)",
                        "userid": userid] as [String : Any]
                }
                else{
                    params = [ "account_id": acct_id,
                               "actionType": "\(self.actionTypId)",
                        "amountPaying": amtPay!,
                        "amountReceived": amtReceive!,
                        "balanceType": "\(self.balTypId)",
                        "changeDue": chngDue!,
                        "comments": "\(self.txtCommnts.text!)",
                        "deliveryMethod": "\(self.deliveryMthdId!)",
                        "dueAmount": dueAmt!,
                        "effectiveDate": "\(self.txtEffectiveDate.text!)" ,
                        "insid": insID,
                        "paymentFrom": "\(self.payFormId!)",
                        "paymentSchedule": self.calculatedArry,
                        "paymentType": "\(self.paymntTypId)",
                        "postDate": "\(self.lblPostDate.text!)",
                        "refrenceNumber": "\(self.txtRefNo.text!)",
                        "userid": userid] as [String : Any]
                }
                Globalfunc.print(object:params)
                BaseApi.onResponsePutWithToken(url: Constant.post_payment, controller: self, parms: params as NSDictionary) { (dict, error) in
                    OperationQueue.main.addOperation {
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.presentViewControllerBasedOnIdentifier("postPaymntSuccessViewController", strStoryName: "colletralStory")
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
    @IBAction func clickOnPostBtn(_ sender : UIButton)
    {
        self.sendCalculatedAmtByPost()
    }
    
    @IBAction func clickOnEpsBtn(_ sender : UIButton)
    {
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "epsPPViewController") as! epsPPViewController
        vc.modalPresentationStyle = .fullScreen
        vc.deliverId = self.deliveryMthdId
        self.present(vc, animated: true, completion: nil)
    }
}

/*MARk - Textfeld and picker delegates*/

extension postPaymentViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerBalType{
            return self.arrBalanceTyp.count
        }
        else if(pickerView == pickerPAymentType){
            return self.arrPaymntTyp.count
        }
        else if(pickerView == pickerActionTyp){
            return self.arrActionTyp.count
        }
        else if(pickerView == pickerDeliveryMthd){
            return Constant.arrDElivryMethod.count
        }
        else if(pickerView == pickerPayForm){
            return self.arrPaymnetForm.count
        }
        else if(pickerView == pickerEpsAct){
            return self.arrEpsAct.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerPAymentType){
            let dict = arrPaymntTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerActionTyp){
            let dict = arrActionTyp[row] as! NSDictionary
            let strTitle =  dict["title"] as? String
            return strTitle
        }
            
        else if(pickerView == pickerDeliveryMthd){
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerPayForm){
            if(arrPaymnetForm.count > 0){
                let dict = arrPaymnetForm[row] as! NSDictionary
                let strTitle = dict["title"] as! String
                return strTitle
            }
            return "Select Delivery Method"
        }
        else if(pickerView == pickerEpsAct){
            
            let dict = arrEpsAct[row] as! NSDictionary
            let epsFirstName = dict["epsFirstName"] as! String
            let epsLastName = dict["epsLastName"] as! String
            let epsAccountNumber = dict["epsAccountNumber"] as! String
            let strTitle = "\(epsFirstName),\(String((epsLastName.first)!))-[\(epsAccountNumber)]"
            return strTitle
            
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            self.txtBalType.text = strTitle
            if let _id = dict["_id"] as? String{
                self.balTypId = _id
            }
            if let _id = dict["_id"] as? Int{
                self.balTypId = "\(_id)"
            }
            
            txtBalType.resignFirstResponder()
            self.callFilterApi()
            
        }
            
        else if(pickerView == pickerPAymentType){
            let dict = arrPaymntTyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            
            if let _id = dict["_id"] as? String{
                self.paymntTypId = _id
            }
            
            if let _id = dict["_id"] as? Int{
                self.paymntTypId = "\(_id)"
            }
            
            self.txtPaymntTyp.text = strTitle
            
            txtPaymntTyp.resignFirstResponder()
            self.callFilterApi()
        }
            
        else if(pickerView == pickerActionTyp){
            
            let dict = arrActionTyp[row] as! NSDictionary
            self.txtActionTyp.text = dict["title"] as? String
            self.actionTypId = dict["_id"] as! String
            
            txtActionTyp.resignFirstResponder()
            self.callFilterApi()
        }
            
            
        else if(pickerView == pickerDeliveryMthd){
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let _id = dict["_id"] as! Int
            
            self.deliveryMthdId = _id
            
            let strTitle = dict["title"] as! String
            self.txtDelivryMthd.text = strTitle
            
            self.callGetApi(strUrl: "\(Constant.pay_form_url)?id=\(_id)", strCheck: "paymentForm")
            
        }
            
        else if(pickerView == pickerPayForm){
            let dict = arrPaymnetForm[row] as! NSDictionary
            
            
            let _id = dict["_id"] as! Int
            self.payFormId = _id
            
            
            let strTitle = dict["title"] as! String
            self.txtPay_form.text = strTitle
            
            if(_id == 17){
                self.hgtEpsView.constant = 120
                self.viewEps.isHidden = false
            }
            else{
                self.hgtEpsView.constant = 0
                self.viewEps.isHidden = true
            }
        }
            
        else if(pickerView == pickerEpsAct){
            
            let dict = arrEpsAct[row] as! NSDictionary
            let epsFirstName = dict["epsFirstName"] as! String
            let epsLastName = dict["epsLastName"] as! String
            let epsAccountNumber = dict["epsAccountNumber"] as! String
            let strTitle = "\(epsFirstName),\(String((epsLastName.first)!))-[\(epsAccountNumber)]"
            
            let _id = dict["_id"] as! Int
            self.epsId = _id
            
            self.txteps.text = strTitle
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtBalType){
            self.pickUp(txtBalType)
        }
            
        else if(textField == self.txtPaymntTyp){
            self.pickUp(txtPaymntTyp)
        }
            
        else if(textField == self.txtActionTyp){
            self.pickUp(txtActionTyp)
        }
            
        else if(textField == self.txtDelivryMthd){
            self.pickUp(txtDelivryMthd)
        }
            
        else if(textField == self.txtPay_form){
            self.pickUp(txtPay_form)
        }
            
        else if(textField == self.txteps){
            self.pickUp(txteps)
        }
            
        else if(textField == self.txtEffectiveDate){
            self.pickUpDateTime(txtEffectiveDate)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBalType){
            
            self.pickerBalType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBalType.delegate = self
            self.pickerBalType.dataSource = self
            self.pickerBalType.backgroundColor = UIColor.white
            textField.inputView = self.pickerBalType
        }
            
        else if(textField == self.txtPaymntTyp){
            
            self.pickerPAymentType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPAymentType.delegate = self
            self.pickerPAymentType.dataSource = self
            self.pickerPAymentType.backgroundColor = UIColor.white
            textField.inputView = self.pickerPAymentType
        }
            
        else if(textField == self.txtActionTyp){
            
            self.pickerActionTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerActionTyp.delegate = self
            self.pickerActionTyp.dataSource = self
            self.pickerActionTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerActionTyp
        }
            
        else if(textField == self.txtDelivryMthd){
            
            self.pickerDeliveryMthd = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDeliveryMthd.delegate = self
            self.pickerDeliveryMthd.dataSource = self
            self.pickerDeliveryMthd.backgroundColor = UIColor.white
            textField.inputView = self.pickerDeliveryMthd
        }
            
        else if(textField == self.txtPay_form){
            
            self.pickerPayForm = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPayForm.delegate = self
            self.pickerPayForm.dataSource = self
            self.pickerPayForm.backgroundColor = UIColor.white
            textField.inputView = self.pickerPayForm
        }
            
        else if(textField == self.txteps){
            
            self.pickerEpsAct = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEpsAct.delegate = self
            self.pickerEpsAct.dataSource = self
            self.pickerEpsAct.backgroundColor = UIColor.white
            textField.inputView = self.pickerEpsAct
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtBalType.resignFirstResponder()
        txtPaymntTyp.resignFirstResponder()
        txtActionTyp.resignFirstResponder()
        txtDelivryMthd.resignFirstResponder()
        txtPay_form.resignFirstResponder()
        txteps.resignFirstResponder()
        
    }
    
    @objc func cancelClick() {
        
        txtBalType.resignFirstResponder()
        txtPaymntTyp.resignFirstResponder()
        txtActionTyp.resignFirstResponder()
        txtDelivryMthd.resignFirstResponder()
        txtPay_form.resignFirstResponder()
        txteps.resignFirstResponder()
        
    }
    
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtEffectiveDate){
            self.pickerEffDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEffDate.datePickerMode = .date
            self.pickerEffDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerEffDate
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtEffectiveDate.text = formatter.string(from: pickerEffDate.date)
        self.txtEffectiveDate.resignFirstResponder()
    }
    
    @objc func cancelClickDate() {
        txtEffectiveDate.resignFirstResponder()
    }
    
    func callFilterApi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let account_id = dataD["_id"] as! Int
                let strUrl = "\(Constant.payment_url)?id=\(account_id)&balance_type=\(self.balTypId)&payment_type=\(self.paymntTypId)&action_type=\(self.actionTypId)"
                Globalfunc.print(object:strUrl)
                self.callGetApi(strUrl: strUrl, strCheck: "filter")
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

/*-----MArk Table Schedule**/
extension postPaymentViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrPaymntSchedule.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblSchedule.dequeueReusableCell(withIdentifier: "tblScheduleCell") as! tblScheduleCell
        
        let dict = self.arrPaymntSchedule[indexPath.section] as! NSDictionary
        Globalfunc.print(object:dict)
        if let dueDate = dict["dueDate"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            let result = formatter.string(from: date!)
            cell.lblDueDate.text = result
        }
        
        let title = dict["desc"] as! String
        cell.lblPay_type.text = title
        
        if let regularDue = dict["regularDue"] as? NSNumber{
            cell.lblAmtDue.text = "$ \(regularDue)"
        }
        
        if let lateFee = dict["lateFee"] as? Int{
            cell.lblS_latefeeDue.text = "$ \(lateFee)"
        }
        
        if let waiveLateFee = dict["waiveLateFee"] as? Bool{
            if(waiveLateFee == false){
                cell.btnCheck.isSelected = false
            }
            else{
                cell.btnCheck.isSelected = true
            }
        }
        
        //total ayega
        if(cell.btnCheck.isSelected == false)
        {
            let regularDue = dict["regularDue"] as! NSNumber
            let waiveLateFee = dict["waiveLateFee"] as! NSNumber
            
            let totalSchd = Double(truncating: regularDue) + Double(truncating: waiveLateFee)
            cell.lblS_totalDue.text = "$ \(totalSchd)"
        }
        else{
            
            let regularDue = dict["regularDue"] as! NSNumber
            let waiveLateFee = dict["waiveLateFee"] as! NSNumber
            
            let totalSchd = Double(truncating: regularDue) - Double(truncating: waiveLateFee)
            cell.lblS_totalDue.text = "$ \(totalSchd)"
        }
        
        let appliedLateFee = dict["appliedLateFee"] as! NSNumber
        cell.txtS_latefeeApplied.text = "\(appliedLateFee)"
        
        
        let appliedPrincipal = dict["appliedPrincipal"] as! NSNumber
        cell.txtS_payApplid.text = "\(appliedPrincipal)"
        
        let sTotalApplied = Double(truncating: appliedPrincipal) + Double(truncating: appliedLateFee)
        cell.lblS_totalApplid.text = "$ \(sTotalApplied)"
        
        if let stillDue = dict["stillDue"] as? NSNumber{
            cell.lblS_remainDue.text = "$ \(stillDue)"
            let sDue = Int(truncating: stillDue)
            if(sDue == 0){
                cell.viewS_remainDue.backgroundColor = UIColor.systemGreen
            }else{
                cell.viewS_remainDue.backgroundColor = UIColorFromHex(rgbValue: 0xEBEBEB, alpha: 1.0)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 680
    }
    
    func performCalculation()
    {
        let amtPay = Double(self.txtAmtPaying.text!)
        var value = amtPay
        
        self.txtAmtReceiving.text = self.txtAmtPaying.text
        
        for i in 0..<arrPaymntSchedule.count{
            let dict = self.arrPaymntSchedule[i] as! NSMutableDictionary
            Globalfunc.print(object:dict)
            
            if let stillDue = dict["stillDue"] as? NSNumber{
                let regularDue = dict["regularDue"] as! NSNumber
                let rDue = Double(truncating: regularDue)
                let calcAmt = Double(truncating: stillDue)
                if(rDue <= value!){
                    value = value! - calcAmt
                    dict.setValue(0.0, forKey: "stillDue")
                    dict.setValue(NSNumber(value: calcAmt), forKey: "appliedPrincipal")
                    self.arrPaymntSchedule.replaceObject(at: i, with: dict)
                    self.calculatedArry.insert(dict, at:i)
                }
                else if(rDue > value!){
                    let stilldue = calcAmt - value!
                    dict.setValue(NSNumber(value: stilldue), forKey: "stillDue")
                    dict.setValue(NSNumber(value: value!), forKey: "appliedPrincipal")
                    self.arrPaymntSchedule.replaceObject(at: i, with: dict)
                    self.calculatedArry.insert(dict, at:i)
                    break
                }
                else if(value! > 0.0){
                    self.arrPaymntSchedule.replaceObject(at: i, with: dict)
                    self.calculatedArry.insert(dict, at:i)
                    break
                }
            }
        }
        Globalfunc.print(object:self.calculatedArry)
        self.tblSchedule.reloadData()
    }
    
    @IBAction func clickONPay(_ sender : UIButton)
    {
        if(txtAmtPaying.text! != ""){
            self.performCalculation()
        }
        self.btnPost.isEnabled = true
        self.btnPost.alpha = 1.0
        self.btnCancel.isEnabled = true
        self.btnCancel.alpha = 1.0
    }
}

class tblScheduleCell: UITableViewCell {
    
    @IBOutlet weak var lblDueDate : UILabel!
    @IBOutlet weak var lblPay_type : UILabel!
    @IBOutlet weak var lblAmtDue : UILabel!
    @IBOutlet weak var lblS_latefeeDue : UILabel!
    @IBOutlet weak var lblS_totalDue : UILabel!
    @IBOutlet weak var txtS_latefeeApplied : UITextField!
    @IBOutlet weak var txtS_payApplid : UITextField!
    @IBOutlet weak var lblS_totalApplid : UILabel!
    @IBOutlet weak var lblS_remainDue : UILabel!
    @IBOutlet weak var btnCheck : UIButton!
    @IBOutlet weak var viewS_remainDue : UIView!
    
}

//total view
extension postPaymentViewController : UICollectionViewDelegate , UICollectionViewDataSource{
    
    @IBAction func clikcONShowBal(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            self.hgtCollOther.constant = 50
            self.collOtherBal.isHidden = true
            
        }else {
            sender.isSelected = true
            self.hgtCollOther.constant = 150
            self.collOtherBal.isHidden = false
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == self.collOtherBal)
        {
            return arrOtherBal.count
        }
        else if (collectionView == self.collTotalBal || collectionView == self.Charg_collTotalBal)
        {
            return arrTotalBal.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = UICollectionViewCell()
        
        if (collectionView == self.collOtherBal)
        {
            let cell = collOtherBal.dequeueReusableCell(withReuseIdentifier: "collOtherBal", for: indexPath) as! collOtherBal
            cell.contentView.layer.borderWidth = 1
            cell.contentView.layer.borderColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0).cgColor
            
            let dict = self.arrOtherBal[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["name"] as! String)
            cell.lblName.text = (dict["value"] as! String)
            return cell
        }
        else if (collectionView == self.collTotalBal || collectionView == self.Charg_collTotalBal)
        {
            let cell = collTotalBal.dequeueReusableCell(withReuseIdentifier: "collTotalBal", for: indexPath) as! collTotalBal
            cell.contentView.layer.borderWidth = 1
            cell.contentView.layer.borderColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0).cgColor
            let dict = self.arrTotalBal[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["name"] as! String)
            cell.lblName.text = (dict["value"] as! String)
            return cell
        }
        return cell
    }
}

class collOtherBal : UICollectionViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblName : UILabel!
}

class collTotalBal : UICollectionViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblName : UILabel!
}
