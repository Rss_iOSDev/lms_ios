//
//  attachEcabViewController.swift
//  LMS
//
//  Created by Apple on 19/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class attachEcabViewController: UIViewController {

    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tblEcabinet: UITableView!
    
    var delegate: AttchmntDelegate?
    
    var arrEcabinetList : NSMutableArray = []
    var attachDocArrList : NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.isHidden = true
         self.lblStatus.text = ""
        self.tblEcabinet.isHidden = true
        
        self.tblEcabinet.tableFooterView = UIView()
        self.call_getEcabinetHistoryApi()
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//Api --
extension attachEcabViewController {
    
    func call_getEcabinetHistoryApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.ecabinet_doc_url)?account_id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            
                            if let response = dict as? NSDictionary{
                                self.parseDatafromApi(response: response)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func parseDatafromApi(response : NSDictionary)
    {
        if let data = response["data"] as? NSDictionary {
            if let arr = data["document"] as? [NSDictionary] {
                if(arr.count > 0){
                    for i in 0..<arr.count{
                        let dictA = arr[i]
                        self.arrEcabinetList.add(dictA)
                    }
                    self.tblEcabinet.isHidden = false
                    self.lblStatus.isHidden = true
                    self.lblStatus.text = ""
                    self.tblEcabinet.reloadData()
                }
                else{
                    self.tblEcabinet.isHidden = true
                    self.lblStatus.isHidden = false
                    self.lblStatus.text = "No Ecabinet Files Have Been Found For This Search."
                }
            }
        }

    }
    
    func callEcabinetListApiwithstatus(param: NSDictionary){
        BaseApi.onResponsePostWithToken(url: Constant.ecabinet_doc_url, controller: self, parms: param) { (dict, err) in
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                if let response = dict as? NSDictionary {
                    self.parseDatafromApi(response: response)
                }
            }
        }
    }
}
extension attachEcabViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEcabinetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblEcabinet.dequeueReusableCell(withIdentifier: "tblEcabinetCell") as! tblEcabinetCell
        let dict = arrEcabinetList[indexPath.row] as! NSDictionary
        
        if let account_no = dict["account_no"] as? Int
        {
            cell.lblAccID.text = "\(account_no)"
        }
        if let created_at = dict["created_at"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: created_at)
            cell.lblDate.text = formatter.string(from: date!)
        }
        if let fileSize = dict["fileSize"] as? String
        {
            cell.lblFileSize.text = fileSize
        }
        
        if let owner = dict["owner"] as? String
        {
            cell.lblOwner.text = owner
        }
        if let customer = dict["customer"] as? String
        {
            cell.lblCustomer.text = customer
        }
        if let included_forms = dict["included_forms"] as? String
        {
            cell.lblIncludedForm.text = included_forms
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(clickOnViewBtn(_:)), for: .touchUpInside)
        
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(clickOnSelectDocBtn(_:)), for: .touchUpInside)

        
        return cell
    }
    
     @objc func clickOnSelectDocBtn(_ sender: UIButton)
     {
        let dict = arrEcabinetList[sender.tag] as! NSDictionary
        print(dict)
        
        let account_no = dict["account_no"] as! Int
        let account_id = dict["account_id"] as! Int
        let filePath = dict["filePath"] as! String
        let strUrl = "http://18.191.178.120:3000/ecabinet/\(account_id)/\(filePath).pdf"
        
        let newDict : NSMutableDictionary = [:]
        newDict.setValue("\(account_no)", forKey: "accountNo")
        newDict.setValue("ecab", forKey: "type")
        newDict.setValue(strUrl, forKey: "file_path")
        
        if(self.attachDocArrList.contains(newDict))
        {
            sender.isSelected = false
            self.attachDocArrList.remove(newDict)
        }
        else{
            sender.isSelected = true
            self.attachDocArrList.add(newDict)
            
            var arrAttch : [NSDictionary] = []
            arrAttch = self.attachDocArrList as! [NSDictionary]
            
            self.delegate?.sendAttchnmnt(attachmntArr: arrAttch)
        }
     }
    
    @objc func clickOnViewBtn(_ sender: UIButton)
    {
        let dict = arrEcabinetList[sender.tag] as! NSDictionary
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "pdfDocViewController") as! pdfDocViewController
        vc.modalPresentationStyle = .fullScreen
        vc.passDict = dict
        self.present(vc, animated: true, completion: nil)
    }
}
