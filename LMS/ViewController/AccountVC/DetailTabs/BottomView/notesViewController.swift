//
//  notesViewController.swift
//  LMS
//
//  Created by Apple on 03/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class notesViewController: UIViewController {
    
    @IBOutlet weak var scrollLbl : UICollectionView!
    
    
    @IBOutlet weak var tblNotes : UITableView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrLAstCount : NSMutableArray = []
    
    var arrData : NSMutableArray = []
    
    
    var pickerViewAction: UIPickerView!
    var pickerViewUser: UIPickerView!
    var pickerViewResult: UIPickerView!
    var pickerViewPromiseTypet: UIPickerView!
    
    var arrAction : [String] = []
    var arrUser : [String] = []
    var arrResult : [String] = []
    var arrPromiseType : [String] = []
    
    @IBOutlet weak var txtAction : UITextField!
    @IBOutlet weak var txtUser : UITextField!
    @IBOutlet weak var txtReslut : UITextField!
    @IBOutlet weak var txtPromiseType : UITextField!
    
    @IBOutlet weak var txtPagination: UITextField!
    @IBOutlet weak var scrollViewPages: UIScrollView!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    @IBOutlet weak var lblResultfound: UILabel!
    
    let buttonPadding:CGFloat = 10
    var xOffset:CGFloat = 10
    var arrPAginationNo = ["10","25","50","75","100","200","250"]
    var pickerViewPages: UIPickerView!
    var defaultPage = 10
    var ispicker = ""
    var btnPages = UIButton()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblNotes.tableFooterView = UIView()
        self.txtPagination.text = self.arrPAginationNo[0]
        
        
        btnPages.tag = 1
        btnPages.setTitle("1", for: .normal)
        
        self.callApi()
        
        
    }
    
    func callApi(){
        
        self.arrData = []
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account no: \(acct_no)"
                
                let acctId = dataD["_id"] as! Int
                let str_url = "\(Constant.getNotes_url)?account_id=\(acctId)&notes_type=bottom_navigation"
                self.callNotesApi(strUrl: str_url)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//call getNotes Api
extension notesViewController {
    
    func callNotesApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        self.parseDatafromApi(dict: responseDict)
                    }
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func parseDatafromApi(dict: NSDictionary){
        
        if let data = dict["data"] as? NSDictionary{
            
            let currentPageNumber = data["currentPageNumber"] as! Int
            let defaultRecordsPage = data["defaultRecordsPage"] as! Int
            
            let record = data["totalrecord"] as! Int
            self.lblResultfound.text = "\(record) RESULTS FOUND"
            
            let cpNo = CGFloat(currentPageNumber)
            let pagSize = CGFloat(defaultRecordsPage)
            let totalR = CGFloat(record)
            
            let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
            let pagecount = Int(pageNo)
            
            let buttonPadding:CGFloat = 10
            var xOffset:CGFloat = 10
            
            for i in 0..<pagecount {
                if (i == 0){
                    self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
                }
                let button = UIButton()
                button.tag = i+1
                button.setTitle("\(i+1)", for: .normal)
                button.layer.borderWidth = 1
                button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
                button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
                button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
                button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
                button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
                xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
                self.scrollViewPages.addSubview(button)
            }
            self.scrollWidth.constant = xOffset
            self.scrollViewPages.contentSize = CGSize(width: xOffset, height: self.scrollViewPages.frame.height)
            
            if let dataArr = data["notes"] as? NSMutableArray{
                self.arrData = dataArr
                
                if(dataArr.count > 0){
                    
                    let dictObject = dataArr[0] as! NSDictionary
                    
                    let created_at = dictObject["created_at"] as? String
                    let updated_at = dictObject["updated_at"] as? String
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM-dd-yyyy HH:mm a"
                    let date = Date.dateFromISOString(string: created_at!)
                    let createDate_str = formatter.string(from: date!)
                    
                    let date1 = Date.dateFromISOString(string: updated_at!)
                    let updateDate_str = formatter.string(from: date1!)
                    
                    
                    let promise = data["promise"] as! NSDictionary
                    
                    let broken = promise["broken"] as! Int
                    let kept = promise["kept"] as! Int
                    let pending = promise["pending"] as! Int
                    let total = promise["total"] as! Int
                    
                    
                    let dict1 = ["title":"Last Note","value":"\(createDate_str)"]
                    let dict2 = ["title":"Last Maintenance","value":"\(updateDate_str)"]
                    let dict3 = ["title":"Note Count","value":"0"]
                    let dict4 = ["title":"Main Count","value":"\(dataArr.count)"]
                    let dict5 = ["title":"Promise Count","value":"\(total)"]
                    let dict6 = ["title":"Promises Open","value":"\(pending)"]
                    let dict7 = ["title":"Promises Kept","value":"\(kept)"]
                    let dict8 = ["title":"Promises Broken","value":"\(broken)"]
                    
                    self.arrLAstCount.add(dict1)
                    self.arrLAstCount.add(dict2)
                    self.arrLAstCount.add(dict3)
                    self.arrLAstCount.add(dict4)
                    self.arrLAstCount.add(dict5)
                    self.arrLAstCount.add(dict6)
                    self.arrLAstCount.add(dict7)
                    self.arrLAstCount.add(dict8)
                    
                    
                    for i in 0..<dataArr.count{
                        
                        let dd = dataArr[i] as! NSDictionary
                        
                        if let action = dd["action"] as? String{
                            self.arrAction.append(action)
                        }
                        
                        if let initials = dd["initials"] as? String{
                            self.arrUser.append(initials)
                        }
                        
                        if let promiseType = dd["promiseType"] as? String{
                            self.arrPromiseType.append(promiseType)
                        }
                        
                        if let result = dd["result"] as? String{
                            self.arrResult.append(result)
                        }
                    }
                    
                    self.arrAction = self.arrAction.removingDuplicates()
                    self.arrUser = self.arrUser.removingDuplicates()
                    self.arrPromiseType = self.arrPromiseType.removingDuplicates()
                    self.arrResult = self.arrResult.removingDuplicates()
                    
                    
                }
                else{
                    
                    let dict1 = ["title":"Last Note","value":""]
                    let dict2 = ["title":"Last Maintenance","value":""]
                    let dict3 = ["title":"Note Count","value":"0"]
                    let dict4 = ["title":"Main Count","value":"0"]
                    
                    self.arrLAstCount.add(dict1)
                    self.arrLAstCount.add(dict2)
                    self.arrLAstCount.add(dict3)
                    self.arrLAstCount.add(dict4)
                }
                
                
                
                self.scrollLbl.reloadData()
                self.tblNotes.reloadData()
                
            }
            
            
        }
    }
    
    
    
    @objc func btnClick(_ sender : UIButton){
        sender.pulsate()
        
        self.arrData = []
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acctId = dataD["_id"] as! Int
                let pagNo = sender.titleLabel?.text!
                self.btnPages.setTitle(pagNo, for: .normal)
                let pN = Int(pagNo!)
                let pagecount = Int(self.txtPagination.text!)
                let param = ["account_id": acctId,
                             "action": self.txtAction.text!,
                             "promiseType": self.txtPromiseType.text!,
                             "results": self.txtReslut.text!,
                             "user": self.txtUser.text!,
                             "notes_type": "bottom_navigation",
                             "currentPageNumber": pN!,
                             "defaultRecordsPage": pagecount!] as NSDictionary
                self.callUserListApiwithstatus(param: param)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
    func callUserListApiwithstatus(param: NSDictionary){
        
        BaseApi.onResponsePostWithToken(url: Constant.getNotes_url, controller: self, parms: param) { (dict, err) in
            Globalfunc.print(object:dict)
            
            if(err == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        self.parseDatafromApi(dict: responseDict)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: err)
                }
            }
        }
    }
}

extension notesViewController : UICollectionViewDelegate , UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrLAstCount.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collLastnote", for: indexPath) as! collLastnote
        
        let dict = self.arrLAstCount[indexPath.row] as! NSDictionary
        
        cell.lblTitle.text = dict["title"] as? String
        cell.lbl.text = dict["value"] as? String
        
        
        return cell
        
    }
}

extension notesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 150, height: 80)
    }
}

class collLastnote : UICollectionViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lbl : UILabel!
}


extension notesViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblNotes.dequeueReusableCell(withIdentifier: "notesTblCell") as! notesTblCell
        
        let dict = self.arrData[indexPath.section] as! NSDictionary
        
        if let user = dict["user"] as? String{
            cell.lblUser.text = user
        }
        
        if let result = dict["result"] as? String{
            cell.lblReulst.text = result
        }
        else{
            cell.lblReulst.text = ""
        }
        
        if let initials = dict["initials"] as? String{
            cell.lblAction.text = initials
        }
        
        
        if let description = dict["description"] as? [String]{
            var string = ""
            for value in description {
                string += value
            }
            cell.lblDesciption.text = string
        }
        
        if let created_at = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: created_at)
            let formatDate = formatter.string(from: date!)
            cell.lblDate.text = formatDate
        }
        
        if let created_at = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let date = Date.dateFromISOString(string: created_at)
            let formatDate = formatter.string(from: date!)
            cell.lblTime.text = formatDate
        }
        
        return cell
    }
    
}


class notesTblCell: UITableViewCell {
    
    @IBOutlet weak var lblDesciption : UILabel!
    @IBOutlet weak var lblReulst : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblAction : UILabel!
    @IBOutlet weak var lblUser : UILabel!
    @IBOutlet weak var lblTime : UILabel!
    
}

extension notesViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewAction{
            return self.arrAction.count
        }
        else if(pickerView == pickerViewUser){
            return self.arrUser.count
        }
            
        else if(pickerView == pickerViewPromiseTypet){
            return self.arrPromiseType.count
        }
            
        else if(pickerView == pickerViewResult){
            return self.arrResult.count
        }
        else if(pickerView == pickerViewPages){
            return self.arrPAginationNo.count
        }
        
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerViewAction{
            let strTitle = arrAction[row]
            return strTitle
        }
            
        else if(pickerView == pickerViewUser){
            let strTitle = arrUser[row]
            return strTitle
        }
            
        else if(pickerView == pickerViewPromiseTypet){
            let strTitle = arrPromiseType[row]
            return strTitle
        }
            
        else if(pickerView == pickerViewResult){
            let strTitle = arrResult[row]
            return strTitle
        }
        else if(pickerView == pickerViewPages){
            let strTitle = self.arrPAginationNo[row]
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerViewAction{
            self.txtAction.text = (arrAction[row] )
        }
            
        else if(pickerView == pickerViewUser){
            self.txtUser.text = (arrUser[row])
        }
            
        else if(pickerView == pickerViewPromiseTypet){
            self.txtPromiseType.text = (arrPromiseType[row] )
        }
            
        else if(pickerView == pickerViewResult){
            self.txtReslut.text = (arrResult[row])
        }
        else if(pickerView == pickerViewPages){
            self.txtPagination.text = self.arrPAginationNo[row]
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtAction){
            self.pickUp(txtAction)
        }
            
        else if(textField == self.txtUser){
            self.pickUp(txtUser)
        }
            
        else if(textField == self.txtPromiseType){
            self.pickUp(txtPromiseType)
        }
            
        else if(textField == self.txtReslut){
            self.pickUp(txtReslut)
        }
        else if(textField == self.txtPagination){
            self.pickUp(txtPagination)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAction){
            
            self.pickerViewAction = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAction.delegate = self
            self.pickerViewAction.dataSource = self
            self.pickerViewAction.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAction
            
            ispicker = ""
        }
            
        else if(textField == self.txtUser){
            
            self.pickerViewUser = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewUser.delegate = self
            self.pickerViewUser.dataSource = self
            self.pickerViewUser.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewUser
            
            ispicker = ""
        }
            
        else if(textField == self.txtPromiseType){
            
            self.pickerViewPromiseTypet = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPromiseTypet.delegate = self
            self.pickerViewPromiseTypet.dataSource = self
            self.pickerViewPromiseTypet.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPromiseTypet
            
            ispicker = ""
        }
            
        else if(textField == self.txtReslut){
            
            self.pickerViewResult = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewResult.delegate = self
            self.pickerViewResult.dataSource = self
            self.pickerViewResult.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewResult
            
            ispicker = ""
        }
            
        else if(textField == self.txtPagination){
            
            self.pickerViewPages = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPages.delegate = self
            self.pickerViewPages.dataSource = self
            self.pickerViewPages.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPages
            
            ispicker = "pages"
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        
        if(ispicker == "pages"){
            
            txtPagination.resignFirstResponder()
            self.arrData = []
            Globalfunc.showLoaderView(view: self.view)
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let pagecount = Int(self.txtPagination.text!)
                    
                    let pagNo = self.btnPages.titleLabel?.text!
                    let pN = Int(pagNo!)
                    let params = ["account_id":accId_selected,
                                  "currentPageNumber":pN!,
                                  "action":"\(self.txtAction.text!)",
                        "user":"\(self.txtUser.text!)",
                        "results":self.txtReslut.text!,
                        "promiseType":self.txtPromiseType.text!,
                        "notes_type":"bottom_navigation",
                        "defaultRecordsPage": pagecount!] as NSDictionary
                    self.callUserListApiwithstatus(param: params)
                    
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
        }
            
        else{
            txtAction.resignFirstResponder()
            txtUser.resignFirstResponder()
            txtReslut.resignFirstResponder()
            txtPromiseType.resignFirstResponder()
        }
    }
    
    @objc func cancelClick() {
        txtAction.resignFirstResponder()
        txtUser.resignFirstResponder()
        txtReslut.resignFirstResponder()
        txtPromiseType.resignFirstResponder()
        txtPagination.resignFirstResponder()
    }
}
