//
//  epsViewController.swift
//  LMS
//
//  Created by Apple on 22/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//
import UIKit

class epsViewController: UIViewController {
    
    @IBOutlet weak var txtDelivey : UITextField!
    @IBOutlet weak var lblAccountNo : UILabel!
    
    
    @IBOutlet weak var viewValidation : UIView!
    @IBOutlet weak var hgtValidation : NSLayoutConstraint!
    
    
    @IBOutlet weak var viewActTyp : UIView!
    @IBOutlet weak var lblActTyp : UILabel!
    @IBOutlet weak var txtActTyp : UITextField!
    
    @IBOutlet weak var viewMainForm : UIView!
    
    var pickerViewAcctTyp: UIPickerView!
    var pickerViewDelivery: UIPickerView!
    var pv_borrower: UIPickerView!
    var pv_month: UIPickerView!
    var pv_year: UIPickerView!
    var pv_state: UIPickerView!
    
    var arMonth = [NSDictionary]()
    
    
    @IBOutlet weak var txtFname : UITextField!
    @IBOutlet weak var txtLname : UITextField!
    @IBOutlet weak var txtAcctNo : UITextField!
    @IBOutlet weak var txtBorrower : UITextField!
    @IBOutlet weak var txtBillAddress : UITextField!
    @IBOutlet weak var txtBillcity : UITextField!
    @IBOutlet weak var txtBillstate : UITextField!
    @IBOutlet weak var txtBillzip : UITextField!
    @IBOutlet weak var txtBillcvv : UITextField!
    @IBOutlet weak var txtmonth : UITextField!
    @IBOutlet weak var txtyear : UITextField!
    
    
    var arrUrl = [Constant.delivery_method_url, Constant.state]
    var arrContactTyp = NSMutableArray()
    var arrState = NSMutableArray()
    
    
    var state_id = ""
    var acct_id : Int?
    var actTyp_id = ""
    var deliver_id : Int?
    var month_id : Int?
    
    
    var arrAcctyp = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let account_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account No: \(account_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        if let contact  = userDef.object(forKey: "contact") as? [NSDictionary]{
            if(contact.count > 0){
                for i in 0..<contact.count{
                    let dict = contact[i]
                    self.arrContactTyp.add(dict)
                }
            }
        }
        
        
        let dict1 = ["title": "Credit Card","val":"1"]
        let dict2 = ["title": "Debit Card","val":"2"]
        self.arrAcctyp.add(dict1)
        self.arrAcctyp.add(dict2)
        
        let dict_1 = ["title": "Jan","val":1] as NSDictionary
        let dict_2 = ["title": "Feb","val":2] as NSDictionary
        let dict_3 = ["title": "Mar","val":3] as NSDictionary
        let dict_4 = ["title": "Apr","val":4] as NSDictionary
        let dict_5 = ["title": "May","val":5] as NSDictionary
        let dict_6 = ["title": "Jun","val":6] as NSDictionary
        let dict_7 = ["title": "Jul","val":7] as NSDictionary
        let dict_8 = ["title": "Aug","val":8] as NSDictionary
        let dict_9 = ["title": "Sep","val":9] as NSDictionary
        let dict_10 = ["title": "Oct","val":10] as NSDictionary
        let dict_11 = ["title": "Nov","val":11] as NSDictionary
        let dict_12 = ["title": "Dec","val":12] as NSDictionary
        
        self.arMonth.append(dict_1)
        self.arMonth.append(dict_2)
        self.arMonth.append(dict_3)
        self.arMonth.append(dict_4)
        self.arMonth.append(dict_5)
        self.arMonth.append(dict_6)
        self.arMonth.append(dict_7)
        self.arMonth.append(dict_8)
        self.arMonth.append(dict_9)
        self.arMonth.append(dict_10)
        self.arMonth.append(dict_11)
        self.arMonth.append(dict_12)

        self.viewValidation.isHidden = true
        self.hgtValidation.constant = 0
        self.viewActTyp.isHidden = true
        self.lblActTyp.isHidden = true
        self.txtActTyp.isHidden = true
        self.viewMainForm.isHidden = true
        
        Globalfunc.showLoaderView(view: self.view)
        self.callDeliveryMEthodApi()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clinOnBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension epsViewController : UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerViewDelivery){
            return Constant.arrDElivryMethod.count
        }
        else if(pickerView == pickerViewAcctTyp){
            return arrAcctyp.count
        }
        else if(pickerView == pv_month){
            return arMonth.count
        }
        else if(pickerView == pv_year){
            return consArrays.arrYr.count
        }
        else if pickerView == pv_borrower{
            return self.arrContactTyp.count
        }
        else if pickerView == pv_state{
            return self.arrState.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerViewDelivery){
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pickerViewAcctTyp){
            let dict = arrAcctyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle

        }
        else if(pickerView == pv_month){
            let dict = arMonth[row]
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pv_year){
            let strTitle = consArrays.arrYr[row]
            return strTitle
        }
        else if(pickerView == pv_borrower){
            let dict = arrContactTyp[row] as! NSDictionary
            let first_name = dict["first_name"] as! String
            let last_name = dict["last_name"] as! String
            let strTitle = "\(first_name) \(last_name)"
            return strTitle
        }
        else if pickerView == pv_state{
            let dict = arrState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        
        
        return ""
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewDelivery){
            let dict = Constant.arrDElivryMethod[row] as! NSDictionary
            let _id = dict["_id"] as! Int
            self.deliver_id = _id
            self.txtDelivey.text = (dict["title"] as! String)
            if(_id == 15 || _id == 21){
                self.viewValidation.isHidden = true
                self.hgtValidation.constant = 0
                self.viewActTyp.isHidden = false
                self.lblActTyp.isHidden = false
                self.txtActTyp.isHidden = false
                self.viewMainForm.isHidden = true
            }
            else{
                self.viewValidation.isHidden = false
                self.hgtValidation.constant = 45
                self.viewActTyp.isHidden = true
                self.lblActTyp.isHidden = true
                self.txtActTyp.isHidden = true
                self.viewMainForm.isHidden = true
            }
        }
        else if(pickerView == pickerViewAcctTyp){
            
            let dict = arrAcctyp[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtActTyp.text = strTitle
            
            let val = dict["val"] as! String
            self.actTyp_id = val
            
            self.viewMainForm.isHidden = false

        }
        else if(pickerView == pv_month){
            let dict = arMonth[row]
            let strTitle = dict["title"] as! String
            self.txtmonth.text = strTitle
            
            let val = dict["val"] as! Int
            self.month_id = val

        }
        else if(pickerView == pv_year){
            let strTitle = consArrays.arrYr[row]
            self.txtyear.text = strTitle
        }
        else  if(pickerView == pv_borrower){
            let dict = arrContactTyp[row] as! NSDictionary
            let first_name = dict["first_name"] as! String
            let last_name = dict["last_name"] as! String
            let strTitle = "\(first_name) \(last_name)"
            self.txtBorrower.text = strTitle
            
            let account_id = dict["account_id"] as! Int
            self.acct_id = account_id
            
        }
            
        else if(pickerView == pv_state){
            let dict = arrState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtBillstate.text = strTitle
            let _id = dict["_id"] as! Int
            self.state_id = "\(_id)"
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtDelivey){
            self.pickUp(txtDelivey)
        }
        else if(textField == self.txtActTyp){
            self.pickUp(txtActTyp)
        }
        else if(textField == self.txtmonth){
            self.pickUp(txtmonth)
        }
        else if(textField == self.txtyear){
            self.pickUp(txtyear)
        }
        else if(textField == self.txtBorrower){
            self.pickUp(txtBorrower)
        }
        else if(textField == self.txtBillstate){
            self.pickUp(txtBillstate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtDelivey){
            self.pickerViewDelivery = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewDelivery.delegate = self
            self.pickerViewDelivery.dataSource = self
            self.pickerViewDelivery.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewDelivery
        }
        else if(textField == self.txtActTyp){
            self.pickerViewAcctTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAcctTyp.delegate = self
            self.pickerViewAcctTyp.dataSource = self
            self.pickerViewAcctTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAcctTyp
        }
        else if(textField == self.txtmonth){
            self.pv_month = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_month.delegate = self
            self.pv_month.dataSource = self
            self.pv_month.backgroundColor = UIColor.white
            textField.inputView = self.pv_month
        }
            
        else if(textField == self.txtyear){
            self.pv_year = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_year.delegate = self
            self.pv_year.dataSource = self
            self.pv_year.backgroundColor = UIColor.white
            textField.inputView = self.pv_year
        }
            
        else if(textField == self.txtBorrower){
            self.pv_borrower = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_borrower.delegate = self
            self.pv_borrower.dataSource = self
            self.pv_borrower.backgroundColor = UIColor.white
            textField.inputView = self.pv_borrower
        }
        else if(textField == self.txtBillstate){
            self.pv_state = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_state.delegate = self
            self.pv_state.dataSource = self
            self.pv_state.backgroundColor = UIColor.white
            textField.inputView = self.pv_state
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtDelivey.resignFirstResponder()
        txtActTyp.resignFirstResponder()
        txtmonth.resignFirstResponder()
        txtyear.resignFirstResponder()
        txtBorrower.resignFirstResponder()
        txtBillstate.resignFirstResponder()
    }
    
}

extension epsViewController {
    func callDeliveryMEthodApi()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.delivery_method_url){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            Constant.arrDElivryMethod.add(dictA)
                                        }
                                    }
                                }
                            }
                            
                        }
                        if(obj == Constant.state){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrState.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}


extension epsViewController {
    
    @IBAction func clickOnConfrmBtn(_ sender: UIButton)
    {
        if(txtFname.text == "" || txtFname.text?.count == 0 || txtFname.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "First Name should not left blank.")
        }
        else if(txtLname.text == "" || txtLname.text?.count == 0 || txtLname.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Last Name should not left blank.")
        }
        else if(txtAcctNo.text == "" || txtAcctNo.text?.count == 0 || txtAcctNo.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Account Number should not left blank.")
        }
        else if(txtBorrower.text == "" || txtBorrower.text?.count == 0 || txtBorrower.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Borrower.")
        }
        else if(txtBillAddress.text == "" || txtBillAddress.text?.count == 0 || txtBillAddress.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Address should not left blank.")
        }
        else if(txtBillcity.text == "" || txtBillcity.text?.count == 0 || txtBillcity.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "City should not left blank.")
        }
        else if(txtBillstate.text == "" || txtBillstate.text?.count == 0 || txtBillstate.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select State.")
        }
        else if(txtBillzip.text == "" || txtBillzip.text?.count == 0 || txtBillzip.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Zipcode should not left blank.")
        }
        else if(txtBillcvv.text == "" || txtBillcvv.text?.count == 0 || txtBillcvv.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Cvv should not left blank.")
        }
        else if(txtmonth.text == "" || txtmonth.text?.count == 0 || txtmonth.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Month.")
        }
        else if(txtyear.text == "" || txtyear.text?.count == 0 || txtyear.text == nil){
            Globalfunc.showToastWithMsg(view: self.view, str: "Please select Year.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let account_id = dataD["_id"] as! Int
                    let params = [
                        "account_id": account_id,
                        "borrower_id": self.acct_id!,
                        "ccBillingAddress": self.txtBillAddress.text!,
                        "ccBillingCVV": self.txtBillcvv.text!,
                        "ccBillingCity": self.txtBillcity.text!,
                        "ccBillingState": self.state_id,
                        "ccBillingZip": self.txtBillzip.text!,
                        "ccExpMonth": self.month_id!,
                        "ccExpYear": self.txtyear.text!,
                        "deliveryMethod": self.deliver_id!,
                        "epsAccountNumber": self.txtAcctNo.text!,
                        "epsAccountType": self.actTyp_id,
                        "epsFirstName": self.txtFname.text!,
                        "epsLastName": self.txtLname.text!,
                        "insid":(user?.institute_id!)!,
                        "userid":(user?._id)!] as [String : Any]
                    
                    
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params)
                    
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
            
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.eps_account, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}




