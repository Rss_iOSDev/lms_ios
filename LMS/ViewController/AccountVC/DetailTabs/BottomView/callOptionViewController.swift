//
//  callOptionViewController.swift
//  LMS
//
//  Created by Apple on 20/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class callOptionViewController: UIViewController {
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    @IBOutlet weak var lblTitleinView : UILabel!
    
    
    //outlet hide for follow up date view
    @IBOutlet weak var viewResult : UIView!
    @IBOutlet weak var lblResult : UILabel!
    @IBOutlet weak var lblTopConstraint : NSLayoutConstraint!
    
    //view call
    @IBOutlet weak var viewMAkeCall : UIView!
    @IBOutlet weak var lblNAme : UILabel!
    @IBOutlet weak var lblCellPhn : UILabel!
    @IBOutlet weak var lblHomePhn : UILabel!
    @IBOutlet weak var btnCall_cell : UIButton!
    @IBOutlet weak var btnCall_home : UIButton!
    
    //view other optiond
    
    @IBOutlet weak var viewOptions : UIView!
    @IBOutlet weak var txtResults : UITextField!
    @IBOutlet weak var txtDate : UITextField!
    @IBOutlet weak var txtTime : UITextField!
    @IBOutlet weak var txtNotes : UITextView!
    
    
    @IBOutlet weak var viewYesNo : UIView!
    @IBOutlet weak var hgtViewYN : NSLayoutConstraint!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var viewBtn : UIView!
    
    
    var arrResult = ["Call Disconnected","Discussed Options","Disputes Account","Offered To Settle","Other","Promised To Pay","Promised To Provide Insurance","Received Payment","Refused To Pay","Unable To Insure","Unable To Pay"]
    
    var pickerViewResultList: UIPickerView!
    
    var pickerDate : UIDatePicker!
    var pickerTime : UIDatePicker!
    
    var isSelectDateTime = ""
    
    
    
    
    
    var contactType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.viewOptions.isHidden = true
        self.viewMAkeCall.isHidden = true
        
        
        btnCall_cell.setImage((UIImage (named: "call"))!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        btnCall_cell.tintColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
        
        btnCall_home.setImage((UIImage (named: "call"))!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        btnCall_home.tintColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
        
        self.hgtViewYN.constant = 0
        self.lblTitle.isHidden = true
        self.viewBtn.isHidden = true
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account no: \(acct_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
    
    
    @IBAction func clickONBackBtn(_ sender : UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickToOpenRecievedActions(_ sender: UIButton)
    {
        self.hgtViewYN.constant = 0
        self.lblTitle.isHidden = true
        self.viewBtn.isHidden = true
        
        self.viewMAkeCall.isHidden = true
        if(sender.tag == 10){ //recievwe a call
            self.viewOptions.isHidden = false
            self.lblTitleinView.text = "RECEIVED A CALL"
            contactType = "Received-Call"
            self.lblResult.isHidden = false
            self.viewResult.isHidden = false
            self.lblTopConstraint.constant = 15
            
        }
        else if(sender.tag == 20){ //recievwe a text
            self.viewOptions.isHidden = false
            self.lblTitleinView.text = "RECEIVED A TEXT"
            contactType = "Received-Text"
            self.lblResult.isHidden = false
            self.viewResult.isHidden = false
            self.lblTopConstraint.constant = 15
        }
        else if(sender.tag == 30){ //recievwe an email
            self.viewOptions.isHidden = false
            self.lblTitleinView.text = "RECEIVED AN EMAIL"
            contactType = "Received-Email"
            self.lblResult.isHidden = false
            self.viewResult.isHidden = false
            self.lblTopConstraint.constant = 15
        }
        else if(sender.tag == 40){ //add follow up date
            self.viewOptions.isHidden = false
            self.lblTitleinView.text = "ADD FOLLOW UP DATE"
            contactType = "Follow-Up-Date"
            self.lblResult.isHidden = true
            self.viewResult.isHidden = true
            self.lblTopConstraint.constant = -63
        }
    }
    
    @IBAction func clickOnMakeCallBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        self.getCallDetailsApi()
    }
    
    @IBAction func clilcONBtnToCloseCallView(_ sender: UIButton)
    {
        if(sender.tag == 50){ //confirm
            self.viewMAkeCall.isHidden = true
        }
        else if(sender.tag == 60){ //cancel
            self.viewMAkeCall.isHidden = true
        }
    }
    
    @IBAction func clickOnBtnPhn(_ sender: UIButton)
    {
        self.viewMAkeCall.isHidden = true
        if(sender == self.btnCall_cell){
            
            self.hgtViewYN.constant = 95
            self.lblTitle.isHidden = false
            self.viewBtn.isHidden = false
            
            self.viewOptions.isHidden = false
            self.lblTitleinView.text = "RECEIVED A CALL"
            contactType = "Make-Call"
            self.lblResult.isHidden = false
            self.viewResult.isHidden = false
            self.lblTopConstraint.constant = 15
        }
        else if(sender == self.btnCall_home){
            
        }
    }
    
    @IBAction func clickOnPerformActionBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){ //cancel
            self.viewOptions.isHidden = true
        }
        else if(sender.tag == 20){ //confirm
            self.callPostApiForCall()
        }
    }
}


//Api for make app view
extension callOptionViewController {
    
    func getCallDetailsApi()
    {
        do {
            let decoded  = userDef.object(forKey: "action_dict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                Globalfunc.print(object:responseDict)
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                        
                        self.viewMAkeCall.isHidden = false
                        self.viewOptions.isHidden = true
                        
                        var strFullName = ""
                        
                        if let first_name = loanAccount["firstName"] as? String{
                            strFullName += first_name
                        }
                        
                        if let middle_name = loanAccount["middleName"] as? String{
                            strFullName += "" + middle_name
                        }
                        
                        if let last_name = loanAccount["lastName"] as? String{
                            strFullName += "" + last_name
                        }
                        
                        self.lblNAme.text = "\(strFullName) [PRIMARY]"
                        
                        
                        if let cellPhone = loanAccount["cellPhone"] as? String{
                            self.lblCellPhn.text = "Cell Phone: \(cellPhone)"
                        }
                        
                        if let homePhone = loanAccount["homePhone"] as? String{
                            self.lblHomePhn.text = "Home Phone: \(homePhone)"
                        }
                    }
                }
            }
        } catch {
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
    
    
    func callPostApiForCall(){
        
        if(txtResults.text == "" || txtResults.text?.count == 0 || txtResults.text == nil){
            txtResults.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Results.")
        }
        else if(txtDate.text == "" || txtDate.text?.count == 0 || txtDate.text == nil){
            txtDate.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Date.")
        }
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    
                    let acct_id = dataD["_id"] as! Int
                    let userid = dataD["userid"] as! Int
                    let insID = dataD["insid"] as! Int
                    
                    var followHr = ""
                    var followMin = ""
                    var followFormat = ""
                    
                    if(txtTime.text != ""){
                        
                        let result = txtTime.text!.split(separator: ":") as NSArray
                        
                        followHr = result.object(at: 0) as! String
                        let follow = result.object(at: 1) as! String
                        
                        let newReslt = follow.split(separator: " ") as NSArray
                        followMin = newReslt.object(at: 0) as! String
                        followFormat = newReslt.object(at: 1) as! String
                    }
                    
                    let param = ["contactType": "\(contactType)", //act_id dynamic
                        "insid": insID,
                        "account_id": acct_id,
                        "notes": "\(txtNotes.text!)",
                        "followUpDate": "\(txtDate.text!)",
                        "userid": userid,
                        "result": "\(self.txtResults.text!)",
                        "followUpFormat": "\(followFormat)",
                        "followUpMin": "\(followMin)",
                        "followUpHour": "\(followHr)"] as [String : Any]
                    
                    Globalfunc.print(object:param)
                    
                    BaseApi.onResponsePostWithToken(url: Constant.Savecontact_url, controller: self, parms: param) { (dict, error) in
                        if(error == ""){
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.print(object:dict)
                                if let responseDict = dict as? NSDictionary {
                                    let msg = responseDict["msg"] as! String
                                    let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        self.dismiss(animated: true) {
                                            self.viewOptions.isHidden = true
                                            self.viewMAkeCall.isHidden = true
                                        }
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        else
                        {
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                            }
                        }
                    }
                    
                    
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
            
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
        
        
        
        
        
        
        
    }
    
}

extension callOptionViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewResultList){
            return self.arrResult.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerViewResultList){
            let strTitle = arrResult[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewResultList){
            self.txtResults.text = arrResult[row]
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtResults){
            self.pickUp(txtResults)
        }
            
        else if(textField == self.txtDate){
            self.pickUpDateTime(txtDate)
        }
            
        else if(textField == self.txtTime){
            self.pickUpDateTime(txtTime)
        }
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtDate){
            
            isSelectDateTime = "date"
            self.pickerDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDate.datePickerMode = .date
            self.pickerDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerDate
        }
            
        else if(textField == self.txtTime){
            
            isSelectDateTime = "time"
            self.pickerTime = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTime.datePickerMode = .time
            self.pickerTime.backgroundColor = UIColor.white
            textField.inputView = self.pickerTime
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtResults){
            
            self.pickerViewResultList = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewResultList.delegate = self
            self.pickerViewResultList.dataSource = self
            self.pickerViewResultList.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewResultList
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        if(isSelectDateTime == "date"){
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            self.txtDate.text = formatter.string(from: pickerDate.date)
            self.txtDate.resignFirstResponder()
        }
        else{
            
            let formatter = DateFormatter()
            formatter.timeStyle = .short
            self.txtTime.text = formatter.string(from: pickerTime.date)
            self.txtTime.resignFirstResponder()
            
        }
        
    }
    
    @objc func cancelClickDate() {
        
        txtDate.resignFirstResponder()
        txtTime.resignFirstResponder()
        
        
    }
    
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtResults.resignFirstResponder()
        
    }
    
    @objc func cancelClick() {
        
        txtResults.resignFirstResponder()
        
        
    }
    
    
}
