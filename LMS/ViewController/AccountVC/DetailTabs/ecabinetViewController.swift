//
//  ecabinetViewController.swift
//  LMS
//
//  Created by Apple on 05/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class ecabinetViewController: UIViewController {
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tblEcabinet: UITableView!
    
    var arrEcabinetList : NSMutableArray = []
    
    
    @IBOutlet weak var viewPAgination: UIView!
    @IBOutlet weak var scrollViewPages: UIScrollView!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.isHidden = true
        self.lblStatus.text = ""
        self.tblEcabinet.isHidden = true
        self.viewPAgination.isHidden = true
        
        self.tblEcabinet.tableFooterView = UIView()
        self.call_getEcabinetHistoryApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}


//Api --
extension ecabinetViewController {
    
    func call_getEcabinetHistoryApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"
                
                
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.ecabinet_doc_url)?account_id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            if let response = dict as? NSDictionary{
                                if let data = response["data"] as? NSDictionary{
                                    self.parseDatafromApi(response: data)
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func parseDatafromApi(response : NSDictionary)
    {
        let currentPageNumber = response["currentPageNumber"] as! Int
        let defaultRecordsPage = response["defaultRecordsPage"] as! Int
        
        let record = response["totalrecord"] as! Int
        let cpNo = CGFloat(currentPageNumber)
        let pagSize = CGFloat(defaultRecordsPage)
        let totalR = CGFloat(record)
        
        let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
        let pagecount = Int(pageNo)
        
        let buttonPadding:CGFloat = 10
        var xOffset:CGFloat = 10
        
        for i in 0..<pagecount {
            if (i == 0){
                self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
            }
            let button = UIButton()
            button.tag = i+1
            button.setTitle("\(i+1)", for: .normal)
            button.layer.borderWidth = 1
            button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
            button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
            button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
            button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
            button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
            xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
            self.scrollViewPages.addSubview(button)
        }
        self.scrollWidth.constant = xOffset
        self.scrollViewPages.contentSize = CGSize(width: xOffset, height: self.scrollViewPages.frame.height)
        
        
        
        if let arr = response["document"] as? [NSDictionary] {
            if(arr.count > 0){
                for i in 0..<arr.count{
                    let dictA = arr[i]
                    self.arrEcabinetList.add(dictA)
                }
                self.tblEcabinet.isHidden = false
                self.lblStatus.isHidden = true
                self.lblStatus.text = ""
                self.viewPAgination.isHidden = false
                self.tblEcabinet.reloadData()
            }
            else{
                self.tblEcabinet.isHidden = true
                self.viewPAgination.isHidden = true
                self.lblStatus.isHidden = false
                self.lblStatus.text = "No Ecabinet Files Have Been Found For This Search."
            }
        }
    }
    
    @objc func btnClick(_ sender : UIButton){
        sender.pulsate()
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                let pagNo = sender.titleLabel?.text!
                let pN = Int(pagNo!)
                self.arrEcabinetList = []
                Globalfunc.showLoaderView(view: self.view)
                let param = ["account_id": accId_selected,
                             "currentPageNumber": pN!,
                             "defaultRecordsPage": 10] as NSDictionary
                self.callEcabinetListApiwithstatus(param: param)
                
            }
        }
        catch{
            
        }
    }
    
    
    
    func callEcabinetListApiwithstatus(param: NSDictionary){
        
        BaseApi.onResponsePostWithToken(url: Constant.ecabinet_doc_url, controller: self, parms: param) { (dict, err) in
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                if let response = dict as? NSDictionary {
                    self.parseDatafromApi(response: response)
                }
            }
        }
    }
}
extension ecabinetViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrEcabinetList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblEcabinet.dequeueReusableCell(withIdentifier: "tblEcabinetCell") as! tblEcabinetCell
        let dict = arrEcabinetList[indexPath.row] as! NSDictionary
        
        
        if let account_no = dict["account_no"] as? Int
        {
            cell.lblAccID.text = "\(account_no)"
        }
        if let created_at = dict["created_at"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: created_at)
            cell.lblDate.text = formatter.string(from: date!)
        }
        if let fileSize = dict["fileSize"] as? String
        {
            cell.lblFileSize.text = fileSize
        }
        
        if let owner = dict["owner"] as? String
        {
            cell.lblOwner.text = owner
        }
        if let customer = dict["customer"] as? String
        {
            cell.lblCustomer.text = customer
        }
        if let included_forms = dict["included_forms"] as? String
        {
            cell.lblIncludedForm.text = included_forms
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(clickOnViewBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func clickOnViewBtn(_ sender: UIButton)
    {
        let dict = arrEcabinetList[sender.tag] as! NSDictionary
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "pdfDocViewController") as! pdfDocViewController
        vc.modalPresentationStyle = .fullScreen
        vc.passDict = dict
        self.present(vc, animated: true, completion: nil)
    }
}

class tblEcabinetCell : UITableViewCell
{
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblAccID : UILabel!
    @IBOutlet weak var lblOwner : UILabel!
    @IBOutlet weak var lblFileSize : UILabel!
    @IBOutlet weak var lblCustomer : UILabel!
    @IBOutlet weak var lblIncludedForm : UILabel!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var btnCheck : UIButton!
}
