//
//  paymntSchdVC.swift
//  LMS
//
//  Created by Apple on 04/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class paymntSchdVC: UIViewController {
    
    @IBOutlet weak var tblPaySchd: UITableView!
    var arrValues : [NSDictionary] = []
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.call_getPayScheduleListApi()
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

}

//Api --
extension paymntSchdVC {
    
    func call_getPayScheduleListApi()
    {
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
        if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
        
            let accId_selected = dataD["_id"] as! Int
            
            let account_no = dataD["account_no"] as! String
            self.lblAcctNo.text = "Account No: \(account_no)"

            let Str_url = "\(Constant.payment_schedule_url)?id=\(accId_selected)"
            BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let responseDict = dict as? NSDictionary {
                            if let arrP = responseDict["data"] as? [NSDictionary] {
                                if(arrP.count > 0){
                                    for i in 0..<arrP.count{
                                        let dictA = arrP[i]
                                        self.arrValues.append(dictA)
                                    }
                                    self.tblPaySchd.reloadData()
                                }
                                else{
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension paymntSchdVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.arrValues.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tblPaySchd.dequeueReusableCell(withIdentifier: "paySchdlGridCell") as! paySchdlGridCell
        //row data
        let dict = self.arrValues[indexPath.row]
        Globalfunc.print(object:dict)
        
        if let typeNumber = dict["typeNumber"] as? String
        {
            cell.lblTypNo.text = "Type- Number : \(typeNumber)"
        }
        
        if let dueDate = dict["dueDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            let createFormat = formatter.string(from: date!)
            cell.lblDueDate.text = "Due Date : \(createFormat)"
        }
        
        if let regularDue = dict["regularDue"] as? NSNumber
        {
            cell.lblRegular_due.text = "Regular Due : $\(regularDue)"
        }
        
        if let lateFee = dict["lateFee"] as? NSNumber
        {
            cell.lblLateFee.text = "Late Fee : $\(lateFee)"
        }
        
        if let totalDue = dict["totalDue"] as? NSNumber
        {
            cell.lblTotalDue.text = "Total Due : $\(totalDue)"
        }
        if let schedPrincipal = dict["schedPrincipal"] as? NSNumber
        {
            cell.lblSchedPrin.text = "Sched Principal : $\(schedPrincipal)"
        }
        if let schedInterest = dict["schedInterest"] as? NSNumber
        {
            cell.lblSchdIntrst.text = "Sched Interest : $\(schedInterest)"
        }
        if let schedTax = dict["schedTax"] as? NSNumber
        {
            cell.lblSchdTax.text = "Sched Tax : $\(schedTax)"
        }
        if let appliedPrincipal = dict["appliedPrincipal"] as? NSNumber
        {
            cell.lblAppliedPrin.text = "Applied Principal : $\(appliedPrincipal)"
        }
        
        if let appliedInterest = dict["appliedInterest"] as? NSNumber
        {
            cell.lblAppliedIntrst.text = "Applied Intrest : $\(appliedInterest)"
        }
        
        if let appliedTax = dict["appliedTax"] as? NSNumber
        {
            cell.lblAppliedTax.text = "Applied Tax : $\(appliedTax)"
        }
        
        if let appliedLateFee = dict["appliedLateFee"] as? NSNumber
        {
            cell.lblAppliedLAteFee.text = "Applied LateFee : $\(appliedLateFee)"
        }
        
        if let stillDue = dict["stillDue"] as? NSNumber
        {
            cell.lblStillDue.text = "Still Due : $\(stillDue)"
        }
        
        if let extended = dict["extended"] as? NSNumber
        {
            if(extended == 0)
            {
                cell.lblExtnded.text = "Extended? : False"
            }
            else
            {
                cell.lblExtnded.text = "Extended? : True"
            }
        }
        return cell
    }
}

class paySchdlGridCell: UITableViewCell {
    
    @IBOutlet weak var lblTypNo: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblRegular_due: UILabel!
    @IBOutlet weak var lblLateFee: UILabel!
    @IBOutlet weak var lblTotalDue: UILabel!
    @IBOutlet weak var lblSchedPrin: UILabel!
    @IBOutlet weak var lblSchdIntrst: UILabel!
    @IBOutlet weak var lblSchdTax: UILabel!
    @IBOutlet weak var lblAppliedPrin: UILabel!
    @IBOutlet weak var lblAppliedIntrst: UILabel!
    @IBOutlet weak var lblAppliedTax: UILabel!
    @IBOutlet weak var lblAppliedLAteFee: UILabel!
    @IBOutlet weak var lblStillDue: UILabel!
    @IBOutlet weak var lblExtnded: UILabel!
    
}
