//
//  reportListViewController.swift
//  LMS
//
//  Created by Apple on 15/10/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class reportListViewController: UIViewController {
    
    
    @IBOutlet weak var txtBalType : UITextField!
    @IBOutlet weak var tblSchedule : UITableView!
    
    var arrReport = NSMutableArray()
    
    var arrBalanceTyp = NSMutableArray()
    var pickerBalType : UIPickerView!
    var balTypId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI(){
        
        Globalfunc.showLoaderView(view: self.view)
        
        
        
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                Globalfunc.print(object:dataD)
                
                
                if let balanceType = dataD["balanceType"] as? [NSDictionary]{
                    
                    if(balanceType.count > 0){
                        for i in 0..<balanceType.count{
                            let dict = balanceType[i]
                            self.arrBalanceTyp.add(dict)
                        }
                        let dict = self.arrBalanceTyp[0] as! NSDictionary
                        let strTitle = dict["description"] as! String
                        self.txtBalType.text = strTitle
                        if let _id = dict["_id"] as? String{
                            self.balTypId = _id
                        }
                    }
                }
            }
        }
        catch{
            
        }
        self.callGetApi()
    }
    
    
    @IBAction func clickONBaclBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

/*MARk - Call APi*/
extension reportListViewController {
    
    func callGetApi(){
        
        BaseApi.callApiRequestForGet(url: Constant.reports) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    
                    if let response = dict as? NSDictionary {
                        Globalfunc.print(object:response)
                        
                        if let dictResponse = response["data"] as? [NSDictionary]{
                            
                            if(dictResponse.count > 0){
                                for i in 0..<dictResponse.count{
                                    let dict = dictResponse[i]
                                    self.arrReport.add(dict)
                                }
                                 self.tblSchedule.reloadData()
                            }
                            else{
                                self.arrReport = []
                                self.tblSchedule.reloadData()
                            }
                        }
                    }
                    
                }
            }
        }
    }
}


/*MARk - Textfeld and picker delegates*/

extension reportListViewController : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerBalType{
            return self.arrBalanceTyp.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerBalType{
            let dict = arrBalanceTyp[row] as! NSDictionary
            let strTitle = dict["description"] as! String
            self.txtBalType.text = strTitle
            if let _id = dict["_id"] as? String{
                self.balTypId = _id
            }
            if let _id = dict["_id"] as? Int{
                self.balTypId = "\(_id)"
            }
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtBalType){
            self.pickUp(txtBalType)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtBalType){
            
            self.pickerBalType = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBalType.delegate = self
            self.pickerBalType.dataSource = self
            self.pickerBalType.backgroundColor = UIColor.white
            textField.inputView = self.pickerBalType
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtBalType.resignFirstResponder()
        
    }
    
    @objc func cancelClick() {
        
        txtBalType.resignFirstResponder()
        
    }
    
}

/*-----MArk Table Schedule**/
extension reportListViewController : UITableViewDelegate , UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrReport.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblSchedule.dequeueReusableCell(withIdentifier: "tblReport") as! tblReport
        let dict = self.arrReport[indexPath.section] as! NSDictionary
        if let report_name = dict["report_name"] as? String{
            cell.lblReportName.text = report_name
        }
        
        
        return cell
    }
    
}

class tblReport: UITableViewCell {
    
    @IBOutlet weak var lblReportName : UILabel!
    @IBOutlet weak var btnRun : UIButton!
    
}

