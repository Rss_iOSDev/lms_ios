//
//  borrowerViewController.swift
//  LMS
//
//  Created by Apple on 17/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borrowerViewController: UIViewController {
    
    @IBOutlet weak var tblBorrower : UITableView!
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrList = ["DETAIL","ADDRESS HISTORY","EMPLOYMENT HISTORY","ACCOUNTS","NOTES","REFERENCES","FILES"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblBorrower.tableFooterView = UIView()
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = responseDict["account_no"] as! String
                self.lblAcctNo.text = "Account no: \(acct_no)"
            }
        }
        catch{
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}


extension borrowerViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblBorrower.dequeueReusableCell(withIdentifier: "tblBorrowerCell") as! tblBorrowerCell
        cell.lblTitle.text = self.arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row{
        case 0:
            self.presentViewControllerBasedOnIdentifier("borr_DetailsVC", strStoryName: "ActionStory")
            
        case 1:
            self.presentViewControllerBasedOnIdentifier("borr_AdressHistVC", strStoryName: "ActionStory")
            
        case 2:
            self.presentViewControllerBasedOnIdentifier("borr_EmployHistVC", strStoryName: "ActionStory")
            
        case 3:
            self.presentViewControllerBasedOnIdentifier("borr_AccountsVC", strStoryName: "ActionStory")
        case 4:
            self.presentViewControllerBasedOnIdentifier("borr_notesVC", strStoryName: "ActionStory")
            
        case 5:
            self.presentViewControllerBasedOnIdentifier("borr_refrence", strStoryName: "ActionStory")
            
        case 6:
            self.presentViewControllerBasedOnIdentifier("borr_filesVC", strStoryName: "ActionStory")
            
        default:
            Globalfunc.print(object:"default")
        }
    }
    
    
}

class tblBorrowerCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
