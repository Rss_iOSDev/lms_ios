//
//  collateralViewController.swift
//  LMS
//
//  Created by Apple on 13/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class collateralViewController: UIViewController {
    
    @IBOutlet weak var collTitles : UITableView!
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrTitles = ["VEHICLE","PRICING","OPTIONS","PAYMENT/GPS DEVICE","PURCHASE","IMAGES","FILES","NOTES"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        collTitles.tableFooterView = UIView()
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                print(dataD)
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension collateralViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.collTitles.dequeueReusableCell(withIdentifier: "colletraltitleCell") as! colletraltitleCell
        cell.lblTitle.text = self.arrTitles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.presentViewControllerBasedOnIdentifier("collVehVC", strStoryName: "colletralStory")
        case 1:
            self.presentViewControllerBasedOnIdentifier("collPriceVC", strStoryName: "colletralStory")
        case 2:
            self.presentViewControllerBasedOnIdentifier("collOptionsVC", strStoryName: "colletralStory")
        case 3:
            self.presentViewControllerBasedOnIdentifier("collPaymntVC", strStoryName: "colletralStory")
        case 4:
            self.presentViewControllerBasedOnIdentifier("collPurchaseVC", strStoryName: "colletralStory")
        case 5:
            self.presentViewControllerBasedOnIdentifier("collImagesVC", strStoryName: "colletralStory")
        case 6:
            self.presentViewControllerBasedOnIdentifier("collFilesVC", strStoryName: "colletralStory")
        case 7:
            self.presentViewControllerBasedOnIdentifier("collNotesVC", strStoryName: "colletralStory")
        default:
            Globalfunc.print(object:"default")
        }
    }
}

class colletraltitleCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
