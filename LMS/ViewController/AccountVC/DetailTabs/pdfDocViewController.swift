//
//  pdfDocViewController.swift
//  LMS
//
//  Created by Apple on 08/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import WebKit

class pdfDocViewController: UIViewController , WKNavigationDelegate{
    
    var passDict : NSDictionary = [:]
    
    
    @IBOutlet weak var webView: WKWebView!
    
    var isFrom = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.webView.navigationDelegate = self
        Globalfunc.showLoaderView(view: self.view)
        self.loadWebView()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func loadWebView()
    {
        if(isFrom == "document"){
            let filePath = self.passDict["file_path"] as! String
            let final_url = NSURL(fileURLWithPath: filePath) as URL
            webView.load(URLRequest(url: final_url))
            webView.allowsBackForwardNavigationGestures = true
            Globalfunc.hideLoaderView(view: self.view)
        }
        else{
            let account_id = self.passDict["account_id"] as! Int
            let filePath = self.passDict["filePath"] as! String
            let strUrl = "\(Constant.docBaseUrl)ecabinet/\(account_id)/\(filePath)"
            let url = URL(string: strUrl)!
            webView.load(URLRequest(url: url))
            webView.allowsBackForwardNavigationGestures = true
            Globalfunc.hideLoaderView(view: self.view)

        }
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        
       
    }
    
    @IBAction func clickOnDownloadBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let account_id = self.passDict["account_id"] as! Int
        let filePath = self.passDict["filePath"] as! String
        let strUrl = "http://18.191.178.120:3000/ecabinet/\(account_id)/\(filePath)"
        let url = URL(string: strUrl)!
        let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
        
    }
    
}

extension pdfDocViewController:  URLSessionDownloadDelegate {
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("downloadLocation:", location)
        // create destination URL with the original pdf name
        guard let url = downloadTask.originalRequest?.url else { return }
        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
        // delete original copy
        try? FileManager.default.removeItem(at: destinationURL)
        // copy from temp to Document
        do {
            try FileManager.default.copyItem(at: location, to: destinationURL)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                let account_id = self.passDict["account_id"] as! Int
                let filePath = self.passDict["filePath"] as! String
                let strUrl = "http://18.191.178.120:3000/ecabinet/\(account_id)/\(filePath)"
                var url = URL(string: strUrl)!
                url = destinationURL
                let alertController = UIAlertController(title: Constant.AppName  , message: "Downloaded successfully!", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            }
        } catch let error {
            print("Copy Error: \(error.localizedDescription)")
        }
    }
}
