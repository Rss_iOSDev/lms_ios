
import UIKit
import SafariServices

class balancesViewController: UIViewController {
    
    @IBOutlet weak var lblbookeDate : UILabel!
    @IBOutlet weak var lblContractedDate : UILabel!
    @IBOutlet weak var lblLastMDate : UILabel!
    @IBOutlet weak var lblLastPayDate : UILabel!
    @IBOutlet weak var lblLAstTransDate : UILabel!
    @IBOutlet weak var lblCreationMthd : UILabel!
    @IBOutlet weak var lblContract : UILabel!
    @IBOutlet weak var lblLoanRate : UILabel!
    @IBOutlet weak var lblRez : UILabel!
    @IBOutlet weak var lblFrstPAymntDate : UILabel!
    @IBOutlet weak var lblMaturityDate : UILabel!
    
    
    //downpayment
    
    @IBOutlet weak var lblDownpay_Begbal : UILabel!
    @IBOutlet weak var lblDownpay_collected : UILabel!
    @IBOutlet weak var lblDownpay_adjusted : UILabel!
    @IBOutlet weak var lblDownpay_rebated : UILabel!
    @IBOutlet weak var lblDownpay_Endbal : UILabel!
    
    //intrest
    
    @IBOutlet weak var lblIntrst_Begbal : UILabel!
    @IBOutlet weak var lblIntrst_collected : UILabel!
    @IBOutlet weak var lblIntrst_adjusted : UILabel!
    @IBOutlet weak var lblIntrst_rebated : UILabel!
    @IBOutlet weak var lblIntrst_Endbal : UILabel!
    
    //lateFees
    
    @IBOutlet weak var lblLatefees_Begbal : UILabel!
    @IBOutlet weak var lblLatefees_collected : UILabel!
    @IBOutlet weak var lblLatefees_adjusted : UILabel!
    @IBOutlet weak var lblLatefees_rebated : UILabel!
    @IBOutlet weak var lblLatefees_Endbal : UILabel!
    
    //misc fees
    
    @IBOutlet weak var lblmiscFees_Begbal : UILabel!
    @IBOutlet weak var lblmiscFees_collected : UILabel!
    @IBOutlet weak var lblmiscFees_adjusted : UILabel!
    @IBOutlet weak var lblmiscFees_rebated : UILabel!
    @IBOutlet weak var lblmiscFees_Endbal : UILabel!
    
    //nonEarnPrin
    
    @IBOutlet weak var lblnonEarnPrin_Begbal : UILabel!
    @IBOutlet weak var lblnonEarnPrin_collected : UILabel!
    @IBOutlet weak var lblnonEarnPrin_adjusted : UILabel!
    @IBOutlet weak var lblnonEarnPrin_rebated : UILabel!
    @IBOutlet weak var lblnonEarnPrin_Endbal : UILabel!
    
    //prin
    
    @IBOutlet weak var lblprin_Begbal : UILabel!
    @IBOutlet weak var lblprin_collected : UILabel!
    @IBOutlet weak var lblprin_adjusted : UILabel!
    @IBOutlet weak var lblprin_rebated : UILabel!
    @IBOutlet weak var lblprin_Endbal : UILabel!
    
    //salesTax
    
    @IBOutlet weak var lblsalesTax_Begbal : UILabel!
    @IBOutlet weak var lblsalesTax_collected : UILabel!
    @IBOutlet weak var lblsalesTax_adjusted : UILabel!
    @IBOutlet weak var lblsalesTax_rebated : UILabel!
    @IBOutlet weak var lblsalesTax_Endbal : UILabel!
    
    //unEarnedInt
    
    @IBOutlet weak var lblunEarnedInt_Begbal : UILabel!
    @IBOutlet weak var lblunEarnedInt_collected : UILabel!
    @IBOutlet weak var lblunEarnedInt_adjusted : UILabel!
    @IBOutlet weak var lblunEarnedInt_rebated : UILabel!
    @IBOutlet weak var lblunEarnedInt_Endbal : UILabel!
    
    
    //total
    
    @IBOutlet weak var lbltotal_Begbal : UILabel!
    @IBOutlet weak var lbltotal_collected : UILabel!
    @IBOutlet weak var lbltotal_adjusted : UILabel!
    @IBOutlet weak var lbltotal_rebated : UILabel!
    @IBOutlet weak var lbltotal_Endbal : UILabel!
    
    
    @IBOutlet weak var viewUpdate : UIView!
    @IBOutlet weak var lblNmae : UILabel!
    @IBOutlet weak var lblActtType : UILabel!
    @IBOutlet weak var lblLoanNo : UILabel!
    @IBOutlet weak var lblAcctNo : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblLAstpromise : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    
    @IBOutlet weak var txtLaon : UITextField!
    @IBOutlet weak var txtBalPicker : UITextField!
    
    
    @IBOutlet weak var lblAcctNo_T : UILabel!
    
    @IBOutlet weak var lblPromise : UILabel!
    @IBOutlet weak var viewPromise : UIView!
    
    
    var arrBalType = [String]()
    var pv_loan: UIPickerView!
    var pv_balTyp: UIPickerView!
    
    var isPvSelected = ""
    
    
    @IBOutlet weak var lblBottomPayDate : UILabel!
    @IBOutlet weak var lblBottomFrquency : UILabel!
    @IBOutlet weak var lblBottomPaymnts : UILabel!
    @IBOutlet weak var lblBottomAmt : UILabel!
    @IBOutlet weak var lblBottomFinalPay : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewUpdate.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                print(dataD)
                
                let acctId = (dataD["_id"] as! Int)
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo_T.text = "Account No: \(account_no)"
                self.lblAcctNo.text = "Acct #: \(account_no)"
                
                
                let account_status = dataD["account_status"] as! String
                self.lblStatus.text = "Status: \(account_status)"
                
                let loanBorrower = dataD["loanBorrower"] as! NSDictionary
                let first_name = loanBorrower["first_name"] as! String
                let last_name = loanBorrower["last_name"] as! String
                self.lblNmae.text = "\(first_name) \(last_name)"
                
                let email = loanBorrower["email"] as! String
                self.lblEmail.text = "Email: \(email)"
                
                let loan_no = dataD["loan_no"] as! String
                self.lblLoanNo.text = "Loan #: \(loan_no)"
                
                let account_type = dataD["account_type"] as! String
                self.lblActtType.text = "Account Type: \(account_type)"
                
                
                
                
                
                if let promiseToPay = dataD["promiseToPay"] as? NSDictionary{
                    
                    let promiseAmount = promiseToPay["promiseAmount"] as! NSNumber
                    let promiseDate = promiseToPay["promiseDate"] as! String
                    if(promiseDate.isEmpty != true){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM-dd-yyyy"
                        let date = Date.dateFromISOString(string: promiseDate)
                        let strDate = formatter.string(from: date!)
                        
                        let promiseStatus = promiseToPay["promiseStatus"] as! String
                        if(promiseStatus == "Kept"){
                            viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            lblPromise.textColor = .white
                            lblPromise.text = "Kept Promise To Pay $\(promiseAmount) by \(strDate)"
                        }
                        else if(promiseStatus == "Open"){
                            viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            lblPromise.textColor = .white
                            lblPromise.text = "Open Promise To Pay $\(promiseAmount) by \(strDate)"
                        }
                        else if(promiseStatus == "Broken"){
                            viewPromise.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            lblPromise.textColor = .white
                            lblPromise.text = "Broken Promise To Pay $\(promiseAmount) by \(strDate)"
                        }


                    }
                }
                
                let balanceType = dataD["balanceType"] as! NSArray
                let balDict = balanceType[0] as! NSDictionary
                
                Globalfunc.print(object:balDict)
                
                let balId = balDict["_id"] as! Int
                let sideNoteType = balDict["sideNoteType"] as! Int
                
                for i in 0..<balanceType.count{
                    
                    let balDict = balanceType[i] as! NSDictionary
                    let description = balDict["description"] as! String
                    self.arrBalType.append(description)
                    
                }
                
                self.txtLaon.text = self.arrBalType[0]
                self.txtBalPicker.text = self.arrBalType[0]
                
                let Str_url = "\(Constant.balances)?account_id=\(acctId)&balance_type=\(balId)&sideNoteType=\(sideNoteType)"
                self.call_getBalanceDataApi(strUrl: Str_url)
            }
            //  http://18.191.178.120:3000/api/loan-account/balances?account_id=262&balance_type=1&sideNoteType=1
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONUpdateBtn(_ sender: UIButton)
    {
        if(sender.tag == 10)
        {
            self.viewUpdate.isHidden = false
        }
        else if(sender.tag == 20)
        {
            self.viewUpdate.isHidden = true
        }
    }
    
    @IBAction func clickOnLink(_ sender: UIButton)
    {
        if(sender.tag == 10)
        {
            if let url = URL(string: "http://18.191.178.120/viewer?id=checks") {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        }
        else  if(sender.tag == 20)
        {
            if let url = URL(string: "http://18.191.178.120/viewer?id=credit") {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        }
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//Api --
extension balancesViewController {
    
    func call_getBalanceDataApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? NSDictionary {
                            self.setDataonPage(dict: data)
                            
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
        
    }
    
    func setDataonPage(dict : NSDictionary)
    {
        print(dict)
        
        if let date_of_sale = dict["date_of_sale"] as? String
        {
            self.lblbookeDate.text = changeDateFormat(strDate: date_of_sale)
            self.lblContractedDate.text = changeDateFormatWithoutTime(strDate: date_of_sale)
        }
        
        if let firstPaymentDate = dict["firstPaymentDate"] as? String
        {
            self.lblFrstPAymntDate.text = changeDateFormatWithoutTime(strDate: firstPaymentDate)
            self.lblBottomPayDate.text = changeDateFormatWithoutTime(strDate: firstPaymentDate)
        }
        
        if let lastModified = dict["lastModified"] as? String
        {
            self.lblLastMDate.text = changeDateFormat(strDate: lastModified)
        }
        
        if let lastPaymentDate = dict["lastPaymentDate"] as? String
        {
            self.lblLastPayDate.text = changeDateFormat(strDate: lastPaymentDate)
        }
        
        if let lastTransaction = dict["lastTransaction"] as? String
        {
            self.lblLAstTransDate.text = changeDateFormat(strDate: lastTransaction)
        }
        
        if let loan_rate = dict["loan_rate"] as? NSNumber
        {
            self.lblLoanRate.text = "\(loan_rate)"
        }
        
        if let loan_reg_zapr = dict["loan_reg_zapr"] as? String
        {
            self.lblRez.text = loan_reg_zapr
        }
        
        if let maturity_date = dict["maturity_date"] as? String
        {
            self.lblMaturityDate.text = changeDateFormatWithoutTime(strDate: maturity_date)
        }
        
        if let transaction = dict["transaction"] as? NSDictionary
        {
            
            if let downPayment = transaction["downPayment"] as? NSDictionary
            {
                if let adjusted = downPayment["adjusted"] as? NSNumber{
                    self.lblDownpay_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = downPayment["begBal"] as? NSNumber
                {
                    if(begBal == 0){
                        self.lblDownpay_Begbal.text = ""
                    }else{
                        self.lblDownpay_Begbal.text = "$\(begBal)"
                    }
                }
                
                if let colleted = downPayment["collected"] as? NSNumber{
                    self.lblDownpay_collected.text = "$\(colleted)"
                }
                
                if let endBal = downPayment["endBal"] as? NSNumber{
                    self.lblDownpay_Endbal.text = "$\(endBal)"
                }
                
                if let rebated = downPayment["rebated"] as? NSNumber{
                    self.lblDownpay_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let interest = transaction["interest"] as? NSDictionary
            {
                
                if let adjusted = interest["adjusted"] as? NSNumber{
                    self.lblIntrst_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = interest["begBal"] as? NSNumber
                {
                    if(begBal == 0){
                        self.lblIntrst_Begbal.text = ""
                    }else{
                        self.lblIntrst_Begbal.text = "$\(begBal)"
                    }
                }
                
                
                if let colleted = interest["collected"] as? NSNumber{
                    
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblIntrst_collected.text = "$\(x)"
                    
                }
                
                
                if let endBal = interest["endBal"] as? NSNumber{
                    self.lblIntrst_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = interest["rebated"] as? NSNumber{
                    self.lblIntrst_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let lateFees = transaction["lateFees"] as? NSDictionary
            {
                if let adjusted = lateFees["adjusted"] as? NSNumber{
                    self.lblLatefees_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = lateFees["begBal"] as? NSNumber{
                    if(begBal == 0){
                        self.lblLatefees_Begbal.text = ""
                    }else{
                        self.lblLatefees_Begbal.text = "$\(begBal)"
                    }
                }
                
                
                if let colleted = lateFees["collected"] as? NSNumber{
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblLatefees_collected.text = "$\(x)"
                }
                
                
                if let endBal = lateFees["endBal"] as? NSNumber{
                    self.lblLatefees_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = lateFees["rebated"] as? NSNumber{
                    self.lblLatefees_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let miscFees = transaction["miscFees"] as? NSDictionary
            {
                if let adjusted = miscFees["adjusted"] as? NSNumber{
                    self.lblmiscFees_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = miscFees["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lblmiscFees_Begbal.text = ""
                    }else{
                        self.lblmiscFees_Begbal.text = "$\(begBal)"
                    }
                    
                }
                
                
                if let colleted = miscFees["collected"] as? NSNumber{
                    
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblmiscFees_collected.text = "$\(x)"
                    
                }
                
                
                if let endBal = miscFees["endBal"] as? NSNumber{
                    self.lblmiscFees_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = miscFees["rebated"] as? NSNumber{
                    self.lblmiscFees_rebated.text = "$\(rebated)"
                }
            }
            
            if let nonEarnPrin = transaction["nonEarnPrin"] as? NSDictionary
            {
                
                if let adjusted = nonEarnPrin["adjusted"] as? NSNumber{
                    self.lblnonEarnPrin_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = nonEarnPrin["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lblnonEarnPrin_Begbal.text = ""
                    }else{
                        self.lblnonEarnPrin_Begbal.text = "$\(begBal)"
                    }
                    
                }
                
                
                if let colleted = nonEarnPrin["collected"] as? NSNumber{
                    
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblnonEarnPrin_collected.text = "$\(x)"
                }
                
                
                if let endBal = nonEarnPrin["endBal"] as? NSNumber{
                    self.lblnonEarnPrin_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = nonEarnPrin["rebated"] as? NSNumber{
                    self.lblnonEarnPrin_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let prin = transaction["prin"] as? NSDictionary
            {
                
                if let adjusted = prin["adjusted"] as? NSNumber{
                    self.lblprin_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = prin["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lblprin_Begbal.text = ""
                    }else{
                        self.lblprin_Begbal.text = "$\(begBal)"
                    }
                    
                }
                
                
                if let colleted = prin["collected"] as? NSNumber{
                    
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblprin_collected.text = "$\(x)"
                    
                }
                
                
                if let endBal = prin["endBal"] as? NSNumber{
                    self.lblprin_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = prin["rebated"] as? NSNumber{
                    self.lblprin_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let salesTax = transaction["salesTax"] as? NSDictionary
            {
                if let adjusted = salesTax["adjusted"] as? NSNumber{
                    self.lblsalesTax_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = salesTax["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lblsalesTax_Begbal.text = ""
                    }else{
                        self.lblsalesTax_Begbal.text = "$\(begBal)"
                    }
                    
                }
                
                if let colleted = salesTax["collected"] as? NSNumber{
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblsalesTax_collected.text = "$\(x)"
                }
                
                if let endBal = salesTax["endBal"] as? NSNumber{
                    self.lblsalesTax_Endbal.text = "$\(endBal)"
                }
                
                if let rebated = salesTax["rebated"] as? NSNumber{
                    self.lblsalesTax_rebated.text = "$\(rebated)"
                }
            }
            
            if let unEarnedInt = transaction["unearned_interest"] as? NSDictionary
            {
                
                if let adjusted = unEarnedInt["adjusted"] as? NSNumber{
                    self.lblunEarnedInt_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = unEarnedInt["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lblunEarnedInt_Begbal.text = ""
                    }else{
                        self.lblunEarnedInt_Begbal.text = "$\(begBal)"
                    }
                }
                
                
                if let colleted = unEarnedInt["collected"] as? NSNumber{
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lblunEarnedInt_collected.text = "$\(x)"
                }
                
                
                if let endBal = unEarnedInt["endBal"] as? NSNumber{
                    self.lblunEarnedInt_Endbal.text = "$\(endBal)"
                }
                
                
                if let rebated = unEarnedInt["rebated"] as? NSNumber{
                    self.lblunEarnedInt_rebated.text = "$\(rebated)"
                }
                
            }
            
            if let totals = transaction["totals"] as? NSDictionary
            {
                
                if let adjusted = totals["adjusted"] as? NSNumber{
                    self.lbltotal_adjusted.text = "$\(adjusted)"
                }
                
                if let begBal = totals["begBal"] as? NSNumber{
                    
                    if(begBal == 0){
                        self.lbltotal_Begbal.text = ""
                    }else{
                        self.lbltotal_Begbal.text = "$\(begBal)"
                    }
                    
                }
                
                if let colleted = totals["collected"] as? NSNumber{
                    let x = Double(truncating: colleted).rounded(toPlaces: 3)
                    self.lbltotal_collected.text = "$\(x)"
                }
                
                if let endBal = totals["endBal"] as? NSNumber{
                    self.lbltotal_Endbal.text = "$\(endBal)"
                }
                
                if let rebated = totals["rebated"] as? NSNumber{
                    self.lbltotal_rebated.text = "$\(rebated)"
                }
            }
        }
        
        if let payment_term = dict["payment_term"] as? NSNumber
        {
            self.lblBottomPaymnts.text = "\(payment_term)"
        }
        
        
        if let payment_frequency = dict["payment_frequency"] as? String
        {
            if(payment_frequency == "M"){
                self.lblBottomFrquency.text = "Monthly"
            }
            else if(payment_frequency == "W"){
                self.lblBottomFrquency.text = "Weekly"
            }
            else if(payment_frequency == "B"){
                self.lblBottomFrquency.text = "Bi-Weekly"
            }
        }
        
        if let firstPaymentAmount = dict["firstPaymentAmount"] as? NSNumber
        {
            self.lblBottomAmt.text = "$\(firstPaymentAmount)"
        }
        
        if let lastPaymentAmount = dict["lastPaymentAmount"] as? NSNumber
        {
            self.lblBottomFinalPay.text = "$\(lastPaymentAmount)"
        }
    }
    
    
    func changeDateFormat(strDate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy hh:mm a"
        let date = Date.dateFromISOString(string: strDate)
        let  createFormat = formatter.string(from: date!)
        
        return createFormat
    }
    
    func changeDateFormatWithoutTime(strDate : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let date = Date.dateFromISOString(string: strDate)
        let  createFormat = formatter.string(from: date!)
        return createFormat
    }
}

extension balancesViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pv_loan{
            return self.arrBalType.count
        }
        else if (pickerView == pv_balTyp){
            return self.arrBalType.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pv_loan{
            let strTitle = arrBalType[row]
            return strTitle
        }
        else if pickerView == pv_balTyp{
            let strTitle = arrBalType[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if pickerView == pv_loan{
            self.txtLaon.text = arrBalType[row]
        }
        
        if pickerView == pv_balTyp{
            self.txtBalPicker.text = arrBalType[row]
            self.txtBalPicker.tag = row
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtLaon){
            self.pickUp(txtLaon)
        }
        if(textField == self.txtBalPicker){
            self.pickUp(txtBalPicker)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtLaon){
            self.pv_loan = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_loan.delegate = self
            self.pv_loan.dataSource = self
            self.pv_loan.backgroundColor = UIColor.white
            textField.inputView = self.pv_loan
            
            isPvSelected = "loan"
        }
        else if(textField == self.txtBalPicker){
            self.pv_balTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_balTyp.delegate = self
            self.pv_balTyp.dataSource = self
            self.pv_balTyp.backgroundColor = UIColor.white
            textField.inputView = self.pv_balTyp
            
            isPvSelected = "bal"
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        if(isPvSelected == "bal"){
            txtBalPicker.resignFirstResponder()
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let acctId = (dataD["_id"] as! Int)
                    
                    let balanceType = dataD["balanceType"] as! NSArray
                    let balDict = balanceType[ self.txtBalPicker.tag] as! NSDictionary
                    
                    Globalfunc.print(object:balDict)
                    
                    let balId = balDict["_id"] as! Int
                    let sideNoteType = balDict["sideNoteType"] as! Int
                    let Str_url = "\(Constant.balances)?account_id=\(acctId)&balance_type=\(balId)&sideNoteType=\(sideNoteType)"
                    self.call_getBalanceDataApi(strUrl: Str_url)
                }
            }
            catch{
                Globalfunc.print(object: "Couldn't read file.")
            }
        }
        else{
            txtLaon.resignFirstResponder()
        }
    }
    
    @objc func cancelClick() {
        txtLaon.resignFirstResponder()
        txtBalPicker.resignFirstResponder()
    }
}


extension balancesViewController {
    
    @IBAction func clickONEmailBtn(_ sender : UIButton)
    {
        let alertController = UIAlertController(title: "Add New Name", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Email Address"
        }
        let saveAction = UIAlertAction(title: "Update", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            Globalfunc.showLoaderView(view: self.view)
            self.updateemailApi(firstTextField.text!)
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    func updateemailApi(_ strText : String)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSMutableDictionary{
                
                
                let loanBorrower = dataD["loanBorrower"] as! NSMutableDictionary
                loanBorrower.setValue(strText, forKey: "email")
                dataD.setValue(loanBorrower, forKey: "loanBorrower")
                
                print(dataD)
                
                do {
                    if #available(iOS 11.0, *) {
                        let myData = try NSKeyedArchiver.archivedData(withRootObject: dataD, requiringSecureCoding: false)
                        userDef.set(myData, forKey: "dataDict")
                    } else {
                        let myData = NSKeyedArchiver.archivedData(withRootObject: dataD)
                        userDef.set(myData, forKey: "dataDict")
                    }
                } catch {
                    Globalfunc.print(object: "Couldn't write file")
                }
                
                
                let accId_selected = dataD["_id"] as! Int
                let params = [
                    "email": strText,
                    "id": accId_selected] as [String : Any]
                self.sendDatatoPostApi(param: params)
            }
        }
        catch{
        }
    }
    
    
    func sendDatatoPostApi( param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.detail_contact_info, controller: self, parms: param) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
