//
//  repoHistoryVC.swift
//  LMS
//
//  Created by Apple on 30/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class repoHistoryVC: UIViewController {
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tblRepo: UITableView!
    
    var arrRepoList : NSMutableArray = []
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.text = ""
        self.tblRepo.isHidden = true
        
        self.tblRepo.tableFooterView = UIView()
        self.call_getRepoHistoryApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}


//Api --
extension repoHistoryVC {
    
    func call_getRepoHistoryApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"

                
                let Str_url = "\(Constant.repo_url)?account_id=\(accId_selected)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object:dict)
                            
                            //let arrTitle  = ["","","",""]
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrRepoList.add(dictA)
                                        }
                                        self.tblRepo.isHidden = false
                                        self.lblStatus.text = "REPO HISTORY"
                                        self.tblRepo.reloadData()
                                    }
                                    else{
                                        self.tblRepo.isHidden = true
                                        self.lblStatus.text = "REPO HISTORY \n \n No Records found"
                                        
                                    }
                                }
                            }
                            

                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension repoHistoryVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRepoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblRepo.dequeueReusableCell(withIdentifier: "tblRepoCell") as! tblRepoCell
        let dict = arrRepoList[indexPath.row] as! NSDictionary
        
        if let repoNotes = dict["repoNotes"] as? String
        {
            cell.lblrepoNotes.text = repoNotes
        }else{
            if let repoCancelNotes = dict["repoCancelNotes"] as? String{
                cell.lblrepoNotes.text = "Cancel repo - \(repoCancelNotes)"
            }
        }

        if let created_at = dict["created_at"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: created_at)
            cell.lblPlaceout.text = formatter.string(from: date!)
        }
        
        if let effectiveDate = dict["effectiveDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: effectiveDate)
            cell.lblEffectDate.text = formatter.string(from: date!)
        }
        
        if let repoAgent = dict["repoAgent"] as? NSDictionary{
            
            if let title = repoAgent["title"] as? String{
                cell.lblrepoAgent.text = title
            }
            
        }
        
        if let repoReason = dict["repoReason"] as? String{
            cell.lblReporeson.text = repoReason
        }
        
        
        if let repoStatus = dict["repoStatus"] as? NSDictionary{
            if let title = repoStatus["title"] as? String{
                cell.lblrepoSatus.text = title
                if(title == "Out For Repo"){
                    let repoNotes = dict["repoNotes"] as! String
                    cell.lblrepoNotes.text = "Placed out for repo - \(repoNotes)"
                }
            }
        }
        else{
            cell.lblrepoSatus.text = "Cancelled"
        }
        
        return cell
    }
    
    
    
}

class tblRepoCell : UITableViewCell
{
    @IBOutlet weak var lblPlaceout : UILabel!
    @IBOutlet weak var lblEffectDate : UILabel!
    @IBOutlet weak var lblReporeson : UILabel!
    @IBOutlet weak var lblrepoAgent : UILabel!
    @IBOutlet weak var lblrepoSatus : UILabel!
    @IBOutlet weak var lblrepoNotes : UILabel!
}
