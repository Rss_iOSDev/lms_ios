//
//  userDefineViewController.swift
//  LMS
//
//  Created by Apple on 25/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class userDefineViewController: UIViewController {
    
    @IBOutlet weak var lblAcctNo : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.call_getUSerfediHistoryApi()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }


}

extension userDefineViewController {
    
    func call_getUSerfediHistoryApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"
                

            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}
