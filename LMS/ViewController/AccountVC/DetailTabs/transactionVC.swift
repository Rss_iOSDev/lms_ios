//
//  transactionVC.swift
//  LMS
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class transactionVC: UIViewController {
    
    @IBOutlet weak var lblTotalRecords : UILabel!
    @IBOutlet weak var tblTransactn : UITableView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrTransactionList : [NSDictionary] = []
    
    var selectedCell = [IndexPath]()
    
    var currentPgNo = 1
    var defRecordPAge = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        tblTransactn.estimatedRowHeight = 250
        self.callTransactionListByPost()
    }
    
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

}

extension transactionVC {
    
    func callTransactionListByPost()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"

                
                let param = ["currentPageNumber": self.currentPgNo, //act_id dynamic
                    "defaultRecordsPage": self.defRecordPAge,
                    "account_id": accId_selected] as [String : Any]
                
                
                BaseApi.onResponsePostWithToken(url: Constant.transac_url, controller: self, parms: param) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                                  Globalfunc.print(object:dict)
                            
                            if let responseDict = dict as? NSDictionary{
                                
                                if let data = responseDict["data"] as? NSDictionary{
                                    
                                    self.currentPgNo = data["currentPageNumber"] as! Int
                                    self.defRecordPAge = data["defaultRecordsPage"] as! Int
                                    
                                    let totalrecord = data["totalrecord"] as! Int
                                    self.lblTotalRecords.text = "\(totalrecord) TRANSACTION HISTORY RECORD"
                                    
                                    if let arrTrans = data["transaction"] as? [NSDictionary] {
                                        if(arrTrans.count > 0){
                                            for i in 0..<arrTrans.count{
                                                let dictA = arrTrans[i]
                                                self.arrTransactionList.append(dictA)
                                            }
                                            self.tblTransactn.reloadData()
                                        }
                                        else{
                                            self.arrTransactionList = []
                                            self.tblTransactn.reloadData()
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
    }
}

extension transactionVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTransactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblTransactn.dequeueReusableCell(withIdentifier: "snapTblTrans") as! snapTblTrans
        
        let dict = self.arrTransactionList[indexPath.row]
        Globalfunc.print(object:dict)
        
        let first_name = dict["first_name"] as! String
        let last_name = dict["last_name"] as! String
        cell.lblPostedBty.text = "Posted By: \(first_name) \(last_name)"
        
        if let postDate = dict["postDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: postDate)
            let createFormat = formatter.string(from: date!)
            cell.lblPostDate.text = "Post Date : \(createFormat)"
        }
        
        if let effectiveDate = dict["effectiveDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: effectiveDate)
            let createFormat = formatter.string(from: date!)
            cell.lblEffctDate.text = "Effective Date : \(createFormat)"
        }
        
        if let begAcctBal = dict["begAcctBal"] as? NSNumber
        {
            cell.lblBeg_accBal.text = "Beg Acct Balance : $\(begAcctBal)"
        }
        
        if let interest = dict["interest"] as? NSDictionary
        {
            let colleted = interest["colleted"] as? NSNumber
            cell.lblCollected.text = "Collected : $\(colleted!)"
        }
        
        if let adjusted = dict["adjusted"] as? NSNumber
        {
            cell.lblAdjusted.text = "Adjusted : $\(adjusted)"
        }
        
        if let endAcctBal = dict["endAcctBal"] as? NSNumber
        {
            cell.lblEnd_accBal.text = "End Acct Balance : $\(endAcctBal)"
        }
        
        if let transDesc = dict["transDesc"] as? String
        {
            cell.lblPymntMthd.text = "Pmt Method Trans Desc : \(transDesc)"
        }
        
        if let comments = dict["comments"] as? String
        {
            cell.lblComments.text = "Comments Ref : \(comments)"
        }
        
        
        if selectedCell.contains(indexPath) {
            cell.viewExpand.isHidden = false
        }
        else {
            cell.viewExpand.isHidden = true
            
        }
        
        if let downPayment = dict["downPayment"] as? NSDictionary
        {
            let adjusted = downPayment["adjusted"] as? NSNumber
            cell.lblDownpay_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = downPayment["begBal"] as? NSNumber
            cell.lblDownpay_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = downPayment["colleted"] as? NSNumber
            cell.lblDownpay_collected.text = "Collected : $\(colleted!)"
            
            let endBal = downPayment["endBal"] as? NSNumber
            cell.lblDownpay_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = downPayment["rebated"] as? NSNumber
            cell.lblDownpay_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let interest = dict["interest"] as? NSDictionary
        {
            let adjusted = interest["adjusted"] as? NSNumber
            cell.lblIntrst_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = interest["begBal"] as? NSNumber
            cell.lblIntrst_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = interest["colleted"] as? NSNumber
            cell.lblIntrst_collected.text = "Collected : $\(colleted!)"
            
            let endBal = interest["endBal"] as? NSNumber
            cell.lblIntrst_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = interest["rebated"] as? NSNumber
            cell.lblIntrst_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let lateFees = dict["lateFees"] as? NSDictionary
        {
            let adjusted = lateFees["adjusted"] as? NSNumber
            cell.lblLatefees_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = lateFees["begBal"] as? NSNumber
            cell.lblLatefees_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = lateFees["colleted"] as? NSNumber
            cell.lblLatefees_collected.text = "Collected : $\(colleted!)"
            
            let endBal = lateFees["endBal"] as? NSNumber
            cell.lblLatefees_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = lateFees["rebated"] as? NSNumber
            cell.lblLatefees_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let miscFees = dict["miscFees"] as? NSDictionary
        {
            let adjusted = miscFees["adjusted"] as? NSNumber
            cell.lblmiscFees_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = miscFees["begBal"] as? NSNumber
            cell.lblmiscFees_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = miscFees["colleted"] as? NSNumber
            cell.lblmiscFees_collected.text = "Collected : $\(colleted!)"
            
            let endBal = miscFees["endBal"] as? NSNumber
            cell.lblmiscFees_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = miscFees["rebated"] as? NSNumber
            cell.lblmiscFees_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let nonEarnPrin = dict["nonEarnPrin"] as? NSDictionary
        {
            let adjusted = nonEarnPrin["adjusted"] as? NSNumber
            cell.lblnonEarnPrin_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = nonEarnPrin["begBal"] as? NSNumber
            cell.lblnonEarnPrin_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = nonEarnPrin["colleted"] as? NSNumber
            cell.lblnonEarnPrin_collected.text = "Collected : $\(colleted!)"
            
            let endBal = nonEarnPrin["endBal"] as? NSNumber
            cell.lblnonEarnPrin_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = nonEarnPrin["rebated"] as? NSNumber
            cell.lblnonEarnPrin_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let prin = dict["prin"] as? NSDictionary
        {
            let adjusted = prin["adjusted"] as? NSNumber
            cell.lblprin_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = prin["begBal"] as? NSNumber
            cell.lblprin_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = prin["colleted"] as? NSNumber
            cell.lblprin_collected.text = "Collected : $\(colleted!)"
            
            let endBal = prin["endBal"] as? NSNumber
            cell.lblprin_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = prin["rebated"] as? NSNumber
            cell.lblprin_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let salesTax = dict["prin"] as? NSDictionary
        {
            let adjusted = salesTax["adjusted"] as? NSNumber
            cell.lblsalesTax_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = salesTax["begBal"] as? NSNumber
            cell.lblsalesTax_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = salesTax["colleted"] as? NSNumber
            cell.lblsalesTax_collected.text = "Collected : $\(colleted!)"
            
            let endBal = salesTax["endBal"] as? NSNumber
            cell.lblsalesTax_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = salesTax["rebated"] as? NSNumber
            cell.lblsalesTax_rebated.text = "Rebated : $\(rebated!)"
        }
        
        if let unEarnedInt = dict["prin"] as? NSDictionary
        {
            let adjusted = unEarnedInt["adjusted"] as? NSNumber
            cell.lblunEarnedInt_adjusted.text = "Adjusted : $\(adjusted!)"
            
            let begBal = unEarnedInt["begBal"] as? NSNumber
            cell.lblunEarnedInt_Begbal.text = "Beg Bal : $\(begBal!)"
            
            let colleted = unEarnedInt["colleted"] as? NSNumber
            cell.lblunEarnedInt_collected.text = "Collected : $\(colleted!)"
            
            let endBal = unEarnedInt["endBal"] as? NSNumber
            cell.lblunEarnedInt_Endbal.text = "End Bal : $\(endBal!)"
            
            let rebated = unEarnedInt["rebated"] as? NSNumber
            cell.lblunEarnedInt_rebated.text = "Rebated : $\(rebated!)"
        }
        
        cell.btnReceipt.tag = indexPath.row
        cell.btnReceipt.addTarget(self, action: #selector(clickonReceiptBtn(_:)), for: .touchUpInside)
        
        cell.btnEmail.tag = indexPath.row
        cell.btnEmail.addTarget(self, action: #selector(clickonEmailBtn(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tblTransactn.cellForRow(at: indexPath) as! snapTblTrans
        
        
        if selectedCell.contains(indexPath) {
            
            selectedCell.remove(at: selectedCell.index(of: indexPath)!)
            cell.viewExpand.isHidden = true
            tblTransactn.reloadData()
            // tblTransactn.estimatedRowHeight = 250
            //tblTransactn.rowHeight = cell.viewMain.frame.height + 10
        }
        else{
            selectedCell.append(indexPath)
            cell.viewExpand.isHidden = false
            tblTransactn.reloadData()
            // tblTransactn.rowHeight = cell.viewExpand.frame.height + cell.viewMain.frame.height + 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCell.contains(indexPath) {
            return 740
        }
        else{
            return 245
        }
    }
    
    @objc func clickonReceiptBtn(_ sender: UIButton)
    {
        let dict = self.arrTransactionList[sender.tag]
        let _id = dict["_id"] as! Int
        transactionId = _id
        isReceiptTyp = "transaction"
        isEmailAttach = "false"
        self.presentViewControllerasPopup("receiptViewController", strStoryName: "colletralStory")
    }
    
    @objc func clickonEmailBtn(_ sender: UIButton)
    {
        userDef.removeObject(forKey: "attach_receipt")
        let dict = self.arrTransactionList[sender.tag]
        let _id = dict["_id"] as! Int
        transactionId = _id
        isReceiptTyp = "transaction"
        isEmailAttach = "true"
        
        let story = UIStoryboard.init(name: "colletralStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "receiptViewController") as! receiptViewController
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true,completion: nil)

    }
}


