//
//  collPaymntVC.swift
//  LMS
//
//  Created by Apple on 15/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import MapKit


class collPaymntVC: UIViewController {
    
    @IBOutlet weak var txtRecoveryTyp : UITextField!
    @IBOutlet weak var txtRecoveryFunc : UITextField!
    @IBOutlet weak var txtRecoveryEsn : UITextField!
    
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblAsof : UILabel!
    @IBOutlet weak var lblPosition : UILabel!
    @IBOutlet weak var lblLocation : UILabel!
    
    @IBOutlet weak var lbltitle1 : UILabel!
    @IBOutlet weak var lbltitle2 : UILabel!
    @IBOutlet weak var lbltitle3 : UILabel!
    @IBOutlet weak var lbltitle4 : UILabel!
    
    
    @IBOutlet weak var switchStatus : UISwitch!
    @IBOutlet weak var viewMap : MKMapView!
    
    var strter_status = false
    
    var pickerRecTyp: UIPickerView!
    var recover_typ_id = ""
    
    var pickerRecFunc: UIPickerView!
    var recover_func_id = ""
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switchStatus.addTarget(self, action: #selector(stateChanged), for: .valueChanged)
        
        self.switchStatus.isHidden = true
        self.viewMap.isHidden = true
        
        self.lblStatus.isHidden = true
        self.lblAsof.isHidden = true
        self.lblPosition.isHidden = true
        self.lblLocation.isHidden = true
        self.lbltitle1.isHidden = true
        self.lbltitle2.isHidden = true
        self.lbltitle3.isHidden = true
        self.lbltitle4.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        self.setApi()
    }
    
    @objc func stateChanged(switchState: UISwitch){
        if switchState.isOn {
            strter_status = true
        } else {
            strter_status = false
        }
        let params = ["starter": self.strter_status,
                      "recoveryESN":"\(self.txtRecoveryEsn.text!)"] as [String : Any]
        self.callUpdateApi(param: params, strTyp: "post")
    }
    
    func setApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let accId_selected = dataD["_id"] as! Int
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"
                let strGetUrl = "\(Constant.Coll_paymnt)?id=\(accId_selected)"
                self.callApiToSetData(strUrl: strGetUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONLocateBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        self.setApi()
    }
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let params = ["recoveryType":self.recover_typ_id,
                              "recoveryFunctionality":self.recover_func_id,
                              "id":accId_selected,
                              "recoveryESN":"\(self.txtRecoveryEsn.text!)"] as [String : Any]
                self.callUpdateApi(param: params, strTyp: "put")
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
}


extension collPaymntVC {
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object: dict)
                    if let arr = dict as? NSDictionary {
                        
                        if let dictP = arr["data"] as? NSDictionary{
                            
                            let recoveryESN = dictP["recoveryESN"] as! String
                            self.txtRecoveryEsn.text = recoveryESN
                            
                            if let recoveryFunctionality = dictP["recoveryFunctionality"] as? String
                            {
                                for i in 0..<consArrays.arrRecoveryFunc.count{
                                    let dict = consArrays.arrRecoveryFunc[i]
                                    let id = dict["id"]!
                                    if(recoveryFunctionality == id){
                                        let strTitle = dict["title"]!
                                        self.recover_func_id = id
                                        self.txtRecoveryFunc.text = strTitle
                                        break
                                    }
                                }
                            }
                            if let recoveryType = dictP["recoveryType"] as? String
                            {
                                for i in 0..<consArrays.arrRecoveryTyp.count{
                                    let dict = consArrays.arrRecoveryTyp[i]
                                    let id = dict["id"]!
                                    if(recoveryType == id){
                                        let strTitle = dict["title"]!
                                        self.recover_typ_id = id
                                        self.txtRecoveryTyp.text = strTitle
                                        break
                                    }
                                }
                            }
                            let starter = dictP["starter"] != nil
                            if(starter){
                                
                                self.switchStatus.isHidden = false
                                self.viewMap.isHidden = false
                                
                                self.lblStatus.isHidden = false
                                self.lblAsof.isHidden = false
                                self.lblPosition.isHidden = false
                                self.lblLocation.isHidden = false
                                
                                self.lbltitle1.isHidden = false
                                self.lbltitle2.isHidden = false
                                self.lbltitle3.isHidden = false
                                self.lbltitle4.isHidden = false
                                
                                
                                
                                let loc = dictP["location"] as! String
                                self.lblLocation.text = loc
                                
                                let position = dictP["position"] as! String
                                self.lblPosition.text = position
                                
                                let status = dictP["status"] as! String
                                self.lblStatus.text = status
                                
                                if let asOf = dictP["asOf"] as? String
                                {
                                    self.lblAsof.text = "\(asOf) (CDT)"
                                }
                                
                                if let strtr = dictP["starter"] as? Bool{
                                    self.strter_status = strtr
                                    self.switchStatus.isOn = strtr
                                }
                                
                                let lat = dictP["latitude"] as! NSNumber
                                let long = dictP["longitude"] as! NSNumber
                                
                                let latDouble = Double(truncating: lat)
                                let longDouble = Double(truncating: long)
                                
                                
                                let location = CLLocationCoordinate2D(latitude: latDouble,
                                                                      longitude: longDouble)
                                
                                // 2
                                let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
                                let region = MKCoordinateRegion(center: location, span: span)
                                self.viewMap.setRegion(region, animated: true)
                                
                                //3
                                let annotation = MKPointAnnotation()
                                annotation.coordinate = location
                                annotation.title = "Houston"
                                self.viewMap.addAnnotation(annotation)
                            }
                            else{
                                self.switchStatus.isHidden = true
                                self.viewMap.isHidden = true
                                self.lblStatus.isHidden = true
                                self.lblAsof.isHidden = true
                                self.lblPosition.isHidden = true
                                self.lblLocation.isHidden = true
                                
                                self.lbltitle1.isHidden = true
                                self.lbltitle2.isHidden = true
                                self.lbltitle3.isHidden = true
                                self.lbltitle4.isHidden = true
                                
                                
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


//?id=249

extension collPaymntVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerRecTyp){
            return consArrays.arrRecoveryTyp.count
        }
        else if(pickerView == pickerRecFunc){
            return consArrays.arrRecoveryFunc.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerRecTyp){
            let dict = consArrays.arrRecoveryTyp[row]
            let strTitle = dict["title"]!
            return strTitle
        }
        else if(pickerView == pickerRecFunc){
            let dict = consArrays.arrRecoveryFunc[row]
            let strTitle = dict["title"]!
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerRecTyp){
            
            let dict = consArrays.arrRecoveryTyp[row]
            let strTitle = dict["title"]!
            let id = dict["id"]!
            self.recover_typ_id = id
            self.txtRecoveryTyp.text = strTitle
        }
        else if(pickerView == pickerRecFunc){
            let dict = consArrays.arrRecoveryFunc[row]
            let strTitle = dict["title"]!
            let id = dict["id"]!
            self.recover_func_id = id
            self.txtRecoveryFunc.text = strTitle
        }
        
        
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtRecoveryTyp){
            self.pickUp(txtRecoveryTyp)
        }
        else if(textField == self.txtRecoveryFunc){
            self.pickUp(txtRecoveryFunc)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtRecoveryTyp){
            
            self.pickerRecTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRecTyp.delegate = self
            self.pickerRecTyp.dataSource = self
            self.pickerRecTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerRecTyp
        }
        else if(textField == self.txtRecoveryFunc){
            
            self.pickerRecFunc = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRecFunc.delegate = self
            self.pickerRecFunc.dataSource = self
            self.pickerRecFunc.backgroundColor = UIColor.white
            textField.inputView = self.pickerRecFunc
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtRecoveryTyp.resignFirstResponder()
        txtRecoveryFunc.resignFirstResponder()
        
    }
}

extension collPaymntVC {
    
    func callUpdateApi(param: [String : Any], strTyp: String){
        
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.Coll_paymnt, controller: self, parms: param as NSDictionary) { (dict, error) in
                OperationQueue.main.addOperation {
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    
                }
                
            }
        }
        else if(strTyp == "post"){
            
            BaseApi.onResponsePostWithToken(url: Constant.Coll_paymnt, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
    }
}
