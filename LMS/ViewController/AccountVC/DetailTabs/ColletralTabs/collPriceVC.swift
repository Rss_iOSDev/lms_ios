//
//  collPriceVC.swift
//  LMS
//
//  Created by Apple on 15/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class collPriceVC: UIViewController {
    
    
    @IBOutlet weak var txtPurchaseDate : UITextField!
    @IBOutlet weak var txtPrimaryLot : UITextField!
    @IBOutlet weak var txtPAckFee : UITextField!
    @IBOutlet weak var txtVehCost : UITextField!
    @IBOutlet weak var lblVehExp : UILabel!
    @IBOutlet weak var lblTotalCost : UILabel!
    
    var pickerVehLot: UIPickerView!
    var pickerPurchseDate : UIDatePicker!
    
    var id_promarylot = ""
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setApi()
    }
    
    func setApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"

                let strGetUrl = "\(Constant.Coll_pricing)?id=\(accId_selected)"
                self.callApiToSetData(strUrl : strGetUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                let accId_selected = dataD["_id"] as! Int
                let params = ["id":accId_selected,
                              "packFee":"\(self.txtPAckFee.text!)",
                    "primaryLot":id_promarylot,
                    "purchaseDate":"\(self.txtPurchaseDate.text!)",
                    "vehicleCost":"\(self.txtVehCost.text!)"] as [String : Any]
                
                
                BaseApi.onResponsePutWithToken(url: Constant.Coll_pricing, controller: self, parms: params as NSDictionary) { (dict, error) in
                    
                    OperationQueue.main.addOperation {
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        
                    }
                    
                }
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONCancelBtn(_ sender: UIButton)
    {
        self.txtPurchaseDate.text = ""
        self.txtVehCost.text = ""
        self.txtPAckFee.text = ""
        self.txtPrimaryLot.text = ""
    }
}

extension collPriceVC {
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object: dict)
                    
                    if let arr = dict as? NSDictionary {
                        if let dictP = arr["data"] as? NSDictionary{
                            
                            
                            if let _ = dictP["autoCheck"] as? String
                            {
                                
                            }
                            
                            if let packFee = dictP["packFee"] as? String
                            {
                                self.txtPAckFee.text = packFee
                            }
                            
                            if let purchaseDate = dictP["purchaseDate"] as? String
                            {
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM/dd/yyyy"
                                let date = Date.dateFromISOString(string: purchaseDate)
                                self.txtPurchaseDate.text = formatter.string(from: date!)
                                
                            }
                            
                            if let primaryLot = dictP["primaryLot"] as? String
                            {
                                self.id_promarylot = primaryLot
                                
                                if(primaryLot == "1"){
                                    let dict = consArrays.arrDefaultBranch[0] as! NSDictionary
                                    let item = dict["title"] as! String
                                    self.txtPrimaryLot.text = item
                                    
                                }
                                else if(primaryLot == "2"){
                                    let dict = consArrays.arrDefaultBranch[1] as! NSDictionary
                                    let item = dict["title"] as! String
                                    self.txtPrimaryLot.text = item
                                }
                                
                            }
                            
                            if let totalCostInCar = dictP["totalCostInCar"] as? String
                            {
                                self.lblTotalCost.text = totalCostInCar
                            }
                            
                            if let _ = dictP["valuations"] as? String
                            {
                                
                            }
                            if let vehicleCost = dictP["vehicleCost"] as? String
                            {
                                self.txtVehCost.text = vehicleCost
                            }
                            if let vehicleExpenses = dictP["vehicleExpenses"] as? String
                            {
                                self.lblVehExp.text = vehicleExpenses
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}



extension collPriceVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerVehLot){
            return consArrays.arrDefaultBranch.count
        }
        
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            
            let item = dict["title"] as! String
            self.txtPrimaryLot.text = item
            
            let _id = dict["_id"] as! Int
            id_promarylot = "\(_id)"
        }
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        
        if(textField == self.txtPurchaseDate){
            self.pickUpDateTime(txtPurchaseDate)
        }
            
        else if(textField == self.txtPrimaryLot){
            self.pickUp(txtPrimaryLot)
        }
        
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtPurchaseDate){
            
            self.pickerPurchseDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPurchseDate.datePickerMode = .date
            self.pickerPurchseDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerPurchseDate
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtPrimaryLot){
            self.pickerVehLot = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerVehLot.delegate = self
            self.pickerVehLot.dataSource = self
            self.pickerVehLot.backgroundColor = UIColor.white
            textField.inputView = self.pickerVehLot
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtPurchaseDate.text = formatter.string(from: pickerPurchseDate.date)
        self.txtPurchaseDate.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        
        txtPurchaseDate.text = ""
        txtPurchaseDate.resignFirstResponder()
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtPrimaryLot.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtPrimaryLot.resignFirstResponder()
        
    }
}
