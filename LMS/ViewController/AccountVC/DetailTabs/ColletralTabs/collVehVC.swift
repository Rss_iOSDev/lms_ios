

import UIKit

class collVehVC: UIViewController {
    
    var arrUrl = [Constant.state,Constant.country,Constant.default_branch_url]
    
    @IBOutlet weak var txtVehc_lot : UITextField!
    @IBOutlet weak var txtMileg : UITextField!
    @IBOutlet weak var txtMileg_status : UITextField!
    
    @IBOutlet weak var lblVin : UILabel!
    @IBOutlet weak var txtYear : UITextField!
    @IBOutlet weak var txtModel : UITextField!
    @IBOutlet weak var txtBodyStyle : UITextField!
    @IBOutlet weak var txtExtcolor : UITextField!
    @IBOutlet weak var txtEngine : UITextField!
    @IBOutlet weak var txtFuel : UITextField!
    @IBOutlet weak var txtcondition : UITextField!
    @IBOutlet weak var txtnew_use : UITextField!
    @IBOutlet weak var txtMake : UITextField!
    @IBOutlet weak var txtTrim : UITextField!
    @IBOutlet weak var txtTransmisn : UITextField!
    @IBOutlet weak var txtIntTrim : UITextField!
    @IBOutlet weak var txtDrivTyp : UITextField!
    @IBOutlet weak var txtWeight : UITextField!
    @IBOutlet weak var txtTyp : UITextField!
    @IBOutlet weak var btnYear : UIButton!
    @IBOutlet weak var btnModel : UIButton!
    @IBOutlet weak var btnBodystyl : UIButton!
    @IBOutlet weak var btnExtcolor : UIButton!
    @IBOutlet weak var btnEngine : UIButton!
    @IBOutlet weak var btnFuel : UIButton!
    @IBOutlet weak var btnMake : UIButton!
    @IBOutlet weak var btnTrim : UIButton!
    @IBOutlet weak var btnTransmisn : UIButton!
    @IBOutlet weak var btnIntTrim : UIButton!
    @IBOutlet weak var btnDrivTyp : UIButton!
    
    @IBOutlet weak var btnInspected : UIButton!
    
    @IBOutlet weak var txtExtPrimColor : UITextField!
    @IBOutlet weak var txtExtSecColor : UITextField!
    
    @IBOutlet weak var txtUserdefin : UITextField!
    @IBOutlet weak var txtPlateNo : UITextField!
    
    @IBOutlet weak var txtPExpireMnth : UITextField!
    @IBOutlet weak var txtPExpireYr : UITextField!
    
    @IBOutlet weak var txtIExpireMnth : UITextField!
    @IBOutlet weak var txtIExpireYr : UITextField!
    
    @IBOutlet weak var txtVan : UITextField!
    @IBOutlet weak var txtEmmison : UITextField!
    var InspectdBy = false
    @IBOutlet weak var txtAdditnNotes : UITextView!
    
    
    @IBOutlet weak var txtTilleStatus : UITextField!
    @IBOutlet weak var txtTitleCommt : UITextField!
    @IBOutlet weak var txtTilLocatn : UITextField!
    @IBOutlet weak var txtAltTilLocatn : UITextField!
    @IBOutlet weak var txtTitlNo : UITextField!
    
    @IBOutlet weak var txtTitleReceivd : UITextField!
    @IBOutlet weak var txtPrevTitleState : UITextField!
    
    
    
    @IBOutlet weak var txtLienBal : UITextField!
    @IBOutlet weak var txtLienNam : UITextField!
    @IBOutlet weak var txtLienAct : UITextField!
    @IBOutlet weak var txtLiencontact : UITextField!
    @IBOutlet weak var txtLienPhn : UITextField!
    @IBOutlet weak var txtLienFax : UITextField!
    @IBOutlet weak var txtLiencountry : UITextField!
    @IBOutlet weak var txtLienAddrs1 : UITextField!
    @IBOutlet weak var txtLienAddrs2 : UITextField!
    @IBOutlet weak var txtLienCity : UITextField!
    @IBOutlet weak var txtLiencounty : UITextField!
    @IBOutlet weak var txtLienState : UITextField!
    @IBOutlet weak var txtLienPostcode : UITextField!
    
    @IBOutlet weak var txtCotNam : UITextField!
    @IBOutlet weak var txtCotPhn : UITextField!
    @IBOutlet weak var txtCotFax : UITextField!
    @IBOutlet weak var txtCotcountry : UITextField!
    @IBOutlet weak var txtCotAddrs1 : UITextField!
    @IBOutlet weak var txtCotAddrs2 : UITextField!
    @IBOutlet weak var txtCotCity : UITextField!
    @IBOutlet weak var txtCotcounty : UITextField!
    @IBOutlet weak var txtCotState : UITextField!
    @IBOutlet weak var txtCotPostcode : UITextField!
    
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var pv_vehLot: UIPickerView!
    var pv_milegstatus: UIPickerView!
    
    var arrVehLot = NSMutableArray()
    var veh_id = ""
    var arrSelectState = NSMutableArray()
    var arrMil_status = ["Actual Miles","True Mileage Unknown","Beyond Mechanical Limits","Exempt"]
    
    var arrYear = ["Custom Year"]
    var pv_Year: UIPickerView!
    
    var arrModel = ["Custom Model"]
    var pv_Model: UIPickerView!
    
    var arrBstyl = ["Custom Bodystyle"]
    var pv_Bodystyl: UIPickerView!
    
    var arrExtColor = ["Custom Color"]
    var pv_ExtColr: UIPickerView!
    
    var arrEngine = ["Custom Engine"]
    var pv_Engine: UIPickerView!
    
    var arrfuel = ["Custom Fuel"]
    var pv_Fuel: UIPickerView!
    
    var arrConditn = ["Unknown","Rough","Average","Clean","Extra Clean"]
    var pv_Condtn: UIPickerView!
    
    var arrNewUse = ["New","Used"]
    var pv_new_use: UIPickerView!
    
    var arrmake = [String]()
    var pv_make: UIPickerView!
    
    var arrtrim = ["Custom Trim"]
    var pv_trim: UIPickerView!
    
    var arrTrans = ["Custom Transmission"]
    var pv_transmisn: UIPickerView!
    
    var arrInttrim = ["Custom Interior"]
    var pv_Inttrim: UIPickerView!
    
    var arrdrivTyp = ["Custom Drive"]
    var pv_drivTyp: UIPickerView!
    
    var arrTyp = ["Unknown","Car(Vehicle)","Truck(Vehicle)","Motorcycle","Recreational vehicle","Boat","Equipment","Aircraft","Powersports","Other","SUV","VAN"]
    var pv_Typ: UIPickerView!
    
    var arrColor = ["Beige","Black","Blue","Brown","Gold","Gray","Green","Maroon","Orange","Pink","Purple","Red","Silver","Tan","White","Yellow"]
    var pv_extPrim_color: UIPickerView!
    var pv_extSec_color: UIPickerView!
    
    var pv_Pmonth: UIPickerView!
    var pv_Pyr: UIPickerView!
    
    var pv_Imonth: UIPickerView!
    var pv_Iyr: UIPickerView!
    
    
    var pv_Tstatus: UIPickerView!
    var pv_Tlocation: UIPickerView!
    
    var arrTstatus = ["Unknown status","Clear title","Flood title","Recondition title","Salvage title","Held title","No title"]
    
    var arrTLocation = ["Unknown Location","[Add Title Location]","Auction Issue","Customer Has Title","Floored","HDA Lien Title L1","Title-Mailed Out","Houston Direct Auto L1","Lost Title","Present At Auction","Trade In-Payoff NEEDED!","Transfer Complete"]
    
    
    
    var pv_titleReciv : UIDatePicker!
    
    var pv_Liencountry: UIPickerView!
    var Lien_country_id = ""
    
    var pv_Cotcountry: UIPickerView!
    var Cot_country_id = ""
    
    
    var pv_Allstate: UIPickerView!
    var arrAllState = NSMutableArray()
    var all_state_id = ""
    
    var arrCountry = NSMutableArray()
    var accarrSelectState = NSMutableArray()
    
    var pv_LienState: UIPickerView!
    var lien_state_id = ""
    var pv_CotState: UIPickerView!
    var cot_state_id = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerData()
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONBtnInspectd(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            InspectdBy = false
        }else {
            sender.isSelected = true
            InspectdBy = true
        }
    }
    
}

extension collVehVC{
    
    func callAllPickerData()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.state){
                            
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrAllState.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                            
                        }
                        if(obj == Constant.country){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrCountry.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                        }
                        if(obj == Constant.default_branch_url){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrVehLot.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.callParticularSate()
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
}
extension collVehVC{
    
    func callParticularSate()
    {
        let dict = arrCountry[0] as! NSDictionary
        let _id = dict["_id"] as! Int
        self.Lien_country_id = "\(_id)"
        let strGetUrl = "\(Constant.state)/country_id?id=\(_id)"
        self.callApiToSetData(strUrl: strGetUrl, strTyp: "state")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setApi()
        }
    }
    
    func setApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                if let account_no = dataD["account_no"] as? String{
                    self.lblAcctNo.text = "Account: \(account_no)"
                }
                let accId_selected = dataD["_id"] as! Int
                
                let strGetUrl = "\(Constant.collateral_vehicle)?id=\(accId_selected)"
                self.callApiToSetData(strUrl: strGetUrl, strTyp: "all")
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func callApiToSetData(strUrl : String, strTyp: String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                
                if(strTyp == "state"){
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrSelectState.add(dictA)
                                    }
                                }
                            }
                        }
                        
                    }
                }
                else if(strTyp == "all"){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        if let arr = dict as? NSDictionary {
                            if let vehicle = arr["data"] as? NSDictionary{
                                
                                
                                self.arrmake = []
                                self.arrmake = ["Custom Make"]
                                
                                self.arrYear = []
                                self.arrYear = ["Custom Year"]
                                
                                self.arrModel = []
                                self.arrModel = ["Custom Model"]
                                
                                self.arrBstyl = []
                                self.arrBstyl = ["Custom Bodystyle"]
                                
                                self.arrExtColor = []
                                self.arrExtColor = ["Custom Color"]
                                
                                self.arrEngine = []
                                self.arrEngine = ["Custom Engine"]
                                
                                self.arrfuel = []
                                self.arrfuel = ["Custom Fuel"]
                                
                                self.arrtrim = []
                                self.arrtrim = ["Custom Trim"]
                                
                                self.arrTrans = []
                                self.arrTrans = ["Custom Transmission"]
                                
                                self.arrInttrim = []
                                self.arrInttrim = ["Custom Interior"]
                                
                                self.arrdrivTyp = []
                                self.arrdrivTyp = ["Custom Drive"]
                                
                                if let vin = vehicle["vin"] as? String{
                                    self.lblVin.text = vin
                                }
                                
                                if let vehicleLot = vehicle["vehicleLot"] as? String{
                                    self.veh_id = "\(vehicleLot)"
                                    
                                    for i in 0..<self.arrVehLot.count{
                                        let dict = self.arrVehLot[i] as! NSDictionary
                                        let _id = dict["_id"] as! Int
                                        if("\(_id)" == vehicleLot){
                                            let strTitle = dict["title"] as! String
                                            self.txtVehc_lot.text = strTitle
                                            break
                                        }
                                    }
                                }
                                
                                if let year = vehicle["year"] as? Int{
                                    self.txtYear.text = "\(year)"
                                    self.arrYear.append("\(year)")
                                }
                                if let model = vehicle["model"] as? String{
                                    self.txtModel.text = "\(model)"
                                    self.arrModel.append("\(model)")
                                }
                                if let bodyStyle = vehicle["bodyStyle"] as? String{
                                    self.txtBodyStyle.text = "\(bodyStyle)"
                                    self.arrBstyl.append("\(bodyStyle)")
                                }
                                if let exteriorColor = vehicle["exteriorColor"] as? String{
                                    self.txtExtcolor.text = "\(exteriorColor)"
                                    self.arrExtColor.append("\(exteriorColor)")
                                }
                                if let engine = vehicle["engine"] as? String{
                                    self.txtEngine.text = "\(engine)"
                                    self.arrEngine.append("\(engine)")
                                }
                                if let condition = vehicle["condition"] as? String{
                                    self.txtcondition.text = "\(condition)"
                                }
                                
                                if let newUsed  = vehicle["newUsed"] as? String{
                                    self.txtnew_use.text = "\(newUsed)"
                                }
                                if let make = vehicle["make"] as? String{
                                    self.txtMake.text = "\(make)"
                                    self.arrmake.append("\(make)")
                                }
                                if let trim = vehicle["trim"] as? String{
                                    self.txtTrim.text = "\(trim)"
                                    self.arrtrim.append("\(trim)")
                                }
                                if let transmission = vehicle["transmission"] as? String{
                                    self.txtTransmisn.text = "\(transmission)"
                                    self.arrTrans.append("\(transmission)")
                                }
                                if let interiorTrim = vehicle["interiorTrim"] as? String{
                                    self.txtIntTrim.text = "\(interiorTrim)"
                                    self.arrInttrim.append("\(interiorTrim)")
                                }
                                if let driveType = vehicle["driveType"] as? String{
                                    self.txtDrivTyp.text = "\(driveType)"
                                    self.arrdrivTyp.append("\(driveType)")
                                }
                                if let type  = vehicle["type"] as? String{
                                    self.txtTyp.text = "\(type)"
                                }
                                if let exteriorPrimaryColor  = vehicle["exteriorPrimaryColor"] as? String{
                                    self.txtExtPrimColor.text = "\(exteriorPrimaryColor)"
                                }
                                if let exteriorSecondaryColor  = vehicle["exteriorSecondaryColor"] as? String{
                                    self.txtExtSecColor.text = "\(exteriorSecondaryColor)"
                                }
                                
                                if let additionalNotes  = vehicle["additionalNotes"] as? String{
                                    self.txtAdditnNotes.text = "\(additionalNotes)"
                                }
                                
                                if let altTitleLocation  = vehicle["altTitleLocation"] as? String{
                                    self.txtAltTilLocatn.text = "\(altTitleLocation)"
                                }
                                
                                if let bodyStyle  = vehicle["bodyStyle"] as? String{
                                    self.txtBodyStyle.text = "\(bodyStyle)"
                                    self.arrBstyl.append("\(bodyStyle)")
                                }
                                
                                if let cotAddress1  = vehicle["cotAddress1"] as? String{
                                    self.txtCotAddrs1.text = "\(cotAddress1)"
                                }
                                
                                if let cotAddress2  = vehicle["cotAddress2"] as? String{
                                    self.txtCotAddrs2.text = "\(cotAddress2)"
                                }
                                
                                if let cotCity  = vehicle["cotCity"] as? String{
                                    self.txtCotCity.text = "\(cotCity)"
                                }
                                
                                if let cotCountry  = vehicle["cotCountry"] as? String{
                                    self.txtCotcountry.text = "\(cotCountry)"
                                }
                                
                                if let cotCounty  = vehicle["cotCounty"] as? String{
                                    self.txtCotcounty.text = "\(cotCounty)"
                                }
                                
                                if let cotFax = vehicle["cotFax"] as? String{
                                    self.txtCotFax.text = "\(cotFax)"
                                }
                                
                                if let cotName = vehicle["cotName"] as? String{
                                    self.txtCotNam.text = "\(cotName)"
                                }
                                
                                if let cotPhone = vehicle["cotPhone"] as? String{
                                    self.txtCotPhn.text = "\(cotPhone)"
                                }
                                
                                if let cotPostalCode = vehicle["cotPostalCode"] as? String{
                                    self.txtCotPostcode.text = "\(cotPostalCode)"
                                }
                                
                                if let cotState = vehicle["cotState"] as? String{
                                    self.txtCotState.text = "\(cotState)"
                                }
                                
                                if let driveType = vehicle["driveType"] as? String{
                                    self.txtDrivTyp.text = "\(driveType)"
                                    self.arrdrivTyp.append(driveType)
                                }
                                
                                if let emissions = vehicle["emissions"] as? String{
                                    self.txtEmmison.text = "\(emissions)"
                                }
                                
                                if let engine = vehicle["engine"] as? String{
                                    self.txtEngine.text = "\(engine)"
                                    self.arrEngine.append(engine)
                                }
                                
                                if let exteriorColor = vehicle["exteriorColor"] as? String{
                                    self.txtExtcolor.text = "\(exteriorColor)"
                                    self.arrExtColor.append(exteriorColor)
                                }
                                
                                if let fuel = vehicle["fuel"] as? String{
                                    self.txtFuel.text = "\(fuel)"
                                    self.arrfuel.append(fuel)
                                }
                                
                                if let inspectedBy3rdParty = vehicle["inspectedBy3rdParty"] as? Bool{
                                    self.InspectdBy = inspectedBy3rdParty
                                    if(inspectedBy3rdParty == false){
                                        self.btnInspected.isSelected = false
                                    }
                                    else if(inspectedBy3rdParty == true){
                                        self.btnInspected.isSelected = true
                                    }
                                }
                                
                                if let inspectionExpMonth = vehicle["inspectionExpMonth"] as? String{
                                    self.txtIExpireMnth.text = "\(inspectionExpMonth)"
                                }
                                
                                if let inspectionExpYear = vehicle["inspectionExpYear"] as? String{
                                    self.txtIExpireYr.text = "\(inspectionExpYear)"
                                }
                                
                                if let interiorTrim = vehicle["interiorTrim"] as? String{
                                    self.txtIntTrim.text = "\(interiorTrim)"
                                    self.arrInttrim.append(interiorTrim)
                                }
                                
                                if let lienAddress1 = vehicle["lienAddress1"] as? String{
                                    self.txtLienAddrs1.text = "\(lienAddress1)"
                                }
                                
                                if let lienAddress2 = vehicle["lienAddress2"] as? String{
                                    self.txtLienAddrs2.text = "\(lienAddress2)"
                                }
                                
                                if let lienBalance = vehicle["lienBalance"] as? String{
                                    self.txtLienBal.text = "\(lienBalance)"
                                }
                                
                                if let lienCity = vehicle["lienCity"] as? String{
                                    self.txtLienCity.text = "\(lienCity)"
                                }
                                
                                if let lienContact = vehicle["lienContact"] as? String{
                                    self.txtLiencontact.text = "\(lienContact)"
                                }
                                
                                if let lienCountry = vehicle["lienCountry"] as? String{
                                    for i in 0..<self.arrCountry.count{
                                        let dict = self.arrCountry[i] as! NSDictionary
                                        let _id = dict["_id"] as! Int
                                        if(lienCountry == "\(_id)"){
                                            let strTitle = dict["country_name"] as! String
                                            self.txtLiencountry.text = strTitle
                                            self.Lien_country_id = "\(_id)"
                                            break
                                        }
                                    }
                                }
                                
                                if let lienCounty = vehicle["lienCounty"] as? String{
                                    self.txtLiencounty.text = "\(lienCounty)"
                                }
                                
                                if let lienFax = vehicle["lienFax"] as? String{
                                    self.txtLienFax.text = "\(lienFax)"
                                }
                                if let lienLienAcct = vehicle["lienLienAcct"] as? String{
                                    self.txtLienAct.text = "\(lienLienAcct)"
                                }
                                if let lienName = vehicle["lienName"] as? String{
                                    self.txtLienNam.text = "\(lienName)"
                                }
                                if let lienPhone = vehicle["lienPhone"] as? String{
                                    self.txtLienPhn.text = "\(lienPhone)"
                                }
                                if let lienPostalCode = vehicle["lienPostalCode"] as? String{
                                    self.txtLienPostcode.text = "\(lienPostalCode)"
                                }
                                if let lienState = vehicle["lienState"] as? String{
                                    self.txtLienState.text = "\(lienState)"
                                }
                                
                                if let mileage = vehicle["mileage"] as? String{
                                    self.txtMileg.text = "\(mileage)"
                                }
                                
                                if let mileageStatus = vehicle["mileageStatus"] as? String{
                                    self.txtMileg_status.text = "\(mileageStatus)"
                                }
                                
                                if let newUsed = vehicle["newUsed"] as? String{
                                    self.txtnew_use.text = "\(newUsed)"
                                }
                                
                                if let plateNumber = vehicle["plateNumber"] as? String{
                                    self.txtPlateNo.text = "\(plateNumber)"
                                }
                                
                                if let platesExpireMonth = vehicle["platesExpireMonth"] as? String{
                                    self.txtPExpireMnth.text = "\(platesExpireMonth)"
                                }
                                
                                if let platesExpireYear = vehicle["platesExpireYear"] as? String{
                                    self.txtPExpireYr.text = "\(platesExpireYear)"
                                }
                                
                                if let previousTitleState = vehicle["previousTitleState"] as? String{
                                    self.txtPrevTitleState.text = "\(previousTitleState)"
                                }
                                
                                if let titleComments = vehicle["titleComments"] as? String{
                                    self.txtTitleCommt.text = "\(titleComments)"
                                }
                                
                                if let titleLocation = vehicle["titleLocation"] as? String{
                                    self.txtTilLocatn.text = "\(titleLocation)"
                                }
                                
                                if let titleNumber = vehicle["titleNumber"] as? String{
                                    self.txtTitlNo.text = "\(titleNumber)"
                                }
                                
                                if let titleReceived = vehicle["titleReceived"] as? String{
                                    self.txtTitleReceivd.text = "\(titleReceived)"
                                }
                                
                                if let titleStatus = vehicle["titleStatus"] as? String{
                                    self.txtTilleStatus.text = "\(titleStatus)"
                                }
                                
                                if let trim = vehicle["trim"] as? String{
                                    self.txtTrim.text = "\(trim)"
                                    self.arrtrim.append(trim)
                                }
                                
                                if let transmission = vehicle["transmission"] as? String{
                                    self.txtTransmisn.text = "\(transmission)"
                                    self.arrTrans.append(transmission)
                                }
                                
                                
                                if let type = vehicle["type"] as? String{
                                    self.txtTyp.text = "\(type)"
                                }
                                
                                if let userDefined = vehicle["userDefined"] as? String{
                                    self.txtUserdefin.text = "\(userDefined)"
                                }
                                
                                if let valNumber = vehicle["valNumber"] as? String{
                                    self.txtVan.text = "\(valNumber)"
                                }
                                
                                if let weight = vehicle["weight"] as? String{
                                    self.txtWeight.text = "\(weight)"
                                }
                            }
                        }
                    }
                }
                
                
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension collVehVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pv_vehLot{
            return self.arrVehLot.count
        }
        
        else if(pickerView == pv_milegstatus){
            return self.arrMil_status.count
        }
        else if(pickerView == pv_Year){
            return arrYear.count
        }
        else if(pickerView == pv_Model){
            return arrModel.count
        }
        else if(pickerView == pv_Bodystyl){
            return arrBstyl.count
        }
        else if(pickerView == pv_ExtColr){
            return arrExtColor.count
        }
        else if(pickerView == pv_Engine){
            return arrEngine.count
        }
        else if(pickerView == pv_Fuel){
            return arrfuel.count
        }
        else if(pickerView == pv_Condtn){
            return arrConditn.count
        }
        else if(pickerView == pv_new_use){
            return arrNewUse.count
        }
        else if(pickerView == pv_make){
            return arrmake.count
        }
        else if(pickerView == pv_trim){
            return arrtrim.count
        }
        
        else if(pickerView == pv_transmisn){
            return arrTrans.count
        }
        
        else if(pickerView == pv_Inttrim){
            return arrInttrim.count
        }
        
        else if(pickerView == pv_drivTyp){
            return arrdrivTyp.count
        }
        
        else if(pickerView == pv_Typ){
            return arrTyp.count
        }
        else if(pickerView == pv_extPrim_color || pickerView == pv_extSec_color){
            return arrColor.count
        }
        else if(pickerView == pv_Pmonth || pickerView == pv_Imonth){
            return consArrays.arrMonth.count
        }
        else if(pickerView == pv_Pyr || pickerView == pv_Iyr){
            return consArrays.arrYr.count
        }
        else if(pickerView == pv_Tstatus){
            return arrTstatus.count
        }
        else if(pickerView == pv_Tlocation){
            return arrTLocation.count
        }
        else if(pickerView == pv_Liencountry || pickerView == pv_Cotcountry){
            return arrCountry.count
        }
        else if(pickerView == pv_LienState || pickerView == pv_CotState){
            return arrSelectState.count
        }
        else if(pickerView == pv_Allstate){
            return arrAllState.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pv_vehLot{
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pv_milegstatus){
            let strTitle = arrMil_status[row]
            return strTitle
        }
        
        else if(pickerView == pv_Year){
            let strTitle = arrYear[row]
            return strTitle
        }
        else if(pickerView == pv_Model){
            let strTitle = arrModel[row]
            return strTitle
            
        }
        else if(pickerView == pv_Bodystyl){
            let strTitle = arrBstyl[row]
            return strTitle
        }
        else if(pickerView == pv_ExtColr){
            let strTitle = arrExtColor[row]
            return strTitle
        }
        else if(pickerView == pv_Engine){
            let strTitle = arrEngine[row]
            return strTitle
        }
        else if(pickerView == pv_Fuel){
            let strTitle = arrfuel[row]
            return strTitle
        }
        else if(pickerView == pv_Condtn){
            let strTitle = arrConditn[row]
            return strTitle
        }
        else if(pickerView == pv_new_use){
            let strTitle = arrNewUse[row]
            return strTitle
            
        }
        else if(pickerView == pv_make){
            let strTitle = arrmake[row]
            return strTitle
        }
        
        else if(pickerView == pv_trim){
            let strTitle = arrtrim[row]
            return strTitle
        }
        
        else if(pickerView == pv_transmisn){
            let strTitle = arrTrans[row]
            return strTitle
        }
        
        else if(pickerView == pv_Inttrim){
            let strTitle = arrInttrim[row]
            return strTitle
        }
        
        else if(pickerView == pv_drivTyp){
            let strTitle = arrdrivTyp[row]
            return strTitle
        }
        
        else if(pickerView == pv_Typ){
            let strTitle = arrTyp[row]
            return strTitle
        }
        else if(pickerView == pv_extPrim_color || pickerView == pv_extSec_color){
            let strTitle = arrColor[row]
            return strTitle
        }
        else if(pickerView == pv_Pmonth || pickerView == pv_Imonth){
            let strTitle = consArrays.arrMonth[row]
            return strTitle
        }
        else if(pickerView == pv_Pyr || pickerView == pv_Iyr){
            let strTitle = consArrays.arrYr[row]
            return strTitle
        }
        else if(pickerView == pv_Tstatus){
            let strTitle = arrTstatus[row]
            return strTitle
        }
        else if(pickerView == pv_Tlocation){
            let strTitle = arrTLocation[row]
            return strTitle
        }
        
        else if(pickerView == pv_Liencountry  || pickerView == pv_Cotcountry){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
        else if(pickerView == pv_LienState || pickerView == pv_CotState){
            let dict = arrSelectState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        else if(pickerView == pv_Allstate){
            let dict = arrAllState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
            
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == pv_vehLot{
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtVehc_lot.text = strTitle
            let _id = dict["_id"] as! Int
            self.veh_id = "\(_id)"
            
        }
        else if(pickerView == pv_Liencountry || pickerView == pv_Cotcountry){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            
            if(pickerView == pv_Liencountry){
                self.txtLiencountry.text = strTitle
                let _id = dict["_id"] as! Int
                self.Lien_country_id = "\(_id)"
                
            }
            else if(pickerView == pv_Cotcountry){
                self.txtCotcountry.text = strTitle
                let _id = dict["_id"] as! Int
                self.Cot_country_id = "\(_id)"
            }
        }
        else if(pickerView == pv_LienState || pickerView == pv_CotState){
            let dict = arrSelectState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            
            if(pickerView == pv_LienState){
                self.txtLienState.text = strTitle
                let _id = dict["_id"] as! Int
                self.lien_state_id = "\(_id)"
                
            }
            else if(pickerView == pv_CotState){
                self.txtCotState.text = strTitle
                let _id = dict["_id"] as! Int
                self.cot_state_id = "\(_id)"
            }
            
        }
        
        else if(pickerView == pv_Allstate){
            let dict = arrAllState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtPrevTitleState.text = strTitle
            let _id = dict["_id"] as! Int
            self.all_state_id = "\(_id)"
        }
        else if(pickerView == pv_milegstatus){
            self.txtMileg_status.text = (arrMil_status[row])
        }
        else if(pickerView == pv_Year){
            let strTitle = arrYear[row]
            if(strTitle == "Custom Year"){
                self.txtYear.text = ""
                self.txtYear.inputView = nil
                self.txtYear.inputAccessoryView = nil
                self.txtYear.reloadInputViews()
                self.btnYear.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnYear.tag = 1
                self.btnYear.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtYear.text = strTitle
            }
        }
        else if(pickerView == pv_Model){
            let strTitle = arrModel[row]
            if(strTitle == "Custom Model"){
                self.txtModel.text = ""
                self.txtModel.inputView = nil
                self.txtModel.inputAccessoryView = nil
                self.txtModel.reloadInputViews()
                self.btnModel.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnModel.tag = 2
                self.btnModel.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtModel.text = strTitle
            }
        }
        else if(pickerView == pv_Bodystyl){
            let strTitle = arrBstyl[row]
            if(strTitle == "Custom Bodystyle"){
                self.txtBodyStyle.text = ""
                self.txtBodyStyle.inputView = nil
                self.txtBodyStyle.inputAccessoryView = nil
                self.txtBodyStyle.reloadInputViews()
                self.btnBodystyl.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnBodystyl.tag = 3
                self.btnBodystyl.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtBodyStyle.text = strTitle
            }
            
        }
        else if(pickerView == pv_ExtColr){
            let strTitle = arrExtColor[row]
            if(strTitle == "Custom Color"){
                self.txtExtcolor.text = ""
                self.txtExtcolor.inputView = nil
                self.txtExtcolor.inputAccessoryView = nil
                self.txtExtcolor.reloadInputViews()
                self.btnExtcolor.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnExtcolor.tag = 4
                self.btnExtcolor.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtExtcolor.text = strTitle
            }
        }
        
        else if(pickerView == pv_Engine){
            let strTitle = arrEngine[row]
            if(strTitle == "Custom Engine"){
                self.txtEngine.text = ""
                self.txtEngine.inputView = nil
                self.txtEngine.inputAccessoryView = nil
                self.txtEngine.reloadInputViews()
                self.btnEngine.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnEngine.tag = 5
                self.btnEngine.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtEngine.text = strTitle
            }
        }
        
        else if(pickerView == pv_Fuel){
            let strTitle = arrfuel[row]
            
            if(strTitle == "Custom Fuel"){
                self.txtFuel.text = ""
                self.txtFuel.inputView = nil
                self.txtFuel.inputAccessoryView = nil
                self.txtFuel.reloadInputViews()
                self.btnFuel.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnFuel.tag = 11
                self.btnFuel.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtFuel.text = strTitle
            }
        }
        else if(pickerView == pv_Condtn){
            self.txtcondition.text = arrConditn[row]
        }
        else if(pickerView == pv_new_use){
            self.txtnew_use.text = arrNewUse[row]
        }
        else if(pickerView == pv_make){
            let strTitle = arrmake[row]
            if(strTitle == "Custom Make"){
                self.txtMake.text = ""
                self.txtMake.inputView = nil
                self.txtMake.inputAccessoryView = nil
                self.txtMake.reloadInputViews()
                self.btnMake.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnMake.tag = 6
                self.btnMake.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtMake.text = strTitle
            }
        }
        else if(pickerView == pv_trim){
            let strTitle = arrtrim[row]
            if(strTitle == "Custom Trim"){
                self.txtTrim.text = ""
                self.txtTrim.inputView = nil
                self.txtTrim.inputAccessoryView = nil
                self.txtTrim.reloadInputViews()
                self.btnTrim.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnTrim.tag = 7
                self.btnTrim.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtTrim.text = strTitle
            }
        }
        
        else if(pickerView == pv_transmisn){
            let strTitle = arrTrans[row]
            if(strTitle == "Custom Transmission"){
                self.txtTransmisn.text = ""
                self.txtTransmisn.inputView = nil
                self.txtTransmisn.inputAccessoryView = nil
                self.txtTransmisn.reloadInputViews()
                self.btnTransmisn.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnTransmisn.tag = 8
                self.btnTrim.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtTransmisn.text = strTitle
            }
            
        }
        
        else if(pickerView == pv_Inttrim){
            let strTitle = arrInttrim[row]
            if(strTitle == "Custom Interior"){
                self.txtIntTrim.text = ""
                self.txtIntTrim.inputView = nil
                self.txtIntTrim.inputAccessoryView = nil
                self.txtIntTrim.reloadInputViews()
                self.btnIntTrim.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnIntTrim.tag = 9
                self.btnIntTrim.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtIntTrim.text = strTitle
            }
            
        }
        
        else if(pickerView == pv_drivTyp){
            let strTitle = arrdrivTyp[row]
            if(strTitle == "Custom Drive"){
                self.txtDrivTyp.text = ""
                self.txtDrivTyp.inputView = nil
                self.txtDrivTyp.inputAccessoryView = nil
                self.txtDrivTyp.reloadInputViews()
                self.btnDrivTyp.setImage(UIImage(named: "pre_page"), for: .normal)
                self.btnDrivTyp.tag = 10
                self.btnDrivTyp.addTarget(self, action: #selector(self.resetTestfield(_:)), for: .touchUpInside)
            }
            else{
                self.txtDrivTyp.text = strTitle
            }
            
        }
        
        else if(pickerView == pv_Typ){
            self.txtTyp.text = arrTyp[row]
        }
        
        else if(pickerView == pv_extPrim_color || pickerView == pv_extSec_color){
            let strTitle = arrColor[row]
            
            if(pickerView == pv_extPrim_color){
                self.txtExtPrimColor.text = strTitle
            }
            else if(pickerView == pv_extSec_color){
                self.txtExtSecColor.text = strTitle
            }
        }
        else if(pickerView == pv_Pmonth || pickerView == pv_Imonth){
            let strTitle = consArrays.arrMonth[row]
            if(pickerView == pv_Pmonth){
                self.txtPExpireMnth.text = strTitle
            }
            else if(pickerView == pv_Imonth){
                self.txtIExpireMnth.text = strTitle
            }
            
            
        }
        else if(pickerView == pv_Pyr || pickerView == pv_Iyr){
            let strTitle = consArrays.arrYr[row]
            if(pickerView == pv_Pyr){
                self.txtPExpireYr.text = strTitle
            }
            else if(pickerView == pv_Iyr){
                self.txtIExpireYr.text = strTitle
            }
            
        }
        else if(pickerView == pv_Tstatus){
            self.txtTilleStatus.text = arrTstatus[row]
            
        }
        else if(pickerView == pv_Tlocation){
            self.txtTilLocatn.text = arrTLocation[row]
            
        }
    }
    
    @objc func resetTestfield(_ sender: UIButton)
    {
        if(sender.tag == 1){
            self.btnYear.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtYear.resignFirstResponder()
        }
        else if(sender.tag == 2){
            self.btnModel.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtModel.resignFirstResponder()
        }
        else if(sender.tag == 3){
            self.btnBodystyl.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtBodyStyle.resignFirstResponder()
        }
        else if(sender.tag == 4){
            self.btnExtcolor.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtExtcolor.resignFirstResponder()
        }
        else if(sender.tag == 5){
            self.btnEngine.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtEngine.resignFirstResponder()
        }
        else if(sender.tag == 6){
            self.btnMake.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtMake.resignFirstResponder()
        }
        else if(sender.tag == 7){
            self.btnTrim.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtTrim.resignFirstResponder()
        }
        else if(sender.tag == 8){
            self.btnTransmisn.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtTransmisn.resignFirstResponder()
        }
        else if(sender.tag == 9){
            self.btnIntTrim.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtIntTrim.resignFirstResponder()
        }
        else if(sender.tag == 10){
            self.btnDrivTyp.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtDrivTyp.resignFirstResponder()
        }
        else if(sender.tag == 11){
            self.btnFuel.setImage(UIImage(named: "dropdown_black"), for: .normal)
            self.txtFuel.resignFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtVehc_lot){
            self.pickUp(txtVehc_lot)
        }
        else if(textField == self.txtMileg_status){
            self.pickUp(txtMileg_status)
        }
        else if(textField == self.txtYear){
            self.pickUp(txtYear)
        }
        else if(textField == self.txtModel){
            self.pickUp(txtModel)
        }
        else if(textField == self.txtBodyStyle){
            self.pickUp(txtBodyStyle)
        }
        else if(textField == self.txtExtcolor){
            self.pickUp(txtExtcolor)
        }
        else if(textField == self.txtEngine){
            self.pickUp(txtEngine)
        }
        else if(textField == self.txtFuel){
            self.pickUp(txtFuel)
        }
        else if(textField == self.txtcondition){
            self.pickUp(txtcondition)
        }
        else if(textField == self.txtnew_use){
            self.pickUp(txtnew_use)
        }
        else if(textField == self.txtMake){
            self.pickUp(txtMake)
        }
        else if(textField == self.txtTrim){
            self.pickUp(txtTrim)
        }
        else if(textField == self.txtTransmisn){
            self.pickUp(txtTransmisn)
        }
        else if(textField == self.txtIntTrim){
            self.pickUp(txtIntTrim)
        }
        else if(textField == self.txtDrivTyp){
            self.pickUp(txtDrivTyp)
        }
        else if(textField == self.txtTyp){
            self.pickUp(txtTyp)
        }
        else if(textField == self.txtExtPrimColor){
            self.pickUp(txtExtPrimColor)
        }
        else if(textField == self.txtExtSecColor){
            self.pickUp(txtExtSecColor)
        }
        
        else if(textField == self.txtPExpireMnth){
            self.pickUp(txtPExpireMnth)
        }
        else if(textField == self.txtIExpireMnth){
            self.pickUp(txtIExpireMnth)
        }
        else if(textField == self.txtPExpireYr){
            self.pickUp(txtPExpireYr)
        }
        else if(textField == self.txtIExpireYr){
            self.pickUp(txtIExpireYr)
        }
        else if(textField == self.txtTilleStatus){
            self.pickUp(txtTilleStatus)
        }
        else if(textField == self.txtTilLocatn){
            self.pickUp(txtTilLocatn)
        }
        else if(textField == self.txtLiencountry){
            self.pickUp(txtLiencountry)
        }
        else if(textField == self.txtCotcountry){
            self.pickUp(txtCotcountry)
        }
        else if(textField == self.txtLienState){
            self.pickUp(txtLienState)
        }
        else if(textField == self.txtCotState){
            self.pickUp(txtCotState)
        }
        else if(textField == self.txtPrevTitleState){
            self.pickUp(txtPrevTitleState)
        }
        else if(textField == self.txtTitleReceivd){
            self.pickUpDateTime(txtTitleReceivd)
        }
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtTitleReceivd){
            self.pv_titleReciv = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_titleReciv.datePickerMode = .date
            self.pv_titleReciv.backgroundColor = UIColor.white
            textField.inputView = self.pv_titleReciv
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtTitleReceivd.text = formatter.string(from: pv_titleReciv.date)
        self.txtTitleReceivd.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        txtTitleReceivd.resignFirstResponder()
    }
    
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtVehc_lot){
            self.pv_vehLot = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_vehLot.delegate = self
            self.pv_vehLot.dataSource = self
            self.pv_vehLot.backgroundColor = UIColor.white
            textField.inputView = self.pv_vehLot
        }
        else if(textField == self.txtMileg_status){
            self.pv_milegstatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_milegstatus.delegate = self
            self.pv_milegstatus.dataSource = self
            self.pv_milegstatus.backgroundColor = UIColor.white
            textField.inputView = self.pv_milegstatus
        }
        else if(textField == self.txtYear){
            self.pv_Year = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Year.delegate = self
            self.pv_Year.dataSource = self
            self.pv_Year.backgroundColor = UIColor.white
            textField.inputView = self.pv_Year
        }
        else if(textField == self.txtModel){
            self.pv_Model = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Model.delegate = self
            self.pv_Model.dataSource = self
            self.pv_Model.backgroundColor = UIColor.white
            textField.inputView = self.pv_Model
        }
        
        else if(textField == self.txtBodyStyle){
            self.pv_Bodystyl = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Bodystyl.delegate = self
            self.pv_Bodystyl.dataSource = self
            self.pv_Bodystyl.backgroundColor = UIColor.white
            textField.inputView = self.pv_Bodystyl
        }
        
        else if(textField == self.txtExtcolor){
            self.pv_ExtColr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_ExtColr.delegate = self
            self.pv_ExtColr.dataSource = self
            self.pv_ExtColr.backgroundColor = UIColor.white
            textField.inputView = self.pv_ExtColr
        }
        
        else if(textField == self.txtEngine){
            self.pv_Engine = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Engine.delegate = self
            self.pv_Engine.dataSource = self
            self.pv_Engine.backgroundColor = UIColor.white
            textField.inputView = self.pv_Engine
        }
        
        else if(textField == self.txtFuel){
            self.pv_Fuel = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Fuel.delegate = self
            self.pv_Fuel.dataSource = self
            self.pv_Fuel.backgroundColor = UIColor.white
            textField.inputView = self.pv_Fuel
        }
        
        else if(textField == self.txtcondition){
            self.pv_Condtn = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Condtn.delegate = self
            self.pv_Condtn.dataSource = self
            self.pv_Condtn.backgroundColor = UIColor.white
            textField.inputView = self.pv_Condtn
        }
        else if(textField == self.txtnew_use){
            self.pv_new_use = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_new_use.delegate = self
            self.pv_new_use.dataSource = self
            self.pv_new_use.backgroundColor = UIColor.white
            textField.inputView = self.pv_new_use
        }
        
        else if(textField == self.txtMake){
            self.pv_make = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_make.delegate = self
            self.pv_make.dataSource = self
            self.pv_make.backgroundColor = UIColor.white
            textField.inputView = self.pv_make
        }
        else if(textField == self.txtTrim){
            self.pv_trim = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_trim.delegate = self
            self.pv_trim.dataSource = self
            self.pv_trim.backgroundColor = UIColor.white
            textField.inputView = self.pv_trim
        }
        else if(textField == self.txtTransmisn){
            self.pv_transmisn = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_transmisn.delegate = self
            self.pv_transmisn.dataSource = self
            self.pv_transmisn.backgroundColor = UIColor.white
            textField.inputView = self.pv_transmisn
        }
        else if(textField == self.txtIntTrim){
            self.pv_Inttrim = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Inttrim.delegate = self
            self.pv_Inttrim.dataSource = self
            self.pv_Inttrim.backgroundColor = UIColor.white
            textField.inputView = self.pv_Inttrim
        }
        else if(textField == self.txtDrivTyp){
            self.pv_drivTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_drivTyp.delegate = self
            self.pv_drivTyp.dataSource = self
            self.pv_drivTyp.backgroundColor = UIColor.white
            textField.inputView = self.pv_drivTyp
        }
        else if(textField == self.txtTyp){
            self.pv_Typ = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Typ.delegate = self
            self.pv_Typ.dataSource = self
            self.pv_Typ.backgroundColor = UIColor.white
            textField.inputView = self.pv_Typ
        }
        else if(textField == self.txtExtPrimColor){
            self.pv_extPrim_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_extPrim_color.delegate = self
            self.pv_extPrim_color.dataSource = self
            self.pv_extPrim_color.backgroundColor = UIColor.white
            textField.inputView = self.pv_extPrim_color
        }
        
        else if(textField == self.txtExtSecColor){
            self.pv_extSec_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_extSec_color.delegate = self
            self.pv_extSec_color.dataSource = self
            self.pv_extSec_color.backgroundColor = UIColor.white
            textField.inputView = self.pv_extSec_color
        }
        
        else if(textField == self.txtPExpireMnth){
            self.pv_Pmonth = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Pmonth.delegate = self
            self.pv_Pmonth.dataSource = self
            self.pv_Pmonth.backgroundColor = UIColor.white
            textField.inputView = self.pv_Pmonth
        }
        
        else if(textField == self.txtIExpireMnth){
            self.pv_Imonth = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Imonth.delegate = self
            self.pv_Imonth.dataSource = self
            self.pv_Imonth.backgroundColor = UIColor.white
            textField.inputView = self.pv_Imonth
        }
        
        else if(textField == self.txtPExpireYr){
            self.pv_Pyr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Pyr.delegate = self
            self.pv_Pyr.dataSource = self
            self.pv_Pyr.backgroundColor = UIColor.white
            textField.inputView = self.pv_Pyr
        }
        else if(textField == self.txtIExpireYr){
            self.pv_Iyr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Iyr.delegate = self
            self.pv_Iyr.dataSource = self
            self.pv_Iyr.backgroundColor = UIColor.white
            textField.inputView = self.pv_Iyr
        }
        
        else if(textField == self.txtTilleStatus){
            self.pv_Tstatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Tstatus.delegate = self
            self.pv_Tstatus.dataSource = self
            self.pv_Tstatus.backgroundColor = UIColor.white
            textField.inputView = self.pv_Tstatus
        }
        
        else if(textField == self.txtTilLocatn){
            self.pv_Tlocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Tlocation.delegate = self
            self.pv_Tlocation.dataSource = self
            self.pv_Tlocation.backgroundColor = UIColor.white
            textField.inputView = self.pv_Tlocation
        }
        else if(textField == self.txtLiencountry){
            self.pv_Liencountry = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Liencountry.delegate = self
            self.pv_Liencountry.dataSource = self
            self.pv_Liencountry.backgroundColor = UIColor.white
            textField.inputView = self.pv_Liencountry
        }
        else if(textField == self.txtCotcountry){
            self.pv_Cotcountry = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Cotcountry.delegate = self
            self.pv_Cotcountry.dataSource = self
            self.pv_Cotcountry.backgroundColor = UIColor.white
            textField.inputView = self.pv_Cotcountry
        }
        
        else if(textField == self.txtLienState){
            self.pv_LienState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_LienState.delegate = self
            self.pv_LienState.dataSource = self
            self.pv_LienState.backgroundColor = UIColor.white
            textField.inputView = self.pv_LienState
        }
        else if(textField == self.txtPrevTitleState){
            self.pv_Allstate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Allstate.delegate = self
            self.pv_Allstate.dataSource = self
            self.pv_Allstate.backgroundColor = UIColor.white
            textField.inputView = self.pv_Allstate
        }
        else if(textField == self.txtCotState){
            self.pv_CotState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_CotState.delegate = self
            self.pv_CotState.dataSource = self
            self.pv_CotState.backgroundColor = UIColor.white
            textField.inputView = self.pv_CotState
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtVehc_lot.resignFirstResponder()
        txtMileg_status.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtBodyStyle.resignFirstResponder()
        txtExtcolor.resignFirstResponder()
        txtEngine.resignFirstResponder()
        txtFuel.resignFirstResponder()
        txtcondition.resignFirstResponder()
        txtnew_use.resignFirstResponder()
        txtMake.resignFirstResponder()
        txtTrim.resignFirstResponder()
        txtTransmisn.resignFirstResponder()
        txtIntTrim.resignFirstResponder()
        txtDrivTyp.resignFirstResponder()
        txtTyp.resignFirstResponder()
        txtExtPrimColor.resignFirstResponder()
        txtExtSecColor.resignFirstResponder()
        txtPExpireMnth.resignFirstResponder()
        txtPExpireYr.resignFirstResponder()
        txtIExpireMnth.resignFirstResponder()
        txtIExpireYr.resignFirstResponder()
        txtTilleStatus.resignFirstResponder()
        txtTilLocatn.resignFirstResponder()
        txtLiencountry.resignFirstResponder()
        txtLienState.resignFirstResponder()
        txtPrevTitleState.resignFirstResponder()
        txtCotcountry.resignFirstResponder()
        txtCotState.resignFirstResponder()
    }
}

extension collVehVC {
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                let params = [
                    "additionalNotes": self.txtAdditnNotes.text!,
                    "altTitleLocation": self.txtAltTilLocatn.text!,
                    "bodyStyle": self.txtBodyStyle.text!,
                    "condition": self.txtcondition.text!,
                    "cotAddress1": self.txtCotAddrs1.text!,
                    "cotAddress2": self.txtCotAddrs2.text!,
                    "cotCity": self.txtCotCity.text!,
                    "cotCountry": self.Cot_country_id,
                    "cotCounty": self.txtCotcounty.text!,
                    "cotFax": self.txtCotFax.text!,
                    "cotName": self.txtCotNam.text!,
                    "cotPhone": self.txtCotPhn.text!,
                    "cotPostalCode": self.txtCotPostcode.text!,
                    "cotState": self.cot_state_id,
                    "driveType": self.txtDrivTyp.text!,
                    "emissions": self.txtEmmison.text!,
                    "engine": self.txtEngine.text!,
                    "exteriorColor": self.txtExtcolor.text!,
                    "exteriorPrimaryColor": self.txtExtPrimColor.text!,
                    "exteriorSecondaryColor": self.txtExtSecColor.text!,
                    "fuel": self.txtFuel.text!,
                    "id": accId_selected,
                    "inspectedBy3rdParty": self.InspectdBy,
                    "inspectionExpMonth": self.txtIExpireMnth.text!,
                    "inspectionExpYear": self.txtIExpireYr.text!,
                    "interiorTrim": self.txtIntTrim.text!,
                    "lienAddress1": self.txtLienAddrs1.text!,
                    "lienAddress2": self.txtLienAddrs2.text!,
                    "lienBalance": self.txtLienBal.text!,
                    "lienCity": self.txtLienCity.text!,
                    "lienContact": self.txtLiencontact.text!,
                    "lienCountry": self.Lien_country_id,
                    "lienCounty": self.txtLiencounty.text!,
                    "lienFax": self.txtLienFax.text!,
                    "lienLienAcct": self.txtLienAct.text!,
                    "lienName": self.txtLienNam.text!,
                    "lienPhone": self.txtLienPhn.text!,
                    "lienPostalCode": self.txtLienPostcode.text!,
                    "lienState": self.lien_state_id,
                    "make": self.txtMake.text!,
                    "mileage": self.txtMileg.text!,
                    "mileageStatus": self.txtMileg_status.text!,
                    "model": self.txtModel.text!,
                    "newUsed": self.txtnew_use.text!,
                    "plateNumber": self.txtPlateNo.text!,
                    "platesExpireMonth": self.txtPExpireMnth.text!,
                    "platesExpireYear": self.txtPExpireYr.text!,
                    "previousTitleState": self.all_state_id,
                    "titleComments": self.txtTitleCommt.text!,
                    "titleLocation": self.txtTilLocatn.text!,
                    "titleNumber": self.txtTitlNo.text!,
                    "titleReceived": self.txtTitleReceivd.text!,
                    "titleStatus": self.txtTilleStatus.text!,
                    "transmission": self.txtTransmisn.text!,
                    "trim": self.txtTrim.text!,
                    "type": self.txtTyp.text!,
                    "userDefined": self.txtUserdefin.text!,
                    "valNumber": self.txtVan.text!,
                    "vehicleLot": self.veh_id,
                    "vin": self.lblVin.text!,
                    "weight": self.txtWeight.text!,
                    "year": self.txtYear.text!
                ] as [String : Any]
                Globalfunc.print(object: params)
                self.callUpdateApi(param: params)
            }
        }
        catch {
            Globalfunc.print(object: "error")
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePutWithToken(url: Constant.collateral_vehicle, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
