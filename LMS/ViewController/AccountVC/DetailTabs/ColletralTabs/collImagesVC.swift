
import UIKit

class collImagesVC: UIViewController {
    
    @IBOutlet weak var collImages : UICollectionView!
    
    @IBOutlet weak var viewData : UIView!
    @IBOutlet weak var viewImg : UIView!
    @IBOutlet weak var imgZoom : UIImageView!
    @IBOutlet weak var viewNoData : UIView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrImagesData : [NSDictionary] = []
    var estimateWidth = 160.0
    var cellMarginSize = 16.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.viewData.isHidden = false
        self.viewImg.isHidden = true
        self.viewNoData.isHidden = true
        
        
        Globalfunc.showLoaderView(view: self.view)
        callGetImgsApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//call Api
extension collImagesVC {
    
    func callGetImgsApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"

                
                let Str_url = "\(Constant.Coll_img_url)?account_id=\(accId_selected)&action_type=collateral-image"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                print(response)
                                if let responseArr = response["data"] as? [NSDictionary] {
                                    
                                    if(responseArr.count > 0){
                                        for i in 0..<responseArr.count{
                                            let dictA = responseArr[i]
                                            self.arrImagesData.append(dictA)
                                        }
                                        
                                        self.viewData.isHidden = false
                                        self.viewImg.isHidden = true
                                        self.viewNoData.isHidden = true
                                        
                                        self.collImages.reloadData()
                                    }
                                    else{
                                        self.arrImagesData = []
                                        self.collImages.reloadData()
                                        
                                        self.viewData.isHidden = true
                                        self.viewImg.isHidden = true
                                        self.viewNoData.isHidden = false
                                        
                                        
                                    }
                                }
                            }
                                
                                
                            else
                            {
                                OperationQueue.main.addOperation {
                                    Globalfunc.hideLoaderView(view: self.view)
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                                }
                            }
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        
    }
    
    func removeImgfromListApi(_ imgId : Int)
    {
        
    }
}

//coll datasource delegate
extension collImagesVC : UICollectionViewDelegate , UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImagesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collImages.dequeueReusableCell(withReuseIdentifier: "colletralImgCell", for: indexPath) as! colletralImgCell
        let dict = self.arrImagesData[indexPath.row]
        let image_name = dict["image_name"] as! String
        let imgUrl = "http://18.191.178.120:3000/uploads/images/\(image_name)"
        cell.imgCollateral.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: UIImage(named: ""))
        
        cell.btnview.tag = indexPath.row
        cell.btnview.addTarget(self, action: #selector(clikcONViewBtn(_:)), for: .touchUpInside)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clikcONRemoveBtn(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func clikcONViewBtn(_ sender : UIButton)
    {
        let dict = arrImagesData[sender.tag]
        if let file_name = dict["image_name"] as? String
        {
            let imgUrl = "http://18.191.178.120:3000/uploads/images/\(file_name)"
            //  self.viewData.isHidden = false
            self.viewImg.isHidden = false
            self.imgZoom.sd_setImage(with: URL.init(string: imgUrl), placeholderImage: UIImage(named: ""))
            
        }
    }
    
    @IBAction func clickONCros(_ sender : UIButton)
    {
        self.arrImagesData = []
        self.callGetImgsApi()
        //self.viewData.isHidden = false
        // self.viewImg.isHidden = true
        //self.viewNoData.isHidden = false
    }
    
    @objc func clikcONRemoveBtn(_ sender : UIButton)
    {
        let dict = arrImagesData[sender.tag]
        let id_del = dict["_id"] as! String
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.Coll_img_url)?id=\(id_del)"
            self.deletFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)

    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.arrImagesData = []
                            self.viewDidLoad()
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    //2020/5/1-15901759207170.png
}

extension collImagesVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.calculateWith()
        return CGSize(width: width, height: 250)
    }
    
    func calculateWith() -> CGFloat {
        let estimatedWidth = CGFloat(estimateWidth)
        let cellCount = floor(CGFloat(self.view.frame.size.width / estimatedWidth))
        let width = (self.view.frame.size.width - CGFloat(cellMarginSize) * (cellCount - 1) - 20) / cellCount
        return width
    }
}


class colletralImgCell : UICollectionViewCell
{
    @IBOutlet weak var imgCollateral : UIImageView!
    @IBOutlet weak var btnview : UIButton!
    @IBOutlet weak var btnRemove : UIButton!
}
//http://18.191.178.120:3000/uploads/images/thumbnail
