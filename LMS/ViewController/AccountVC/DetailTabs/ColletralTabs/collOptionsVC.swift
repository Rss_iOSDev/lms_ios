//
//  collOptionsVC.swift
//  LMS
//
//  Created by Apple on 15/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import WebKit

class collOptionsVC: UIViewController {
    
    @IBOutlet weak var txtAdditonl : UITextView!
    
    @IBOutlet weak var tblStandar : UITableView!
    @IBOutlet weak var tblOptionl : UITableView!
    @IBOutlet weak var tblcustom : UITableView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrStandard = NSMutableArray()
    var arrOptionl = NSMutableArray()
    var arrCustom = NSMutableArray()
    
    var arrSelectedStandard = NSMutableArray()
    var arrSelectedOptionl = NSMutableArray()
    var arrSelectedCustom = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.setDataonView()
        self.loadAddionlSpec()
        self.setApi()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                if let account_no = dataD["account_no"] as? String{
                    self.lblAcctNo.text = "Account: \(account_no)"
                }
                let accId_selected = dataD["_id"] as! Int
                let strGetUrl = "\(Constant.Coll_options)?id=\(accId_selected)"
                self.callApiToSetData(strUrl: strGetUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}
extension collOptionsVC {
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        if let option = arr["data"] as? NSDictionary{
                                if let Custm = option["customEquipment"] as? NSMutableArray{
                                    for i in 0..<Custm.count{
                                        let dict = Custm[i] as! NSMutableDictionary
                                        let _id = dict["_id"] as! Int
                                        for j in 0..<self.arrCustom.count{
                                            let dict1 = self.arrCustom[j] as! NSMutableDictionary
                                            let _id1 = dict1["_id"] as! Int
                                            if(_id1 == _id){
                                                self.arrSelectedCustom.add(dict1)
                                                dict1.setValue(true, forKey: "isCheck")
                                                self.arrCustom.replaceObject(at: j, with: dict1)
                                            }
                                        }
                                    }
                                }
                                if let Option = option["optionalEquipment"] as? NSMutableArray{
                                    for i in 0..<Option.count{
                                        let dict = Option[i] as! NSMutableDictionary
                                        let _id = dict["_id"] as! Int
                                        for j in 0..<self.arrOptionl.count{
                                            let dict1 = self.arrOptionl[j] as! NSMutableDictionary
                                            let _id1 = dict1["_id"] as! Int
                                            if(_id1 == _id){
                                                self.arrSelectedOptionl.add(dict1)
                                                dict1.setValue(true, forKey: "isCheck")
                                                self.arrOptionl.replaceObject(at: j, with: dict1)
                                            }
                                        }
                                    }
                                }
                                if let stndrd = option["standardEquipment"] as? NSMutableArray{
                                    for i in 0..<stndrd.count{
                                        let dict = stndrd[i] as! NSMutableDictionary
                                        let _id = dict["_id"] as! Int
                                        for j in 0..<self.arrStandard.count{
                                            let dict1 = self.arrStandard[j] as! NSMutableDictionary
                                            let _id1 = dict1["_id"] as! Int
                                            if(_id1 == _id){
                                                self.arrSelectedStandard.add(dict1)
                                                dict1.setValue(true, forKey: "isCheck")
                                                self.arrStandard.replaceObject(at: j, with: dict1)
                                            }
                                        }
                                    }
                                }
                                self.tblcustom.reloadData()
                                self.tblStandar.reloadData()
                                self.tblOptionl.reloadData()
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension collOptionsVC {
    
    private func readLocalFile(forName name: String) -> Any {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                do {
                    let response = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                    return response
                    
                } catch {
                    print(error.localizedDescription)
                }
            }
        } catch {
            print(error)
        }
        return [:]
    }
    
    func loadAddionlSpec(){
        txtAdditonl.text = consText.additionalText
    }
    
    func setDataonView(){
        let dict = self.readLocalFile(forName: "options")
        if let Response = dict as? NSDictionary{
            if let equipment = Response["equipment"] as? NSArray{
                self.arrStandard = equipment[0] as! NSMutableArray
                self.arrOptionl = equipment[1] as! NSMutableArray
                self.arrCustom = equipment[2] as! NSMutableArray
                self.tblStandar.reloadData()
                self.tblOptionl.reloadData()
                self.tblcustom.reloadData()
            }
        }
    }
}

class tblEqipmntCell : UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
}

extension collOptionsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count : Int?
        if(tableView == self.tblStandar){
            count = self.arrStandard.count
        }
        if(tableView == self.tblOptionl){
            count = self.arrOptionl.count
        }
        if(tableView == self.tblcustom){
            count = self.arrCustom.count
        }
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if(tableView == self.tblStandar){
            let cell = self.tblStandar.dequeueReusableCell(withIdentifier: "tblEqipmntCell") as! tblEqipmntCell
            
            let dict = self.arrStandard[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["title"] as? String)
            
            let isCheck = dict["isCheck"] as! Bool
            if(isCheck == false){
                cell.btnCheck.isSelected = false
            }
            else if(isCheck == true){
                cell.btnCheck.isSelected = true
            }
            
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(clickONCheckstndtBtn(_:)), for: .touchUpInside)
            
            return cell
        }
        if(tableView == self.tblOptionl){
            let cell = self.tblOptionl.dequeueReusableCell(withIdentifier: "tblEqipmntCell") as! tblEqipmntCell
            
            let dict = self.arrOptionl[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["title"] as? String)
            
            let isCheck = dict["isCheck"] as! Bool
            if(isCheck == false){
                cell.btnCheck.isSelected = false
            }
            else if(isCheck == true){
                cell.btnCheck.isSelected = true
            }
            
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(clickONCheckOptnBtn(_:)), for: .touchUpInside)
            
            
            return cell
        }
        if(tableView == self.tblcustom){
            let cell = self.tblcustom.dequeueReusableCell(withIdentifier: "tblEqipmntCell") as! tblEqipmntCell
            
            let dict = self.arrCustom[indexPath.row] as! NSDictionary
            cell.lblTitle.text = (dict["title"] as? String)
            
            let isCheck = dict["isCheck"] as! Bool
            if(isCheck == false){
                cell.btnCheck.isSelected = false
            }
            else if(isCheck == true){
                cell.btnCheck.isSelected = true
            }
            cell.btnCheck.tag = indexPath.row
            cell.btnCheck.addTarget(self, action: #selector(clickONCheckCustmBtn(_:)), for: .touchUpInside)
            return cell
        }
        return cell
    }
    
    
    @objc func clickONCheckstndtBtn(_ sender: UIButton)
    {
        let dict = self.arrStandard[sender.tag]  as! NSDictionary
        if (self.arrSelectedStandard.contains(dict))
        {
            self.arrSelectedStandard.remove(dict)
            dict.setValue(false, forKey: "isCheck")
            self.arrStandard.replaceObject(at: sender.tag, with: dict)
            self.tblStandar.reloadData()
        }
        else{
            self.arrSelectedStandard.add(dict)
            dict.setValue(true, forKey: "isCheck")
            self.arrStandard.replaceObject(at: sender.tag, with: dict)
            self.tblStandar.reloadData()
        }
    }
    
    @objc func clickONCheckOptnBtn(_ sender: UIButton)
    {
        let dict = self.arrOptionl[sender.tag]  as! NSDictionary
        if (self.arrSelectedOptionl.contains(dict))
        {
            self.arrSelectedOptionl.remove(dict)
            dict.setValue(false, forKey: "isCheck")
            self.arrOptionl.replaceObject(at: sender.tag, with: dict)
            self.tblOptionl.reloadData()
        }
        else{
            self.arrSelectedOptionl.add(dict)
            dict.setValue(true, forKey: "isCheck")
            self.arrOptionl.replaceObject(at: sender.tag, with: dict)
            self.tblOptionl.reloadData()
        }
    }
    
    @objc func clickONCheckCustmBtn(_ sender: UIButton)
    {
        
        let dict = self.arrCustom[sender.tag]  as! NSDictionary
        if (self.arrSelectedCustom.contains(dict))
        {
            self.arrSelectedCustom.remove(dict)
            dict.setValue(false, forKey: "isCheck")
            self.arrCustom.replaceObject(at: sender.tag, with: dict)
            self.tblcustom.reloadData()
        }
        else{
            self.arrSelectedCustom.add(dict)
            dict.setValue(true, forKey: "isCheck")
            self.arrCustom.replaceObject(at: sender.tag, with: dict)
            self.tblcustom.reloadData()
        }
    }
}

extension collOptionsVC {
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                
                
                for i in 0..<self.arrSelectedCustom.count
                {
                    let dict = self.arrSelectedCustom[i] as! NSDictionary
                    let _id = dict["_id"] as! Int
                    let title = dict["title"] as! String
                    
                    let dictAdd : NSMutableDictionary = [:]
                    dictAdd.setValue(_id, forKey: "_id")
                    dictAdd.setValue(title, forKey: "title")
                    self.arrSelectedCustom.replaceObject(at: i, with: dictAdd)
                    
                }
                
                for i in 0..<self.arrSelectedStandard.count
                {
                    let dict = self.arrSelectedStandard[i] as! NSDictionary
                    let _id = dict["_id"] as! Int
                    let title = dict["title"] as! String
                    
                    let dictAdd : NSMutableDictionary = [:]
                    dictAdd.setValue(_id, forKey: "_id")
                    dictAdd.setValue(title, forKey: "title")
                    self.arrSelectedStandard.replaceObject(at: i, with: dictAdd)
                    
                }
                
                for i in 0..<self.arrSelectedOptionl.count
                {
                    let dict = self.arrSelectedOptionl[i] as! NSDictionary
                    let _id = dict["_id"] as! Int
                    let title = dict["title"] as! String
                    
                    let dictAdd : NSMutableDictionary = [:]
                    dictAdd.setValue(_id, forKey: "_id")
                    dictAdd.setValue(title, forKey: "title")
                    self.arrSelectedOptionl.replaceObject(at: i, with: dictAdd)
                    
                }
                let params = [
                    "customEquipment": self.arrSelectedCustom,
                    "optionalEquipment": self.arrSelectedOptionl,
                    "standardEquipment": self.arrSelectedStandard,
                    "id": accId_selected] as [String : Any]
                self.callUpdateApi(param: params)
            }
        }
        catch {
            Globalfunc.print(object: "error")
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePutWithToken(url: Constant.Coll_options, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
