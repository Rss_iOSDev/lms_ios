//
//  collPurchaseVC.swift
//  LMS
//
//  Created by Apple on 15/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class collPurchaseVC: UIViewController {
    
    @IBOutlet weak var txtPurchaseDate : UITextField!
    @IBOutlet weak var txtVehCost : UITextField!
    @IBOutlet weak var txtPAckFee : UITextField!
    @IBOutlet weak var txtAcqMileage : UITextField!
    @IBOutlet weak var txtAcqMileage_status : UITextField!
    @IBOutlet weak var txtVehLot : UITextField!
    @IBOutlet weak var txtBuyer : UITextField!
    @IBOutlet weak var txtWhereAcc : UITextField!
    @IBOutlet weak var txtAccRef : UITextField!
    @IBOutlet weak var txtStockno : UITextField!
    @IBOutlet weak var txtPurchsMthd : UITextField!
    @IBOutlet weak var txtYear : UITextField!
    @IBOutlet weak var txtviewMsg : UITextView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrUrl = [String]()
    var strGetUrl = ""
    
    var arrBuyer : NSMutableArray = []
    var dictCollateral : NSDictionary = [:]
    
    var pickerPurchseDate : UIDatePicker!
    
    var pickerMil_status: UIPickerView!
    var arrMilStatus = ["Actual Miles","True Mileage Unknown","Beyond Mechanical Limits","Exempt"]
    
    var pickerVehLot: UIPickerView!
    
    var pickerBuyer: UIPickerView!
    
    var pickerWhereAccq: UIPickerView!
    var arrWhereAccq = ["Dealer/Wholesaler/Auction","Individual","Unknown"]
    
    var pickerPurchsMthd: UIPickerView!
    var arrPurchsMthd = ["ACH","AP","Arm","Cash","Check","Draft","Floor Plan","Repossession","Trade-In"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"

                
                strGetUrl = "\(Constant.Coll_purchase)?id=\(accId_selected)"
                arrUrl = [strGetUrl,Constant.buyer_url]
                self.callApiToSetData()
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let accId_selected = dataD["_id"] as! Int
                let params = ["purchaseDate":"\(self.txtPurchaseDate.text!)",
                    "vehicleCost":"\(self.txtVehCost.text!)",
                    "id":accId_selected,
                    "packFee":"\(self.txtPAckFee.text!)",
                    "acquiredMileage":"\(self.txtAcqMileage.text!)",
                    "acquiredMileageStatus":"\(self.txtAcqMileage_status.text!)",
                    "vehicleLot":"\(self.txtVehLot.text!)",
                    "buyer":"\(self.txtBuyer.text!)",
                    "whereAcquired":"\(self.txtWhereAcc.text!)",
                    "acquiredRefInvoiceSlip":"\(self.txtAccRef.text!)",
                    "stockNumber":"\(self.txtStockno.text!)",
                    "purchasingMethod":"\(self.txtPurchsMthd.text!)",
                    "yearPreviousSale":"\(self.txtYear.text!)",
                    "additionalNotes":"\(self.txtviewMsg.text!)"] as [String : Any]
                
                BaseApi.onResponsePutWithToken(url: Constant.Coll_purchase, controller: self, parms: params as NSDictionary) { (dict, error) in
                    
                    OperationQueue.main.addOperation {
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickONCancelBtn(_ sender: UIButton)
    {
        self.txtPurchaseDate.text = ""
        self.txtVehCost.text = ""
        self.txtPAckFee.text = ""
        self.txtAcqMileage.text = ""
        self.txtAcqMileage_status.text = ""
        self.txtVehLot.text = ""
        self.txtWhereAcc.text = ""
        self.txtAccRef.text = ""
        self.txtPurchsMthd.text = ""
        self.txtYear.text = ""
        self.txtviewMsg.text = ""
        
    }
}

extension collPurchaseVC {
    
    func callApiToSetData(){
        
        let group = DispatchGroup() // initialize
        arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if(obj == Constant.buyer_url){
                            
                            if let resp = dict as? NSDictionary {
                                if let arr = resp["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrBuyer.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                            
                            
                        }
                        
                        if(obj == self.strGetUrl){
                            Globalfunc.print(object:dict)
                            if let response = dict as? NSDictionary{
                                if let purchase = response["data"] as? NSDictionary{
                                    self.dictCollateral = purchase
                                    if let acquiredMileage = purchase["acquiredMileage"] as? String{
                                        self.txtAcqMileage.text = acquiredMileage
                                    }
                                    
                                    if let acquiredMileageStatus = purchase["acquiredMileageStatus"] as? String{
                                        self.txtAcqMileage_status.text = acquiredMileageStatus
                                    }
                                    
                                    if let acquiredRefInvoiceSlip = purchase["acquiredRefInvoiceSlip"] as? String{
                                        self.txtAccRef.text = acquiredRefInvoiceSlip
                                    }
                                    
                                    if let additionalNotes = purchase["additionalNotes"] as? String{
                                        self.txtviewMsg.text = additionalNotes
                                    }
                                    
                                    if let buyer = purchase["buyer"] as? String{
                                        self.txtBuyer.text = buyer
                                    }
                                    
                                    
                                    if let packFee = purchase["packFee"] as? String{
                                        self.txtPAckFee.text = packFee
                                    }
                                    
                                    if let purchaseDate = purchase["purchaseDate"] as? String{
                                        let formatter = DateFormatter()
                                        formatter.dateFormat = "MM/dd/yyyy"
                                        let date = Date.dateFromISOString(string: purchaseDate)
                                        self.txtPurchaseDate.text = formatter.string(from: date!)
                                    }
                                    
                                    if let purchasingMethod = purchase["purchasingMethod"] as? String{
                                        self.txtPurchsMthd.text = purchasingMethod
                                    }
                                    
                                    if let stockNumber = purchase["stockNumber"] as? String{
                                        self.txtStockno.text = "\(stockNumber)"
                                    }
                                    
                                    if let vehicleCost = purchase["vehicleCost"] as? String{
                                        self.txtVehCost.text = vehicleCost
                                    }
                                    
                                    if let whereAcquired = purchase["whereAcquired"] as? String{
                                        self.txtWhereAcc.text = whereAcquired
                                    }
                                    
                                    if let vehicleLot = purchase["vehicleLot"] as? String{
                                        self.txtVehLot.text = vehicleLot
                                    }
                                    
                                    if let yearPreviousSale = purchase["yearPreviousSale"] as? String{
                                        self.txtYear.text = yearPreviousSale
                                    }
                                }
                            }
                        }
                        
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}


//?id=249

extension collPurchaseVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerMil_status){
            return self.arrMilStatus.count
        }
            
        else if(pickerView == pickerVehLot){
            return consArrays.arrDefaultBranch.count
        }
            
        else if(pickerView == pickerBuyer){
            return self.arrBuyer.count
        }
            
        else if(pickerView == pickerWhereAccq){
            return self.arrWhereAccq.count
        }
        else if(pickerView == pickerPurchsMthd){
            return self.arrPurchsMthd.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickerMil_status){
            let strTitle = arrMilStatus[row]
            return strTitle
        }
            
        else if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
            
        else if(pickerView == pickerBuyer){
            let dict = arrBuyer[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
            
        }
        else if(pickerView == pickerWhereAccq){
            let strTitle = arrWhereAccq[row]
            return strTitle
        }
        else if(pickerView == pickerPurchsMthd){
            let strTitle = arrPurchsMthd[row]
            return strTitle
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerMil_status){
            self.txtAcqMileage_status.text = arrMilStatus[row]
        }
            
        else if(pickerView == pickerVehLot){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            self.txtVehLot.text = item
        }
            
        else if(pickerView == pickerBuyer){
            let dict = arrBuyer[row] as! NSDictionary
            let item = dict["title"] as! String
            self.txtBuyer.text = item
        }
            
        else if(pickerView == pickerWhereAccq){
            self.txtWhereAcc.text = arrWhereAccq[row]
        }
        else if(pickerView == pickerPurchsMthd){
            self.txtPurchsMthd.text = arrPurchsMthd[row]
        }
    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtAcqMileage_status){
            self.pickUp(txtAcqMileage_status)
        }
            
        else if(textField == self.txtPurchaseDate){
            self.pickUpDateTime(txtPurchaseDate)
        }
            
        else if(textField == self.txtVehLot){
            self.pickUp(txtVehLot)
        }
            
        else if(textField == self.txtBuyer){
            self.pickUp(txtBuyer)
        }
        else if(textField == self.txtWhereAcc){
            self.pickUp(txtWhereAcc)
        }
        else if(textField == self.txtPurchsMthd){
            self.pickUp(txtPurchsMthd)
        }
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtPurchaseDate){
            self.pickerPurchseDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPurchseDate.datePickerMode = .date
            self.pickerPurchseDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerPurchseDate
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAcqMileage_status){
            
            self.pickerMil_status = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerMil_status.delegate = self
            self.pickerMil_status.dataSource = self
            self.pickerMil_status.backgroundColor = UIColor.white
            textField.inputView = self.pickerMil_status
        }
            
        else if(textField == self.txtVehLot){
            self.pickerVehLot = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerVehLot.delegate = self
            self.pickerVehLot.dataSource = self
            self.pickerVehLot.backgroundColor = UIColor.white
            textField.inputView = self.pickerVehLot
        }
            
        else if(textField == self.txtBuyer){
            self.pickerBuyer = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerBuyer.delegate = self
            self.pickerBuyer.dataSource = self
            self.pickerBuyer.backgroundColor = UIColor.white
            textField.inputView = self.pickerBuyer
        }
            
        else if(textField == self.txtWhereAcc){
            self.pickerWhereAccq = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWhereAccq.delegate = self
            self.pickerWhereAccq.dataSource = self
            self.pickerWhereAccq.backgroundColor = UIColor.white
            textField.inputView = self.pickerWhereAccq
        }
            
            
        else if(textField == self.txtPurchsMthd){
            self.pickerPurchsMthd = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPurchsMthd.delegate = self
            self.pickerPurchsMthd.dataSource = self
            self.pickerPurchsMthd.backgroundColor = UIColor.white
            textField.inputView = self.pickerPurchsMthd
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtPurchaseDate.text = formatter.string(from: pickerPurchseDate.date)
        self.txtPurchaseDate.resignFirstResponder()
        
    }
    
    @objc func cancelClickDate() {
        
        txtPurchaseDate.text = ""
        txtPurchaseDate.resignFirstResponder()
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtAcqMileage_status.resignFirstResponder()
        txtVehLot.resignFirstResponder()
        txtBuyer.resignFirstResponder()
        txtWhereAcc.resignFirstResponder()
        txtPurchsMthd.resignFirstResponder()
        
    }
    
    @objc func cancelClick() {
        txtAcqMileage_status.resignFirstResponder()
        txtVehLot.resignFirstResponder()
        txtBuyer.resignFirstResponder()
        txtWhereAcc.resignFirstResponder()
        txtPurchsMthd.resignFirstResponder()
        
    }
}
