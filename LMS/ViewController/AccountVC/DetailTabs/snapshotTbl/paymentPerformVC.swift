//
//  paymentPerformVC.swift
//  LMS
//
//  Created by Apple on 28/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class paymentPerformVC: UIViewController {
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    @IBOutlet weak var collTotal : UICollectionView!
    var arrTotal : [NSDictionary] = []
    
    var dictTotalCount : NSDictionary = [:]
    
    var arrPayPerfHeader : NSMutableArray = []
    var arrPayPerfCell : [[NSDictionary]] = []
    var arr_PT : [[NSDictionary]] = []
    
    
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var tblPaymtPerf: FZAccordionTableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.isHidden = true
        self.tblPaymtPerf.isHidden = true
        
        tblPaymtPerf.allowMultipleSectionsOpen = true
        tblPaymtPerf.register(UINib(nibName: "AccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
        
        Globalfunc.showLoaderView(view: self.view)
        self.getColorFromApi()
    }
    
    func getUrl(strType : String){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account no: \(acct_no)"
                let acctId = dataD["_id"] as! Int
                let Str_url = "\(Constant.pay_perf_data)?id=\(acctId)&action=\(strType)"
                self.getResponseFromApi(strUrl: Str_url)
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clikcOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

//Api
extension paymentPerformVC {
    
    func getColorFromApi(){
        BaseApi.callApiRequestForGet(url: Constant.pay_perf_color) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary {
                        if let arr = response["data"] as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrTotal.append(dictA)
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    // your code here
                                    self.getUrl(strType: "ALL")
                                }
                            }
                        }
                    }
                    
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func getResponseFromApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        
                        
                        
                        if let data = responseDict["data"] as? NSDictionary
                        {
                            if let paymentPerformance = data["paymentPerformance"] as? NSDictionary{
                                self.dictTotalCount = paymentPerformance
                            }
                            
                            if let arr = data["paymentSchedule"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        if let trans = dictA["transaction"] as? [NSDictionary]{
                                            self.arrPayPerfCell.append(trans)
                                        }
                                        
                                        if let arrpt = dictA["PT"] as? [NSDictionary]{
                                            self.arr_PT.append(arrpt)
                                        }
                                        
                                        
                                        
                                        self.arrPayPerfHeader.add(dictA)
                                    }
                                    self.lblStatus.isHidden = true
                                    self.tblPaymtPerf.isHidden = false
                                    self.collTotal.reloadData()
                                    self.tblPaymtPerf.reloadData()
                                }
                                else{
                                    self.lblStatus.isHidden = false
                                    self.tblPaymtPerf.isHidden = true
                                }
                            }
                            
                        }
                        
                        
                        
                        
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension paymentPerformVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrTotal.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collTotal.dequeueReusableCell(withReuseIdentifier: "colTotalCell", for: indexPath) as! colTotalCell
        let dict = self.arrTotal[indexPath.row]
        
        if let title = dict["title"] as? String{
            cell.lblTitleName.text = title
        }
        
        if let color = dict["color"] as? String{
            let replaced = color.replacingOccurrences(of: "#", with: "0x")
            let result = UInt32(String(replaced.dropFirst(2)), radix: 16)
            cell.lblCount.backgroundColor = UIColorFromHex(rgbValue: result!, alpha: 1.0)
        }
        
        switch indexPath.row{
        case 0:
            let paid_all = dictTotalCount["paid_all"] as! Int
            cell.lblCount.text = "\(paid_all)"
            
        case 1:
            let paid_current = dictTotalCount["paid_current"] as! Int
            cell.lblCount.text = "\(paid_current)"
        case 2:
            let paid_1_15 = dictTotalCount["paid_1_15"] as! Int
            cell.lblCount.text = "\(paid_1_15)"
            
        case 3:
            let paid_16_30 = dictTotalCount["paid_16_30"] as! Int
            cell.lblCount.text = "\(paid_16_30)"
            
        case 4:
            let paid_31_60 = dictTotalCount["paid_31_60"] as! Int
            cell.lblCount.text = "\(paid_31_60)"
            
        case 5:
            let paid_60_90 = dictTotalCount["paid_60_90"] as! Int
            cell.lblCount.text = "\(paid_60_90)"
            
        case 6:
            let paid_90_120 = dictTotalCount["paid_90_120"] as! Int
            cell.lblCount.text = "\(paid_90_120)"
            
        case 7:
            let paid_120 = dictTotalCount["paid_120"] as! Int
            cell.lblCount.text = "\(paid_120)"
            
        case 8:
            cell.lblCount.text = "0"
            
        default:
            Globalfunc.print(object:"default")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.arrPayPerfHeader = []
        self.arrPayPerfCell = []
        self.arr_PT = []
        Globalfunc.showLoaderView(view: self.view)
        switch indexPath.row {
        case 0:
            self.getUrl(strType: "ALL")
        case 1:
            self.getUrl(strType: "T0")
        case 2:
            self.getUrl(strType: "T1")
        case 3:
            self.getUrl(strType: "T2")
        case 4:
            self.getUrl(strType: "T3")
        case 5:
            self.getUrl(strType: "T4")
        case 6:
            self.getUrl(strType: "T5")
        case 7:
            self.getUrl(strType: "T6")
        case 8:
            self.getUrl(strType: "T7")
        default:
            Globalfunc.print(object:"default")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0{
            return CGSize(width: 100, height: collectionView.frame.height)
        }else {
            return CGSize(width: 160, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


class colTotalCell : UICollectionViewCell
{
    @IBOutlet weak var lblTitleName : UILabel!
    @IBOutlet weak var lblCount : UILabel!
}


// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension paymentPerformVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrPayPerfCell[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrPayPerfHeader.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return AccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView(tableView, heightForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return self.tableView(tableView, heightForHeaderInSection:section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "perfmTblCell", for: indexPath) as! perfmTblCell
        
        let dictInner = self.arrPayPerfCell[indexPath.section][indexPath.row]
        let dictPt = self.arr_PT[indexPath.section][indexPath.row]
        let principal = dictPt["principal"] as! NSNumber
        let interest = dictPt["interest"] as! NSNumber
        let lateFee = dictPt["lateFee"] as! NSNumber
        let tax = dictPt["tax"] as! NSNumber
        let coll = Double(truncating: principal) + Double(truncating: interest) + Double(truncating: lateFee) + Double(truncating: tax)
        cell.lblCollected.text = "Collected: $\(coll)"
        
        if let postDate = dictInner["postDate"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let date = Date.dateFromISOString(string: postDate)
            let result = formatter.string(from: date!)
            cell.lblDate.text = "Date: \(result)"
        }
        
        if let recepit_id = dictInner["_id"] as? Int{
            cell.lblReceipt.text = "Receipt #: \(recepit_id)"
        }
        
        if let paymentFrom = dictInner["paymentFrom"] as? NSDictionary{
            if let strTitle = paymentFrom["title"] as? String{
                cell.lblPmt.text = "Pmt Method: \(strTitle)"
            }
        }
        
        let comments = dictInner["comments"] as! String
        let refrenceNumber = dictInner["refrenceNumber"] as! String
        cell.lblCommt.text = "Comments/Ref: \(comments) \(refrenceNumber)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view_a = tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as! AccordionHeaderView
        
        let dictMain = self.arrPayPerfHeader[section] as! NSDictionary
        if let daysPastDue = dictMain["daysPastDue"] as? Int
        {
            view_a.lblDaysLate.text = "\(daysPastDue)"
            if(daysPastDue >= 120){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha:1.0)
            }
            else if(daysPastDue >= 90 && daysPastDue < 120){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha:1.0)
            }
            else if(daysPastDue <= 90 && daysPastDue > 60){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xD47300, alpha:1.0)
            }
            else if(daysPastDue < 60 && daysPastDue > 30){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xD47300, alpha:1.0)
            }
                
            else if(daysPastDue <= 30 && daysPastDue > 15){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha:1.0)
            }
            else if(daysPastDue <= 15 && daysPastDue > 0){
                
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha:1.0)
            }
            else if(daysPastDue <= 0){
                view_a.view_bg.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha:1.0)
            }
        }
        
        if let totalDue = dictMain["totalDue"] as? NSNumber
        {
            view_a.lblAmt.text = "$ \(totalDue)"
        }
        
        if let dueDate = dictMain["dueDate"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            let result = formatter.string(from: date!)
            view_a.lblDueOn.text = result
        }
        
        if let paidDate = dictMain["paidDate"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let date = Date.dateFromISOString(string: paidDate)
            let result = formatter.string(from: date!)
            view_a.lblPstedOn.text = result
        }
        view_a.lblHistryTier.text = "0"
        
        if let paymentType = dictMain["paymentType"] as? NSDictionary{
            if let strTitle = paymentType["title"] as? String{
                let array = strTitle.components(separatedBy: " ")
                view_a.lblType.text = "\(array[0].first!) \(array[1].first!)"
            }
        }
        
        return view_a
    }
}

// MARK: - <FZAccordionTableViewDelegate> -

extension paymentPerformVC : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool
    {
        return true
    }
}

class perfmTblCell : UITableViewCell
{
    @IBOutlet weak var lblCollected : UILabel!
    @IBOutlet weak var lblCommt : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblReceipt : UILabel!
    @IBOutlet weak var lblPmt : UILabel!
}
