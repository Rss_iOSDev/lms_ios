//
//  chargeOffVC.swift
//  LMS
//
//  Created by Apple on 01/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class chargeOffVC: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    
    @IBOutlet weak var lbltax_AdjAcc : UILabel!
    @IBOutlet weak var lbltax_AdjOther : UILabel!
    @IBOutlet weak var lbltax_Currco : UILabel!
    @IBOutlet weak var lbltax_Orignlco : UILabel!
    @IBOutlet weak var lbltax_recovries : UILabel!
    
    @IBOutlet weak var lbltotal_AdjAcc : UILabel!
    @IBOutlet weak var lbltotal_AdjOther : UILabel!
    @IBOutlet weak var lbltotal_Currco : UILabel!
    @IBOutlet weak var lbltotal_Orignlco : UILabel!
    @IBOutlet weak var lbltotal_recovries : UILabel!
    
    @IBOutlet weak var lblbal_AdjAcc : UILabel!
    @IBOutlet weak var lblbal_AdjOther : UILabel!
    @IBOutlet weak var lblbal_Currco : UILabel!
    @IBOutlet weak var lblbal_Orignlco : UILabel!
    @IBOutlet weak var lblbal_recovries : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Globalfunc.showLoaderView(view: self.view)
        self.call_getChargeOffApi()
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension chargeOffVC
{
    func call_getChargeOffApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                
                let acct_id = dataD["_id"] as! Int
                
                let Str_url = "\(Constant.chargeOff)?account_id=\(acct_id)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary
                            {
                                self.parseDataonView(response: response)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func parseDataonView( response : NSDictionary)
    {
        print(response)
        
        if let data = response["data"] as? NSDictionary{
            if let Tax = data["Tax"] as? NSDictionary{
                
                if let adjustedAcv = Tax["adjustedAcv"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedAcv).rounded(toPlaces: 2)
                    self.lbltax_AdjAcc.text = "$ \(convert)"
                }
                if let originalCo = Tax["originalCo"] as? NSNumber
                {
                    let convert = Double(truncating: originalCo).rounded(toPlaces: 2)
                    self.lbltax_Orignlco.text = "$ \(convert)"
                }
                if let adjustedOther = Tax["adjustedOther"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedOther).rounded(toPlaces: 2)
                    self.lbltax_AdjOther.text = "$ \(convert)"
                }
                
                if let currentCo = Tax["currentCo"] as? NSNumber
                {
                    let convert = Double(truncating: currentCo).rounded(toPlaces: 2)
                    self.lbltax_Currco.text = "$ \(convert)"
                }
                
                if let recoveries = Tax["recoveries"] as? NSNumber
                {
                    let convert = Double(truncating: recoveries).rounded(toPlaces: 2)
                    self.lbltax_recovries.text = "$ \(convert)"
                }
                
            }
            
            if let Total = data["Total"] as? NSDictionary{
                
                if let adjustedAcv = Total["adjustedAcv"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedAcv).rounded(toPlaces: 2)
                    self.lbltotal_AdjAcc.text = "$ \(convert)"
                }
                
                if let originalCo = Total["originalCo"] as? NSNumber
                {
                    let convert = Double(truncating: originalCo).rounded(toPlaces: 2)
                    self.lbltotal_Orignlco.text = "$ \(convert)"
                }
                
                if let adjustedOther = Total["adjustedOther"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedOther).rounded(toPlaces: 2)
                    self.lbltotal_AdjOther.text = "$ \(convert)"
                }
                
                if let currentCo = Total["currentCo"] as? NSNumber
                {
                    let convert = Double(truncating: currentCo).rounded(toPlaces: 2)
                    self.lbltotal_Currco.text = "$ \(convert)"
                }
                
                if let recoveries = Total["recoveries"] as? NSNumber
                {
                    let convert = Double(truncating: recoveries).rounded(toPlaces: 2)
                    self.lbltotal_recovries.text = "$ \(convert)"
                }
            }
            
            if let balance = data["balance"] as? NSDictionary{
                
                if let adjustedAcv = balance["adjustedAcv"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedAcv).rounded(toPlaces: 2)
                    self.lblbal_AdjAcc.text = "$ \(convert)"
                }
                
                if let originalCo = balance["originalCo"] as? NSNumber
                {
                    let convert = Double(truncating: originalCo).rounded(toPlaces: 2)
                    self.lblbal_Orignlco.text = "$ \(convert)"
                }
                
                if let adjustedOther = balance["adjustedOther"] as? NSNumber
                {
                    let convert = Double(truncating: adjustedOther).rounded(toPlaces: 2)
                    self.lblbal_AdjOther.text = "$ \(convert)"
                }
                
                if let currentCo = balance["currentCo"] as? NSNumber
                {
                    let convert = Double(truncating: currentCo).rounded(toPlaces: 2)
                    self.lblbal_Currco.text = "$ \(convert)"
                }
                
                if let recoveries = balance["recoveries"] as? NSNumber
                {
                    let convert = Double(truncating: recoveries).rounded(toPlaces: 2)
                    self.lblbal_recovries.text = "$ \(convert)"
                }
            }
        }
        

        

    }
}
