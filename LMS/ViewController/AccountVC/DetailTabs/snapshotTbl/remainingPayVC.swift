//
//  remainingPayVC.swift
//  LMS
//
//  Created by Apple on 14/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class remainingPayVC: UIViewController {
    
    @IBOutlet weak var tblRemainingPay : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var arrRemainingPay : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblRemainingPay.isHidden = true
        self.lblStatus.text = ""
        
        self.tblRemainingPay.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        self.call_getReainingPayApi()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension remainingPayVC {
    
    func call_getReainingPayApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                
                let acct_id = dataD["_id"] as! Int
                
                let Str_url = "\(Constant.remaingPay)?id=\(acct_id)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? NSDictionary
                                {
                                    if let arr = data["paymentSchedule"] as? [NSDictionary]
                                    {
                                        if(arr.count > 0){
                                            for i in 0..<arr.count{
                                                let dictA = arr[i]
                                                self.arrRemainingPay.add(dictA)
                                            }
                                            self.tblRemainingPay.isHidden = false
                                            self.lblStatus.text = "Remaining Payments"
                                            self.tblRemainingPay.reloadData()
                                        }
                                        else{
                                            self.tblRemainingPay.isHidden = true
                                            self.lblStatus.text = "No Records found"
                                        }
                                    }
                                }
                                

                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
                
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}


extension remainingPayVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRemainingPay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblRemainingPay.dequeueReusableCell(withIdentifier: "tblRemainingPay") as! tblRemainingPay
        let dict = self.arrRemainingPay[indexPath.row] as! NSDictionary
        
        Globalfunc.print(object:dict)
        
        if let desc = dict["desc"] as? String
        {
            cell.lblDescription.text = desc
        }
        
        if let dueDate = dict["dueDate"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            let date = Date.dateFromISOString(string: dueDate)
            let result = formatter.string(from: date!)
            cell.lblDueDate.text = result
        }
        
        if let daysPastDue = dict["daysPastDue"] as? Int{
            cell.lblDaysPAst.text = "\(daysPastDue)"
        
            if(daysPastDue >= 120){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha:1.0)
            }
            else if(daysPastDue >= 90 && daysPastDue < 120){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha:1.0)
            }
            else if(daysPastDue <= 90 && daysPastDue > 60){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xD47300, alpha:1.0)
            }
            else if(daysPastDue < 60 && daysPastDue > 30){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xD47300, alpha:1.0)
            }
                
            else if(daysPastDue <= 30 && daysPastDue > 15){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha:1.0)
            }
            else if(daysPastDue <= 15 && daysPastDue > 0){
                
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha:1.0)
            }
            else if(daysPastDue <= 0){
                cell.viewbg.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha:1.0)
            }
        }
        
        if let lateFee = dict["lateFee"] as? Int{
            cell.lblLAteFee.text = "$ \(lateFee)"
        }
        
        if let stillDue = dict["stillDue"] as? NSNumber{
            cell.lblTotalDue.text = "$ \(stillDue)"
        }
        
        if let regularDue = dict["regularDue"] as? NSNumber{
            cell.lblPmtAmt.text = "$ \(regularDue)"
        }
        
        return cell
    }
}

class tblRemainingPay : UITableViewCell
{
    @IBOutlet weak var lblDueDate : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDaysPAst : UILabel!
    @IBOutlet weak var lblPmtAmt : UILabel!
    @IBOutlet weak var lblLAteFee : UILabel!
    @IBOutlet weak var lblTotalDue : UILabel!
    
    @IBOutlet weak var lblTitle_1 : UILabel!
    @IBOutlet weak var lblTitle_2 : UILabel!
    @IBOutlet weak var lblTitle_3 : UILabel!
    @IBOutlet weak var lblTitle_4 : UILabel!
    @IBOutlet weak var lblTitle_5 : UILabel!
    @IBOutlet weak var lblTitle_6 : UILabel!
    
    @IBOutlet weak var viewbg : UIView!
}
