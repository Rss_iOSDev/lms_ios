//
//  transactionSnapVC.swift
//  LMS
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class transactionSnapVC: UIViewController {
  
    @IBOutlet weak var tblTransactn : UITableView!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var arrTransactionList : [NSDictionary] = []
    var selectedCell = [IndexPath]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblTransactn.estimatedRowHeight = 250
        self.call_getTransactionListApi()
    }
    
    @IBAction func backBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension transactionSnapVC {
    
    func call_getTransactionListApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                
                let acct_id = dataD["_id"] as! Int
                
                let Str_url = "\(Constant.transac_url)/accid?account_id=\(acct_id)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                
                                Globalfunc.print(object:response)
                                
                                if let data = response["data"] as? NSDictionary {
                                    if let arr = data["transaction"] as? [NSDictionary]{
                                        if(arr.count > 0){
                                            for i in 0..<arr.count{
                                                let dictA = arr[i]
                                                self.arrTransactionList.append(dictA)
                                            }
                                            self.tblTransactn.reloadData()
                                        }
                                        else{
                                            self.arrTransactionList = []
                                            self.tblTransactn.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension transactionSnapVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrTransactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblTransactn.dequeueReusableCell(withIdentifier: "snapTblTrans") as! snapTblTrans
        let dict = self.arrTransactionList[indexPath.row]
        Globalfunc.print(object:dict)
        
        let first_name = dict["first_name"] as! String
        let last_name = dict["last_name"] as! String
        cell.lblPostedBty.text = "Posted By: \(first_name) \(last_name)"
        
        if let postDate = dict["postDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: postDate)
            let createFormat = formatter.string(from: date!)
            cell.lblPostDate.text = "Post Date : \(createFormat)"
        }
        
        if let effectiveDate = dict["effectiveDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: effectiveDate)
            let createFormat = formatter.string(from: date!)
            cell.lblEffctDate.text = "Effective Date : \(createFormat)"
        }
        
        if let begAcctBal = dict["begAcctBal"] as? NSNumber
        {
            cell.lblBeg_accBal.text = "Beg Acct Balance : $\(begAcctBal)"
        }
        
        if let amountReceived = dict["amountReceived"] as? NSNumber
        {
            cell.lblCollected.text = "Amount Collected : $\(amountReceived)"
        }
        
        if let adjusted = dict["adjusted"] as? NSNumber
        {
            cell.lblAdjusted.text = "Adjusted Amount : $\(adjusted)"
        }
        
        if let endAcctBal = dict["endAcctBal"] as? NSNumber
        {
            cell.lblEnd_accBal.text = "End Acct Balance : $\(endAcctBal)"
        }
        
        if let transDesc = dict["paymentFrom"] as? String
        {
            cell.lblPymntMthd.text = "Payment Method : \(transDesc)"
        }
        
        if let comments = dict["comments"] as? String
        {
            cell.lblComments.text = "Comments : \(comments)"
        }
        
        if let refrenceNumber = dict["refrenceNumber"] as? String
        {
            cell.lblRef.text = "Ref : \(refrenceNumber)"
        }
        
        if selectedCell.contains(indexPath) {
            cell.viewExpand.isHidden = false
        }
        else {
            cell.viewExpand.isHidden = true
            
        }
        
        if let downPayment = dict["downPayment"] as? NSDictionary
        {
            let adjusted = downPayment["adjusted"] as? NSNumber
            cell.lblDownpay_adjusted.text = "$\(adjusted!)"
            
            let begBal = downPayment["begBal"] as? NSNumber
            cell.lblDownpay_Begbal.text = "$\(begBal!)"
            
            let colleted = downPayment["colleted"] as? NSNumber
            cell.lblDownpay_collected.text = "$\(colleted!)"
            
            let endBal = downPayment["endBal"] as? NSNumber
            cell.lblDownpay_Endbal.text = "$\(endBal!)"
            
            let rebated = downPayment["rebated"] as? NSNumber
            cell.lblDownpay_rebated.text = "$\(rebated!)"
        }
        
        if let interest = dict["interest"] as? NSDictionary
        {
            let adjusted = interest["adjusted"] as? NSNumber
            cell.lblIntrst_adjusted.text = "$\(adjusted!)"
            
            let begBal = interest["begBal"] as? NSNumber
            cell.lblIntrst_Begbal.text = "$\(begBal!)"
            
            let colleted = interest["colleted"] as? NSNumber
            cell.lblIntrst_collected.text = "$\(colleted!)"
            
            let endBal = interest["endBal"] as? NSNumber
            cell.lblIntrst_Endbal.text = "$\(endBal!)"
            
            let rebated = interest["rebated"] as? NSNumber
            cell.lblIntrst_rebated.text = "$\(rebated!)"
        }
        
        if let lateFees = dict["lateFees"] as? NSDictionary
        {
            let adjusted = lateFees["adjusted"] as? NSNumber
            cell.lblLatefees_adjusted.text = "$\(adjusted!)"
            
            let begBal = lateFees["begBal"] as? NSNumber
            cell.lblLatefees_Begbal.text = "$\(begBal!)"
            
            let colleted = lateFees["colleted"] as? NSNumber
            cell.lblLatefees_collected.text = "$\(colleted!)"
            
            let endBal = lateFees["endBal"] as? NSNumber
            cell.lblLatefees_Endbal.text = "$\(endBal!)"
            
            let rebated = lateFees["rebated"] as? NSNumber
            cell.lblLatefees_rebated.text = "$\(rebated!)"
        }
        
        if let miscFees = dict["miscFees"] as? NSDictionary
        {
            let adjusted = miscFees["adjusted"] as? NSNumber
            cell.lblmiscFees_adjusted.text = "$\(adjusted!)"
            
            let begBal = miscFees["begBal"] as? NSNumber
            cell.lblmiscFees_Begbal.text = "$\(begBal!)"
            
            let colleted = miscFees["colleted"] as? NSNumber
            cell.lblmiscFees_collected.text = "$\(colleted!)"
            
            let endBal = miscFees["endBal"] as? NSNumber
            cell.lblmiscFees_Endbal.text = "$\(endBal!)"
            
            let rebated = miscFees["rebated"] as? NSNumber
            cell.lblmiscFees_rebated.text = "$\(rebated!)"
        }
        
        if let nonEarnPrin = dict["nonEarnPrin"] as? NSDictionary
        {
            let adjusted = nonEarnPrin["adjusted"] as? NSNumber
            cell.lblnonEarnPrin_adjusted.text = "$\(adjusted!)"
            
            let begBal = nonEarnPrin["begBal"] as? NSNumber
            cell.lblnonEarnPrin_Begbal.text = "$\(begBal!)"
            
            let colleted = nonEarnPrin["colleted"] as? NSNumber
            cell.lblnonEarnPrin_collected.text = "$\(colleted!)"
            
            let endBal = nonEarnPrin["endBal"] as? NSNumber
            cell.lblnonEarnPrin_Endbal.text = "$\(endBal!)"
            
            let rebated = nonEarnPrin["rebated"] as? NSNumber
            cell.lblnonEarnPrin_rebated.text = "$\(rebated!)"
        }
        
        if let prin = dict["prin"] as? NSDictionary
        {
            let adjusted = prin["adjusted"] as? NSNumber
            cell.lblprin_adjusted.text = "$\(adjusted!)"
            
            let begBal = prin["begBal"] as? NSNumber
            cell.lblprin_Begbal.text = "$\(begBal!)"
            
            let colleted = prin["colleted"] as? NSNumber
            cell.lblprin_collected.text = "$\(colleted!)"
            
            let endBal = prin["endBal"] as? NSNumber
            cell.lblprin_Endbal.text = "$\(endBal!)"
            
            let rebated = prin["rebated"] as? NSNumber
            cell.lblprin_rebated.text = "$\(rebated!)"
        }
        
        if let salesTax = dict["salesTax"] as? NSDictionary
        {
            let adjusted = salesTax["adjusted"] as? NSNumber
            cell.lblsalesTax_adjusted.text = "$\(adjusted!)"
            
            let begBal = salesTax["begBal"] as? NSNumber
            cell.lblsalesTax_Begbal.text = "$\(begBal!)"
            
            let colleted = salesTax["colleted"] as? NSNumber
            cell.lblsalesTax_collected.text = "$\(colleted!)"
            
            let endBal = salesTax["endBal"] as? NSNumber
            cell.lblsalesTax_Endbal.text = "$\(endBal!)"
            
            let rebated = salesTax["rebated"] as? NSNumber
            cell.lblsalesTax_rebated.text = "$\(rebated!)"
        }
        
        if let unEarnedInt = dict["unEarnedInt"] as? NSDictionary
        {
            let adjusted = unEarnedInt["adjusted"] as? NSNumber
            cell.lblunEarnedInt_adjusted.text = "$\(adjusted!)"
            
            let begBal = unEarnedInt["begBal"] as? NSNumber
            cell.lblunEarnedInt_Begbal.text = "$\(begBal!)"
            
            let colleted = unEarnedInt["colleted"] as? NSNumber
            cell.lblunEarnedInt_collected.text = "$\(colleted!)"
            
            let endBal = unEarnedInt["endBal"] as? NSNumber
            cell.lblunEarnedInt_Endbal.text = "$\(endBal!)"
            
            let rebated = unEarnedInt["rebated"] as? NSNumber
            cell.lblunEarnedInt_rebated.text = "$\(rebated!)"
        }
        
        cell.btnReceipt.tag = indexPath.row
        cell.btnReceipt.addTarget(self, action: #selector(clickonReceiptBtn(_:)), for: .touchUpInside)
        
        cell.btnEmail.tag = indexPath.row
        cell.btnEmail.addTarget(self, action: #selector(clickonEmailBtn(_:)), for: .touchUpInside)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tblTransactn.cellForRow(at: indexPath) as! snapTblTrans
        
        
        if selectedCell.contains(indexPath) {
            
            selectedCell.remove(at: selectedCell.index(of: indexPath)!)
            cell.viewExpand.isHidden = true
            tblTransactn.reloadData()
            // tblTransactn.estimatedRowHeight = 250
            //tblTransactn.rowHeight = cell.viewMain.frame.height + 10
        }
        else{
            selectedCell.append(indexPath)
            cell.viewExpand.isHidden = false
            tblTransactn.reloadData()
            // tblTransactn.rowHeight = cell.viewExpand.frame.height + cell.viewMain.frame.height + 10
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCell.contains(indexPath) {
            return 740
        }
        else{
            return 265
        }
    }
    
    @objc func clickonReceiptBtn(_ sender: UIButton)
    {
        let dict = self.arrTransactionList[sender.tag]
        let _id = dict["_id"] as! Int
        transactionId = _id
        isReceiptTyp = "transaction"
        isEmailAttach = "false"
        self.presentViewControllerasPopup("receiptViewController", strStoryName: "colletralStory")
    }
    
    @objc func clickonEmailBtn(_ sender: UIButton)
    {
        userDef.removeObject(forKey: "attach_receipt")
        let dict = self.arrTransactionList[sender.tag]
        let _id = dict["_id"] as! Int
        transactionId = _id
        isReceiptTyp = "transaction"
        isEmailAttach = "true"
        
        let story = UIStoryboard.init(name: "colletralStory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "receiptViewController") as! receiptViewController
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true,completion: nil)


    }
    
}

class snapTblTrans : UITableViewCell
{
    
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var viewExpand : UIView!
    
    @IBOutlet weak var lblPostedBty : UILabel!
    @IBOutlet weak var lblPostDate : UILabel!
    @IBOutlet weak var lblEffctDate : UILabel!
    @IBOutlet weak var lblBeg_accBal : UILabel!
    @IBOutlet weak var lblCollected : UILabel!
    @IBOutlet weak var lblAdjusted : UILabel!
    @IBOutlet weak var lblEnd_accBal : UILabel!
    @IBOutlet weak var lblPymntMthd : UILabel!
    @IBOutlet weak var lblComments : UILabel!
    @IBOutlet weak var lblRef : UILabel!
    
    //downpayment
    
    @IBOutlet weak var lblDownpay_Begbal : UILabel!
    @IBOutlet weak var lblDownpay_collected : UILabel!
    @IBOutlet weak var lblDownpay_adjusted : UILabel!
    @IBOutlet weak var lblDownpay_rebated : UILabel!
    @IBOutlet weak var lblDownpay_Endbal : UILabel!
    
    //intrest
    
    @IBOutlet weak var lblIntrst_Begbal : UILabel!
    @IBOutlet weak var lblIntrst_collected : UILabel!
    @IBOutlet weak var lblIntrst_adjusted : UILabel!
    @IBOutlet weak var lblIntrst_rebated : UILabel!
    @IBOutlet weak var lblIntrst_Endbal : UILabel!
    
    //lateFees
    
    @IBOutlet weak var lblLatefees_Begbal : UILabel!
    @IBOutlet weak var lblLatefees_collected : UILabel!
    @IBOutlet weak var lblLatefees_adjusted : UILabel!
    @IBOutlet weak var lblLatefees_rebated : UILabel!
    @IBOutlet weak var lblLatefees_Endbal : UILabel!
    
    //misc fees
    
    @IBOutlet weak var lblmiscFees_Begbal : UILabel!
    @IBOutlet weak var lblmiscFees_collected : UILabel!
    @IBOutlet weak var lblmiscFees_adjusted : UILabel!
    @IBOutlet weak var lblmiscFees_rebated : UILabel!
    @IBOutlet weak var lblmiscFees_Endbal : UILabel!
    
    //nonEarnPrin
    
    @IBOutlet weak var lblnonEarnPrin_Begbal : UILabel!
    @IBOutlet weak var lblnonEarnPrin_collected : UILabel!
    @IBOutlet weak var lblnonEarnPrin_adjusted : UILabel!
    @IBOutlet weak var lblnonEarnPrin_rebated : UILabel!
    @IBOutlet weak var lblnonEarnPrin_Endbal : UILabel!
    
    //prin
    
    @IBOutlet weak var lblprin_Begbal : UILabel!
    @IBOutlet weak var lblprin_collected : UILabel!
    @IBOutlet weak var lblprin_adjusted : UILabel!
    @IBOutlet weak var lblprin_rebated : UILabel!
    @IBOutlet weak var lblprin_Endbal : UILabel!
    
    //salesTax
    
    @IBOutlet weak var lblsalesTax_Begbal : UILabel!
    @IBOutlet weak var lblsalesTax_collected : UILabel!
    @IBOutlet weak var lblsalesTax_adjusted : UILabel!
    @IBOutlet weak var lblsalesTax_rebated : UILabel!
    @IBOutlet weak var lblsalesTax_Endbal : UILabel!
    
    //unEarnedInt
    
    @IBOutlet weak var lblunEarnedInt_Begbal : UILabel!
    @IBOutlet weak var lblunEarnedInt_collected : UILabel!
    @IBOutlet weak var lblunEarnedInt_adjusted : UILabel!
    @IBOutlet weak var lblunEarnedInt_rebated : UILabel!
    @IBOutlet weak var lblunEarnedInt_Endbal : UILabel!
    
    @IBOutlet weak var btnReceipt : UIButton!
    @IBOutlet weak var btnEmail : UIButton!
    
}
