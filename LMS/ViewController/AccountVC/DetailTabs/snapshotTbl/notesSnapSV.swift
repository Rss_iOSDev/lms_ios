//
//  notesSnapSV.swift
//  LMS
//
//  Created by Apple on 15/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class notesSnapSV: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    @IBOutlet weak var tblNotes : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var scrollItems : UIScrollView!
    
    @IBOutlet weak var lblCalOut : UILabel!
    @IBOutlet weak var lblCalIn : UILabel!
    @IBOutlet weak var lblCollector : UILabel!
    @IBOutlet weak var lblSystem : UILabel!
    @IBOutlet weak var lblIp : UILabel!
    @IBOutlet weak var lblTp : UILabel!
    
    var acctId : Int?
    
    
    var arrNotesData : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblNotes.isHidden = true
        self.lblStatus.text = ""
        
        self.tblNotes.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                acctId = (dataD["_id"] as! Int)
                
                let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)"
                self.call_getNotes(strUrl: Str_url, strCheck: "sy")
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension notesSnapSV {
    
    func call_getNotes(strUrl : String, strCheck : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? NSDictionary
                        {
                            if let total = data["total"] as? NSDictionary
                            {
                                if let ci = total["ci"] as? Int{
                                    self.lblCalIn.text = "\(ci)"
                                }
                                if let cl = total["cl"] as? Int{
                                    self.lblCollector.text = "\(cl)"
                                }
                                if let co = total["co"] as? Int{
                                    self.lblCalOut.text = "\(co)"
                                }
                                if let ip = total["ip"] as? Int{
                                    self.lblIp.text = "\(ip)"
                                }
                                if let ptp = total["ptp"] as? Int{
                                    self.lblTp.text = "\(ptp)"
                                }
                                if let sy = total["sy"] as? Int{
                                    self.lblSystem.text = "\(sy)"
                                }
                                
                                if(strCheck == "sy"){
                                    self.lblSystem.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                                    self.scrollItems.setContentOffset(CGPoint(x: 450, y: 0), animated: true)
                                }
                            }
                            
                            self.arrNotesData = []
                            if let arr = data["notes"] as? [NSDictionary]
                            {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrNotesData.add(dictA)
                                    }
                                    self.tblNotes.isHidden = false
                                    self.lblStatus.text = "Notes"
                                    self.tblNotes.reloadData()
                                }
                                else{
                                    self.tblNotes.isHidden = true
                                    self.lblStatus.text = "No Records found"
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
        
    }
}

extension notesSnapSV {
    
    @IBAction func clikcONTotal(_ sender : UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        self.tblNotes.isHidden = true
        self.lblStatus.text = ""
        
        
        if(sender.tag == 10){
            
            self.lblCalOut.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            
            self.lblCalIn.textColor = .black
            self.lblCollector.textColor = .black
            self.lblSystem.textColor = .black
            self.lblTp.textColor = .black
            self.lblIp.textColor = .black
            
            self.scrollItems.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=co"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
        else if(sender.tag == 20){
            
            self.lblCalOut.textColor = .black
            self.lblCalIn.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            self.lblCollector.textColor = .black
            self.lblSystem.textColor = .black
            self.lblTp.textColor = .black
            self.lblIp.textColor = .black
            
            self.scrollItems.setContentOffset(CGPoint(x: 150, y: 0), animated: true)
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=ci"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
        else if(sender.tag == 30){
            
            self.lblCalOut.textColor = .black
            self.lblCalIn.textColor = .black
            self.lblCollector.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            self.lblSystem.textColor = .black
            self.lblTp.textColor = .black
            self.lblIp.textColor = .black
            
            self.scrollItems.setContentOffset(CGPoint(x: 300, y: 0), animated: true)
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=cl"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
        else if(sender.tag == 40){
            
            self.lblCalOut.textColor = .black
            self.lblCalIn.textColor = .black
            self.lblCollector.textColor = .black
            self.lblSystem.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            self.lblTp.textColor = .black
            self.lblIp.textColor = .black
            
            self.scrollItems.setContentOffset(CGPoint(x: 450, y: 0), animated: true)
            
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=sy"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
        else if(sender.tag == 50){
            
            self.lblCalOut.textColor = .black
            self.lblCalIn.textColor = .black
            self.lblCollector.textColor = .black
            self.lblSystem.textColor = .black
            self.lblIp.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            self.lblTp.textColor = .black
            
            self.scrollItems.setContentOffset(CGPoint(x: 600, y: 0), animated: true)
            
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=ip"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
        else if(sender.tag == 60){
            
            self.lblCalOut.textColor = .black
            self.lblCalIn.textColor = .black
            self.lblCollector.textColor = .black
            self.lblSystem.textColor = .black
            self.lblIp.textColor = .black
            self.lblTp.textColor = self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
            
            self.scrollItems.setContentOffset(CGPoint(x: 750, y: 0), animated: true)
            
            
            let Str_url = "\(Constant.snap_notes)?account_id=\(acctId!)&type=ptp"
            self.call_getNotes(strUrl: Str_url, strCheck: "")
        }
    }
}


extension notesSnapSV : UITableViewDelegate , UITableViewDataSource
{
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblNotes.dequeueReusableCell(withIdentifier: "tblSnapNotes") as! tblSnapNotes
        let dict = self.arrNotesData[indexPath.row] as! NSDictionary
        
        if let sub_type = dict["sub_type"] as? String{
            
            if(sub_type == "promise_to_pay"){
                cell.lblPromsDesc.isHidden = false
                cell.lblPromsAmt.isHidden = false
                cell.lblPromsDate.isHidden = false
                cell.lblPromsStatus.isHidden = false
                
                cell.lblPromsDesc_t.isHidden = false
                cell.lblPromsDate_t.isHidden = false
                cell.lblPromsAmt_t.isHidden = false
                cell.lblPromsStatus_t.isHidden = false
                
                if let description = dict["description"] as? NSArray{
                    let descDict = description[1] as! NSDictionary
                    
                    if let PromiseDate = descDict["PromiseDate"] as? String{
                        cell.lblPromsDate.text = PromiseDate
                    }
                    
                    if let PromiseDesc = descDict["PromiseDesc"] as? String{
                        cell.lblPromsDesc.text = PromiseDesc
                    }
                    
                    if let PromiseAmt = descDict["PromiseAmt"] as? String{
                        cell.lblPromsAmt.text = PromiseAmt
                    }
                    
                    if let PromiseStatus = descDict["PromiseStatus"] as? String{
                        cell.lblPromsStatus.text = PromiseStatus
                        
                        if(PromiseStatus == "Promise Kept"){
                            
                            cell.lblPromsDate.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsDesc.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsStatus.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsAmt.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            
                            cell.lblPromsDate_t.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsDesc_t.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsStatus_t.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.lblPromsAmt_t.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            
                            cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                            cell.imgBorder.layer.borderColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0).cgColor
                            cell.lblInnerTxt.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                        }
                        else if(PromiseStatus == "Promise Open"){
                            
                            cell.lblPromsDate.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsDesc.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsStatus.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsAmt.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            
                            cell.lblPromsDate_t.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsDesc_t.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsStatus_t.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.lblPromsAmt_t.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            
                            cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                            cell.imgBorder.layer.borderColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0).cgColor
                            cell.lblInnerTxt.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                        }
                        else if(PromiseStatus == "Promise Broken"){
                            cell.lblPromsDate.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsDesc.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsStatus.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsAmt.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            
                            cell.lblPromsDate_t.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsDesc_t.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsStatus_t.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.lblPromsAmt_t.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            
                            cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            cell.imgBorder.layer.borderColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0).cgColor
                            cell.lblInnerTxt.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                            
                        }
                    }
                }
            }
            else{
                cell.lblPromsDesc.isHidden = true
                cell.lblPromsAmt.isHidden = true
                cell.lblPromsDate.isHidden = true
                cell.lblPromsStatus.isHidden = true
                
                cell.lblPromsDesc_t.isHidden = true
                cell.lblPromsDate_t.isHidden = true
                cell.lblPromsAmt_t.isHidden = true
                cell.lblPromsStatus_t.isHidden = true
                
                cell.lblLine.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                cell.imgBorder.layer.borderColor = UIColor.black.cgColor
                cell.lblInnerTxt.textColor = .black
                
            }
        }
        
        if let dueDate = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy HH:mm a"
            let date = Date.dateFromISOString(string: dueDate)
            let result = formatter.string(from: date!)
            cell.lblDate.text = result
        }
        
        if let initials = dict["initials"] as? String{
            cell.lblInitials.text = initials
        }
        
        if let result = dict["result"] as? String{
            cell.lblResult.text = result
        }
        else{
            cell.lblResult.text = ""
        }
        
        if let description = dict["description"] as? NSArray{
            let desc = description[0] as! String
            cell.lblDescription.text = desc
        }
        
        if let user = dict["user"] as? String{
            
            if(user == "System Updated"){
                cell.lblImg.text = "SY"
            }
            else if(user == "Contact Out"){
                cell.lblImg.text = "CO"
            }
            else if(user == "Contact Note"){
                cell.lblImg.text = "CN"
            }
            else if(user == "Contact In"){
                cell.lblImg.text = "CI"
            }
            else if(user == "IP"){
                cell.lblImg.text = "IP"
            }
            else if(user == "Promise to Pay"){
                cell.lblImg.text = "PP"
            }
        }
        
        return cell
    }
}

class tblSnapNotes : UITableViewCell
{
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblInitials : UILabel!
    @IBOutlet weak var lblResult : UILabel!
    @IBOutlet weak var lblImg : UILabel!
    
    @IBOutlet weak var lblPromsDesc : UILabel!
    @IBOutlet weak var lblPromsDate : UILabel!
    @IBOutlet weak var lblPromsAmt : UILabel!
    @IBOutlet weak var lblPromsStatus : UILabel!
    
    @IBOutlet weak var lblPromsDesc_t : UILabel!
    @IBOutlet weak var lblPromsDate_t : UILabel!
    @IBOutlet weak var lblPromsAmt_t : UILabel!
    @IBOutlet weak var lblPromsStatus_t : UILabel!
    
    @IBOutlet weak var lblLine : UILabel!
    @IBOutlet weak var imgBorder : UIImageView!
    @IBOutlet weak var lblInnerTxt : UILabel!
    
}
