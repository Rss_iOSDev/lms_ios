//
//  receiptViewController.swift
//  LMS
//
//  Created by Apple on 25/09/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import PDFGenerator
import WebKit

var transactionId : Int?
var isReceiptTyp = ""
var isEmailAttach = ""

class receiptViewController: UIViewController {
    
    @IBOutlet weak var reportScrollview: UIScrollView!
    @IBOutlet weak var pdfView: UIView!
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet weak var btnTitle: UIButton!
    
    @IBOutlet weak var lbl_1: UILabel!
    @IBOutlet weak var lbl_2: UILabel!
    @IBOutlet weak var lbl_3: UILabel!
    @IBOutlet weak var lbl_4: UILabel!
    @IBOutlet weak var lbl_5: UILabel!
    @IBOutlet weak var lbl_6: UILabel!
    
    @IBOutlet weak var lbl_receiptId: UILabel!
    @IBOutlet weak var lbl_tranacDate: UILabel!
    @IBOutlet weak var lbl_effectiveDate: UILabel!
    
    @IBOutlet weak var lbl_address1: UILabel!
    @IBOutlet weak var lbl_address2: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_city: UILabel!
    @IBOutlet weak var lbl_country: UILabel!
    @IBOutlet weak var lbl_state: UILabel!
    
    @IBOutlet weak var lbl_loan: UILabel!
    @IBOutlet weak var lbl_account: UILabel!
    @IBOutlet weak var lbl_stock_Vin: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet weak var lbl_transPosted: UILabel!
    @IBOutlet weak var lbl_Notes: UILabel!
    
    @IBOutlet weak var tblTransData: MyOwnTableView!
    @IBOutlet weak var hgttblTransac: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_TotalTrans: UILabel!
    
    @IBOutlet weak var lbl_amtPaid: UILabel!
    @IBOutlet weak var lbl_amtReceived: UILabel!
    @IBOutlet weak var lbl_chngdue: UILabel!
    @IBOutlet weak var lbl_Paymthd: UILabel!
    @IBOutlet weak var lbl_Refrence: UILabel!
    @IBOutlet weak var lbl_deliveryMthd: UILabel!
    
    @IBOutlet weak var lbl_stillDueAftrTrans: UILabel!
    @IBOutlet weak var lbl_dueDate: UILabel!
    @IBOutlet weak var lbl_nextSchdPaymnt: UILabel!
    @IBOutlet weak var lbl_nextSchdPaymntDate: UILabel!
    
    
    @IBOutlet weak var lbl_prinBeforeTrans: UILabel!
    @IBOutlet weak var lbl_prinAfterTrans: UILabel!
    
    @IBOutlet weak var viewdp: UIView!
    @IBOutlet weak var hgtdp: NSLayoutConstraint!
    @IBOutlet weak var lbl_dpBeforeTrans: UILabel!
    @IBOutlet weak var lbl_dpAfterTrans: UILabel!
    
    @IBOutlet weak var viewsn: UIView!
    @IBOutlet weak var hgtsn: NSLayoutConstraint!
    @IBOutlet weak var lbl_sideNBeforeTrans: UILabel!
    @IBOutlet weak var lbl_sideNAfterTrans: UILabel!
    
    
    var arrtransData : [NSDictionary] = []
    
    var delegate: AttchmntDelegate?
    var attachDocArrList : [NSDictionary] = []
    
    var acctNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.pdfView.isHidden = true
        self.tblTransData.tableFooterView = UIView()
        
        self.viewdp.isHidden = true
        self.hgtdp.constant = 0
        
        self.viewsn.isHidden = true
        self.hgtsn.constant = 0
        
        
        self.tblTransData.estimatedRowHeight = 170
        self.tblTransData.rowHeight = UITableView.automaticDimension
        
        Globalfunc.showLoaderView(view: self.view)
        self.setDatafromDetail()
        
        if(isReceiptTyp == "transaction"){
            self.call_getReceiptdataApi()
        }
        else if(isReceiptTyp == "payment"){
            self.call_postReceiptDataApi()
        }
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPrintDocument(_ sender: UIButton) {
        Globalfunc.showLoaderView(view: self.view)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.convertToPdfandDownload()
        })
    }
    
    
    override func viewDidLayoutSubviews()
    {
        self.hgttblTransac.constant = self.tblTransData.contentSize.height + 10
    }
    
    
    func convertToPdfandDownload(){
        
        OperationQueue.main.addOperation {
            let dst = NSHomeDirectory() + "/test.pdf"
            try! PDFGenerator.generate(self.reportScrollview, to: dst)
            
            print(dst)
            
            
            if let buttonTitle = self.btnTitle.title(for: .normal) {
                print(buttonTitle)
                if(buttonTitle == "Send Email"){
                    
                    let dict : NSMutableDictionary = [:]
                    dict.setValue(self.acctNo, forKey: "accountNo")
                    dict.setValue("doc", forKey: "type")
                    dict.setValue(dst, forKey: "file_path")
                    
                    self.attachDocArrList.append(dict)
                    userDef.setValue(self.attachDocArrList, forKey: "attach_receipt")
                    
                    do {
                        let decoded  = userDef.object(forKey: "action_dict") as! Data
                        if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                            Globalfunc.print(object:responseDict)
                            OperationQueue.main.addOperation {
                                
                                if let loanAccount = responseDict["loanAccount"] as? NSDictionary{
                                    if let email = loanAccount["email"] as? String{
                                        if(email != ""){
                                            let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
                                            let dest = story.instantiateViewController(identifier: "email_sendVC") as! email_sendVC
                                            dest.account_no = self.acctNo
                                            dest.email = email
                                            dest.isFromReceipt = true
                                            dest.modalPresentationStyle = .fullScreen
                                            self.present(dest, animated: true, completion: nil)
                                        }
                                    }
                                }
                                
                                
                            }
                        }
                    } catch {
                        Globalfunc.print(object: "Couldn't read file.")
                    }
                }
                else if(buttonTitle == "Print This Receipt"){
                    let alert = UIAlertController(title: "Alert", message: "Pdf downlaoaded successfully", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                        let url = URL(fileURLWithPath: dst)
                        self.webView.load(URLRequest(url: url))
                        self.webView.allowsBackForwardNavigationGestures = true
                        
                        Globalfunc.hideLoaderView(view: self.view)
                        self.pdfView.isHidden = false
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
        }
    }
}

extension receiptViewController {
    
    func setDatafromDetail(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let account_no = dataD["account_no"] as! String
                self.lbl_account.text = "\(account_no)"
                self.acctNo = account_no
                
                
                let loan_no = dataD["loan_no"] as! String
                self.lbl_loan.text = "Loan: \(loan_no)"
                
                
                let loanVehicle = dataD["loanVehicle"] as! NSDictionary
                let vin = loanVehicle["vin"] as! String
                let stockNumber = loanVehicle["stockNumber"] as! String
                self.lbl_stock_Vin.text = "Stock #/VIN: \(stockNumber)/\(vin)"
                
                
                let make = loanVehicle["make"] as! String
                let model = loanVehicle["model"] as! String
                let year = loanVehicle["year"] as! Int
                self.lbl_description.text = "Description: \(year) \(make) \(model)"
                
                let loanBorrower = dataD["loanBorrower"] as! NSDictionary
                
                let first_name = loanBorrower["first_name"] as! String
                let last_name = loanBorrower["last_name"] as! String
                self.lbl_name.text = "\(first_name) \(last_name)"
                
                let loanAddress = dataD["loanAddress"] as! NSDictionary
                
                if let address1 = loanAddress["address1"] as? String
                {
                    self.lbl_address1.text = "\(address1)"
                }
                
                if let address2 = loanAddress["address2"] as? String
                {
                    self.lbl_address2.text = "\(address2)"
                }
                
                if let city = loanAddress["city"] as? String
                {
                    self.lbl_city.text = "\(city)"
                }
                
                if let country = loanAddress["country"] as? String
                {
                    self.lbl_country.text = "\(country)"
                }
                
                if let state = loanAddress["state"] as? String
                {
                    self.lbl_state.text = "\(state)"
                }
            }
        }
        catch{
            
        }
    }
    
    func call_postReceiptDataApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let acct_id = dataD["_id"] as! Int
                let  params = [ "account_id": acct_id,
                                "currentPageNumber": 1,
                                "defaultRecordsPage": 10] as [String : Any]
                Globalfunc.print(object:params)
                BaseApi.onResponsePostWithToken(url: Constant.transac_url, controller: self, parms: params) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            print(dict)
                            
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? NSDictionary
                                {
                                    if let transaction = data["transaction"] as? [NSDictionary]
                                    {
                                        let ditcTransac = transaction[0]
                                        let _id = ditcTransac["_id"] as! Int
                                        transactionId = _id
                                        
                                        self.call_getReceiptdataApi()
                                        
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
            
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
    }
    
    func call_getReceiptdataApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                
                
                self.lbl_receiptId.text = "\(transactionId!)"
                let acct_id = dataD["_id"] as! Int
                let Str_url = "\(Constant.receipt)?transaction_id=\(transactionId!)&account_id=\(acct_id)"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? NSDictionary
                                {
                                    if let transaction_data = data["transaction_data"] as? [NSDictionary]{
                                        if(transaction_data.count > 0){
                                            for i in 0..<transaction_data.count{
                                                let dictA = transaction_data[i] as! NSMutableDictionary
                                                dictA.setValue(false, forKey: "isTemp")
                                                self.arrtransData.append(dictA)
                                            }
                                            self.tblTransData.reloadData()
                                            //self.hgttblTransac.constant = self.tblTransData.contentSize.height
                                        }
                                        else{
                                            self.arrtransData = []
                                            self.tblTransData.reloadData()
                                            //self.hgttblTransac.constant = self.tblTransData.contentSize.height
                                        }
                                    }
                                    
                                    if let transaction = data["transaction"] as? NSDictionary
                                    {
                                        self.parseDataFromApi(transaction: transaction)
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
                
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    func parseDataFromApi(transaction: NSDictionary){
        
        if let effectiveDate = transaction["effectiveDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: effectiveDate)
            let createFormat = formatter.string(from: date!)
            self.lbl_effectiveDate.text = "\(createFormat)"
        }
        if let postDate = transaction["postDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: postDate)
            let createFormat = formatter.string(from: date!)
            self.lbl_tranacDate.text = "\(createFormat)"
        }
        
        if let comments = transaction["comments"] as? String
        {
            self.lbl_Notes.text = "\(comments)"
        }
        
        let strFname = user?.fname
        let strLname = user?.lname
        let strUid = user?._id
        self.lbl_transPosted.text = "\(String((strFname?.first)!))\(String((strLname?.first)!))\(strUid!)"
        
        if let amountReceived = transaction["amountReceived"] as? Double
        {
            self.lbl_TotalTrans.text = "$ \(amountReceived)"
        }
        
        let transactionDesc = transaction["transactionDesc"] as! NSDictionary
        let transactionId = transactionDesc["_id"] as! Int
        
        if (transactionId != 17)
        {
            if let amountPaying = transaction["amountPaying"] as? Double
            {
                self.lbl_amtPaid.text = "Amount Paid :$ \(amountPaying)"
            }
            
            if let amountReceived = transaction["amountReceived"] as? Double
            {
                self.lbl_amtReceived.text = "Amount Received :$ \(amountReceived)"
            }
        }
        
        if (transactionId == 0)
        {
            self.lbl_amtPaid.text = "Amount Paid :$0.00"
            self.lbl_amtReceived.text = "Amount Received :$0.00"
        }
        
        if let changeDue = transaction["changeDue"] as? Double
        {
            self.lbl_chngdue.text = "Change Due :$ \(changeDue)"
        }
        
        if let paymentFrom = transaction["paymentFrom"] as? String
        {
            self.lbl_Paymthd.text = "Payment Method :\(paymentFrom)"
        }
        
        if let refrenceNumber = transaction["refrenceNumber"] as? Int
        {
            self.lbl_Refrence.text = "Reference #:\(refrenceNumber)"
        }
        
        if let deliveryMethod = transaction["deliveryMethod"] as? String
        {
            self.lbl_deliveryMthd.text = "Delivery Method :\(deliveryMethod)"
        }
        
        let miscFees = transaction["miscFees"] as! NSDictionary
        let miscFees_endBal = miscFees["endBal"] as! Double
        
        if(transactionId == 6)
        {
            if let endBal = miscFees["endBal"] as? Double
            {
                self.lbl_stillDueAftrTrans.text = "$\(endBal)"
            }
            if let postDate = transaction["postDate"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: postDate)
                let createFormat = formatter.string(from: date!)
                self.lbl_dueDate.text = "\(createFormat)"
            }
        }
        
        let downPayment = transaction["downPayment"] as! NSDictionary
        let dpbegBal = downPayment["begBal"] as! Double
        let dpendBal = downPayment["endBal"] as! Double
        
        
        let stillDue = transaction["stillDue"] as! Double
        if(stillDue > 0 && transactionId != 12 && dpbegBal > 0 && transactionId == 1 || transactionId == 7)
        {
            self.lbl_stillDueAftrTrans.text = "$\(stillDue)"
            if let stillDueDate = transaction["stillDueDate"] as? String
            {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: stillDueDate)
                let createFormat = formatter.string(from: date!)
                self.lbl_dueDate.text = "\(createFormat)"
            }
        }
        
        if let nextPayment = transaction["nextPayment"] as? Double
        {
            self.lbl_nextSchdPaymnt.text = "$\(nextPayment)"
        }
        
        if let nextDate = transaction["nextDate"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: nextDate)
            let createFormat = formatter.string(from: date!)
            self.lbl_nextSchdPaymntDate.text = "\(createFormat)"
        }
        
        let prin = transaction["prin"] as! NSDictionary
        let prin_begBal = prin["begBal"] as! Double
        let prin_endBal = prin["endBal"] as! Double
        let prin_adjusted = prin["adjusted"] as! Double
        
        let salesTax = transaction["salesTax"] as! NSDictionary
        let salesTax_begBal = salesTax["begBal"] as! Double
        let salesTax_endBal = salesTax["endBal"] as! Double
        
        let nonEarnPrin = transaction["nonEarnPrin"] as! NSDictionary
        let nonEarn_begBal = nonEarnPrin["begBal"] as! Double
        let nonEarn_endBal = nonEarnPrin["endBal"] as! Double
        
        let lateFees = transaction["lateFees"] as! NSDictionary
        let lateFees_begBal = lateFees["begBal"] as! Double
        let lateFees_endBal = lateFees["endBal"] as! Double
        
        let unEarnedInt = transaction["unEarnedInt"] as! NSDictionary
        let unEarnedInt_begBal = unEarnedInt["begBal"] as! Double
        let unEarnedInt_endBal = unEarnedInt["endBal"] as! Double
        
        let prinBeforeTrans = prin_begBal + lateFees_begBal + nonEarn_begBal + salesTax_begBal + unEarnedInt_begBal
        self.lbl_prinBeforeTrans.text = "$\(Double(truncating: NSNumber(value: prinBeforeTrans)).rounded(toPlaces: 3))"
        
        if(transactionId != 6)
        {
            let prinAfterTrans = prin_endBal + miscFees_endBal + lateFees_endBal + nonEarn_endBal + salesTax_endBal + unEarnedInt_endBal
            self.lbl_prinAfterTrans.text = "$\(Double(truncating: NSNumber(value: prinAfterTrans)).rounded(toPlaces: 3))"
        }
        
        if(transactionId == 6)
        {
            let prinAfterTrans = prin_endBal + lateFees_endBal + nonEarn_endBal + salesTax_endBal + unEarnedInt_endBal
            self.lbl_prinAfterTrans.text = "$\(Double(truncating: NSNumber(value: prinAfterTrans)).rounded(toPlaces: 3))"
        }
        
        if(dpbegBal > 0){
            self.viewdp.isHidden = false
            self.hgtdp.constant = 150
            
            self.lbl_dpBeforeTrans.text = "$\(Double(truncating: NSNumber(value: dpbegBal)).rounded(toPlaces: 3))"
            self.lbl_dpAfterTrans.text = "$\(Double(truncating: NSNumber(value: dpendBal)).rounded(toPlaces: 3))"
        }
        else{
            self.viewdp.isHidden = true
            self.hgtdp.constant = 0
        }
        
        var sideBal : [NSDictionary] = []
        var totalSideBal = 0.0
        var amt = 0.0
        for i in 0..<self.arrtransData.count
        {
            let dict = self.arrtransData[i]
            if let balance = dict["balance"] as? NSDictionary
            {
                let sideNoteType = balance["sideNoteType"] as! Int
                if(sideNoteType != 1){
                    sideBal.append(dict)
                }
            }
        }
        print(sideBal)
        
        for i in 0..<sideBal.count{
            let dict = sideBal[i]
            if let balance = dict["balance"] as? NSDictionary
            {
                let regular_amount = balance["regular_amount"] as! Double
                amt += regular_amount
            }
        }
        totalSideBal = amt
        let sideNoteBalance = transaction["sideNoteBalance"] as! Double
        if(sideNoteBalance > 0){
            self.viewsn.isHidden = false
            self.hgtsn.constant = 150
            
            if(transactionId != 2 && transactionId != 8 && transactionId != 14 && transactionId != 23 && transactionId != 19 && transactionId != 18 && transactionId != 3 && transactionId != 4 && transactionId != 5 && transactionId != 9 && transactionId != 10 && transactionId != 11 && transactionId != 12 && transactionId != 13 && transactionId != 15 && transactionId != 16 && transactionId != 17 && transactionId != 20 && transactionId != 21 && transactionId != 22 && dpbegBal == 0 && prin_adjusted > 0 || (sideNoteBalance*1 + totalSideBal*1) > 0){
                
                if(sideNoteBalance == 0 && (sideNoteBalance + totalSideBal == 0))
                {
                    self.lbl_sideNBeforeTrans.text = "$\(prin_adjusted)"
                    self.lbl_sideNAfterTrans.text = "$\(prin_adjusted)"
                }
                if(sideNoteBalance > 0 || (sideNoteBalance + totalSideBal) != 0){
                    let total = sideNoteBalance + totalSideBal
                    self.lbl_sideNBeforeTrans.text = "$\(total)"
                    self.lbl_sideNAfterTrans.text = "$\(total)"
                }
            }
        }
        else{
            self.viewsn.isHidden = true
            self.hgtsn.constant = 0
        }
        if(isEmailAttach == "true"){
            self.btnTitle.setTitle("Send Email", for: .normal)
        }
        else if(isEmailAttach == "false"){
            self.btnTitle.setTitle("Print This Receipt", for: .normal)
        }
        
    }
}

extension receiptViewController : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrtransData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblTransData.dequeueReusableCell(withIdentifier: "tblTransData") as! tblTransData
        let dict = self.arrtransData[indexPath.row]
        Globalfunc.print(object:dict)
        
        if let payment = dict["payment"] as? NSDictionary
        {
            let title = payment["title"] as! String
            cell.lbl_Transtyp.text = "\(title)"
        }
        
        if let balance = dict["balance"] as? NSDictionary
        {
            let description = balance["description"] as! String
            cell.lbl_BalDesc.text = "\(description)"
        }
        
        let interest = dict["interest"] as! Double
        let principal = dict["principal"] as! Double
        let tax = dict["tax"] as! Double
        
        let amt = interest + principal + tax
        cell.lbl_Amt.text = "$ \(amt)"
        return cell
    }
}

class tblTransData: UITableViewCell
{
    @IBOutlet weak var lbl_Transtyp: UILabel!
    @IBOutlet weak var lbl_BalDesc: UILabel!
    @IBOutlet weak var lbl_Amt: UILabel!
}


class MyOwnTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
