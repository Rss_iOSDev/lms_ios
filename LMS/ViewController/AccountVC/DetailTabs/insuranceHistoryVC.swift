//
//  insuranceHistoryVC.swift
//  LMS
//
//  Created by Apple on 02/05/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class insuranceHistoryVC: UIViewController {
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var tblInsurance: UITableView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrInsuranceList : NSMutableArray = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblStatus.text = ""
        self.tblInsurance.isHidden = true
               
        self.tblInsurance.tableFooterView = UIView()
        
        self.call_getIns_HistoryApi()

    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

}

//Api --
extension insuranceHistoryVC {
    
    func call_getIns_HistoryApi()
    {
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
        if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
            
            let account_no = dataD["account_no"] as! String
            self.lblAcctNo.text = "Account No: \(account_no)"

            
            let accId_selected = dataD["_id"] as! Int
            let Str_url = "\(Constant.insurance_url)?account_id=\(accId_selected)"
            
            BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let data = dict as? NSDictionary {
                            if let arr = data["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrInsuranceList.add(dictA)
                                    }
                                    self.tblInsurance.isHidden = false
                                    self.lblStatus.text = "INSURANCE HISTORY"
                                    self.tblInsurance.reloadData()
                                }
                                else{
                                    self.tblInsurance.isHidden = true
                                    self.lblStatus.text = "INSURANCE HISTORY \n \n No Records found"
                                    
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }

    }
}

extension insuranceHistoryVC : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrInsuranceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblInsurance.dequeueReusableCell(withIdentifier: "tblInsuranceCell") as! tblInsuranceCell
        let dict = arrInsuranceList[indexPath.row] as! NSDictionary
        
        Globalfunc.print(object:dict)
        
//        if let repoNotes = dict["repoNotes"] as? String
//        {
//            cell.lblrepoNotes.text = repoNotes
//        }else{
//            if let repoCancelNotes = dict["repoCancelNotes"] as? String{
//                cell.lblrepoNotes.text = "Cancel repo - \(repoCancelNotes)"
//            }
//        }
//
//        if let created_at = dict["created_at"] as? String
//        {
//            let formatter = DateFormatter()
//            formatter.dateFormat = "MM-dd-yyyy"
//            let date = Date.dateFromISOString(string: created_at)
//            cell.lblPlaceout.text = formatter.string(from: date!)
//        }
//
//        if let effectiveDate = dict["effectiveDate"] as? String
//        {
//            let formatter = DateFormatter()
//            formatter.dateFormat = "MM-dd-yyyy"
//            let date = Date.dateFromISOString(string: effectiveDate)
//            cell.lblEffectDate.text = formatter.string(from: date!)
//        }
//
//        if let repoAgent = dict["repoAgent"] as? NSDictionary{
//
//            if let title = repoAgent["title"] as? String{
//                cell.lblrepoAgent.text = title
//            }
//
//        }
//
//        if let repoReason = dict["repoReason"] as? String{
//            cell.lblReporeson.text = repoReason
//        }
//
//
//        if let repoStatus = dict["repoStatus"] as? NSDictionary{
//            if let title = repoStatus["title"] as? String{
//                cell.lblrepoSatus.text = title
//                if(title == "Out For Repo"){
//                    let repoNotes = dict["repoNotes"] as! String
//                    cell.lblrepoNotes.text = "Placed out for repo - \(repoNotes)"
//                }
//            }
//        }
//        else{
//            cell.lblrepoSatus.text = "Cancelled"
//        }
        
        return cell
    }
    
    
    
}

class tblInsuranceCell : UITableViewCell
{
    @IBOutlet weak var lblPolicyno : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblAgent : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblInsCm : UILabel!
    @IBOutlet weak var lblInsCmptest : UILabel!
    @IBOutlet weak var lblEffectDate : UILabel!
       @IBOutlet weak var lblExpireDate : UILabel!
       @IBOutlet weak var lblcommDeduct : UILabel!
       @IBOutlet weak var lblColldeduct : UILabel!
}
