//
//  snapshotViewController.swift
//  LMS
//
//  Created by Apple on 01/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class snapshotViewController: UIViewController {
    
    @IBOutlet weak var tblSnapshot : UITableView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    var arrList : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblSnapshot.tableFooterView = UIView()
        self.setAccountStaus()
    }
    
    @IBAction func clickonBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setAccountStaus()
    {
        do {
        let decoded  = userDef.object(forKey: "dataDict") as! Data
        if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                OperationQueue.main.addOperation {
                    
                    let acct_no = responseDict["account_no"] as! String
                    self.lblAcctNo.text = "Account no: \(acct_no)"

                   let account_status = responseDict["account_status"] as! String
                    if(account_status == "Charge Off"){
                        self.arrList = ["Charge Off Balance","Remaining Payments","Payment Performance","Transaction History","Notes"]
                    }
                    else{
                        self.arrList = ["Remaining Payments","Payment Performance","Transaction History","Notes"]
                    }
                    self.tblSnapshot.reloadData()
            }
            }
        }
        catch{
        }
    }
}

extension snapshotViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblSnapshot.dequeueReusableCell(withIdentifier: "tblSnapshotCell") as! tblSnapshotCell
        cell.lblTitle.text = self.arrList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(self.arrList.count == 5){
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("chargeOffVC", strStoryName: "ActionStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("remainingPayVC", strStoryName: "ActionStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("paymentPerformVC", strStoryName: "ActionStory")
            case 3:
                self.presentViewControllerBasedOnIdentifier("transactionSnapVC", strStoryName: "ActionStory")
            case 4:
                    self.presentViewControllerBasedOnIdentifier("notesSnapSV", strStoryName: "ActionStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
        else{
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("remainingPayVC", strStoryName: "ActionStory")
            case 1:
                self.presentViewControllerBasedOnIdentifier("paymentPerformVC", strStoryName: "ActionStory")
            case 2:
                self.presentViewControllerBasedOnIdentifier("transactionSnapVC", strStoryName: "ActionStory")
            case 3:
                self.presentViewControllerBasedOnIdentifier("notesSnapSV", strStoryName: "ActionStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
    }
}

class tblSnapshotCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
