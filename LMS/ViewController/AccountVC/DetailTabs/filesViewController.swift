//
//  filesViewController.swift
//  Created by Apple on 11/05/20.
//  LMS
//

import UIKit
import WebKit
import SDWebImage

class filesViewController: UIViewController {
    
    @IBOutlet weak var tblFiles : UITableView!
    var arrFilesList : NSMutableArray = []
    
    var arrRemoveFile = NSMutableArray()
    
    @IBOutlet weak var viewData : UIView!
    @IBOutlet weak var viewNoData : UIView!
    
    //view Documents
    
    @IBOutlet weak var viewMAin_Doc : UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblAcctNo : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpData()
        
    }
    
    func setUpData(){
        self.viewData.isHidden = true
        self.viewNoData.isHidden = true
        
        self.viewMAin_Doc.isHidden = true
        self.imgView.isHidden = true
        self.webView.isHidden = true
        self.webView.navigationDelegate = self
        call_getFilesListApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//Api --
extension filesViewController {
    
    func call_getFilesListApi(){
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let accId_selected = dataD["_id"] as! Int
                
                let account_no = dataD["account_no"] as! String
                self.lblAcctNo.text = "Account No: \(account_no)"
                
                let Str_url = "\(Constant.file_url)?account_id=\(accId_selected)&action_type=account"
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                if let responseArr = response["data"] as? [NSDictionary] {
                                    if(responseArr.count > 0){
                                        for i in 0..<responseArr.count{
                                            let dictA = responseArr[i] as! NSMutableDictionary
                                            dictA.setValue(false, forKey: "remove")
                                            self.arrFilesList.add(dictA)
                                        }
                                        self.viewData.isHidden = false
                                        self.viewNoData.isHidden = true
                                        self.tblFiles.reloadData()
                                    }
                                    else{
                                        self.arrFilesList = []
                                        self.viewData.isHidden = true
                                        self.viewNoData.isHidden = false
                                    }
                                }
                            }
                                
                                
                            else
                            {
                                OperationQueue.main.addOperation {
                                    Globalfunc.hideLoaderView(view: self.view)
                                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                                }
                            }
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}


extension filesViewController : UITableViewDataSource , UITableViewDelegate, WKNavigationDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblFiles.dequeueReusableCell(withIdentifier: "filesTblCell") as! filesTblCell
        
        let dict = arrFilesList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let remove = dict["remove"] as? Bool
        {
            if(remove == true){
                cell.btnRemove.isSelected = true
            }
            else{
                cell.btnRemove.isSelected = false
            }
        }
        
        if let file_desc = dict["file_desc"] as? String
        {
            cell.lblDescription.text = file_desc
        }
        
        if let createdAt = dict["createdAt"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: createdAt)
            cell.lblDate.text = formatter.string(from: date!)
        }
        
        let file_type = dict["file_type"] as! String
        let extnsn = file_type.components(separatedBy: "/")
        
        if((extnsn.last!) == "png"){
            cell.imgFileTyp.image = UIImage(named: "png")
        }
        else if((extnsn.last!) == "pdf"){
            cell.imgFileTyp.image = UIImage(named: "pdf")
        }
        else {
            cell.imgFileTyp.image = UIImage(named: "notepad")
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(clikcONViewBtn(_:)), for: .touchUpInside)
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clikcONRemoveBtn(_:)), for: .touchUpInside)
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clikcONEditBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clikcONEditBtn(_ sender : UIButton)
    {
        let dict = arrFilesList[sender.tag] as! NSMutableDictionary
        Globalfunc.print(object:dict)
        
        let file_desc = dict["file_desc"] as! String
        
        let alertController = UIAlertController(title: "Edit Description", message: "", preferredStyle: UIAlertController.Style.alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.text = file_desc
        }
        let saveAction = UIAlertAction(title: "Edit", style: UIAlertAction.Style.default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            Globalfunc.showLoaderView(view: self.view)
            self.setParamtoCallApi(strDescription: firstTextField.text!, index: "\(sender.tag)")
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: {
            (action : UIAlertAction!) -> Void in
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func setParamtoCallApi(strDescription: String,index: String)
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary{
                
                let userid = dataD["userid"] as! Int
                
                for i in 0..<self.arrFilesList.count{
                    
                    let dict = arrFilesList[i] as! NSMutableDictionary
                    
                    dict.removeObject(forKey: "is_deleted")
                    dict.removeObject(forKey: "file_name")
                    
                    dict.removeObject(forKey: "userid")
                    dict.removeObject(forKey: "action_type")
                    
                    if(strDescription == ""){
                        let file_desc = dict["file_desc"] as! String
                        dict.setValue(file_desc, forKey: "desc")
                        dict.removeObject(forKey: "file_desc")
                    }
                    else{
                        if(index == "\(i)"){
                            dict.setValue(strDescription, forKey: "desc")
                            dict.removeObject(forKey: "file_desc")
                        }
                        else{
                            let file_desc = dict["file_desc"] as! String
                            dict.setValue(file_desc, forKey: "desc")
                            dict.removeObject(forKey: "file_desc")
                        }
                    }
                    dict.removeObject(forKey: "file_type")
                    dict.removeObject(forKey: "createdAt")
                    dict.removeObject(forKey: "updated_at")
                    
                    let _id = dict["_id"] as! String
                    dict.setValue(_id, forKey: "id")
                    dict.removeObject(forKey: "_id")
                    
                    self.arrFilesList.replaceObject(at: i, with: dict)
                }
                print(self.arrFilesList)
                let params = [
                    "supportFile": self.arrFilesList,
                    "userid": userid
                    ] as [String : Any]
                Globalfunc.print(object:params)
                self.callUpdateApi(param: params)
            }
        }
        catch {
        }
    }
    
    @objc func clikcONRemoveBtn(_ sender : UIButton)
    {
        let dict = arrFilesList[sender.tag] as! NSMutableDictionary
        Globalfunc.print(object:dict)
        
        if (self.arrRemoveFile.contains(dict)){
            self.arrRemoveFile.remove(dict)
            dict.setValue(false, forKey: "remove")
            self.arrFilesList.replaceObject(at: sender.tag, with: dict)
            self.tblFiles.reloadData()
        }
        else{
            self.arrRemoveFile.add(dict)
            dict.setValue(true, forKey: "remove")
            self.arrFilesList.replaceObject(at: sender.tag, with: dict)
            self.tblFiles.reloadData()
        }
    }
    
    @objc func clikcONViewBtn(_ sender : UIButton)
    {
        let dict = arrFilesList[sender.tag]  as! NSDictionary
        if let file_name = dict["file_name"] as? String
        {
            let fileUrl = "\(Constant.file_baseUrl)\(file_name)"
            Globalfunc.print(object:fileUrl)
            
            let file_type = dict["file_type"] as! String
            let extnsn = file_type.components(separatedBy: "/")
            
            if((extnsn.last!) == "png" || (extnsn.last!) == "jpeg" || (extnsn.last!) == "jpg"){
                //show imgView
                self.viewMAin_Doc.isHidden = false
                self.imgView.isHidden = false
                self.webView.isHidden = true
                showImgView(strUrl: fileUrl)
            }
            else{
                //show webview
                self.viewMAin_Doc.isHidden = false
                self.imgView.isHidden = true
                self.webView.isHidden = false
                loadWebView(strUrl: fileUrl)
            }
        }
    }
    
    func loadWebView(strUrl : String)
    {
        let url = URL(string: strUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func showImgView(strUrl : String)
    {
        //self.imgView.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        //self.imgView.setShowActivityIndicator(true)
        self.imgView.sd_setImage(with: URL.init(string: strUrl), placeholderImage: UIImage(named: ""))
    }
    
    @IBAction func clickONCros(_ sender : UIButton)
    {
        self.viewMAin_Doc.isHidden = true
        self.imgView.isHidden = true
        self.webView.isHidden = true
    }
}

extension filesViewController {
    
    @IBAction func clickONSaveBtn(_ sender: UIButton)
    {
        self.setParamtoCallApi(strDescription: "", index: "")
    }
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePutWithToken(url: Constant.file_url, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
class filesTblCell : UITableViewCell
{
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblDate : UILabel!
    @IBOutlet weak var imgFileTyp : UIImageView!
    @IBOutlet weak var btnView : UIButton!
    @IBOutlet weak var btnRemove : UIButton!
    @IBOutlet weak var btnEdit : UIButton!
}
