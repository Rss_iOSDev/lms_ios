//
//  borr_refrence.swift
//  LMS
//
//  Created by Apple on 04/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borr_refrence: UIViewController {
    
    @IBOutlet weak var tblRefrence : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var arrRefrenceList : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        arrRefrenceList = []
        self.tblRefrence.isHidden = true
        self.lblStatus.text = ""
        self.tblRefrence.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        self.setUpData()
        
        
        
    }
    
    func setUpData(){
        do{
        let decoded  = userDef.object(forKey: "dataDict") as! Data
        if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
            let acct_no = dataD["account_no"] as! String
            self.lblAccountNo.text = "Account no: \(acct_no)"
            let acct_id = dataD["_id"] as! Int
            let strUrl = "\(Constant.borrower_refrence)?account_id=\(acct_id)"
            self.getRefApi(strUrl: strUrl)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension borr_refrence {
    
    func getRefApi(strUrl : String){
        
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                            if(error == ""){
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.print(object: dict)
                                if let arr = dict as? NSDictionary{
                                    if let arr = arr["data"] as? [NSDictionary]{
                                        if(arr.count > 0){
                                            for i in 0..<arr.count{
                                                let dictA = arr[i]
                                                self.arrRefrenceList.append(dictA)
                                            }
                                            self.tblRefrence.isHidden = false
                                            self.lblStatus.text = ""
                                            self.tblRefrence.reloadData()
                                        }
                                        else{
                                            self.tblRefrence.isHidden = true
                                            self.lblStatus.text = "No Records found"
                                        }
                                    }
                                }
                                

                            }
                        }
                        else
                        {
                            OperationQueue.main.addOperation {
                                Globalfunc.hideLoaderView(view: self.view)
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                            }
                        }

                }
            }
}

extension borr_refrence : UITableViewDelegate , UITableViewDataSource
{
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrRefrenceList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblRefrence.dequeueReusableCell(withIdentifier: "tblborr_RefCell") as! tblborr_RefCell
        let dict = self.arrRefrenceList[indexPath.row]
        Globalfunc.print(object:dict)
        
        let first_name = dict["first_name"] as? String
        let last_name = dict["last_name"] as? String
        cell.lblName.setTitle("\(first_name!) \(last_name!)", for: .normal)
        if let email = dict["email"] as? String{
            cell.lblEmail.text = "\(email)"
        }
        if let relationship = dict["relationship"] as? String{
            cell.lblRelation.text = "\(relationship)"
        }
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(self.clickOnRemoveBtn(_:)), for: .touchUpInside)
        
        cell.lblName.tag = indexPath.row
        cell.lblName.addTarget(self, action: #selector(self.clickOnOpenDetailBtn(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickOnRemoveBtn(_ sender: UIButton)
    {
        let dict = self.arrRefrenceList[sender.tag]
        Globalfunc.print(object:dict)
        let id_del = (dict["id"] as! Int)
        let alertController = UIAlertController(title: Constant.AppName, message: "Are you sure. You want to delete?", preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "Delete", style: .default) { (action:UIAlertAction!) in
            let strUrl = "\(Constant.add_reference)?id=\(id_del)"
            self.deletFlag(strUrl)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(OKAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func deletFlag(_ strUrl: String)
    {
        BaseApi.onResponseDeleteWithToken(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    @objc func clickOnOpenDetailBtn(_ sender: UIButton)
    {
        let dict = self.arrRefrenceList[sender.tag]
        Globalfunc.print(object:dict)
        let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
        let dest = story.instantiateViewController(withIdentifier: "borr_refrence_detailVC") as! borr_refrence_detailVC
        dest.modalPresentationStyle = .fullScreen
        dest.passDict = dict
        self.present(dest, animated: true, completion: nil)
    }
}

class tblborr_RefCell : UITableViewCell
{
    @IBOutlet weak var lblName : UIButton!
    @IBOutlet weak var lblRelation : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblHomePhn : UILabel!
    @IBOutlet weak var lblWorkPhn : UILabel!
    @IBOutlet weak var lblCellPhn : UILabel!
    @IBOutlet weak var btnRemove : UIButton!
}
