//
//  borr_DetailsVC.swift
//  LMS
//
//  Created by Apple on 23/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borr_DetailsVC: UIViewController {
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    //Identity info
    
    @IBOutlet weak var viewEdit : UIView!
    
    @IBOutlet weak var viewIdentityInfo : UIView!
    @IBOutlet weak var lblSalutation : UILabel!
    @IBOutlet weak var lblFrstName : UILabel!
    @IBOutlet weak var lblMiddleName : UILabel!
    @IBOutlet weak var lblLastName : UILabel!
    @IBOutlet weak var lblsuffix : UILabel!
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lbldob : UILabel!
    @IBOutlet weak var lblSsn : UILabel!
    @IBOutlet weak var lblformFild : UILabel!
    @IBOutlet weak var lblServcMmber : UILabel!
    @IBOutlet weak var txtSalutation : UITextField!
    @IBOutlet weak var txtFrstName : UITextField!
    @IBOutlet weak var txtMiddleName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtsuffix : UITextField!
    @IBOutlet weak var txtGender : UITextField!
    @IBOutlet weak var txtdob : UITextField!
    @IBOutlet weak var txtSsn : UITextField!
    @IBOutlet weak var txtformFild : UITextField!
    @IBOutlet weak var txtServcMmber : UITextField!
    
    
    @IBOutlet weak var viewDriverLicenceInfo : UIView!
    @IBOutlet weak var lblDlno : UILabel!
    @IBOutlet weak var lblDlState : UILabel!
    @IBOutlet weak var lblDlIssueDate : UILabel!
    @IBOutlet weak var lblDlExpDate : UILabel!
    @IBOutlet weak var txtDLNo : UITextField!
    @IBOutlet weak var txtDLIssueDate : UITextField!
    @IBOutlet weak var txtDLState : UITextField!
    @IBOutlet weak var txtDLExpDate : UITextField!
    
    @IBOutlet weak var viewBusinessInfo : UIView!
    @IBOutlet weak var imgBusiness : UIImageView!
    @IBOutlet weak var lblDoingBusiness : UILabel!
    @IBOutlet weak var lblFed : UILabel!
    @IBOutlet weak var btnBusiness : UIButton!
    @IBOutlet weak var txtDoingBusiness : UITextField!
    @IBOutlet weak var txtFed : UITextField!
    var isDoneBusiness = false
    
    @IBOutlet weak var viewAlt_identityInfo : UIView!
    @IBOutlet weak var lblAltGovIdTyp : UILabel!
    @IBOutlet weak var lblAltGovId : UILabel!
    @IBOutlet weak var lblAltGovIdCountry : UILabel!
    @IBOutlet weak var lblAltGovIdState : UILabel!
    @IBOutlet weak var txtAltGovIdTyp : UITextField!
    @IBOutlet weak var txtAltGovId : UITextField!
    @IBOutlet weak var txtAltGovIdCountry : UITextField!
    @IBOutlet weak var txtAltGovIdState : UITextField!
    var pickerGovtIDtyp : UIPickerView!
    var pickerGovtIDCountry: UIPickerView!
    var pickerGovtIDState : UIPickerView!
    var arrGovtIDtyp = ["Passport","Photo Id"]
    var arrGovtIDCountry = NSMutableArray()
    var arrGovtIDState = NSMutableArray()
    var state_id = ""
    var country_id = ""
    
    @IBOutlet weak var viewContactInfo : UIView!
    @IBOutlet weak var lblemail : UILabel!
    @IBOutlet weak var btnAcct : UIButton!
    @IBOutlet weak var btnMArketing : UIButton!
    @IBOutlet weak var lblhomePhn : UILabel!
    @IBOutlet weak var lblhomePhn_2 : UILabel!
    @IBOutlet weak var lblCelPhn : UILabel!
    @IBOutlet weak var lblWorkPhn : UILabel!
    @IBOutlet weak var lblWorkPhn_2 : UILabel!
    @IBOutlet weak var lblWorkExtsn : UILabel!
    @IBOutlet weak var lblWorkExtsn_2 : UILabel!
    @IBOutlet weak var lblPager : UILabel!
    @IBOutlet weak var lblFax : UILabel!
    @IBOutlet weak var lblFax_2 : UILabel!
    @IBOutlet weak var lblOther : UILabel!
    
    var is_btnAcct = false
    var is_btnMArket = false
    
    @IBOutlet weak var txtemail : UITextField!
    @IBOutlet weak var txthomePhn : UITextField!
    @IBOutlet weak var txthomePhn_2 : UITextField!
    @IBOutlet weak var txtWorkPhn : UITextField!
    @IBOutlet weak var txtCelPhn : UITextField!
    @IBOutlet weak var txtWorkPhn_2 : UITextField!
    @IBOutlet weak var txtWorkExtsn : UITextField!
    @IBOutlet weak var txtWorkExtsn_2 : UITextField!
    @IBOutlet weak var txtPager : UITextField!
    @IBOutlet weak var txtFax : UITextField!
    @IBOutlet weak var txtFax_2 : UITextField!
    @IBOutlet weak var txtOther : UITextField!
    
    
    @IBOutlet weak var viewAddress : UIView!
    @IBOutlet weak var lblCurr_Address1 : UILabel!
    @IBOutlet weak var lblCurr_Address2 : UILabel!
    @IBOutlet weak var lblCurr_City : UILabel!
    @IBOutlet weak var lblCurr_State : UILabel!
    @IBOutlet weak var lblCurr_Zipcode : UILabel!
    @IBOutlet weak var lblCurr_Conty : UILabel!
    @IBOutlet weak var lblCurr_Country : UILabel!
    @IBOutlet weak var lblCurr_years : UILabel!
    @IBOutlet weak var lblCurr_months : UILabel!
    
    @IBOutlet weak var btnEditAddress : UIButton!
    
    @IBOutlet weak var txtCurr_Address1 : UITextField!
    @IBOutlet weak var txtCurr_Address2 : UITextField!
    @IBOutlet weak var txtCurr_City : UITextField!
    @IBOutlet weak var txtCurr_State : UITextField!
    @IBOutlet weak var txtCurr_Zipcode : UITextField!
    @IBOutlet weak var txtCurr_Conty : UITextField!
    @IBOutlet weak var txtCurr_Country : UITextField!
    @IBOutlet weak var txtCurr_years : UITextField!
    @IBOutlet weak var txtCurr_months : UITextField!
    var pickerAddrs_county: UIPickerView!
    var pickerAddrs_state : UIPickerView!
    var address_id = ""
    var address_state_id = ""
    var address_country_id = ""
    
    @IBOutlet weak var lblPrev_Address1 : UILabel!
    @IBOutlet weak var lblPrev_Address2 : UILabel!
    @IBOutlet weak var lblPrev_City : UILabel!
    @IBOutlet weak var lblPrev_State : UILabel!
    @IBOutlet weak var lblPrev_Zipcode : UILabel!
    @IBOutlet weak var lblPrev_Conty : UILabel!
    @IBOutlet weak var lblPrev_Country : UILabel!
    @IBOutlet weak var lblPrev_years : UILabel!
    @IBOutlet weak var lblPrev_months : UILabel!
    
    @IBOutlet weak var btnEmpEditAddress : UIButton!
    
    @IBOutlet weak var viewEmploymt : UIView!
    @IBOutlet weak var lblCurr_Empoyer : UILabel!
    @IBOutlet weak var lblCurr_wrkPhn : UILabel!
    @IBOutlet weak var lblCurr_Position : UILabel!
    @IBOutlet weak var lblCurr_EmpTyp : UILabel!
    @IBOutlet weak var lblCurr_Income : UILabel!
    @IBOutlet weak var lblCurr_PayFreq : UILabel!
    @IBOutlet weak var lblCurr_yearsEmp : UILabel!
    @IBOutlet weak var lblCurr_monthEmp : UILabel!
    @IBOutlet weak var lblCurr_Addrs_1Emp : UILabel!
    @IBOutlet weak var lblCurr_Addrs_2Emp : UILabel!
    @IBOutlet weak var lblCurr_ZipEmp : UILabel!
    @IBOutlet weak var lblCurr_CityEmp : UILabel!
    @IBOutlet weak var lblCurr_CountyEmp : UILabel!
    @IBOutlet weak var lblCurr_StateEmp : UILabel!
    @IBOutlet weak var lblCurr_EmailEmp : UILabel!
    
    @IBOutlet weak var txtCurr_Empoyer : UITextField!
    @IBOutlet weak var txtCurr_wrkPhn : UITextField!
    @IBOutlet weak var txtCurr_Position : UITextField!
    @IBOutlet weak var txtCurr_EmpTyp : UITextField!
    @IBOutlet weak var txtCurr_Income : UITextField!
    @IBOutlet weak var txtCurr_PayFreq : UITextField!
    @IBOutlet weak var txtCurr_yearsEmp : UITextField!
    @IBOutlet weak var txtCurr_monthEmp : UITextField!
    @IBOutlet weak var txtCurr_Addrs_1Emp : UITextField!
    @IBOutlet weak var txtCurr_Addrs_2Emp : UITextField!
    @IBOutlet weak var txtCurr_ZipEmp : UITextField!
    @IBOutlet weak var txtCurr_CityEmp : UITextField!
    @IBOutlet weak var txtCurr_CountyEmp : UITextField!
    @IBOutlet weak var txtCurr_StateEmp : UITextField!
    var employmnt_id = ""
    var pickerEmpTyp: UIPickerView!
    var arrEmptyp = ["Full Time","Homemaker","Part Time","Retired","Self Employed"]
    var pickerPayFreq: UIPickerView!
    var arrPayFreq = ["Weekly","Bi-Weekly","Semi-Monthly","Monthly"]
    var pickerEmp_state : UIPickerView!
    var emp_state_id = ""
    
    
    
    @IBOutlet weak var lblPrev_Empoyer : UILabel!
    @IBOutlet weak var lblPrev_wrkPhn : UILabel!
    @IBOutlet weak var lblPrev_Position : UILabel!
    @IBOutlet weak var lblPrev_EmpTyp : UILabel!
    @IBOutlet weak var lblPrev_Income : UILabel!
    @IBOutlet weak var lblPrev_PayFreq : UILabel!
    @IBOutlet weak var lblPrev_yearsEmp : UILabel!
    @IBOutlet weak var lblPrev_monthEmp : UILabel!
    @IBOutlet weak var lblPrev_Addrs_1Emp : UILabel!
    @IBOutlet weak var lblPrev_Addrs_2Emp : UILabel!
    @IBOutlet weak var lblPrev_ZipEmp : UILabel!
    @IBOutlet weak var lblPrev_CityEmp : UILabel!
    @IBOutlet weak var lblPrev_CountyEmp : UILabel!
    @IBOutlet weak var lblPrev_StateEmp : UILabel!
    @IBOutlet weak var lblPrev_EmailEmp : UILabel!
    
    
    
    var pickerDob : UIDatePicker!
    var pickerFormfiled : UIDatePicker!
    
    var pickerIssueDate : UIDatePicker!
    var pickerExpDate : UIDatePicker!
    
    var arrSalutation = ["Mr.","Mrs.","Ms.","Miss"]
    var arrSuffix = ["Sr.","Jr.","I","II","III","IV","V"]
    var arrGender = ["Male","Female"]
    var arrServcMember = ["Yes","No"]
    
    
    var pickerSalutation : UIPickerView!
    var pickersuffix: UIPickerView!
    
    var pickerGender : UIPickerView!
    var pickerServcMember: UIPickerView!
    
    var sericerMmber = ""
    var dateSelect = ""
    
    var arrUrl = [Constant.state,Constant.country]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewEdit.isHidden = true
        self.viewIdentityInfo.isHidden = true
        self.viewDriverLicenceInfo.isHidden = true
        self.viewBusinessInfo.isHidden = true
        self.viewAlt_identityInfo.isHidden = true
        self.viewContactInfo.isHidden = true
        self.viewAddress.isHidden = true
        self.viewEmploymt.isHidden = true
        
        Globalfunc.showLoaderView(view: self.view)
        self.callGetPickerApi()
        self.getPstExtensionapiApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONEditBtn(_ sender: UIButton)
    {
        self.viewEdit.isHidden = false
        if(sender.tag == 10){
            self.viewIdentityInfo.isHidden = false
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 20){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = false
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 30){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = false
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 40){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = false
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 50){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = false
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 60){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = false
            self.viewEmploymt.isHidden = true
        }
        else if(sender.tag == 70){
            self.viewIdentityInfo.isHidden = true
            self.viewDriverLicenceInfo.isHidden = true
            self.viewBusinessInfo.isHidden = true
            self.viewAlt_identityInfo.isHidden = true
            self.viewContactInfo.isHidden = true
            self.viewAddress.isHidden = true
            self.viewEmploymt.isHidden = false
        }
    }
    
    @IBAction func clickONCloseBtn(_ sender: UIButton)
    {
        self.viewWillAppear(true)
    }
    
    @IBAction func clcikOnBusinessBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            isDoneBusiness = false
        }else {
            sender.isSelected = true
            isDoneBusiness = true
        }
    }
    
    @IBAction func clcikOnActBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            is_btnAcct = false
        }else {
            sender.isSelected = true
            is_btnAcct = true
        }
    }
    
    @IBAction func clcikOnMArketBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            is_btnMArket = false
        }else {
            sender.isSelected = true
            is_btnMArket = true
        }
    }
}

//call action api for all pages once
extension borr_DetailsVC{
    
    func callGetPickerApi()
    {
        let group = DispatchGroup() // initialize
        arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if(obj == Constant.state){
                            
                            if let data = dict as? NSDictionary{
                                if let arr = data["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrGovtIDState.add(dictA)
                                        }
                                    }
                                }
                            }

                        }
                        
                        if(obj == Constant.country){
                            if let data = dict as? NSDictionary{
                                    if let arr = data["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrGovtIDCountry.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
    
    func getPstExtensionapiApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                if let account_no = dataD["account_no"] as? String{
                    self.lblActtNo.text = "Account no: \(account_no)"
                }
                let accId_selected = dataD["_id"] as! Int
                let Str_url = "\(Constant.borro_detail)?id=\(accId_selected)"
                
                BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                if let data = response["data"] as? NSDictionary {
                                    self.parseData(response: data)
                                }
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch
        {
        }
    }
    
    func parseData( response: NSDictionary)
    {
            if let identity_info = response["identity_info"] as? NSDictionary{
                self.setIDentityInfoData(identity_info: identity_info)
            }
            
            if let drivers_license = response["drivers_license"] as? NSDictionary{
                self.setDrivingLicencrInfo(drivers_license)
            }
            
            if let doing_business_as = response["doing_business_as"] as? NSDictionary{
                self.setBusinessData(doing_business_as: doing_business_as)
            }
            
            if let alternate_identity_info = response["alternate_identity_info"] as? NSDictionary{
                self.setAlt_IDentityInfo(alternate_identity_info)
            }
            
            if let contact_info = response["contact_info"] as? NSDictionary{
                self.setContactInfoData(contact_info: contact_info)
            }
            
            if let current_address = response["current_address"] as? NSDictionary{
                
                self.setCurr_AddressData(c_address: current_address)
            }
            
            if let previous_address = response["previous_address"] as? NSDictionary{
                self.setPrev_AddressData(p_address: previous_address)
            }
            
            
            if let current_employment = response["current_employment"] as? NSDictionary{
                self.setCurr_EmploymntData(c_employmnt: current_employment)
            }
            
            if let previous_employment = response["previous_employment"] as? NSDictionary{
                self.setPrev_EmploymntData(p_employmnt: previous_employment)
            }
    }
    
    func setIDentityInfoData( identity_info : NSDictionary)
    {
        if let first_name = identity_info["first_name"] as? String
        {
            self.lblFrstName.text = first_name
            self.txtFrstName.text = first_name
        }
        
        if let gender = identity_info["gender"] as? String
        {
            self.lblGender.text = gender
            self.txtGender.text = gender
        }
        
        if let last_name = identity_info["last_name"] as? String
        {
            self.lblLastName.text = last_name
            self.txtLastName.text = last_name
        }
        
        if let middle_name = identity_info["middle_name"] as? String
        {
            self.lblMiddleName.text = middle_name
            self.txtMiddleName.text = middle_name
        }
        
        if let last_8300_form_file = identity_info["last_8300_form_file"] as? String
        {
            if(last_8300_form_file != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: last_8300_form_file)
                self.lblformFild.text = formatter.string(from: date!)
                self.txtformFild.text = formatter.string(from: date!)
            }
            else{
                self.lblformFild.text = ""
                self.txtformFild.text = ""
            }
        }
        
        if let salutation = identity_info["salutation"] as? String
        {
            self.lblSalutation.text = salutation
            self.txtSalutation.text = salutation
        }
        
        if let suffix = identity_info["suffix"] as? String
        {
            self.lblsuffix.text = suffix
            self.txtsuffix.text = suffix
        }
        if let active_servicemember = identity_info["active_servicemember"] as? String
        {
            if(active_servicemember == "true")
            {
                self.lblServcMmber.text = "Yes"
                self.txtServcMmber.text = arrServcMember[0]
            }
            else if(active_servicemember == "false")
            {
                self.lblServcMmber.text = "No"
                self.txtServcMmber.text = arrServcMember[1]
            }
        }
        if let customer_ssn = identity_info["customer_ssn"] as? String
        {
            self.lblSsn.text = customer_ssn
            self.txtSsn.text = customer_ssn
            
        }
        if let date_of_birth = identity_info["date_of_birth"] as? String
        {
            if(date_of_birth != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: date_of_birth)
                self.lbldob.text = formatter.string(from: date!)
                self.lbldob.text = formatter.string(from: date!)
            }
            else{
                self.lbldob.text = ""
                self.lbldob.text = ""
            }
        }
    }
    
    func setDrivingLicencrInfo(_ drivers_license: NSDictionary)
    {
        if let dl_no = drivers_license["dl_no"] as? String
        {
            self.lblDlno.text = "\(dl_no)"
            self.txtDLNo.text = "\(dl_no)"
        }
        
        if let dl_state = drivers_license["dl_state"] as? String
        {
            self.lblDlState.text = "\(dl_state)"
            self.txtDLState.text = "\(dl_state)"
        }
        
        
        if let dl_exp_date = drivers_license["dl_exp_date"] as? String
        {
            if(dl_exp_date != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: dl_exp_date)
                self.lblDlExpDate.text = formatter.string(from: date!)
                self.txtDLExpDate.text = formatter.string(from: date!)
            }
            else{
                self.lblDlExpDate.text = ""
                self.txtDLExpDate.text = ""
            }
        }
        
        if let dl_issue_date = drivers_license["dl_issue_date"] as? String
        {
            if(dl_issue_date != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: dl_issue_date)
                self.lblDlIssueDate.text = formatter.string(from: date!)
                self.txtDLIssueDate.text = formatter.string(from: date!)
            }
            else{
                self.lblDlIssueDate.text = ""
                self.txtDLIssueDate.text = ""
            }
        }
    }
    
    func setBusinessData( doing_business_as : NSDictionary)
    {
        if let fed_id = doing_business_as["fed_id"] as? String
        {
            self.lblFed.text = "\(fed_id)"
            self.txtFed.text = "\(fed_id)"
        }
        
        if let doing_business_as = doing_business_as["doing_business_as"] as? String
        {
            self.lblDoingBusiness.text = "\(doing_business_as)"
            self.txtDoingBusiness.text = "\(doing_business_as)"
        }
        
        if let is_a_business = doing_business_as["is_a_business"] as? Bool
        {
            if(is_a_business == true){
                self.isDoneBusiness = true
                self.imgBusiness.image = UIImage(named: "check")
                self.btnBusiness.isSelected = true
            }
            else{
                self.isDoneBusiness = false
                self.imgBusiness.image = UIImage(named: "uncheck")
                self.btnBusiness.isSelected = false
            }
        }
    }
    
    func setContactInfoData( contact_info : NSDictionary)
    {
        if let cellPhone = contact_info["cellPhone"] as? NSNumber
        {
            self.lblCelPhn.text = "\(cellPhone)"
            self.txtCelPhn.text = "\(cellPhone)"
        }
        
        if let email = contact_info["email"] as? String
        {
            self.lblemail.text = "\(email)"
            self.txtemail.text = "\(email)"
        }
        
        if let fax1 = contact_info["fax1"] as? String
        {
            self.lblFax.text = "\(fax1)"
            self.txtFax_2.text = "\(fax1)"
        }
        
        if let fax2 = contact_info["fax2"] as? String
        {
            self.lblFax_2.text = "\(fax2)"
            self.txtFax_2.text = "\(fax2)"
        }
        
        if let homePhone = contact_info["homePhone"] as? String
        {
            self.lblhomePhn.text = "\(homePhone)"
            self.txthomePhn.text = "\(homePhone)"
        }
        
        if let homePhone2 = contact_info["homePhone2"] as? String
        {
            self.lblhomePhn_2.text = "\(homePhone2)"
            self.txthomePhn_2.text = "\(homePhone2)"
        }
        
        
        if let otherPhone = contact_info["otherPhone"] as? String
        {
            self.lblOther.text = "\(otherPhone)"
            self.txtOther.text = "\(otherPhone)"
        }
        
        if let pager = contact_info["pager"] as? String
        {
            self.lblPager.text = "\(pager)"
            self.txtPager.text = "\(pager)"
        }
        
        if let workExtension = contact_info["workExtension"] as? String
        {
            self.lblWorkExtsn.text = "\(workExtension)"
            self.txtWorkExtsn_2.text = "\(workExtension)"
        }
        
        if let workExtension2 = contact_info["workExtension2"] as? String
        {
            self.lblWorkExtsn_2.text = "\(workExtension2)"
            self.txtWorkExtsn_2.text = "\(workExtension2)"
        }
        
        if let workPhone = contact_info["workPhone"] as? String
        {
            self.lblWorkPhn.text = "\(workPhone)"
            self.txtWorkPhn_2.text = "\(workPhone)"
        }
        
        if let workPhone2 = contact_info["workPhone2"] as? String
        {
            self.lblWorkPhn_2.text = "\(workPhone2)"
            self.txtWorkPhn_2.text = "\(workPhone2)"
        }
        
        if let account_opt_out = contact_info["account_opt_out"] as? Bool
        {
            if(account_opt_out == true){
                self.is_btnAcct = true
                self.btnAcct.isSelected = true
            }
            else{
                self.is_btnAcct = false
                self.btnAcct.isSelected = false
            }
        }
        
        if let marketing_opt_out = contact_info["marketing_opt_out"] as? Bool
        {
            if(marketing_opt_out == true){
                self.is_btnMArket = true
                self.btnMArketing.isSelected = true
            }
            else{
                self.is_btnMArket = false
                self.btnMArketing.isSelected = false
            }
        }
    }
    
    
    func setAlt_IDentityInfo(_ alt_identity_info: NSDictionary)
    {
        if let alt_govt_id = alt_identity_info["alt_govt_id"] as? String
        {
            self.lblAltGovId.text = alt_govt_id
            self.txtAltGovId.text = alt_govt_id
        }
        
        if let alt_govt_id_country = alt_identity_info["alt_govt_id_country"] as? String
        {
            for i in 0..<arrGovtIDCountry.count
            {
                let dict = arrGovtIDCountry[i] as! NSDictionary
                let _id = dict["_id"] as! NSNumber
                if("\(_id)" == alt_govt_id_country){
                    let country_name = dict["country_name"] as! String
                    self.lblAltGovIdCountry.text = country_name
                    self.txtAltGovIdCountry.text = country_name
                    break
                }
            }
        }
        
        if let alt_govt_id_state = alt_identity_info["alt_govt_id_state"] as? String
        {
            for i in 0..<arrGovtIDState.count
            {
                let dict = arrGovtIDState[i] as! NSDictionary
                let _id = dict["_id"] as! NSNumber
                if("\(_id)" == alt_govt_id_state){
                    let state_name = dict["state_name"] as! String
                    self.lblAltGovIdState.text = state_name
                    self.txtAltGovIdState.text = state_name
                    break
                }
            }
        }
        
        if let alt_govt_id_type = alt_identity_info["alt_govt_id_type"] as? String
        {
            self.lblAltGovIdTyp.text = alt_govt_id_type
            self.txtAltGovIdTyp.text = alt_govt_id_type
        }
    }
    
    func setCurr_AddressData( c_address : NSDictionary)
    {
        
        if let _id = c_address["_id"] as? String
        {
            self.address_id = _id
            self.btnEditAddress.setTitle("Update", for: .normal)
        }
        
        if let address1 = c_address["address1"] as? String
        {
            self.lblCurr_Address1.text = "\(address1)"
            self.txtCurr_Address1.text = "\(address1)"
        }
        
        if let address2 = c_address["address2"] as? String
        {
            self.lblCurr_Address2.text = "\(address2)"
            self.txtCurr_Address2.text = "\(address2)"
        }
        
        if let city = c_address["city"] as? String
        {
            self.lblCurr_City.text = "\(city)"
            self.txtCurr_City.text = "\(city)"
        }
        
        if let country = c_address["country"] as? String
        {
            self.lblCurr_Country.text = "\(country)"
            self.txtCurr_Country.text = "\(country)"
        }
        
        if let county = c_address["county"] as? String
        {
            self.lblCurr_Conty.text = "\(county)"
            self.txtCurr_Conty.text = "\(county)"
        }
        
        if let monthsAt = c_address["monthsAt"] as? Int
        {
            self.lblCurr_months.text = "\(monthsAt)"
            self.txtCurr_months.text = "\(monthsAt)"
        }
        
        if let state = c_address["state"] as? String
        {
            self.lblCurr_State.text = "\(state)"
            self.txtCurr_State.text = "\(state)"
        }
        
        if let yearsAt = c_address["yearsAt"] as? Int
        {
            self.lblCurr_years.text = "\(yearsAt)"
            self.txtCurr_years.text = "\(yearsAt)"
        }
        
        if let zipCode = c_address["zipCode"] as? String
        {
            self.lblCurr_Zipcode.text = "\(zipCode)"
            self.txtCurr_Zipcode.text = "\(zipCode)"
        }
        
        
    }
    
    func setPrev_AddressData( p_address : NSDictionary)
    {
        if let address1 = p_address["address1"] as? String
        {
            self.lblPrev_Address1.text = "\(address1)"
        }
        
        if let address2 = p_address["address2"] as? String
        {
            self.lblPrev_Address2.text = "\(address2)"
        }
        
        if let city = p_address["city"] as? String
        {
            self.lblPrev_City.text = "\(city)"
        }
        
        if let country = p_address["country"] as? String
        {
            self.lblPrev_Country.text = "\(country)"
        }
        
        if let county = p_address["county"] as? String
        {
            self.lblPrev_Conty.text = "\(county)"
        }
        
        if let monthsAt = p_address["monthsAt"] as? String
        {
            self.lblPrev_months.text = "\(monthsAt)"
        }
        
        if let state = p_address["state"] as? String
        {
            self.lblPrev_State.text = "\(state)"
        }
        
        if let yearsAt = p_address["yearsAt"] as? String
        {
            self.lblPrev_years.text = "\(yearsAt)"
        }
        
        if let zipCode = p_address["zipCode"] as? String
        {
            self.lblPrev_Zipcode.text = "\(zipCode)"
        }
    }
    
    
    func setCurr_EmploymntData( c_employmnt : NSDictionary)
    {
        if let _id = c_employmnt["_id"] as? String
        {
            self.employmnt_id = _id
            self.btnEmpEditAddress.setTitle("Update", for: .normal)
        }
        
        if let address1 = c_employmnt["address1"] as? String
        {
            self.lblCurr_Addrs_1Emp.text = "\(address1)"
            self.txtCurr_Addrs_1Emp.text = "\(address1)"
        }
        
        if let address2 = c_employmnt["address2"] as? String
        {
            self.lblCurr_Addrs_2Emp.text = "\(address2)"
            self.txtCurr_Addrs_2Emp.text = "\(address2)"
            
        }
        
        if let city = c_employmnt["city"] as? String
        {
            self.lblCurr_CityEmp.text = "\(city)"
            self.txtCurr_CityEmp.text = "\(city)"
            
        }
        
        if let email = c_employmnt["email"] as? String
        {
            self.lblCurr_EmailEmp.text = "\(email)"
        }
        
        
        if let employer = c_employmnt["employer"] as? String
        {
            self.lblCurr_Empoyer.text = "\(employer)"
            self.txtCurr_Empoyer.text = "\(employer)"
        }
        
        if let employment_type = c_employmnt["employment_type"] as? String
        {
            self.lblCurr_EmpTyp.text = "\(employment_type)"
            self.txtCurr_EmpTyp.text = "\(employment_type)"
        }
        
        if let county = c_employmnt["county"] as? String
        {
            self.lblCurr_CountyEmp.text = "\(county)"
            self.txtCurr_CountyEmp.text = "\(county)"
        }
        
        if let income = c_employmnt["income"] as? String
        {
            self.lblCurr_Income.text = "\(income)"
            self.txtCurr_Income.text = "\(income)"
        }
        
        if let monthsAt = c_employmnt["monthsAt"] as? String
        {
            self.lblCurr_monthEmp.text = "\(monthsAt)"
            self.txtCurr_monthEmp.text = "\(monthsAt)"
        }
        
        if let payFrequency = c_employmnt["payFrequency"] as? String
        {
            self.lblCurr_PayFreq.text = "\(payFrequency)"
            self.txtCurr_PayFreq.text = "\(payFrequency)"
        }
        
        if let position = c_employmnt["position"] as? String
        {
            self.lblCurr_Position.text = "\(position)"
            self.txtCurr_Position.text = "\(position)"
        }
        
        if let state = c_employmnt["state"] as? String
        {
            self.lblCurr_StateEmp.text = "\(state)"
            self.txtCurr_StateEmp.text = "\(state)"
        }
        
        if let work_phone1 = c_employmnt["work_phone1"] as? String
        {
            self.lblCurr_wrkPhn.text = "\(work_phone1)"
            self.txtCurr_wrkPhn.text = "\(work_phone1)"
        }
        
        if let yearsAt = c_employmnt["yearsAt"] as? String
        {
            self.lblCurr_yearsEmp.text = "\(yearsAt)"
            self.txtCurr_yearsEmp.text = "\(yearsAt)"
        }
        
        if let zipCode = c_employmnt["zipCode"] as? String
        {
            self.lblCurr_ZipEmp.text = "\(zipCode)"
            self.txtCurr_ZipEmp.text = "\(zipCode)"
            
        }
    }
    
    
    func setPrev_EmploymntData( p_employmnt : NSDictionary)
    {
        
        
        if let address1 = p_employmnt["address1"] as? String
        {
            self.lblPrev_Addrs_1Emp.text = "\(address1)"
        }
        
        if let address2 = p_employmnt["address2"] as? String
        {
            self.lblPrev_Addrs_2Emp.text = "\(address2)"
        }
        
        if let city = p_employmnt["city"] as? String
        {
            self.lblPrev_CityEmp.text = "\(city)"
        }
        
        if let email = p_employmnt["email"] as? String
        {
            self.lblPrev_EmailEmp.text = "\(email)"
        }
        
        
        if let employer = p_employmnt["employer"] as? String
        {
            self.lblPrev_Empoyer.text = "\(employer)"
        }
        
        if let employment_type = p_employmnt["employment_type"] as? String
        {
            self.lblPrev_EmpTyp.text = "\(employment_type)"
        }
        
        if let county = p_employmnt["county"] as? String
        {
            self.lblPrev_CountyEmp.text = "\(county)"
        }
        
        if let income = p_employmnt["income"] as? String
        {
            self.lblPrev_Income.text = "\(income)"
        }
        
        
        
        if let monthsAt = p_employmnt["monthsAt"] as? String
        {
            self.lblPrev_monthEmp.text = "\(monthsAt)"
        }
        
        if let payFrequency = p_employmnt["payFrequency"] as? String
        {
            self.lblPrev_PayFreq.text = "\(payFrequency)"
        }
        
        if let position = p_employmnt["position"] as? String
        {
            self.lblPrev_Position.text = "\(position)"
        }
        
        
        if let state = p_employmnt["state"] as? String
        {
            self.lblPrev_StateEmp.text = "\(state)"
        }
        
        if let work_phone1 = p_employmnt["work_phone1"] as? String
        {
            self.lblPrev_wrkPhn.text = "\(work_phone1)"
        }
        
        if let yearsAt = p_employmnt["yearsAt"] as? String
        {
            self.lblPrev_yearsEmp.text = "\(yearsAt)"
        }
        
        if let zipCode = p_employmnt["zipCode"] as? String
        {
            self.lblPrev_ZipEmp.text = "\(zipCode)"
        }
    }
}

extension borr_DetailsVC {
    
    @IBAction func clickONSubmitBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let params = [
                        "active_servicemember": sericerMmber,
                        "customer_ssn": "\(txtSsn.text!)",
                        "date_of_birth": "\(self.txtdob.text!)",
                        "first_name": "\(txtFrstName.text!)",
                        "gender": "\(txtGender.text!)",
                        "id": accId_selected,
                        "last_8300_form_file": "\(txtformFild.text!)",
                        "last_name": "\(txtLastName.text!)" ,
                        "middle_name": "\(txtMiddleName.text!)",
                        "salutation": "\(txtSalutation.text!)",
                        "suffix": "\(txtsuffix.text!)"] as [String : Any]
                    
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.detail_identity_info, type: "put")
                }
                
            }
            catch{
                
            }
        }
        else if(sender.tag == 100){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let params = [
                        "dl_exp_date": "\(txtDLExpDate.text!)",
                        "dl_issue_date": "\(self.txtDLIssueDate.text!)",
                        "dl_no": "\(txtDLNo.text!)",
                        "dl_state": "\(txtDLState.text!)",
                        "id": accId_selected] as [String : Any]
                    
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.detail_drivers_license, type: "put")
                }
                
            }
            catch{
                
            }
        }
        else if(sender.tag == 200){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let params = [
                        "doing_business_as": "\(txtDoingBusiness.text!)",
                        "fed_id": "\(self.txtFed.text!)",
                        "is_a_business": self.isDoneBusiness,
                        "id": accId_selected] as [String : Any]
                    
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.detail_doing_business, type: "put")
                }
            }
            catch{
                
            }
        }
        else if(sender.tag == 300){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let params = [
                        "alt_govt_id_type": "\(txtAltGovIdTyp.text!)",
                        "alt_govt_id": "\(self.txtAltGovId.text!)",
                        "alt_govt_id_country": self.country_id,
                        "alt_govt_id_state": self.state_id,
                        "id": accId_selected] as [String : Any]
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.detail_alternate_id_info, type: "put")
                }
            }
            catch{
                
            }
        }
            
        else if(sender.tag == 400){
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    
                    let accId_selected = dataD["_id"] as! Int
                    let params = [
                        "account_opt_out": is_btnAcct,
                        "cellPhone": "\(self.txtCelPhn.text!)",
                        "cp_do_not_call": false,
                        "email": "\(self.txtemail.text!)",
                        "fax1": "\(self.txtFax.text!)",
                        "fax1_do_not_call": false,
                        "fax2": "\(self.txtFax_2.text!)",
                        "fax2_do_not_call": false,
                        "homePhone": "\(self.txthomePhn.text!)",
                        "homePhone2": "\(self.txthomePhn_2.text!)",
                        "hp2_do_not_call": false,
                        "hp_do_not_call": false,
                        "marketing_opt_out": is_btnMArket,
                        "op_do_not_call": false,
                        "otherPhone": "\(self.txtOther.text!)",
                        "pager": "\(self.txtPager.text!)",
                        "pager_do_not_call": false,
                        "workExtension": "\(self.txtWorkExtsn.text!)",
                        "workExtension2": "\(self.txtWorkExtsn_2.text!)",
                        "workPhone": "\(self.txtWorkPhn.text!)",
                        "workPhone2": "\(self.txtWorkPhn_2.text!)",
                        "wp2_do_not_call": false,
                        "wp_do_not_call": false,
                        "id": accId_selected] as [String : Any]
                    
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.detail_contact_info, type: "put")
                }
            }
            catch{
            }
        }
        else if(sender.tag == 500){
            
            if let buttonTitle = self.btnEditAddress.title(for: .normal) {
                if(buttonTitle == "Add"){
                    
                    do {
                        let decoded  = userDef.object(forKey: "dataDict") as! Data
                        if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                            let accId_selected = dataD["_id"] as! Int
                            let params = [
                                "address1": "\(self.txtCurr_Address1.text!)",
                                "address2": "\(self.txtCurr_Address2.text!)",
                                "city": "\(self.txtCurr_City.text!)",
                                "country": self.address_country_id,
                                "county": "\(self.txtCurr_Conty.text!)",
                                "monthsAt": "\(self.txtCurr_months.text!)",
                                "state": self.address_state_id,
                                "yearsAt": "\(self.txtCurr_years.text!)",
                                "zipCode": "\(self.txtCurr_Zipcode.text!)",
                                "id": accId_selected] as [String : Any]
                            Globalfunc.print(object:params)
                            self.callUpdateApi(param: params, StrUrl: Constant.detail_address, type: "post")
                        }
                    }
                    catch{
                    }
                    
                }
                else if(buttonTitle == "Update"){
                    if(self.address_id == ""){
                        Globalfunc.showToastWithMsg(view: self.view, str: "Please Add current address first by clicking on Plus Icon.")
                    }
                    else{
                        do {
                            let decoded  = userDef.object(forKey: "dataDict") as! Data
                            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                                let accId_selected = dataD["_id"] as! Int
                                let params = [
                                    "address1": "\(self.txtCurr_Address1.text!)",
                                    "address2": "\(self.txtCurr_Address2.text!)",
                                    "address_id": self.address_id,
                                    "city": "\(self.txtCurr_City.text!)",
                                    "country": self.address_country_id,
                                    "county": "\(self.txtCurr_Conty.text!)",
                                    "monthsAt": "\(self.txtCurr_months.text!)",
                                    "state": self.address_state_id,
                                    "yearsAt": "\(self.txtCurr_years.text!)",
                                    "zipCode": "\(self.txtCurr_Zipcode.text!)",
                                    "id": accId_selected] as [String : Any]
                                Globalfunc.print(object:params)
                                self.callUpdateApi(param: params, StrUrl: Constant.detail_address, type: "put")
                            }
                        }
                        catch{
                        }
                    }
                }
            }
        }
            
        else if(sender.tag == 600){
            
            if let buttonTitle = self.btnEmpEditAddress.title(for: .normal) {
                if(buttonTitle == "Add"){
                    do {
                        let decoded  = userDef.object(forKey: "dataDict") as! Data
                        if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                            let accId_selected = dataD["_id"] as! Int
                            let params = [
                                "address1": "\(self.txtCurr_Addrs_1Emp.text!)",
                                "address2": "\(self.txtCurr_Addrs_2Emp.text!)",
                                "city": "\(self.txtCurr_CityEmp.text!)",
                                "county": "\(self.txtCurr_CountyEmp.text!)",
                                "monthsAt": "\(self.txtCurr_monthEmp.text!)",
                                "state": self.emp_state_id,
                                "yearsAt": "\(self.txtCurr_yearsEmp.text!)",
                                "zipCode": "\(self.txtCurr_ZipEmp.text!)",
                                "employer": "\(self.txtCurr_Empoyer.text!)",
                                "employment_type": "\(self.txtCurr_EmpTyp.text!)",
                                "income": "\(self.txtCurr_Income.text!)",
                                "payFrequency": "\(self.txtCurr_PayFreq.text!)",
                                "position": "\(self.txtCurr_Position.text!)",
                                "work_phone1": "\(self.txtCurr_wrkPhn.text!)",
                                "id": accId_selected] as [String : Any]
                            Globalfunc.print(object:params)
                            self.callUpdateApi(param: params, StrUrl: Constant.detail_employment, type: "post")
                        }
                    }
                    catch{
                    }
                    
                    
                }
                else if(buttonTitle == "Update"){
                    if(self.employmnt_id == ""){
                        Globalfunc.showToastWithMsg(view: self.view, str: "Please Add current employment first by clicking on Plus Icon.")
                    }
                    else{
                        
                        do {
                            let decoded  = userDef.object(forKey: "dataDict") as! Data
                            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                                let accId_selected = dataD["_id"] as! Int
                                let params = [
                                    "address1": "\(self.txtCurr_Addrs_1Emp.text!)",
                                    "address2": "\(self.txtCurr_Addrs_2Emp.text!)",
                                    "employment_id": self.employmnt_id,
                                    "city": "\(self.txtCurr_CityEmp.text!)",
                                    "county": "\(self.txtCurr_CountyEmp.text!)",
                                    "monthsAt": "\(self.txtCurr_monthEmp.text!)",
                                    "state": self.emp_state_id,
                                    "yearsAt": "\(self.txtCurr_yearsEmp.text!)",
                                    "zipCode": "\(self.txtCurr_ZipEmp.text!)",
                                    "employer": "\(self.txtCurr_Empoyer.text!)",
                                    "employment_type": "\(self.txtCurr_EmpTyp.text!)",
                                    "income": "\(self.txtCurr_Income.text!)",
                                    "payFrequency": "\(self.txtCurr_PayFreq.text!)",
                                    "position": "\(self.txtCurr_Position.text!)",
                                    "work_phone1": "\(self.txtCurr_wrkPhn.text!)",
                                    "id": accId_selected] as [String : Any]
                                Globalfunc.print(object:params)
                                self.callUpdateApi(param: params, StrUrl: Constant.detail_employment, type: "put")
                            }
                        }
                        catch{
                        }
                    }
                }
            }
        }
        
    }
    
    @IBAction func clickONAddPLusBtn(_ sender: UIButton)
    {
        if(sender.tag == 10)
        {
            self.btnEditAddress.setTitle("Add", for: .normal)
            self.txtCurr_Address1.text = ""
            self.txtCurr_Address2.text = ""
            self.txtCurr_City.text = ""
            self.txtCurr_Country.text = ""
            self.txtCurr_Conty.text = ""
            self.txtCurr_months.text = ""
            self.txtCurr_State.text = ""
            self.txtCurr_years.text = ""
            self.txtCurr_Zipcode.text = ""
            self.address_id = ""
            
        }
        else if(sender.tag == 20)
        {
            self.btnEmpEditAddress.setTitle("Add", for: .normal)
            
            self.txtCurr_Addrs_1Emp.text = ""
            self.txtCurr_Addrs_2Emp.text = ""
            self.txtCurr_CityEmp.text = ""
            self.txtCurr_Empoyer.text = ""
            self.txtCurr_EmpTyp.text = ""
            self.txtCurr_CountyEmp.text = ""
            self.txtCurr_Income.text = ""
            self.txtCurr_monthEmp.text = ""
            self.txtCurr_PayFreq.text = ""
            self.txtCurr_Position.text = ""
            self.txtCurr_StateEmp.text = ""
            self.txtCurr_wrkPhn.text = ""
            self.txtCurr_yearsEmp.text = ""
            self.txtCurr_ZipEmp.text = ""
            self.employmnt_id = ""
            
        }
        
    }
    
    func callUpdateApi(param: [String : Any], StrUrl : String, type: String)
    {
        if(type == "put"){
            BaseApi.onResponsePutWithToken(url: StrUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
        else if(type == "post"){
            BaseApi.onResponsePostWithToken(url: StrUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
        }
        
    }
    
}


/*MARk - Textfeld and picker delegates*/

extension borr_DetailsVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerSalutation{
            return self.arrSalutation.count
        }
        else if(pickerView == pickersuffix){
            return self.arrSuffix.count
        }
        else if(pickerView == pickerGender){
            return self.arrGender.count
        }
        else if(pickerView == pickerServcMember){
            return arrServcMember.count
        }
        else if(pickerView == pickerGovtIDtyp){
            return arrGovtIDtyp.count
        }
            
        else if(pickerView == pickerGovtIDCountry){
            return arrGovtIDCountry.count
        }
            
        else if(pickerView == pickerGovtIDState){
            return arrGovtIDState.count
        }
            
        else if(pickerView == pickerAddrs_state){
            return arrGovtIDState.count
        }
            
        else if(pickerView == pickerAddrs_county){
            return arrGovtIDCountry.count
        }
            
        else if(pickerView == pickerEmpTyp){
            return arrEmptyp.count
        }
            
        else if(pickerView == pickerPayFreq){
            return arrPayFreq.count
        }
        else if(pickerView == pickerEmp_state){
            return arrGovtIDState.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerSalutation{
            let strTitle = arrSalutation[row]
            return strTitle
        }
            
        else if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            return strTitle
        }
            
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            return strTitle
        }
            
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            return strTitle
        }
            
        else if(pickerView == pickerGovtIDtyp){
            let strTitle = arrGovtIDtyp[row]
            return strTitle
        }
            
        else if(pickerView == pickerGovtIDCountry){
            let dict = arrGovtIDCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerGovtIDState){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerAddrs_state){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerAddrs_county){
            let dict = arrGovtIDCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerEmpTyp){
            let strTitle = arrEmptyp[row]
            return strTitle
        }
        else if(pickerView == pickerPayFreq){
            let strTitle = arrPayFreq[row]
            return strTitle
        }
        else if(pickerView == pickerEmp_state){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerSalutation{
            let strTitle = arrSalutation[row]
            self.txtSalutation.text = strTitle
        }
            
        else if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            self.txtsuffix.text = strTitle
        }
            
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            self.txtGender.text = strTitle
        }
            
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            self.txtServcMmber.text = strTitle
            
            if(strTitle == "Yes"){
                self.sericerMmber = "true"
            }
            else{
                self.sericerMmber = "false"
            }
        }
            
        else if(pickerView == pickerGovtIDtyp){
            let strTitle = arrGovtIDtyp[row]
            self.txtAltGovIdTyp.text = strTitle
        }
            
        else if(pickerView == pickerGovtIDCountry){
            let dict = arrGovtIDCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            self.txtAltGovIdCountry.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.country_id = "\(_id)"
        }
            
        else if(pickerView == pickerGovtIDState){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtAltGovIdState.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.state_id = "\(_id)"
        }
            
        else if(pickerView == pickerAddrs_state){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtCurr_State.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.address_state_id = "\(_id)"
        }
            
        else if(pickerView == pickerAddrs_county){
            let dict = arrGovtIDCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            self.txtCurr_Country.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.address_country_id = "\(_id)"
            
        }
        else if(pickerView == pickerEmpTyp){
            let strTitle = arrEmptyp[row]
            self.txtCurr_EmpTyp.text = strTitle
        }
        else if(pickerView == pickerPayFreq){
            let strTitle = arrPayFreq[row]
            self.txtCurr_PayFreq.text = strTitle
        }
            
        else if(pickerView == pickerEmp_state){
            let dict = arrGovtIDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            
            self.txtCurr_StateEmp.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.emp_state_id = "\(_id)"
            
        }
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtSalutation){
            self.pickUp(txtSalutation)
        }
            
        else if(textField == self.txtsuffix){
            self.pickUp(txtsuffix)
        }
            
        else if(textField == self.txtGender){
            self.pickUp(txtGender)
        }
            
        else if(textField == self.txtServcMmber){
            self.pickUp(txtServcMmber)
        }
            
        else if(textField == self.txtAltGovIdTyp){
            self.pickUp(txtAltGovIdTyp)
        }
            
        else if(textField == self.txtAltGovIdCountry){
            self.pickUp(txtAltGovIdCountry)
        }
            
        else if(textField == self.txtAltGovIdState){
            self.pickUp(txtAltGovIdState)
        }
            
        else if(textField == self.txtCurr_State){
            self.pickUp(txtCurr_State)
        }
            
            
            
        else if(textField == self.txtCurr_Country){
            self.pickUp(txtCurr_Country)
        }
        else if(textField == self.txtCurr_EmpTyp){
            self.pickUp(txtCurr_EmpTyp)
        }
        else if(textField == self.txtCurr_PayFreq){
            self.pickUp(txtCurr_PayFreq)
        }
        else if(textField == self.txtCurr_StateEmp){
            self.pickUp(txtCurr_StateEmp)
        }
            
        else if(textField == self.txtdob){
            self.pickUpDateTime(txtdob)
        }
            
        else if(textField == self.txtformFild){
            self.pickUpDateTime(txtformFild)
        }
            
        else if(textField == self.txtDLIssueDate){
            self.pickUpDateTime(txtDLIssueDate)
        }
            
        else if(textField == self.txtDLExpDate){
            self.pickUpDateTime(txtDLExpDate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtSalutation){
            self.pickerSalutation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSalutation.delegate = self
            self.pickerSalutation.dataSource = self
            self.pickerSalutation.backgroundColor = UIColor.white
            textField.inputView = self.pickerSalutation
        }
            
        else if(textField == self.txtsuffix){
            self.pickersuffix = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersuffix.delegate = self
            self.pickersuffix.dataSource = self
            self.pickersuffix.backgroundColor = UIColor.white
            textField.inputView = self.pickersuffix
        }
            
        else if(textField == self.txtGender){
            self.pickerGender = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGender.delegate = self
            self.pickerGender.dataSource = self
            self.pickerGender.backgroundColor = UIColor.white
            textField.inputView = self.pickerGender
        }
            
        else if(textField == self.txtAltGovIdTyp){
            self.pickerGovtIDtyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGovtIDtyp.delegate = self
            self.pickerGovtIDtyp.dataSource = self
            self.pickerGovtIDtyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerGovtIDtyp
        }
            
        else if(textField == self.txtAltGovIdState){
            self.pickerGovtIDState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGovtIDState.delegate = self
            self.pickerGovtIDState.dataSource = self
            self.pickerGovtIDState.backgroundColor = UIColor.white
            textField.inputView = self.pickerGovtIDState
        }
            
        else if(textField == self.txtAltGovIdCountry){
            self.pickerGovtIDCountry = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGovtIDCountry.delegate = self
            self.pickerGovtIDCountry.dataSource = self
            self.pickerGovtIDCountry.backgroundColor = UIColor.white
            textField.inputView = self.pickerGovtIDCountry
        }
            
        else if(textField == self.txtCurr_State){
            self.pickerAddrs_state = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAddrs_state.delegate = self
            self.pickerAddrs_state.dataSource = self
            self.pickerAddrs_state.backgroundColor = UIColor.white
            textField.inputView = self.pickerAddrs_state
        }
            
        else if(textField == self.txtCurr_Country){
            self.pickerAddrs_county = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAddrs_county.delegate = self
            self.pickerAddrs_county.dataSource = self
            self.pickerAddrs_county.backgroundColor = UIColor.white
            textField.inputView = self.pickerAddrs_county
        }
            
        else if(textField == self.txtCurr_EmpTyp){
            self.pickerEmpTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEmpTyp.delegate = self
            self.pickerEmpTyp.dataSource = self
            self.pickerEmpTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerEmpTyp
        }
            
        else if(textField == self.txtCurr_PayFreq){
            self.pickerPayFreq = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerPayFreq.delegate = self
            self.pickerPayFreq.dataSource = self
            self.pickerPayFreq.backgroundColor = UIColor.white
            textField.inputView = self.pickerPayFreq
        }
        else if(textField == self.txtCurr_StateEmp){
            self.pickerEmp_state = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerEmp_state.delegate = self
            self.pickerEmp_state.dataSource = self
            self.pickerEmp_state.backgroundColor = UIColor.white
            textField.inputView = self.pickerEmp_state
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtSalutation.resignFirstResponder()
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtServcMmber.resignFirstResponder()
        txtAltGovIdState.resignFirstResponder()
        txtAltGovIdCountry.resignFirstResponder()
        txtAltGovIdTyp.resignFirstResponder()
        txtCurr_State.resignFirstResponder()
        txtCurr_Country.resignFirstResponder()
        txtCurr_EmpTyp.resignFirstResponder()
        txtCurr_PayFreq.resignFirstResponder()
        txtCurr_StateEmp.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtSalutation.resignFirstResponder()
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtServcMmber.resignFirstResponder()
        txtAltGovIdState.resignFirstResponder()
        txtAltGovIdCountry.resignFirstResponder()
        txtAltGovIdTyp.resignFirstResponder()
        txtCurr_State.resignFirstResponder()
        txtCurr_Country.resignFirstResponder()
        txtCurr_EmpTyp.resignFirstResponder()
        txtCurr_PayFreq.resignFirstResponder()
        txtCurr_StateEmp.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtdob){
            self.pickerDob = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDob.datePickerMode = .date
            self.pickerDob.backgroundColor = UIColor.white
            textField.inputView = self.pickerDob
            dateSelect = "dob"
        }
            
        else if(textField == self.txtformFild){
            self.pickerFormfiled = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerFormfiled.datePickerMode = .date
            self.pickerFormfiled.backgroundColor = UIColor.white
            textField.inputView = self.pickerFormfiled
            dateSelect = "form"
        }
            
        else if(textField == self.txtDLIssueDate){
            self.pickerIssueDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerIssueDate.datePickerMode = .date
            self.pickerIssueDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerIssueDate
            dateSelect = "issue"
        }
            
        else if(textField == self.txtDLExpDate){
            self.pickerExpDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerExpDate.datePickerMode = .date
            self.pickerExpDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerExpDate
            dateSelect = "expire"
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        if(dateSelect == "dob"){
            self.txtdob.text = formatter.string(from: pickerDob.date)
            self.txtdob.resignFirstResponder()
        }
        else if(dateSelect == "form"){
            self.txtformFild.text = formatter.string(from: pickerFormfiled.date)
            self.txtformFild.resignFirstResponder()
        }
        else if(dateSelect == "issue"){
            self.txtDLIssueDate.text = formatter.string(from: pickerIssueDate.date)
            self.txtDLIssueDate.resignFirstResponder()
        }
        else if(dateSelect == "expire"){
            self.txtDLExpDate.text = formatter.string(from: pickerExpDate.date)
            self.txtDLExpDate.resignFirstResponder()
        }
    }
    
    @objc func cancelClickDate() {
        txtdob.resignFirstResponder()
        txtformFild.resignFirstResponder()
        self.txtDLIssueDate.resignFirstResponder()
        self.txtDLExpDate.resignFirstResponder()
    }
}
