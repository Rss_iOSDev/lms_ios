//
//  borr_AdressHistVC.swift
//  LMS
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.

import UIKit

class borr_AdressHistVC: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    @IBOutlet weak var tblAddressHistry : UITableView!
    var arrAddressData : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblAddressHistry.tableFooterView = UIView()
        self.callApi()
    }
    
    func callApi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                let acctId = dataD["_id"] as! Int
                
                let Str_url = "\(Constant.address_history)?id=\(acctId)"
                self.call_getAddressHistory(strUrl: Str_url)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension borr_AdressHistVC {
    
    func call_getAddressHistory(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                            if let arrAddress = response["data"] as? [NSDictionary]{
                                if(arrAddress.count > 0){
                                    for i in 0..<arrAddress.count{
                                        let dictA = arrAddress[i]
                                        self.arrAddressData.add(dictA)
                                    }
                                    self.arrAddressData =  NSMutableArray(array: self.arrAddressData.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                                    self.tblAddressHistry.reloadData()
                                }
                                else{
                                    self.arrAddressData = []
                                    self.tblAddressHistry.reloadData()
                                }
                            }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension borr_AdressHistVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrAddressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblAddressHistry.dequeueReusableCell(withIdentifier: "tblAddressHistryCell") as! tblAddressHistryCell
        let dict = self.arrAddressData[indexPath.row] as! NSDictionary
        if(indexPath.row == 0){
            cell.lblTitle.text = "C"
        }
        else{
            cell.lblTitle.text = "P"
        }
        if let address1 = dict["address1"] as? String
        {
            cell.lblAddress_1.text = "\(address1)"
        }
        if let address2 = dict["address2"] as? String
        {
            cell.lblAddress_2.text = "\(address2)"
        }
        if let city = dict["city"] as? String
        {
            cell.lblCity.text = "\(city)"
        }
        if let state = dict["state"] as? String
        {
            cell.lblState.text = "\(state)"
        }
        if let zipCode = dict["zipCode"] as? String
        {
            cell.lblZip.text = "\(zipCode)"
        }
        return cell
    }
}

class tblAddressHistryCell : UITableViewCell
{
    @IBOutlet weak var lblAddress_1 : UILabel!
    @IBOutlet weak var lblAddress_2 : UILabel!
    @IBOutlet weak var lblCity : UILabel!
    @IBOutlet weak var lblState : UILabel!
    @IBOutlet weak var lblZip : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
}
