//
//  borr_filesVC.swift
//  LMS
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
import WebKit
import SDWebImage

class borr_filesVC: UIViewController {
    
    @IBOutlet weak var tblFiles : UITableView!
    var arrFilesList : NSMutableArray = []
    
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    
    @IBOutlet weak var viewNoData : UIView!
    @IBOutlet weak var viewData : UIView!
    
    //view Documents
    
    
    @IBOutlet weak var viewMAin_Doc : UIView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var imgView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewNoData.isHidden = true
        self.viewData.isHidden = true
        self.viewMAin_Doc.isHidden = true
        self.imgView.isHidden = true
        self.webView.isHidden = true
        self.webView.navigationDelegate = self
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblActtNo.text = "Account no: \(acct_no)"
                let acctId = (dataD["_id"] as! Int)
                self.call_getFilesListApi(accId: acctId)
                
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

//Api --
extension borr_filesVC {
    
    func call_getFilesListApi(accId : Int)
    {
        let Str_url = "\(Constant.file_url)?account_id=\(accId)&action_type=borrower"
        BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let response = dict as? NSDictionary {
                        if let responseArr = response["data"] as? [NSDictionary] {
                            
                            if(responseArr.count > 0){
                                for i in 0..<responseArr.count{
                                    let dictA = responseArr[i] as! NSMutableDictionary
                                    dictA.setValue(false, forKey: "remove")
                                    self.arrFilesList.add(dictA)
                                }
                                
                                self.viewNoData.isHidden = true
                                self.viewData.isHidden = false
                                self.viewMAin_Doc.isHidden = true
                                
                                self.tblFiles.reloadData()
                            }
                            else{
                                self.arrFilesList = []
                                
                                self.viewNoData.isHidden = false
                                self.viewData.isHidden = true
                                self.viewMAin_Doc.isHidden = true
                                
                                self.tblFiles.reloadData()
                            }
                        }
                    }
                    

                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
    }
}

extension borr_filesVC : UITableViewDataSource , UITableViewDelegate, WKNavigationDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFilesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblFiles.dequeueReusableCell(withIdentifier: "coll_filesTbl") as! coll_filesTbl
        
        let dict = arrFilesList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let is_deleted = dict["remove"] as? Bool{
            if(is_deleted == false){
                cell.btnRemove.isSelected = false
            }
            else{
                cell.btnRemove.isSelected = true
            }
        }

        
        if let file_desc = dict["file_desc"] as? String
        {
            cell.lblDescription.text = file_desc
        }
        
        if let createdAt = dict["createdAt"] as? String
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy HH:mm a"
            let date = Date.dateFromISOString(string: createdAt)
            cell.lblDate.text = formatter.string(from: date!)
        }
        
        let file_type = dict["file_type"] as! String
        let extnsn = file_type.components(separatedBy: "/")
        
        if((extnsn.last!) == "png"){
            cell.imgFileTyp.image = UIImage(named: "png")
        }
        else if((extnsn.last!) == "pdf"){
            cell.imgFileTyp.image = UIImage(named: "pdf")
        }
        else {
            cell.imgFileTyp.image = UIImage(named: "notepad")
        }
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action: #selector(clikcONViewBtn(_:)), for: .touchUpInside)
        
        
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(clickONBtnDel(_:)), for: .touchUpInside)
        
        
        
        
        return cell
    }
    
    @objc func clikcONViewBtn(_ sender : UIButton)
    {
        let dict = arrFilesList[sender.tag]  as! NSDictionary
        if let file_name = dict["file_name"] as? String
        {
            let fileUrl = "\(Constant.file_baseUrl)\(file_name)"
            Globalfunc.print(object:fileUrl)
            
            let file_type = dict["file_type"] as! String
            let extnsn = file_type.components(separatedBy: "/")
            
            if((extnsn.last!) == "png"){
                //show imgView
                
                self.viewMAin_Doc.isHidden = false
                self.imgView.isHidden = false
                self.webView.isHidden = true
                showImgView(strUrl: fileUrl)
                
                
            }
            else{
                //show webview
                self.viewMAin_Doc.isHidden = false
                self.imgView.isHidden = true
                self.webView.isHidden = false
                
                loadWebView(strUrl: fileUrl)
                
            }
        }
    }
    
    func loadWebView(strUrl : String)
    {
        let url = URL(string: strUrl)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    func showImgView(strUrl : String)
    {
        //self.imgView.setIndicatorStyle(UIActivityIndicatorView.Style.gray)
        //self.imgView.setShowActivityIndicator(true)
        self.imgView.sd_setImage(with: URL.init(string: strUrl), placeholderImage: UIImage(named: ""))
    }
    
    @IBAction func clickONCros(_ sender : UIButton)
    {
        self.viewMAin_Doc.isHidden = true
        self.imgView.isHidden = true
        self.webView.isHidden = true
    }
    
    @objc func clickONBtnDel(_ sender: UIButton){
        
        
        let dict = arrFilesList[sender.tag] as! NSMutableDictionary
        
        if let remove = dict["remove"] as? Bool{
            if(remove == false){
                sender.isSelected = true
                dict.setValue(true, forKey: "remove")
                self.arrFilesList.replaceObject(at: sender.tag, with: dict)
                self.tblFiles.reloadData()

            }
            else{
                sender.isSelected = false
                dict.setValue(false, forKey: "remove")
                self.arrFilesList.replaceObject(at: sender.tag, with: dict)
                self.tblFiles.reloadData()

            }
        }
    }
    
    @IBAction func clickONDelIdBtn(_ sender: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let arrSupportfile = NSMutableArray()
        for i in 0..<arrFilesList.count{
                let newDict : NSMutableDictionary = [:]
                let obj = arrFilesList[i] as! NSDictionary
                
                let remove = obj["remove"] as! Bool
                let file_desc = obj["file_desc"] as! String
                let id = obj["_id"] as! String
                
                newDict.setValue(remove, forKey: "remove")
                newDict.setValue(file_desc, forKey: "desc")
                newDict.setValue(id, forKey: "id")
                    
                arrSupportfile.add(newDict)
        }
        let param = ["supportFile":arrSupportfile,
                         "userid": (user?._id)!] as [String: Any]
        BaseApi.onResponsePutWithToken(url: Constant.file_url, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.viewWillAppear(true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
        }
    }
}
