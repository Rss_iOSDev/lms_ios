//
//  borr_refrence_detailVC.swift
//  LMS
//
//  Created by Apple on 04/08/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borr_refrence_detailVC: UIViewController {
    
    var passDict : NSDictionary = [:]
    
    @IBOutlet weak var lblActtNo : UILabel!
    
    @IBOutlet weak var viewEdit : UIView!
    
    @IBOutlet weak var viewIdentityInfo : UIView!
    @IBOutlet weak var lblFrstName : UILabel!
    @IBOutlet weak var lblMiddleName : UILabel!
    @IBOutlet weak var lblLastName : UILabel!
    @IBOutlet weak var lblsuffix : UILabel!
    @IBOutlet weak var lblGender : UILabel!
    @IBOutlet weak var lbldob : UILabel!
    @IBOutlet weak var lblSsn : UILabel!
    
    @IBOutlet weak var txtFrstName : UITextField!
    @IBOutlet weak var txtMiddleName : UITextField!
    @IBOutlet weak var txtLastName : UITextField!
    @IBOutlet weak var txtsuffix : UITextField!
    @IBOutlet weak var txtGender : UITextField!
    @IBOutlet weak var txtdob : UITextField!
    @IBOutlet weak var txtSsn : UITextField!
    @IBOutlet weak var txtServcMmber : UITextField!
    
    
    @IBOutlet weak var heightViewCont : NSLayoutConstraint!
    @IBOutlet weak var viewUpdate : UIView!
    @IBOutlet weak var lnlName : UILabel!
    @IBOutlet weak var lblTitle_name : UILabel!
    @IBOutlet weak var lblAddress : UILabel!
    @IBOutlet weak var lblcity : UILabel!
    
    @IBOutlet weak var txtDLNo : UITextField!
    @IBOutlet weak var txtDLIssueDate : UITextField!
    @IBOutlet weak var txtDLState : UITextField!
    @IBOutlet weak var txtDLExpDate : UITextField!
    @IBOutlet weak var txtformFild : UITextField!
    
    @IBOutlet weak var txtemail : UITextField!
    @IBOutlet weak var txthomePhn : UITextField!
    @IBOutlet weak var txthomePhn_2 : UITextField!
    @IBOutlet weak var txtWorkPhn : UITextField!
    @IBOutlet weak var txtCelPhn : UITextField!
    @IBOutlet weak var txtWorkPhn_2 : UITextField!
    @IBOutlet weak var txtWorkExtsn : UITextField!
    @IBOutlet weak var txtWorkExtsn_2 : UITextField!
    
    @IBOutlet weak var txtCurr_Address1 : UITextField!
    @IBOutlet weak var txtCurr_Address2 : UITextField!
    @IBOutlet weak var txtCurr_City : UITextField!
    @IBOutlet weak var txtCurr_State : UITextField!
    @IBOutlet weak var txtCurr_Zipcode : UITextField!
    @IBOutlet weak var txtCurr_Conty : UITextField!
    @IBOutlet weak var txtCurr_Country : UITextField!
    @IBOutlet weak var txtCurr_years : UITextField!
    @IBOutlet weak var txtCurr_months : UITextField!
    @IBOutlet weak var txtLienHolder : UITextField!
    @IBOutlet weak var txtLienHolderPhn : UITextField!
    @IBOutlet weak var txtResidencePymnt : UITextField!
    @IBOutlet weak var txtResidenceTyp : UITextField!
    
    @IBOutlet weak var btnAuthorise : UIButton!
    @IBOutlet weak var txtRelation : UITextField!
    var pickerRelation : UIPickerView!
    var arrRelation = NSMutableArray()
    var relation_id = ""
    
    
    var pickerDLstate : UIPickerView!
    var pickerAddrs_county: UIPickerView!
    var pickerAddrs_state : UIPickerView!
    var dl_state_id = ""
    var address_state_id = ""
    var address_country_id = ""
    
    var arrCountry = NSMutableArray()
    var arrDState = NSMutableArray()
    
    var pickerResidncTyp: UIPickerView!
    var arrResidnctyp = ["Rent","Own-Mortage","Own-No Mortage","Living with other"]
    
    var pickerDob : UIDatePicker!
    var pickerFormfiled : UIDatePicker!
    
    var pickerIssueDate : UIDatePicker!
    var pickerExpDate : UIDatePicker!
    
    var arrSuffix = ["Sr.","Jr.","I","II","III","IV","V"]
    var arrGender = ["Male","Female"]
    var arrServcMember = ["Yes","No"]
    
    
    var pickersuffix: UIPickerView!
    
    var pickerGender : UIPickerView!
    var pickerServcMember: UIPickerView!
    
    var sericerMmber = ""
    var dateSelect = ""
    var address_id = ""
    
    var isAuthorise = false
    
    var arrUrl = [Constant.state,Constant.country,Constant.relationship]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.viewEdit.isHidden = true
        self.viewIdentityInfo.isHidden = true
        self.viewUpdate.isHidden = true
        self.heightViewCont.constant = 0
        
        Globalfunc.showLoaderView(view: self.view)
        self.callGetPickerApi()
        self.getPstExtensionapiApi()
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONEditBtn(_ sender: UIButton)
    {
        self.viewEdit.isHidden = false
        if(sender.tag == 10){
            self.viewIdentityInfo.isHidden = false
        }
    }
    
    @IBAction func clickONCloseBtn(_ sender: UIButton)
    {
        self.viewWillAppear(true)
    }
    
    @IBAction func clcikOnAuthoriseBtn(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            isAuthorise = false
        }else {
            sender.isSelected = true
            isAuthorise = true
        }
    }
    
}

//call action api for all pages once
extension borr_refrence_detailVC{
    
    func callGetPickerApi()
    {
        let group = DispatchGroup() // initialize
        arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if(obj == Constant.state){
                            
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrDState.add(dictA)
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                        if(obj == Constant.country){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrCountry.add(dictA)
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                        if(obj == Constant.relationship){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrRelation.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
    
    func getPstExtensionapiApi()
    {
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                if let account_no = dataD["account_no"] as? String{
                    self.lblActtNo.text = "Account no: \(account_no)"
                }
                
                //http://18.191.178.120:3000/api/add-reference/detail?ref_id=56&borrower_id=9&ref_type=New
                
                print(self.passDict)
                
                let id = self.passDict["id"] as! Int
                let borrower_id = self.passDict["borrower_id"] as! Int
                let type = self.passDict["type"] as! String
                let strUrl = "\(Constant.add_reference)/detail?ref_id=\(id)&borrower_id=\(borrower_id)&ref_type=\(type)"
                
                
                print(strUrl)
                
                BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                    
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.print(object: dict)
                            
                            if let responseDict = dict as? NSDictionary{
                                if let data = responseDict["data"] as? NSDictionary{
                                    self.parseData(response: data)
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                    
                }
                
            }
        }
        catch
        {
        }
    }
    
    func parseData( response: NSDictionary)
    {
        print(response)
        
        let is_authorized = response["is_authorized"] as! Bool
        if(is_authorized == true){
            btnAuthorise.isSelected = true
            self.isAuthorise = true
        }
        else if(self.isAuthorise == false){
            btnAuthorise.isSelected = false
            self.isAuthorise = false
            
        }
        
        let _id = response["_id"] as! Int
        userDef.set(_id, forKey: "borrower_id")
        
        let relationship = response["relationship"] as! Int
        for i in 0..<self.arrRelation.count
        {
            let dict = self.arrRelation[i] as! NSDictionary
            let _id = dict["_id"] as! Int
            if(relationship == _id){
                self.relation_id = "\(_id)"
                self.txtRelation.text = "\(dict["title"] as! String)"
                break
            }
        }
        
        if let identity_info = response["identity_info"] as? NSDictionary{
            self.setIDentityInfoData(identity_info: identity_info)
        }
        
        if let drivers_license = response["drivers_license"] as? NSDictionary{
            self.setDrivingLicencrInfo(drivers_license)
        }
        
        if let contact_info = response["contact_info"] as? NSDictionary{
            self.setContactInfoData(contact_info: contact_info)
        }
        
        if let current_address = response["address"] as? NSDictionary{
            
            self.setCurr_AddressData(c_address: current_address)
        }
    }
    
    func setIDentityInfoData( identity_info : NSDictionary)
    {
        if let first_name = identity_info["first_name"] as? String
        {
            self.lblFrstName.text = first_name
            self.txtFrstName.text = first_name
        }
        
        if let gender = identity_info["gender"] as? String
        {
            self.lblGender.text = gender
            self.txtGender.text = gender
        }
        
        if let last_name = identity_info["last_name"] as? String
        {
            self.lblLastName.text = last_name
            self.txtLastName.text = last_name
        }
        
        if let middle_name = identity_info["middle_name"] as? String
        {
            self.lblMiddleName.text = middle_name
            self.txtMiddleName.text = middle_name
        }
        
        if let last_8300_form_file = identity_info["last_8300_form_file"] as? String
        {
            if(last_8300_form_file != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: last_8300_form_file)
                self.txtformFild.text = formatter.string(from: date!)
            }
            else{
                self.txtformFild.text = ""
            }
        }
        
        if let suffix = identity_info["suffix"] as? String
        {
            self.lblsuffix.text = suffix
            self.txtsuffix.text = suffix
        }
        if let active_servicemember = identity_info["active_servicemember"] as? String
        {
            if(active_servicemember == "true")
            {
                self.txtServcMmber.text = arrServcMember[0]
            }
            else if(active_servicemember == "false")
            {
                self.txtServcMmber.text = arrServcMember[1]
            }
        }
        if let customer_ssn = identity_info["customer_ssn"] as? String
        {
            self.lblSsn.text = customer_ssn
            self.txtSsn.text = customer_ssn
            
        }
        if let date_of_birth = identity_info["date_of_birth"] as? String
        {
            if(date_of_birth != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: date_of_birth)
                self.lbldob.text = formatter.string(from: date!)
                self.lbldob.text = formatter.string(from: date!)
            }
            else{
                self.lbldob.text = ""
                self.lbldob.text = ""
            }
        }
    }
    
    func setDrivingLicencrInfo(_ drivers_license: NSDictionary)
    {
        if let dl_no = drivers_license["dl_no"] as? String
        {
            self.txtDLNo.text = "\(dl_no)"
        }
        
        if let dl_state = drivers_license["dl_state"] as? String
        {
            self.txtDLState.text = "\(dl_state)"
        }
        
        
        if let dl_exp_date = drivers_license["dl_exp_date"] as? String
        {
            if(dl_exp_date != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: dl_exp_date)
                self.txtDLExpDate.text = formatter.string(from: date!)
            }
            else{
                self.txtDLExpDate.text = ""
            }
        }
        
        if let dl_issue_date = drivers_license["dl_issue_date"] as? String
        {
            if(dl_issue_date != ""){
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: dl_issue_date)
                self.txtDLIssueDate.text = formatter.string(from: date!)
            }
            else{
                self.txtDLIssueDate.text = ""
            }
        }
    }
    
    
    
    func setContactInfoData( contact_info : NSDictionary)
    {
        if let cellPhone = contact_info["cellPhone"] as? NSNumber
        {
            self.txtCelPhn.text = "\(cellPhone)"
        }
        
        if let email = contact_info["email"] as? String
        {
            self.txtemail.text = "\(email)"
        }
        
        
        
        if let homePhone = contact_info["homePhone"] as? String
        {
            self.txthomePhn.text = "\(homePhone)"
        }
        
        if let homePhone2 = contact_info["homePhone2"] as? String
        {
            self.txthomePhn_2.text = "\(homePhone2)"
        }
        
        if let workExtension = contact_info["workExtension"] as? String
        {
            self.txtWorkExtsn_2.text = "\(workExtension)"
        }
        
        if let workExtension2 = contact_info["workExtension2"] as? String
        {
            self.txtWorkExtsn_2.text = "\(workExtension2)"
        }
        
        if let workPhone = contact_info["workPhone"] as? String
        {
            self.txtWorkPhn_2.text = "\(workPhone)"
        }
        
        if let workPhone2 = contact_info["workPhone2"] as? String
        {
            self.txtWorkPhn_2.text = "\(workPhone2)"
        }
        
    }
    
    func setCurr_AddressData( c_address : NSDictionary)
    {
        
        if let _id = c_address["_id"] as? String
        {
            self.address_id = _id
        }
        
        if let address1 = c_address["address1"] as? String
        {
            self.txtCurr_Address1.text = "\(address1)"
        }
        
        if let address2 = c_address["address2"] as? String
        {
            self.txtCurr_Address2.text = "\(address2)"
        }
        
        if let city = c_address["city"] as? String
        {
            self.txtCurr_City.text = "\(city)"
        }
        
        if let country = c_address["country"] as? String
        {
            self.txtCurr_Country.text = "\(country)"
        }
        
        if let county = c_address["county"] as? String
        {
            self.txtCurr_Conty.text = "\(county)"
        }
        
        if let monthsAt = c_address["monthsAt"] as? Int
        {
            self.txtCurr_months.text = "\(monthsAt)"
        }
        
        if let state = c_address["state"] as? String
        {
            self.txtCurr_State.text = "\(state)"
        }
        
        if let yearsAt = c_address["yearsAt"] as? Int
        {
            self.txtCurr_years.text = "\(yearsAt)"
        }
        
        if let zipCode = c_address["zipCode"] as? String
        {
            self.txtCurr_Zipcode.text = "\(zipCode)"
        }
    }
}

extension borr_refrence_detailVC {
    
    @IBAction func clickONSubmitBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            
            self.viewUpdate.isHidden = false
            self.heightViewCont.constant = 150
            
            self.lnlName.text = "\(txtFrstName.text!) \(txtLastName.text!) \(txtsuffix.text!)"
            self.lblTitle_name.text = String((txtFrstName.text?.first)!)
            self.lblAddress.text = "\(txtCurr_Address1.text!)"
            self.lblcity.text = "\(txtCurr_City.text!) \(txtCurr_State.text!) \(txtCurr_Zipcode.text!)"
            
        }
            
        else if(sender.tag == 20){
            
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let accId_selected = dataD["_id"] as! Int
                    
                    let type = self.passDict["type"] as! String
                    let borr_id = userDef.value(forKey: "borrower_id") as! Int
                    let params = [
                        "account_id": accId_selected,
                        "active_servicemember": sericerMmber,
                        "customer_ssn": "\(txtSsn.text!)",
                        "date_of_birth": "\(self.txtdob.text!)",
                        "first_name": "\(txtFrstName.text!)",
                        "gender": "\(txtGender.text!)",
                        "id": borr_id,
                        "last_8300_form_file": "",
                        "last_name": "\(txtLastName.text!)" ,
                        "middle_name": "\(txtMiddleName.text!)",
                        "salutation": "",
                        "ref_type": type,
                        "suffix": "\(txtsuffix.text!)"] as [String : Any]
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.ref_identity_info)
                    
                }
            }
            catch{
                
            }
            
        }
        else if(sender.tag == 100){
            
            do {
                let decoded  = userDef.object(forKey: "dataDict") as! Data
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let accId_selected = dataD["_id"] as! Int
                    
                    let type = self.passDict["type"] as! String
                    let id = self.passDict["id"] as! Int
                    let userid = dataD["userid"] as! Int
                    
                    let borr_id = userDef.value(forKey: "borrower_id") as! Int
                    
                    let params = [
                        "account_id": accId_selected,
                        "address1": "\(self.txtCurr_Address1.text!)",
                        "address2": "\(self.txtCurr_Address2.text!)",
                        "address_id": self.address_id,
                        "cellPhone": "\(self.txtCelPhn.text!)",
                        "city": "\(self.txtCurr_City.text!)",
                        "country": "\(self.txtCurr_Country.text!)",
                        "county": "\(self.txtCurr_Conty.text!)",
                        "dl_exp_date": "\(self.txtDLExpDate.text!)",
                        "dl_issue_date": "\(self.txtDLIssueDate.text!)",
                        "dl_no": "\(self.txtDLNo.text!)",
                        "dl_state": self.dl_state_id,
                        "email": "\(self.txtemail.text!)",
                        "homePhone2": "\(self.txthomePhn.text!)",
                        "position": "\(self.txthomePhn_2.text!)",
                        "is_authorized":isAuthorise,
                        "last_8300_form_file": "\(self.txtformFild.text!)",
                        "lienholder": "\(txtLienHolder.text!)",
                        "lienholderPhone": "\(txtLienHolderPhn.text!)",
                        "monthsAt": "\(self.txtCurr_months.text!)",
                        "ref_type": type,
                        "relationship": self.relation_id,
                        "residencePayment": self.txtResidencePymnt.text!,
                        "residenceType": self.txtResidenceTyp.text!,
                        "state": self.address_state_id,
                        "userid": userid,
                        "workExtension": self.txtWorkExtsn.text!,
                        "workExtension2": self.txtWorkExtsn_2.text!,
                        "workPhone": self.txtWorkPhn.text!,
                        "workPhone2": self.txtWorkPhn_2.text!,
                        "yearsAt": self.txtCurr_years.text!,
                        "zipCode": self.txtCurr_Zipcode.text!,
                        "borrower_accid": borr_id,
                        "borrower_id": id,
                        "id": borr_id
                        ] as [String : Any]
                    Globalfunc.print(object:params)
                    self.callUpdateApi(param: params, StrUrl: Constant.add_reference)
                }
            }
            catch{
            }
        }
        
    }
    
    
    func callUpdateApi(param: [String : Any], StrUrl : String)
    {
        BaseApi.onResponsePutWithToken(url: StrUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewWillAppear(true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
        
    }
    
}


/*MARk - Textfeld and picker delegates*/

extension borr_refrence_detailVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickersuffix){
            return self.arrSuffix.count
        }
        else if(pickerView == pickerGender){
            return self.arrGender.count
        }
        else if(pickerView == pickerServcMember){
            return arrServcMember.count
        }
            
        else if(pickerView == pickerAddrs_county){
            return arrCountry.count
        }
            
        else if(pickerView == pickerDLstate){
            return arrDState.count
        }
            
        else if(pickerView == pickerAddrs_state){
            return arrDState.count
        }
            
        else if(pickerView == pickerResidncTyp){
            return arrResidnctyp.count
        }
        else if(pickerView == pickerRelation){
            return arrRelation.count
        }
        
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            return strTitle
        }
            
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            return strTitle
        }
            
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            return strTitle
        }
            
        else if(pickerView == pickerAddrs_county){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
            
        else if(pickerView == pickerDLstate){
            let dict = arrDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerAddrs_state){
            let dict = arrDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        else if(pickerView == pickerResidncTyp){
            let strTitle = arrResidnctyp[row]
            return strTitle
        }
        else if(pickerView == pickerRelation){
            let dict = arrRelation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickersuffix){
            let strTitle = arrSuffix[row]
            self.txtsuffix.text = strTitle
        }
            
        else if(pickerView == pickerGender){
            let strTitle = arrGender[row]
            self.txtGender.text = strTitle
        }
            
        else if(pickerView == pickerServcMember){
            let strTitle = arrServcMember[row]
            self.txtServcMmber.text = strTitle
            
            if(strTitle == "Yes"){
                self.sericerMmber = "true"
            }
            else{
                self.sericerMmber = "false"
            }
        }
            
        else if(pickerView == pickerDLstate){
            let dict = arrDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtDLState.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.dl_state_id = "\(_id)"
        }
            
        else if(pickerView == pickerAddrs_state){
            let dict = arrDState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            self.txtCurr_State.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.address_state_id = "\(_id)"
        }
            
        else if(pickerView == pickerAddrs_county){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            self.txtCurr_Country.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.address_country_id = "\(_id)"
            
        }
        else if(pickerView == pickerResidncTyp){
            let strTitle = arrResidnctyp[row]
            self.txtResidenceTyp.text = strTitle
        }
        else if(pickerView == pickerRelation){
            let dict = arrRelation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtRelation.text = strTitle
            let _id = dict["_id"] as! NSNumber
            self.relation_id = "\(_id)"
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtsuffix){
            self.pickUp(txtsuffix)
        }
            
        else if(textField == self.txtGender){
            self.pickUp(txtGender)
        }
            
        else if(textField == self.txtServcMmber){
            self.pickUp(txtServcMmber)
        }
            
        else if(textField == self.txtDLState){
            self.pickUp(txtDLState)
        }
            
        else if(textField == self.txtRelation){
            self.pickUp(txtRelation)
        }
            
            
        else if(textField == self.txtCurr_State){
            self.pickUp(txtCurr_State)
        }
            
        else if(textField == self.txtCurr_Country){
            self.pickUp(txtCurr_Country)
        }
            
        else if(textField == self.txtResidenceTyp){
            self.pickUp(txtResidenceTyp)
        }
            
        else if(textField == self.txtdob){
            self.pickUpDateTime(txtdob)
        }
            
        else if(textField == self.txtformFild){
            self.pickUpDateTime(txtformFild)
        }
            
        else if(textField == self.txtDLIssueDate){
            self.pickUpDateTime(txtDLIssueDate)
        }
            
        else if(textField == self.txtDLExpDate){
            self.pickUpDateTime(txtDLExpDate)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtsuffix){
            self.pickersuffix = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersuffix.delegate = self
            self.pickersuffix.dataSource = self
            self.pickersuffix.backgroundColor = UIColor.white
            textField.inputView = self.pickersuffix
        }
            
        else if(textField == self.txtGender){
            self.pickerGender = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerGender.delegate = self
            self.pickerGender.dataSource = self
            self.pickerGender.backgroundColor = UIColor.white
            textField.inputView = self.pickerGender
        }
            
        else if(textField == self.txtServcMmber){
            self.pickerServcMember = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerServcMember.delegate = self
            self.pickerServcMember.dataSource = self
            self.pickerServcMember.backgroundColor = UIColor.white
            textField.inputView = self.pickerServcMember
        }
            
        else if(textField == self.txtDLState){
            self.pickerDLstate = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDLstate.delegate = self
            self.pickerDLstate.dataSource = self
            self.pickerDLstate.backgroundColor = UIColor.white
            textField.inputView = self.pickerDLstate
        }
            
            
        else if(textField == self.txtCurr_State){
            self.pickerAddrs_state = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAddrs_state.delegate = self
            self.pickerAddrs_state.dataSource = self
            self.pickerAddrs_state.backgroundColor = UIColor.white
            textField.inputView = self.pickerAddrs_state
        }
            
        else if(textField == self.txtCurr_Country){
            self.pickerAddrs_county = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerAddrs_county.delegate = self
            self.pickerAddrs_county.dataSource = self
            self.pickerAddrs_county.backgroundColor = UIColor.white
            textField.inputView = self.pickerAddrs_county
        }
            
        else if(textField == self.txtResidenceTyp){
            self.pickerResidncTyp = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerResidncTyp.delegate = self
            self.pickerResidncTyp.dataSource = self
            self.pickerResidncTyp.backgroundColor = UIColor.white
            textField.inputView = self.pickerResidncTyp
        }
            
        else if(textField == self.txtRelation){
            self.pickerRelation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerRelation.delegate = self
            self.pickerRelation.dataSource = self
            self.pickerRelation.backgroundColor = UIColor.white
            textField.inputView = self.pickerRelation
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtServcMmber.resignFirstResponder()
        txtDLState.resignFirstResponder()
        txtCurr_State.resignFirstResponder()
        txtCurr_Country.resignFirstResponder()
        txtResidenceTyp.resignFirstResponder()
        txtRelation.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtsuffix.resignFirstResponder()
        txtGender.resignFirstResponder()
        txtServcMmber.resignFirstResponder()
        txtDLState.resignFirstResponder()
        txtCurr_State.resignFirstResponder()
        txtCurr_Country.resignFirstResponder()
        txtResidenceTyp.resignFirstResponder()
        txtRelation.resignFirstResponder()
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtdob){
            self.pickerDob = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerDob.datePickerMode = .date
            self.pickerDob.backgroundColor = UIColor.white
            textField.inputView = self.pickerDob
            dateSelect = "dob"
        }
            
        else if(textField == self.txtformFild){
            self.pickerFormfiled = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerFormfiled.datePickerMode = .date
            self.pickerFormfiled.backgroundColor = UIColor.white
            textField.inputView = self.pickerFormfiled
            dateSelect = "form"
        }
            
        else if(textField == self.txtDLIssueDate){
            self.pickerIssueDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerIssueDate.datePickerMode = .date
            self.pickerIssueDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerIssueDate
            dateSelect = "issue"
        }
            
        else if(textField == self.txtDLExpDate){
            self.pickerExpDate = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerExpDate.datePickerMode = .date
            self.pickerExpDate.backgroundColor = UIColor.white
            textField.inputView = self.pickerExpDate
            dateSelect = "expire"
        }
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        if(dateSelect == "dob"){
            self.txtdob.text = formatter.string(from: pickerDob.date)
            self.txtdob.resignFirstResponder()
        }
        else if(dateSelect == "form"){
            self.txtformFild.text = formatter.string(from: pickerFormfiled.date)
            self.txtformFild.resignFirstResponder()
        }
        else if(dateSelect == "issue"){
            self.txtDLIssueDate.text = formatter.string(from: pickerIssueDate.date)
            self.txtDLIssueDate.resignFirstResponder()
        }
        else if(dateSelect == "expire"){
            self.txtDLExpDate.text = formatter.string(from: pickerExpDate.date)
            self.txtDLExpDate.resignFirstResponder()
        }
    }
    
    @objc func cancelClickDate() {
        txtdob.resignFirstResponder()
        txtformFild.resignFirstResponder()
        self.txtDLIssueDate.resignFirstResponder()
        self.txtDLExpDate.resignFirstResponder()
    }
}
