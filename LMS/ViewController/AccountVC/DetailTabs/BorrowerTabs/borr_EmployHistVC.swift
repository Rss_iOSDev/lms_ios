//
//  borr_EmployHistVC.swift
//  LMS
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borr_EmployHistVC: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    @IBOutlet weak var tblEmplymntHistry : UITableView!
    var arrEmploymntData : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblEmplymntHistry.tableFooterView = UIView()
        self.callApi()
    }
    
    func callApi(){
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                let acctId = dataD["_id"] as! Int
                let Str_url = "\(Constant.employment_history)?id=\(acctId)"
                self.call_getEmploymtHistory(strUrl: Str_url)
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension borr_EmployHistVC {
    
    func call_getEmploymtHistory(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary {
                        if let arrEmploymnt = response["data"] as? [NSDictionary]{
                            if(arrEmploymnt.count > 0){
                                for i in 0..<arrEmploymnt.count{
                                    let dictA = arrEmploymnt[i]
                                    self.arrEmploymntData.add(dictA)
                                }
                                self.arrEmploymntData =  NSMutableArray(array: self.arrEmploymntData.reverseObjectEnumerator().allObjects).mutableCopy() as! NSMutableArray
                                self.tblEmplymntHistry.reloadData()
                            }
                            else{
                                self.arrEmploymntData = []
                                self.tblEmplymntHistry.reloadData()
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension borr_EmployHistVC : UITableViewDataSource, UITableViewDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmploymntData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblEmplymntHistry.dequeueReusableCell(withIdentifier: "tblEmpHistryCell") as! tblEmpHistryCell
        let dict = self.arrEmploymntData[indexPath.row] as! NSDictionary
        
        if(indexPath.row == 0){
            cell.lblTitle.text = "C"
            cell.lblstatus.text = "C"
        }
        else{
            cell.lblTitle.text = "P"
            cell.lblstatus.text = "P"
        }
        if let address1 = dict["address1"] as? String
        {
            cell.lblAddress1.text = "\(address1)"
        }
        if let address2 = dict["address2"] as? String
        {
            cell.lblAddress2.text = "\(address2)"
        }
        if let city = dict["city"] as? String
        {
            cell.lblCity.text = "\(city)"
        }
        if let state = dict["state"] as? String
        {
            cell.lblState.text = "\(state)"
        }
        if let zipCode = dict["zipCode"] as? String
        {
            cell.lblZip.text = "\(zipCode)"
        }
        
        if let employer = dict["employer"] as? String
        {
            cell.lblEmployer.text = "\(employer)"
        }
        
        if let position = dict["position"] as? String
        {
            cell.lblPosition.text = "\(position)"
        }
        return cell
    }
}

class tblEmpHistryCell : UITableViewCell
{
    @IBOutlet weak var lblEmployer : UILabel!
    @IBOutlet weak var lblAddress1 : UILabel!
    @IBOutlet weak var lblAddress2 : UILabel!
    @IBOutlet weak var lblState : UILabel!
    @IBOutlet weak var lblZip : UILabel!
    @IBOutlet weak var lblCity : UILabel!
    @IBOutlet weak var lblPosition : UILabel!
    @IBOutlet weak var lblEmpDate : UILabel!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var lblstatusDate : UILabel!
    @IBOutlet weak var lblverifiedMthd : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
}
