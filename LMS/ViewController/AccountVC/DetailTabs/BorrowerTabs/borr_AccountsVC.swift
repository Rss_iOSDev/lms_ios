//
//  borr_AccountsVC.swift
//  LMS
//
//  Created by Apple on 09/07/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class borr_AccountsVC: UIViewController {
    
    
    @IBOutlet weak var tblAccounts : UITableView!
    @IBOutlet weak var lblStatus : UILabel!
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    var arrAccounts : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblAccounts.isHidden = true
        self.lblStatus.text = ""
        self.tblAccounts.tableFooterView = UIView()
        Globalfunc.showLoaderView(view: self.view)
        self.call_getReainingPayApi()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension borr_AccountsVC {
    
    func call_getReainingPayApi()
    {
        do{
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                let acct_no = dataD["account_no"] as! String
                self.lblAccountNo.text = "Account no: \(acct_no)"
                let acct_id = dataD["_id"] as! Int
                
                let strUrl = "\(Constant.borrower_accounts)?id=\(acct_id)"
                BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrAccounts.add(dictA)
                                        }
                                        self.tblAccounts.isHidden = false
                                        self.lblStatus.text = ""
                                        self.tblAccounts.reloadData()
                                    }
                                    else{
                                        self.tblAccounts.isHidden = true
                                        self.lblStatus.text = "No Records found"
                                    }
                                }
                            }
                            
                            
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
}

extension borr_AccountsVC : UITableViewDelegate , UITableViewDataSource
{
    func tableView(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblAccounts.dequeueReusableCell(withIdentifier: "tblBorrAccounts") as! tblBorrAccounts
        let dict = self.arrAccounts[indexPath.row] as! NSDictionary
        
        Globalfunc.print(object:dict)
        
        if let promiseAmount = dict["promiseAmount"] as? String
        {
            cell.lnlPromAmt.text = "\(promiseAmount)"
        }
        
        if let promiseDate = dict["promiseDate"] as? String
        {
            cell.lblPromDate.text = "\(promiseDate)"
        }
        
        if let promiseStatus = dict["promiseStatus"] as? String
        {
            cell.lblPromStatus.text = "\(promiseStatus)"
        }
        
        if let status = dict["status"] as? String
        {
            cell.lblStatus.text = "\(status)"
        }
        
        if let totalBalance = dict["totalBalance"] as? NSNumber
        {
            cell.lblTotalBal.text = "\(totalBalance)"
        }
        
        
        if let account_no = dict["account_no"] as? String
        {
            cell.lblAccNo.text = "\(account_no)"
        }
        
        
        if let stock_no = dict["stock_no"] as? String{
            cell.lblStockNo.text = "\(stock_no)"
        }
        
        if let colleteral = dict["colleteral"] as? String{
            cell.lblCollaterl.text = "\(colleteral)"
        }
        
        if let currentDue = dict["currentDue"] as? NSNumber
        {
            cell.lblCurrdue.text = "\(currentDue)"
        }
        
        if let daysPO = dict["daysPO"] as? NSNumber
        {
            cell.lblDaysPo.text = "\(daysPO)"
        }
        
        
        
        
        return cell
    }
}

class tblBorrAccounts : UITableViewCell
{
    @IBOutlet weak var lblAccNo : UILabel!
    @IBOutlet weak var lblStockNo : UILabel!
    @IBOutlet weak var lblCollaterl : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblTotalBal : UILabel!
    @IBOutlet weak var lblCurrdue : UILabel!
    @IBOutlet weak var lblDaysPo : UILabel!
    @IBOutlet weak var lblPromDate : UILabel!
    @IBOutlet weak var lnlPromAmt : UILabel!
    @IBOutlet weak var lblPromStatus : UILabel!
    
}
