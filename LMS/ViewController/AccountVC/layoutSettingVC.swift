//
//  layoutSettingVC.swift
//  LMS
//
//  Created by Apple on 28/01/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class layoutSettingVC: UIViewController {
    
    
    @IBOutlet weak var txtQueuLayout: UITextField!
    @IBOutlet weak var txtSortcolumn_1: UITextField!
    @IBOutlet weak var txtSortDir_1: UITextField!
    @IBOutlet weak var txtSortcolumn_2: UITextField!
    @IBOutlet weak var txtSortDir_2: UITextField!
    @IBOutlet weak var txtSortcolumn_3: UITextField!
    @IBOutlet weak var txtSortDir_3: UITextField!
    
    var isOpenVC = ""
    
    @IBOutlet weak var btnDefault: UIButton!
    
    var dictLayout : NSDictionary = [:]
    
    var pickerQueue : UIPickerView!
    var arrQueueLayout = NSMutableArray()
    
    var pickerColumn_1 : UIPickerView!
    var pickerColumn_2 : UIPickerView!
    var pickerColumn_3 : UIPickerView!
    var arrColumn = NSMutableArray()
    
    var pickerSort_1 : UIPickerView!
    var pickerSort_2 : UIPickerView!
    var pickerSort_3 : UIPickerView!
    var arrDirection = NSMutableArray()
    
    var queueId = 0
    
    var direc_1 = ""
    var direc_2 = ""
    var direc_3 = ""
    
    var col_1 = ""
    var col_2 = ""
    var col_3 = ""
    
    var makeDefault = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let dict1 = ["name":"ASC","value":"asc"]
        let dict2 = ["name":"DESC","value":"desc"]
        
        self.arrDirection.add(dict1)
        self.arrDirection.add(dict2)
        
        self.callGetQueueLayoutApi()
        
        
    }
    
    @IBAction func clickONCheckBoxes(_ sender: UIButton)
    {
        if sender.isSelected == true {
            sender.isSelected = false
            self.makeDefault = false
        }
        else {
            sender.isSelected = true
            self.makeDefault = false
        }
    }
    
}


extension layoutSettingVC {
    
    @IBAction func clickONDismiss(_ sender : UIButton)
    {
        self.dismiss(animated: true){
        }
    }
}

extension layoutSettingVC {
    
    func callGetLayoutDataApi()
    {
        var url = ""
        if(self.isOpenVC == "inventory"){
            url = "\(Constant.get_layoutId_url)?layout=&dataSourceId=11"
        }else if(self.isOpenVC == "account"){
            url = Constant.get_layoutId_url
        }
        
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? NSDictionary {
                            self.dictLayout = data
                            print(self.dictLayout)
                            
                            if let make_default = self.dictLayout["make_default"] as? Bool {
                                self.makeDefault = make_default
                                if(make_default == true){
                                    self.btnDefault.isSelected = true
                                }
                                else{
                                    self.btnDefault.isSelected = false
                                }
                            }
                            
                            
                            if let order1 = self.dictLayout["order1"] as? String {
                                for i in 0..<self.arrDirection.count{
                                    let dict = self.arrDirection[i] as! NSDictionary
                                    let value = dict["value"] as! String
                                    if(value == order1){
                                        let strTitle = dict["name"] as! String
                                        self.txtSortDir_1.text = strTitle
                                        self.direc_1 = value
                                        break
                                    }
                                }
                            }
                            if let order2 = self.dictLayout["order2"] as? String {
                                for i in 0..<self.arrDirection.count{
                                    let dict = self.arrDirection[i] as! NSDictionary
                                    let value = dict["value"] as! String
                                    if(value == order2){
                                        let strTitle = dict["name"] as! String
                                        self.txtSortDir_2.text = strTitle
                                        self.direc_2 = value
                                        break
                                    }
                                }
                            }
                            if let order3 = self.dictLayout["order3"] as? String {
                                for i in 0..<self.arrDirection.count{
                                    let dict = self.arrDirection[i] as! NSDictionary
                                    let value = dict["value"] as! String
                                    if(value == order3){
                                        let strTitle = dict["name"] as! String
                                        self.txtSortDir_3.text = strTitle
                                        self.direc_3 = value
                                        break
                                    }
                                }
                            }
                            
                            if let layout_id = self.dictLayout["layout_id"] as? Int {
                                self.queueId = layout_id
                                
                                for i in 0..<self.arrQueueLayout.count{
                                    let dict = self.arrQueueLayout[i] as! NSDictionary
                                    let _id = dict["_id"] as! Int
                                    if(layout_id == _id){
                                        let strTitle = dict["layoutName"] as! String
                                        self.txtQueuLayout.text = strTitle
                                        break
                                    }
                                }
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                                    self.callColumnListApi(queue_id: layout_id)
                                })
                            }
                            
                            
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func callGetQueueLayoutApi()
    {
        var url = ""
        if(self.isOpenVC == "inventory"){
            
            url = Constant.inevtry_layoutS
            
        }else if(self.isOpenVC == "account"){
            
            url = Constant.queue_layouts_url
        }
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? [NSDictionary] {
                            for i in 0..<data.count{
                                let dict = data[i]
                                self.arrQueueLayout.add(dict)
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                            self.callGetLayoutDataApi()
                        })
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    func callColumnListApi(queue_id : Int){
        
        let params = ["queueLayoutsId": queue_id] as [String : Any]
        Globalfunc.print(object: params)
        self.sendDatatoPostApi(param: params, StrCheck: "post", strUrl: Constant.layoutcolumn_url)
    }
    
    
    func sendDatatoPostApi( param: [String : Any], StrCheck: String,strUrl : String)
    {
        if(StrCheck == "post"){
            BaseApi.onResponsePostWithToken(url: strUrl, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            if let data = arr["data"] as? [NSDictionary] {
                                for i in 0..<data.count{
                                    let dict = data[i]
                                    self.arrColumn.add(dict)
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        else if(StrCheck == "put"){
            BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
    }
}


/*MARk - Textfeld and picker delegates*/

extension layoutSettingVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerSort_1{
            return self.arrDirection.count
        }
        else if(pickerView == pickerSort_2){
            return self.arrDirection.count
        }
        else if(pickerView == pickerSort_3){
            return self.arrDirection.count
        }
        else if(pickerView == pickerColumn_1){
            return self.arrColumn.count
        }
        else if(pickerView == pickerColumn_2){
            return self.arrColumn.count
        }
        else if(pickerView == pickerColumn_3){
            return self.arrColumn.count
        }
        else if(pickerView == pickerQueue){
            return self.arrQueueLayout.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerSort_1{
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            return strTitle
        }
        else if(pickerView == pickerSort_2){
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            return strTitle
        }
        else if(pickerView == pickerSort_3){
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            return strTitle
        }
        else if(pickerView == pickerColumn_1){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            return strTitle
            
        }
        else if(pickerView == pickerColumn_2){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            return strTitle
            
        }
        else if(pickerView == pickerColumn_3){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            return strTitle
        }
        else if(pickerView == pickerQueue){
            let dict = arrQueueLayout[row] as! NSDictionary
            let strTitle = dict["layoutName"] as! String
            return strTitle
            
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerSort_1{
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            self.txtSortDir_1.text = strTitle
            self.direc_1 = dict["value"] as! String
        }
        else if(pickerView == pickerSort_2){
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            self.txtSortDir_2.text = strTitle
            self.direc_2 = dict["value"] as! String
        }
        else if(pickerView == pickerSort_3){
            let dict = arrDirection[row] as! NSDictionary
            let strTitle = dict["name"] as! String
            self.txtSortDir_3.text = strTitle
            self.direc_3 = dict["value"] as! String
        }
        else if(pickerView == pickerColumn_1){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            self.txtSortcolumn_1.text = strTitle
            self.col_1 = dict["name"] as! String
        }
        else if(pickerView == pickerColumn_2){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            self.txtSortcolumn_2.text = strTitle
            self.col_2 = dict["name"] as! String
        }
        else if(pickerView == pickerColumn_3){
            let dict = arrColumn[row] as! NSDictionary
            let strTitle = dict["colname"] as! String
            self.txtSortcolumn_3.text = strTitle
            self.col_3 = dict["name"] as! String
            
        }
        else if(pickerView == pickerQueue){
            let dict = arrQueueLayout[row] as! NSDictionary
            let strTitle = dict["layoutName"] as! String
            self.txtQueuLayout.text = strTitle
            self.queueId = dict["_id"] as! Int
            
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtQueuLayout){
            self.pickUp(txtQueuLayout)
        }
        else if(textField == self.txtSortDir_1){
            self.pickUp(txtSortDir_1)
        }
        else if(textField == self.txtSortDir_2){
            self.pickUp(txtSortDir_2)
        }
        else if(textField == self.txtSortDir_3){
            self.pickUp(txtSortDir_3)
        }
        else if(textField == self.txtSortcolumn_1){
            self.pickUp(txtSortcolumn_1)
        }
        else if(textField == self.txtSortcolumn_2){
            self.pickUp(txtSortcolumn_2)
        }
        else if(textField == self.txtSortcolumn_3){
            self.pickUp(txtSortcolumn_3)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtQueuLayout){
            self.pickerQueue = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerQueue.delegate = self
            self.pickerQueue.dataSource = self
            self.pickerQueue.backgroundColor = UIColor.white
            textField.inputView = self.pickerQueue
        }
        
        else if(textField == self.txtSortDir_1){
            self.pickerSort_1 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSort_1.delegate = self
            self.pickerSort_1.dataSource = self
            self.pickerSort_1.backgroundColor = UIColor.white
            textField.inputView = self.pickerSort_1
        }
        else if(textField == self.txtSortDir_2){
            self.pickerSort_2 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSort_2.delegate = self
            self.pickerSort_2.dataSource = self
            self.pickerSort_2.backgroundColor = UIColor.white
            textField.inputView = self.pickerSort_2
        }
        
        else if(textField == self.txtSortDir_3){
            self.pickerSort_3 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSort_3.delegate = self
            self.pickerSort_3.dataSource = self
            self.pickerSort_3.backgroundColor = UIColor.white
            textField.inputView = self.pickerSort_3
        }
        
        
        else if(textField == self.txtSortcolumn_1){
            self.pickerColumn_1 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerColumn_1.delegate = self
            self.pickerColumn_1.dataSource = self
            self.pickerColumn_1.backgroundColor = UIColor.white
            textField.inputView = self.pickerColumn_1
        }
        else if(textField == self.txtSortcolumn_2){
            self.pickerColumn_2 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerColumn_2.delegate = self
            self.pickerColumn_2.dataSource = self
            self.pickerColumn_2.backgroundColor = UIColor.white
            textField.inputView = self.pickerColumn_2
        }
        
        else if(textField == self.txtSortcolumn_3){
            self.pickerColumn_3 = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerColumn_3.delegate = self
            self.pickerColumn_3.dataSource = self
            self.pickerColumn_3.backgroundColor = UIColor.white
            textField.inputView = self.pickerColumn_3
        }
        
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtQueuLayout.resignFirstResponder()
        txtSortcolumn_1.resignFirstResponder()
        txtSortcolumn_2.resignFirstResponder()
        txtSortcolumn_3.resignFirstResponder()
        txtSortDir_1.resignFirstResponder()
        self.txtSortDir_2.resignFirstResponder()
        self.txtSortDir_3.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtQueuLayout.resignFirstResponder()
        txtSortcolumn_1.resignFirstResponder()
        txtSortcolumn_2.resignFirstResponder()
        txtSortcolumn_3.resignFirstResponder()
        txtSortDir_1.resignFirstResponder()
        self.txtSortDir_2.resignFirstResponder()
        self.txtSortDir_3.resignFirstResponder()
    }
}



extension layoutSettingVC {
    
    @IBAction func clickOnCancel(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickONSAveBtn(_ sender: UIButton)
    {
        var Primd = 0
        if(self.isOpenVC == "inventory"){
            
            Primd = 11
            
            
        }else if(self.isOpenVC == "account"){
            
            Primd = 1
        }

        
        if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            let _id = self.dictLayout["_id"] as! Int
            let params = ["make_default": self.makeDefault,
                          "insid": (user?.institute_id!)!,
                          "layout_id": self.queueId,
                          "order1": self.direc_1,
                          "order2": self.direc_2,
                          "order3": self.direc_3,
                          "primaryDataSource": Primd,
                          "sort1": self.col_1,
                          "sort2": self.col_2,
                          "sort3": self.col_3,
                          "id": _id,
                          "userid": (user?._id)!] as [String : Any]
            Globalfunc.print(object:params)
            
            self.sendDatatoPostApi(param: params, StrCheck: "put", strUrl: Constant.layout_setting_url)
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
}

