//
//  accountPageViewController.swift
//  LMS
//
//  Created by Apple on 03/03/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

var isUpload = ""


class accountDetailPageController: UIViewController {
    
    
    @IBOutlet weak var lblAccountNo : UILabel!
    @IBOutlet weak var viewMain : UIView!
    
    //view borrower outlets
    @IBOutlet weak var viewBorower : UIView!
    @IBOutlet weak var lblBorowerNAme : UILabel!
    @IBOutlet weak var lblCustCredit : UILabel!
    @IBOutlet weak var lblHomePhn : UILabel!
    @IBOutlet weak var lblWorkPhn : UILabel!
    @IBOutlet weak var lblCellPhn : UILabel!
    @IBOutlet weak var lblHomePhn_2 : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblColorBorrwr : UILabel!
    @IBOutlet weak var btnBorrwr_More : UIButton!
    @IBOutlet weak var viewBorrowerHgt : NSLayoutConstraint!
    
    
    //view address outlets
    @IBOutlet weak var viewAddress : UIView!
    @IBOutlet weak var lblAddress_1 : UILabel!
    @IBOutlet weak var lblAddress_2 : UILabel!
    @IBOutlet weak var lblCitystate : UILabel!
    @IBOutlet weak var lblCountry : UILabel!
    @IBOutlet weak var lblColorAddrss : UILabel!
    
    
    //view borrower outlets
    @IBOutlet weak var viewPastDue : UIView!
    @IBOutlet weak var lblActualPstDue : UILabel!
    @IBOutlet weak var lblActualPymtPD : UILabel!
    @IBOutlet weak var lblFilteredPstDue : UILabel!
    @IBOutlet weak var lblFilteredPymtPD : UILabel!
    @IBOutlet weak var lblLAstPaidDate : UILabel!
    @IBOutlet weak var lblLAstPaidAmt : UILabel!
    @IBOutlet weak var lblTier : UILabel!
    @IBOutlet weak var lblMgmtOfficer : UILabel!
    @IBOutlet weak var lblMgmtQueue : UILabel!
    @IBOutlet weak var btnPast_More : UIButton!
    @IBOutlet weak var viewPAstHgt : NSLayoutConstraint!
    
    
    //view insurance outlets
    @IBOutlet weak var viewInsurance : UIView!
    @IBOutlet weak var lblInsStatus : UILabel!
    @IBOutlet weak var lblInsCompany : UILabel!
    @IBOutlet weak var lblInspolicy : UILabel!
    @IBOutlet weak var lblInsExpire : UILabel!
    @IBOutlet weak var lblInsColl_Ded : UILabel!
    @IBOutlet weak var lblInsComp_ded : UILabel!
    @IBOutlet weak var lblInsAgent : UILabel!
    @IBOutlet weak var lblInsAgent_phn : UILabel!
    @IBOutlet weak var lblInsAgent_fax : UILabel!
    @IBOutlet weak var lblInsCompany_phn : UILabel!
    @IBOutlet weak var lblInsCompany_fax : UILabel!
    
    @IBOutlet weak var lblIns_gap : UILabel!
    @IBOutlet weak var lblInsCompany_gap : UILabel!
    @IBOutlet weak var lblInsPolicy_gap : UILabel!
    @IBOutlet weak var lblInsExpire_gap : UILabel!
    
    @IBOutlet weak var btnIns_More : UIButton!
    @IBOutlet weak var viewInstHgt : NSLayoutConstraint!
    
    //view act status outlets
    @IBOutlet weak var viewAccount : UIView!
    @IBOutlet weak var lblAcctStatus : UILabel!
    @IBOutlet weak var lblAcctType : UILabel!
    @IBOutlet weak var lblAcct_hash : UILabel!
    @IBOutlet weak var lblAcctLoan_hash : UILabel!
    @IBOutlet weak var lblAcctFlags : UILabel!
    @IBOutlet weak var lblAcctLender : UILabel!
    @IBOutlet weak var lblAcctPrtfolio : UILabel!
    @IBOutlet weak var lblAcctServiceBranch : UILabel!
    @IBOutlet weak var lblAcctBookKeeping : UILabel!
    @IBOutlet weak var btnStatus_More : UIButton!
    @IBOutlet weak var viewAcctHgt : NSLayoutConstraint!
    @IBOutlet weak var imgAcctColor : UIImageView!
    @IBOutlet weak var btnAcctColor : UIButton!
    @IBOutlet weak var lblAcctColor : UILabel!
    
    //view act status outlets
    @IBOutlet weak var viewPrimaryLoan : UIView!
    @IBOutlet weak var lblPLPrin : UILabel!
    @IBOutlet weak var lblPLSideNotePrin : UILabel!
    @IBOutlet weak var lblPLCpiLoan : UILabel!
    @IBOutlet weak var lblPL_loan_hash : UILabel!
    @IBOutlet weak var lblPLBal_street : UILabel!
    @IBOutlet weak var lblPL_Autopay : UILabel!
    @IBOutlet weak var lblPL_PrinciplIntrst : UILabel!
    @IBOutlet weak var lblPL_NextDue : UILabel!
    @IBOutlet weak var lblPLSideNoteIntrst : UILabel!
    @IBOutlet weak var lblPL_downPymtDue : UILabel!
    @IBOutlet weak var lblPL_salesTaxtDue : UILabel!
    @IBOutlet weak var lblPLMiscFees : UILabel!
    @IBOutlet weak var lblPLLateFees : UILabel!
    @IBOutlet weak var btnPL_More : UIButton!
    @IBOutlet weak var viewPLHgt : NSLayoutConstraint!
    
    @IBOutlet weak var viewLast : UIView!
    @IBOutlet weak var lblPromiseToPay : UILabel!
    @IBOutlet weak var lblLast : UILabel!
    @IBOutlet weak var imgPromColor : UIImageView!
    @IBOutlet weak var btnPromColor : UIButton!
    @IBOutlet weak var lblPromColor : UILabel!
    
    @IBOutlet weak var viewDescription : UIView!
    @IBOutlet weak var lblDEscription : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblVin : UILabel!
    @IBOutlet weak var lblLAstLocated : UILabel!
    @IBOutlet weak var imgDescrptn : UIImageView!
    
    @IBOutlet weak var viewDollarList : UIView!
    @IBOutlet weak var tblList : UITableView!
    
    @IBOutlet weak var viewMoeList : UIView!
    @IBOutlet weak var tblMoreList : UITableView!
    
    @IBOutlet weak var viewDocList : UIView!
    @IBOutlet weak var tblDocList : UITableView!
    
    
    
    @IBOutlet weak var viewAlertMsg : UIView!
    @IBOutlet weak var lblMsg : UILabel!
    @IBOutlet weak var hgtAlertview : NSLayoutConstraint!
    
    
    var isDetailOpenfrom = ""
    
    var arrMoreList = ["Add File","Add Borrower File","Add Collateral Image","Add Collateral File","Add/Edit Flags","Add References"]
    var arrList = [String]()
    var arrDocList = ["Documents","Reports"]
    var account_status = ""
    var accId : Int!
    var passDict_LayoutId : NSDictionary = [:]
    var accountDetailsData : NSDictionary = [:]
    let dict_details : NSMutableDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblList.tableFooterView = UIView()
        self.tblMoreList.tableFooterView = UIView()
        self.tblDocList.tableFooterView = UIView()
        
        Globalfunc.showLoaderView(view: self.view)
        self.viewMain.isHidden = true
        self.viewDollarList.alpha = 0
        self.viewMoeList.alpha = 0
        self.viewDocList.alpha = 0
        
        self.getAccountDetails()
    }
    
    func setDollarSectionList(){
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let responseDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                OperationQueue.main.addOperation {
                    self.account_status = responseDict["account_status"] as! String
                    if(self.account_status == "Charge Off"){
                        self.arrList = ["Create a Side Note","Reverse a Transaction","Post Return Item(NSF)","Post A Recovery","Post A Recovery Reversal","Post A Charge Off Adjustment","Post A Charge Off Reversal","Post Account Amendment"]
                    }
                    else if(self.account_status == "Active"){
                        self.arrList = ["Post a Payment","Post a Payoff","Post an Extension","Post a Charge Off","Post Account Write Down","Create a Side Note","Skip Auto Pay","Reverse a Transaction","Post Return Item(NSF)","Reverse an Extension","Reverse Skip AutoPay","Post Principal Adjustment","Post Non Earn Prin Adjustment","Post Sales Tax Advance","Post Interest Adjustment","Post Misc Fee Adjustment","Post Account Amendment"]
                    }
                    else if(self.account_status == "Closed"){
                        self.arrList = ["Create a Side Note","Reverse a Transaction","Post Return Item(NSF)","Post Misc Fee Adjustment","Post Account Transfer","Post Account Amendment"]
                    }
                    else if(self.account_status == "Out For repo"){
                        self.arrList = ["Post a Charge Off","Post Account Write Down","Create a Side Note","Reverse a Transaction","Post Return Item(NSF)","Post Principal Adjustment","Post Non Earn Prin Adjustment","Post Sales Tax Advance","Post Interest Adjustment","Post Misc Fee Adjustment","Post Account Amendment"]
                    }
                    else if(self.account_status == "Repossessed"){
                        self.arrList = ["Post a Payment","Post a Payoff","Post a Charge Off","Post Account Write Down","Create a Side Note","Reverse a Transaction","Post Return Item(NSF)","Post Principal Adjustment","Post Non Earn Prin Adjustment","Post Sales Tax Advance","Post Interest Adjustment","Post Misc Fee Adjustment","Post Account Amendment"]
                    }
                    else if(self.account_status == "Skip"){
                        self.arrList = ["Post Account Amendment"]
                    }
                    self.tblList.reloadData()
                }
            }
        }
        catch{
        }
    }
    
    func setUpUI(){
        self.viewMain.isHidden = false
        
        let loanStatus = self.accountDetailsData["loanStatus"] as! NSDictionary
        let acttNo = loanStatus["account_no"] as! String
        self.lblAccountNo.text = "Account No: \(acttNo)"
        
        self.setBrrowerData()
        self.setActualPasData()
        self.setInsuranceData()
        self.setAccountStatus()
        self.setPrimaryLoanStatus()
        self.setLastView()
        self.setDollarSectionList()
    }
    
    @IBAction func clickOnDollarBtn(_ sender : UIButton){
        if(sender.tag == 10){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDollarList.alpha = 1
                self.viewMoeList.alpha = 0
                self.viewDocList.alpha = 0
            }) { _ in
            }
        }
        else if(sender.tag == 20 || sender.tag == 30){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDollarList.alpha = 0
                self.viewMoeList.alpha = 0
                self.viewDocList.alpha = 0
            }) { _ in
            }
        }
        if(sender.tag == 40){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDollarList.alpha = 0
                self.viewMoeList.alpha = 1
                self.viewDocList.alpha = 0
            }) { _ in
            }
        }
        if(sender.tag == 50){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDollarList.alpha = 0
                self.viewMoeList.alpha = 0
                self.viewDocList.alpha = 1
            }) { _ in
            }
        }
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnMoreDetailsBorrowerBtn(_ sender: UIButton){
        if sender.tag == 1
        {
            sender.tag = 0
            self.viewBorrowerHgt.constant = 250
            
            //                if(self.lblHomePhn_2.text == "Home Phone 2: "){
            //                    self.lblHomePhn_2.isHidden = true
            //                }
            //                else{
            self.lblHomePhn_2.isHidden = false
            //                }
            //
            //                if(self.lblWorkPhn.text == "Work Phone: "){
            //                    self.lblWorkPhn.isHidden = true
            //                }
            //                else{
            self.lblWorkPhn.isHidden = false
            //                }
            //
            //                if(self.lblCellPhn.text == "Cell Phone: "){
            //                    self.lblCellPhn.isHidden = true
            //                }
            //                else{
            self.lblCellPhn.isHidden = false
            //                }
            //
            //                if(self.lblEmail.text == "Email: "){
            //                    self.lblEmail.isHidden = true
            //                }
            //                else{
            self.lblEmail.isHidden = false
            //                }
            
            
            self.btnBorrwr_More.setTitle("Less Detail", for: .normal)
        }
        else{
            sender.tag = 1
            self.viewBorrowerHgt.constant = 150
            self.lblEmail.isHidden = true
            self.lblCellPhn.isHidden = true
            self.lblWorkPhn.isHidden = true
            self.lblHomePhn_2.isHidden = true
            self.btnBorrwr_More.setTitle("More Detail", for: .normal)
        }
    }
    
    @IBAction func clickOnMoreDetailsPastBtn(_ sender: UIButton){
        if sender.tag == 10
        {
            sender.tag = 9
            self.viewPAstHgt.constant = 300
            self.lblLAstPaidAmt.isHidden = false
            self.lblTier.isHidden = false
            self.lblMgmtOfficer.isHidden = false
            self.lblMgmtQueue.isHidden = false
            self.btnPast_More.setTitle("Less Detail", for: .normal)
        }
        else{
            sender.tag = 10
            self.viewPAstHgt.constant = 200
            self.lblLAstPaidAmt.isHidden = true
            self.lblTier.isHidden = true
            self.lblMgmtOfficer.isHidden = true
            self.lblMgmtQueue.isHidden = true
            self.btnPast_More.setTitle("More Detail", for: .normal)
        }
    }
    
    @IBAction func clickOnMoreDetailsInsBtn(_ sender: UIButton)
    {
        if sender.tag == 20
        {
            sender.tag = 19
            self.viewInstHgt.constant = 460
            self.lblInsComp_ded.isHidden = false
            self.lblInsAgent.isHidden = false
            self.lblInsAgent_phn.isHidden = false
            self.lblInsAgent_fax.isHidden = false
            self.lblInsCompany_phn.isHidden = false
            self.lblInsCompany_fax.isHidden = false
            self.lblIns_gap.isHidden = false
            self.lblInsCompany_gap.isHidden = false
            self.lblInsPolicy_gap.isHidden = false
            self.lblInsExpire_gap.isHidden = false
            
            self.btnIns_More.setTitle("Less Detail", for: .normal)
            
        }
        else{
            sender.tag = 20
            self.viewInstHgt.constant = 200
            self.lblInsComp_ded.isHidden = true
            self.lblInsAgent.isHidden = true
            self.lblInsAgent_phn.isHidden = true
            self.lblInsAgent_fax.isHidden = true
            self.lblInsCompany_phn.isHidden = true
            self.lblInsCompany_fax.isHidden = true
            self.lblIns_gap.isHidden = true
            self.lblInsCompany_gap.isHidden = true
            self.lblInsPolicy_gap.isHidden = true
            self.lblInsExpire_gap.isHidden = true
            
            
            self.btnIns_More.setTitle("More Detail", for: .normal)
        }
    }
    
    @IBAction func clickOnMoreDetailsAcctStatusBtn(_ sender: UIButton)
    {
        if sender.tag == 30
        {
            sender.tag = 29
            self.viewAcctHgt.constant = 300
            self.lblAcctLender.isHidden = false
            self.lblAcctPrtfolio.isHidden = false
            self.lblAcctBookKeeping.isHidden = false
            self.lblAcctServiceBranch.isHidden = false
            self.btnStatus_More.setTitle("Less Detail", for: .normal)
        }
        else{
            sender.tag = 30
            self.viewAcctHgt.constant = 200
            self.lblAcctLender.isHidden = true
            self.lblAcctPrtfolio.isHidden = true
            self.lblAcctBookKeeping.isHidden = true
            self.lblAcctServiceBranch.isHidden = true
            self.btnStatus_More.setTitle("More Detail", for: .normal)
        }
    }
    
    
    @IBAction func clickOnMoreDetailsPLBtn(_ sender: UIButton)
    {
        if sender.tag == 40
        {
            sender.tag = 39
            self.viewPLHgt.constant = 420
            self.lblPL_PrinciplIntrst.isHidden = false
            self.lblPL_NextDue.isHidden = false
            self.lblPLSideNoteIntrst.isHidden = false
            self.lblPL_downPymtDue.isHidden = false
            self.lblPL_salesTaxtDue.isHidden = false
            self.lblPLMiscFees.isHidden = false
            self.lblPLLateFees.isHidden = false
            self.btnPL_More.setTitle("Less Detail", for: .normal)
        }
        else{
            sender.tag = 40
            self.viewPLHgt.constant = 220
            self.lblPL_PrinciplIntrst.isHidden = true
            self.lblPL_NextDue.isHidden = true
            self.lblPLSideNoteIntrst.isHidden = true
            self.lblPL_downPymtDue.isHidden = true
            self.lblPL_salesTaxtDue.isHidden = true
            self.lblPLMiscFees.isHidden = true
            self.lblPLLateFees.isHidden = true
            self.btnPL_More.setTitle("More Detail", for: .normal)
        }
    }
}


//set data on view
extension accountDetailPageController {
    
    func setBrrowerData(){
        
        
        let borrower_dict = self.accountDetailsData["loanBorrower"] as! NSDictionary
        let first_name = borrower_dict["first_name"] as! String
        let last_name = borrower_dict["last_name"] as! String
        self.lblBorowerNAme.text = "Borrower: \(first_name) \(last_name)"
        
        
        if let homephone = borrower_dict["home_phone"] as? String{
            self.lblHomePhn.text = "Home Phone: \(homephone)"
        }
        
        if let homephone2 = borrower_dict["home_phone2"] as? String{
            self.lblHomePhn_2.text = "Home Phone 2: \(homephone2)"
        }
        
        if let workPhone = borrower_dict["work_phone"] as? String{
            
            self.lblWorkPhn.text = "Work Phone: \(workPhone)"
        }
        
        if let cellPhone = borrower_dict["cell_phone"] as? String{
            self.lblCellPhn.text = "Cell Phone: \(cellPhone)"
        }
        
        if let email = borrower_dict["email"] as? String{
            self.lblEmail.text = "Email: \(email)"
        }
        
        self.viewBorrowerHgt.constant = 150
        self.lblEmail.isHidden = true
        self.lblCellPhn.isHidden = true
        self.lblWorkPhn.isHidden = true
        self.lblHomePhn_2.isHidden = true
        
        
        let loanAddress = self.accountDetailsData["loanAddress"] as! NSDictionary
        
        if let address1 = loanAddress["address1"] as? String{
            self.lblAddress_1.text = "Address 1: \(address1)"
        }
        
        if let address2 = loanAddress["address2"] as? String{
            self.lblAddress_2.text = "Address 2: \(address2)"
        }
        
        var description = ""
        if let city = loanAddress["city"] as? String{
            description = city
        }
        if let state = loanAddress["state"] as? String{
            description += state
        }
        if let zipCode = loanAddress["zipCode"] as? String{
            description += zipCode
        }
        
        self.lblCitystate.text = "City,State,Zip: \(description)"
        
        if let country = loanAddress["country"] as? String{
            self.lblCountry.text = "Country: \(country)"
        }
    }
    
    
    func setActualPasData(){
        self.viewPAstHgt.constant = 200
        self.lblLAstPaidAmt.isHidden = true
        self.lblTier.isHidden = true
        self.lblMgmtOfficer.isHidden = true
        self.lblMgmtQueue.isHidden = true
        
        let loanPaymentHistory = self.accountDetailsData["loanPaymentHistory"] as! NSDictionary
        
        if let tier = loanPaymentHistory["tier"] as? String{
            self.lblTier.text = "Tier: \(tier)"
        }
        
        var actual_past_due_days = ""
        var formatDate = ""
        var actual_past_due_amount = 0
        
        if let actual_pt_due_days = loanPaymentHistory["actual_past_due_days"] as? String{
            actual_past_due_days = actual_pt_due_days
        }
        
        if let actual_pt_due_amount = loanPaymentHistory["actual_past_due_amount"] as? Int{
            actual_past_due_amount = actual_pt_due_amount
        }
        
        if let actual_past_due_date = loanPaymentHistory["actual_past_due_date"] as? String{
            
            if(actual_past_due_date == ""){
            }else{
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: actual_past_due_date)
                formatDate = formatter.string(from: date!)
            }
            
            
        }
        self.lblActualPstDue.text = "Actual Past Due: $\(actual_past_due_amount) \(formatDate) Days-\(actual_past_due_days)"
        
        let actual_payment_pd = loanPaymentHistory["actual_payment_pd"] as? Int
        self.lblActualPymtPD.text = "Actual Pmnts PD: \(actual_payment_pd!)"
        
        self.lblFilteredPymtPD.text = "Filtered Pmnts PD: \(actual_payment_pd!)"
        
        
        if let last_paid_date = loanPaymentHistory["last_paid_date"] as? String{
            self.lblLAstPaidDate.text = "Last Paid Date: \(last_paid_date)"
        }
        
        if let last_paid_amount = loanPaymentHistory["last_paid_amount"] as? Int{
            self.lblLAstPaidAmt.text = "Last Paid Amount: \(last_paid_amount)"
        }
    }
    
    func setInsuranceData(){
        
        self.viewInstHgt.constant = 200
        self.lblInsComp_ded.isHidden = true
        self.lblInsAgent.isHidden = true
        self.lblInsAgent_phn.isHidden = true
        self.lblInsAgent_fax.isHidden = true
        self.lblInsCompany_phn.isHidden = true
        self.lblInsCompany_fax.isHidden = true
        self.lblIns_gap.isHidden = true
        self.lblInsCompany_gap.isHidden = true
        self.lblInsPolicy_gap.isHidden = true
        self.lblInsExpire_gap.isHidden = true
        
        
        
        
        
        if let insurance = self.accountDetailsData["loanInsurance"] as? NSDictionary{
            
            
            if let status = insurance["status"] as? String{
                self.lblInsStatus.text = "Ins Status: \(status)"
            }
            
            if let insurance_company = insurance["insurance_company"] as? String{
                self.lblInsCompany.text = "Ins Company: \(insurance_company)"
            }
            
            if let policy_number = insurance["policy_number"] as? Int{
                self.lblInspolicy.text = "Policy #: \(policy_number)"
            }
            
            
            if let expire_date = insurance["expire_date"] as? String{
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                let date = Date.dateFromISOString(string: expire_date)
                self.lblInsExpire.text = "Ins Expire: \(formatter.string(from: date!))"
            }
            
            if let collision_deduct = insurance["collision_deduct"] as? Int{
                self.lblInsColl_Ded.text = "Coll Deductible: $\(collision_deduct)"
            }
            
            if let comprehensive_deduct = insurance["comprehensive_deduct"] as? Int{
                self.lblInsComp_ded.text = "Comp Deductible: \(comprehensive_deduct)"
            }
            
            if let insurance_agent = insurance["insurance_agent"] as? String{
                self.lblInsAgent.text = "Agent: \(insurance_agent)"
            }
            
            if let agentPhone = insurance["agentPhone"] as? String{
                self.lblInsAgent_phn.text = "Agent Phone: \(agentPhone)"
            }
            
            if let agentFax = insurance["agentFax"] as? String{
                self.lblInsAgent_fax.text = "Agent Fax: \(agentFax)"
            }
            
            if let companyPhone = insurance["companyPhone"] as? String{
                self.lblInsCompany_phn.text = "Company Phone: \(companyPhone)"
            }
            
            if let companyFax = insurance["companyFax"] as? String{
                self.lblInsCompany_fax.text = "Company Fax: \(companyFax)"
            }
            
            if let gap_insurance = insurance["gap_insurance"] as? NSDictionary{
                if let insurance_company = gap_insurance["insurance_company"] as? String{
                    self.lblInsCompany_gap.text = "Ins Company: \(insurance_company)"
                }
                
                if let policy_number = gap_insurance["policy_number"] as? String{
                    self.lblInsPolicy_gap.text = "Policy #: \(policy_number)"
                }
                
                if let expire_date = insurance["expire_date"] as? String{
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM-dd-yyyy"
                    let date = Date.dateFromISOString(string: expire_date)
                    self.lblInsExpire_gap.text = "Ins Expire: \(formatter.string(from: date!))"
                }
            }
            //            else{
            //                self.lblInsCompany_gap.text = ""
            //                self.lblInsPolicy_gap.text = ""
            //                self.lblInsExpire_gap.text = ""
            //                self.lblIns_gap.text = ""
            //            }
        }
    }
    
    func setAccountStatus()
    {
        
        self.viewAcctHgt.constant = 200
        self.lblAcctLender.isHidden = true
        self.lblAcctPrtfolio.isHidden = true
        self.lblAcctServiceBranch.isHidden = true
        self.lblAcctBookKeeping.isHidden = true
        
        let loanStatus = self.accountDetailsData["loanStatus"] as! NSDictionary
        
        if let account_no = loanStatus["account_no"] as? String{
            self.lblAcct_hash.text = "Acct #: \(account_no)"
        }
        
        if let account_status = loanStatus["account_status"] as? String{
            self.lblAcctStatus.text = "Acct Status: \(account_status)"
            
            if(account_status == "Charge Off"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha: 1.0)
                self.btnAcctColor.tintColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha: 1.0)
                self.imgAcctColor.layer.borderColor = UIColorFromHex(rgbValue: 0xCF5C60, alpha: 1.0).cgColor
                
            }
            else if(account_status == "Active"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                self.btnAcctColor.tintColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                self.imgAcctColor.layer.borderColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0).cgColor
                
            }
            else if(account_status == "Closed" || account_status == "Deleted"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0x26648B, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0x26648B, alpha: 1.0)
                self.btnAcctColor.tintColor = UIColorFromHex(rgbValue: 0x26648B, alpha: 1.0)
                self.imgAcctColor.layer.borderColor = UIColorFromHex(rgbValue: 0x26648B, alpha: 1.0).cgColor
                
            }
            else if(account_status == "Out For repo"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0xD47300, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0xD47300, alpha: 1.0)
                self.btnAcctColor.tintColor = UIColorFromHex(rgbValue: 0xD47300, alpha: 1.0)
                self.imgAcctColor.layer.borderColor = UIColorFromHex(rgbValue: 0xD47300, alpha: 1.0).cgColor
                
            }
            else if(account_status == "Repossessed"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                self.btnAcctColor.tintColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                self.imgAcctColor.layer.borderColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0).cgColor
                
            }
            else if(account_status == "Skip"){
                self.viewAccount.layer.borderColor = UIColorFromHex(rgbValue: 0xC3DAE8, alpha: 1.0).cgColor
                self.lblAcctColor.backgroundColor = UIColorFromHex(rgbValue: 0xC3DAE8, alpha: 1.0)
                self.btnAcctColor.tintColor = .black
                self.imgAcctColor.layer.borderColor = (UIColor.black).cgColor
                
            }
            
        }
        
        if let account_type = loanStatus["account_type"] as? String{
            self.lblAcctType.text = "Acct Type: \(account_type)"
        }
        
        if let loan_no = loanStatus["loan_no"] as? String{
            self.lblAcctType.text = "Loan #: \(loan_no)"
        }
        
        
    }
    
    func setPrimaryLoanStatus()
    {
        
        Globalfunc.print(object:self.accountDetailsData)
        self.viewPLHgt.constant = 220
        self.lblPL_PrinciplIntrst.isHidden = true
        self.lblPL_NextDue.isHidden = true
        self.lblPLSideNoteIntrst.isHidden = true
        self.lblPL_downPymtDue.isHidden = true
        self.lblPL_salesTaxtDue.isHidden = true
        self.lblPLMiscFees.isHidden = true
        self.lblPLLateFees.isHidden = true
        
        if let loanBalance = self.accountDetailsData["loanBalance"] as? NSDictionary{
            
            if let totalBalance = loanBalance["totalBalance"] as? NSNumber{
                self.lblPL_loan_hash.text = "Total Balance: $\(totalBalance)"
            }
            
            if let totalCpiBal = loanBalance["totalCpiBal"] as? NSNumber{
                self.lblPLCpiLoan.text = "CPI Loan Bal: $\(totalCpiBal)"
            }
            
            if let totalDownPayment = loanBalance["totalDownPayment"] as? NSNumber{
                self.lblPL_downPymtDue.text = "Down Pmt Due: $\(totalDownPayment)"
            }
            
            if let totalInterest = loanBalance["totalInterest"] as? NSNumber{
                self.lblPL_PrinciplIntrst.text = "Principal Interest: $\(totalInterest)"
            }
            
            if let totalMiscFees = loanBalance["totalMiscFees"] as? NSNumber{
                self.lblPLMiscFees.text = "Misc Fees: $\(totalMiscFees)"
            }
            
            if let totalLateFees = loanBalance["totalLateFees"] as? NSNumber{
                self.lblPLLateFees.text = "Late Fees: $\(totalLateFees)"
            }
            
            if let totalPrincipal = loanBalance["totalPrincipal"] as? NSNumber{
                self.lblPLPrin.text = "Primary Loan Prin: $\(totalPrincipal)"
            }
            
            if let totalSalesTax = loanBalance["totalSalesTax"] as? NSNumber{
                self.lblPL_salesTaxtDue.text = "Sales Tax Due: $\(totalSalesTax)"
            }
            
            if let totalSideNotePrin = loanBalance["totalSideNotePrin"] as? NSNumber{
                self.lblPLSideNotePrin.text = "Side Note Prin: $\(totalSideNotePrin)"
            }
            
            if let totalSideNoteInterest = loanBalance["totalSideNoteInterest"] as? NSNumber{
                self.lblPLSideNoteIntrst.text = "Side Note: $\(totalSideNoteInterest)"
            }
            
            if let balance_on_street = loanBalance["balance_on_street"] as? NSNumber{
                self.lblPLBal_street.text = "Balance On Street: \(balance_on_street)"
            }
            
            if let payoff_status = loanBalance["payoff_status"] as? Bool{
                if(payoff_status == true){
                    self.lblPL_Autopay.text = "AutoPay Settings: ON"
                }else{
                    self.lblPL_Autopay.text = "AutoPay Settings: OFF"
                }
            }
            
            var due_amt = ""
            var due_date = ""
            var due_days = ""
            
            if let next_due_amount = loanBalance["next_due_amount"] as? NSNumber{
                due_amt = "\(next_due_amount)"
            }
            
            if let next_due_date = loanBalance["next_due_date"] as? String{
                if(next_due_date != ""){
                    let formatter = DateFormatter()
                    formatter.dateFormat = "MM-dd-yyyy"
                    let date = Date.dateFromISOString(string: next_due_date)
                    due_date = "\(formatter.string(from: date!))"
                }
                else{
                    due_date = ""
                }
            }
            
            if let next_due_days = loanBalance["next_due_days"] as? NSNumber{
                due_days = "\(next_due_days)"
            }
            
            self.lblPLBal_street.text = "Next Due: $\(due_amt)-\(due_date)-Days \(due_days)"
            
            
        }
        
        
        //        next_due_amount , next_due_date, next_due_days
        
        
    }
    
    func setLastView(){
        
        
        if let promiseToPay = self.accountDetailsData["promiseToPay"] as? NSDictionary{
            
            let broken = promiseToPay["broken"] as? Int
            let kept = promiseToPay["kept"] as? Int
            let total = promiseToPay["total"] as? Int
            self.lblPromiseToPay.text = "0 Open/ \(kept!) Kept/ \(broken!) Broken/ \(total!) Total"
            
            let promiseStatus = promiseToPay["promiseStatus"] as! String
            if(promiseStatus == "Kept"){
                viewLast.layer.borderColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0).cgColor
                lblPromColor.backgroundColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                self.imgPromColor.layer.borderColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0).cgColor
                btnPromColor.tintColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
                self.lblPromiseToPay.textColor = UIColorFromHex(rgbValue: 0x009F0B, alpha: 1.0)
            }
            else if(promiseStatus == "Open"){
                viewLast.layer.borderColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0).cgColor
                lblPromColor.backgroundColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                self.imgPromColor.layer.borderColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0).cgColor
                btnPromColor.tintColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                self.lblPromiseToPay.textColor = UIColorFromHex(rgbValue: 0xF3AE4E, alpha: 1.0)
                
            }
            else if(promiseStatus == "Broken"){
                viewLast.layer.borderColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0).cgColor
                lblPromColor.backgroundColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                self.imgPromColor.layer.borderColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0).cgColor
                btnPromColor.tintColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                self.lblPromiseToPay.textColor = UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
            }
        }
        
        if let collateral = self.accountDetailsData["collateral"] as? NSDictionary{
            
            if let vehicle = collateral["vehicle"] as? NSDictionary{
                
                var description = ""
                
                if let year = vehicle["year"] as? Int{
                    description = "\(year)"
                }
                if let make = vehicle["make"] as? String{
                    description += make
                }
                if let model = vehicle["model"] as? String{
                    
                    description += model
                }
                
                
                self.lblDEscription.text = "Description: \(description))"
                
                let vin = vehicle["vin"] as? String
                self.lblVin.text = "VIN: \(vin!)"
            }
            
            if let purchase = collateral["purchase"] as? NSDictionary{
                if let stockNumber = purchase["stockNumber"] as? String
                {
                    self.lblStock.text = "Stock #: \(stockNumber)"
                }
            }
        }
    }
}

//Bottom view functionality
extension accountDetailPageController{
    
    @IBAction func clickONBottomView(_ sender: UIButton)
    {
        if(sender.tag == 100){  //Call
            self.presentViewControllerBasedOnIdentifier("callOptionViewController", strStoryName: "Main")
        }
        else if(sender.tag == 200){  //Actions
            self.presentViewControllerBasedOnIdentifier("actionViewController", strStoryName: "Main")
        }
            
        else if(sender.tag == 300){  //Send
            self.presentViewControllerBasedOnIdentifier("sendViewController", strStoryName: "ActionStory")
        }
        else if(sender.tag == 400){  //Calendar
            self.presentViewControllerBasedOnIdentifier("promiseToPayViewController", strStoryName: "ActionStory")
        }
        else if(sender.tag == 500){  //Dollar
            self.presentViewControllerBasedOnIdentifier("postPaymentViewController", strStoryName: "ActionStory")
        }
        else if(sender.tag == 600){  //Notes
            self.presentViewControllerBasedOnIdentifier("notesViewController", strStoryName: "ActionStory")
        }
            
        else if(sender.tag == 700){  //Details tab
            let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
            let dest = story.instantiateViewController(identifier: "accountTabsViewController") as! accountTabsViewController
            dest.modalPresentationStyle = .fullScreen
            self.present(dest, animated: true, completion: nil)
        }
    }
}


// call api to get page detail
extension accountDetailPageController {
    
    func getAccountDetails(){
        var param = [String : Any]()
        if(self.isDetailOpenfrom == "accounts"){
            let queLayoutId = passDict_LayoutId["layout_id"] as! Int
            let defaultRecordsPage = passDict_LayoutId["defaultRecordsPage"] as! Int
            param = ["id": accId, //act_id dynamic
                "queueLayoutsId": queLayoutId,
                "currentPageNumber": 1,
                "defaultRecordsPage": defaultRecordsPage,
                "isGet": false,
                "isNext": false] as [String : Any]
        }
        else if(self.isDetailOpenfrom == "bin"){
            param = ["id": accId, //act_id dynamic
                "queueLayoutsId": 16,
                "currentPageNumber": 1,
                "isGet": false,
                "isNext": false] as [String : Any]
        }
        
        BaseApi.onResponsePostWithToken(url: Constant.id_acctDetails, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    
                    if let dictResponse = dict as? NSDictionary {
                        
                        if let data = dictResponse["data"] as? NSDictionary{
                            
                            self.accountDetailsData = data
                            
                            Globalfunc.print(object:self.accountDetailsData)
                            
                            let loanStatus = data["loanStatus"] as! NSDictionary
                            let actNo = loanStatus["account_no"] as! String
                            let account_status = loanStatus["account_status"] as! String
                            let account_type = loanStatus["account_type"] as! String
                            let loan_no = loanStatus["loan_no"] as! String
                            
                            let action = data["action"] as! NSDictionary
                            
                            if let alertMessage = action["alertMessage"] as? NSDictionary{
                                
                                self.viewAlertMsg.isHidden = false
                                self.hgtAlertview.constant = 60
                                
                                let message = alertMessage["message"] as! String
                                self.lblMsg.text = message
                                
                                let default_style = alertMessage["default_style"] as! Bool
                                if(default_style == true){
                                    self.viewAlertMsg.backgroundColor = self.UIColorFromHex(rgbValue: 0xEB0C0C, alpha: 1.0)
                                    self.lblMsg.textColor = .white
                                }
                                else{
                                    
                                    if(message == "")
                                    {
                                       self.viewAlertMsg.isHidden = true
                                       self.hgtAlertview.constant = 0
                                    }
                                    else{
                                        let back_color = (alertMessage["back_color"] as! String).trimmingCharacters(in: .whitespaces)
                                        let font_color = (alertMessage["font_color"] as! String).trimmingCharacters(in: .whitespaces)
                                        if(back_color != ""){
                                            let replaced = back_color.replacingOccurrences(of: "#", with: "0x")
                                            let result = UInt32(String(replaced.dropFirst(2)), radix: 16)
                                            self.viewAlertMsg.backgroundColor = self.UIColorFromHex(rgbValue: result!, alpha: 1.0)
                                        }
                                        if(font_color != ""){
                                            let fReplace = font_color.replacingOccurrences(of: "#", with: "0x")
                                            let result_1 = UInt32(String(fReplace.dropFirst(2)), radix: 16)
                                            self.lblMsg.textColor = self.UIColorFromHex(rgbValue: result_1!, alpha: 1.0)
                                        }

                                    }
                                    
                                }
                            }
                            else{
                                
                                self.viewAlertMsg.isHidden = true
                                self.hgtAlertview.constant = 0
                                
                            }
                            
                            let balanceType = action["balanceType"] as! NSArray
                            
                            var promiseDict : NSDictionary = [:]
                            if let promiseToPay = data["loanPromiseToPay"] as? NSDictionary
                            {
                                promiseDict = promiseToPay
                            }
                            
                            let loanVehicle = data["loanVehicle"] as! NSDictionary
                            
                            
                            
                            let borrower = data["loanBorrower"] as! NSDictionary
                            let _id = self.accId
                            let insid = user?.institute_id
                            let userid = user?._id
                            
                            let loanBalance = data["loanBalance"] as! NSDictionary
                            let totalPrincipal = loanBalance["totalPrincipal"] as! NSNumber
                            let totalMiscFees = loanBalance["totalMiscFees"] as! NSNumber
                            let totalInterest = loanBalance["totalInterest"] as! NSNumber
                            let totalSalesTax = loanBalance["totalSalesTax"] as! NSNumber
                            
                            let loanAddress = data["loanAddress"] as! NSDictionary
                            
                            self.dict_details.setValue(_id, forKey: "_id")
                            self.dict_details.setValue(insid, forKey: "insid")
                            self.dict_details.setValue(userid, forKey: "userid")
                            
                            self.dict_details.setValue(actNo, forKey: "account_no")
                            self.dict_details.setValue(account_status, forKey: "account_status")
                            self.dict_details.setValue(account_type, forKey: "account_type")
                            self.dict_details.setValue(loan_no, forKey: "loan_no")
                            self.dict_details.setValue(borrower, forKey: "loanBorrower")
                            self.dict_details.setValue(balanceType, forKey: "balanceType")
                            self.dict_details.setValue(promiseDict, forKey: "promiseToPay")
                            self.dict_details.setValue(totalPrincipal, forKey: "totalPrincipal")
                            self.dict_details.setValue(totalMiscFees, forKey: "totalMiscFees")
                            self.dict_details.setValue(totalInterest, forKey: "totalInterest")
                            self.dict_details.setValue(totalSalesTax, forKey: "totalSalesTax")
                            self.dict_details.setValue(action, forKey: "action")
                            self.dict_details.setValue(loanAddress, forKey: "loanAddress")
                            self.dict_details.setValue(loanVehicle, forKey: "loanVehicle")
                            
                            
                            print(self.dict_details)
                            
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: self.dict_details, requiringSecureCoding: false)
                                    userDef.set(myData, forKey: "dataDict")
                                    
                                    
                                    print(myData)
                                    
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: self.dict_details)
                                    userDef.set(myData, forKey: "dataDict")
                                }
                            } catch {
                                Globalfunc.print(object: "Couldn't write file")
                            }
                            self.getCallDetailsApi()
                            self.setUpUI()
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//call action api for all pages once
extension accountDetailPageController{
    
    func getCallDetailsApi()
    {
        let Str_url = "\(Constant.getCall_url)?account_id=\(self.accId!)"
        BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        if let data = responseDict["data"] as? NSDictionary{
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false)
                                    userDef.set(myData, forKey: "action_dict")
                                    
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: data)
                                    userDef.set(myData, forKey: "action_dict")
                                }
                            } catch {
                                Globalfunc.print(object: "Couldn't write file")
                            }
                            
                        }
                        
                    }
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension accountDetailPageController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int?
        if tableView == self.tblList{
            count = arrList.count
        }
        if tableView == self.tblMoreList{
            count = arrMoreList.count
        }
        if tableView == self.tblDocList{
            count = arrDocList.count
        }
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if tableView == self.tblList{
            let cell = self.tblList.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
            cell.lblTitle.text = self.arrList[indexPath.row]
            return cell
        }
        if tableView == self.tblMoreList{
            let cell = self.tblList.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
            cell.lblTitle.text = self.arrMoreList[indexPath.row]
            return cell
            
        }
        if tableView == self.tblDocList{
            let cell = self.tblList.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
            cell.lblTitle.text = self.arrDocList[indexPath.row]
            return cell
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tblList{
            self.passToView(indexPath: indexPath)
        }
        if tableView == self.tblMoreList{
            switch indexPath.row {
            case 0:
                isUpload = "account"
                self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
            case 1:
                isUpload = "borrower"
                self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
            case 2:
                isUpload = "collateral-image"
                self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
            case 3:
                isUpload = "collateral"
                self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
            case 4:
                self.presentViewControllerBasedOnIdentifier("addEditFlagsVC", strStoryName: "dollarStory")
            case 5:
                self.presentViewControllerBasedOnIdentifier("addRefrenceVC", strStoryName: "dollarStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
        if tableView == self.tblDocList{
            switch indexPath.row {
            case 0:
                let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
                let dest = story.instantiateViewController(identifier: "attachDocViewController") as! attachDocViewController
                dest.modalPresentationStyle = .fullScreen
                dest.isOpenFrom = "document"
                dest.isFromInventory = "false"
                self.present(dest, animated: true, completion: nil)
            case 1:
                self.presentViewControllerasPopup("reportListViewController", strStoryName: "ActionStory")
            default:
                Globalfunc.print(object:"default")
            }
            
        }
    }
    
    func passToView(indexPath: IndexPath)
    {
        if(self.account_status == "Charge Off"){
            switch indexPath.row {
            case 0:
                self.presentViewControllerasPopup("sideNoteVC", strStoryName: "dollarStory")
            case 1:
                self.presentViewControllerasPopup("reverseTransVC", strStoryName: "dollarStory")
            case 2:
                self.presentViewControllerasPopup("posrReturnNsfVC", strStoryName: "dollarStory")
            case 3:
                self.presentViewControllerBasedOnIdentifier("postPaymentViewController", strStoryName: "ActionStory")
            case 4:
                self.presentViewControllerasPopup("recoveryReversalVC", strStoryName: "dollarStory")
            case 5:
                self.presentViewControllerasPopup("chargeOffAdjustmntVC", strStoryName: "dollarStory")
            case 6:
                self.presentViewControllerasPopup("chargeOffReversalVC", strStoryName: "dollarStory")
            case 7:
                self.presentViewControllerasPopup("postamendmentVC", strStoryName: "dollarStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
        else if(self.account_status == "Active"){
            
            switch indexPath.row {
            case 0:
                self.presentViewControllerBasedOnIdentifier("postPaymentViewController", strStoryName: "ActionStory")
            case 1:
                self.presentViewControllerasPopup("payOffVC", strStoryName: "dollarStory")
            case 2:
                self.presentViewControllerasPopup("postExtentionVC", strStoryName: "dollarStory")
            case 3:
                self.presentViewControllerasPopup("postchargeOffVC", strStoryName: "dollarStory")
            case 4:
                self.presentViewControllerasPopup("writeDownVC", strStoryName: "dollarStory")
            case 5:
                self.presentViewControllerasPopup("sideNoteVC", strStoryName: "dollarStory")
            case 6:
                self.presentViewControllerasPopup("skipAutpPayVC", strStoryName: "dollarStory")
            case 7:
                self.presentViewControllerasPopup("reverseTransVC", strStoryName: "dollarStory")
            case 8:
                self.presentViewControllerasPopup("posrReturnNsfVC", strStoryName: "dollarStory")
            case 9:
                self.presentViewControllerasPopup("reversePostExtnsionVC", strStoryName: "dollarStory")
            case 10:
                self.presentViewControllerasPopup("reverseSkipVC", strStoryName: "dollarStory")
            case 11:
                self.presentViewControllerasPopup("prinAdjustmntVC", strStoryName: "dollarStory")
            case 12:
                self.presentViewControllerasPopup("nonEarnAdjustmntVC", strStoryName: "dollarStory")
            case 13:
                self.presentViewControllerasPopup("salesTaxAdjustmnt", strStoryName: "dollarStory")
            case 14:
                self.presentViewControllerasPopup("intrestAdjustmntVC", strStoryName: "dollarStory")
            case 15:
                self.presentViewControllerasPopup("miscFeeAdjustmntVc", strStoryName: "dollarStory")
            case 16:
                self.presentViewControllerasPopup("postamendmentVC", strStoryName: "dollarStory")
            default:
                Globalfunc.print(object:"default")
            }
            
        }
        else if(self.account_status == "Closed"){
            
            switch indexPath.row {
            case 0:
                self.presentViewControllerasPopup("sideNoteVC", strStoryName: "dollarStory")
            case 1:
                self.presentViewControllerasPopup("reverseTransVC", strStoryName: "dollarStory")
            case 2:
                self.presentViewControllerasPopup("posrReturnNsfVC", strStoryName: "dollarStory")
            case 3:
                self.presentViewControllerasPopup("miscFeeAdjustmntVc", strStoryName: "dollarStory")
            case 5:
                self.presentViewControllerasPopup("postamendmentVC", strStoryName: "dollarStory")
            default:
                Globalfunc.print(object:"default")
            }
            
        }
        else if(self.account_status == "Out For repo"){
        }
        else if(self.account_status == "Repossessed"){
        }
        else if(self.account_status == "Skip"){
            switch indexPath.row {
            case 0:
                self.presentViewControllerasPopup("postamendmentVC", strStoryName: "dollarStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
    }
}

class tblDolarCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}
