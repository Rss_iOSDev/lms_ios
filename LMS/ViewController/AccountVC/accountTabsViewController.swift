//
//  accountTabsViewController.swift
//  LMS
//
//  Created by Apple on 28/04/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit


class accountTabsViewController: UIViewController {
    
    @IBOutlet weak var collTitles : UITableView!
    @IBOutlet weak var lblAcctno : UILabel!
    
    
    var arrTitles = ["SNAPSHOT","BALANCES","BORROWER","COLLATERAL","TRANSACTIONS","PAYMENT SCHED","INSURANCE","REPOS","FILES","ECABINET","USER DEFINED"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collTitles.tableFooterView = UIView()
    
        do {
            if let decoded  = userDef.object(forKey: "dataDict") as? Data{
                if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                    let account_no = dataD["account_no"] as! String
                    self.lblAcctno.text = "Account No: \(account_no)"
                }
            }

        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
   
}

extension accountTabsViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.collTitles.dequeueReusableCell(withIdentifier: "colltitleCell") as! colltitleCell
        cell.lblTitle.text = self.arrTitles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.presentViewControllerBasedOnIdentifier("snapshotViewController", strStoryName: "ActionStory")
        case 1:
            self.presentViewControllerBasedOnIdentifier("balancesViewController", strStoryName: "ActionStory")
        case 2:
            self.presentViewControllerBasedOnIdentifier("borrowerViewController", strStoryName: "ActionStory")
        case 3:
            self.presentViewControllerBasedOnIdentifier("collateralViewController", strStoryName: "ActionStory")
        case 4:
            self.presentViewControllerBasedOnIdentifier("transactionVC", strStoryName: "ActionStory")
        case 5:
            self.presentViewControllerBasedOnIdentifier("paymntSchdVC", strStoryName: "ActionStory")
        case 6:
            self.presentViewControllerBasedOnIdentifier("insuranceHistoryVC", strStoryName: "ActionStory")
        case 7:
            self.presentViewControllerBasedOnIdentifier("repoHistoryVC", strStoryName: "ActionStory")
        case 8:
            self.presentViewControllerBasedOnIdentifier("filesViewController", strStoryName: "ActionStory")
        case 9:
            self.presentViewControllerBasedOnIdentifier("ecabinetViewController", strStoryName: "ActionStory")
        case 10:
            self.presentViewControllerBasedOnIdentifier("userDefineViewController", strStoryName: "ActionStory")
        
        default:
            Globalfunc.print(object:"default")
        }
    }
}

class colltitleCell : UITableViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
}




