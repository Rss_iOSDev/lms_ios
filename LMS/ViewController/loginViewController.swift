
import UIKit

class loginViewController: UIViewController {
    
    @IBOutlet weak var txtUSername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRemember: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let isRemember = userDef.object(forKey: "isRemember") as? NSDictionary {
            let email = isRemember["username"] as! String
            let password = isRemember["password"] as! String
            self.txtUSername.text = email
            self.txtPassword.text = password
            btnRemember.isSelected = true
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default //.default for black style
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.clear
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    
    func callPerFormLoginApionBtnLogin()
    {
        let param = ["email" : "\(txtUSername.text!)",
                     "password" : "\(txtPassword.text!)"] as NSDictionary
        BaseApi.onResponsePost(url: "\(Constant.login_url)", parms: param) { (dict) in
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation{
                do {
                    
                    Globalfunc.hideLoaderView(view: self.view)
                    let responseDict = dict as! NSDictionary
                    
                    print(responseDict)
                    
                    if let success = responseDict["success"] as? Bool
                    {
                        if(success == false){
                            let msg = responseDict["msg"] as! String
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                        }
                        else{
                            let authToken = responseDict["token"] as! String
                            userDef.set(authToken, forKey: "authToken")
                            userDef.set(self.txtPassword.text, forKey: "pass")
                            
                            let userD = responseDict["user"] as! [String:Any]
                            Globalfunc.print(object:userD)
                            
                            let jsonData = try? JSONSerialization.data(withJSONObject: userD, options: [])
                            let jsonString = String(data: jsonData!, encoding: .utf8)
                            if let jsonData = jsonString?.data(using: .utf8)
                            {
                                //And here you get the Car object back
                                user = try? JSONDecoder().decode(UserModal.self, from: jsonData)
                                //But only with name, age, gender properties decode from json as we have defined CodingKeys enum in Person class. For more info, check the definition of Person class
                            }
                            
                            let encoder = JSONEncoder()
                            if let encoded = try? encoder.encode(user) {
                                userDef.set(encoded, forKey: "userInfo")
                            }
                            
                            let home = self.storyboard?.instantiateViewController(identifier: "homeViewController") as! homeViewController
                            self.navigationController?.pushViewController(home, animated: true)
                            
                        }
                    }
                }
            }
        }
    }
    
    
    @IBAction func clickOnLoginBtn(_ sender: UIButton)
    {
        //self.txtUSername.text = "rssindore01teamlead@gmail.com"
        //self.txtPassword.text = "password"

        if(txtUSername.text == "" || txtUSername.text?.count == 0 || txtUSername.text == nil){
            txtUSername.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
        }
        else if(!(Globalfunc.isValidEmail(testStr: txtUSername.text!))){
            txtUSername.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
        }
        else if(txtPassword.text == "" || txtPassword.text?.count == 0 || txtPassword.text == nil){
            txtPassword.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill password field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.callPerFormLoginApionBtnLogin()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    @IBAction func clickOnRememberMe(_ sender: UIButton)
    {
        if (sender.isSelected == true){
            sender.isSelected = false
            userDef.removeObject(forKey: "isRemember")
        }
        else{
            sender.isSelected = true
            let dict = ["username":"\(self.txtUSername.text!)",
                        "password":"\(self.txtPassword.text!)"]
            userDef.set(dict, forKey: "isRemember")
        }
    }
    
    @IBAction func clickOnForgetPAsBtn(_ sender: UIButton)
    {
        let forget = self.storyboard?.instantiateViewController(identifier: "forgetPassViewController") as! forgetPassViewController
        self.navigationController?.pushViewController(forget, animated: true)
    }
}

//                    let encoder = DictionaryEncoder()
//                    if let encoded = try? encoder.encode(user) as [String:Any]{
//                     userDef.set(encoded, forKey: "userinfo")
//                    }


//                            do {
//                                if #available(iOS 11.0, *) {
//                                        let myData = try NSKeyedArchiver.archivedData(withRootObject: userD, requiringSecureCoding: false)
//                                        userDef.set(myData, forKey: "userinfo")
//                                } else {
//                                        let myData = NSKeyedArchiver.archivedData(withRootObject: userD)
//                                        userDef.set(myData, forKey: "userinfo")
//                                }
//                            } catch {
//                                        Globalfunc.print(object:"Couldn't write file")
//                            }



//                        Defaults[.session] = true
//                        Defaults[.email] = user.email



//                        Settings.userId = user._id
//                        Settings.firstName = user.fname
//                        Settings.lastName = user.lname
//                        Settings.email =
//
