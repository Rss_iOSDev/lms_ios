
import UIKit

class homeViewController: baseViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var collFavList: UICollectionView!
    var arrFavList = NSMutableArray()
     
    @IBOutlet weak var btnSave: UIView!
    @IBOutlet weak var btnCancel: UIView!
    @IBOutlet weak var btnSetting: UIButton!
    
    
    @IBOutlet weak var viewEdit: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var btnEditPass: UIButton!
    
    var arrSelectId = NSMutableArray()
    var arrSelectEditId = NSMutableArray()
    
    var isSelectTyp = ""
    
    var selectTyp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrFavList = []
        
        self.btnSave.isHidden = true
        self.btnCancel.isHidden = true
        self.btnSetting.isHidden = false
        self.viewEdit.isHidden = true
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerapi()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            let url = "\(Constant.favorite)?userid=\((user?._id)!)"
            self.call_getFavoriteList(strUrl: url)
        })
        
        
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    
    @IBAction func clikcSettingBtn(_ sender : UIButton)
    {
        self.btnSave.isHidden = false
        self.btnCancel.isHidden = false
        self.btnSetting.isHidden = true
        
        for i in 0..<self.arrFavList.count{
            let dictA = self.arrFavList[i] as! NSMutableDictionary
            dictA.setValue(true, forKey: "edit")
            self.arrFavList.replaceObject(at: i, with: dictA)
        }
        
        let dict : NSMutableDictionary = [:]
        dict.setValue(true, forKey: "add")
        self.arrFavList.add(dict)
        
        self.collFavList.reloadData()
    }
    
    @IBAction func clikcOnCancelBtn(_ sender : UIButton)
    {
        self.arrFavList = []
        self.collFavList.reloadData()
        self.viewDidLoad()
    }
    
    
}

//Call Api for common
extension homeViewController
{
    func callAllPickerapi()
    {
        let group = DispatchGroup() // initialize
        Constant.arrUrl_forCommon.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.timezone_url){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            Constant.arrTimeZoneData.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        if(obj == Constant.institution_url){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            Constant.arrInstitutionData.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        if(obj == Constant.default_branch_url){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary]
                                {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            consArrays.arrDefaultBranch.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                        }
                        group.leave()
                        
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
            // do something here when loop finished
            Globalfunc.hideLoaderView(view: self.view)
        }
    }
}

extension homeViewController {
    
    func call_getFavoriteList(strUrl: String)
    {
        self.arrFavList = []
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let response = dict as? NSDictionary {
                        if let data = response["data"] as? [NSDictionary]
                        {
                            if(data.count > 0){
                                for i in 0..<data.count{
                                    let dictA = data[i] as! NSMutableDictionary
                                    dictA.setValue(false, forKey: "edit")
                                    self.arrFavList.add(dictA)
                                }
                                self.collFavList.reloadData()
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


//coll datasource delegate
extension homeViewController : UICollectionViewDelegate , UICollectionViewDataSource
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFavList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
           let cell = self.collFavList.dequeueReusableCell(withReuseIdentifier: "collFavCell", for: indexPath) as! collFavCell
           let dict = self.arrFavList[indexPath.row] as! NSDictionary
        
        print(dict)
        
        if let type = dict["type"] as? String{
            if(type == "search-account"){
            }
            else if(type == "account-queue"){
            }
            else if(type == "search-inventory"){
            }
            else if(type == "add-inventory"){
            }
            else if(type == "eval-inventory"){
            }
            else if(type == "inventory-queue"){
            }
            else if(type == "dashboard"){
            }
            else if(type == "report"){
            }
        }
            cell.viewAdd.isHidden = true
        if let title = dict["title"] as? String{
            cell.lblTitle.text = title
        }
           if let edit = dict["edit"] as? Bool
           {
               if(edit == true){
                   cell.btnEdit.isHidden = false
                   cell.btnCross.isHidden = false
               }
               else{
                   cell.btnEdit.isHidden = true
                   cell.btnCross.isHidden = true
               }
           }
        
            if(indexPath.row == self.arrFavList.count - 1){
             //this is the last row in section.
                if let _ = dict["add"] as? Bool{
                    cell.viewAdd.isHidden = false
                    
                }
                else{
                    cell.viewAdd.isHidden = true
                }
        }
        
        cell.btnCross.tag = indexPath.row
        cell.btnCross.addTarget(self, action: #selector(clickOnCrossBtn(_:)), for: .touchUpInside)
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clickOnEditBtn(_:)), for: .touchUpInside)
        
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(clickOnAddBtn(_:)), for: .touchUpInside)
           
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = self.arrFavList[indexPath.row] as! NSDictionary
        if let url = dict["url"] as? String{
            if url.hasPrefix("/account") { // true
               self.openViewControllerBasedOnIdentifier("accountsViewController", strStoryName: "Main")
            }
            else if url.hasPrefix("/dashboard") { // true
            }
        }
    }
    
    @objc func clickOnCrossBtn(_ sender: UIButton)
    {
        isSelectTyp = "delete"
        
        let dict = arrFavList[sender.tag] as! NSMutableDictionary
        let _id = dict["_id"] as! Int
        self.arrSelectId.add(_id)
        
        self.arrFavList.remove(dict)
        self.collFavList.reloadData()
        
    }
    
    @objc func clickOnEditBtn(_ sender: UIButton)
    {
        isSelectTyp = "edit"
        self.lblTitle.text = "MODIFY FAVORITE"
        self.viewEdit.isHidden = false
        
        self.btnEditPass.tag = sender.tag
        self.txtTitle.tag = sender.tag
        
        let newdict = arrFavList[sender.tag] as! NSMutableDictionary
        let title = newdict["title"] as! String
        self.txtTitle.text = title

        
    }
    
    @objc func clickOnAddBtn(_ sender: UIButton)
    {
        isSelectTyp = "add"
        self.txtTitle.text = ""
        self.lblTitle.text = "ADD FAVORITE"
        self.viewEdit.isHidden = false
    }

    
    @IBAction func clickOnOkEditBtn(_ sender: UIButton){
        
        if(self.lblTitle.text == "MODIFY FAVORITE"){
            self.arrSelectEditId = []
            let newdict = arrFavList[sender.tag] as! NSMutableDictionary
            self.viewEdit.isHidden = true
            newdict.setValue(self.txtTitle.text!, forKey: "title")
            self.arrFavList.replaceObject(at: sender.tag, with: newdict)
            self.arrSelectEditId.add(newdict)
            self.collFavList.reloadData()
        }
        else if(self.lblTitle.text == "ADD FAVORITE"){
            
            self.arrSelectEditId = []
            let new : NSMutableDictionary = [:]
            new.setValue(self.txtTitle.text!, forKey: "title")
            new.setValue(self.selectTyp, forKey: "type")
            let url = "/\(self.selectTyp)"
            new.setValue(url, forKey: "url")
            
            self.viewEdit.isHidden = true
            
            self.arrSelectEditId.add(new)
            self.arrFavList.add(new)
            self.collFavList.reloadData()
            
        }
        
    }
    
    @IBAction func clickOnSelectTypBtn(_ sender: UIButton){
        if(sender.tag == 10){
            self.selectTyp = "search-account"
            self.txtTitle.text = "Search Account"
        }
        else if(sender.tag == 20){
            self.selectTyp = "dashboard"
            self.txtTitle.text = "Dashboard"
        }
        else if(sender.tag == 30){
            self.selectTyp = "report"
            self.txtTitle.text = "Reports"
        }

    }

    @IBAction func clickOnCrossEditBtn(_ sender: UIButton){
        self.viewDidLoad()
    }
    
    @IBAction func clickONSaveIdBtn(_ sender: UIButton)
    {
            if(self.isSelectTyp == "delete"){
                Globalfunc.showLoaderView(view: self.view)
                let param = ["id":self.arrSelectId] as [String: Any]
                
                BaseApi.onResponseDeleteWithTP(url: Constant.favorite, controller: self, parms: param as NSDictionary) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let arr = dict as? NSDictionary {
                                let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    self.viewDidLoad()
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
            else if(isSelectTyp == "edit"){
                let dict = self.arrSelectEditId[0] as! NSDictionary
                let title = dict["title"] as! String
                let url = dict["url"] as! String
                let _id = dict["_id"] as! Int
                let type = dict["type"] as! String
                
                Globalfunc.showLoaderView(view: self.view)
                let params = [ "id": _id,
                               "insid": (user?.institute_id!)!,
                               "title": title,
                               "type":type,
                               "url": url,
                               "userid": (user?._id)!] as [String : Any]

                BaseApi.onResponsePutWithToken(url: Constant.favorite, controller: self, parms: params as NSDictionary) { (dict, error) in
                    if(error == ""){
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            if let arr = dict as? NSDictionary {
                                let msg = arr["msg"] as! String
                                let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                    self.viewDidLoad()
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                    else
                    {
                        OperationQueue.main.addOperation {
                            Globalfunc.hideLoaderView(view: self.view)
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                        }
                    }
                }
            }
             else if(isSelectTyp == "add"){
                
                 let dict = self.arrSelectEditId[0] as! NSDictionary
                 let title = dict["title"] as! String
                 let url = dict["url"] as! String
                 let type = dict["type"] as! String
                 
                 Globalfunc.showLoaderView(view: self.view)
                 let params = [ "id": "",
                                "insid": (user?.institute_id!)!,
                                "title": title,
                                "type":type,
                                "url": url,
                                "userid": (user?._id)!] as [String : Any]
                
                BaseApi.onResponsePostWithToken(url: Constant.favorite, controller: self, parms: params) { (dict, error) in
                
                     if(error == ""){
                         OperationQueue.main.addOperation {
                             Globalfunc.hideLoaderView(view: self.view)
                             if let arr = dict as? NSDictionary {
                                 let msg = arr["msg"] as! String
                                 let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                                 let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                     self.viewDidLoad()
                                 }
                                 alertController.addAction(OKAction)
                                 self.present(alertController, animated: true, completion:nil)
                             }
                         }
                     }
                     else
                     {
                         OperationQueue.main.addOperation {
                             Globalfunc.hideLoaderView(view: self.view)
                             Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                         }
                     }
                 }
             }

    }
}

extension homeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension homeViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
}
class collFavCell : UICollectionViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnCross : UIButton!
    
    @IBOutlet weak var viewAdd : UIView!
    @IBOutlet weak var btnAdd : UIButton!
    
}


