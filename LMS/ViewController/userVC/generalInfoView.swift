//
//  generalInfoView.swift
//  LMS
//
//  Created by Reinforce on 10/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit



class generalInfoView: UIViewController {
    
    
    @IBOutlet weak var collUserRole_frst: UICollectionView!
    @IBOutlet weak var collUserRole_sec: UICollectionView!
    
    
    //textfields
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtUSercode: UITextField!
    @IBOutlet weak var txtTimeZone: UITextField!
    @IBOutlet weak var txtDEfaultInstitution: UITextField!
    @IBOutlet weak var txtFrstName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtSalutation: UITextField!
    @IBOutlet weak var txtsuffix: UITextField!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtAltEmail: UITextField!
    @IBOutlet weak var txtHomePhn: UITextField!
    @IBOutlet weak var txtWorkPhn: UITextField!
    @IBOutlet weak var txtCellPhn: UITextField!
    @IBOutlet weak var txtOther: UITextField!
    @IBOutlet weak var txtFax: UITextField!
    
    @IBOutlet weak var btnEmailDisable: UIButton!
    
    @IBOutlet weak var btnAsignd: UIButton!
    @IBOutlet weak var lblassignd: UILabel!
    
    var pickerViewTimeZone: UIPickerView!
    var pickerViewSalutation: UIPickerView!
    var pickerViewSuffix: UIPickerView!
    var pickerViewInstitution: UIPickerView!
    
    var timeZone = ""
    var instId = ""
    
    var arrRole_frst : NSMutableArray = []
    // var arrSelected_role_frst : NSMutableArray = []
    
    var arrRole_sec : NSMutableArray = []
    var arrSelected_role : NSMutableArray = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collUserRole_frst.allowsMultipleSelection = true
        collUserRole_sec.allowsMultipleSelection = true
        
        let dict1 = ["_id": 2, "title": "Principal Role", "isSelect": false] as [String : Any];
        let dict2 = ["_id": 3, "title": "System/User Admin","isSelect": false] as [String : Any];
        let dict3 = ["_id": 4, "title": "Sales Mgr Role","isSelect": false] as [String : Any];
        let dict4 = ["_id": 5, "title": "FI Mgr Role","isSelect": false] as [String : Any];
        
        
        arrRole_sec.add(dict1)
        arrRole_sec.add(dict2)
        arrRole_sec.add(dict3)
        arrRole_sec.add(dict4)
        
        if(isViewFrom == "Add"){
            let dict5 = ["title": "Active User", "isSelect": true] as [String : Any]
            let dict6 = ["title": "Password Never Expires","isSelect": false] as [String : Any]
            let dict7 = ["title": "Receive System Message","isSelect": false] as [String : Any]
            let dict8 = ["title": "Cannot Change Password","isSelect": false] as [String : Any]
            arrRole_frst.add(dict5)
            arrRole_frst.add(dict6)
            arrRole_frst.add(dict7)
            arrRole_frst.add(dict8)
            
            self.btnEmailDisable.isHidden = true
        }
        else if(isViewFrom == "Edit"){
            self.btnEmailDisable.isHidden = false
            self.setUpUI()
        }
        // Do any additional setup after loading the view.
    }
    
    
    
    func setUpUI(){
        
        do {
            let decoded  = userDef.object(forKey: "passDict") as! Data
            if let passDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                
                Globalfunc.print(object: passDict)
                
                if let is_deleted = passDict["is_deleted"] as? Bool{
                    
                    if(is_deleted == false){
                        let dict5 = ["title": "Active User", "isSelect": true] as [String : Any]
                        arrRole_frst.add(dict5)
                    }
                    else{
                        let dict5 = ["title": "Active User", "isSelect": false] as [String : Any]
                        arrRole_frst.add(dict5)
                    }
                }
                
                let dict6 = ["title": "Password Never Expires","isSelect": false] as [String : Any]
                let dict7 = ["title": "Receive System Message","isSelect": false] as [String : Any]
                let dict8 = ["title": "Cannot Change Password","isSelect": false] as [String : Any]
                arrRole_frst.add(dict6)
                arrRole_frst.add(dict7)
                arrRole_frst.add(dict8)
                
                
                if let email = passDict["email"] as? String{
                    self.txtEmail.text = email
                }
                
                if let user_code = passDict["user_code"] as? String{
                    self.txtUSercode.text = user_code
                }
                
                if let fname = passDict["fname"] as? String{
                    self.txtFrstName.text = fname
                }
                
                if let mname = passDict["mname"] as? String{
                    self.txtMiddleName.text = mname
                }
                
                if let lname = passDict["lname"] as? String{
                    self.txtLastName.text = lname
                }
                
                if let other = passDict["other"] as? String{
                    self.txtOther.text = other
                }
                
                if let alt_email = passDict["alt_email"] as? String{
                    self.txtAltEmail.text = alt_email
                }
                
                if let title = passDict["title"] as? String{
                    self.txtTitle.text = title
                }
                
                if let home_phone = passDict["home_phone"] as? String{
                    self.txtHomePhn.text = home_phone
                }
                
                if let cell_phone = passDict["cell_phone"] as? String{
                    self.txtCellPhn.text = cell_phone
                }
                
                if let work_phone = passDict["work_phone"] as? String{
                    self.txtWorkPhn.text = work_phone
                }
                
                if let fax = passDict["fax"] as? String{
                    self.txtFax.text = fax
                }
                
                if let suffix = passDict["suffix"] as? String{
                    self.txtsuffix.text = suffix
                }
                
                if let salutation = passDict["salutation"] as? String{
                    self.txtSalutation.text = salutation
                }
                
                if let timezone = passDict["timezone"] as? String{
                    
                    for i in 0..<Constant.arrTimeZoneData.count{
                        let dict = Constant.arrTimeZoneData[i] as! NSDictionary
                        let id = dict["_id"] as! Int
                        
                        if(timezone == "\(id)"){
                            self.txtTimeZone.text = dict["title"] as? String
                        }
                    }
                }
                
                if let timezone = passDict["timezone"] as? Int{
                    
                    for i in 0..<Constant.arrTimeZoneData.count{
                        let dict = Constant.arrTimeZoneData[i] as! NSDictionary
                        let id = dict["_id"] as! Int
                        
                        if(timezone == id){
                            self.txtTimeZone.text = dict["title"] as? String
                        }
                    }
                }
                
                
                if let institute_id = passDict["institute_id"] as? String{
                    
                    for i in 0..<Constant.arrInstitutionData.count{
                        let dict = Constant.arrInstitutionData[i] as! NSDictionary
                        let id = dict["_id"] as! Int
                        
                        if(institute_id == "\(id)"){
                            self.txtDEfaultInstitution.text = dict["code"] as? String
                            self.instId = "\(institute_id)"
                        }
                    }
                    
                    let defInsId = user?.institute_id
                    if(institute_id == "\(defInsId!)"){
                        self.btnAsignd.isSelected = true
                        let defCode = user?.institute_code
                        let defTitle = user?.institute_title
                        self.lblassignd.text = "\(defCode!)-\(defTitle!)"
                    }
                    else{
                        self.btnAsignd.isSelected = false
                        self.lblassignd.text = ""
                    }
                }
                
                if let role_id = passDict["role_id"] as? NSMutableArray{
                    
                    for i in 0..<arrRole_sec.count{
                        
                        var dictR = arrRole_sec[i] as! [String: Any]
                        let main_id = dictR["_id"] as! Int
                        
                        for j in 0..<role_id.count{
                            let dict = role_id[j] as! NSDictionary
                            if let _id = dict["_id"] as? Int{
                                
                                if(main_id == _id){
                                    let _ = dictR.updateValue(true, forKey: "isSelect")
                                    arrRole_sec.replaceObject(at: i, with: dictR)
                                }
                            }
                            
                        }
                    }
                    arrSelected_role = role_id
                }
                Globalfunc.print(object:arrRole_sec)
                
            }
        }
        catch {
        }
        //   Globalfunc.print(object:arrSelected_role)
        
        
        
        //        if let  = passDict["salutation"] as? String{
        //            self.txtSalutation.text = salutation
        //        }
        //
        //        if let  = passDict["salutation"] as? String{
        //            self.txtSalutation.text = salutation
        //        }
        //
        
        
    }
    
    
    @IBAction func clickOnbackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}

extension generalInfoView : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerViewTimeZone){
            return Constant.arrTimeZoneData.count
        }
        else if(pickerView == pickerViewSalutation){
            return Constant.arrSalutation.count
        }
        else if(pickerView == pickerViewSuffix){
            return Constant.arrSuffix.count
        }
        else if pickerView == pickerViewInstitution{
            return Constant.arrInstitutionData.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        if(pickerView == pickerViewTimeZone){
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        else if(pickerView == pickerViewSalutation){
            let item = Constant.arrSalutation[row]
            return item
        }
        else if(pickerView == pickerViewSuffix){
            let item = Constant.arrSuffix[row]
            return item
        }
        else if pickerView == pickerViewInstitution{
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            return item
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewTimeZone){
            
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtTimeZone.text = item
            self.timeZone = "\(_id)"
            
        }
            
        else if(pickerView == pickerViewSalutation){
            
            let item = Constant.arrSalutation[row]
            self.txtSalutation.text = item
            
        }
            
        else if(pickerView == pickerViewSuffix){
            
            let item = Constant.arrSuffix[row]
            self.txtsuffix.text = item
            
        }
            
        else  if(pickerView == pickerViewInstitution){
            
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtDEfaultInstitution.text = item
            self.instId = "\(_id)"
            
        }
        
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtTimeZone){
            self.pickUp(txtTimeZone)
            
        }
        else if(textField == self.txtSalutation){
            self.pickUp(txtSalutation)
        }
        else if(textField == self.txtsuffix){
            self.pickUp(txtsuffix)
        }
        else if(textField == self.txtDEfaultInstitution){
            self.pickUp(txtDEfaultInstitution)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtTimeZone){
            self.pickerViewTimeZone = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTimeZone.delegate = self
            self.pickerViewTimeZone.dataSource = self
            self.pickerViewTimeZone.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTimeZone
        }
            
        else if(textField == self.txtSalutation){
            self.pickerViewSalutation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewSalutation.delegate = self
            self.pickerViewSalutation.dataSource = self
            self.pickerViewSalutation.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewSalutation
        }
        else if(textField == self.txtsuffix){
            self.pickerViewSuffix = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewSuffix.delegate = self
            self.pickerViewSuffix.dataSource = self
            self.pickerViewSuffix.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewSuffix
        }
        
        if(textField == self.txtDEfaultInstitution){
            self.pickerViewInstitution = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInstitution.delegate = self
            self.pickerViewInstitution.dataSource = self
            self.pickerViewInstitution.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInstitution
            
            
        }
        //
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editPasswordViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(editPasswordViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        self.txtTimeZone.resignFirstResponder()
        self.txtSalutation.resignFirstResponder()
        self.txtsuffix.resignFirstResponder()
        self.txtDEfaultInstitution.resignFirstResponder()
        
    }
    
    
    @objc func cancelClick() {
        
        
        self.txtTimeZone.resignFirstResponder()
        self.txtSalutation.resignFirstResponder()
        self.txtsuffix.resignFirstResponder()
        self.txtDEfaultInstitution.resignFirstResponder()
    }
    
}


extension generalInfoView : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        if(collectionView == self.collUserRole_frst){
            count = self.arrRole_frst.count
        }
        else if collectionView == self.collUserRole_sec {
            count = self.arrRole_sec.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = UICollectionViewCell()
        
        if(collectionView == self.collUserRole_frst){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkRoleGeneralCell", for: indexPath) as! checkRoleGeneralCell
            let dict = self.arrRole_frst[indexPath.item] as! NSDictionary
            cell.lblText.text = dict["title"] as? String
            // cell.btnSelect.tag = indexPath.item
            cell.btnSelect.addTarget(self, action: #selector(clickOnSelectbtn(_:)), for: .touchUpInside)
            
            let isSelect = dict["isSelect"] as! Bool
            cell.btnSelect.isSelected = isSelect
            
            
            return cell
        }
        else if collectionView == self.collUserRole_sec {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkRoleGeneralCell_sec", for: indexPath) as! checkRoleGeneralCell_sec
            let dict = self.arrRole_sec[indexPath.item] as! NSDictionary
            cell.lblText.text = dict["title"] as? String
            //
            
            cell.btnSelect.addTarget(self, action: #selector(clickOnSelectbtn_sec(_:)), for: .touchUpInside)
            cell.btnSelect.tag = indexPath.item
            
            let isSelect = dict["isSelect"] as? Bool
            if(isSelect == false){
                cell.btnSelect.isSelected = false
            }
            else{
                cell.btnSelect.isSelected = true
            }
            
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 155, height: 70)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    @objc func clickOnSelectbtn(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collUserRole_frst)
        let indexPath = self.collUserRole_frst.indexPathForItem(at: buttonPosition)
        var dict = arrRole_frst[indexPath!.item] as! [String: Any]
        let isSelect = dict["isSelect"] as? Bool
        
        if sender.isSelected == true {
            sender.isSelected = false
            
            if(isSelect == true)
            {
                
                let _ = dict.updateValue(false, forKey: "isSelect")
                self.arrRole_frst.replaceObject(at: indexPath!.item, with: dict)
                
                if(arrSelected_role.count == 0){
                    arrSelected_role = []
                }
                else{
                    arrSelected_role.remove(dict)
                }
            }
        }else {
            sender.isSelected = true
            if(isSelect == false){
                let _ = dict.updateValue(true, forKey: "isSelect")
                arrRole_frst.replaceObject(at: indexPath!.item, with: dict)
                
                
            }
        }
    }
    
    @objc func clickOnSelectbtn_sec(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collUserRole_sec)
        let indexPath = self.collUserRole_sec.indexPathForItem(at: buttonPosition)
        var dict = self.arrRole_sec[indexPath!.item] as! [String: Any]
        let isSelect = dict["isSelect"] as? Bool
        
        if sender.isSelected == true {
            sender.isSelected = false
            if(isSelect == true)
            {
                let _ = dict.updateValue(false, forKey: "isSelect")
                self.arrRole_sec.replaceObject(at: indexPath!.item, with: dict)
                if(arrSelected_role.count == 0){
                    arrSelected_role = []
                }
                else{
                    arrSelected_role.remove(dict)
                }
            }
        }else {
            sender.isSelected = true
            if(isSelect == false){
                let _ = dict.updateValue(true, forKey: "isSelect")
                arrSelected_role.add(dict)
                self.arrRole_sec.replaceObject(at: indexPath!.item, with: dict)
            }
        }
    }
}

class checkRoleGeneralCell: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
}

class checkRoleGeneralCell_sec: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
}


extension generalInfoView {
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please enter email.")
        }
            
        else if(txtFrstName.text == "" || txtFrstName.text?.count == 0 || txtFrstName.text == nil){
            txtFrstName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please enter your first name.")
        }
            
        else if(txtLastName.text == "" || txtLastName.text?.count == 0 || txtLastName.text == nil){
            txtLastName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please enter your last name.")
        }
        else if(txtWorkPhn.text == "" || txtWorkPhn.text?.count == 0 || txtWorkPhn.text == nil){
            txtWorkPhn.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please enter your work phone.")
        }
            
        else if(txtUSercode.text == "" || txtUSercode.text?.count == 0 || txtUSercode.text == nil){
            txtUSercode.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please enter user code.")
        }
            
        else if(txtTimeZone.text == "" || txtTimeZone.text?.count == 0 || txtTimeZone.text == nil){
            txtTimeZone.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select time zone.")
        }
        else if(txtDEfaultInstitution.text == "" || txtDEfaultInstitution.text?.count == 0 || txtDEfaultInstitution.text == nil){
            txtDEfaultInstitution.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please Select Institution.")
        }
            
            
            
            
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            self.callAddUserApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callAddUserApi()
    {
        var is_del = false
        var is_psswrd = false
        var is_receive = false
        var is_cannot = false
        
        for i in 0..<arrSelected_role.count{
            var dict = arrSelected_role[i] as! [String: Any]
            let _ = dict.removeValue(forKey: "isSelect")
            arrSelected_role.replaceObject(at: i, with: dict)
        }
        
        for i in 0..<arrRole_frst.count{
            let dict = arrRole_frst[i] as! [String: Any]
            
            if(i == 0){
                is_del = dict["isSelect"] as! Bool
                if(is_del == true){
                    is_del = false
                }
                else{
                    is_del = true
                }
                
            }
            
            if (i == 1){
                is_psswrd = dict["isSelect"] as! Bool
            }
            if (i == 2){
                is_receive = dict["isSelect"] as! Bool
            }
            if (i == 3){
                is_cannot = dict["isSelect"] as! Bool
            }
        }
        
        do {
            let decoded  = userDef.object(forKey: "passDict") as! Data
            if let passDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                
                if(isViewFrom == "Edit"){
                    
                    let _id = passDict["_id"] as! Int
                    let params = ["action": "user.detail.update",
                                  "email":self.txtEmail.text!,
                                  "timezone":timeZone,
                                  "user_code":self.txtUSercode.text!,
                                  "fname":self.txtFrstName.text!,
                                  "mname":self.txtMiddleName.text!,
                                  "lname":self.txtLastName.text!,
                                  "work_phone":self.txtWorkPhn.text!,
                                  "institute_id":instId,
                                  "is_deleted":is_del,   //bool from coll cell
                        "password_never_expire":is_psswrd,
                        "receive_system_message":is_receive,
                        "cannot_change_password":is_cannot,
                        "role_id":self.arrSelected_role,  //arr of role id
                        "salutation": self.txtSalutation.text!,
                        "suffix": self.txtsuffix.text!,
                        "title": self.txtTitle.text!,
                        "alt_email": self.txtAltEmail.text!,
                        "home_phone": self.txtHomePhn.text!,
                        "cell_phone": self.txtCellPhn.text!,
                        "other": self.txtOther.text!,
                        "fax": self.txtFax.text!,
                        "id":_id,
                        "insid": (user?.institute_id)!,
                        "userid":(user?._id)!] as [String : Any]
                    Globalfunc.print(object:params)
                    self.sendParamTopost(param: params, strTyp: "put")
                }
                    
                else if(isViewFrom == "Add"){
                    let params = [
                        "email":self.txtEmail.text!,
                        "timezone":timeZone,
                        "user_code":self.txtUSercode.text!,
                        "fname":self.txtFrstName.text!,
                        "mname":self.txtMiddleName.text!,
                        "lname":self.txtLastName.text!,
                        "work_phone":self.txtWorkPhn.text!,
                        "institute_id":instId,
                        "is_deleted":is_del,   //bool from coll cell
                        "password_never_expire":is_psswrd,
                        "receive_system_message":is_receive,
                        "cannot_change_password":is_cannot,
                        "role_id":self.arrSelected_role,  //arr of role id
                        "salutation": self.txtSalutation.text!,
                        "suffix": self.txtsuffix.text!,
                        "title": self.txtTitle.text!,
                        "alt_email": self.txtAltEmail.text!,
                        "home_phone": self.txtHomePhn.text!,
                        "cell_phone": self.txtCellPhn.text!,
                        "other": self.txtOther.text!,
                        "fax": self.txtFax.text!,
                        "insid": (user?.institute_id)!,
                        "userid":(user?._id)!] as [String : Any]
                    Globalfunc.print(object:params)
                    self.sendParamTopost(param: params, strTyp: "post")
                }
            }
        }
        catch{
        }
    }
    
    func sendParamTopost(param: [String : Any],strTyp: String)
    {
        if(strTyp == "put"){
            BaseApi.onResponsePutWithToken(url: Constant.update_user_pref, controller: self, parms: param as NSDictionary) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
                
            }
            
        }
        else if(strTyp == "post"){
            
            print(Constant.add_user_url)
            
            BaseApi.onResponsePostWithToken(url: Constant.add_user_url, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            print(arr)
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
}

