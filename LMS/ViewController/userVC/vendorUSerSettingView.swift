//
//  vendorUSerSettingView.swift
//  LMS
//
//  Created by Reinforce on 10/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class vendorUSerSettingView: UIViewController {
    
    @IBOutlet weak var txtSetting: UITextField!
    var pickerViewInstitution: UIPickerView!
    var instId : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickOnbackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}


extension vendorUSerSettingView : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerViewInstitution{
            return Constant.arrInstitutionData.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewInstitution){
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            return item
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewInstitution){
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtSetting.text = item
            self.instId = "\(_id)"
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtSetting){
            self.pickUp(txtSetting)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtSetting){
            self.pickerViewInstitution = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInstitution.delegate = self
            self.pickerViewInstitution.dataSource = self
            self.pickerViewInstitution.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInstitution
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editPasswordViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(editPasswordViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtSetting.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtSetting.resignFirstResponder()
    }
}
