//
//  usersViewController.swift
//  LMS
//
//  Created by Reinforce on 13/01/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit
var isViewFrom = ""

class usersViewController: baseViewController {
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var tblUserList : UITableView!
    
    @IBOutlet weak var txtFilter : UITextField!
    var pickerviewFilter : UIPickerView!
    
    @IBOutlet weak var viewFilter : UIView!
    
    var pickerFilterData : NSMutableArray = []
    
    
    
    var isSortTypeVal = ""
    var sortDirection = ""
    var is_status_filter = ""
    var isSelectedPicker = ""
    
    var arrUserList : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        self.tblUserList.tableFooterView = UIView()
        
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        
        let dict_1 = ["name":"Active","value":"false"]
        let dict_2 = ["name":"In Active","value":"true"]
        let dict_3 = ["name":"All Status","value":"all"]
        
        
        pickerFilterData.add(dict_1)
        pickerFilterData.add(dict_2)
        pickerFilterData.add(dict_3)
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.arrUserList = []
        self.tblUserList.isHidden = true
        self.lblStatus.isHidden = true
        
        self.viewFilter.isHidden = false
        
        isSortTypeVal = "fname"
        sortDirection = "asc"
        is_status_filter = "false"
        
        self.txtFilter.text = "Active"
        
        Globalfunc.showLoaderView(view: self.view)
        let param = ["sort":"\(isSortTypeVal)", "sortDirection":"\(sortDirection)", "status":"\(is_status_filter)"] as NSDictionary
        self.callUserListApiwithstatus(param: param)
        
    }
    
    
    
    func callUserListApiwithstatus(param: NSDictionary){
        
        BaseApi.onResponsePostWithToken(url: Constant.user_list_url, controller: self, parms: param) { (dict, err) in
            
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                
                if let response = dict as? NSDictionary {
                    if let arr = response["data"] as? [NSDictionary]{
                    if(arr.count > 0){
                        for i in 0..<arr.count{
                            let dictA = arr[i]
                            self.arrUserList.add(dictA)
                        }
                        self.tblUserList.isHidden = false
                        self.lblStatus.isHidden = true
                        self.tblUserList.reloadData()
                    }
                    else{
                        self.lblStatus.isHidden = false
                        self.tblUserList.isHidden = true
                        self.arrUserList = []
                    }
                }
                }
                if let arr = dict as? NSDictionary {
                    if let msg = arr["message"] as? String{
                        if(msg == "Your token has expired."){
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
}

extension usersViewController {
    
    @IBAction func clickOnMoreBtn(_ sender: UIButton)
    {
        
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.viewFilter.isHidden = true
                        
        }, completion: { (finished) -> Void in
            
        })
    }
    
    @IBAction func clickOnCloseViewBtn(_ sender: UIButton)
    {
        UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        
                        self.viewFilter.isHidden = false
                        
        }, completion: { (finished) -> Void in
            
        })
    }
    
    @IBAction func clikOnPlusBtn(_ sender: UIButton)
    {
        isViewFrom = "Add"
        self.presentViewControllerBasedOnIdentifier("generalInfoView", strStoryName: "SettingStory")
    }
}

extension usersViewController :  UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //        if(pickerView == pickerviewSort){
        //            return pickerSortData.count
        //        }
        if(pickerView == pickerviewFilter){
            return pickerFilterData.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        //        if(pickerView == pickerviewSort){
        //            let dict = self.pickerSortData[row] as! NSDictionary
        //            let strname = dict["name"] as! String
        //            return strname
        //        }
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            let strname = dict["name"] as! String
            return strname
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerviewFilter){
            let dict = self.pickerFilterData[row] as! NSDictionary
            self.txtFilter.text = (dict["name"] as! String)
            is_status_filter = dict["value"] as! String
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtFilter){
            self.pickUp(self.txtFilter)
        }
        
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtFilter){
            self.pickerviewFilter = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerviewFilter.delegate = self
            self.pickerviewFilter.dataSource = self
            self.pickerviewFilter.backgroundColor = UIColor.white
            textField.inputView = self.pickerviewFilter
            isSelectedPicker = "filter"
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        self.arrUserList = []
        Globalfunc.showLoaderView(view: self.view)
        txtFilter.resignFirstResponder()
        
        if (isSelectedPicker == "sort"){
            let param = ["sort":"\(isSortTypeVal)", "sortDirection":"\(sortDirection)", "status":"\(is_status_filter)"] as NSDictionary
            self.callUserListApiwithstatus(param: param)
        }
            
        else if (isSelectedPicker == "filter"){
            let param = ["sort":"\(isSortTypeVal)", "sortDirection":"\(sortDirection)", "status":"\(is_status_filter)"] as NSDictionary
            Globalfunc.print(object:param)
            self.callUserListApiwithstatus(param: param)
        }
    }
    
    @objc func cancelClick(){
        txtFilter.resignFirstResponder()
    }
}

extension usersViewController : UITableViewDataSource , UITableViewDelegate
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrUserList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblUserList.dequeueReusableCell(withIdentifier: "tblUserCell") as! tblUserCell
        let dict = arrUserList[indexPath.row] as! NSDictionary
        Globalfunc.print(object:dict)
        
        if let is_deleted = dict["is_deleted"] as? Bool{
            if(is_deleted == false){
                cell.lblActive.text = "True"
            }
            else{
                cell.lblActive.text = "False"
            }
        }
        
        let email = dict["email"] as! String
        cell.lblEmail.text = email
        
        let fname = dict["fname"] as! String
        cell.lblFname.text = fname
        
        let lname = dict["lname"] as! String
        cell.lblLname.text = lname
        
        if let title = dict["title"] as? String{
            cell.lblTitle.text = title
        }
        
        let user_code = dict["user_code"] as! String
        cell.lblUserCode.text = user_code
        
        let work_phone = dict["work_phone"] as! String
        cell.lblPhone.text = work_phone
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(clickONBtnEdit(_:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func clickONBtnEdit(_ sender: UIButton)
    {
        let dict = arrUserList[sender.tag] as! NSDictionary
        do {
            if #available(iOS 11.0, *) {
                let myData = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
                userDef.set(myData, forKey: "userDetail")
            } else {
                let myData = NSKeyedArchiver.archivedData(withRootObject: dict)
                userDef.set(myData, forKey: "userDetail")
            }
        } catch {
            Globalfunc.print(object: "Couldn't write file")
        }
        self.presentViewControllerasPopup("editPasswordViewController", strStoryName: "SettingStory")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrUserList[indexPath.row] as! NSDictionary
        do {
            if #available(iOS 11.0, *) {
                let myData = try NSKeyedArchiver.archivedData(withRootObject: dict, requiringSecureCoding: false)
                userDef.set(myData, forKey: "userDetail")
            } else {
                let myData = NSKeyedArchiver.archivedData(withRootObject: dict)
                userDef.set(myData, forKey: "userDetail")
            }
        } catch {
            Globalfunc.print(object: "Couldn't write file")
        }
        self.presentViewControllerBasedOnIdentifier("userDetailViewController", strStoryName: "SettingStory")
    }
}

class tblUserCell : UITableViewCell
{
    @IBOutlet weak var lblFname : UILabel!
    @IBOutlet weak var lblLname : UILabel!
    @IBOutlet weak var lblUserCode : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblPhone : UILabel!
    @IBOutlet weak var lblEmail : UILabel!
    @IBOutlet weak var lblActive : UILabel!
    @IBOutlet weak var btnEdit : UIButton!
}




