//
//  securityRightView.swift
//  LMS
//
//  Created by Reinforce on 10/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class securityRightView: UIViewController {
    
    @IBOutlet weak var txtSetting: UITextField!
    @IBOutlet weak var txtDefBranch: UITextField!
    @IBOutlet weak var txtSecurerole: UITextField!
    
    @IBOutlet weak var txtsundayStrtHr: UITextField!
    @IBOutlet weak var txtsundayStrtMin: UITextField!
    @IBOutlet weak var txtmondayStrtHr: UITextField!
    @IBOutlet weak var txtmondayStrtMin: UITextField!
    @IBOutlet weak var txtTuesStrtHr: UITextField!
    @IBOutlet weak var txtTuesStrtMin: UITextField!
    @IBOutlet weak var txtWedStrtHr: UITextField!
    @IBOutlet weak var txtWedStrtMin: UITextField!
    @IBOutlet weak var txtThursStrtHr: UITextField!
    @IBOutlet weak var txtThursStrtMin: UITextField!
    @IBOutlet weak var txtfridayStrtHr: UITextField!
    @IBOutlet weak var txtfridayStrtMin: UITextField!
    @IBOutlet weak var txtSatStrtHr: UITextField!
    @IBOutlet weak var txtSatStrtMin: UITextField!
    
    var pickersundayStrtHr: UIPickerView!
    var pickersundayStrtMin: UIPickerView!
    var pickermondayStrtHr: UIPickerView!
    var pickermondayStrtMin: UIPickerView!
    var pickerTuesStrtHr: UIPickerView!
    var pickerTuesStrtMin: UIPickerView!
    var pickerWedStrtHr: UIPickerView!
    var pickerWedStrtMin: UIPickerView!
    var pickerThursStrtHr: UIPickerView!
    var pickerThursStrtMin: UIPickerView!
    var pickerfridayStrtHr: UIPickerView!
    var pickerfridayStrtMin: UIPickerView!
    var pickerSatStrtHr: UIPickerView!
    var pickerSatStrtMin: UIPickerView!

    
    @IBOutlet weak var txtsundayEndHr: UITextField!
    @IBOutlet weak var txtsundayEndMin: UITextField!
    @IBOutlet weak var txtmondayEndHr: UITextField!
    @IBOutlet weak var txtmondayEndMin: UITextField!
    @IBOutlet weak var txtTuesEndHr: UITextField!
    @IBOutlet weak var txtTuesEndMin: UITextField!
    @IBOutlet weak var txtWedEndHr: UITextField!
    @IBOutlet weak var txtWedEndMin: UITextField!
    @IBOutlet weak var txtThursEndHr: UITextField!
    @IBOutlet weak var txtThursEndMin: UITextField!
    @IBOutlet weak var txtfridayEndHr: UITextField!
    @IBOutlet weak var txtfridayEndMin: UITextField!
    @IBOutlet weak var txtSatEndHr: UITextField!
    @IBOutlet weak var txtSatEndMin: UITextField!
    
    var pickersundayEndHr: UIPickerView!
    var pickersundayEndMin: UIPickerView!
    var pickermondayEndHr: UIPickerView!
    var pickermondayEndMin: UIPickerView!
    var pickerTuesEndHr: UIPickerView!
    var pickerTuesEndMin: UIPickerView!
    var pickerWedEndHr: UIPickerView!
    var pickerWedEndMin: UIPickerView!
    var pickerThursEndHr: UIPickerView!
    var pickerThursEndMin: UIPickerView!
    var pickerfridayEndHr: UIPickerView!
    var pickerfridayEndMin: UIPickerView!
    var pickerSatEndHr: UIPickerView!
    var pickerSatEndMin: UIPickerView!

    var pickerViewInstitution: UIPickerView!
    var pickerViewDefaultBranch: UIPickerView!
    
    var pickerViewSecurity: UIPickerView!
    var arrSecurity = ["Custom","Bookkeeper","Cashier","Collection","Collection Mgr","F&I Sales","F&I Sales Mgr","Full Access"]
    
    var pickerStartTimeHr: UIPickerView!
    var pickerStartTimeMin: UIPickerView!
    
    var pickerEndTimeHr: UIPickerView!
    var pickerEndTimeMin: UIPickerView!

    var channelId : NSNumber?
    var instId : String?
    
    var isEdit = ""
    
    var btn_canview = false
    var btn_canbook = false
    var btn_canedit = false
    var btn_calculator = false
    var btn_user = false
    
    var arrPemissionRight : [NSDictionary] = []

    var dictSecurity : NSDictionary = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "userDetail") as! Data
            if let usedict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let _id = usedict["_id"] as! NSNumber
                let url = "\(Constant.security_rights)/id?userid=\(_id)"
                self.callSecurityRightApi(strUrl: url)
            }
        }
        catch{
            
        }
    }

    @IBAction func clickOnbackBtn(_ sender: UIButton)
    {
          self.dismiss(animated: true, completion: nil)
    }
}

extension securityRightView {
    
    func callSecurityRightApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        self.isEdit = "true"
                        self.dictSecurity = arr
                        
                        let institute_id = self.dictSecurity["institute_id"] as! String
                        for i in 0..<Constant.arrInstitutionData.count{
                            let defDict = Constant.arrInstitutionData[i] as! NSDictionary
                            let _id = defDict["_id"] as! NSNumber
                            if(institute_id == "\(_id)"){
                                let title = defDict["code"] as! String
                                self.instId = "\(_id)"
                                self.txtSetting.text = title
                                break
                            }
                        }
                        
                        if let security_rights = self.dictSecurity["security_rights"] as? [NSDictionary]{
                            self.arrPemissionRight = security_rights
                        }
                        
                        let security_role = self.dictSecurity["security_role"] as! String
                        self.txtSecurerole.text = security_role
                        
                        if let channel = self.dictSecurity["channel"] as? NSNumber{
                            for i in 0..<consArrays.arrDefaultBranch.count{
                                let defDict = consArrays.arrDefaultBranch[i] as! NSDictionary
                                let _id = defDict["_id"] as! NSNumber
                                if(channel == _id){
                                    let title = defDict["title"] as! String
                                    self.channelId = _id
                                    self.txtDefBranch.text = title
                                    break
                                }
                            }
                        }
                        
                        if let loginsDay = self.dictSecurity["loginsDay"] as? [NSDictionary]{
                            
                            for i in 0..<loginsDay.count{
                                
                                let dict = loginsDay[i]
                                let day = dict["day"] as! String
                                
                                if(day == "Sunday"){
                                    self.txtsundayEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtsundayEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtsundayStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtsundayStrtMin.text = "\(dict["starting_time2"] as! Int)"
                                }
                                else if(day == "Monday"){
                                    self.txtmondayEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtmondayEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtmondayStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtmondayStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                else if(day == "Tuesday"){
                                    self.txtTuesEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtTuesEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtTuesStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtTuesStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                else if(day == "Wednesday"){
                                    self.txtWedEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtWedEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtWedStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtWedStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                else if(day == "Thursday"){
                                    self.txtThursEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtThursEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtThursStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtThursStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                else if(day == "Friday"){
                                    self.txtfridayEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtfridayEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtfridayStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtfridayStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                else if(day == "Saturday"){
                                    self.txtSatEndHr.text = "\(dict["ending_time1"] as! Int)"
                                    self.txtSatEndMin.text = "\(dict["ending_time2"] as! Int)"
                                    self.txtSatStrtHr.text = "\(dict["starting_time1"] as! Int)"
                                    self.txtSatStrtMin.text = "\(dict["starting_time2"] as! Int)"

                                }
                                
                            }
                            
                        }
                    }
                    else{
                       self.isEdit = ""
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension securityRightView : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerViewInstitution{
            return Constant.arrInstitutionData.count
        }
        else if(pickerView == pickerViewDefaultBranch){
            return consArrays.arrDefaultBranch.count
        }
        else if(pickerView == pickerViewSecurity){
            return self.arrSecurity.count
        }
       
        else if(pickerView == pickersundayStrtHr || pickerView == pickermondayStrtHr || pickerView == pickerTuesStrtHr || pickerView == pickerWedStrtHr || pickerView == pickerThursStrtHr ||  pickerView == pickerfridayStrtHr || pickerView == pickerSatStrtHr)
        {
            return consArrays.arrHour.count
        }
        else if(pickerView == pickersundayStrtMin || pickerView == pickermondayStrtMin || pickerView == pickerTuesStrtMin || pickerView == pickerWedStrtMin || pickerView == pickerThursStrtMin ||  pickerView == pickerfridayStrtMin || pickerView == pickerSatStrtMin)
        {
            return consArrays.arrMin.count
        }
         
        else if(pickerView == pickersundayEndHr || pickerView == pickermondayEndHr || pickerView == pickerTuesEndHr || pickerView == pickerWedEndHr || pickerView == pickerThursEndHr ||  pickerView == pickerfridayEndHr || pickerView == pickerSatEndHr)
        {
                return consArrays.arrHour.count
        }
        else if(pickerView == pickersundayEndMin || pickerView == pickermondayEndMin || pickerView == pickerTuesEndMin || pickerView == pickerWedEndMin || pickerView == pickerThursEndMin ||  pickerView == pickerfridayEndMin || pickerView == pickerSatEndMin)
        {
                return consArrays.arrMin.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewInstitution){
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            return item
        }
        else if(pickerView == pickerViewDefaultBranch){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        else if(pickerView == pickerViewSecurity){
            let item = self.arrSecurity[row]
            return item
        }
        
        else if(pickerView == pickersundayStrtHr || pickerView == pickermondayStrtHr || pickerView == pickerTuesStrtHr || pickerView == pickerWedStrtHr || pickerView == pickerThursStrtHr ||  pickerView == pickerfridayStrtHr || pickerView == pickerSatStrtHr)
        {
                 let item = consArrays.arrHour[row]
                 return item
        }
        else if(pickerView == pickersundayStrtMin || pickerView == pickermondayStrtMin || pickerView == pickerTuesStrtMin || pickerView == pickerWedStrtMin || pickerView == pickerThursStrtMin ||  pickerView == pickerfridayStrtMin || pickerView == pickerSatStrtMin)
        {
            let item = consArrays.arrMin[row]
            return item
        }

        else if(pickerView == pickersundayEndHr || pickerView == pickermondayEndHr || pickerView == pickerTuesEndHr || pickerView == pickerWedEndHr || pickerView == pickerThursEndHr ||  pickerView == pickerfridayEndHr || pickerView == pickerSatEndHr)
            {
                let item = consArrays.arrHour[row]
                return item
            }
        else if(pickerView == pickersundayEndMin || pickerView == pickermondayEndMin || pickerView == pickerTuesEndMin || pickerView == pickerWedEndMin || pickerView == pickerThursEndMin ||  pickerView == pickerfridayEndMin || pickerView == pickerSatEndMin)
            {
                let item = consArrays.arrMin[row]
                return item
            }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewInstitution){
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtSetting.text = item
            self.instId = "\(_id)"
        }
        else if(pickerView == pickerViewDefaultBranch){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let _id = dict["_id"] as! NSNumber
            let item = dict["title"] as! String
            self.channelId = _id
            self.txtDefBranch.text = item
        }
        else if(pickerView == pickerViewSecurity){
            let item = self.arrSecurity[row]
            self.txtSecurerole.text = item
        }
        
        else if(pickerView == pickersundayStrtHr || pickerView == pickermondayStrtHr || pickerView == pickerTuesStrtHr || pickerView == pickerWedStrtHr || pickerView == pickerThursStrtHr ||  pickerView == pickerfridayStrtHr || pickerView == pickerSatStrtHr)
        {
            let item = consArrays.arrHour[row]
                 
            if(pickerView == pickersundayStrtHr){
                self.txtsundayStrtHr.text = item
            }
            else if(pickerView == pickermondayStrtHr){
                self.txtmondayStrtHr.text = item
            }
            else if(pickerView == pickerTuesStrtHr){
                self.txtTuesStrtHr.text = item
            }
            else if(pickerView == pickerWedStrtHr){
                self.txtWedStrtHr.text = item
            }
            else if(pickerView == pickerThursStrtHr){
                self.txtThursStrtHr.text = item
            }
            else if(pickerView == pickerfridayStrtHr){
                self.txtfridayStrtHr.text = item
            }
            else if(pickerView == pickerSatStrtHr){
                self.txtSatStrtHr.text = item
            }
        }
        
        else if(pickerView == pickersundayStrtMin || pickerView == pickermondayStrtMin || pickerView == pickerTuesStrtMin || pickerView == pickerWedStrtMin || pickerView == pickerThursStrtMin ||  pickerView == pickerfridayStrtMin || pickerView == pickerSatStrtMin)
        {
            let item = consArrays.arrMin[row]
            
            if(pickerView == pickersundayStrtMin){
                self.txtsundayStrtMin.text = item
            }
            else if(pickerView == pickermondayStrtMin){
                self.txtmondayStrtMin.text = item
            }
            else if(pickerView == pickerTuesStrtMin){
                self.txtTuesStrtMin.text = item
            }
            else if(pickerView == pickerWedStrtMin){
                self.txtWedStrtMin.text = item
            }

            else if(pickerView == pickerThursStrtMin){
                self.txtThursStrtMin.text = item
            }

            else if(pickerView == pickerfridayStrtMin){
                self.txtfridayStrtMin.text = item
            }
            else if(pickerView == pickerSatStrtMin){
                self.txtSatStrtMin.text = item
            }
        }
        else if(pickerView == pickersundayEndHr || pickerView == pickermondayEndHr || pickerView == pickerTuesEndHr || pickerView == pickerWedEndHr || pickerView == pickerThursEndHr ||  pickerView == pickerfridayEndHr || pickerView == pickerSatEndHr)
        {
                let item = consArrays.arrHour[row]
                if(pickerView == pickersundayEndHr){
                    self.txtsundayEndHr.text = item
                }
                else if(pickerView == pickermondayEndHr){
                    self.txtmondayEndHr.text = item
                }
                else if(pickerView == pickerTuesEndHr){
                    self.txtTuesEndHr.text = item
                }
                else if(pickerView == pickerWedEndHr){
                    self.txtWedEndHr.text = item
                }
                else if(pickerView == pickerThursEndHr){
                    self.txtThursEndHr.text = item
                }
                else if(pickerView == pickerfridayEndHr){
                    self.txtfridayEndHr.text = item
                }
                else if(pickerView == pickerSatEndHr){
                    self.txtSatEndHr.text = item
                }
        }
        else if(pickerView == pickersundayEndMin || pickerView == pickermondayEndMin || pickerView == pickerTuesEndMin || pickerView == pickerWedEndMin || pickerView == pickerThursEndMin ||  pickerView == pickerfridayEndMin || pickerView == pickerSatEndMin)
        {
                let item = consArrays.arrMin[row]
                
                if(pickerView == pickersundayEndMin){
                    self.txtsundayEndMin.text = item
                }
                else if(pickerView == pickermondayEndMin){
                    self.txtmondayEndMin.text = item
                }
                else if(pickerView == pickerTuesEndMin){
                    self.txtTuesEndMin.text = item
                }
                else if(pickerView == pickerWedEndMin){
                    self.txtWedEndMin.text = item
                }
                else if(pickerView == pickerThursEndMin){
                    self.txtThursEndMin.text = item
                }
                else if(pickerView == pickerfridayEndMin){
                    self.txtfridayEndMin.text = item
                }
                else if(pickerView == pickerSatEndMin){
                    self.txtSatEndMin.text = item
                }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtSetting){
            self.pickUp(txtSetting)
        }
        else if(textField == self.txtDefBranch){
            self.pickUp(txtDefBranch)
        }
        else if(textField == self.txtSecurerole){
            self.pickUp(txtSecurerole)
        }
        else if(textField == self.txtsundayStrtHr){
            self.pickUp(txtsundayStrtHr)
        }
        else if(textField == self.txtmondayStrtHr){
            self.pickUp(txtmondayStrtHr)
        }
        else if(textField == self.txtTuesStrtHr){
            self.pickUp(txtTuesStrtHr)
        }
        else if(textField == self.txtWedStrtHr){
            self.pickUp(txtWedStrtHr)
        }
        else if(textField == self.txtThursStrtHr){
            self.pickUp(txtThursStrtHr)
        }
        else if(textField == self.txtfridayStrtHr){
            self.pickUp(txtfridayStrtHr)
        }
        else if(textField == self.txtSatStrtHr){
            self.pickUp(txtSatStrtHr)
        }
        else if(textField == self.txtsundayStrtMin){
            self.pickUp(txtsundayStrtMin)
        }
        else if(textField == self.txtmondayStrtMin){
            self.pickUp(txtmondayStrtMin)
        }
        else if(textField == self.txtTuesStrtMin){
            self.pickUp(txtTuesStrtMin)
        }
        else if(textField == self.txtWedStrtMin){
            self.pickUp(txtWedStrtMin)
        }
        else if(textField == self.txtThursStrtMin){
            self.pickUp(txtThursStrtMin)
        }
        else if(textField == self.txtfridayStrtMin){
            self.pickUp(txtfridayStrtMin)
        }
        else if(textField == self.txtSatStrtMin){
            self.pickUp(txtSatStrtMin)
        }
        else if(textField == self.txtsundayEndHr){
            self.pickUp(txtsundayEndHr)
        }
        else if(textField == self.txtmondayEndHr){
            self.pickUp(txtmondayEndHr)
        }
        else if(textField == self.txtTuesEndHr){
            self.pickUp(txtTuesEndHr)
        }
        else if(textField == self.txtWedEndHr){
            self.pickUp(txtWedEndHr)
        }
        else if(textField == self.txtThursEndHr){
            self.pickUp(txtThursEndHr)
        }
        else if(textField == self.txtfridayEndHr){
            self.pickUp(txtfridayEndHr)
        }
        else if(textField == self.txtSatEndHr){
            self.pickUp(txtSatEndHr)
        }
        else if(textField == self.txtsundayEndMin){
            self.pickUp(txtsundayEndMin)
        }
        else if(textField == self.txtmondayEndMin){
            self.pickUp(txtmondayEndMin)
        }
        else if(textField == self.txtTuesEndMin){
            self.pickUp(txtTuesEndMin)
        }
        else if(textField == self.txtWedEndMin){
            self.pickUp(txtWedEndMin)
        }
        else if(textField == self.txtThursEndMin){
            self.pickUp(txtThursEndMin)
        }
        else if(textField == self.txtfridayEndMin){
            self.pickUp(txtfridayEndMin)
        }
        else if(textField == self.txtSatEndMin){
            self.pickUp(txtSatEndMin)
        }
    }
    
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtSetting){
            self.pickerViewInstitution = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInstitution.delegate = self
            self.pickerViewInstitution.dataSource = self
            self.pickerViewInstitution.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInstitution
         }
        else if(textField == self.txtDefBranch){
            self.pickerViewDefaultBranch = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewDefaultBranch.delegate = self
            self.pickerViewDefaultBranch.dataSource = self
            self.pickerViewDefaultBranch.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewDefaultBranch
        }
        else if(textField == self.txtSecurerole){
            self.pickerViewSecurity = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewSecurity.delegate = self
            self.pickerViewSecurity.dataSource = self
            self.pickerViewSecurity.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewSecurity
        }
            
        else if(textField == self.txtsundayStrtHr){
            self.pickersundayStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersundayStrtHr.delegate = self
            self.pickersundayStrtHr.dataSource = self
            self.pickersundayStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickersundayStrtHr
        }
        
        else if(textField == self.txtmondayStrtHr){
            self.pickermondayStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickermondayStrtHr.delegate = self
            self.pickermondayStrtHr.dataSource = self
            self.pickermondayStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickermondayStrtHr
        }
        
        else if(textField == self.txtTuesStrtHr){
            self.pickerTuesStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTuesStrtHr.delegate = self
            self.pickerTuesStrtHr.dataSource = self
            self.pickerTuesStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerTuesStrtHr
        }

        else if(textField == self.txtWedStrtHr){
            self.pickerWedStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWedStrtHr.delegate = self
            self.pickerWedStrtHr.dataSource = self
            self.pickerWedStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerWedStrtHr
        }

        else if(textField == self.txtThursStrtHr){
            self.pickerThursStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerThursStrtHr.delegate = self
            self.pickerThursStrtHr.dataSource = self
            self.pickerThursStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerThursStrtHr
        }

        else if(textField == self.txtfridayStrtHr){
            self.pickerfridayStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerfridayStrtHr.delegate = self
            self.pickerfridayStrtHr.dataSource = self
            self.pickerfridayStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerfridayStrtHr
        }

        else if(textField == self.txtSatStrtHr){
            self.pickerSatStrtHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSatStrtHr.delegate = self
            self.pickerSatStrtHr.dataSource = self
            self.pickerSatStrtHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerSatStrtHr
        }
        
        else if(textField == self.txtsundayStrtMin){
            self.pickersundayStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersundayStrtMin.delegate = self
            self.pickersundayStrtMin.dataSource = self
            self.pickersundayStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickersundayStrtMin
        }
        
        else if(textField == self.txtmondayStrtMin){
            self.pickermondayStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickermondayStrtMin.delegate = self
            self.pickermondayStrtMin.dataSource = self
            self.pickermondayStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickermondayStrtMin
        }
        
        else if(textField == self.txtTuesStrtMin){
            self.pickerTuesStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTuesStrtMin.delegate = self
            self.pickerTuesStrtMin.dataSource = self
            self.pickerTuesStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerTuesStrtMin
        }

        else if(textField == self.txtWedStrtMin){
            self.pickerWedStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWedStrtMin.delegate = self
            self.pickerWedStrtMin.dataSource = self
            self.pickerWedStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerWedStrtMin
        }

        else if(textField == self.txtThursStrtMin){
            self.pickerThursStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerThursStrtMin.delegate = self
            self.pickerThursStrtMin.dataSource = self
            self.pickerThursStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerThursStrtMin
        }

        else if(textField == self.txtfridayStrtMin){
            self.pickerfridayStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerfridayStrtMin.delegate = self
            self.pickerfridayStrtMin.dataSource = self
            self.pickerfridayStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerfridayStrtMin
        }

        else if(textField == self.txtSatStrtMin){
            self.pickerSatStrtMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSatStrtMin.delegate = self
            self.pickerSatStrtMin.dataSource = self
            self.pickerSatStrtMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerSatStrtMin
        }
        else if(textField == self.txtsundayEndHr){
            self.pickersundayEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersundayEndHr.delegate = self
            self.pickersundayEndHr.dataSource = self
            self.pickersundayEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickersundayEndHr
        }
        
        else if(textField == self.txtmondayEndHr){
            self.pickermondayEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickermondayEndHr.delegate = self
            self.pickermondayEndHr.dataSource = self
            self.pickermondayEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickermondayEndHr
        }
        
        else if(textField == self.txtTuesEndHr){
            self.pickerTuesEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTuesEndHr.delegate = self
            self.pickerTuesEndHr.dataSource = self
            self.pickerTuesEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerTuesEndHr
        }

        else if(textField == self.txtWedEndHr){
            self.pickerWedEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWedEndHr.delegate = self
            self.pickerWedEndHr.dataSource = self
            self.pickerWedEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerWedEndHr
        }

        else if(textField == self.txtThursEndHr){
            self.pickerThursEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerThursEndHr.delegate = self
            self.pickerThursEndHr.dataSource = self
            self.pickerThursEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerThursEndHr
        }

        else if(textField == self.txtfridayEndHr){
            self.pickerfridayEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerfridayEndHr.delegate = self
            self.pickerfridayEndHr.dataSource = self
            self.pickerfridayEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerfridayEndHr
        }

        else if(textField == self.txtSatEndHr){
            self.pickerSatEndHr = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSatEndHr.delegate = self
            self.pickerSatEndHr.dataSource = self
            self.pickerSatEndHr.backgroundColor = UIColor.white
            textField.inputView = self.pickerSatEndHr
        }
        
        else if(textField == self.txtsundayEndMin){
            self.pickersundayEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickersundayEndMin.delegate = self
            self.pickersundayEndMin.dataSource = self
            self.pickersundayEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickersundayEndMin
        }
        
        else if(textField == self.txtmondayEndMin){
            self.pickermondayEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickermondayEndMin.delegate = self
            self.pickermondayEndMin.dataSource = self
            self.pickermondayEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickermondayEndMin
        }
        
        else if(textField == self.txtTuesEndMin){
            self.pickerTuesEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerTuesEndMin.delegate = self
            self.pickerTuesEndMin.dataSource = self
            self.pickerTuesEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerTuesEndMin
        }

        else if(textField == self.txtWedEndMin){
            self.pickerWedEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerWedEndMin.delegate = self
            self.pickerWedEndMin.dataSource = self
            self.pickerWedEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerWedEndMin
        }

        else if(textField == self.txtThursEndMin){
            self.pickerThursEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerThursEndMin.delegate = self
            self.pickerThursEndMin.dataSource = self
            self.pickerThursEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerThursEndMin
        }

        else if(textField == self.txtfridayEndMin){
            self.pickerfridayEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerfridayEndMin.delegate = self
            self.pickerfridayEndMin.dataSource = self
            self.pickerfridayEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerfridayEndMin
        }

        else if(textField == self.txtSatEndMin){
            self.pickerSatEndMin = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerSatEndMin.delegate = self
            self.pickerSatEndMin.dataSource = self
            self.pickerSatEndMin.backgroundColor = UIColor.white
            textField.inputView = self.pickerSatEndMin
        }


        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editPasswordViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(editPasswordViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtDefBranch.resignFirstResponder()
        txtSetting.resignFirstResponder()
        txtSecurerole.resignFirstResponder()
            
        txtsundayStrtHr.resignFirstResponder()
        txtmondayStrtHr.resignFirstResponder()
        txtTuesStrtHr.resignFirstResponder()
        txtWedStrtHr.resignFirstResponder()
        txtThursStrtHr.resignFirstResponder()
        txtfridayStrtHr.resignFirstResponder()
        txtSatStrtHr.resignFirstResponder()
        txtsundayStrtMin.resignFirstResponder()
        txtsundayStrtMin.resignFirstResponder()
        txtTuesStrtMin.resignFirstResponder()
        txtWedStrtMin.resignFirstResponder()
        txtThursStrtMin.resignFirstResponder()
        txtfridayStrtMin.resignFirstResponder()
        txtSatStrtMin.resignFirstResponder()
        
        txtsundayEndHr.resignFirstResponder()
        txtmondayEndHr.resignFirstResponder()
        txtTuesEndHr.resignFirstResponder()
        txtWedEndHr.resignFirstResponder()
        txtThursEndHr.resignFirstResponder()
        txtfridayEndHr.resignFirstResponder()
        txtSatEndHr.resignFirstResponder()
        txtsundayEndMin.resignFirstResponder()
        txtsundayEndMin.resignFirstResponder()
        txtTuesEndMin.resignFirstResponder()
        txtWedEndMin.resignFirstResponder()
        txtThursEndMin.resignFirstResponder()
        txtfridayEndMin.resignFirstResponder()
        txtSatEndMin.resignFirstResponder()


    }
    
    @objc func cancelClick() {
        txtDefBranch.resignFirstResponder()
        txtSetting.resignFirstResponder()
        txtSecurerole.resignFirstResponder()
        
        txtsundayStrtHr.resignFirstResponder()
        txtmondayStrtHr.resignFirstResponder()
        txtTuesStrtHr.resignFirstResponder()
        txtWedStrtHr.resignFirstResponder()
        txtThursStrtHr.resignFirstResponder()
        txtfridayStrtHr.resignFirstResponder()
        txtSatStrtHr.resignFirstResponder()
        txtsundayStrtMin.resignFirstResponder()
        txtmondayStrtMin.resignFirstResponder()
        txtTuesStrtMin.resignFirstResponder()
        txtWedStrtMin.resignFirstResponder()
        txtThursStrtMin.resignFirstResponder()
        txtfridayStrtMin.resignFirstResponder()
        txtSatStrtMin.resignFirstResponder()
        
        txtsundayEndHr.resignFirstResponder()
        txtmondayEndHr.resignFirstResponder()
        txtTuesEndHr.resignFirstResponder()
        txtWedEndHr.resignFirstResponder()
        txtThursEndHr.resignFirstResponder()
        txtfridayEndHr.resignFirstResponder()
        txtSatEndHr.resignFirstResponder()
        txtsundayEndMin.resignFirstResponder()
        txtmondayEndMin.resignFirstResponder()
        txtTuesEndMin.resignFirstResponder()
        txtWedEndMin.resignFirstResponder()
        txtThursEndMin.resignFirstResponder()
        txtfridayEndMin.resignFirstResponder()
        txtSatEndMin.resignFirstResponder()

    }
}

extension securityRightView {
    
    @IBAction func clikcOnSavebtn(_ sender: UIButton)
    {
        if(self.isEdit == ""){ //post
            do {
            let decoded  = userDef.object(forKey: "passDict") as! Data
            if let passDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let _id = passDict["_id"] as! Int
                
                let params = ["can_be_assigned_coll_queues": "false",
                              "channel": self.channelId!,
                              "day1": "Sunday",
                              "day1_ending_time1": self.txtsundayEndHr.text!,
                              "day1_ending_time2": self.txtsundayEndMin.text!,
                              "day1_starting_time1": self.txtsundayStrtHr.text!,
                              "day1_starting_time2": self.txtsundayStrtMin.text!,
                              "day2": "Monday",
                              "day2_ending_time1": self.txtmondayEndHr.text!,
                              "day2_ending_time2": self.txtmondayEndMin.text!,
                              "day2_starting_time1": self.txtmondayStrtHr.text!,
                              "day2_starting_time2": self.txtmondayStrtMin.text!,
                              "day3": "Tuesday",
                              "day3_ending_time1": self.txtTuesEndHr.text!,
                              "day3_ending_time2": self.txtTuesEndMin.text!,
                              "day3_starting_time1": self.txtTuesStrtHr.text!,
                              "day3_starting_time2": self.txtTuesStrtMin.text!,
                              "day4": "Wednesday",
                              "day4_ending_time1": self.txtWedEndHr.text!,
                              "day4_ending_time2": self.txtWedEndMin.text!,
                              "day4_starting_time1": self.txtWedStrtHr.text!,
                              "day4_starting_time2": self.txtWedStrtMin.text!,
                              "day5": "Thursday",
                              "day5_ending_time1": self.txtThursEndHr.text!,
                              "day5_ending_time2": self.txtThursEndMin.text!,
                              "day5_starting_time1": self.txtThursStrtHr.text!,
                              "day5_starting_time2": self.txtThursStrtMin.text!,
                              "day6": "Friday",
                              "day6_ending_time1": self.txtfridayEndHr.text!,
                              "day6_ending_time2": self.txtfridayEndHr.text!,
                              "day6_starting_time1": self.txtfridayEndHr.text!,
                              "day6_starting_time2": self.txtfridayEndHr.text!,
                              "day7": "Saturday",
                              "day7_ending_time1": self.txtSatEndHr.text!,
                              "day7_ending_time2": self.txtSatEndHr.text!,
                              "day7_starting_time1": self.txtSatEndHr.text!,
                              "day7_starting_time2": self.txtSatEndHr.text!,
                              "security_rights": self.arrPemissionRight,
                              "institute_id":self.instId!,
                              "security_role": self.txtSecurerole.text!,
                    "user_id":_id,
                    "id":"",
                    "insid": (user?.institute_id)!,
                    "userid":(user?._id)!] as [String : Any]
                
                Globalfunc.print(object:params)
                
                self.sendParamTopost(param: params, strTyp: "post")
                
                }
            }
            catch{
            }
            
        }
        else if(self.isEdit == "true"){ //put
            do {
            let decoded  = userDef.object(forKey: "passDict") as! Data
            if let passDict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let _id = passDict["_id"] as! Int
                let sec_id = self.dictSecurity["_id"] as! Int
                let params = ["can_be_assigned_coll_queues": "false",
                              "channel": self.channelId!,
                              "day1": "Sunday",
                              "day1_ending_time1": self.txtsundayEndHr.text!,
                              "day1_ending_time2": self.txtsundayEndMin.text!,
                              "day1_starting_time1": self.txtsundayStrtHr.text!,
                              "day1_starting_time2": self.txtsundayStrtMin.text!,
                              "day2": "Monday",
                              "day2_ending_time1": self.txtmondayEndHr.text!,
                              "day2_ending_time2": self.txtmondayEndMin.text!,
                              "day2_starting_time1": self.txtmondayStrtHr.text!,
                              "day2_starting_time2": self.txtmondayStrtMin.text!,
                              "day3": "Tuesday",
                              "day3_ending_time1": self.txtTuesEndHr.text!,
                              "day3_ending_time2": self.txtTuesEndMin.text!,
                              "day3_starting_time1": self.txtTuesStrtHr.text!,
                              "day3_starting_time2": self.txtTuesStrtMin.text!,
                              "day4": "Wednesday",
                              "day4_ending_time1": self.txtWedEndHr.text!,
                              "day4_ending_time2": self.txtWedEndMin.text!,
                              "day4_starting_time1": self.txtWedStrtHr.text!,
                              "day4_starting_time2": self.txtWedStrtMin.text!,
                              "day5": "Thursday",
                              "day5_ending_time1": self.txtThursEndHr.text!,
                              "day5_ending_time2": self.txtThursEndMin.text!,
                              "day5_starting_time1": self.txtThursStrtHr.text!,
                              "day5_starting_time2": self.txtThursStrtMin.text!,
                              "day6": "Friday",
                              "day6_ending_time1": self.txtfridayEndHr.text!,
                              "day6_ending_time2": self.txtfridayEndHr.text!,
                              "day6_starting_time1": self.txtfridayEndHr.text!,
                              "day6_starting_time2": self.txtfridayEndHr.text!,
                              "day7": "Saturday",
                              "day7_ending_time1": self.txtSatEndHr.text!,
                              "day7_ending_time2": self.txtSatEndHr.text!,
                              "day7_starting_time1": self.txtSatEndHr.text!,
                              "day7_starting_time2": self.txtSatEndHr.text!,
                              "security_rights": self.arrPemissionRight,
                              "institute_id":self.instId!,
                              "security_role": self.txtSecurerole.text!,
                    "user_id":_id,
                    "id":sec_id,
                    "insid": (user?.institute_id)!,
                    "userid":(user?._id)!] as [String : Any]
                
                Globalfunc.print(object:params)
                
                self.sendParamTopost(param: params, strTyp: "post")
                
                }
            }
            catch{
            }

            
        }
        


    }
    
    func sendParamTopost(param: [String : Any], strTyp : String)
    {
        BaseApi.onResponsePostWithToken(url: Constant.security_rights, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//table for permssion right

