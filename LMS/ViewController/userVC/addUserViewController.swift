//
//  addUserViewController.swift
//  LMS
//
//  Created by Reinforce on 04/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class addUserViewController: UIViewController {
    
    @IBOutlet weak var collUserRole_frst: UICollectionView!
    @IBOutlet weak var collUserRole_sec: UICollectionView!
    
     
    //textfields
    
     @IBOutlet weak var txtEmail: UITextField!
     @IBOutlet weak var txtUSercode: UITextField!
     @IBOutlet weak var txtTimeZone: UITextField!
     @IBOutlet weak var txtDEfaultInstitution: UITextField!
     @IBOutlet weak var txtFrstName: UITextField!
     @IBOutlet weak var txtMiddleName: UITextField!
     @IBOutlet weak var txtLastName: UITextField!
     @IBOutlet weak var txtSalutation: UITextField!
     @IBOutlet weak var txtsuffix: UITextField!
     @IBOutlet weak var txtTitle: UITextField!
     @IBOutlet weak var txtAltEmail: UITextField!
     @IBOutlet weak var txtHomePhn: UITextField!
     @IBOutlet weak var txtWorkPhn: UITextField!
     @IBOutlet weak var txtCellPhn: UITextField!
     @IBOutlet weak var txtOther: UITextField!
     @IBOutlet weak var txtFax: UITextField!
    
    var pickerViewTimeZone: UIPickerView!
    var pickerViewSalutation: UIPickerView!
    var pickerViewSuffix: UIPickerView!
    var pickerViewInstitution: UIPickerView!
    
    var timeZone = ""
    var instId = ""
    
     var arrRole_frst : NSMutableArray = []
   // var arrSelected_role_frst : NSMutableArray = []
    
    var arrRole_sec : NSMutableArray = []
    var arrSelected_role : NSMutableArray = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collUserRole_frst.allowsMultipleSelection = true
         let dict1 = ["_id": 2, "title": "Principal Role", "isSelect": false] as [String : Any];
         let dict2 = ["_id": 3, "title": "System/User Admin","isSelect": false] as [String : Any];
         let dict3 = ["_id": 4, "title": "Sales Mgr Role","isSelect": false] as [String : Any];
         let dict4 = ["_id": 5, "title": "FI Mgr Role","isSelect": false] as [String : Any];
        
         arrRole_sec.add(dict1)
         arrRole_sec.add(dict2)
         arrRole_sec.add(dict3)
         arrRole_sec.add(dict4)
        
          let dict5 = ["title": "Active User", "isSelect": false] as [String : Any];
          let dict6 = ["title": "Password Never Expires","isSelect": false] as [String : Any];
          let dict7 = ["title": "Receive System Message","isSelect": false] as [String : Any];
          let dict8 = ["title": "Cannot Change Password","isSelect": false] as [String : Any];
         
         arrRole_frst.add(dict5)
         arrRole_frst.add(dict6)
         arrRole_frst.add(dict7)
         arrRole_frst.add(dict8)
        
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickOnSaveBtn(_ sender : UIButton)
    {
        if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
            txtEmail.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }

        else if(txtFrstName.text == "" || txtFrstName.text?.count == 0 || txtFrstName.text == nil){
            txtFrstName.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }

        else if(txtLastName.text == "" || txtLastName.text?.count == 0 || txtLastName.text == nil){
                txtLastName.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }
        else if(txtWorkPhn.text == "" || txtWorkPhn.text?.count == 0 || txtWorkPhn.text == nil){
                txtWorkPhn.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
        }

        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            self.callAddUserApi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callAddUserApi()
    {
        var is_deleted = false
        var is_psswrd = false
        var is_receive = false
        var is_cannot = false
        for i in 0..<arrSelected_role.count{
            var dict = arrSelected_role[i] as! [String: Any]
            let _ = dict.removeValue(forKey: "isSelect")
            arrSelected_role.replaceObject(at: i, with: dict)
        }
        
        for i in 0..<arrRole_frst.count{
            let dict = arrRole_frst[i] as! [String: Any]
            if (i == 0){
                let val = dict["isSelect"] as! Bool
                if(val == true){
                    is_deleted = false
                }
                else{
                    is_deleted = true
                }
            }
            if (i == 1){
                is_psswrd = dict["isSelect"] as! Bool
            }
            if (i == 2){
                is_receive = dict["isSelect"] as! Bool
            }
            if (i == 3){
                is_cannot = dict["isSelect"] as! Bool
            }
        }
        
        let params = ["email":self.txtEmail.text!,
                          "timezone":timeZone,
                         "user_code":self.txtUSercode.text!,
                         "fname":self.txtFrstName.text!,
                         "mname":self.txtMiddleName.text!,
                         "lname":self.txtLastName.text!,
                         "work_phone":self.txtWorkPhn.text!,
                         "institute_id":instId,
                         "is_deleted":is_deleted,   //bool from coll cell
                        "password_never_expire":is_psswrd,
                        "receive_system_message":is_receive,
                        "cannot_change_password":is_cannot,
                        "role_id":self.arrSelected_role,  //arr of role id
                        "salutation": self.txtSalutation.text!,
                        "suffix": self.txtSalutation.text!,
                        "title": self.txtTitle.text!,
                        "alt_email": self.txtAltEmail.text!,
                        "home_phone": self.txtHomePhn.text!,
                        "cell_phone": self.txtCellPhn.text!,
                        "other": self.txtOther.text!,
                        "fax": self.txtFax.text!,
                        "insid": (user?.institute_id)!,
                        "userid":(user?._id)!] as [String : Any]
        
        Globalfunc.print(object:params)
        BaseApi.onResponsePostWithToken(url: Constant.add_user_url, controller: self, parms: params) { (dict, err) in
                
            Globalfunc.print(object:dict)
            OperationQueue.main.addOperation {
                Globalfunc.hideLoaderView(view: self.view)
                
                if let arr = dict as? NSDictionary {
                    let msg = arr["msg"] as! String
                    if(msg == "Your token has expired."){
                        self.viewWillAppear(true)
                    }
                    else if(msg == "New User Created Successfull."){
                        
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }
                         alertController.addAction(OKAction)
                         self.present(alertController, animated: true, completion:nil)
                    }
                    else{
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                    }
                }
            }
        }
    }
}

extension addUserViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerViewTimeZone){
            return Constant.arrTimeZoneData.count
        }
        else if(pickerView == pickerViewSalutation){
            return Constant.arrSalutation.count
        }
        else if(pickerView == pickerViewSuffix){
            return Constant.arrSuffix.count
        }
        else if pickerView == pickerViewInstitution{
                return Constant.arrInstitutionData.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat
    {
        return 40
    }
    
      func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
      {
        
         if(pickerView == pickerViewTimeZone){
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        else if(pickerView == pickerViewSalutation){
            let item = Constant.arrSalutation[row]
            return item
        }
        else if(pickerView == pickerViewSuffix){
            let item = Constant.arrSuffix[row]
            return item
        }
        else if pickerView == pickerViewInstitution{
                let dict = Constant.arrInstitutionData[row] as! NSDictionary
                let item = dict["code"] as! String
                return item
        }
        
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
         if(pickerView == pickerViewTimeZone){
            
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
             let _id = dict["_id"] as! NSNumber
            self.txtTimeZone.text = item
            self.timeZone = "\(_id)"
            
        }
        
        else if(pickerView == pickerViewSalutation){

            let item = Constant.arrSalutation[row]
            self.txtSalutation.text = item

        }
        
        else if(pickerView == pickerViewSuffix){

             let item = Constant.arrSuffix[row]
             self.txtsuffix.text = item

         }
        
        else  if(pickerView == pickerViewInstitution){
                   
                   let dict = Constant.arrInstitutionData[row] as! NSDictionary
                   let item = dict["code"] as! String
                   let _id = dict["_id"] as! NSNumber
                   self.txtDEfaultInstitution.text = item
                   self.instId = "\(_id)"
                 
        }
        
       
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if(textField == self.txtTimeZone){
            self.pickUp(txtTimeZone)
            
        }
        else if(textField == self.txtSalutation){
            self.pickUp(txtSalutation)
        }
        else if(textField == self.txtsuffix){
            self.pickUp(txtsuffix)
        }
        else if(textField == self.txtDEfaultInstitution){
            self.pickUp(txtDEfaultInstitution)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
         if(textField == self.txtTimeZone){
            self.pickerViewTimeZone = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTimeZone.delegate = self
            self.pickerViewTimeZone.dataSource = self
            self.pickerViewTimeZone.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTimeZone
        }

        else if(textField == self.txtSalutation){
            self.pickerViewSalutation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewSalutation.delegate = self
            self.pickerViewSalutation.dataSource = self
            self.pickerViewSalutation.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewSalutation
        }
        else if(textField == self.txtsuffix){
            self.pickerViewSuffix = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewSuffix.delegate = self
            self.pickerViewSuffix.dataSource = self
            self.pickerViewSuffix.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewSuffix
        }

        if(textField == self.txtDEfaultInstitution){
            self.pickerViewInstitution = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInstitution.delegate = self
            self.pickerViewInstitution.dataSource = self
            self.pickerViewInstitution.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInstitution
            
            
        }
//
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editPasswordViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(editPasswordViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        
        self.txtTimeZone.resignFirstResponder()
        self.txtSalutation.resignFirstResponder()
        self.txtsuffix.resignFirstResponder()
        self.txtDEfaultInstitution.resignFirstResponder()
        
    }
    
    
    @objc func cancelClick() {
        
        self.txtTimeZone.text = ""
        self.txtSalutation.text = ""
        self.txtsuffix.text = ""
        self.txtDEfaultInstitution.text = ""
        
        self.txtTimeZone.resignFirstResponder()
        self.txtSalutation.resignFirstResponder()
        self.txtsuffix.resignFirstResponder()
         self.txtDEfaultInstitution.resignFirstResponder()
    }
    
}


extension addUserViewController : UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        var count = 0
        if(collectionView == self.collUserRole_frst){
            count = self.arrRole_frst.count
        }
        else if collectionView == self.collUserRole_sec {
            count = self.arrRole_sec.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = UICollectionViewCell()
        
        if(collectionView == self.collUserRole_frst){
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkMarkcollCell", for: indexPath) as! checkMarkcollCell
             let dict = self.arrRole_frst[indexPath.item] as! NSDictionary
             cell.lblText.text = dict["title"] as? String
            // cell.btnSelect.tag = indexPath.item
             cell.btnSelect.addTarget(self, action: #selector(clickOnSelectbtn(_:)), for: .touchUpInside)
            
            let isSelect = dict["isSelect"] as? Bool
            
            if(isSelect == false){
                if(indexPath.item == 0){
                     cell.btnSelect.isSelected = true
                }else{
                    cell.btnSelect.isSelected = false
                }
                
            }
            else{
                if(indexPath.item == 0){
                     cell.btnSelect.isSelected = false
                }
                else{
                    cell.btnSelect.isSelected = true
                }
                
            }
            return cell
        }
        else if collectionView == self.collUserRole_sec {
           
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "checkMarkcollCell_sec", for: indexPath) as! checkMarkcollCell_sec
            let dict = self.arrRole_sec[indexPath.item] as! NSDictionary
            cell.lblText.text = dict["title"] as? String
           // cell.btnSelect.tag = indexPath.item
            
            cell.btnSelect.addTarget(self, action: #selector(clickOnSelectbtn_sec(_:)), for: .touchUpInside)
            
            let isSelect = dict["isSelect"] as? Bool
            if(isSelect == false){
                cell.btnSelect.isSelected = false
            }
            else{
                cell.btnSelect.isSelected = true
            }
         
            return cell
        }
       return cell
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: 155, height: 70)
     }
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    @objc func clickOnSelectbtn(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collUserRole_frst)
        let indexPath = self.collUserRole_frst.indexPathForItem(at: buttonPosition)
        var dict = arrRole_frst[indexPath!.item] as! [String: Any]
        let isSelect = dict["isSelect"] as? Bool

        
        if sender.isSelected == true {
            sender.isSelected = false
            if(isSelect == true)
            {
                let _ = dict.updateValue(false, forKey: "isSelect")
                self.arrRole_sec.replaceObject(at: indexPath!.item, with: dict)
                if(arrSelected_role.count == 0){
                    arrSelected_role = []
                }
                else{
                    arrSelected_role.remove(dict)
                }
            }
        }else {
            sender.isSelected = true
            if(isSelect == false){
                let _ = dict.updateValue(true, forKey: "isSelect")
                arrRole_frst.replaceObject(at: indexPath!.item, with: dict)
            }
        }
    }
    
    @objc func clickOnSelectbtn_sec(_ sender: UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.collUserRole_sec)
        let indexPath = self.collUserRole_sec.indexPathForItem(at: buttonPosition)
        var dict = self.arrRole_sec[indexPath!.item] as! [String: Any]
        let isSelect = dict["isSelect"] as? Bool
        
        if sender.isSelected == true {
            sender.isSelected = false
            if(isSelect == true)
            {
                let _ = dict.updateValue(false, forKey: "isSelect")
                self.arrRole_sec.replaceObject(at: indexPath!.item, with: dict)
                if(arrSelected_role.count == 0){
                    arrSelected_role = []
                }
                else{
                    arrSelected_role.remove(dict)
                }
            }
        }else {
            sender.isSelected = true
            if(isSelect == false){
                let _ = dict.updateValue(true, forKey: "isSelect")
                arrSelected_role.add(dict)
                self.arrRole_sec.replaceObject(at: indexPath!.item, with: dict)
            }
        }
    }
}

class checkMarkcollCell: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
}

class checkMarkcollCell_sec: UICollectionViewCell {
    
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
}


