//
//  editPasswordViewController.swift
//  LMS
//
//  Created by Reinforce on 30/01/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class editPasswordViewController: UIViewController {
    
    @IBOutlet weak var viewHeader: UIView!
    
    var dictUSerDetail : NSDictionary = [:]
    
    var channelId : NSNumber?
    var instId : String?
    var timeZone : String?
    
    
    @IBOutlet weak var txtInstitution: UITextField!
    @IBOutlet weak var txtTimezone: UITextField!
    @IBOutlet weak var txtDefaultBranch: UITextField!
    
    
    @IBOutlet weak var txtUsercode: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtConfrmPass: UITextField!
    @IBOutlet weak var txtUSerPAss: UITextField!
    
    
    
    var pickerViewInstitution: UIPickerView!
    var pickerViewTimeZone: UIPickerView!
    var pickerViewDefaultBranch: UIPickerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.isOpaque = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        Globalfunc.showLoaderView(view: self.view)
        do {
            let decoded  = userDef.object(forKey: "userDetail") as! Data
            if let usedict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let _id = usedict["_id"] as! NSNumber
                let url = "\(Constant.user_detail_url)?id=\(_id)"
                
                if let user_code = usedict["user_code"] as? String{
                    self.txtUsercode.text = user_code
                }
                
                if let email = usedict["email"] as? String{
                    self.txtEmail.text = email
                }
                
                self.callUserDetailApi(strUrl: url)
            }
        }
        catch{
            
        }
        
        
    }
    
    @IBAction func clickOnCrossBtn(_ saender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnUpdateSettingBtn(_ sender: UIButton)
    {
        if(sender.tag == 10){
            if(txtInstitution.text == "" || txtInstitution.text?.count == 0 || txtInstitution.text == nil){
                txtInstitution.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if(txtTimezone.text == "" || txtTimezone.text?.count == 0 || txtTimezone.text == nil){
                txtTimezone.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if(txtDefaultBranch.text == "" || txtDefaultBranch.text?.count == 0 || txtDefaultBranch.text == nil){
                txtDefaultBranch.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                
                do {
                    let decoded  = userDef.object(forKey: "userDetail") as! Data
                    if let usedict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
                    {
                        let _id = usedict["_id"] as! NSNumber
                        let params = ["action":"user.prefrence.update",
                                      "channel":self.channelId!,
                                      "id":_id,
                                      "insid":self.instId!,
                                      "timezone":self.timeZone!,
                                      "userid":(user?._id)!] as [String : Any]
                        self.callUpdteSetingApi(strCheck: "user_pref", param: params)
                    }
                }
                catch{
                    
                }
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
        else if(sender.tag == 20){
            if(txtEmail.text == "" || txtEmail.text?.count == 0 || txtEmail.text == nil){
                txtEmail.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if(txtUsercode.text == "" || txtUsercode.text?.count == 0 || txtUsercode.text == nil){
                txtUsercode.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if(txtNewPass.text == "" || txtNewPass.text?.count == 0 || txtNewPass.text == nil){
                txtNewPass.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
            else if(txtConfrmPass.text == "" || txtConfrmPass.text?.count == 0 || txtConfrmPass.text == nil){
                txtConfrmPass.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if(txtNewPass.text != txtConfrmPass.text){
                txtNewPass.shake()
                txtConfrmPass.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Passwords do not match.")
            }
            else if(txtUSerPAss.text == "" || txtUSerPAss.text?.count == 0 || txtUSerPAss.text == nil){
                txtUSerPAss.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
            }
                
            else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
                
                do {
                    let decoded  = userDef.object(forKey: "userDetail") as! Data
                    if let usedict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
                    {
                        let _id = usedict["_id"] as! NSNumber
                        let params = ["action":"user.password.update",
                                      "email":self.txtEmail.text!,
                                      "id":_id,
                                      "usercode":self.txtUsercode.text!,
                                      "password":self.txtNewPass.text!,
                                      "useremail":(user?.email)!,
                                      "userpassword":txtUSerPAss.text!,
                                      "userid":(user?._id)!] as [String : Any]
                        self.callUpdteSetingApi(strCheck: "security", param: params)
                    }
                }
                catch{
                    
                }
            }
            else{
                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
            }
        }
    }
}

extension editPasswordViewController {
    
    func callUserDetailApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        if let msg = arr["message"] as? String
                        {
                            if(msg == "Your token has expired."){
                                self.viewWillAppear(true)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                        if let dictData = arr["data"] as? NSDictionary{
                            self.dictUSerDetail = dictData
                            let institute_id = self.dictUSerDetail["institute_id"] as! String
                            for i in 0..<Constant.arrInstitutionData.count{
                                let defDict = Constant.arrInstitutionData[i] as! NSDictionary
                                let _id = defDict["_id"] as! NSNumber
                                if(institute_id == "\(_id)"){
                                    let title = defDict["code"] as! String
                                    self.instId = "\(_id)"
                                    self.txtInstitution.text = title
                                    break
                                }
                            }
                            if let timezone = self.dictUSerDetail["timezone"] as? String
                            {
                                for i in 0..<Constant.arrTimeZoneData.count{
                                    let defDict = Constant.arrTimeZoneData[i] as! NSDictionary
                                    let _id = defDict["_id"] as! NSNumber
                                    if(timezone == "\(_id)"){
                                        let title = defDict["title"] as! String
                                        self.txtTimezone.text = title
                                        self.timeZone = "\(_id)"
                                        break
                                    }
                                }
                            }
                            if let _ = self.dictUSerDetail["timezone"] as? NSNumber
                            {
                                //when time zone will change to number put code here
                            }
                            
                            if let channel = self.dictUSerDetail["channel"] as? NSNumber{
                                for i in 0..<consArrays.arrDefaultBranch.count{
                                    let defDict = consArrays.arrDefaultBranch[i] as! NSDictionary
                                    let _id = defDict["_id"] as! NSNumber
                                    if(channel == _id){
                                        let title = defDict["title"] as! String
                                        self.channelId = _id
                                        self.txtDefaultBranch.text = title
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    
    func callUpdteSetingApi(strCheck: String, param : [String: Any])
    {
        
        Globalfunc.print(object:param)
        BaseApi.onResponsePutWithToken(url: Constant.update_user_pref, controller: self, parms: param as NSDictionary) { (dict, error) in
            
            if(strCheck == "user_pref"){
                
                OperationQueue.main.addOperation {
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        if(msg == "Your token has expired."){
                            self.viewWillAppear(true)
                        }
                        else if (msg == "User Profile is updated successfully."){
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                        }
                    }
                    
                }
            }
            else if(strCheck == "security"){
                
                OperationQueue.main.addOperation {
                    
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        if(msg == "Your token has expired."){
                            self.viewWillAppear(true)
                        }
                        else if (msg == "User Password is updated successfully."){
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                        else{
                            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                        }
                    }
                    
                }
            }
            
        }
        
    }
    
}

extension editPasswordViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pickerViewInstitution{
            return Constant.arrInstitutionData.count
        }
        else if(pickerView == pickerViewTimeZone){
            return Constant.arrTimeZoneData.count
        }
            
        else if(pickerView == pickerViewDefaultBranch){
            return consArrays.arrDefaultBranch.count
        }
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewInstitution){
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            return item
        }
        else if(pickerView == pickerViewTimeZone){
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
            
            return item
        }
        else if(pickerView == pickerViewDefaultBranch){
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let item = dict["title"] as! String
            return item
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewInstitution){
            
            let dict = Constant.arrInstitutionData[row] as! NSDictionary
            let item = dict["code"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtInstitution.text = item
            self.instId = "\(_id)"
            
        }
            
        else if(pickerView == pickerViewTimeZone){
            
            let dict = Constant.arrTimeZoneData[row] as! NSDictionary
            let item = dict["title"] as! String
            let _id = dict["_id"] as! NSNumber
            self.txtTimezone.text = item
            self.timeZone = "\(_id)"
            
        }
            
        else if(pickerView == pickerViewDefaultBranch){
            
            let dict = consArrays.arrDefaultBranch[row] as! NSDictionary
            let _id = dict["_id"] as! NSNumber
            let item = dict["title"] as! String
            
            self.channelId = _id
            self.txtDefaultBranch.text = item
            
        }
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtInstitution){
            self.pickUp(txtInstitution)
        }
        else if(textField == self.txtTimezone){
            self.pickUp(txtTimezone)
            
        }
        else if(textField == self.txtDefaultBranch){
            self.pickUp(txtDefaultBranch)
        }
        
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        
        if(textField == self.txtInstitution){
            self.pickerViewInstitution = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewInstitution.delegate = self
            self.pickerViewInstitution.dataSource = self
            self.pickerViewInstitution.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewInstitution
        }
        else if(textField == self.txtTimezone){
            self.pickerViewTimeZone = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewTimeZone.delegate = self
            self.pickerViewTimeZone.dataSource = self
            self.pickerViewTimeZone.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewTimeZone
        }
            
        else if(textField == self.txtDefaultBranch){
            self.pickerViewDefaultBranch = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewDefaultBranch.delegate = self
            self.pickerViewDefaultBranch.dataSource = self
            self.pickerViewDefaultBranch.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewDefaultBranch
        }
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(editPasswordViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(editPasswordViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtDefaultBranch.resignFirstResponder()
        txtInstitution.resignFirstResponder()
        txtTimezone.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txtDefaultBranch.resignFirstResponder()
        txtInstitution.resignFirstResponder()
        txtTimezone.resignFirstResponder()
    }
}
