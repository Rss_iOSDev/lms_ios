//
//  userDetailViewController.swift
//  LMS
//
//  Created by Reinforce on 10/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class userDetailViewController: UIViewController {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblWorkphn: UILabel!
    @IBOutlet weak var lblLastLogin: UILabel!
    
    @IBOutlet weak var tblListBtn: UITableView!
    var arrBtnList = ["GENERAL INFO","SECURITY RIGHTS","VENDOR USER SETTINGS"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        self.tblListBtn.tableFooterView = UIView()
        do {
            let decoded  = userDef.object(forKey: "userDetail") as! Data
            if let usedict = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary
            {
                let _id = usedict["_id"] as! NSNumber
                let url = "\(Constant.user_detail_url)?id=\(_id)"
                
                var strName = ""
                if let fname = usedict["fname"] as? String{
                    strName = fname
                }
                if let lname = usedict["lname"] as? String{
                    strName = strName + lname
                    self.lblName.text = strName
                }
                if let email = usedict["email"] as? String{
                    self.lblEmail.text = email
                }
                if let work_phone = usedict["work_phone"] as? String{
                    self.lblWorkphn.text = work_phone
                }
                
                self.callUserDetailApi(strUrl: url)
            }
        }
        catch{
            
        }
        
    }
    
    
    @IBAction func clickOnbackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}

//Call Api , set and pas data
extension userDetailViewController{
    
    func callUserDetailApi(strUrl: String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let respnse = dict as? NSDictionary {
                        if let responseDict = respnse["data"] as? NSDictionary{
                            do {
                                if #available(iOS 11.0, *) {
                                    let myData = try NSKeyedArchiver.archivedData(withRootObject: responseDict, requiringSecureCoding: false)
                                    userDef.set(myData, forKey: "passDict")
                                } else {
                                    let myData = NSKeyedArchiver.archivedData(withRootObject: responseDict)
                                    userDef.set(myData, forKey: "passDict")
                                }
                            } catch {
                                Globalfunc.print(object: "Couldn't write file")
                            }
                            if let last_login = responseDict["last_login_date"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy hh:mm a"
                                let date = Date.dateFromISOString(string: last_login)
                                let createFormat = formatter.string(from: date!)
                                self.lblLastLogin.text = "\(createFormat)"
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


extension userDetailViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBtnList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblListBtn.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
        cell.lblTitle.text = self.arrBtnList[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            isViewFrom = "Edit"
            self.presentViewControllerBasedOnIdentifier("generalInfoView", strStoryName: "SettingStory")
        case 1:
            self.presentViewControllerBasedOnIdentifier("securityRightView", strStoryName: "SettingStory")
        case 2:
            self.presentViewControllerBasedOnIdentifier("vendorUSerSettingView", strStoryName: "SettingStory")
        default:
            Globalfunc.print(object:"default")
        }
    }
}
