//
//  inventoryDetailViewController.swift
//  LMS
//
//  Created by Apple on 05/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class inventoryDetailViewController: UIViewController {
    
    @IBOutlet weak var lblAccountNo : UILabel!
    
    //view borrower outlets
    @IBOutlet weak var view_1 : UIView!
    @IBOutlet weak var lblStatus : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblStock : UILabel!
    @IBOutlet weak var lblBodyStyle : UILabel!
    @IBOutlet weak var lblBodyTrim : UILabel!
    @IBOutlet weak var lblVin : UILabel!
    @IBOutlet weak var lblTyp : UILabel!
    @IBOutlet weak var lblColor : UILabel!
    @IBOutlet weak var lblMileage : UILabel!
    @IBOutlet weak var lblCondition : UILabel!
    @IBOutlet weak var lblVehAge : UILabel!
    @IBOutlet weak var btnView_1More : UIButton!
    @IBOutlet weak var viewiew_1Hgt : NSLayoutConstraint!
    
    
    //view address outlets
    @IBOutlet weak var view_2 : UIView!
    @IBOutlet weak var lblAskingPrice : UILabel!
    @IBOutlet weak var lblAskingDown : UILabel!
    @IBOutlet weak var lblCost : UILabel!
    @IBOutlet weak var lblTotalCost : UILabel!
    @IBOutlet weak var lblBuyersGuide : UILabel!
    @IBOutlet weak var lblMaxTerms : UILabel!
    @IBOutlet weak var btnView_2More : UIButton!
    @IBOutlet weak var viewiew_2Hgt : NSLayoutConstraint!
    
    
    
    //view borrower outlets
    @IBOutlet weak var view_3 : UIView!
    @IBOutlet weak var lblLoc : UILabel!
    @IBOutlet weak var lblAltloc : UILabel!
    @IBOutlet weak var lblInventry : UILabel!
    @IBOutlet weak var lblDaysLoc : UILabel!
    @IBOutlet weak var lblFlags : UILabel!
    @IBOutlet weak var lblBookkepng : UILabel!
    @IBOutlet weak var btnView_3_More : UIButton!
    @IBOutlet weak var view_3Hgt : NSLayoutConstraint!
    
    
    //view insurance outlets
    @IBOutlet weak var lblManhiem : UILabel!
    @IBOutlet weak var lblNada : UILabel!
    
    @IBOutlet weak var viewDocList : UIView!
    @IBOutlet weak var tblDocList : UITableView!
    
    @IBOutlet weak var viewMoeList : UIView!
    @IBOutlet weak var tblMoreList : UITableView!
    
    var arrMoreList = [String]()
    
    
    let txtRemoveMsg = UITextView(frame: CGRect.zero)
    
    
    var arrList = [String]()
    var arrDocList = ["Documents","Reports"]
    var account_status = ""
    var inventoryId : Int!
    var passDict_LayoutId : NSDictionary = [:]
    var accountDetailsData : NSDictionary = [:]
    let dict_details : NSMutableDictionary = [:]
    
    @IBOutlet weak var imgDel : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tblMoreList.tableFooterView = UIView()
        self.tblDocList.tableFooterView = UIView()
        
        userDef.set(self.inventoryId ?? 0, forKey: "inventoryId")
        userDef.synchronize()
        
        self.viewDocList.alpha = 0
        self.viewMoeList.alpha = 0
        Globalfunc.showLoaderView(view: self.view)
        self.getCallDetailsApi()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clikcOnDetailTabs(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "Inventory", bundle: nil)
        let dest = story.instantiateViewController(identifier: "inventoryTabsViewController") as! inventoryTabsViewController
        dest.modalPresentationStyle = .fullScreen
        self.present(dest, animated: true, completion: nil)
        
    }
    
    @IBAction func clickOnMoreDetailsView1Btn(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            sender.tag = 0
            self.viewiew_1Hgt.constant = 350
            self.lblVin.isHidden = false
            self.lblTyp.isHidden = false
            self.lblColor.isHidden = false
            self.lblMileage.isHidden = false
            self.lblCondition.isHidden = false
            self.lblVehAge.isHidden = false
            self.btnView_1More.setTitle("Less Detail", for: .normal)
        }
        else
        {
            sender.tag = 1
            self.viewiew_1Hgt.constant = 180
            self.lblVin.isHidden = true
            self.lblTyp.isHidden = true
            self.lblColor.isHidden = true
            self.lblMileage.isHidden = true
            self.lblCondition.isHidden = true
            self.lblVehAge.isHidden = true
            self.btnView_1More.setTitle("More Detail", for: .normal)
        }
    }
    
    @IBAction func clickOnMoreDetailsView2Btn(_ sender: UIButton){
        if sender.tag == 10
        {
            sender.tag = 9
            self.viewiew_2Hgt.constant = 240
            self.lblMaxTerms.isHidden = false
            self.btnView_2More.setTitle("Less Detail", for: .normal)
        }
        else{
            sender.tag = 10
            self.viewiew_2Hgt.constant = 200
            self.lblMaxTerms.isHidden = true
            self.btnView_2More.setTitle("More Detail", for: .normal)
        }
    }
    
}



//call action api for all pages once
extension inventoryDetailViewController{
    
    // http://3.129.150.191:3000/api/inventory/id?id=14
    // id
    
    
    func getCallDetailsApi()
    {
        let Str_url = "\(Constant.inventory)/id?id=\(self.inventoryId!)"
        BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let responseDict = dict as? NSDictionary {
                        if let data = responseDict["data"] as? NSDictionary{
                            self.setDataonView(dict: data)
                        }
                        
                    }
                    
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func setDataonView(dict: NSDictionary){
        
        self.viewiew_1Hgt.constant = 180
        self.lblVin.isHidden = true
        self.lblTyp.isHidden = true
        self.lblColor.isHidden = true
        self.lblMileage.isHidden = true
        self.lblCondition.isHidden = true
        self.lblVehAge.isHidden = true
        self.btnView_1More.setTitle("More Detail", for: .normal)
        
        self.viewiew_2Hgt.constant = 200
        self.lblMaxTerms.isHidden = true
        self.btnView_2More.setTitle("More Detail", for: .normal)
        
        if let location = dict["location"] as? NSDictionary{
            
            if let altLocation = location["altLocation"] as? String{
                self.lblAltloc.text = "Alt Location: \(altLocation)"
            }
            
            if let location = location["location"] as? String{
                self.lblLoc.text = "Location: \(location)"
            }
            
            if let book_keeping_company = location["book_keeping_company"] as? String{
                self.lblBookkepng.text = "Bookkeeping Co:: \(book_keeping_company)"
            }
            if let days_at_location = location["days_at_location"] as? String{
                self.lblDaysLoc.text = "Days At Location: \(days_at_location)"
            }
            
            if let vin = location["vin"] as? String{
                self.lblVin.text = "VIN: \(vin)"
            }
            
            if let in_inventory = location["in_inventory"] as? String{
                self.lblInventry.text = "In Inventory: \(in_inventory)"
            }
            
            if let flags = location["flags"] as? [String]{
                self.lblFlags.text = "Flags: \(flags)"
            }
            //:Inv High, Inv Medium
        }
        
        if let pricing = dict["pricing"] as? NSDictionary{
            if let buyer_guide = pricing["buyer_guide"] as? String{
                self.lblTyp.text = "Buyers Guide: \(buyer_guide)"
            }
            
            //            Asking Price:$100.00
            //            Asking Down:$500.00
            //            Cost:$78,555.00
            //            Total Cost:$78,555.00
            
            
        }
        
        if let vehicle = dict["vehicle"] as? NSDictionary{
            if let bodyStyle = vehicle["bodyStyle"] as? String{
                self.lblBodyStyle.text = "Body Style: \(bodyStyle)"
            }
            
            if let status = vehicle["status"] as? String{
                self.lblStatus.text = "Status: \(status)"
                
                userDef.set(status, forKey: "inventory_status")
                userDef.synchronize()
                
                if(status == "Deleted"){
                    self.imgDel.image = UIImage(named: "refresh")
                }else if(status == "Evaluating"){
                    self.imgDel.image = UIImage(named: "trash")
                    self.imgDel.tintColor = .white
                }
            }
            
            if let description = vehicle["description"] as? String{
                self.lblDescription.text = "Description: \(description)"
            }
            if let color = vehicle["color"] as? String{
                self.lblColor.text = "Color: \(color)"
            }
            
            if let vin = vehicle["vin"] as? String{
                self.lblVin.text = "VIN: \(vin)"
            }
            
            if let vehicleAge = vehicle["vehicleAge"] as? String{
                self.lblVehAge.text = "Vehicle Age: \(vehicleAge)"
            }
            if let mileage = vehicle["mileage"] as? String{
                self.lblMileage.text = "Mileage: \(mileage)"
            }
            
            if let type = vehicle["type"] as? String{
                self.lblTyp.text = "Type: \(type)"
            }
        }
        
        
        
        
    }
}


extension inventoryDetailViewController
{
    @IBAction func clickOnDollarBtn(_ sender : UIButton){
        if(sender.tag == 10){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDocList.alpha = 1
                self.viewMoeList.alpha = 0
            }) { _ in
            }
        }
        else if(sender.tag == 20){
            UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
                self.viewDocList.alpha = 0
                self.viewMoeList.alpha = 0
                
            }) { _ in
            }
        }
        
    }
}

extension inventoryDetailViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count : Int?
        if tableView == self.tblDocList{
            count = arrDocList.count
        }
        if tableView == self.tblMoreList{
            count = arrMoreList.count
        }
        
        
        return count!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if tableView == self.tblDocList{
            let cell = self.tblDocList.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
            cell.lblTitle.text = self.arrDocList[indexPath.row]
            return cell
        }
        if tableView == self.tblMoreList{
            let cell = self.tblMoreList.dequeueReusableCell(withIdentifier: "tblDolarCell") as! tblDolarCell
            cell.lblTitle.text = self.arrMoreList[indexPath.row]
            return cell
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if tableView == self.tblDocList{
            switch indexPath.row {
            case 0:
                let story = UIStoryboard.init(name: "ActionStory", bundle: nil)
                let dest = story.instantiateViewController(identifier: "attachDocViewController") as! attachDocViewController
                dest.modalPresentationStyle = .fullScreen
                dest.isOpenFrom = "document"
                dest.isFromInventory = "true"
                self.present(dest, animated: true, completion: nil)
            case 1:
                self.presentViewControllerasPopup("reportListViewController", strStoryName: "ActionStory")
            default:
                Globalfunc.print(object:"default")
            }
        }
        
        if tableView == self.tblMoreList{
            
            if let status  = userDef.object(forKey: "inventory_status") as? String {
                if(status == "Evaluating"){
                    switch indexPath.row {
                    case 0:
                        self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
                    case 1:
                        self.presentViewControllerBasedOnIdentifier("chngLocVC", strStoryName: "Inventory")
                    case 2:
                        self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
                    case 3:
                        self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
                    case 4:
                        self.presentViewControllerBasedOnIdentifier("addNoteInventryVC", strStoryName: "Inventory")
                    case 5:
                        self.presentViewControllerBasedOnIdentifier("buyersGuideVC", strStoryName: "Inventory")
                    default:
                        Globalfunc.print(object:"default")
                    }
                }
                else{
                    switch indexPath.row {
                    case 0:
                        self.presentViewControllerBasedOnIdentifier("chngLocVC", strStoryName: "Inventory")
                    case 1:
                        self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
                    case 2:
                        self.presentViewControllerBasedOnIdentifier("", strStoryName: "")
                    case 3:
                        self.presentViewControllerBasedOnIdentifier("addNoteInventryVC", strStoryName: "Inventory")
                    case 4:
                        self.presentViewControllerBasedOnIdentifier("buyersGuideVC", strStoryName: "Inventory")
                    default:
                        Globalfunc.print(object:"default")
                    }
                }
            }
            else{
                
                //                switch indexPath.row {
                //                case 0:
                //                    isUpload = "account"
                //                    self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
                //                case 1:
                //                    isUpload = "borrower"
                //                    self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
                //                case 2:
                //                    isUpload = "collateral-image"
                //                    self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
                //                case 3:
                //                    isUpload = "collateral"
                //                    self.presentViewControllerBasedOnIdentifier("uploadFilesVC", strStoryName: "dollarStory")
                //                case 4:
                //                    self.presentViewControllerBasedOnIdentifier("addEditFlagsVC", strStoryName: "dollarStory")
                //
                //                default:
                //                    Globalfunc.print(object:"default")
                //                }
                //
            }
            
        }
        
    }
}

extension inventoryDetailViewController{
    
    @IBAction func clinOnDeletBtn(_ sender: UIButton){
        
        
        if let status  = userDef.object(forKey: "inventory_status") as? String {
            if(status == "Evaluating"){
                
                // Create the alert controller
                let alertController = UIAlertController(title: "DELETE INVENTORY", message: "", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Confirm", style: .default) {
                    UIAlertAction in
                    
                    Globalfunc.showLoaderView(view: self.view)
                    if let _id  = userDef.object(forKey: "inventoryId") as? Int {
                        let params = [ "id": _id,
                                       "inventoryStatus": 3,
                                       "removeReason": "",
                                       "insid": (user?.institute_id!)!,
                                       "userid": (user?._id)!] as [String : Any]
                        
                        Globalfunc.print(object:params)
                        self.callUpdateApi(param: params)
                    }
                    
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                    UIAlertAction in
                    //self.dismiss(animated: true, completion: nil)
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }
            else if(status == "Deleted"){
                
                // Create the alert controller
                let alertController = UIAlertController(title: "REINSTATE INVENTORY", message: "", preferredStyle: .alert)
                
                // Create the actions
                let okAction = UIAlertAction(title: "Confirm", style: .default) {
                    UIAlertAction in
                    
                    Globalfunc.showLoaderView(view: self.view)
                    if let _id  = userDef.object(forKey: "inventoryId") as? Int {
                        let params = [ "id": _id,
                                       "inventoryStatus": 4,
                                       "removeReason": "",
                                       "insid": (user?.institute_id!)!,
                                       "userid": (user?._id)!] as [String : Any]
                        
                        Globalfunc.print(object:params)
                        self.callUpdateApi(param: params)
                    }
                    
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                    UIAlertAction in
                    // self.dismiss(animated: true, completion: nil)
                }
                
                // Add the actions
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                
                // Present the controller
                self.present(alertController, animated: true, completion: nil)
                
            }
            
            else if(status == "Available"){
                
                
                let alertController = UIAlertController(title: "REMOVE INVENTORY", message: "", preferredStyle: .alert)
                
                alertController.addTextField { (textField : UITextField!) -> Void in
                    textField.placeholder = "Enter Message"
                }
                
                let saveAction = UIAlertAction(title: "REMOVE THIS VEHICLE", style: .default, handler: { alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                    
                    Globalfunc.showLoaderView(view: self.view)
                    if let _id  = userDef.object(forKey: "inventoryId") as? Int {
                        let params = [ "id": _id,
                                       "inventoryStatus": 4,
                                       "removeReason": firstTextField.text!,
                                       "insid": (user?.institute_id!)!,
                                       "userid": (user?._id)!] as [String : Any]
                        
                        Globalfunc.print(object:params)
                        self.callUpdateApi(param: params)
                    }
                    
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) {
                    UIAlertAction in
                    // self.dismiss(animated: true, completion: nil)
                }
                
                alertController.addAction(saveAction)
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func clinOnMoreBtn(_ sender: UIButton){
        if let status  = userDef.object(forKey: "inventory_status") as? String {
            if(status == "Evaluating"){
                arrMoreList = ["Acquire This Vehicle","Change Location","Add/Edit Flags","Add File","Add Note","Edit Buyers Guide"]
            }
            else{
                arrMoreList = ["Change Location","Add/Edit Flags","Add File","Add Note","Edit Buyers Guide"]
            }
        }
        else{
            arrMoreList = ["Change Location","Add/Edit Flags","Add File","Add Note","Edit Buyers Guide"]
        }
        
        UIView.animate(withDuration: 1, delay: 0.5, options: .curveEaseInOut, animations: {
            self.viewMoeList.alpha = 1
            self.viewDocList.alpha = 0
        }) { _ in
            self.tblMoreList.reloadData()
        }
    }
}


extension inventoryDetailViewController {
    
    func callUpdateApi(param: [String : Any])
    {
        BaseApi.onResponsePostWithToken(url: Constant.rem_invntry, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
