//
//  chngLocVC.swift
//  LMS
//
//  Created by Apple on 17/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class chngLocVC: UIViewController {
    
    var arrUrl = [Constant.default_branch_url]
    
    @IBOutlet weak var txtLocation: UITextField!
    var pv_titleLoc: UIPickerView!
    var arrVehLot = NSMutableArray()
    var veh_id = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        self.callPickerData()
        // Do any additional setup after loading the view.
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension chngLocVC{
    
    func callPickerData(){
        
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.default_branch_url){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrVehLot.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.setApi()
                        }

                        
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
}

extension chngLocVC{

    func setApi(){
        if let _id = userDef.object(forKey: "inventoryId") as? Int {
            let strGeturl = "\(Constant.inventory)/location?id=\(_id)"
            self.callApiToSetData(strUrl: strGeturl)
            
        }
    }
    func callApiToSetData(strUrl : String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict,error ) in
            if(error == ""){

                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: dict)
                        if let arr = dict as? NSDictionary{
                            if let data = arr["data"] as? Int{
                                    self.veh_id = "\(data)"
                                    for i in 0..<self.arrVehLot.count{
                                        let dict = self.arrVehLot[i] as! NSDictionary
                                        let _id = dict["_id"] as! Int
                                        if("\(_id)" == "\(data)"){
                                            let strTitle = dict["title"] as! String
                                            self.txtLocation.text = strTitle
                                            break
                                        }
                                    }
                                }
                            }
                        }
                }
                else{
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
}
extension chngLocVC: UIPickerViewDelegate , UIPickerViewDataSource ,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
        if(pickerView == pv_titleLoc){
                return arrVehLot.count
            }
        else{
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if (pickerView == pv_titleLoc)
        {
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     
        if(pickerView == pv_titleLoc){
            
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtLocation.text = strTitle
            let _id = dict["_id"] as! Int
            self.veh_id = "\(_id)"

        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == self.txtLocation){
            self.pickUp(txtLocation)
        }
    }
    
    func pickUp(_ textField : UITextField){
        if(textField == self.txtLocation){
            self.pv_titleLoc = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_titleLoc.delegate = self
            self.pv_titleLoc.dataSource = self
            self.pv_titleLoc.backgroundColor = UIColor.white
            textField.inputView = self.pv_titleLoc
        }
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    @objc func doneClick() {
        txtLocation.resignFirstResponder()
    }
    

}



extension chngLocVC {
    
    @IBAction func clickOnSaveBtn(_ sender: UIButton)
    {
        if let _id = userDef.object(forKey: "inventoryId") as? Int {
            let params = [
                "id": _id,
                "insid": (user?.institute_id!)!,
                "userid": (user?._id)!,
                "vehicleLot": self.veh_id] as [String : Any]
            Globalfunc.print(object: params)
            self.callUpdateApi(param: params)
        }
    }
    
    func callUpdateApi(param: [String : Any])
    {
        let strUrl = "\(Constant.inventory)/location"
        BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
