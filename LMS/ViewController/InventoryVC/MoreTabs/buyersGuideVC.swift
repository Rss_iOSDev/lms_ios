//
//  buyersGuideVC.swift
//  LMS
//
//  Created by Apple on 24/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class buyersGuideVC: UIViewController {

    @IBOutlet weak var txtBuigeyGuide:UITextField!
    @IBOutlet weak var txtServiceContact:UITextField!
    
    @IBOutlet weak var btnCheckbox:UIButton!
    @IBOutlet weak var viewFields:UIView!
    
    
    var pv_Buyer: UIPickerView!
    var arrBuyer = NSMutableArray()
    var buyer_id = ""
    
    var pv_Scontract: UIPickerView!
    var arrScontract = NSMutableArray()
    var Scontct = false
    
    var saleDef = false
    
    var dataDict : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dict_1 = ["name":"Yes","value":true] as [String : Any]
        let dict_2 = ["name":"No","value":false] as [String : Any]
        
        self.arrScontract.add(dict_1)
        self.arrScontract.add(dict_2)
        
        let dict_3 = ["name":"Warranty","value":"0"] as [String : String]
        let dict_4 = ["name":"AS-IS","value":"1"] as [String : String]
        
        self.arrBuyer.add(dict_3)
        self.arrBuyer.add(dict_4)

        self.saleDef = false
        self.viewFields.isHidden = false

        Globalfunc.showLoaderView(view: self.view)
        self.callParticularSate()
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnBtnLoc(_ sender: UIButton)
    {
        
        if(sender.isSelected == false){
                self.btnCheckbox.isSelected = true
                self.saleDef = true
                self.viewFields.isHidden = true
        }
        else if(sender.isSelected == true){
                self.btnCheckbox.isSelected = false
                self.saleDef = false
                self.viewFields.isHidden = false
        }
    }
}
//change keys

extension buyersGuideVC {
    
    
    func callParticularSate()
    {
        if let _id  = userDef.object(forKey: "inventoryId") as? Int {
            let strGetUrl = "\(Constant.inventory)/buyer-guide?id=\(_id)"
            self.callApiToSetData(strUrl: strGetUrl)
        }
    
    }
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        if let vehicle = arr["data"] as? NSDictionary{
                            
                            self.dataDict = vehicle
                            
                            if let buyer_guide = vehicle["buyer_guide"] as? String{
                                self.buyer_id = "\(buyer_guide)"
                                for i in 0..<self.arrBuyer.count{
                                    let dict = self.arrBuyer[i] as! [String:String]
                                    let _id = dict["value"]
                                    if(_id == buyer_guide){
                                        let strTitle = dict["name"]
                                        self.txtBuigeyGuide.text = strTitle
                                        break
                                    }
                                }
                            }
                            
                            if let service_contact = vehicle["service_contact"] as? Bool{
                                self.Scontct = service_contact
                                for i in 0..<self.arrScontract.count{
                                    let dict = self.arrScontract[i] as! [String:Any]
                                    let val = dict["value"] as! Bool
                                    if(val == service_contact){
                                        let strTitle = dict["name"] as! String
                                        self.txtServiceContact.text = strTitle
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension buyersGuideVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pv_Buyer{
            return self.arrBuyer.count
        }
        else if(pickerView == pv_Scontract){
            return self.arrScontract.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pv_Buyer{
            let dict = arrBuyer[row] as! [String:String]
            let strTitle = dict["name"]
            return strTitle
        }
        else if(pickerView == pv_Scontract){
            let dict = arrScontract[row] as! [String:Any]
            let strTitle = dict["name"] as! String
            return strTitle
        }
        return ""
    }
    //
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == pv_Buyer{
            let dict = arrBuyer[row] as! [String:String]
            let strTitle = dict["name"]
            self.txtBuigeyGuide.text = strTitle
            self.buyer_id = dict["value"]!
        }
        
        else if(pickerView == pv_Scontract){
            let dict = arrScontract[row] as! [String:Any]
            let strTitle = dict["name"] as! String
            self.txtServiceContact.text = strTitle
            self.Scontct = dict["value"] as! Bool
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtBuigeyGuide){
            self.pickUp(txtBuigeyGuide)
        }
        else if(textField == self.txtServiceContact){
            self.pickUp(txtServiceContact)
        }
    }
    
        
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtBuigeyGuide){
            self.pv_Buyer = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Buyer.delegate = self
            self.pv_Buyer.dataSource = self
            self.pv_Buyer.backgroundColor = UIColor.white
            textField.inputView = self.pv_Buyer
        }
        else if(textField == self.txtServiceContact){
            self.pv_Scontract = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Scontract.delegate = self
            self.pv_Scontract.dataSource = self
            self.pv_Scontract.backgroundColor = UIColor.white
            textField.inputView = self.pv_Scontract
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtServiceContact.resignFirstResponder()
        txtBuigeyGuide.resignFirstResponder()
    }
}

extension buyersGuideVC{
    
    @IBAction func clickBtn(_ sender: UIButton){
        
        let inventory_id = self.dataDict["_id"] as! Int
            
        let params = [
                        "buyer_guide": self.buyer_id,
                        "id": inventory_id,
                        "insid": (user?.institute_id!)!,
                "sales_location_default": self.saleDef,
                        "service_contact": self.Scontct,
                        "userid": (user?._id)!] as [String : Any]
        Globalfunc.print(object: params)
        self.callUpdateApi(param: params)
    }
    
    
    func callUpdateApi(param: [String : Any])
    {
        
        let strUrl = "\(Constant.inventory)/buyer-guide"
        BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
