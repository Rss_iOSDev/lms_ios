//
//  addNoteInventryVC.swift
//  LMS
//
//  Created by Apple on 24/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class addNoteInventryVC: UIViewController {


    @IBOutlet weak var txtNotes :UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

}
//change keys



extension addNoteInventryVC{
    
    @IBAction func clickBtn(_ sender: UIButton){
        
        
        if let _id  = userDef.object(forKey: "inventoryId") as? Int {
            let params = [
                        "action": "Notes",
                "description": self.txtNotes.text!,
                        "inventory_id": _id,
                "notes_type": "inventory",
                "results": "",
                "sub_type": "add_notes",
                "updated": "Contact Note",
                "user": "MS1",
                        "insid": (user?.institute_id!)!,
                        "userid": (user?._id)!] as [String : Any]
            Globalfunc.print(object: params)
            self.callUpdateApi(param: params)

        }
        
    }
    
    
    func callUpdateApi(param: [String : Any])
    {
        
        
        BaseApi.onResponsePostWithToken(url: Constant.inventory_notes, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
