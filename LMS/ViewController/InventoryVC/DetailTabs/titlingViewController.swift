//
//  titlingViewController.swift
//  LMS
//
//  Created by Apple on 17/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class titlingViewController: UIViewController {
    
    var arrUrl = [Constant.state,Constant.country,Constant.default_branch_url]
    
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var view_1:UIView!
    @IBOutlet weak var txtTitleStatus: UITextField!
    @IBOutlet weak var txtTitleComments:UITextField!
    @IBOutlet weak var txtTitleLocation:UITextField!
    @IBOutlet weak var txtAltTitleLocation:UITextField!
    @IBOutlet weak var txtTitleNo:UITextField!
    
    //view2
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var txtTitle_received:UITextField!
    @IBOutlet weak var txtPre_titleState:UITextField!
    @IBOutlet weak var txtTitleSent:UITextField!
    @IBOutlet weak var txtTitle_transferred:UITextField!
    //view3
    @IBOutlet weak var view_3:UIView!
    @IBOutlet weak var txtLien_balance:UITextField!
    @IBOutlet weak var txtLien_name:UITextField!
    @IBOutlet weak var txtLien_Acct:UITextField!
    @IBOutlet weak var txtLien_contact:UITextField!
    @IBOutlet weak var txtLien_phone:UITextField!
    @IBOutlet weak var txtLien_Fax:UITextField!
    @IBOutlet weak var txtLien_Country:UITextField!
    @IBOutlet weak var txtLienAdd_1:UITextField!
    @IBOutlet weak var txtLienAdd_2:UITextField!
    @IBOutlet weak var txtLien_zip:UITextField!
    @IBOutlet weak var txtLien_City:UITextField!
    @IBOutlet weak var txtLien_state:UITextField!
    @IBOutlet weak var txtLien_county:UITextField!
    //COT information
    @IBOutlet weak var view_4:UIView!
    @IBOutlet weak var txtCot_name:UITextField!
    @IBOutlet weak var txtCot_phone:UITextField!
    @IBOutlet weak var txtCot_fax:UITextField!
    @IBOutlet weak var txtCotAdd_1:UITextField!
    @IBOutlet weak var txtCotAdd_2:UITextField!
    @IBOutlet weak var txtCot_zip:UITextField!
    @IBOutlet weak var txtCot_city:UITextField!
    @IBOutlet weak var txtCot_state:UITextField!
    @IBOutlet weak var txtCot_country:UITextField!
    @IBOutlet weak var txtCot_county:UITextField!
    
     var strSelect = ""
    
    var pv_titleStatus: UIPickerView!
    var pv_arrtitleStatus = ["Unknown Status","Clear Title","Flood Title","Recondition Title","Salvage Title","Held Title","No Title"]
    var pv_titleLoc: UIPickerView!
    var pv_arrTitleLoc = ["Unknown Location","[Add Title Location]","test","John More","and test"]
    
    var arrSelectState = NSMutableArray()
    var arrCountry = NSMutableArray()
    
    var pv_lien_Country: UIPickerView!
    var lien_country_id = ""
    
    var pv_lienState:UIPickerView!
    var lienState_id = ""
    
    var pv_Make: UIPickerView!
    
    var pv_cotCountry: UIPickerView!
    var cot_country_id = ""
    
    var pvCot_state: UIPickerView!
    var cot_state_id = ""
    
    var pv_prevTitle:UIPickerView!
    
    var datePickrTitle_Received : UIDatePicker!
    var datePickrTitle_sent: UIDatePicker!
    var datePickrT_transferred : UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        Globalfunc.showLoaderView(view: self.view)
        self.callPickerData()
        // Do any additional setup after loading the view.
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension titlingViewController{
    func callPickerData(){
        
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        
                        if(obj == Constant.country){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    
                                    print(arr)
                                    
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrCountry.add(dictA)
                                        }
                                    }
                                    else{
                                    }
                                }
                            }
                            
                        }
                        
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.callParticularState()
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
}

extension titlingViewController{
    func callParticularState()
    {
        let dict = arrCountry[0] as! NSDictionary
        let _id = dict["_id"] as! Int
        self.lien_country_id = "\(_id)"
        let strGetUrl = "\(Constant.state)/country_id?id=\(_id)"
        self.callApiToSetData(strUrl: strGetUrl, strTyp: "state")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setApi()
        }
    }
    
    func setApi(){
        if let _id = userDef.object(forKey: "inventoryId") as? Int {
            let strGeturl = "\(Constant.inventory)/titling?id=\(_id)"
            self.callApiToSetData(strUrl: strGeturl, strTyp: "all")
            
        }
    }
    func callApiToSetData(strUrl : String, strTyp: String){
        BaseApi.callApiRequestForGet(url: strUrl) { (dict,error ) in
            if(error == ""){
                if ( strTyp == "state"){
                    OperationQueue.main.addOperation {
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrSelectState.add(dictA)
                                    }
                                }
                            }
                        }
                        
                    }
                }
                
                else if ( strTyp == "all" ){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        Globalfunc.print(object: dict)
                        if let arr = dict as? NSDictionary{
                            if let title = arr["data"] as? NSDictionary{
                                
                                if let lienCountry = title["lienCountry"] as? String{
                                    for i in 0..<self.arrCountry.count{
                                        let dict = self.arrCountry[i]
                                            as! NSDictionary
                                        let _id = dict["_id"] as! Int
                                        if (lienCountry == "\(_id))"){
                                            let strTitle = dict["country_name"] as! String
                                            self.txtLien_Country.text = strTitle
                                            self.lien_country_id = "\(_id)"
                                            break
                                        }
                                        
                                    }
                                }
                                if let altTitleLocation  = title["altTitleLocation"] as? String{
                                                                   self.txtAltTitleLocation.text = "\(altTitleLocation)"
                                                               }
                                
                                if let titleTransferred  = title["titleTransferred"] as? String{
                                    self.txtTitle_transferred.text = "\(titleTransferred)"
                                }
                                if let titleSent  = title["titleSent"] as? String{
                                                                   self.txtTitleSent.text = "\(titleSent)"
                                                               }
                                
                                if let cotCountry  = title["cotCountry"] as? String{
                                    self.txtCot_country.text = "\(cotCountry)"
                                }
                                
                                if let cotAddress1  = title["cotAddress1"] as? String{
                                    self.txtCotAdd_1.text = "\(cotAddress1)"
                                }
                                
                                if let cotAddress2  = title["cotAddress2"] as? String{
                                    self.txtCotAdd_2.text = "\(cotAddress2)"
                                }
                                
                                if let cotCity  = title["cotCity"] as? String{
                                    self.txtCot_city.text = "\(cotCity)"
                                }
                                
                                
                                
                                
                                if let cotFax = title["cotFax"] as? String{
                                    self.txtCot_fax.text = "\(cotFax)"
                                }
                                
                                if let cotName = title["cotName"] as? String{
                                    self.txtCot_name.text = "\(cotName)"
                                }
                                
                                if let cotPhone = title["cotPhone"] as? String{
                                    self.txtCot_phone.text = "\(cotPhone)"
                                }
                                
                                if let cotPostalCode = title["cotPostalCode"] as? String{
                                    self.txtCot_zip.text = "\(cotPostalCode)"
                                }
                                
                                

                                
                                if let titleStatus = title["titleStatus"] as? String{
                                    self.txtTitleStatus.text = "\(titleStatus)"
                                }
                                if let titleNumber = title["titleNumber"] as? String{
                                                                   self.txtTitleNo.text = "\(titleNumber)"
                                                               }
                                
                                if let previousTitleState = title["previousTitleState"] as? String{
                                    self.txtPre_titleState.text = "\(previousTitleState)"
                                }
                                if let titleReceived = title["titleReceived"] as? String{
                                                                   self.txtTitle_received.text = "\(titleReceived)"
                                                               }
                                                                                     
                                if let cotState = title["cotState"] as? String{
                                    self.txtCot_state.text = "\(cotState)"
                                }
                                if let lienAddress1 = title["lienAddress1"] as? String{
                                    self.txtLienAdd_1.text = "\(lienAddress1)"
                                }
                                
                                if let lienAddress2 = title["lienAddress2"] as? String{
                                    self.txtLienAdd_2.text = "\(lienAddress2)"
                                }
                                
                                if let lienBalance = title["lienBalance"] as? String{
                                    self.txtLien_balance.text = "\(lienBalance)"
                                }
                                
                                if let lienCity = title["lienCity"] as? String{
                                    self.txtLien_City.text = "\(lienCity)"
                                }
                                
                                if let lienContact = title["lienContact"] as? String{
                                    self.txtLien_contact.text = "\(lienContact)"
                                }
                                if let lienCounty = title["lienCounty"] as? String{
                                    self.txtLien_county.text = "\(lienCounty)"
                                }
                                
                                if let lienFax = title["lienFax"] as? String{
                                    self.txtLien_Fax.text = "\(lienFax)"
                                }
                                if let lienLienAcct = title["lienLienAcct"] as? String{
                                    self.txtLien_Acct.text = "\(lienLienAcct)"
                                }
                                if let lienName = title["lienName"] as? String{
                                    self.txtLien_name.text = "\(lienName)"
                                }
                                if let lienPhone = title["lienPhone"] as? String{
                                    self.txtLien_phone.text = "\(lienPhone)"
                                }
                                if let lienPostalCode = title["lienPostalCode"] as? String{
                                    self.txtLien_zip.text = "\(lienPostalCode)"
                                }
                                if let lienState = title["lienState"] as? String{
                                    self.txtLien_state.text = "\(lienState)"
                                }
                                if let titleComments = title["titleComments"] as? String{
                                    self.txtTitleComments.text = "\(titleComments)"
                                }
                                
                                
                                
                                
                                
                            }
                        }
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
            
        }
    }
}
extension titlingViewController: UIPickerViewDelegate , UIPickerViewDataSource ,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
       
            if(pickerView == pv_lien_Country || pickerView == pv_cotCountry){
                       return arrCountry.count
                   }
            else if(pickerView == pv_lienState || pickerView == pvCot_state){
                return arrSelectState.count
            }
            else if(pickerView == pv_titleStatus){
                       return pv_arrtitleStatus.count
                   }
            else if(pickerView == pv_titleLoc){
                       return pv_arrTitleLoc.count
                   }
            else if(pickerView == pv_prevTitle){
                       return arrSelectState.count
                   }
        else{
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
         if (pickerView == pv_titleStatus)
        {
            let strTitle = pv_arrtitleStatus[row]
            return strTitle
        }
        else if (pickerView == pv_titleLoc)
        {
            let strTitle = pv_arrTitleLoc[row]
            return strTitle
        }
            else if (pickerView == pv_prevTitle)
            {
               let dict = arrSelectState[row] as! NSDictionary
               let strTitle =  dict["state_name"] as! String
               return strTitle
            }
        else if(pickerView == pv_lien_Country  || pickerView == pv_cotCountry){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            return strTitle
        }
        else if(pickerView == pv_lienState || pickerView == pvCot_state){
            let dict = arrSelectState[row] as! NSDictionary
            let strTitle = dict["state_name"] as! String
            return strTitle
        }
        
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView == pv_lien_Country || pickerView == pv_cotCountry){
            let dict = arrCountry[row] as! NSDictionary
            let strTitle = dict["country_name"] as! String
            
            if(pickerView == pv_lien_Country){
                self.txtLien_Country.text = strTitle
                let _id = dict["_id"] as! Int
                self.lien_country_id = "\(_id)"
                
            }
            else if (pickerView == pv_cotCountry){
                self.txtCot_country.text = strTitle
                let _id = dict["_id"] as! Int
                self.cot_country_id = "\(_id)"
            }
            else if (pickerView == pv_lienState || pickerView == pvCot_state){
                let dict = arrSelectState[row] as!NSDictionary
                let strTitl = dict["state_name"] as! String
                if( pickerView == pv_lienState ){
                    self.txtLien_state.text = strTitl
                    let _id = dict["_id"] as! Int
                    self.lienState_id = "_\(_id)"
                    
                }
                else if (pickerView == pvCot_state){
                    self.txtCot_state.text = strTitl
                    let _id = dict["_id"] as! Int
                    self.cot_state_id = "_\(_id)"
                }
                else if(pickerView == pv_titleStatus){
                    self.txtTitleStatus.text =  pv_arrtitleStatus[row]
                }
                else if(pickerView == pv_titleLoc){
                    self.txtTitleLocation.text = pv_arrTitleLoc[row]
                    
                }
                else if(pickerView == pv_prevTitle){
                    let dict = arrSelectState[row] as!NSDictionary
                    let strTitl = dict["state_name"] as! String
                    self.txtPre_titleState.text = strTitl
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == self.txtLien_Country){
            self.pickUp(txtLien_Country)
        }
        else if ( textField == self.txtCot_country){
            self.pickUp(txtCot_country)
        }
        else if (textField == self.txtTitle_received){
            self.pickUpDateTime(self.txtTitle_received)
        }
        else if(textField == self.txtTitleStatus){
            self.pickUp(txtTitleStatus)
        }
        else if(textField == self.txtTitleLocation){
            self.pickUp(txtTitleLocation)
        }
        else if(textField == self.txtLien_Country){
            self.pickUp(txtLien_Country)
        }
        else if(textField == self.txtCot_country){
            self.pickUp(txtCot_country)
        }
        else if(textField == self.txtLien_state){
            self.pickUp(txtLien_state)
        }
        else if(textField == self.txtCot_state){
            self.pickUp(txtCot_state)
        }
        else if(textField == self.txtPre_titleState){
            self.pickUp(txtPre_titleState)
        }
        else if(textField == self.txtTitle_received){
            self.pickUpDateTime(txtTitle_received)
        }
        
    }
    
    func pickUp(_ textField : UITextField){
        if(textField == self.txtLien_Country){
            self.pv_lien_Country = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_lien_Country.delegate = self
            self.pv_lien_Country.dataSource = self
            self.pv_lien_Country.backgroundColor = UIColor.white
            textField.inputView = self.pv_lien_Country
        }
        else if(textField == self.txtCot_country){
            self.pv_cotCountry = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_cotCountry.delegate = self
            self.pv_cotCountry.dataSource = self
            self.pv_cotCountry.backgroundColor = UIColor.white
            textField.inputView = self.pv_cotCountry
        }
        
        else if(textField == self.txtTitleStatus){
                   self.pv_titleStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_titleStatus.delegate = self
                   self.pv_titleStatus.dataSource = self
                   self.pv_titleStatus.backgroundColor = UIColor.white
                   textField.inputView = self.pv_titleStatus
               }
               
               else if(textField == self.txtTitleLocation){
                   self.pv_titleLoc = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_titleLoc.delegate = self
                   self.pv_titleLoc.dataSource = self
                   self.pv_titleLoc.backgroundColor = UIColor.white
                   textField.inputView = self.pv_titleLoc
               }
              
             
               
               else if(textField == self.txtLien_state){
                   self.pv_lienState = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_lienState.delegate = self
                   self.pv_lienState.dataSource = self
                   self.pv_lienState.backgroundColor = UIColor.white
                   textField.inputView = self.pv_lienState
               }
               else if(textField == self.txtPre_titleState){
                   self.pv_prevTitle = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_prevTitle.delegate = self
                   self.pv_prevTitle.dataSource = self
                   self.pv_prevTitle.backgroundColor = UIColor.white
                   textField.inputView = self.pv_prevTitle
               }
               else if(textField == self.txtCot_state){
                   self.pvCot_state = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pvCot_state.delegate = self
                   self.pvCot_state.dataSource = self
                   self.pvCot_state.backgroundColor = UIColor.white
                   textField.inputView = self.pvCot_state
               }
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtTitle_received){
            self.datePickrTitle_Received = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePickrTitle_Received.datePickerMode = .date
            self.datePickrTitle_Received.backgroundColor = UIColor.white
            textField.inputView = self.datePickrTitle_Received
            self.strSelect = "titleReceived"
        }
        else if(textField == self.txtTitleSent){
            self.datePickrTitle_sent = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePickrTitle_sent.datePickerMode = .date
            self.datePickrTitle_sent.backgroundColor = UIColor.white
            textField.inputView = self.datePickrTitle_sent
            self.strSelect = "titleSent"
        }
        else if(textField == self.txtTitle_transferred){
            self.datePickrT_transferred = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePickrT_transferred.datePickerMode = .date
            self.datePickrT_transferred.backgroundColor = UIColor.white
            textField.inputView = self.datePickrT_transferred
            self.strSelect = "titleTransferred"
        }

        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick() {
        txtLien_Country.resignFirstResponder()
        txtCot_country.resignFirstResponder()
        txtTitleStatus.resignFirstResponder()
        txtPre_titleState.resignFirstResponder()
        txtTitleLocation.resignFirstResponder()
        txtLien_state.resignFirstResponder()
        txtCot_state.resignFirstResponder()
    }
    @objc func doneClickDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
       
        if (self.strSelect == "titleSent")
       {
        self.txtTitleSent.text = formatter.string(from: datePickrTitle_sent.date)
        self.txtTitleSent.resignFirstResponder()
        
        }
        else if (self.strSelect == "titleTransferred")
        {
            self.txtTitle_transferred.text = formatter.string(from: datePickrT_transferred.date)
        self.txtTitle_transferred.resignFirstResponder()
        }
        else if (self.strSelect == "titleReceived"){
            
        }
        
        self.txtTitle_received.text = formatter.string(from: datePickrTitle_Received.date)
        self.txtTitle_received.resignFirstResponder()
        
        
    }
    
    @objc func cancelClickDate() {
        txtTitle_received.resignFirstResponder()
        txtTitleSent.resignFirstResponder()
        txtTitle_transferred.resignFirstResponder()
    }
    
    func callUpdateApi(param: [String : Any])
    {
        let strUrl = "\(Constant.inventory)/titling?id"
        BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}



