//
//  inventoryNotesVc.swift
//  LMS
//
//  Created by Dr.Mac on 01/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//



import UIKit

class inventoryNotesVc: UIViewController {
    
    @IBOutlet weak var tblNotes : UITableView!
    
    var arrData : NSMutableArray = []
    
    
    var pickerViewAction: UIPickerView!
    var pickerViewUser: UIPickerView!
    
    var arrAction : [String] = []
    var arrUser : [String] = []
    
    @IBOutlet weak var txtAction : UITextField!
    @IBOutlet weak var txtUser : UITextField!
    
    
    @IBOutlet weak var txtPagination: UITextField!
    @IBOutlet weak var scrollViewPages: UIScrollView!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    @IBOutlet weak var lblResultfound: UILabel!
    
    let buttonPadding:CGFloat = 10
    var xOffset:CGFloat = 10
    var arrPAginationNo = ["10","25","50","75","100","200","250"]
    var pickerViewPages: UIPickerView!
    var defaultPage = 10
    
    @IBOutlet weak var lblInventoryNo : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblNotes.tableFooterView = UIView()
        self.txtPagination.text = self.arrPAginationNo[0]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.callApi()
    }
    
    func callApi(){
        self.arrData = []
        Globalfunc.showLoaderView(view: self.view)
        
        //change url
        
        if let _id  = userDef.object(forKey: "inventoryId") as? Int {
            self.lblInventoryNo.text = "Inventory: \(_id)"
            
            let str_url = "\(Constant.inventory_notes)/search?inventory_id=\(_id)"
            self.callNotesApi(strUrl: str_url)
        }
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension inventoryNotesVc {
    
    //change keys
    
    func setDataonPage(_ dict : NSDictionary)
    {
        if let data = dict["data"] as? NSDictionary{
            
            print(data)
            
            let currentPageNumber = data["currentPageNumber"] as! Int
            let defaultRecordsPage = data["defaultRecordsPage"] as! Int
            
            let record = data["totalrecord"] as! Int
            self.lblResultfound.text = "\(record) RESULTS FOUND"
            
            let cpNo = CGFloat(currentPageNumber)
            let pagSize = CGFloat(defaultRecordsPage)
            let totalR = CGFloat(record)
            
            let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
            let pagecount = Int(pageNo)
            
            let buttonPadding:CGFloat = 10
            var xOffset:CGFloat = 10
            
            for i in 0..<pagecount {
                if (i == 0){
                    self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
                }
                let button = UIButton()
                button.tag = i+1
                button.setTitle("\(i+1)", for: .normal)
                button.layer.borderWidth = 1
                button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
                button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
                button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
                button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
                button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
                xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
                self.scrollViewPages.addSubview(button)
            }
            self.scrollWidth.constant = xOffset
            self.scrollViewPages.contentSize = CGSize(width: xOffset, height: self.scrollViewPages.frame.height)
            
            if let dataArr = data["data"] as? NSMutableArray{
                self.arrData = dataArr
                if(dataArr.count > 0){
                    for i in 0..<dataArr.count{
                        let dd = dataArr[i] as! NSDictionary
                        if let action = dd["action"] as? String{
                            self.arrAction.append(action)
                        }
                        if let initials = dd["user"] as? String{
                            self.arrUser.append(initials)
                        }
                    }
                    self.arrAction = self.arrAction.removingDuplicates()
                    self.arrUser = self.arrUser.removingDuplicates()
                }
                self.tblNotes.reloadData()
            }
        }
    }
}

//call getNotes Api
extension inventoryNotesVc {
    
    func callNotesApi(strUrl : String)
    {
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let responseDict = dict as? NSDictionary {
                        self.setDataonPage(responseDict)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    @objc func btnClick(_ sender : UIButton){
        sender.pulsate()
    }
    
    
}

extension inventoryNotesVc : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblNotes.dequeueReusableCell(withIdentifier: "notesTblCell") as! notesTblCell
        
        let dict = self.arrData[indexPath.section] as! NSDictionary
        if let user = dict["user"] as? String{
            cell.lblUser.text = user
        }
        
        if let result = dict["result"] as? String{
            cell.lblReulst.text = result
        }
        else{
            cell.lblReulst.text = ""
        }
        
        if let initials = dict["initials"] as? String{
            cell.lblAction.text = initials
        }
        
        if let description = dict["description"] as? [String]{
            var string = ""
            for value in description {
                string += value
            }
            cell.lblDesciption.text = string
        }
        
        if let created_at = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "MM-dd-yyyy"
            let date = Date.dateFromISOString(string: created_at)
            let formatDate = formatter.string(from: date!)
            cell.lblDate.text = formatDate
        }
        
        if let created_at = dict["created_at"] as? String{
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let date = Date.dateFromISOString(string: created_at)
            let formatDate = formatter.string(from: date!)
            cell.lblTime.text = formatDate
        }
        
        return cell
    }
    
}

extension inventoryNotesVc : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pickerViewAction{
            return self.arrAction.count
        }
        else if(pickerView == pickerViewUser){
            return self.arrUser.count
        }
        if(pickerView == pickerViewPages){
            return self.arrPAginationNo.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView == pickerViewAction{
            let strTitle = arrAction[row]
            return strTitle
        }
        
        else if(pickerView == pickerViewUser){
            let strTitle = arrUser[row]
            return strTitle
        }
        
        if(pickerView == pickerViewPages){
            return self.arrPAginationNo[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerViewAction{
            self.txtAction.text = (arrAction[row] )
        }
        
        else if(pickerView == pickerViewUser){
            self.txtUser.text = (arrUser[row])
        }
        
        if(pickerView == pickerViewPages){
            self.txtPagination.text = self.arrPAginationNo[row]
        }
        
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtAction){
            self.pickUp(txtAction)
        }
        
        else if(textField == self.txtUser){
            self.pickUp(txtUser)
        }
        
        if(textField == self.txtPagination){
            self.pickUp(txtPagination)
        }
    }
    
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtAction){
            
            self.pickerViewAction = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewAction.delegate = self
            self.pickerViewAction.dataSource = self
            self.pickerViewAction.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewAction
        }
        
        else if(textField == self.txtUser){
            
            self.pickerViewUser = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewUser.delegate = self
            self.pickerViewUser.dataSource = self
            self.pickerViewUser.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewUser
        }
        
        if(textField == self.txtPagination){
            self.pickerViewPages = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPages.delegate = self
            self.pickerViewPages.dataSource = self
            self.pickerViewPages.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPages
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        
        txtAction.resignFirstResponder()
        txtUser.resignFirstResponder()
        txtPagination.resignFirstResponder()
        
        self.arrData = []
        Globalfunc.showLoaderView(view: self.view)
        
        
        do {
            let decoded  = userDef.object(forKey: "dataDict") as! Data
            if let dataD = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(decoded) as? NSDictionary {
                
                let pagecount = Int(self.txtPagination.text!)
                let accId_selected = dataD["_id"] as! Int
                let params = ["account_id":accId_selected,
                              "currentPageNumber":1,
                              "action":"\(self.txtAction.text!)",
                              "user":"\(self.txtUser.text!)",
                              "results":"",
                              "promiseType":"",
                              "notes_type":"collateral",
                              "defaultRecordsPage":pagecount!] as [String : Any]
                
                BaseApi.onResponsePostWithToken(url: Constant.getNotes_url, controller: self, parms: params) { (dict, error) in
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        print(dict)
                        
                        if let responseDict = dict as? NSDictionary {
                            self.setDataonPage(responseDict)
                        }
                    }
                }
            }
        }
        catch{
            Globalfunc.print(object: "Couldn't read file.")
        }
    }
    
    @objc func cancelClick() {
        txtAction.resignFirstResponder()
        txtUser.resignFirstResponder()
    }
}
