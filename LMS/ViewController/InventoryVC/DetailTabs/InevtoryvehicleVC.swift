//
//  InevtoryvehicleVC.swift
//  LMS
//
//  Created by Apple on 19/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class InevtoryvehicleVC: UIViewController {
    //view1
    @IBOutlet weak var txtPrimaryLot: UITextField!
    @IBOutlet weak var txtActualMileage:UITextField!
    @IBOutlet weak var txtAct_MilStatus:UITextField!
    @IBOutlet weak var txtVehicleStatus:UITextField!
    //view2
    @IBOutlet weak var txtNew_Used:UITextField!
    @IBOutlet weak var txtYear:UITextField!
    @IBOutlet weak var txtMake:UITextField!
    @IBOutlet weak var txtModel:UITextField!
    @IBOutlet weak var txtTrim:UITextField!
    @IBOutlet weak var txtExt_color:UITextField!
    @IBOutlet weak var txtInt_trim:UITextField!
    @IBOutlet weak var txtBodyStyle:UITextField!
    @IBOutlet weak var txtTransmission:UITextField!
    @IBOutlet weak var txtDriveType:UITextField!
    @IBOutlet weak var txtEngine:UITextField!
    @IBOutlet weak var txtFuel:UITextField!
    @IBOutlet weak var txtWeight:UITextField!
    @IBOutlet weak var txtCondition:UITextField!
    @IBOutlet weak var txtType:UITextField!
    
    @IBOutlet weak var  Ext_primaryClr:UITextField!
    @IBOutlet weak var  Ext_secondaryClr:UITextField!
    //view3
    @IBOutlet weak var txtDealerTag:UITextField!
    @IBOutlet weak var txtDealerDate :UITextField!
    @IBOutlet weak var txtBuyerTag:UITextField!
    @IBOutlet weak var txtBuyerDate:UITextField!
    @IBOutlet weak var txtSuppl_Buyer:UITextField!
    @IBOutlet weak var txtSuppl_BuyerDate :UITextField!
    
    //view4
    @IBOutlet weak var txtAltLocation:UITextField!
    
    //view5
    @IBOutlet weak var txtUserDef:UITextField!
    @IBOutlet weak var txtPlateNumber:UITextField!
    @IBOutlet weak var txtPlate_ExpMonth:UITextField!
    @IBOutlet weak var txtPlate_ExpDateYr:UITextField!
    @IBOutlet weak var txtInsp_ExpMonth:UITextField!
    @IBOutlet weak var txtInsp_ExpYr:UITextField!
    @IBOutlet weak var txtValuation_No_: UITextField!
    
    @IBOutlet weak var txtEmissions:UITextField!
    @IBOutlet weak var txtRecovery_Type:UITextField!
    @IBOutlet weak var txtRecovery_Functionality:UITextField!
    @IBOutlet weak var Recovery:UITextField!
    @IBOutlet weak var txtGpsExpDate:UITextField!
    @IBOutlet weak var AdditionalNotes:UITextView!
    
    var strSelect = ""
    
    var pv_Act_status: UIPickerView!
    var arrAct_status = ["Actual Miles","True Mileage Unknown","Beyond Mechanical Limits","Exempt"]
    
    var pv_Veh_status: UIPickerView!
    var arrVeh_status = ["Evaluating"]
    
    var pv_New_Used: UIPickerView!
    var arrNew_Used = ["New","Used"]
    
    var pv_Year: UIPickerView!
    var arrYear = ["Custom Year"]
    
    var arrModel = ["Custom Model"]
    var pv_Model: UIPickerView!
    
    
    var pv_Make: UIPickerView!
    var arrMake = [String]()
    
    var pv_trim: UIPickerView!
    var arrtrim = ["Custom Trim"]
    
    var pv_Bodystyle: UIPickerView!
    var arrBodyStyle = ["Custom Bodystyle"]
    
    var pv_Transmisson: UIPickerView!
    var arr_transmisson = ["Custom Transmission"]
    
    var pv_DriveType : UIPickerView!
    var arrDriveType = ["Custom Drive Type"]
    
    
    var pv_Engine : UIPickerView!
    var arrEngine = ["Custom Engine"]
    
    var pv_Fuel : UIPickerView!
    var arrFuel = ["Custom Fuel"]
    
    var pv_Condition:UIPickerView!
    var arrCondition = ["Unknown","Rough","Average","Clean","Extra Clean"]
    
    var pv_Type: UIPickerView!
    var arrType = ["Unknown","Car(Vehicle)","Truck(Vehicle)","Motorcycle","Recreational vehicle","Boat","Equipment","Aircraft","Powersports","Other","SUV","VAN"]
    
    var arrColor = ["Beige","Black","Blue","Brown"]
    var pv_extColor : UIPickerView!
    var pv_extPrim_color:UIPickerView!
    var pv_extSec_color :UIPickerView!
    
    var pv_plateExpMonth:UIPickerView!
    var pv_plateExpYr:UIPickerView!
    var pv_inspectnExpMonth:UIPickerView!
    var pv_inspectnExpYr:UIPickerView!
    var pv_recoveryType:UIPickerView!
    var pv_recoveryFunc:UIPickerView!
    var pv_intTrim : UIPickerView!
    var Dealer_datePickr:UIDatePicker!
    var buyer_datePickr:UIDatePicker!
    var suppl_datePickr:UIDatePicker!
    var gps_expDatePickr: UIDatePicker!

    var dataDict = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.callParticularSate()
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension InevtoryvehicleVC {
    
    
    func callParticularSate()
    {
        if let _id  = userDef.object(forKey: "inventoryId") as? Int {
            let strGetUrl = "\(Constant.inventory)/vehicle?id=\(_id)"
            self.callApiToSetData(strUrl: strGetUrl)
        }
        
    }
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        if let vehicle = arr["data"] as? NSDictionary{

                            print(vehicle)

                            //                            self.dataDict = vehicle
//
                            if let acquiredMileage = vehicle["acquiredMileage"] as? Int{
                                self.txtActualMileage.text = "\(acquiredMileage)"
                            }
//                            //
                            if let mileageStatus = vehicle["mileageStatus"] as? String{
                                self.txtAct_MilStatus.text = mileageStatus
                            }
//
//                           
                            if let tempDealerTagIssueDate = vehicle["tempDealerTagIssueDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: tempDealerTagIssueDate)
                                self.txtDealerDate.text = formatter.string(from: date!)
                            }
                            if let tempBuyerTagIssueDate = vehicle["tempBuyerTagIssueDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: tempBuyerTagIssueDate)
                                self.txtBuyerDate.text = formatter.string(from: date!)
                            }
                            if let buyerSupplIssueDate = vehicle["buyerSupplIssueDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: buyerSupplIssueDate)
                                self.txtSuppl_Buyer.text = formatter.string(from: date!)
                            }
                            if let gspExpDate = vehicle["gspExpDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: gspExpDate)
                                self.txtGpsExpDate.text = formatter.string(from: date!)
                            }
//
//
//                            //
//                            if let vehStatus = vehicle["vehStatus"] as? String{
//                                self.txtVehicleStatus.text = "\(vehStatus)"
//                            }
//
//                            //key = newFlag?
//                            if let newUsed = vehicle["newUsed"] as? String{
//                                self.txtNew_Used.text = "\(newUsed)"
//                            }
//                            //
                            if let yearModel = vehicle["yearModel"] as? Int{
                                self.txtYear.text = "\(yearModel)"
                            }
//                            //
                            if let make = vehicle["make"] as? Int{
                                self.txtMake.text = "\(make)"
                            }
                            if let model = vehicle["model"] as? Int{
                                self.txtModel.text = "\(model)"
                            }
                            if let trim = vehicle["trim"] as? Int{
                                self.txtTrim.text = "\(trim)"
                            }
                            if let exteriorColor = vehicle["exteriorColor"] as? Int{
                                self.txtExt_color.text = "\(exteriorColor)"
                            }
                            if let intetiorTrim = vehicle["intetiorTrim"] as? Int{
                                self.txtInt_trim.text = "\(intetiorTrim)"
                            }
                            if let bodyStyle = vehicle["bodyStyle"] as? Int{
                                self.txtBodyStyle.text = "\(bodyStyle)"
                            }
                            if let transmission = vehicle["transmission"] as? Int{
                                self.txtTransmission.text = "\(transmission)"
                            }
                            if let driveType = vehicle["driveType"] as? Int{
                                self.txtDriveType.text = "\(driveType)"
                            }
                            if let engine = vehicle["engine"] as? Int{
                                self.txtEngine.text = "\(engine)"
                            }
                            if let fuelType = vehicle["fuelType"] as? Int{
                                self.txtFuel.text = "\(fuelType)"
                            }
                            if let weight = vehicle["weight"] as? Int{
                                self.txtWeight.text = "\(weight)"
                            }
                            if let condition = vehicle["condition"] as? Int{
                                self.txtCondition.text = "\(condition)"
                            }
                            if let type = vehicle["type"] as? Int{
                                self.txtType.text = "\(type)"
                            }
                            if let exteriorPrimaryColor = vehicle["exteriorPrimaryColor"] as? Int{
                                self.Ext_primaryClr.text = "\(exteriorPrimaryColor)"
                            }
//
                            if let exteriorSecondaryColor = vehicle["exteriorSecondaryColor"] as? Int{
                                self.Ext_secondaryClr.text = "\(exteriorSecondaryColor)"
                            }
//
                            if let tempDealerTag = vehicle["tempDealerTag"] as? Int{
                                self.txtDealerTag.text = "\(tempDealerTag)"
                            }
                            if let tempBuyerTag = vehicle["tempBuyerTag"] as? Int{
                                self.txtBuyerTag.text = "\(tempBuyerTag)"
                            }
                            if let buyerSupplTag = vehicle["buyerSupplTag"] as? Int{
                                self.txtSuppl_Buyer.text = "\(buyerSupplTag)"
                            }
                            if let userDefined = vehicle["userDefined"] as? Int{
                                self.txtUserDef.text = "\(userDefined)"
                            }
                            if let plateNumber = vehicle["plateNumber"] as? Int{
                                self.txtPlateNumber.text = "\(plateNumber)"
                            }
                            if let plateExpireMongth = vehicle["plateExpireMongth"] as? Int{
                                self.txtPlate_ExpMonth.text = "\(plateExpireMongth)"
                            }
                            if let plateExpireYear = vehicle["plateExpireYear"] as? Int{
                                self.txtPlate_ExpDateYr.text = "\(plateExpireYear)"
                            }
                            if let inspectedExpireMonth = vehicle["inspectedExpireMonth"] as? Int{
                                self.txtInsp_ExpMonth.text = "\(inspectedExpireMonth)"
                            }
                            if let inspectedExpireYear = vehicle["inspectedExpireYear"] as? Int{
                                self.txtInsp_ExpYr.text = "\(inspectedExpireYear)"
                            }
                            if let emissions = vehicle["emissions"] as? Int{
                                self.txtEmissions.text = "\(emissions)"
                            }
                            if let recoveryType = vehicle["recoveryType"] as? Int{
                                self.txtRecovery_Type.text = "\(recoveryType)"
                            }
                            if let recoveryFuncationality = vehicle["recoveryFuncationality"] as? Int{
                                self.txtRecovery_Functionality.text = "\(recoveryFuncationality)"
                            }
                            if let recoveryESN = vehicle["recoveryESN"] as? Int{
                                self.Recovery.text = "\(recoveryESN)"
                            }
                            if let vehicleNotes = vehicle["vehicleNotes"] as? Int{
                                self.AdditionalNotes.text = "\(vehicleNotes)"
                            }

                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension InevtoryvehicleVC: UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pv_Act_status{
            return self.arrAct_status.count
        }
        else if pickerView == pv_Veh_status{
            return self.arrVeh_status.count
        }
        else if pickerView == pv_Fuel{
            return self.arrFuel.count
        }
        else if pickerView == pv_Make{
            return self.arrMake.count
        }
        else if pickerView == pv_trim{
            return self.arrtrim.count
        }
        else if pickerView == pv_Type{
            return self.arrType.count
        }
        else if pickerView == pv_Year{
            return self.arrYear.count
        }
        else if pickerView == pv_Bodystyle{
            return self.arrBodyStyle.count
        }
        
        else if pickerView == pv_DriveType{
            return self.arrDriveType.count
        }
        else if pickerView == pv_Condition{
            return self.arrCondition.count
        }
        else if pickerView == pv_Engine{
            return self.arrEngine.count
        }
        else if pickerView == pv_New_Used{
            return self.arrNew_Used.count
        }
        else if pickerView == pv_Model{
            return self.arrModel.count
        }
        else if (pickerView == pv_plateExpMonth) || (pickerView == pv_inspectnExpMonth){
            return consArrays.arrMonth.count
        }
        else if (pickerView == pv_plateExpYr) || ( pickerView == pv_inspectnExpYr){
            return consArrays.arrYr.count
        }
        
        else if (pickerView == pv_recoveryType){
            return consArrays.arrRecoveryTyp.count
        }
        else{
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pv_Act_status{
            let strTitle = arrAct_status[row]
            return strTitle
        }
        else if(pickerView == pv_Veh_status){
            let strTitle = arrVeh_status[row]
            return strTitle
        }
        
        else if(pickerView == pv_Year){
            let strTitle = arrYear[row]
            return strTitle
        }
        else if(pickerView == pv_Model){
            let strTitle = arrModel[row]
            return strTitle
            
        }
        else if(pickerView == pv_Bodystyle){
            let strTitle = arrBodyStyle[row]
            return strTitle
        }
        else if(pickerView == pv_Type){
            let strTitle = arrType[row]
            return strTitle
        }
        else if(pickerView == pv_Engine){
            let strTitle = arrEngine[row]
            return strTitle
        }
        else if(pickerView == pv_Fuel){
            let strTitle = arrFuel[row]
            return strTitle
        }
        else if(pickerView == pv_Condition){
            let strTitle = arrCondition[row]
            return strTitle
        }
        else if(pickerView == pv_New_Used){
            let strTitle = arrNew_Used[row]
            return strTitle
            
        }
        else if(pickerView == pv_Make){
            let strTitle = arrMake[row]
            return strTitle
        }
        
        else if(pickerView == pv_trim){
            let strTitle = arrtrim[row]
            return strTitle
        }
        
        else if(pickerView == pv_Transmisson){
            let strTitle = arr_transmisson[row]
            return strTitle
        }
        
        else if(pickerView == pv_DriveType){
            let strTitle = arrDriveType[row]
            return strTitle
        }
        else if (pickerView == pv_plateExpMonth) || (pickerView == pv_inspectnExpMonth){
            return consArrays.arrMonth[row]
        }
        else if (pickerView == pv_plateExpYr) || ( pickerView == pv_inspectnExpYr){
            return consArrays.arrYr[row]
        }
        else if(pickerView == pv_extPrim_color || pickerView == pv_extSec_color){
            let strTitle = arrColor[row]
            return strTitle
        }
        return ""
        
    }
    func pickUpDate(_ textField : UITextField) {
        
        if(textField == self.txtDealerDate){
            self.Dealer_datePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.Dealer_datePickr.datePickerMode = .date
            self.Dealer_datePickr.backgroundColor = UIColor.white
        }
        if(textField == self.txtBuyerDate){
            self.buyer_datePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.buyer_datePickr.datePickerMode = .date
            self.buyer_datePickr.backgroundColor = UIColor.white
        }
        if(textField == self.txtSuppl_Buyer){
            self.suppl_datePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.suppl_datePickr.datePickerMode = .date
            self.suppl_datePickr.backgroundColor = UIColor.white
        }
        if(textField == self.txtGpsExpDate){
            self.gps_expDatePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.gps_expDatePickr.datePickerMode = .date
            self.gps_expDatePickr.backgroundColor = UIColor.white
            //1.ERROR:Use of unresolved identifier 'UIDatePickerStyle' while running the code
            //2.tried clean build folder - not helpful
            //need to declare UIDatePickerStyle
            
//            if #available(iOS 13.4, *) {
//                self.gps_expDatePickr.preferredDatePickerStyle = UIDatePickerStyle.wheels
//            } else {
//            }
            
            textField.inputView = self.Dealer_datePickr
            self.strSelect = "Dealer date"
        }
        else if(textField == self.txtBuyerDate){
            self.buyer_datePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.buyer_datePickr.datePickerMode = .date
            self.buyer_datePickr.backgroundColor = UIColor.white
            textField.inputView = self.buyer_datePickr
            self.strSelect = "Buyer Date"
        }
        else if(textField == self.txtSuppl_BuyerDate){
            self.suppl_datePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.suppl_datePickr.datePickerMode = .date
            self.suppl_datePickr.backgroundColor = UIColor.white
            textField.inputView = self.suppl_datePickr
            self.strSelect = "suppl Date"
        }
        else if(textField == self.txtGpsExpDate){
            self.gps_expDatePickr = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.gps_expDatePickr.datePickerMode = .date
            self.gps_expDatePickr.backgroundColor = UIColor.white
            textField.inputView = self.gps_expDatePickr
            self.strSelect = "gps exp date"
        }
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    

    
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        if(self.strSelect == "Dealer date"){
            self.txtDealerDate.text = formatter.string(from: Dealer_datePickr.date)
            self.txtDealerDate.resignFirstResponder()
            
        }
        else if(self.strSelect == "Buyer Date"){
            self.txtBuyerDate.text = formatter.string(from: Dealer_datePickr.date)
            self.txtBuyerDate.resignFirstResponder()
            
        }
        else if(self.strSelect == "suppl Date"){
            self.txtSuppl_BuyerDate.text = formatter.string(from: suppl_datePickr.date)
            self.txtSuppl_BuyerDate.resignFirstResponder()
            
        }
        else if(self.strSelect == "gps exp date"){
            self.txtGpsExpDate.text = formatter.string(from: gps_expDatePickr.date)
            self.txtGpsExpDate.resignFirstResponder()
            
        }
    }
    @objc func cancelClickDate() {
        txtDealerDate.resignFirstResponder()
        txtBuyerDate.resignFirstResponder()
        txtSuppl_BuyerDate.resignFirstResponder()
        txtGpsExpDate.resignFirstResponder()
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    @objc func doneClick() {
        txtAct_MilStatus.resignFirstResponder()
        txtVehicleStatus.resignFirstResponder()
        txtNew_Used.resignFirstResponder()
        txtYear.resignFirstResponder()
        txtMake.resignFirstResponder()
        txtModel.resignFirstResponder()
        txtTrim.resignFirstResponder()
        txtExt_color.resignFirstResponder()
        txtInt_trim.resignFirstResponder()
        txtBodyStyle.resignFirstResponder()
        txtTransmission.resignFirstResponder()
        txtDriveType.resignFirstResponder()
        txtEngine.resignFirstResponder()
        txtFuel.resignFirstResponder()
        txtCondition.resignFirstResponder()
        txtType.resignFirstResponder()
        Ext_primaryClr.resignFirstResponder()
        Ext_secondaryClr.resignFirstResponder()
        
    }
}



extension InevtoryvehicleVC {
    
    @IBAction func clickSaveBtn(_ sender: UIButton){
        
        if sender.tag == 10
        {
            self.dismiss(animated: true, completion: nil)
        }
        else if sender.tag == 20{
          
            let inventory_id = self.dataDict["_id"] as! Int // error : Unexpectedly found nil while unwrapping an Optional value
            let params = [
                "acquiredMileage": self.txtActualMileage.text!,
                "altLocation": self.txtAltLocation.text!,
                "bodyStyle": self.txtBodyStyle.text!,
                "buyerSupplIssueDate": self.txtSuppl_BuyerDate.text!,
                "buyerSupplTag": self.txtBuyerTag.text!,
                "condition": self.txtCondition.text!,
                "driveType": self.txtDriveType.text!,
                "emissions": self.txtEmissions.text!,
                "engine": self.txtEngine.text!,
                "exteriorColor": self.txtExt_color.text!,
                "exteriorPrimaryColor": self.Ext_primaryClr.text!,
                "exteriorSecondaryColor": self.Ext_secondaryClr.text!,
                "fuelType": self.txtFuel.text!,
                "gspExpDate": self.txtGpsExpDate.text!,
                "id": inventory_id,//??
                
                //
                "insepctedBy3rdParty": "",
                "insid": (user?.institute_id!)!,
                "inspectedExpireMonth": self.txtInsp_ExpMonth.text!,
                "inspectedExpireYear": self.txtInsp_ExpYr.text!,
                "intetiorTrim": self.txtInt_trim.text!,
                //
                "inventoryStatus": "",
                "make": self.txtMake.text!,
                "mileageStatus": self.txtAct_MilStatus.text!,
                "model": self.txtModel.text!,
                //??
                "newFlag": "",
                "plateExpireMongth": self.txtPlate_ExpMonth.text!,
                "plateExpireYear": self.txtPlate_ExpDateYr.text!,
                "plateNumber": self.txtPlateNumber.text!,
                "recoveryESN": self.Recovery.text!,
                "recoveryFuncationality": self.txtRecovery_Functionality.text!,
                "recoveryType": self.txtRecovery_Type.text!,
                "stock": self.txtGpsExpDate.text!,
                "tempBuyerTag": self.txtBuyerTag.text!,
                "tempBuyerTagIssueDate": self.txtBuyerDate.text!,
                "tempDealerTag": self.txtDealerTag.text!,
                "tempDealerTagIssueDate":self.txtDealerTag.text!,
                "transmission": self.txtTransmission.text!,
                "trim": self.txtTrim.text!,
                "type": self.txtType.text!,
                "userDefined": self.txtUserDef.text!,
                "userid": (user?._id)!,
                "valuationNumber": self.txtValuation_No_.text!,
                //
                "vehicleLot": "",
                "vehicleNotes":"",
                "vin": self.txtGpsExpDate.text!,
                "weight": self.txtWeight.text!,
                "yearModel": self.txtYear.text! ] as [String : Any]
            
            
            Globalfunc.print(object: params)
            self.callUpdateApi(param: params)
        }
    }
   func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtDealerDate){
            self.pickUpDate(self.txtDealerDate)
        }
        else if (textField == self.txtAct_MilStatus){
            self.pickUp(txtAct_MilStatus)
        }
        else if (textField == self.txtVehicleStatus){
            self.pickUp(txtVehicleStatus)
        }
        else if (textField == self.txtNew_Used){
            self.pickUp(txtNew_Used)
        }
        
        else if (textField == self.txtMake){
            self.pickUp(txtMake)
        }
        else if (textField == self.txtModel){
            self.pickUp(txtModel)
        }
        else if (textField == self.txtTrim){
            self.pickUp(txtTrim)
        }
        else if (textField == self.txtExt_color){
            self.pickUp(txtExt_color)
        }
      
        else if(textField == self.txtEngine)
        {
            self.pickUp(txtEngine)
        }
        else if(textField == self.txtCondition)
        {
            self.pickUp(txtCondition)
        }
    else if(textField == self.txtInt_trim)
           {
               self.pickUp(txtInt_trim)
           }
    else if(textField == self.txtBodyStyle)
           {
               self.pickUp(txtBodyStyle)
           }
    else if(textField == self.txtTransmission)
    {
        self.pickUp(txtTransmission)
    }
    else if(textField == self.txtDriveType)
    {
        self.pickUp(txtDriveType)
    }
    else if(textField == self.txtFuel)
    {
        self.pickUp(txtFuel)
    }
    else if(textField == self.Ext_primaryClr)
    {
        self.pickUp(Ext_primaryClr)
    }
        else if(textField == self.Ext_secondaryClr)
        {
            self.pickUp(Ext_secondaryClr)
        }
        else if(textField == self.txtPlate_ExpMonth)
        {
            self.pickUp(txtPlate_ExpMonth)
        } else if(textField == self.txtPlate_ExpDateYr)
        {
            self.pickUp(txtPlate_ExpDateYr)
        } else if(textField == self.txtInsp_ExpYr)
        {
            self.pickUp(txtInsp_ExpYr)
        } else if(textField == self.txtInsp_ExpMonth)
        {
            self.pickUp(txtInsp_ExpMonth)
        } else if(textField == self.txtRecovery_Type)
        {
            self.pickUp(txtRecovery_Type)
    }
    else if(textField == self.txtDealerDate)
        {
            self.pickUpDate(txtDealerDate)
    }
    



    
    }
    func pickUp(_ textField : UITextField){
        if(textField == self.txtAct_MilStatus){
            self.pv_Act_status = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Act_status.delegate = self
            self.pv_Act_status.backgroundColor = UIColor.white
            textField.inputView = self.pv_Act_status
        }
        else  if(textField == self.txtVehicleStatus){
                   self.pv_Veh_status = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_Veh_status.delegate = self
                   self.pv_Veh_status.backgroundColor = UIColor.white
                   textField.inputView = self.pv_Veh_status
               }
        else if (textField == self.txtNew_Used){
            self.pv_New_Used = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_New_Used.delegate = self
            self.pv_New_Used.backgroundColor = UIColor.yellow
            textField.inputView =  self.pv_New_Used
        }
        else if (textField == self.txtYear){
            self.pv_Year = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Year.delegate =  self
            self.pv_Year.backgroundColor = UIColor.white
             textField.inputView =  self.pv_Year
        }
        else if (textField == txtModel){
            self.pv_Model = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Model.delegate = self
            self.pv_Model.backgroundColor = UIColor.white
             textField.inputView =  self.pv_Model
        }
        else if(textField == txtMake){
            self.pv_Make = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Make.delegate = self
            self.pv_Make.backgroundColor = UIColor.white
            textField.inputView =  self.pv_Make
        }
        else if(textField == txtTrim){
            self.pv_trim = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_trim.delegate = self
            self.pv_trim.backgroundColor = UIColor.white
            textField.inputView =  self.pv_trim
        }
        else if(textField == txtExt_color){
            self.pv_extColor = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_extColor.delegate = self
            self.pv_extColor.backgroundColor = UIColor.white
            textField.inputView =  self.pv_extColor
        }
        else if(textField == txtInt_trim){
            self.pv_intTrim = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_intTrim.delegate = self
            self.pv_intTrim.backgroundColor = UIColor.white
            textField.inputView =  self.pv_intTrim
        }
        else if(textField == txtBodyStyle){
            self.pv_Bodystyle = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Bodystyle.delegate = self
            self.pv_Bodystyle.backgroundColor = UIColor.white
            textField.inputView =  self.pv_Bodystyle
        }
        else if(textField == txtTransmission){
                   self.pv_Transmisson = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_Transmisson.delegate = self
                   self.pv_Transmisson.backgroundColor = UIColor.white
            textField.inputView =  self.pv_Transmisson
               }
        else if(textField == txtDriveType){
                   self.pv_DriveType = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_DriveType.delegate = self
                   self.pv_DriveType.backgroundColor = UIColor.white
            textField.inputView =  self.pv_DriveType
               }
        else if(textField == txtEngine){
                          self.pv_Engine = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                          self.pv_Engine.delegate = self
                          self.pv_Engine.backgroundColor = UIColor.white
            textField.inputView =  self.pv_Engine
                      }
        else if(textField == txtCondition){
                          self.pv_Condition = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                          self.pv_Condition.delegate = self
                          self.pv_Condition.backgroundColor = UIColor.white
            textField.inputView =  self.pv_Condition
                      }
        
        else if(textField == txtFuel){
                      self.pv_Fuel = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                      self.pv_Fuel.delegate = self
                      self.pv_Fuel.backgroundColor = UIColor.white
        textField.inputView =  self.pv_Fuel
                  }
        else if(textField == Ext_primaryClr){
                   self.pv_extPrim_color = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_extPrim_color.delegate = self
                   self.pv_extPrim_color.backgroundColor = UIColor.white
                   textField.inputView =  self.pv_extPrim_color
               }
        else if(textField == Ext_secondaryClr){
                   self.pv_extSec_color = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
                   self.pv_extSec_color.delegate = self
                   self.pv_extSec_color.backgroundColor = UIColor.white
                   textField.inputView =  self.pv_extSec_color
               }
      else if(textField == txtPlate_ExpMonth){
            self.pv_plateExpMonth = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_plateExpMonth.delegate = self
            self.pv_plateExpMonth.backgroundColor = UIColor.white
            textField.inputView =  self.pv_plateExpMonth
        }
        else if(textField == txtPlate_ExpDateYr){
            self.pv_plateExpYr = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_plateExpYr.delegate = self
            self.pv_plateExpYr.backgroundColor = UIColor.white
            textField.inputView =  self.pv_plateExpYr
        }
        
 else if(textField == txtInsp_ExpYr){
            self.pv_inspectnExpYr = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_inspectnExpYr.delegate = self
            self.pv_inspectnExpYr.backgroundColor = UIColor.white
            textField.inputView =  self.pv_inspectnExpYr
        }
        else if(textField == txtInsp_ExpMonth){
            self.pv_inspectnExpMonth = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_inspectnExpMonth.delegate = self
            self.pv_inspectnExpMonth.backgroundColor = UIColor.white
            textField.inputView =  self.pv_inspectnExpMonth
        }
        else if(textField == txtRecovery_Type){
            self.pv_recoveryType = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_recoveryType.delegate = self
            self.pv_recoveryType.backgroundColor = UIColor.white
            textField.inputView =  self.pv_recoveryType
        }
        
        
        
        
    }
//needed or not?
    func callUpdateApi(param: [String : Any])
    {
        let strUrl = "\(Constant.inventory)/vehicle"
        BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
