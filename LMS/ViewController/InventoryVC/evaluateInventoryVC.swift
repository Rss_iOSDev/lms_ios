//
//  addInventoryVC.swift
//  LMS
//
//  Created by Apple on 22/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class evaluateInventoryVC: UIViewController {
    
    @IBOutlet weak var view_1: UIView!
    // outlets of view 1a
    @IBOutlet weak var view_1a: UIView!
    @IBOutlet weak var txtYear:UITextField!
    @IBOutlet weak var txtMake:UITextField!
    @IBOutlet weak var txtModel:UITextField!
    @IBOutlet weak var txtBodyStyle:UITextField!
    @IBOutlet weak var txtDriveType:UITextField!
    @IBOutlet weak var txtTransmission:UITextField!
    @IBOutlet weak var txtEngine:UITextField!
    @IBOutlet weak var txtFuelType : UITextField!
    //outlets of view1b
     @IBOutlet weak var view_1b: UIView!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtExt_color: UITextField!
    @IBOutlet weak var txtInt_color: UITextField!
    @IBOutlet weak var txtAcquired_mileage: UITextField!
    @IBOutlet weak var txtMileage_status: UITextField!
    @IBOutlet weak var txtExt_primaryClr: UITextField!
   @IBOutlet weak var  txtExt_secondaryClr: UITextField!
   //outlets of view2
    @IBOutlet weak var view_2: UIView!
    @IBOutlet weak var txtEval_date: UITextField!
    @IBOutlet weak var txtBid_price:UITextField!
    @IBOutlet weak var txtOffer_price: UITextField!
    @IBOutlet weak var txtEval_group: UITextField!
    @IBOutlet weak var txtAuction_Lane: UITextField!
    @IBOutlet weak var txtAuction_No: UITextField!
    
    @IBOutlet weak var hgtView_1: NSLayoutConstraint!
//   var delegate: Inventory_filterAccountDelegate?
//
//    var passDict_LayoutId : NSDictionary = [:]
//
    var arrEngine : NSMutableArray = []
    var arrTransmission : NSMutableArray = []
    var arrDriveType : NSMutableArray = []
    var arrModel : NSMutableArray = []
    var arrMake : NSMutableArray = []
    var arrFuelType : NSMutableArray = []
    var arrType : NSMutableArray = []
    var arrAuction_Lane : NSMutableArray = []
    var arrAuction_No: NSMutableArray = []
    
    var pickerEngine: UIPickerView!
    var pickerViewTransmission : UIPickerView!
    var pickerViewDriveType: UIPickerView!
    var pickerViewModel : UIPickerView!
    var pickerViewMake: UIPickerView!
    var pickerViewFuelType: UIPickerView!
    var pickerViewType: UIPickerView!
    var pickerViewAuction_Lane: UIPickerView!
    var pickerViewAuction_No: UIPickerView!
   
    var selectedEngineId = [String]()
    var selectedEngineTitle = [String]()
    
    var selectedTransmissionId = [String]()
    var selectedTransmissionTitle = [String]()
    
    var selectedDriveTypeId = [String]()
    var selectedDriveTypeTitle = [String]()
    
    var selectedMAkeTitle = [String]()
    
    var selectedModelId = [String]()
    var selectedModelTitle = [String]()
    var selectedFuelTypeId = [String]()
    var selectedFuelTypeTitle = [String]()
    
    var selectedTypeId = [String]()
    var selectedTypeTitle = [String]()
    
//    var selectedDriveTypeId = [String]()
//    var selectedDriveTypeTitle = [String]()
    
    
    
    var selectedAuction_LaneId = [String]()
    var selectedAuction_LaneTitle = [String]()
    var selectedAuction_NoId = [String]()
    var selectedAuction_NoTitle = [String]()
    
    var arrYear = [String]()
    var arrDates = [String]()
    var arrAmount = [String]()
    var arrMileage = [String]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
       //  callApiToSetDataonPicker()
        // Do any additional setup after loading the view.
        
        self.hgtView_1.constant = 0
        self.view_1.isHidden = true
    }
 @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func clickOnBTnEdit(_ sender: UIButton)
    {
        if(sender.tag == 10){
            sender.tag = 20
            self.hgtView_1.constant = 570
            self.view_1.isHidden = false

        }
        else if(sender.tag == 20){
            sender.tag = 10
            self.hgtView_1.constant = 0
            self.view_1.isHidden = true

        }

    }
   

//extension evaluateInventoryVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        //  if component == 0 {
//        if pickerView == pickerEngine{
//            return self.arrEngine.count
//        }
//        else if(pickerView == pickerViewTransmission){
//            return self.arrTransmission.count
//        }
//
//        else if(pickerView == pickerViewDriveType){
//            return self.arrDriveType.count
//        }
//
//        else if(pickerView == pickerViewMake){
//            return self.arrMake.count
//        }
//        else if(pickerView == pickerViewModel){
//            return self.arrModel.count
//        }
//
//        else if(pickerView == pickerViewFuelType){
//            return self.arrFuelType.count
//        }
//        else if(pickerView == pickerViewType){
//            return self.arrType.count
//        }
//
//        else if(pickerView == pickerViewAuction_Lane){
//            return self.arrAuction_Lane.count
//        }
//        else if(pickerView == pickerViewAuction_No){
//            return self.arrAuction_No.count
//        }
//
//
//        else{
//            return 0
//        }
//    }
////
//
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        let attributedString = NSMutableAttributedString()
//        if pickerView == pickerEngine{
//
//            let dict = arrEngine[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//            attributedString.append(attrString)
//
//            for item in   selectedEngineId{
//                if item == "\(_id)" {
//                    attributedString.append(NSAttributedString(string: " "))
//                    let imageAtchement = NSTextAttachment()
//                    imageAtchement.image = UIImage(named: "check")
//                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                    attributedString.append(imageAttachString)
//                }
//            }
//        }
//        else if pickerView == pickerViewTransmission{
//
//
//            let dict = arrTransmission[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//
//            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//            attributedString.append(attrString)
//
//            for item in selectedTransmissionId{
//                if item == "\(_id)" {
//                    attributedString.append(NSAttributedString(string: " "))
//                    let imageAtchement = NSTextAttachment()
//                    imageAtchement.image = UIImage(named: "check")
//                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                    attributedString.append(imageAttachString)
//                }
//            }
//        }
//
//        else if(pickerView == pickerViewDriveType){
//
//            let dict = arrDriveType[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//
//
//            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//            attributedString.append(attrString)
//
//            for item in selectedDriveTypeId {
//                if item == "\(_id)"{
//                    attributedString.append(NSAttributedString(string: " "))
//                    let imageAtchement = NSTextAttachment()
//                    imageAtchement.image = UIImage(named: "check")
//                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                    attributedString.append(imageAttachString)
//                }
//            }
//        }
//
//        else if(pickerView == pickerViewMake){
//
//            let dict = arrMake[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//
//            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//            attributedString.append(attrString)
//
//            for item in selectedMAkeTitle{
//                if item == "\(strTitle)" {
//                    attributedString.append(NSAttributedString(string: " "))
//                    let imageAtchement = NSTextAttachment()
//                    imageAtchement.image = UIImage(named: "check")
//                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                    attributedString.append(imageAttachString)
//                }
//            }
//        }
//
//        else if(pickerView == pickerViewModel){
//
//            let dict = arrModel[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//
//            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//            attributedString.append(attrString)
//
//            for item in selectedModelId {
//                if item == "\(_id)" {
//                    attributedString.append(NSAttributedString(string: " "))
//                    let imageAtchement = NSTextAttachment()
//                    imageAtchement.image = UIImage(named: "check")
//                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                    attributedString.append(imageAttachString)
//                }
//            }
//
//                else if pickerView == pickerViewFuelType{
//
//
//                                      let dict = arrFuelType[row] as! NSDictionary
//                                      let strTitle = dict["title"] as! String
//                                      let _id = dict["_id"] as! Int
//
//                                      let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//                                      attributedString.append(attrString)
//
//                                      for item in selectedFuelTypeId{
//                                          if item == "\(_id)" {
//                                              attributedString.append(NSAttributedString(string: " "))
//                                              let imageAtchement = NSTextAttachment()
//                                              imageAtchement.image = UIImage(named: "check")
//                                              imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                                              let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                                              attributedString.append(imageAttachString)
//                                          }
//                                      }
//                   else if pickerView == pickerViewType{
//
//
//                       let dict = arrType[row] as! NSDictionary
//                       let strTitle = dict["title"] as! String
//                       let _id = dict["_id"] as! Int
//
//                       let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//                       attributedString.append(attrString)
//
//                       for item in selectedTypeId{
//                           if item == "\(_id)" {
//                               attributedString.append(NSAttributedString(string: " "))
//                               let imageAtchement = NSTextAttachment()
//                               imageAtchement.image = UIImage(named: "check")
//                               imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                               let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                               attributedString.append(imageAttachString)
//                           }
//                       }
//                   }
//
//                   else if(pickerView == pickerViewAuction_Lane){
//
//                       let dict = arrAuction_Lane[row] as! NSDictionary
//                       let strTitle = dict["title"] as! String
//                       let _id = dict["_id"] as! Int
//
//
//                       let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//                       attributedString.append(attrString)
//
//                       for item in selectedAuction_LaneId {
//                           if item == "\(_id)"{
//                               attributedString.append(NSAttributedString(string: " "))
//                               let imageAtchement = NSTextAttachment()
//                               imageAtchement.image = UIImage(named: "check")
//                               imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                               let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                               attributedString.append(imageAttachString)
//                           }
//                       }
//                   }
//
//
//
//                   else if(pickerView == pickerViewAuction_No){
//
//                       let dict = arrAuction_No[row] as! NSDictionary
//                       let strTitle = dict["title"] as! String
//                       let _id = dict["_id"] as! Int
//
//                       let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
//                       attributedString.append(attrString)
//
//                for item in selectedAuction_NoId {
//                           if item == "\(_id)" {
//                               attributedString.append(NSAttributedString(string: " "))
//                               let imageAtchement = NSTextAttachment()
//                               imageAtchement.image = UIImage(named: "check")
//                               imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
//                               let imageAttachString = NSAttributedString(attachment: imageAtchement)
//                               attributedString.append(imageAttachString)
//                           }
//                       }
//                   }
//        }
//        return attributedString
//    }
//
//    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
//        return 40
//    }
//
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
//        if(pickerView == pickerAltLocation){
//
//            let dict = arrEngine[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//            if selectedEngineId.contains("\(_id)") {
//                var index = 0
//                for item in selectedEngineId {
//                    if item == "\(_id)" {
//                        selectedEngineId.remove(at: index)
//                        selectedEngineTitle.remove(at: index)
//                    } else {
//                        index += 1
//                    }
//                }
//            } else {
//                selectedEngineId.append("\(_id)")
//                selectedEngineTitle.append(strTitle)
//            }
//            self.txtEngine.text = selectedEngineTitle.joined(separator: ", ")    //selectedArray is array has selected item
//
//            //            let queLayoutId = passDict_LayoutId["layout_id"] as! Int
//            //            url = "/lms/account/search-accounts?account_type=\(txtAcctType.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
//
//            self.pickerAltLocation.reloadAllComponents()
//        }
//
//        else if(pickerView == pickerViewTransmission){
//
//            let dict = arrTransmission[row] as! NSDictionary
//            let strTitle = dict["title"] as! String
//            let _id = dict["_id"] as! Int
//
//            if selectedTransmissionId .contains("\(_id)") {
//                var index = 0
//                for item in selectedLoc {
//                    if item == "\(_id)" {
//
//                       selectedTransmissionId .remove(at: index)
//                        selectedTransmissionTitle.remove(at: index)
//
//                    } else {
//                        index += 1
//                    }
//                }
//            } else {
//                selectedTransmissionId .append("\(_id)")
//                selectedTransmissionTitle.append(strTitle)
//            }
//
//            self.txtTransmission.text = selectedTransmissionTitle.joined(separator: ", ")
//            self.pickerViewTransmission.reloadAllComponents()
//        }
//        else if(pickerView == pickerViewDriveType){
//
//                          let dict = arrDriveType[row] as! NSDictionary
//                          let strTitle = dict["title"] as! String
//                          let _id = dict["_id"] as! Int
//
//                          if selectedDriveTypeId.contains("\(_id)") {
//                              var index = 0
//                              for item in selectedDriveTypeId{
//                                  if item == "\(_id)" {
//                                      selectedDriveTypeId.remove(at: index)
//                                     selectedDriveTypeTitle.remove(at: index)
//
//                                  } else {
//                                      index += 1
//                                  }
//                              }
//                          } else {
//                              selectedDriveTypeId.append("\(_id)")
//                               selectedDriveTypeTitle.append(strTitle)
//                          }
//
//                          self.txtDriveType.text = selectedDriveTypeTitle.joined(separator: ", ")
//                          self.pickerViewDriveType.reloadAllComponents()
//                      }
//
//                  }
//
//       else if(pickerView == pickerViewFuelType){
//
//                         let dict = arrFuelType[row] as! NSDictionary
//                         let strTitle = dict["title"] as! String
//                         let _id = dict["_id"] as! Int
//
//                         if selectedFuelTypeId.contains("\(_id)") {
//                             var index = 0
//                             for item in selectedFuelTypeId {
//                                 if item == "\(_id)" {
//                                     selectedFuelTypeId.remove(at: index)
//                                     selectedFuelTypeTitle.remove(at: index)
//
//                                 } else {
//                                     index += 1
//                                 }
//                             }
//                         } else {
//                             selectedFuelTypeId.append("\(_id)")
//                             selectedFuelTypeTitle.append(strTitle)
//                         }
//
//                         self.txtFuelType.text = selectedFuelTypeTitle.joined(separator: ", ")
//                         self.pickerViewFuelType.reloadAllComponents()
//                     }
//
//                 }
//
//    else if(pickerView == pickerViewAuction_Lane){
//
//      let dict = arrAuction_Lane[row] as! NSDictionary
//      let strTitle = dict["title"] as! String
//      let _id = dict["_id"] as! Int
//
//      if selectedAuction_Lane("\(_id)") {
//          var index = 0
//          for item in selectedFlagId {
//              if item == "\(_id)" {
//                  selectedAuction_LaneId.remove(at: index)
//                  selectedAuction_LaneTitle.remove(at: index)
//
//              } else {
//                  index += 1
//              }
//          }
//      } else {
//         selectedAuction_LaneId.append("\(_id)")
//         selectedAuction_LaneTitle.append(strTitle)
//      }
//
//      self.txtAuction_Lane.text = selectedAuction_LaneTitle.joined(separator: ", ")
//      self.pickerViewAuction_Lane.reloadAllComponents()
//  }
//
//}
//    else if(pickerView == pickerViewAuction_No){
//    //
//                      let dict = arrAuction_No[row] as! NSDictionary
//                      let strTitle = dict["title"] as! String
//                      let _id = dict["_id"] as! Int
//
//                      if selectedAuction_NoId.contains("\(_id)") {
//                          var index = 0
//                          for item in selectedFlagId {
//                              if item == "\(_id)" {
//                                  selectedAuction_NoId.remove(at: index)
//                                  selectedAuction_NoTitle.remove(at: index)
//
//                              } else {
//                                  index += 1
//                              }
//                          }
//                      } else {
//                          selectedAuction_NoId.append("\(_id)")
//                          selectedAuction_NoTitle.append(strTitle)
//                      }
//
//                      self.txtAuction_No.text = selectedAuction_NoTitle.joined(separator: ", ")
//                      self.pickerViewAuction_No.reloadAllComponents()
//                  }
    
 //             }
            
//    @IBOutlet weak var txtYear:UITextField!
//     @IBOutlet weak var txtMake:UITextField!
//     @IBOutlet weak var txtModel:UITextField!
//     @IBOutlet weak var txtBodyStyle:UITextField!
//     @IBOutlet weak var txtDriveType:UITextField!
//     @IBOutlet weak var txtTransmission:UITextField!
//     @IBOutlet weak var txtEngine:UITextField!
//     @IBOutlet weak var txtFuelType : UITextField!
//     //outlets of view1b
//      @IBOutlet weak var view_1b: UIView!
//     @IBOutlet weak var txtType: UITextField!
//     @IBOutlet weak var txtExt_color: UITextField!
//     @IBOutlet weak var txtInt_color: UITextField!
//     @IBOutlet weak var txtAcquired_mileage: UITextField!
//     @IBOutlet weak var txtMileage_status: UITextField!
//     @IBOutlet weak var txtExt_primaryClr: UITextField!
//    @IBOutlet weak var  txtExt_secondaryClr: UITextField!
//    //outlets of view2
//     @IBOutlet weak var view_2: UIView!
//     @IBOutlet weak var txtEval_date: UITextField!
//     @IBOutlet weak var txtBid_price:UITextField!
//     @IBOutlet weak var txtOffer_price: UITextField!
//     @IBOutlet weak var txtEval_group: UITextField!
//     @IBOutlet weak var txtAuction_Lane: UITextField!
//     @IBOutlet weak var txtAuction_No: UITextField!
//
//    func textFieldDidBeginEditing(_ textField: UITextField){
//        if(textField == self.txtEngine){
//            self.pickUp(txtEngine)
//        }
//        else if(textField == self.txtMake){
//            self.pickUp(txtMake)
//        }
//        else if(textField == self.txtDriveType){
//            self.pickUp(txtDriveType)
//        }
//        else if(textField == self.txtTransmission){
//            self.pickUp(txtTransmission)
//        }
//        else if(textField == self.txtType){
//            self.pickUp(txtType)
//        }
//
//        else if(textField == self.txtAuction_Lane){
//            self.pickUp(txtFlags)
//        }else if(textField == self.txtAuction_No){
//            self.pickUp(txtAuction_No)
//        }else if(textField == self.txtFuelType){
//            self.pickUp(txtFuelType)
//        }
//
//    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//        let queLayoutId = passDict_LayoutId["layout_id"] as! Int
//        if(textField == txtSearch){
//           // url = "/lms/account/search-accounts?search=\(txtSearch.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
//        }
//    }
//
//
//
//
//    func pickUp(_ textField : UITextField) {
//
//         if(textField == self.txtAltLocation){
//             self.pickerAltLocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//             self.pickerAltLocation.delegate = self
//             self.pickerAltLocation.dataSource = self
//             self.pickerAltLocation.backgroundColor = UIColor.white
//             textField.inputView = self.pickerAltLocation
//         }
//         else if(textField == self.txtLocation){
//             self.pickerViewLocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//             self.pickerViewLocation.delegate = self
//             self.pickerViewLocation.dataSource = self
//             self.pickerViewLocation.backgroundColor = UIColor.white
//             textField.inputView = self.pickerViewLocation
//         }
//         else if(textField == self.txtStatus){
//             self.pickerViewStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//             self.pickerViewStatus.delegate = self
//             self.pickerViewStatus.dataSource = self
//             self.pickerViewStatus.backgroundColor = UIColor.white
//             textField.inputView = self.pickerViewStatus
//         }
//
//         else if(textField == self.txtMake){
//             self.pickerViewMake = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//             self.pickerViewMake.delegate = self
//             self.pickerViewMake.dataSource = self
//             self.pickerViewMake.backgroundColor = UIColor.white
//             textField.inputView = self.pickerViewMake
//
//         }
//         else if(textField == self.txtFlags){
//             self.pickerViewFlag = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
//             self.pickerViewFlag.delegate = self
//             self.pickerViewFlag.dataSource = self
//             self.pickerViewFlag.backgroundColor = UIColor.white
//             textField.inputView = self.pickerViewFlag
//
//         }
//        // ToolBar
//        let toolBar = UIToolbar()
//        toolBar.barStyle = .default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
//        toolBar.sizeToFit()
//
//        // Adding Button ToolBar
//        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        textField.inputAccessoryView = toolBar
//    }
//
//
//    //MARK:- Button
//    @objc func doneClick() {
//        txtStatus.resignFirstResponder()
//        txtLocation.resignFirstResponder()
//       txtAltLocation.resignFirstResponder()
//       txtMake.resignFirstResponder()
//        txtFlags.resignFirstResponder()
//    }

}
