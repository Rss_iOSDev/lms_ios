//
//  searchInventoryVC.swift
//  LMS
//
//  Created by Apple on 26/02/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class searchInventoryVC: baseViewController {

    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var txtPagination: UITextField!
    
    @IBOutlet weak var scrollViewPages: UIScrollView!
    @IBOutlet weak var scrollWidth: NSLayoutConstraint!
    
    @IBOutlet weak var lblResultfound: UILabel!
    
    
    let buttonPadding:CGFloat = 10
    var xOffset:CGFloat = 10
    
    
    
    @IBOutlet weak var gridCollectionView: UICollectionView! {
        didSet {
            gridCollectionView.bounces = false
        }
    }
    
    @IBOutlet weak var gridLayout: StickyGridCollectionViewLayout! {
        didSet {
            gridLayout.stickyColumnsCount = 1
        }
    }
    
    var arrPAginationNo = ["10","25","50","75","100","200","250"]
    var pickerViewPages: UIPickerView!
    
    var mainArrListData : [NSMutableArray] = []
    
    var dictLayout : NSDictionary = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        addSlideMenuButton(btnMenu)
        addProfileMenuButton(btnProfile)
        
        let strFname = user?.fname
        let strLname = user?.lname
        lblUserName.text = String((strFname?.first)!) + String((strLname?.first)!)
        
        self.txtPagination.text = self.arrPAginationNo[0]
        
        Globalfunc.showLoaderView(view: self.view)
        self.callGetLayoutIdApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //spreadSheetAccount.bounces = false
    }
    
    @IBAction func clickOnFilterBtn(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "Inventory", bundle: nil)
        let filter = story.instantiateViewController(identifier: "inventoryFilterVC") as! inventoryFilterVC
        filter.modalPresentationStyle = .fullScreen
        filter.delegate = self
        filter.passDict_LayoutId = self.dictLayout
        self.present(filter, animated: true, completion: nil)
    }
}

extension searchInventoryVC {
    
    func callGetLayoutIdApi()
    {
        let url = "\(Constant.get_layoutId_url)?layout=&dataSourceId=11"
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.print(object:dict)
                    Globalfunc.hideLoaderView(view: self.view)
                    if let response = dict as? NSDictionary {
                        
                        if let data = response["data"] as? NSDictionary {
                            self.dictLayout = data
                            if let layout_id = data["layout_id"] as? Int
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                                    self.callAccountListApi(layout_id: layout_id)
                                })
                            }
                        }
                        
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
    
    func callAccountListApi(layout_id : Int){
        
        let Str_url = "\(Constant.inventory_queue_layout)?queueLayoutsId=\(layout_id)"
        BaseApi.callApiRequestForGet(url: Str_url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    self.parseResponseAndSetData(dict: dict, strCheck: "get")
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

//MARK: - Testfield delegate methods

extension searchInventoryVC : UITextFieldDelegate , UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewPages){
            return self.arrPAginationNo.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewPages){
            return self.arrPAginationNo[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerViewPages){
            self.txtPagination.text = self.arrPAginationNo[row]
        }
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txtPagination){
            self.pickUp(txtPagination)
        }
    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtPagination){
            self.pickerViewPages = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewPages.delegate = self
            self.pickerViewPages.dataSource = self
            self.pickerViewPages.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewPages
        }
        
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtPagination.resignFirstResponder()
        
        self.mainArrListData = []
        
        Globalfunc.showLoaderView(view: self.view)
        
        let pagecount = Int(self.txtPagination.text!)
        
        let queLayoutId = self.dictLayout["layout_id"] as! Int
        
        let passDict = ["alternate_location": "",
        "bodyStyle": "",
        "currentPageNumber": 1,
        "defaultRecordsPage": pagecount!,
        "extColor": "",
        "flags": "",
        "inventory_status": "1",
        "isGet": true,
        "licensePlate": "",
        "location": "",
        "make": "",
        "maxAskPrice": "",
        "maxAskingDown": "",
        "maxAskingTerms": "",
        "maxCost": "",
        "maxDaysOnLot": "",
        "maxMileage": "",
        "maxYear": "",
        "minAskPrice": "",
        "minCost": "",
        "minDaysOnLot": "",
        "minMileage": "",
        "minYear": "",
        "model": "",
        "order1": "asc",
        "order2": "asc",
        "order3": "asc",
        "queueLayoutsId": queLayoutId,
        "search": "",
        "sort1": "vin",
        "sort2": "",
        "sort3": "",
        "stock": "",
        "trimLevel": "",
        "vin": ""] as [String : Any]

        self.callApiwithPostParam(param: passDict)
    }
    
    @objc func cancelClick() {
        txtPagination.resignFirstResponder()
    }
    
    
    @objc func btnClick(_ sender : UIButton){
        sender.pulsate()
        
        self.mainArrListData = []
        
        let pagNo = sender.titleLabel?.text!
        let pN = Int(pagNo!)
        Globalfunc.showLoaderView(view: self.view)
        let pagecount = Int(self.txtPagination.text!)
        
        let queLayoutId = self.dictLayout["layout_id"] as! Int
        let passDict = ["alternate_location": "",
        "bodyStyle": "",
        "currentPageNumber": pN!,
        "defaultRecordsPage": pagecount!,
        "extColor": "",
        "flags": "",
        "inventory_status": "1",
        "isGet": true,
        "licensePlate": "",
        "location": "",
        "make": "",
        "maxAskPrice": "",
        "maxAskingDown": "",
        "maxAskingTerms": "",
        "maxCost": "",
        "maxDaysOnLot": "",
        "maxMileage": "",
        "maxYear": "",
        "minAskPrice": "",
        "minCost": "",
        "minDaysOnLot": "",
        "minMileage": "",
        "minYear": "",
        "model": "",
        "order1": "asc",
        "order2": "asc",
        "order3": "asc",
        "queueLayoutsId": queLayoutId,
        "search": "",
        "sort1": "vin",
        "sort2": "",
        "sort3": "",
        "stock": "",
        "trimLevel": "",
        "vin": ""] as [String : Any]
        
        self.callApiwithPostParam(param: passDict)
        
    }
}

// MARK: - Collection view data source and delegate methods

extension searchInventoryVC: UICollectionViewDataSource , UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.mainArrListData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let arr = self.mainArrListData[section]
        return arr.count //column
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collAccountCell", for: indexPath) as? collAccountCell else {
            return UICollectionViewCell()
        }
        
        
        if(self.mainArrListData.count > 0){
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary
            {
                print(arrInside)
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    cell.lblLine.isHidden = true
                    cell.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
                    cell.titleLabel.text = "\(arrInside["column"] as! String)"
                    cell.titleLabel.textColor = .white
                }
                else{
                    cell.lblLine.isHidden = false
                    cell.titleLabel.textColor = .black
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        cell.backgroundColor = .systemGroupedBackground
                        cell.titleLabel.text = "rrrr"
                    }
                    else{
                        Globalfunc.print(object:arrInside)
                        
                        if let val = arrInside["value"] as? String{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                        if let val = arrInside["value"] as? Int{
                            cell.titleLabel.text = "\(val)"
                            cell.backgroundColor = .white
                        }
                    }
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(self.mainArrListData.count > 0){
            
            let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as! NSDictionary
            let columnNam = arrInside["column"] != nil
            if(columnNam){
                //sorting
//                let sortName = arrInside["name"] as! String
//
//                self.mainArrListData = []
//
//                Globalfunc.showLoaderView(view: self.view)
//
//                let pagecount = Int(self.txtPagination.text!)
//                let queLayoutId = self.dictLayout["layout_id"] as! Int
//                Globalfunc.print(object:passDict)
//                self.callApiwithPostParam(param: passDict)
            }
            else{
                let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as! NSDictionary
                let id_dict = arrInside["id_data"] as! NSDictionary
                let addId = id_dict["id"] as! Int
                let detail = self.storyboard?.instantiateViewController(identifier: "inventoryDetailViewController") as! inventoryDetailViewController
                //detail.isDetailOpenfrom = "invenrory"
                detail.inventoryId = addId
                //detail.passDict_LayoutId = self.dictLayout
                detail.modalPresentationStyle = .fullScreen
                self.present(detail, animated: true, completion: nil)
            }
            
        }
    }
}

extension searchInventoryVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height = 0.0
        let width = (collectionView.frame.width) / 2
        
        if(self.mainArrListData.count > 0){
            
            //            print(indexPath.section)
            //            print(indexPath.row)
            //
            //            let lastRowIndex = collectionView.numberOfItems(inSection: indexPath.section)
            //            if indexPath.row == lastRowIndex - 1 {
            //
            //
            //                print(self.mainArrListData[indexPath.section][indexPath.row])
            //            }
            
            
            
            if let arrInside = self.mainArrListData[indexPath.section][indexPath.row] as? NSDictionary{
                
                let columnNam = arrInside["column"] != nil
                if(columnNam){
                    height = 30.0
                }
                else{
                    let keyExists = arrInside["id"] != nil
                    if(keyExists){
                        height = 10.0
                    }
                    else{
                        height = 50
                    }
                }
            }
        }
        return CGSize(width: Double(width), height: height)
    }
}

//class collAccountCell: UICollectionViewCell {
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var lblLine: UILabel!
//}

extension searchInventoryVC : Inventory_filterAccountDelegate
{
    func setInventoryFilteronPage(filter: [String : Any]) {
        
        self.mainArrListData = []
        Globalfunc.showLoaderView(view: self.view)
        self.dismiss(animated: true) {
            self.callApiwithPostParam(param: filter)
        }
    }
    
    func callApiwithPostParam(param: [String : Any]){

        BaseApi.onResponsePostWithToken(url: Constant.inventory_queue_layout, controller: self, parms: param) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    self.parseResponseAndSetData(dict: dict, strCheck: "")
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }

    
}

//MARK:
extension searchInventoryVC{
    
    func parseResponseAndSetData(dict : Any, strCheck : String){
        
        if let dictResponse = dict as? NSDictionary {
            
            Globalfunc.print(object:dict)
            
            if(strCheck == "get"){
                
                let currentPageNumber = dictResponse["currentPageNumber"] as! Int
                let defaultRecordsPage = dictResponse["defaultRecordsPage"] as! Int
                
                let record = dictResponse["totalrecord"] as! Int
                self.lblResultfound.text = "\(record) RESULTS FOUND"
                
                let cpNo = CGFloat(currentPageNumber)
                let pagSize = CGFloat(defaultRecordsPage)
                let totalR = CGFloat(record)
                
                let pageNo = self.getPager(totalItems: totalR, currentPage: cpNo, pageSize: pagSize)
                let pagecount = Int(pageNo)
                
                let buttonPadding:CGFloat = 10
                var xOffset:CGFloat = 10
                
                for i in 0..<pagecount {
                    if (i == 0){
                        self.scrollViewPages.subviews.forEach ({ ($0 as? UIButton)?.removeFromSuperview() })
                    }
                    let button = UIButton()
                    button.tag = i+1
                    button.setTitle("\(i+1)", for: .normal)
                    button.layer.borderWidth = 1
                    button.layer.borderColor = self.UIColorFromHex(rgbValue: 0xDDE0E6, alpha: 1.0).cgColor
                    button.setTitleColor(self.UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0), for: .normal)
                    button.addTarget(self, action: #selector(self.btnClick), for: .touchUpInside)
                    button.titleLabel?.font = UIFont(name:"NewYorkMedium-Regular",size:11)
                    button.frame = CGRect(x: xOffset, y: 5, width: 30, height: 30)
                    xOffset = xOffset + CGFloat(buttonPadding) + button.frame.size.width
                    self.scrollViewPages.addSubview(button)
                }
                self.scrollWidth.constant = xOffset
                self.scrollViewPages.contentSize = CGSize(width: xOffset, height: self.scrollViewPages.frame.height)
                
                
            }
            
            let arrHeading = dictResponse["heading"] as! NSMutableArray
            print(arrHeading)
            
            for i in 0..<arrHeading.count{
                let new_arr : NSMutableArray = []
                let arr = arrHeading.object(at: i) as! NSMutableArray
                Globalfunc.print(object:arr)
                for i in 0..<arr.count{
                    let dict = arr[i] as! NSDictionary
                    let arr = dict["attribute"] as! NSDictionary
                    print(arr)
                    let name = dict["name"] as? String
                    let columnname = arr["colname"] as? String
                    let dictt = ["column":columnname,"name":name]
                    new_arr.add(dictt)
                    
                }
                self.mainArrListData.append(new_arr)
            }
            
            let arrRowCount = dictResponse["data"] as! NSMutableArray
            for i in 0..<arrRowCount.count
            {
                let arr0 = arrRowCount.object(at: i) as! NSMutableArray
                for j in 0..<arr0.count{
                    let arr1 = arr0[j] as! NSMutableArray
                    Globalfunc.print(object:arr1)
                    if(j == arr0.count-1){
                        let arrId = arr0[j] as! NSMutableArray
                        let dictId : NSMutableDictionary = [:]
                        let dd = arrId[0] as! NSDictionary
                        let val = dd["id"] as! Int
                        dictId.setValue(val, forKey: "id")
                        for _ in 0..<j-1{
                            let dict = ["id":""]
                            arr1.add(dict)
                        }
                        arr0.replaceObject(at: j, with: arr1)
                        arrRowCount.replaceObject(at: i, with: arr0)
                    }
                }
            }
            for x in 0..<arrRowCount.count{
                let arr0 = arrRowCount.object(at: x) as! NSMutableArray
                Globalfunc.print(object:arr0)
                let arr1 = arr0[arr0.count - 1] as! NSMutableArray
                let dictId = arr1[0] as! NSDictionary
                Globalfunc.print(object:dictId)
                for y in 0..<arr0.count{
                    let newArr = arr0[y] as! NSMutableArray
                    for z in 0..<newArr.count{
                        let newDict = newArr[z] as! NSDictionary
                        let tempDic: NSMutableDictionary = newDict.mutableCopy() as! NSMutableDictionary
                        tempDic.setValue(dictId, forKey: "id_data")
                        Globalfunc.print(object:tempDic)
                        newArr.replaceObject(at: z, with: tempDic)
                    }
                    self.mainArrListData.append(newArr)
                }
            }
            Globalfunc.print(object:self.mainArrListData)
            self.gridLayout.stickyRowsCount = arrHeading.count
            self.gridCollectionView.reloadData()
        }
    }
}


extension searchInventoryVC {
    
    @IBAction func clickONSettingBtn(_ sender: UIButton)
    {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let setting = story.instantiateViewController(identifier: "layoutSettingVC") as! layoutSettingVC
        setting.modalPresentationStyle = .overCurrentContext
        setting.isOpenVC = "inventory"
       // filter.delegate = self
       // filter.passDict_LayoutId = self.dictLayout
        self.present(setting, animated: true, completion: nil)
    }
}



