//
//  addInventoryViewController.swift
//  LMS
//
//  Created by Apple on 26/02/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class addInventoryViewController: UIViewController {
    @IBOutlet weak var contentView:UIView!
    
    @IBOutlet weak var viewVin: UIView!
    @IBOutlet weak var hgtVin: NSLayoutConstraint!
    @IBOutlet weak var txtVinValidate: UITextField!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view_1a:UIView!
    @IBOutlet weak var txtType:UITextField!
    @IBOutlet weak var txtExt_color:UITextField!
    @IBOutlet weak var txtInt_color:UITextField!
    @IBOutlet weak var txtAcquiredMileage:UITextField!
    @IBOutlet weak var txtMileageStatus:UITextField!
    @IBOutlet weak var txtExtPri_color:UITextField!
    @IBOutlet weak var txtExt_Sec_color:UITextField!
    
    //view_1b
    @IBOutlet weak var view_1b:UIView!
    @IBOutlet weak var txtYear:UITextField!
    @IBOutlet weak var txtMake:UITextField!
    @IBOutlet weak var txtModel:UITextField!
    @IBOutlet weak var txtBodyStyle:UITextField!
    @IBOutlet weak var txtDriveType:UITextField!
    @IBOutlet weak var txtTransmission:UITextField!
    @IBOutlet weak var txtEngine:UITextField!
    @IBOutlet weak var txtFuelType:UITextField!
    //PURCHASE INFORMATION
    //view2
    @IBOutlet weak var view_2:UIView!
    @IBOutlet weak var txtVehicleLot:UITextField!
    @IBOutlet weak var txtBuyer:UITextField!
    @IBOutlet weak var txtDateAcquired:UITextField!
    @IBOutlet weak var txtAcquiredPrice:UITextField!
    @IBOutlet weak var txtAcquiredRef_No:UITextField!
    @IBOutlet weak var txt_acqFromName:UITextField!
    @IBOutlet weak var txtPaymethd:UITextField!
    @IBOutlet weak var txtAcqFrom: UITextField!
    
    var pv_year:UIPickerView!
    var pv_make: UIPickerView!
    var pv_model: UIPickerView!
    
    
    
    var arrIntcolor = NSMutableArray()
    var arrExtcolor = NSMutableArray()
    
    
    var pv_Type:UIPickerView!
    var arr_type = ["Unknown","Car(Vehicle)","Truck(Vehicle)","MotorCycle","Recreational Vehicle","Boat","Equipment","AirCraft","Powersports","Other","SUV","Van"]
    var pv_Ext_color:UIPickerView!
    var pv_Int_color:UIPickerView!
    
    var pv_MilStatus:UIPickerView!
    var arrMilStatus = ["Actual Miles" ,"True Mileage Unknown" , "Beyond Mechanical Limits", "Exempt"]
    var pv_extPri_color:UIPickerView!
    var pv_extSec_color:UIPickerView!
    var pv_VehLot:UIPickerView!
    var arrVehLot = NSMutableArray()
    var veh_id = ""
    var pv_Buyer:UIPickerView!
    var arrBuyer = NSMutableArray()
    var buyer_id = ""
    
    var pv_acquiredFrom:UIPickerView!
    var arrAcq_from = ["Dealer/Wholeseller/Auction","Individual","Unknown"]
    var pv_pay_methd: UIPickerView!
    var arrPay_methd = ["ACH","AP","Arm","Cash","Cheque","Draft","Floor Plan","Repossession","Trade-In"]
    
    var DatePickrDate_acq :UIDatePicker!
    
    var dataDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewVin.isHidden = false
        self.view1.isHidden = true
        self.view_2.isHidden = true
        self.hgtVin.constant = 150
        
        // Do any additional setup after loading the view.
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func clickOnBTnEdit(_ sender: UIButton)
    {
        if(sender.tag == 10){
            sender.tag = 20
            self.view_1b.isHidden = false
        }
        else if(sender.tag == 20){
            sender.tag = 10
            self.view_1b.isHidden = true
        }
    }
    
    @IBAction func clickOnDecode(_ sender: UIButton)
    {
        if(txtVinValidate.text == "" || txtVinValidate.text?.count == 0 || txtVinValidate.text == nil){
            txtVinValidate.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill VIN field.")
        }
        else{
            Globalfunc.showLoaderView(view: self.view)
            let getUrl = "\(Constant.decode_vin)?vin=\(self.txtVinValidate.text!)"
            self.callApiToSetData(strUrl: getUrl, strTyp: "vin")
            
        }
        
    }
    
}//http://3.129.150.191/lms/inventory/add
extension addInventoryViewController{
    
    func callApiToSetData(strUrl : String, strTyp: String){
        
        if(strTyp == "vin"){
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if( error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        if let arr = dict as? NSDictionary{
                            if let resp = arr[ "data"] as? NSDictionary{
                                
                                print(resp)
                                
                                if let msg = resp["msg"] as? String{
                                    
                                    if msg == "Vin no already exist"{
                                        
                                        let _id = resp["id"] as? Int
                                        let alertController = UIAlertController(title: Constant.AppName, message: "Vehicle Exist in Available Inventory! [Click OK to View.]", preferredStyle: .alert)
                                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                            
                                            let story = UIStoryboard.init(name: "Inventory", bundle: nil)
                                            let detail = story.instantiateViewController(identifier: "inventoryDetailViewController") as! inventoryDetailViewController
                                            detail.inventoryId = _id
                                            //detail.passDict_LayoutId = self.dictLayout
                                            detail.modalPresentationStyle = .fullScreen
                                            self.present(detail, animated: true, completion: nil)

                                        }
                                        alertController.addAction(OKAction)
                                        self.present(alertController, animated: true, completion:nil)

                                    }
                                }
                                else{
                                    let VIN = resp["VIN"] as! String
                                    let Make = resp["Make"] as! String
                                    let ModelYear = resp["ModelYear"] as! String
                                    let Model = resp["Model"] as! String
                                    
                                    
                                    self.lblTitle.text = "\(VIN) \(ModelYear) \(Make) \(Model)"
                                    
                                    self.txtYear.text = "\(ModelYear)"
                                    self.txtMake.text = "\(Make)"
                                    self.txtModel.text = "\(Model)"
                                    
                                    self.viewVin.isHidden = true
                                    self.view1.isHidden = false
                                    self.view_2.isHidden = false
                                    self.hgtVin.constant = 0
                                    self.view_1a.isHidden = false
                                    self.view_1b.isHidden = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        let getUrl = "\(Constant.colorApi)?id=2"
                                        self.callApiToSetData(strUrl: getUrl, strTyp: "i_color")
                                    }
                                }
                                

                            }
                        }
                    }
                }
            }
        }
        else if(strTyp == "i_color"){
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if( error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        if let arr = dict as? NSDictionary{
                            if let resp = arr[ "data"] as? [NSDictionary]{
                                if(resp.count > 0){
                                    for i in 0..<resp.count{
                                        let dictA = resp[i]
                                        self.arrIntcolor.add(dictA)
                                    }
                                }
                            }
                            
                        }
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let getUrl = "\(Constant.colorApi)?id=1"
                    self.callApiToSetData(strUrl: getUrl, strTyp: "ext_color")
                }
            }
        }
        else if(strTyp == "ext_color"){
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if( error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        if let arr = dict as? NSDictionary{
                            if let resp = arr[ "data"] as? [NSDictionary]{
                                if(resp.count > 0){
                                    for i in 0..<resp.count{
                                        let dictA = resp[i]
                                        self.arrExtcolor.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let getUrl = "\(Constant.default_branch_url)"
                    self.callApiToSetData(strUrl: getUrl, strTyp: "vehLot")
                }
                
            }
            
        }
        
        else if(strTyp == "vehLot"){
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if( error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrVehLot.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let getUrl = "\(Constant.buyer_url)"
                    self.callApiToSetData(strUrl: getUrl, strTyp: "buyer")
                }
                
            }
            
        }
        
        else if(strTyp == "buyer"){
            
            BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
                if( error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object: dict)
                        
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary] {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrBuyer.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
    }
    
    
}
extension addInventoryViewController: UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if ( pickerView == pv_Type){
            return self.arr_type.count
        }
        else if ( pickerView == pv_extPri_color || pickerView == pv_extSec_color || pickerView == pv_Ext_color){
            return self.arrExtcolor.count
        }
        else if ( pickerView == pv_Int_color){
            return self.arrIntcolor.count
        }
        
        else if ( pickerView == pv_MilStatus){
            return arrMilStatus.count
        }
        else if ( pickerView == pv_VehLot){
            return arrVehLot.count
        }
        else if ( pickerView == pv_Buyer){
            return arrBuyer.count
        }
        
        else if( pickerView == pv_acquiredFrom){
            return arrAcq_from.count
        }
        else if(pickerView == pv_pay_methd){
            return arrPay_methd.count
        }
        
        else{
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if( pickerView == pv_Type ){
            let strTitle = arr_type[row]
            return strTitle
        }
        else if ( pickerView == pv_extPri_color || pickerView == pv_extSec_color || pickerView == pv_Ext_color){
            let dict =  self.arrExtcolor[row] as! NSDictionary
            let strTitle = dict["color_name"] as! String
            return strTitle
        }
        else if ( pickerView == pv_Int_color){
            let dict =  self.arrIntcolor[row] as! NSDictionary
            let strTitle = dict["color_name"] as! String
            return strTitle
        }
        
        else if( pickerView == pv_MilStatus ){
            let strTitle = arrMilStatus[row]
            return strTitle
        }
        
        else if( pickerView == pv_VehLot){
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if ( pickerView == pv_Buyer){
            let dict = arrBuyer[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
            
        }
        
        else if( pickerView == pv_acquiredFrom){
            let strTitle = arrAcq_from[row]
            return strTitle
        }
        else if( pickerView == pv_pay_methd){
            let strTitle = arrPay_methd[row]
            return strTitle
        }
        
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if  (pickerView == pv_Type){
            self.txtType.text = (arr_type[row])
        }
        
        else if (pickerView == pv_extPri_color || pickerView == pv_extSec_color || pickerView == pv_Ext_color){
            
            let dict =  self.arrExtcolor[row] as! NSDictionary
            let strTitle = dict["color_name"] as! String
            if(pickerView == pv_extPri_color){
                self.txtExtPri_color.text = strTitle
            }
            else if(pickerView == pv_extSec_color){
                self.txtExt_Sec_color.text = strTitle
            }
            else if(pickerView == pv_Ext_color){
                self.txtExt_color.text = strTitle
            }
        }
        else if ( pickerView == pv_Int_color){
            let dict =  self.arrIntcolor[row] as! NSDictionary
            let strTitle = dict["color_name"] as! String
            self.txtInt_color.text = strTitle
        }
        
        else if  (pickerView == pv_MilStatus){
            self.txtMileageStatus.text = (arrMilStatus[row])
        }
        
        
        else if  (pickerView == pv_VehLot){
            
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtVehicleLot.text = strTitle
            let _id = dict["_id"] as! Int
            self.veh_id = "\(_id)"
            
        }
        
        else if ( pickerView == pv_Buyer){
            let dict = arrBuyer[row] as! NSDictionary
            let item = dict["title"] as! String
            let _id = dict["_id"] as! Int
            self.buyer_id = "\(_id)"
            self.txtBuyer.text = item
            
        }
        
        
        else if  (pickerView == pv_acquiredFrom){
            self.txtAcqFrom.text = (arrAcq_from[row])
        }
        else if (pickerView == pv_pay_methd){
            self.txtPaymethd.text = (arrPay_methd[row])
        }
        
        
        
    }
    func pickUpDate(_ textField : UITextField) {
        
        if(textField == self.txtDateAcquired){
            self.DatePickrDate_acq = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.DatePickrDate_acq.datePickerMode = .date
            self.DatePickrDate_acq.backgroundColor = UIColor.white
        }
        
        
    }
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        self.txtDateAcquired.text =
            formatter.string(from: DatePickrDate_acq.date)
        self.txtDateAcquired.resignFirstResponder()
        
        
    }
    @objc func cancelClickDate(){
        txtDateAcquired.resignFirstResponder()
    }
    
    
    func pickUp(_ textfield:UITextField){
        
        if(textfield == self.txtType){
            self.pv_Type = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Type.delegate = self
            self.pv_Type.dataSource = self
            self.pv_Type.backgroundColor = UIColor.white
            textfield.inputView = self.pv_Type
        }
        
        if(textfield == self.txtExt_color){
            self.pv_Ext_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Ext_color.delegate = self
            self.pv_Ext_color.dataSource = self
            self.pv_Ext_color.backgroundColor = UIColor.white
            textfield.inputView = self.pv_Ext_color
        }
        
        if(textfield == self.txtExtPri_color){
            self.pv_extPri_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_extPri_color.delegate = self
            self.pv_extPri_color.dataSource = self
            self.pv_extPri_color.backgroundColor = UIColor.white
            textfield.inputView = self.pv_extPri_color
            
        }
        if(textfield == self.txtExt_Sec_color){
            self.pv_extSec_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_extSec_color.delegate = self
            self.pv_extSec_color.dataSource = self
            self.pv_extSec_color.backgroundColor = UIColor.white
            textfield.inputView = self.pv_extSec_color
            
        }
        
        
        if(textfield == self.txtInt_color){
            self.pv_Int_color = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Int_color.delegate = self
            self.pv_Int_color.dataSource = self
            self.pv_Int_color.backgroundColor = UIColor.white
            textfield.inputView = self.pv_Int_color
            
        }
        else if( textfield == txtMileageStatus){
            self.pv_MilStatus = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_MilStatus.delegate = self
            self.pv_MilStatus.dataSource = self
            self.pv_MilStatus.backgroundColor = UIColor.white
            textfield.inputView = self.pv_MilStatus
        }
        
        
        else if( textfield == self.txtVehicleLot){
            self.pv_VehLot = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_VehLot.delegate = self
            self.pv_VehLot.dataSource = self
            self.pv_VehLot.backgroundColor = UIColor.white
            textfield.inputView = self.pv_VehLot
        }
        
        else if( textfield == self.txtBuyer){
            self.pv_Buyer = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Buyer.delegate = self
            self.pv_Buyer.dataSource = self
            self.pv_Buyer.backgroundColor = UIColor.white
            textfield.inputView = self.pv_Buyer
        }
        
        
        
        else if( textfield == txtAcqFrom){
            self.pv_acquiredFrom = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_acquiredFrom.delegate = self
            self.pv_acquiredFrom.dataSource = self
            textfield.inputView = self.pv_acquiredFrom
        }
        else if(textfield == txtPaymethd){
            self.pv_pay_methd = UIPickerView (frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width
                                                            , height: 216))
            self.pv_pay_methd.delegate = self
            self.pv_pay_methd.dataSource = self
            self.pv_pay_methd.backgroundColor = UIColor.white
            textfield.inputView = self.pv_pay_methd
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if( textField == self.txtType){
            self.pickUp(self.txtType)
        }
        else if( textField == self.txtVehicleLot){
            self.pickUp(self.txtVehicleLot)
        }
        else if( textField == txtMileageStatus){
            self.pickUp(self.txtMileageStatus)
        }
        else if( textField == txtAcqFrom){
            self.pickUp(self.txtAcqFrom)
        }
        else if(textField == txtPaymethd){
            self.pickUp(self.txtPaymethd)
        }
        else if(textField == txtDateAcquired){
            self.pickUp(self.txtDateAcquired)
        }
        else if(textField == txtExt_color){
            self.pickUp(self.txtExt_color)
        }
        else if(textField == txtExt_Sec_color){
            self.pickUp(self.txtExt_Sec_color)
        }
        else if(textField == txtInt_color){
            self.pickUp(self.txtInt_color)
        }
        else if(textField == txtBuyer){
            self.pickUp(self.txtBuyer)
        }
        
    }
    
    
    @objc func doneCick(){
        txtType.resignFirstResponder()
        txtExt_color.resignFirstResponder()
        txtExt_Sec_color.resignFirstResponder()
        txtInt_color.resignFirstResponder()
        txtExtPri_color.resignFirstResponder()
        txtMileageStatus.resignFirstResponder()
        txtVehicleLot.resignFirstResponder()
        txtBuyer.resignFirstResponder()
        txtPaymethd.resignFirstResponder()
        txtAcqFrom.resignFirstResponder()
        
    }
    
}
extension addInventoryViewController{
    //    @IBAction func SaveBtn(_ sender: UIButton){
    //        if sender.tag == 10
    //        {
    //            self.dismiss(animated: true, completion: nil)
    //        }
    //        else if sender.tag == 20{
    //            _ = self.dataDict["_id"] as! Int
    //            let params = ["acquiredFrom": self.txtAcqFrom!,
    //                          "acquiredFromName":self.txt_acqFromName!,"acquiredMileage":self.txtAcquiredMileage.text!,"acquiredPrice":self.txtAcquiredPrice.text!,"acquiredRefNumber":self.txtAcquiredRef_No.text!,"buyer":self.txtBuyer.text!,"exteriorColor":self.txtExt_color.text!,"exteriorPrimaryColor":self.txtExtPri_color.text!,"exteriorSecondaryColor":self.txtExt_Sec_color,"type":self.txtType.text!,"interiorColor":self.txtInt_color.text!,"mileageStatus":self.txtMileageStatus.text!,
    //
    //                          "dateAcquired": self.txtDateAcquired.text!,
    //            ] as [String: Any]
    //            Globalfunc.print(object: params)
    //        }
    //    }
    
    @IBAction func saveBtn(_ sender: UIButton)
    {
        
        if(txtAcquiredMileage.text == "" || txtAcquiredMileage.text?.count == 0 || txtAcquiredMileage.text == nil){
            txtAcquiredMileage.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill acquired mileage field.")
        }
        else if(txtMileageStatus.text == "" || txtMileageStatus.text?.count == 0 || txtMileageStatus.text == nil)
        {
            txtMileageStatus.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill mileage status field.")
        }
        else if(txtVehicleLot.text == "" || txtVehicleLot.text?.count == 0 || txtVehicleLot.text == nil)
        {
            txtVehicleLot.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill vehicle lot field.")
        }
        else if(txtAcquiredPrice.text == "" || txtAcquiredPrice.text?.count == 0 || txtAcquiredPrice.text == nil)
        {
            txtAcquiredPrice.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill acquired price field.")
        }
        
        else if(txtAcquiredMileage.text == "" || txtAcquiredMileage.text?.count == 0 || txtAcquiredMileage.text == nil){
            txtAcquiredMileage.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill acquired mileage field.")
        }
        else if(txtModel.text == "" || txtModel.text?.count == 0 || txtModel.text == nil){
            txtModel.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill Model field.")
        }
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            Globalfunc.showLoaderView(view: self.view)
            self.callAddInventoryBtnapi()
        }
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    func callAddInventoryBtnapi(){
        
        let params = [ "acquiredFrom": "0",
                       "acquiredFromName": self.txtAcqFrom.text!,
                       "acquiredMileage": self.txtAcquiredMileage.text!,
                       "acquiredPrice": self.txtAcquiredPrice.text!,
                       "acquiredRefNumber": self.txtAcquiredRef_No.text!,
        "bodyStyle": self.txtBodyStyle.text!,
        "buyer": self.buyer_id,
        "dateAcquired": self.txtDateAcquired.text!,
        "driveType": self.txtDriveType.text!,
        "engine": self.txtEngine.text!,
        "exteriorColor": self.txtExt_color.text!,
        "exteriorPrimaryColor": self.txtExtPri_color.text!,
        "exteriorSecondaryColor": self.txtExt_Sec_color.text!,
        "fuelType": self.txtFuelType.text!,
        "insid": (user?.institute_id!)!,
        "interiorColor": self.txtInt_color.text!,
        "inventoryStatus": 1,
        "make": self.txtMake.text!,
        "mileageStatus": self.txtMileageStatus.text!,
        "model": self.txtModel.text!,
        "paymentMethod": self.txtPaymethd.text!,
        "transmission": self.txtType.text!,
        "type": self.txtType.text!,
        "userid": (user?._id)!,
        "vehicleLot": self.veh_id,
        "vin": self.txtVinValidate.text!,
        "yearModel": self.txtYear.text!] as [String : Any]

        Globalfunc.print(object:params)
        self.callUpdateApi(param: params)
        
    }
    
    func callUpdateApi(param: [String : Any])
    {
            BaseApi.onResponsePostWithToken(url: Constant.inventory, controller: self, parms: param) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        if let arr = dict as? NSDictionary {
                            let msg = arr["msg"] as! String
                            let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
    }
}



