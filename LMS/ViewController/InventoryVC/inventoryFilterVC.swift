//
//  inventoryFilterVC.swift
//  LMS
//
//  Created by Apple on 26/02/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

protocol Inventory_filterAccountDelegate {
    func setInventoryFilteronPage(filter: [String: Any])
}


class inventoryFilterVC: UIViewController {

    @IBOutlet weak var txtSearch : UITextField!
    @IBOutlet weak var txtStatus : UITextField!
    @IBOutlet weak var txtLocation : UITextField!
    @IBOutlet weak var txtAltLocation: UITextField!
    @IBOutlet weak var txtStock : UITextField!
    @IBOutlet weak var txtVin : UITextField!
    @IBOutlet weak var txtMake : UITextField!
    @IBOutlet weak var txtModel : UITextField!
    @IBOutlet weak var txtBodyStle : UITextField!
    @IBOutlet weak var txtTrim : UITextField!
    @IBOutlet weak var txtLicence : UITextField!
    @IBOutlet weak var txtExt_color : UITextField!
    @IBOutlet weak var txtFlags : UITextField!
    
    @IBOutlet weak var txtMinyear : UITextField!
    @IBOutlet weak var txtMaxyear : UITextField!
    @IBOutlet weak var txtMin_dayslot : UITextField!
    @IBOutlet weak var txtMax_dayslot : UITextField!
    @IBOutlet weak var txtMin_price : UITextField!
    @IBOutlet weak var txtMax_price : UITextField!
    @IBOutlet weak var txtMax_down : UITextField!
    @IBOutlet weak var txtMax_terms : UITextField!
    @IBOutlet weak var txtMin_Mileage : UITextField!
    @IBOutlet weak var txtMax_Mileage : UITextField!
    @IBOutlet weak var txtMin_Cost : UITextField!
    @IBOutlet weak var txtMax_Cost : UITextField!

    var delegate: Inventory_filterAccountDelegate?
    
    var passDict_LayoutId : NSDictionary = [:]
    
    var arrAltLocation : NSMutableArray = []
    var arrLocation : NSMutableArray = []
    var arrStatus : NSMutableArray = []
    var arrFlag : NSMutableArray = []
    var arrMake : NSMutableArray = []
    
    var pickerAltLocation: UIPickerView!
    var pickerViewLocation: UIPickerView!
    var pickerViewStatus: UIPickerView!
    var pickerViewMake: UIPickerView!
    var pickerViewFlag: UIPickerView!
    
    var selectedAltLocId = [String]()
    var selectedAltLocTitle = [String]()
    
    var selectedLoc = [String]()
    var selectedLocTitle = [String]()
    
    var selectedAccountSatusId = [String]()
    var selectedAccountSatusTitle = [String]()
    
    var selectedMAkeTitle = [String]()
    
    var selectedFlagId = [String]()
    var selectedFlagTitle = [String]()
    
    var arrYear = [String]()
    var arrDates = [String]()
    var arrAmount = [String]()
    var arrMileage = [String]()
    
    // var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callApiToSetDataonPicker()
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func clickOnSearchBtn(_ sender: UIButton){
        
        let queLayoutId = passDict_LayoutId["layout_id"] as! Int
        let defaultRecordsPage = passDict_LayoutId["defaultRecordsPage"] as! Int
        let order1 = passDict_LayoutId["order1"] as! String
        let order2 = passDict_LayoutId["order2"] as! String
        let order3 = passDict_LayoutId["order1"] as! String
        let sort1 = passDict_LayoutId["sort1"] as! String
        let sort2 = passDict_LayoutId["sort2"] as! String
        let sort3 = passDict_LayoutId["sort3"] as! String
        
        var straccount_status = ""
        if(selectedAccountSatusId.count > 0){
            straccount_status = selectedAccountSatusId.joined(separator: ",")
            //            url = "/lms/account/search-accounts?account_status=\(straccount_status)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strAltLoc = ""
        if(selectedAltLocId.count > 0){
            strAltLoc = selectedAltLocId.joined(separator: ",")
            //  url = "/lms/account/search-accounts?account_type=\(straccount_type)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strLoc = ""
        if(selectedLoc.count > 0){
            strLoc = selectedLoc.joined(separator: ",")
            //url = "/lms/account/search-accounts?channel=\(strchannel)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strMake = ""
        if(selectedMAkeTitle.count > 0){
            strMake = selectedMAkeTitle.joined(separator: ",")
            //  url = "/lms/account/search-accounts?portfolio=\(strportfolio)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        var strflags = ""
        if(selectedFlagId.count > 0){
            strflags = selectedFlagId.joined(separator: ",")
            
            //  url = "/lms/account/search-accounts?flags=\(strflags)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
        
        
        
        let fParam = [ "alternate_location": strAltLoc,
                       "bodyStyle": self.txtBodyStle.text!,
                       "currentPageNumber": 1,
                       "defaultRecordsPage": defaultRecordsPage,
                       "extColor": self.txtExt_color.text!,
                       "flags": strflags,
                       "inventory_status": straccount_status,
                       "isGet": false,
                       "licensePlate": self.txtLicence.text!,
                       "location": strLoc,
                       "make": strMake,
                       "maxAskPrice": "",
                       "maxAskingDown": "",
                       "maxAskingTerms": "",
                       "maxCost": "",
                       "maxDaysOnLot": "",
                       "maxMileage": "",
                       "maxYear": "",
                       "minAskPrice": "",
                       "minCost": "",
                       "minDaysOnLot": "",
                       "minMileage": "",
                       "minYear": "",
                       "model": self.txtModel.text!,
                       "order1": order1,
                       "order2": order2,
                       "order3": order3,
                       "queueLayoutsId": queLayoutId,
                       "search": "\(txtSearch.text!)",
            "sort1": sort1,
            "sort2": sort2,
            "sort3": sort3,
            "stock": self.txtStock.text!,
            "trimLevel": self.txtTrim.text!,
            "vin": self.txtVin.text!] as [String : Any]
        
        
        Globalfunc.print(object:fParam)
        
        delegate?.setInventoryFilteronPage(filter: fParam)
    }
    
    
    @IBAction func clikcOnCrossBtn(_ sender : UIButton)
    {
        self.viewDidLoad()
    }
    
    @IBAction func clickOnOkBtn(_ sneder: UIButton)
    {
        Globalfunc.showLoaderView(view: self.view)
        let params = [
            "insid": (user?.institute_id!)!,
            //    "title": self.txtTitle.text!,
            "type":"search-account",
            //   "url": url,
            "userid": (user?._id)!] as [String : Any]
        
        BaseApi.onResponsePostWithToken(url: Constant.favorite, controller: self, parms: params) { (dict, error) in
            
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.viewDidLoad()
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}



extension inventoryFilterVC{
    
    func callApiToSetDataonPicker(){
        
        let url = "\(Constant.master_url)/inventory-master"
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    
                    Globalfunc.print(object:dict)
                    
                    if let response = dict as? NSDictionary{
                        
                        if let data = response["data"] as? NSDictionary{
                            if let accountType = data["altLocation"] as? [NSDictionary] {
                                if(accountType.count > 0){
                                    for i in 0..<accountType.count{
                                        let dictA = accountType[i]
                                        self.arrAltLocation.add(dictA)
                                    }
                                }
                            }
                            if let accountStatus = data["inventoryStatus"] as? [NSDictionary] {
                                if(accountStatus.count > 0){
                                    for i in 0..<accountStatus.count{
                                        let dictA = accountStatus[i]
                                        self.arrStatus.add(dictA)
                                    }
                                }
                            }
                            
                            
                            
                            if let portfolio = data["location"] as? [NSDictionary] {
                                if(portfolio.count > 0){
                                    for i in 0..<portfolio.count{
                                        let dictA = portfolio[i]
                                        self.arrLocation.add(dictA)
                                    }
                                }
                            }
                            
                            
                            if let flag = data["flag"] as? [NSDictionary] {
                                if(flag.count > 0){
                                    for i in 0..<flag.count{
                                        let dictA = flag[i]
                                        self.arrFlag.add(dictA)
                                    }
                                }
                            }
                            
                            
                            if let location = data["make"] as? [NSDictionary] {
                                if(location.count > 0){
                                    for i in 0..<location.count{
                                        let dictA = location[i]
                                        self.arrMake.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                    
                    if let arr = dict as? NSDictionary {
                        if let msg = arr["message"] as? String{
                            if(msg == "Your token has expired."){
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
        
    }
}

extension inventoryFilterVC : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        //  if component == 0 {
        if pickerView == pickerAltLocation{
            return self.arrAltLocation.count
        }
        else if(pickerView == pickerViewLocation){
            return self.arrLocation.count
        }
            
        else if(pickerView == pickerViewStatus){
            return self.arrStatus.count
        }
            
        else if(pickerView == pickerViewMake){
            return self.arrMake.count
        }
        else if(pickerView == pickerViewFlag){
            return self.arrFlag.count
        }
            
        else{
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let attributedString = NSMutableAttributedString()
        if pickerView == pickerAltLocation{
            
            let dict = arrAltLocation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in  selectedAltLocId{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        else if pickerView == pickerViewLocation{
            
            
            let dict = arrLocation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedLoc{
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewStatus){
            
            let dict = arrStatus[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedAccountSatusId {
                if item == "\(_id)"{
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewMake){
            
            let dict = arrMake[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedMAkeTitle{
                if item == "\(strTitle)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
            
        else if(pickerView == pickerViewFlag){
            
            let dict = arrFlag[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            let attrString = NSAttributedString(string: strTitle , attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            attributedString.append(attrString)
            
            for item in selectedFlagId {
                if item == "\(_id)" {
                    attributedString.append(NSAttributedString(string: " "))
                    let imageAtchement = NSTextAttachment()
                    imageAtchement.image = UIImage(named: "check")
                    imageAtchement.bounds = CGRect(x: 0, y: 0, width: 20, height: 20)
                    let imageAttachString = NSAttributedString(attachment: imageAtchement)
                    attributedString.append(imageAttachString)
                }
            }
        }
        return attributedString
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if(pickerView == pickerAltLocation){
            
            let dict = arrAltLocation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            if selectedAltLocId.contains("\(_id)") {
                var index = 0
                for item in selectedAltLocId {
                    if item == "\(_id)" {
                        selectedAltLocId.remove(at: index)
                        selectedAltLocTitle.remove(at: index)
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedAltLocId.append("\(_id)")
                selectedAltLocTitle.append(strTitle)
            }
            self.txtAltLocation.text = selectedAltLocTitle.joined(separator: ", ")    //selectedArray is array has selected item
            
            //            let queLayoutId = passDict_LayoutId["layout_id"] as! Int
            //            url = "/lms/account/search-accounts?account_type=\(txtAcctType.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
            
            self.pickerAltLocation.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewLocation){
            
            let dict = arrLocation[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedLoc.contains("\(_id)") {
                var index = 0
                for item in selectedLoc {
                    if item == "\(_id)" {
                        
                        selectedLoc.remove(at: index)
                        selectedLocTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedLoc.append("\(_id)")
                selectedLocTitle.append(strTitle)
            }
            
            self.txtLocation.text = selectedLocTitle.joined(separator: ", ")
            self.pickerViewLocation.reloadAllComponents()
        }
            //
        else if(pickerView == pickerViewStatus){
            
            let dict = arrStatus[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedAccountSatusId.contains("\(_id)") {
                var index = 0
                for item in selectedAccountSatusId {
                    if item == "\(_id)" {
                        selectedAccountSatusId.remove(at: index)
                        selectedAccountSatusTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedAccountSatusId.append("\(_id)")
                selectedAccountSatusTitle.append(strTitle)
            }
            self.txtStatus.text = selectedAccountSatusTitle.joined(separator: ", ")
            self.pickerViewStatus.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewMake){
            
            let dict = arrMake[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            
            if selectedMAkeTitle.contains("\(strTitle)") {
                var index = 0
                for item in selectedMAkeTitle {
                    if item == "\(strTitle)" {
                        selectedMAkeTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedMAkeTitle.append("\(strTitle)")
            }
            
            self.txtMake.text = selectedMAkeTitle.joined(separator: ", ")
            self.pickerViewMake.reloadAllComponents()
        }
            
        else if(pickerView == pickerViewFlag){
            
            let dict = arrFlag[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            let _id = dict["_id"] as! Int
            
            if selectedFlagId.contains("\(_id)") {
                var index = 0
                for item in selectedFlagId {
                    if item == "\(_id)" {
                        selectedFlagId.remove(at: index)
                        selectedFlagTitle.remove(at: index)
                        
                    } else {
                        index += 1
                    }
                }
            } else {
                selectedFlagId.append("\(_id)")
                selectedFlagTitle.append(strTitle)
            }
            
            self.txtFlags.text = selectedFlagTitle.joined(separator: ", ")
            self.pickerViewFlag.reloadAllComponents()
        }
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtAltLocation){
            self.pickUp(txtAltLocation)
        }
        else if(textField == self.txtLocation){
            self.pickUp(txtLocation)
        }
        else if(textField == self.txtStatus){
            self.pickUp(txtStatus)
        }
        else if(textField == self.txtMake){
            self.pickUp(txtMake)
        }
        else if(textField == self.txtFlags){
            self.pickUp(txtFlags)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let queLayoutId = passDict_LayoutId["layout_id"] as! Int
        if(textField == txtSearch){
           // url = "/lms/account/search-accounts?search=\(txtSearch.text!)&order3=asc&sort3=account_no&order2=asc&sort2=account_no&order1=asc&sort1=account_no&defaultRecordsPage=10&currentPageNumber=1&queueLayoutsId=\(queLayoutId)"
        }
    }
    
    
    
    
    func pickUp(_ textField : UITextField) {
        
         if(textField == self.txtAltLocation){
             self.pickerAltLocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
             self.pickerAltLocation.delegate = self
             self.pickerAltLocation.dataSource = self
             self.pickerAltLocation.backgroundColor = UIColor.white
             textField.inputView = self.pickerAltLocation
         }
         else if(textField == self.txtLocation){
             self.pickerViewLocation = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
             self.pickerViewLocation.delegate = self
             self.pickerViewLocation.dataSource = self
             self.pickerViewLocation.backgroundColor = UIColor.white
             textField.inputView = self.pickerViewLocation
         }
         else if(textField == self.txtStatus){
             self.pickerViewStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
             self.pickerViewStatus.delegate = self
             self.pickerViewStatus.dataSource = self
             self.pickerViewStatus.backgroundColor = UIColor.white
             textField.inputView = self.pickerViewStatus
         }
 
         else if(textField == self.txtMake){
             self.pickerViewMake = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
             self.pickerViewMake.delegate = self
             self.pickerViewMake.dataSource = self
             self.pickerViewMake.backgroundColor = UIColor.white
             textField.inputView = self.pickerViewMake
 
         }
         else if(textField == self.txtFlags){
             self.pickerViewFlag = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
             self.pickerViewFlag.delegate = self
             self.pickerViewFlag.dataSource = self
             self.pickerViewFlag.backgroundColor = UIColor.white
             textField.inputView = self.pickerViewFlag
 
         }
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txtStatus.resignFirstResponder()
        txtLocation.resignFirstResponder()
       txtAltLocation.resignFirstResponder()
       txtMake.resignFirstResponder()
        txtFlags.resignFirstResponder()
    }
}
