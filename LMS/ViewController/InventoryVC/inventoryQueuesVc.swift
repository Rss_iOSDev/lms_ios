//
//  inventoryQueuesVc.swift
//  LMS
//
//  Created by Dr.Mac on 13/04/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class inventoryQueuesVc: UIViewController {
    
    @IBOutlet weak var txtQueueOwner: UITextField!
    @IBOutlet weak var txtQueueGroup: UITextField!
    @IBOutlet weak var txtQueue: UITextField!
    
    @IBOutlet weak var viewMyqueu: UIView!
    @IBOutlet weak var viewAllQueue: UIView!
    
    var arrQueueOwner = ["My Queues","All Queues"]
    
    var pickerQueuOwner: UIPickerView!
    var pickerqueGrup: UIPickerView!
    var pickerQueue: UIPickerView!
    
    var arrGrup = NSMutableArray()
    var arrQueue = NSMutableArray()
    
    var grupId = ""
    var queueID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let item = arrQueueOwner[0]
        self.txtQueueOwner.text = item
        self.viewMyqueu.isHidden = false
        self.viewAllQueue.isHidden = true
        
        // Do any additional setup after loading the view.
    }
}

extension inventoryQueuesVc{
    
    func callQueueGrupApi(strTyp : String){
        
        if(strTyp == "group"){
            let url = "\(Constant.queue_group)?groupType=Inventory"
            BaseApi.callApiRequestForGet(url: url) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary]
                            {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrGrup.add(dictA)
                                    }
                                    let obj = self.arrGrup[0] as! NSDictionary
                                    self.grupId = "\(obj["_id"] as! Int)"
                                }
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                            self.callQueueGrupApi(strTyp: "queue")
                        })
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        
        else if(strTyp == "queue"){
            
            let url = "\(Constant.queue)/datasource?id=\(self.grupId)"
            BaseApi.callApiRequestForGet(url: url) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.print(object:dict)
                        
                        if let response = dict as? NSDictionary {
                            if let arr = response["data"] as? [NSDictionary]
                            {
                                if(arr.count > 0){
                                    for i in 0..<arr.count{
                                        let dictA = arr[i]
                                        self.arrQueue.add(dictA)
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
    }
}

extension inventoryQueuesVc : UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if(pickerView == pickerQueuOwner){
            return arrQueueOwner.count
        }
        else if(pickerView == pickerqueGrup){
            return arrGrup.count
        }
        else if(pickerView == pickerQueue){
            return arrQueue.count
        }

        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        if(pickerView == pickerQueuOwner){
            let item = arrQueueOwner[row]
            return item
        }
        else if(pickerView == pickerqueGrup){
            let dict = arrGrup[row] as! NSDictionary
            let title = dict["title"] as! String
            return title
        }
        else if(pickerView == pickerQueue){
            if(arrQueue.count > 0){
                let dict = arrQueue[row] as! NSDictionary
                let queue_group = dict["queue_group"] as! NSDictionary
                let title = queue_group["title"] as! String
                let queue_name = dict["queue_name"] as! String
                let str = "[\(title)] \(queue_name)"
                return str
            }
            else{
                return ""
            }
        }

        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if(pickerView == pickerQueuOwner){
            let item = arrQueueOwner[row]
            self.txtQueueOwner.text = item
            
            if(row == 0){
                self.viewMyqueu.isHidden = false
                self.viewAllQueue.isHidden = true

            }
            else if(row == 1){
                self.viewMyqueu.isHidden = true
                self.viewAllQueue.isHidden = false
                self.callQueueGrupApi(strTyp: "group")
            }
        }
        else if(pickerView == pickerqueGrup){
            let dict = arrGrup[row] as! NSDictionary
            let title = dict["title"] as! String
            self.txtQueueGroup.text = title
            self.grupId = "\(dict["_id"] as! Int)"
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.callQueueGrupApi(strTyp: "queue")
            })
        }
        else if(pickerView == pickerQueue){
            if(arrQueue.count > 0){
                let dict = arrQueue[row] as! NSDictionary
                let queue_group = dict["queue_group"] as! NSDictionary
                let title = queue_group["title"] as! String
                let queue_name = dict["queue_name"] as! String
                let str = "[\(title)] \(queue_name)"
                self.txtQueue.text = str
                self.queueID = "\(dict["_id"] as! Int)"
                self.passToViewController()
                
            }
        }

    }
    
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == self.txtQueueOwner){
            self.pickUp(txtQueueOwner)
        }
        if(textField == self.txtQueueGroup){
            self.pickUp(txtQueueGroup)
        }
        if(textField == self.txtQueue){
            self.pickUp(txtQueue)
        }

    }
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txtQueueOwner){
    self.pickerQueuOwner = UIPickerView(frame:CGRect(x:0,y:0,width:self.view.frame.size.width, height: 216))
            self.pickerQueuOwner.delegate = self
            self.pickerQueuOwner.dataSource = self
            self.pickerQueuOwner.backgroundColor = UIColor.white
            textField.inputView = self.pickerQueuOwner
        }
       else if(textField == self.txtQueueGroup){
            self.pickerqueGrup = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerqueGrup.delegate = self
            self.pickerqueGrup.dataSource = self
            self.pickerqueGrup.backgroundColor = UIColor.white
            textField.inputView = self.pickerqueGrup
        }
        else if(textField == self.txtQueue){
            self.pickerQueue = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerQueue.delegate = self
            self.pickerQueue.dataSource = self
            self.pickerQueue.backgroundColor = UIColor.white
            textField.inputView = self.pickerQueue
        }

        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    //MARK:- Button
    
    //MARK:- Button
    @objc func doneClick() {
        txtQueueOwner.resignFirstResponder()
        txtQueueGroup.resignFirstResponder()
        txtQueue.resignFirstResponder()
    }
    
    func passToViewController(){
        let story = UIStoryboard.init(name: "Queues", bundle: nil)
        let edit = story.instantiateViewController(withIdentifier: "queueGridViewController") as! queueGridViewController
        edit.passId = self.queueID
        edit.isTyp = "inventory"
        self.navigationController?.pushViewController(edit, animated: true)

    }
}
