
//
//  pricingViewController.swift
//  LMS
//
//  Created by Dr.Mac on 24/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class EvaluatePViewController: UIViewController {
    @IBOutlet weak var txtVehicleLot:UITextField!
    @IBOutlet weak var txtMileage:UITextField!
    @IBOutlet weak var txtMileageStatus:UITextField!
    @IBOutlet weak var txtEval_Date:UITextField!
    @IBOutlet weak var txtEval_Group:UITextField!
    @IBOutlet weak var txtAuctionLane:UITextField!
    @IBOutlet weak var txtAuction_no :UITextField!
    @IBOutlet weak var txtofferPice:UITextField!
    @IBOutlet weak var txtBidPrice:UITextField!
    @IBOutlet weak var txtBid_expire:UITextField!
    
    var arrUrl = [Constant.default_branch_url]
    
    var pv_vehLot: UIPickerView!
    var arrVehLot = NSMutableArray()
    var veh_id = ""
    
    var pv_Mil_status: UIPickerView!
    var arrMil_status = ["Actual Miles","True Mileage Unknown","Beyond Mechanical Limits","Exempt"]
    
    var evalDatePicker: UIDatePicker!
    var expDatePicker: UIDatePicker!
    
    var strSelect = ""
    
    var dataDict = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        Globalfunc.showLoaderView(view: self.view)
        self.callAllPickerData()
        
        
    }
    
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
//change keys

extension EvaluatePViewController{
    
    func callAllPickerData()
    {
        let group = DispatchGroup() // initialize
        self.arrUrl.forEach { obj in
            
            group.enter() // wait
            BaseApi.callApiRequestForGet(url: obj) { (dict, error) in
                if(error == ""){
                    OperationQueue.main.addOperation {
                        if(obj == Constant.default_branch_url){
                            if let response = dict as? NSDictionary {
                                if let arr = response["data"] as? [NSDictionary] {
                                    if(arr.count > 0){
                                        for i in 0..<arr.count{
                                            let dictA = arr[i]
                                            self.arrVehLot.add(dictA)
                                        }
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            self.callParticularSate()
                        }
                        group.leave()
                    }
                }
                else
                {
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    }
                }
            }
        }
        group.notify(queue: .main) {
        }
    }
}

extension EvaluatePViewController {
    
    
    func callParticularSate()
    {
        if let _id  = userDef.object(forKey: "inventoryId") as? Int {
            let strGetUrl = "\(Constant.inventory)/pricing?id=\(_id)"
            self.callApiToSetData(strUrl: strGetUrl)
        }
    
    }
    
    func callApiToSetData(strUrl : String){
        
        BaseApi.callApiRequestForGet(url: strUrl) { (dict, error) in
            if(error == ""){
                
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.print(object:dict)
                    if let arr = dict as? NSDictionary {
                        if let vehicle = arr["data"] as? NSDictionary{
                            
                            self.dataDict = vehicle
                            
                            if let acquiredMileage = vehicle["acquiredMileage"] as? Int{
                                self.txtMileage.text = "\(acquiredMileage)"
                            }
                            
                            if let mileageStatus = vehicle["mileageStatus"] as? String{
                                self.txtMileageStatus.text = mileageStatus
                            }
                            
                            
                            if let vehicleLot = vehicle["vehicleLot"] as? Int{
                                self.veh_id = "\(vehicleLot)"
                                for i in 0..<self.arrVehLot.count{
                                    let dict = self.arrVehLot[i] as! NSDictionary
                                    let _id = dict["_id"] as! Int
                                    if(_id == vehicleLot){
                                        let strTitle = dict["title"] as! String
                                        self.txtVehicleLot.text = strTitle
                                        break
                                    }
                                }
                            }
                            
                            if let evaluationDate = vehicle["evaluationDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: evaluationDate)
                                self.txtEval_Date.text = formatter.string(from: date!)
                            }
                            
                            if let auctionLane = vehicle["auctionLane"] as? String{
                                self.txtAuctionLane.text = "\(auctionLane)"
                            }
                            
                            
                            if let auctionNumber = vehicle["auctionNumber"] as? String{
                                self.txtAuction_no.text = "\(auctionNumber)"
                            }
                            
                            if let offerPrice = vehicle["offerPrice"] as? Int{
                                self.txtofferPice.text = "\(offerPrice)"
                            }
                            
                            if let bidPrice = vehicle["bidPrice"] as? Int{
                                self.txtBidPrice.text = "\(bidPrice)"
                            }
                            
                            if let bidExpireDate = vehicle["bidExpireDate"] as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy"
                                let date = Date.dateFromISOString(string: bidExpireDate)
                                self.txtBid_expire.text = formatter.string(from: date!)
                            }
                            
                            
                        }
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}

extension EvaluatePViewController : UIPickerViewDelegate, UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView == pv_vehLot{
            return self.arrVehLot.count
        }
        else if(pickerView == pv_Mil_status){
            return self.arrMil_status.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pv_vehLot{
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            return strTitle
        }
        else if(pickerView == pv_Mil_status){
            let strTitle = arrMil_status[row]
            return strTitle
        }
        return ""
    }
    //
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if pickerView == pv_vehLot{
            let dict = arrVehLot[row] as! NSDictionary
            let strTitle = dict["title"] as! String
            self.txtVehicleLot.text = strTitle
            let _id = dict["_id"] as! Int
            self.veh_id = "\(_id)"
        }
        
        else if(pickerView == pv_Mil_status){
            let strTitle = arrMil_status[row]
            self.txtMileageStatus.text = strTitle
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if(textField == self.txtVehicleLot){
            self.pickUp(txtVehicleLot)
        }
        else if(textField == self.txtMileageStatus){
            self.pickUp(txtMileageStatus)
        }
        else if(textField == self.txtEval_Date){
            self.pickUpDateTime(txtEval_Date)
        }
        else if(textField == self.txtBid_expire){
            self.pickUpDateTime(txtBid_expire)
        }
        
    }
    
    func pickUpDateTime(_ textField : UITextField) {
        
        if(textField == self.txtEval_Date){
            self.evalDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.evalDatePicker.datePickerMode = .date
            self.evalDatePicker.backgroundColor = UIColor.white
            textField.inputView = self.evalDatePicker
            self.strSelect = "evalDate"
        }
        else if(textField == self.txtBid_expire){
            self.expDatePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.expDatePicker.datePickerMode = .date
            self.expDatePicker.backgroundColor = UIColor.white
            textField.inputView = self.expDatePicker
            self.strSelect = "expire"
        }
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClickDate))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClickDate))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func doneClickDate() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        
        if(self.strSelect == "expire"){
            self.txtBid_expire.text = formatter.string(from: expDatePicker.date)
            self.txtBid_expire.resignFirstResponder()
            
        }
        else if(self.strSelect == "evalDate"){
            self.txtEval_Date.text = formatter.string(from: evalDatePicker.date)
            self.txtEval_Date.resignFirstResponder()
            
        }
        
        
    }
    
    @objc func cancelClickDate() {
        txtEval_Date.resignFirstResponder()
        txtBid_expire.resignFirstResponder()
    }
    
    // ?
    func pickUp(_ textField : UITextField) {
        if(textField == self.txtVehicleLot){
            self.pv_vehLot = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_vehLot.delegate = self
            self.pv_vehLot.dataSource = self
            self.pv_vehLot.backgroundColor = UIColor.white
            textField.inputView = self.pv_vehLot
        }
        else if(textField == self.txtMileageStatus){
            self.pv_Mil_status = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pv_Mil_status.delegate = self
            self.pv_Mil_status.dataSource = self
            self.pv_Mil_status.backgroundColor = UIColor.white
            textField.inputView = self.pv_Mil_status
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(doneClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    //MARK:- Button
    @objc func doneClick() {
        txtVehicleLot.resignFirstResponder()
        txtMileageStatus.resignFirstResponder()
    }
}

extension EvaluatePViewController{
    
    @IBAction func clickBtn(_ sender: UIButton){
        
        if sender.tag == 10
        {
            self.dismiss(animated: true, completion: nil)
        }
        else if sender.tag == 20{

            let inventory_id = self.dataDict["_id"] as! Int
                    let params = [
                        "acquiredMileage": self.txtMileage.text!,
                        "auctionLane": self.txtAuctionLane.text!,
                        "auctionNumber": self.txtAuction_no.text!,
                        "bidExpireDate": self.txtBid_expire.text!,
                        "bidPrice": self.txtBidPrice.text!,
                        "evaluationDate": self.txtEval_Date.text!,
                        "evaluationGroup": 0,
                        "id": inventory_id,
                        "insid": (user?.institute_id!)!,
                        "mileageStatus": self.txtMileageStatus.text!,
                        "offerPrice": self.txtofferPice.text!,
                        "userid": (user?._id)!,
                        "vehicleLot": self.veh_id] as [String : Any]
                    Globalfunc.print(object: params)
                    self.callUpdateApi(param: params)
        }
    }
    
    
    func callUpdateApi(param: [String : Any])
    {
        let strUrl = "\(Constant.inventory)/pricing"
        BaseApi.onResponsePutWithToken(url: strUrl, controller: self, parms: param as NSDictionary) { (dict, error) in
            if(error == ""){
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    if let arr = dict as? NSDictionary {
                        let msg = arr["msg"] as! String
                        let alertController = UIAlertController(title: Constant.AppName, message: msg, preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            self.dismiss(animated: true, completion: nil)
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}
