//
//  pricing1ViewController.swift
//  LMS
//
//  Created by Dr.Mac on 24/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class pricingViewController: UIViewController {
    @IBOutlet weak var txtNormalPrice:UITextField!
    @IBOutlet weak var txtPromotional_Price:UITextField!
    @IBOutlet weak var txtWholesalePrice:UITextField!
    @IBOutlet weak var txtAskingDown:UITextField!
    @IBOutlet weak var txtAskingTerm:UITextField!
    //costdetsils
    @IBOutlet weak var txtPrimaryLot:UITextField!
    @IBOutlet weak var txtPurchaseDate:UITextField!
    @IBOutlet weak var txtVehicleCost:UITextField!
    @IBOutlet weak var txtPackFee:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func clickOnBackBtn(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
