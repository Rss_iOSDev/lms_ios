//
//  inventoryTabsViewController.swift
//  LMS
//
//  Created by Apple on 17/03/21.
//  Copyright © 2021 Reinforce. All rights reserved.
//

import UIKit

class inventoryTabsViewController: UIViewController {
    
    @IBOutlet weak var collTitles : UITableView!
    @IBOutlet weak var lblAcctno : UILabel!
    
    
    var arrTitles = ["PRICING","VEHICLE","FILES","TITLING","NOTES"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collTitles.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func clickONBackBtn(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension inventoryTabsViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.collTitles.dequeueReusableCell(withIdentifier: "colltitleCell") as! colltitleCell
        cell.lblTitle.text = self.arrTitles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if let status  = userDef.object(forKey: "inventory_status") as? String {
                if(status == "Evaluating"){
                    self.presentViewControllerBasedOnIdentifier("EvaluatePViewController", strStoryName: "Inventory")
                }
                else{
                    self.presentViewControllerBasedOnIdentifier("pricingViewController", strStoryName: "Inventory")
                }
            }
        case 1:
            self.presentViewControllerBasedOnIdentifier("InevtoryvehicleVC", strStoryName: "Inventory")
        case 2:
            self.presentViewControllerBasedOnIdentifier("inventoryFilesVC", strStoryName: "Inventory")
        case 3:
            self.presentViewControllerBasedOnIdentifier("titlingViewController", strStoryName: "Inventory")
        case 4:
            self.presentViewControllerBasedOnIdentifier("inventoryNotesVc", strStoryName: "Inventory")
        default:
            Globalfunc.print(object:"default")
        }
    }
}
