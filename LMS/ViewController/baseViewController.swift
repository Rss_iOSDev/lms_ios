//
//  baseViewController.swift
//  LMS
//
//  Created by Reinforce on 21/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class baseViewController: UIViewController , SlideMenuDelegate{
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent // .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = UIColorFromHex(rgbValue: 0x3C83C3, alpha: 1.0)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func slideMenuItemSelectedAtIndex(_ section: Int, index: Int, strType: String) {
        let _ : UIViewController = self.navigationController!.topViewController!
        if(strType == "menu")
        {
          
        }
        else{
            
            switch index {
            case 0:
                openViewControllerBasedOnIdentifier("editProfileViewController", strStoryName: "Main")
                break
                
            case 1:
                
                userDef.removeObject(forKey: "userInfo")
                let login = self.storyboard?.instantiateViewController(identifier: "loginViewController") as! loginViewController
                self.navigationController?.pushViewController(login, animated: true)
                break
                
            default:
                print("Home\n", terminator: "")
            }
            
        }
      }
      
//    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
//        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
//
//        let topViewController : UIViewController = self.navigationController!.topViewController!
//
//        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
//           self.navigationController!.pushViewController(destViewController, animated: true)
//        } else {
//            self.navigationController!.pushViewController(destViewController, animated: true)
//        }
//    }
    
    func addSlideMenuButton(_ btn: UIButton){
        btn.addTarget(self, action: #selector(baseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
    }
    
    func addProfileMenuButton(_ btn: UIButton){
           btn.addTarget(self, action: #selector(baseViewController.onSlideProfileButtonPressed(_:)), for: UIControl.Event.touchUpInside)
       }
       
    
    
    @objc func onSlideProfileButtonPressed(_ sender:UIButton)
    {
        
       let story = UIStoryboard.init(name: "Main", bundle: nil)
       let menuVC = story.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
       self.view.addSubview(menuVC.view)
       self.addChild(menuVC)
       menuVC.view.layoutIfNeeded()
                   
                   
        menuVC.isType = "right"
                       
       menuVC.view.frame=CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
       UIView.animate(withDuration: 0.3, animations: { () -> Void in
                                 menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
                                 sender.isEnabled = true
       }, completion: nil)
    }

    
    
    
    @objc func onSlideMenuButtonPressed(_ sender:UIButton){
        
          // sender.isEnabled = false
          // sender.tag = 10
           
          let story = UIStoryboard.init(name: "Main", bundle: nil)
          let menuVC = story.instantiateViewController(withIdentifier: "sideMenuViewController") as! sideMenuViewController
                 
                 menuVC.isType = "left"
                 
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()
                 
            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                               menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
                               sender.isEnabled = true
            }, completion:nil)
           
    }
        
        
        
        
        

        
        
      
    
    
// Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onMoveNewSideMenu(timer:)), userInfo: ["theButton" :sender], repeats: false)
//    @objc func onMoveNewSideMenu(timer:Timer)
//    {
//
//        let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
//        let sender = (userInfo["theButton"] as! UIButton)
//    }
       
}


