//
//  bookAccountViewController.swift
//  LMS
//
//  Created by Reinforce on 26/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

class bookAccountViewController: baseViewController {
    
    @IBOutlet weak var tblBookList : UITableView!
    @IBOutlet weak var lblstatus : UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    
    //filter View Outlets
    @IBOutlet weak var viewSearchByFilter : UIView!
    @IBOutlet weak var txt_BokingID : UITextField!
    @IBOutlet weak var txt_BokingStatus : UITextField!
    @IBOutlet weak var txt_lname : UITextField!
    var pickerViewBStatus : UIPickerView!
    

    
    var arrBookAcct : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        addSlideMenuButton(btnMenu)
        tblBookList.tableFooterView = UIView()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.viewSearchByFilter.isHidden = true
        
        self.arrBookAcct = []
        tblBookList.isHidden = true
        lblstatus.isHidden =  true
        
        Globalfunc.showLoaderView(view: self.view)
        callBorrowerApiOnPage(url: Constant.borrower_url, strCheck: "list")
        
    }
    
    func callBorrowerApiOnPage(url : String, strCheck: String){
        
        BaseApi.callApiRequestForGet(url: url) { (dict, error) in
            if(error == ""){
                if(strCheck == "list"){
                    
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if let arr = dict as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrBookAcct.add(dictA)
                                }
                                self.tblBookList.isHidden = false
                                self.lblstatus.isHidden =  true
                                self.tblBookList.reloadData()
                            }
                            else{
                                self.tblBookList.isHidden = true
                                self.lblstatus.isHidden =  false
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["message"] as! String
                            if(msg == "Your token has expired."){
                                self.viewWillAppear(true)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }
                else if(strCheck == "filter"){
                    
                    OperationQueue.main.addOperation {
                        Globalfunc.hideLoaderView(view: self.view)
                        
                        if let arr = dict as? [NSDictionary] {
                            if(arr.count > 0){
                                for i in 0..<arr.count{
                                    let dictA = arr[i]
                                    self.arrBookAcct.add(dictA)
                                }
                                self.tblBookList.isHidden = false
                                self.lblstatus.isHidden =  true
                                self.tblBookList.reloadData()
                            }
                            else{
                                self.tblBookList.isHidden = true
                                self.lblstatus.isHidden =  false
                            }
                        }
                        
                        if let arr = dict as? NSDictionary {
                            let msg = arr["message"] as! String
                            if(msg == "Your token has expired."){
                                self.viewWillAppear(true)
                            }
                            else{
                                Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: msg)
                            }
                        }
                    }
                }

            }
            else{
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                    
                }
            }
        }
    }
    
}

extension bookAccountViewController : UITableViewDataSource, UITableViewDelegate
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrBookAcct.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tblBookList.dequeueReusableCell(withIdentifier: "tblBookListCell") as! tblBookListCell
        
        let dict = self.arrBookAcct[indexPath.row] as! NSDictionary
        
        let bookingDict = dict["booking_id"] as! NSDictionary
        let strId = bookingDict["_id"] as! Int
        cell.lblId.text = "\(strId)"
        
        let strDescription = bookingDict["account_desc"] as! String
        cell.lblDescription.text = "\(strDescription)"
        
        let fname = dict["fname"] as! String
        let lname = dict["lname"] as! String
        cell.lblBorrowr.text = "\(fname) \(lname)"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-yyyy hh:mm a"
        
        let created_at = bookingDict["created_at"] as! String
        let date = Date.dateFromISOString(string: created_at)
        let createFormat = formatter.string(from: date!)
        cell.lblAddDate.text = "\(createFormat)"

        let updated_at = bookingDict["updated_at"] as! String
        let date_update = Date.dateFromISOString(string: updated_at)
        let updateFormat = formatter.string(from: date_update!)
        cell.lblLAstModified.text = "\(updateFormat)"
        
        let status = bookingDict["status"] as! String
        cell.lblStatus.text = "\(status)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 200
    }
    
}

//Filter Functionality
extension bookAccountViewController : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView == pickerViewBStatus){
            return Constant.arrBookingstatus.count
        }
       
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerView == pickerViewBStatus){
            return Constant.arrBookingstatus[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(pickerView == pickerViewBStatus){
            self.txt_BokingStatus.text = Constant.arrBookingstatus[row]
        }
       
        
    }
    //MARK:- TextFiled Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == self.txt_BokingStatus){
            self.pickUp(txt_BokingStatus)
        }
    }
    
  
    
    
    func pickUp(_ textField : UITextField) {
        
        if(textField == self.txt_BokingStatus){
            self.pickerViewBStatus = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.pickerViewBStatus.delegate = self
            self.pickerViewBStatus.dataSource = self
            self.pickerViewBStatus.backgroundColor = UIColor.white
            textField.inputView = self.pickerViewBStatus
        }
        
       
        // UIPickerView
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(bookAccountViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(bookAccountViewController.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    
    //MARK:- Button
    @objc func doneClick() {
        txt_BokingStatus.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        txt_BokingStatus.text = ""
        txt_BokingStatus.resignFirstResponder()
    }
    
    
    @IBAction func clickOnFilterBtn(_ sender: UIButton)
    {
        self.setView(view: self.viewSearchByFilter, hidden: false)
    }
    
    
    @IBAction func clickOnCrossBtn(_ sender: UIButton)
    {
        self.setView(view: self.viewSearchByFilter, hidden: true)
    }
    
    @IBAction func CLickOnSearch(_ sender: UIButton)
    {
        if(txt_BokingID.text == "" || txt_BokingID.text?.count == 0 || txt_BokingID.text == nil){
                txt_BokingID.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
         }
         else if(txt_BokingStatus.text == "" || txt_BokingStatus.text?.count == 0 || txt_BokingStatus.text == nil){
             txt_BokingStatus.shake()
             Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
         }
         else if(txt_lname.text == "" || txt_lname.text?.count == 0 || txt_lname.text == nil){
                txt_lname.shake()
                Globalfunc.showToastWithMsg(view: self.view, str: "Please fill this field.")
         }
         else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            
            self.arrBookAcct = []
            self.setView(view: self.viewSearchByFilter, hidden: true)
            let url = "\(Constant.search_borrower_url)?booking_id=\(txt_BokingID.text!)&lname=\(txt_lname.text!)&status=\(txt_BokingStatus.text!)"
            
            Globalfunc.print(object:url)
            
            callBorrowerApiOnPage(url: url, strCheck: "filter")
         }
         else{
             Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
         }
    }
    
    
    
}

class tblBookListCell : UITableViewCell
{
    @IBOutlet weak var lblId : UILabel!
    @IBOutlet weak var lblBorrowr : UILabel!
    @IBOutlet weak var lblDescription : UILabel!
    @IBOutlet weak var lblAddDate : UILabel!
    @IBOutlet weak var lblLAstModified : UILabel!
    @IBOutlet weak var lblStatus : UILabel!
}
