//
//  forgetPassViewController.swift
//  LMS
//
//  Created by Apple on 26/02/20.
//  Copyright © 2020 Reinforce. All rights reserved.
//

import UIKit

class forgetPassViewController: UIViewController {
    
    @IBOutlet weak var txtEmailAddress : UITextField!
    @IBOutlet weak var btnBack : UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let image = UIImage(named: "back")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(image, for: .normal)
        btnBack.tintColor = UIColorFromHex(rgbValue: 0x4D8DCE, alpha: 1.0)


    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default //.default for black style
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarUIView?.backgroundColor = UIColor.clear
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction func clickONSendEmailBtn(_ sender: UIButton)
    {
        if(txtEmailAddress.text == "" || txtEmailAddress.text?.count == 0 || txtEmailAddress.text == nil){
            txtEmailAddress.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill email field.")
        }
        else if(!(Globalfunc.isValidEmail(testStr: txtEmailAddress.text!))){
            txtEmailAddress.shake()
            Globalfunc.showToastWithMsg(view: self.view, str: "Please fill valid email field.")
        }
        
        else if (reach?.connection != .unavailable || reach?.connection == .wifi || reach?.connection == .cellular){
            self.callForgetPassApi()
        }
            
        else{
            Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: "PLease check your internet connection.")
        }
    }
    
    @IBAction func clickOnBaclBtn(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

}

extension forgetPassViewController {
    
    func callForgetPassApi(){
        
        let url = "\(Constant.forgetPass_url)?email=\(self.txtEmailAddress.text!)"
        BaseApi.callApiRequestForGetWithoutToken(url: url) { (dict, error) in
            if(error == ""){
               OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    let arrResponse = dict as? NSDictionary
                    Globalfunc.print(object:arrResponse!)
                    let _ = arrResponse?["msg"] as? String
                
                    let alertController = UIAlertController(title: Constant.AppName, message: "Password reset link has sent to your registered email address successfully!", preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
                        UIAlertAction in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    Globalfunc.hideLoaderView(view: self.view)
                    Globalfunc.showAlertMessage(vc: self, titleStr: Constant.AppName, messageStr: error)
                }
            }
        }
    }
}


