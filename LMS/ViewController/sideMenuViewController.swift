//
//  sideMenuViewController.swift
//  LMS
//
//  Created by Reinforce on 16/12/19.
//  Copyright © 2019 Reinforce. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ section : Int, index : Int, strType: String)  //strTblType: String
}

class sideMenuViewController: baseViewController {
    
    
    @IBOutlet weak var viewLeft: UIView!
    @IBOutlet weak var viewRight: UIView!
    
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    
    @IBOutlet weak var tblLeft: UITableView!
    @IBOutlet weak var tblRight: UITableView!
    
    
    @IBOutlet var btncloseLeft : UIButton!
    @IBOutlet var btncloseRight : UIButton!
    
    
    let kHeaderSectionTag: Int = 6900;
    //  var btnMenu : UIButton!
    
    
    //@IBOutlet weak var btnMainMenu: UIButton!
    @IBOutlet weak var lblUserNAme: UILabel!
    
    
    
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionItemsImg: Array<Any> = []
    var sectionNames: Array<Any> = []
    var arrProfile: Array<Any> = []
    
    
    var isType = ""
    var delegateMenu : SlideMenuDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // self.viewSetting.isHidden = true
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        sectionNames = [ "Home","Inventory", "Accounts","Tasking" ,"Reports", "Settings"]
        sectionItemsImg = ["home_menu","","accounts_menu", "task_list" ,"piechart_menu", "settings_menu"]
        arrProfile = ["Edit Profile", "Log Off"];

        
        let arrRoleid = user?.role_id
        let roledict = arrRoleid?[0]
        if(roledict?.title == "super admin"){
            sectionItems = [ [],["Add Inventory","Evaluate Inventory","Search Inventory","Queues"],
                             ["Search Accounts", "Book/Load an Account","Queues"],["Automated Task","Bin Maintenance"],["Reports"],
            ["Users", "Channels", "Flags", "Portfolios", "Manage"]]
        }
        else{
            sectionItems = [ [],["Add Inventory","Evaluate Inventory","Search Inventory","Queues"],
            ["Search Accounts", "Book/Load an Account","Queues"],["Automated Task","Bin Maintenance"],
            ["Reports"],
            ["Users", "Channels", "Flags", "Portfolios"]]
        }
        
        
        self.tblLeft.tableFooterView = UIView()
        self.tblLeft.register(sidemenuHeader.nib, forHeaderFooterViewReuseIdentifier: sidemenuHeader.reuseIdentifier)
        //self.tblLeft.reloadData()
        
        self.tblRight.tableFooterView = UIView()
        
        
        
        if isType == "left"{
            
            viewRight.alpha = 0.0
            viewLeft.alpha = 1.0
            
            btnLeft.isHidden = true
            btnRight.isHidden = false
            
            lblUserNAme.alpha = 1.0
            lblUserNAme.text = setUSernameOnProfileBtn()
            
        }
        else{
            viewRight.alpha = 1.0
            viewLeft.alpha = 0.0
            
            btnLeft.isHidden = false
            btnRight.isHidden = true
            
            lblUserNAme.alpha = 0.0
        }
        
        
    }
    
    
    
}


//MARK: handle menu
extension sideMenuViewController
{
    @IBAction func onCloseMenuClick(_ button:UIButton){
        
        if button == btncloseLeft{
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
            })
            
        }
        else if button == btncloseRight{
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
            })
        }
        
        
    }
    
    
    @IBAction func clickOnBarBtn(_ sender: UIButton){
        
        if sender == btnLeft{
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                
                self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onMoveSideRightToLeft(timer:)), userInfo: ["theButton" :sender], repeats: false)
            })
            
        }
        else if sender == btnRight{
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                
                
                self.view.frame = CGRect(x: UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                
                Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onMoveSideLefttoRight(timer:)), userInfo: ["theButton" :sender], repeats: false)
            })
        }
    }
    
    
    
    @objc func onMoveSideRightToLeft(timer:Timer)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.viewRight.alpha = 0.0
            self.viewLeft.alpha = 1.0
            
            self.btnRight.isHidden = false
            self.lblUserNAme.isHidden = false
            self.btnLeft.isHidden = true
        }, completion: nil)
    }
    
    
    @objc func onMoveSideLefttoRight(timer:Timer)
    {
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.viewRight.alpha = 1.0
            self.viewLeft.alpha = 0.0
            
            
            self.btnRight.isHidden = true
            self.lblUserNAme.isHidden = true
            self.btnLeft.isHidden = false
            
        }, completion: nil)
    }
    
}

//MARK: tableview delegate and datasource

extension sideMenuViewController : UITableViewDelegate , UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        var count = 0
        if tableView == self.tblLeft{
            
            count = sectionNames.count
            
        }
        
        if tableView == self.tblRight{
            
            count = arrProfile.count
            
        }
        return count
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblLeft{
            if (self.expandedSectionHeaderNumber == section) {
                let arrayOfItems = self.sectionItems[section] as! NSArray
                return arrayOfItems.count;
            }else{
                return 0
            }
        }
        if tableView == self.tblRight{
            return 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        if tableView == self.tblLeft{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblsideMEnu", for: indexPath) as! tblsideMEnu
            let section = self.sectionItems[indexPath.section] as! NSArray
            cell.lblNAme.text = section[indexPath.row] as? String
            return cell
        }
        
        if tableView == self.tblRight{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblProfileSetting", for: indexPath) as! tblProfileSetting
            cell.lblNAme.text = arrProfile[indexPath.section] as? String
            return cell
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.tblLeft{
            if (self.sectionNames.count != 0) {
                return self.sectionNames[section] as? String
            }
        }
        if tableView == self.tblRight{
            return ""
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.tblLeft{
            return 45
        }
        if tableView == self.tblRight{
            return 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.tblLeft{
            let btn = UIButton(type: UIButton.ButtonType.custom)
            
            btn.tag = indexPath.row
            
            Globalfunc.print(object:indexPath.section)
            
            switch(indexPath.section){
            case 0:
                Globalfunc.print(object:"home")
                break
            case 1:
                if(indexPath.row == 0){
                    openViewControllerBasedOnIdentifier("addInventoryViewController", strStoryName: "Inventory")
                }
                else if(indexPath.row == 1){
                    openViewControllerBasedOnIdentifier("", strStoryName: "Inventory")
                }
                else if(indexPath.row == 2){
                    openViewControllerBasedOnIdentifier("searchInventoryVC", strStoryName: "Inventory")
                }
                else if(indexPath.row == 3){
                    openViewControllerBasedOnIdentifier("inventoryQueuesVc", strStoryName: "Inventory")
                }


                break
            case 2:
                if(indexPath.row == 0){
                    openViewControllerBasedOnIdentifier("accountsViewController", strStoryName: "Main")
                }
                else if(indexPath.row == 1){
                    openViewControllerBasedOnIdentifier("bookAccountViewController", strStoryName: "Main")
                }
                else if(indexPath.row == 2){
                    openViewControllerBasedOnIdentifier("queuesViewController", strStoryName: "Queues")
                }
                break
            case 3:  //tasking
                if(indexPath.row == 0){
                    openViewControllerBasedOnIdentifier("AutomatedTaskViewController", strStoryName: "taskingStory")
                }
                else if(indexPath.row == 1){
                    openViewControllerBasedOnIdentifier("binMViewController", strStoryName: "taskingStory")
                }
                break
            case 4: //report
                if(indexPath.row == 0){
                }
                break
            case 5:
                if(indexPath.row == 0){
                    openViewControllerBasedOnIdentifier("usersViewController", strStoryName: "SettingStory")
                }
                else if(indexPath.row == 1){
                    openViewControllerBasedOnIdentifier("channelListViewController", strStoryName: "SettingStory")
                    
                }
                else if(indexPath.row == 2){
                    openViewControllerBasedOnIdentifier("flagsViewController", strStoryName: "SettingStory")
                }
                else if(indexPath.row == 3){
                    
                }
                else if(indexPath.row == 4){
                    openViewControllerBasedOnIdentifier("manageSettingViewController", strStoryName: "SettingStory")
                }
                break
                
            default:
                print("default\n", terminator: "")
            }
            
        }
        
        if tableView == self.tblRight{
            
            switch indexPath.section {
            case 0:
                self.openViewControllerBasedOnIdentifier("editProfileViewController", strStoryName: "Main")
                break
                
            case 1:
                
                userDef.removeObject(forKey: "userInfo")
                self.openViewControllerBasedOnIdentifier("loginViewController", strStoryName: "Main")
                // let login = self.storyboard?.instantiateViewController(identifier: "loginViewController") as!
                //  self.navigationController?.pushViewController(login, animated: true)
                //  break
                
            default:
                print("Home\n", terminator: "")
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if let viewHeader = tableView.dequeueReusableHeaderFooterView(
            withIdentifier: sidemenuHeader.reuseIdentifier)
            as? sidemenuHeader{
            
            let color = UIColor.black //UIColor(red:60/255, green:131/255, blue:195/255, alpha:1.0)
            //viewHeader.viewBg.backgroundColor = color.lighter(by: 13)
            
            let strImg = self.sectionItemsImg[section] as? String
            viewHeader.imgIcon.image = UIImage(named: strImg!)
            
            viewHeader.imgIcon.image = viewHeader.imgIcon.image?.withRenderingMode(.alwaysTemplate)
            viewHeader.imgIcon.tintColor = color.darker(by: 13)
            
            viewHeader.lblLine.backgroundColor = UIColor(red:60/255, green:131/255, blue:195/255, alpha:1.0)   //.darker(by: 13)
            
            viewHeader.lblTitle.text = self.sectionNames[section] as? String
            viewHeader.lblTitle.textColor = color.darker(by: 13)
            
            
            let sectionArr = self.sectionItems[section] as! NSArray
            if (sectionArr.count > 0){
                viewHeader.imgDropdown.isHidden = false
                
                viewHeader.imgDropdown.image = viewHeader.imgDropdown.image?.withRenderingMode(.alwaysTemplate)
                viewHeader.imgDropdown.tintColor = color.darker(by: 13)
                
            }else{
                viewHeader.imgDropdown.isHidden = true
            }
            
            if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
                viewWithTag.removeFromSuperview()
            }
            // make headers touchable
            viewHeader.tag = section
            let headerTapGesture = UITapGestureRecognizer()
            headerTapGesture.addTarget(self, action: #selector(sideMenuViewController.sectionHeaderWasTouched(_:)))
            viewHeader.addGestureRecognizer(headerTapGesture)
            
            return viewHeader
        }
        
        return nil
        
    }
    
    
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        // let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as? UIImageView
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section)
            } else {
                let _ = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as? UIImageView
                tableViewCollapeSection(self.expandedSectionHeaderNumber)
                tableViewExpandSection(section)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            
            return;
        } else {
            //              UIView.animate(withDuration: 0.4, animations: {
            //                  imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            //              })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tblLeft.beginUpdates()
            self.tblLeft.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblLeft.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            
            self.openViewControllerBasedOnIdentifier("homeViewController", strStoryName: "Main")
            
            //  btnMenu.tag = 0
            //              UIView.animate(withDuration: 0.3, animations: { () -> Void in
            //                    self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            //                    self.view.layoutIfNeeded()
            //                    self.view.backgroundColor = UIColor.clear
            //              }, completion: { (finished) -> Void in
            //
            //                    self.view.removeFromSuperview()
            //                    self.removeFromParent()
            //
            //
            //              })
            return;
        } else {
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tblLeft.beginUpdates()
            self.tblLeft.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tblLeft.endUpdates()
        }
    }
    
    
}

class tblsideMEnu: UITableViewCell
{
    @IBOutlet weak var lblNAme : UILabel!
    
}

class tblProfileSetting : UITableViewCell
{
    @IBOutlet weak var lblNAme : UILabel!
}
